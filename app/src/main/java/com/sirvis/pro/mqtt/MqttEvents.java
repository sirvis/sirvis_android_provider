package com.sirvis.pro.mqtt;

/**
 * Created by moda on 29/06/17.
 */


public enum MqttEvents {

    Booking("booking"),

    //    LastWillTopic("location"),
    LastWillTopic("lastWill"),

    PresenceTopic("PresenceTopic"),

    LiveTrack("liveTrack"),

    JobStatus("jobStatus"),

    JobStatusByAdmin("jobStatusByAdmin"),

    Message("message"),

    Call("call"),
    Calls("Calls"),
  /*  Calls("call"),
    Call("Calls"),*/
    CallsAvailability("CallsAvailability");


    public String value;


    MqttEvents(String value) {
        this.value = value;
    }


}