package com.sirvis.pro.mqtt;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.main.chats.ChatOnMessageCallback;
import com.sirvis.pro.pojo.callpojo.LiveTackPojo;
import com.sirvis.pro.pojo.callpojo.NewCallData;
import com.sirvis.pro.pojo.callpojo.NewCallMqttResponse;
import com.sirvis.pro.pojo.chat.ChatData;
import com.sirvis.pro.pojo.chat.ChatMqttResponce;
import com.sirvis.pro.telecall.RxCallInfo;
import com.sirvis.pro.telecall.UtilityVideoCall;
import com.sirvis.pro.telecall.utility.CallingApis;
import com.sirvis.pro.telecall.utility.LiveTrackObservable;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.NotificationHelper;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by murashid on 10-Jan-18.
 */

public class MqttHelper {

    private static final String TAG = "MqttHelper";
    private static MqttAndroidClient mqttAndroidClient;
    private static MqttConnectOptions mqttConnectOptions;
    private static IMqttActionListener listener;

    private Context context;
    private SessionManager sessionManager;
    private ChatOnMessageCallback chatListener;

    private boolean isFromCall = false;
    private String userId = "";
    private JSONObject callObj;

    public MqttHelper(Context context) {
        this.context = context;
        sessionManager = SessionManager.getSessionManager(context);
        listener = new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {
                updatePresence(1, false);
                subscribeToTopic(sessionManager.getProviderId(), 1);
                subscribeToTopic(MqttEvents.Booking.value + "/" + sessionManager.getProviderId(), 1);
                subscribeToTopic(MqttEvents.JobStatus.value + "/" + sessionManager.getProviderId(), 1);
                subscribeToTopic(MqttEvents.Message.value + "/" + sessionManager.getProviderId(), 1);
                subscribeToTopic(MqttEvents.Calls.value + "/" + sessionManager.getProviderId(), 1);
//                subscribeToTopic(MqttEvents.Call.value + "/" + sessionManager.getProviderId(), 1);

                Log.d(TAG, "onSuccess: myqtt client ");

                if (isFromCall) {
                    isFromCall = false;
                    AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value + "/" + userId, callObj, 0, true);
                } else {
                    JSONObject tempObj = new JSONObject();
                    try {
                        tempObj.put("status", 1);

                        AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getCallHelper().getUserId(), tempObj, 0, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                Log.d(TAG, "onFailure: myqtt client ");
//                Toast.makeText(this,"mqtt connection failure",Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void setChatListener(ChatOnMessageCallback chatListener) {
        this.chatListener = chatListener;
    }

    public void setUserId(String userId, JSONObject callObj) {
        this.userId = userId;
        this.callObj = callObj;
    }


    /*
     * handle new Call data and open incoming screen
     */
    public void handleNewCall(String callDataResponse) {
        try {
            NewCallMqttResponse newCallMqttResponse = new Gson().fromJson(callDataResponse, NewCallMqttResponse.class);
            NewCallData callData = newCallMqttResponse.getData();
            CallingApis.OpenIncomingCallScreen(callData, context);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private void handleOnGoingCalls(String payload) {

        Log.d(TAG, "handleOnGoingCalls: " + payload + " myContext " + context);

        try {

            JSONObject jobs = new JSONObject();


            JSONObject jsonObject = new JSONObject(payload);
            jsonObject.put("eventName", MqttEvents.Calls.value);

            JSONObject obj = new JSONObject();
            if (jsonObject.getString("type").equals("0") ||
                    jsonObject.getString("type").equals("audio")) {
                jobs.put("status", 0);
                Log.d(TAG, "handleOnGoingCalls: " + jsonObject.getString("type"));
                obj.put("callId", jsonObject.getString("callId"));
                obj.put("callerImage", jsonObject.getString("callerImage"));
                obj.put("callerName", jsonObject.getString("callerName"));
                obj.put("callerId", jsonObject.getString("callerId"));
                obj.put("callType", jsonObject.getString("callType"));
                obj.put("callerIdentifier", jsonObject.getString("callerIdentifier"));
                obj.put("bookingId", jsonObject.getString("bookingId"));
                UtilityVideoCall.getInstance().setActiveOnACall(true, true);
                CallingApis.OpenIncomingCallScreen(jsonObject, context);
                publish(MqttEvents.CallsAvailability.value + "/" + sessionManager.getProviderId(), jobs, 0, true);//UserId

                Log.d(TAG, "handleOnGoingCallsJSON: " + jsonObject);
            } else {
                jobs.put("status", 1);
                Log.d(TAG, "handleOnGoingCalls: " + jsonObject.toString());
                RxCallInfo.getInstance().emitData(jsonObject.toString());
                publish(MqttEvents.CallsAvailability.value + "/" + sessionManager.getProviderId(), jobs, 0, true);//UserId
            }
//com.telecall.RxCallInfo.getInstance().emitData(jsonObject.toString());
// com.telecall.RxCallInfo.getInstance().emitData(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * handle new Call data and open incoming screen
     */
    public void handleActiveCall(String callDataResponse) {
        try {
            //ActiveCallResponse activeCallResponse = gson.fromJson(callDataResponse,ActiveCallResponse.class);
            //ActiveCallData activeCallData = activeCallResponse.getData();

            //Publish to the rx
            //action type
            /*
            2 call not Answer of left from a call
            3 join on call
            4 call ended.
            */
            String dataRes = new JSONObject(callDataResponse).getJSONObject("data").toString();
            RxCallInfo.getInstance().emitData(dataRes);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    // region Mqqtt connection
    @SuppressWarnings("unchecked")
    public void createMQttConnection(String clientId, boolean isSetWill) {
        String serverUri = "tcp://" + BuildConfig.MQTT_HOST + ":" + BuildConfig.MQTT_PORT;

//        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId, new MemoryPersistence(), MqttAndroidClient.Ack.AUTO_ACK);
        mqttAndroidClient = new MqttAndroidClient(context, serverUri, clientId + (System.currentTimeMillis() / 1000));
        mqttAndroidClient.setCallback(new MqttCallback() {

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                /*
                 * To parse the result of the message received on a MQtt topic
                 */
                Log.d(TAG, "messageArrived: " + message);
                JSONObject obj = convertMessageToJsonObject(message);
                Log.d(TAG, "messageArrivedTopic: " + topic);
                Log.d(TAG, "messageArrived: " + obj);


                if (topic.equals(sessionManager.getProviderId())) {
                    handleNewCall(new String(message.getPayload()));//handle new Incoming call
                } else if (topic.startsWith(MqttEvents.Call.value) || topic.startsWith(MqttEvents.Calls.value) || topic.startsWith(MqttEvents.CallsAvailability.value)) {
                    String topicSplit[] = topic.split("/");
                    if (topicSplit[0].equals(MqttEvents.Call.value) || topicSplit[0].equals(MqttEvents.Calls.value) || topicSplit[0].equals(MqttEvents.CallsAvailability.value)) {

                        if (topicSplit[0].equals(MqttEvents.Call.value /*+ "/" + com.telecall.UtilityVideoCall.getInstance().getActiveCallId()*/)) {
                            handleActiveCall(new String(message.getPayload()));//handle the call that has been called by you

                        } else if (topicSplit[0].equals(MqttEvents.Calls.value /*+ "/" + com.telecall.UtilityVideoCall.getInstance().getActiveCallId()*/)) {
                            handleOnGoingCalls(new String(message.getPayload()));//handle actons of ongoing calls
                        } else if (topicSplit[0].equals(MqttEvents.CallsAvailability.value /*+ "/" + com.telecall.UtilityVideoCall.getInstance().getActiveCallId()*/)) {
                            handleCallsAvailability(new String(message.getPayload()));
                            unsubscribeToTopic(topic);
                        }
                    }

                } else if (topic.equals(MqttEvents.LiveTrack.value + "/" + UtilityVideoCall.getInstance().getActiveCallId())) {
                    // handleOnGoingCalls(new String(message.getPayload()));
                } else if (topic.equals(MqttEvents.Booking.value + "/" + sessionManager.getProviderId())) {
//                    JSONObject jsonObjectBooking = new JSONObject(obj.getJSONObject("data").toString());
                    //
                    if (!obj.getString("data").equals(sessionManager.getLastBooking())) {
                        sessionManager.setLastBooking(obj.getString("data"));
                        sessionManager.setIsNewBooking(true);
                        Log.d(TAG, "Booking from Mqtt");
//                        sessionManager.setIncmgCallBookingType(jsonObjectBooking.getString("callType"));
                        String title = context.getString(R.string.newBookingTitle);
                        String notificationMsg = context.getString(R.string.newBookingMsg);
                       /*   boolean isAssignedBooking = false;

                      if (jsonObjectBooking.has("status") && jsonObjectBooking.getString("status").equals("3")) {
                            sessionManager.setIsAssignedBooking(true);
                            isAssignedBooking = true;
                        }*/

                        if (VariableConstant.IS_MYBOOKING_OPENED /*&& !isAssignedBooking*/) {
                            //justRefresh();
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                            context.sendBroadcast(intent);
                        } else if (VariableConstant.IS_ACCEPTEDBOOKING_OPENED /*&& isAssignedBooking*/) {
                            //justRefresh();
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                            context.sendBroadcast(intent);
                        } else {
                            VariableConstant.IS_BOOKING_UPDATED = true;
                            sessionManager.setNewBookingFromMain(true);
                            Intent intentOpen = new Intent(context, MainActivity.class);
                            intentOpen.putExtra("callType", obj.getString("callType"));
                            intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(intentOpen);
                        }

                        Utility.updateBookingAck(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), obj.getString("bookingId"), sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                    }
                } else if (topic.equals(MqttEvents.JobStatus.value + "/" + sessionManager.getProviderId())) {
                    JSONObject cancelJsonObject = obj.getJSONObject("data");
                    String bookingId = cancelJsonObject.getString("bookingId");
                    String header = cancelJsonObject.has("statusMsg") ? cancelJsonObject.getString("statusMsg") :
                            context.getString(R.string.message);
                    String msg = cancelJsonObject.has("cancellationReason") ? cancelJsonObject.getString("cancellationReason") :
                            context.getString(R.string.bookingUnassigned);
                    String action = cancelJsonObject.has("status") ? cancelJsonObject.getString("status") :
                            "12";

                    if (cancelJsonObject.has("msg")) {
                        msg = cancelJsonObject.getString("msg");
                    }

                    if (!bookingId.equals(sessionManager.getLastBookingIdCancel())) {
                        sessionManager.setLastBookingIdCancel(bookingId);
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                        intent.putExtra("cancelid", bookingId);
                        intent.putExtra("header", header);
                        intent.putExtra("msg", msg);
                        intent.putExtra("action", action);
                        context.sendBroadcast(intent);

                        VariableConstant.IS_BOOKING_UPDATED = true;
                        NotificationHelper.sendNotification(context, "15", header, msg);

                        Log.d(TAG, "Booking cancel from Mqtt");

                    }
                } else if (topic.equals(MqttEvents.Message.value + "/" + sessionManager.getProviderId())) {
                    Gson gson = new Gson();
                    ChatMqttResponce chatMqttResponce = gson.fromJson(obj.toString(), ChatMqttResponce.class);
                    ChatData chatData = chatMqttResponce.getData();
                    if (chatData.getTimestamp() > sessionManager.getLastTimeStampMsg()) {
                        sessionManager.setLastTimeStampMsg(chatData.getTimestamp());

                        if (chatListener != null) {
                            chatListener.onMessageReceived(chatData);
                        }

                        if (!VariableConstant.IS_CHATTING_RESUMED) {
                            sessionManager.setChatCount(String.valueOf(chatData.getBid()), sessionManager.getChatCount(String.valueOf(chatData.getBid())) + 1);
                            sessionManager.setChatBookingID(String.valueOf(chatData.getBid()));
                            sessionManager.setChatCustomerName(chatData.getName());
                            sessionManager.setChatCustomerID(chatData.getFromID());

                            NotificationHelper.sendChatNotification(context, chatData.getName(), chatData.getContent(), sessionManager.getChatNotificationId());
                            sessionManager.setChatNotificationId(sessionManager.getChatNotificationId() + 1);

                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_NEW_CHAT);
                            context.sendBroadcast(intent);
                        } else if (VariableConstant.IS_IN_CALL) {
                            NotificationHelper.sendChatNotification(context, chatData.getName(), chatData.getContent(), sessionManager.getChatNotificationId());
                            sessionManager.setChatNotificationId(sessionManager.getChatNotificationId() + 1);
                        }
                    }

                }
//                else if (topic.equals(MqttEvents.Calls.value + "/" + sessionManager.getProviderId()))
//                {
//                    /*
//                    * Incoming Call handling though Mqtt
//                    * */
//                    /*
//                     * For receiving of the call request,will set mine status to be busy as well and open the incoming call screen
//                     */
//                    /*
//                     * Have to retain the message of currently being busy
//                     */
//                    if(obj.getString("type").equals("0"))
//                    {
//                       /* sessionManager.setChatCount(obj.getString("bookingId"), sessionManager.getChatCount(obj.getString("bookingId")+1));
//                        sessionManager.setChatBookingID(obj.getString("bookingId"));
//                        sessionManager.setChatCustomerName(obj.getString("callerName"));
//                        sessionManager.setChatCustomerID(obj.getString("callerId"));
//
//                        JSONObject tempObj = new JSONObject();
//                        tempObj.put("status", 0);
//                        AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getCallHelper().getUserId(), tempObj, 0, true);
//                        AppController.getInstance().getCallHelper().setActiveOnACall(true, true);
//                        CallingApis.OpenIncomingCallScreen(obj , context);*/
//                    }
//                    else
//                    {
//                        obj.put("eventName", topic);
//                        /*
//                         * Will have to eventually unsubscribe from this topic,as only use  of this is to check if opponent is available to receive the call
//                         */
//                        Log.d(TAG, "messageArrivedAvail: "+obj);
//                        Intent intent = new Intent();
//                        intent.setAction(VariableConstant.INTENT_ACTION_CALL);
//                        intent.putExtra("value", obj.toString());
//                        context.sendBroadcast(intent);
//                    }
//                }
                //topic.equals(MqttEvents.Message.value+"/"+sessionManager.getProviderId())
                else {
                    try {
                        String split[] = topic.split("/");
                        if (split[0].equals(MqttEvents.CallsAvailability.value)) {
                            obj.put("eventName", split[0]);
                            /*
                             * Will have to eventually unsubscribe from this topic,as only use  of this is to check if opponent is available to receive the call
                             */
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_CALL);
                            intent.putExtra("value", obj.toString());
                            context.sendBroadcast(intent);
                            /*
                             * For message received on the call event to check for the availability
                             */
                            unsubscribeToTopic(topic);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {


            }
        });




      /*  JSONObject obj = new JSONObject();
        try {
            obj.put("id", sessionManager.getProviderId());
            obj.put("status", VariableConstant.TIME_OUT);
            obj.put("lat", sessionManager.getCurrentLat());
            obj.put("long", sessionManager.getCurrentLng());
            obj.put("time", Utility.getCurrentTime());
        } catch (JSONException e) {

        }*/



        /*
         * Has been removed from here to avoid the reace condition for the mqtt connection with the mqtt broker
         */
        connectMqttClient(isSetWill);

    }

    private void handleCallsAvailability(String payload) {
        Log.d(TAG, "handleCallsAvailability: " + payload);
        try {
            JSONObject jsonObject = new JSONObject(payload);
            jsonObject.put("eventName", MqttEvents.CallsAvailability.value);
            RxCallInfo.getInstance().emitData(jsonObject.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // endregion

    @SuppressWarnings("TryWithIdenticalCatches")
    public void publish(String topicName, JSONObject obj, int qos, boolean retained) {
        Log.d(TAG, "publishT: " + topicName + "=" + obj + "=" + retained);
        try {
            mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void handleLiveBookingStatus(String jsonObject) {

        Log.d("LIVETRACK", "managerMqttLIVE: " + jsonObject);
        LiveTackPojo liveTrackPojo = new Gson().fromJson(jsonObject, LiveTackPojo.class);
        LiveTrackObservable.getInstance().emitLiveTrack(liveTrackPojo);
        VariableConstant.LiveTrackBookingPid = liveTrackPojo.getPid();
    }

    private JSONObject convertMessageToJsonObject(MqttMessage message) {

        JSONObject obj = new JSONObject();
        try {

            obj = new JSONObject(new String(message.getPayload()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }


    private void connectMqttClient(boolean isSetWill) {
        try {
            mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setCleanSession(false);
            mqttConnectOptions.setAutomaticReconnect(true);
            byte[] payload = sessionManager.getProviderId().getBytes();
            mqttConnectOptions.setKeepAliveInterval(45/*Integer.parseInt(sessionManager.getProTimeOut())*/);
            if (isSetWill) {
                Log.d("WillTopic: ", "" + true);
                mqttConnectOptions.setWill(MqttEvents.LastWillTopic.value, payload, 0, false);
            } else {
                Log.d("WillTopic: ", "" + false);
            }
            mqttAndroidClient.connect(mqttConnectOptions, context, listener);

        } catch (MqttException e) {

        }
    }

    public void subscribeToTopic(String topic, int qos) {

        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient.subscribe(topic, qos);
            }
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @param topic Topic name from which to  unsubscribe
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    public void unsubscribeToTopic(String topic) {

        try {
            if (mqttAndroidClient != null) {
                mqttAndroidClient.unsubscribe(topic);
            }
        } catch (Exception e) {

        }
    }

    public void updatePresence(int status, boolean applicationKilled) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("id", 1);
            obj.put("status", "");


            publish(MqttEvents.PresenceTopic.value + "/" + "", obj, 0, true);
        } catch (JSONException w) {

        }
    }


    /**
     * @return boolean value depending on whther currently can publish or not
     */

    public boolean isMqttConnected() {
        try {
            return mqttAndroidClient != null && mqttAndroidClient.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * To disconnect the MQtt client on app being killed
     */

    public void disconnect(/*String mqttTopic*/) {
        try {
            if (mqttAndroidClient != null) {
                unsubscribeToTopic(sessionManager.getProviderId());
                mqttAndroidClient.disconnect();
            }
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Callback when reconnection is made
     */
    public void updateReconnected() {
        updatePresence(1, false);
    }


}
