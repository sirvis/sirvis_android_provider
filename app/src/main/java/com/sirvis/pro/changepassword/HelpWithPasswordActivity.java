package com.sirvis.pro.changepassword;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.sirvis.pro.R;
import com.sirvis.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.Utility;


/**
 * Created by murashid on 11-Sep-17.
 * <h1>HelpWithPasswordActivity</h1>
 * HelpWithPasswordActivity Activity is used to change the password of the user by email or phone number
 */

public class HelpWithPasswordActivity extends AppCompatActivity implements View.OnClickListener, HelpWithPassworPresenter.HelpWithPassworPresenterImple, View.OnFocusChangeListener {

    private EditText etPhone,etEmail;
    private LinearLayout llPhone;

    private TextView tvForgotPassNext,tvHelpWithPasswordMsg;

    private ProgressDialog progressDialog;
    private HelpWithPassworPresenter presenter;

    private TextView tvPhoneNumber,tvEmai;
    private View vPhoneNumber,vEmail;

    private CountryCodePicker ccp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_with_password);

        init();
    }
    /**
     * <h1>init Views</h1>
     * initilize the Views
     */
    private void init() {
        presenter = new HelpWithPassworPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.validatingPhone));
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.helpWithPassword));
        tvTitle.setTypeface(fontBold);

        llPhone = findViewById(R.id.llPhone);
        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);

        tvForgotPassNext = findViewById(R.id.tvForgotPassNext);
        tvHelpWithPasswordMsg = findViewById(R.id.tvHelpWithPasswordMsg);

        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvEmai = findViewById(R.id.tvEmai);

        vPhoneNumber = findViewById(R.id.vPhoneNumber);
        vEmail = findViewById(R.id.vEmail);

        LinearLayout llPhoneSelection = findViewById(R.id.llPhoneSelection);
        LinearLayout llEmailSelection = findViewById(R.id.llEmailSelection);

        ccp = findViewById(R.id.ccp);


        etPhone.setTypeface(fontRegular);
        etEmail.setTypeface(fontRegular);
        tvForgotPassNext.setTypeface(fontBold);
        tvHelpWithPasswordMsg.setTypeface(fontBold);
        tvPhoneNumber.setTypeface(fontMedium);
        tvEmai.setTypeface(fontMedium);

        tvForgotPassNext.setOnClickListener(this);
        llPhoneSelection.setOnClickListener(this);
        llEmailSelection.setOnClickListener(this);

        etPhone.setOnFocusChangeListener(this);
        etEmail.setOnFocusChangeListener(this);

        etPhone.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    presenter.forgotPassword(ccp.getSelectedCountryCodeWithPlus(),etPhone.getText().toString(),"1");
                    return true;
                }
                return false;
            }
        });

        etEmail.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    presenter.forgotPassword(ccp.getSelectedCountryCodeWithPlus(),etEmail.getText().toString(),"2");
                    return true;
                }
                return false;
            }
        });

        try {
            ccp.setCountryForNameCode(Utility.getCurrentCountryCode(this));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * Close the activity with animation
     */
    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }


    @Override
    public void onFocusChange(View view, boolean b) {
        if(b)
        {
            switch (view.getId())
            {
                case R.id.etPhone:
                    Utility.showKeyBoard(this,etPhone);
                    break;

                case R.id.etEmail:
                    Utility.showKeyBoard(this,etEmail);
                    break;

            }
        }
    }

    /**********************************************************************************************/
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvForgotPassNext:
                if(etEmail.getVisibility() == View.VISIBLE)
                {
                    presenter.forgotPassword(ccp.getSelectedCountryCodeWithPlus(),etEmail.getText().toString(),"2");
                }
                else
                {
                    presenter.forgotPassword(ccp.getSelectedCountryCodeWithPlus(),etPhone.getText().toString(),"1");
                }
                break;


            case R.id.llEmailSelection:
                progressDialog.setMessage(getString(R.string.validatingEmail));
                tvHelpWithPasswordMsg.setText(getString(R.string.helpWithPasswordEmailMsg));
                llPhone.setVisibility(View.GONE);
                etEmail.setVisibility(View.VISIBLE);
                tvForgotPassNext.setText(getString(R.string.oK));

                tvEmai.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                vEmail.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                tvPhoneNumber.setTextColor(ContextCompat.getColor(this,R.color.lightestTextColor));
                vPhoneNumber.setBackgroundColor(ContextCompat.getColor(this,R.color.lightestTextColor));

                etEmail.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        etEmail.requestFocus();
                    }
                },200);
                break;

            case R.id.llPhoneSelection:
                progressDialog.setMessage(getString(R.string.validatingPhone));
                tvHelpWithPasswordMsg.setText(getString(R.string.helpWithPasswordPhoneMsg));
                llPhone.setVisibility(View.VISIBLE);
                etEmail.setVisibility(View.GONE);
                tvForgotPassNext.setText(getString(R.string.next));

                tvPhoneNumber.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
                vPhoneNumber.setBackgroundColor(ContextCompat.getColor(this,R.color.colorPrimary));
                tvEmai.setTextColor(ContextCompat.getColor(this,R.color.lightestTextColor));
                vEmail.setBackgroundColor(ContextCompat.getColor(this,R.color.lightestTextColor));
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessPhone(PhoneValidationPojo phoneValidationPojo) {
        finish();
        Intent intent = new Intent(this, OTPVerifyActivity.class);
        intent.putExtra("phone",etPhone.getText().toString());
        intent.putExtra("countryCode",ccp.getSelectedCountryCodeWithPlus());
        intent.putExtra("userId", phoneValidationPojo.getData().getSid());
        intent.putExtra("expireOtp", phoneValidationPojo.getData().getExpireOtp());
        intent.putExtra("otpOption",2);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else
        {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
        }
    }

    @Override
    public void onPhoneNumberError() {
      /*  tilPhone.setError(getString(R.string.enterPhone));
        tilPhone.setErrorEnabled(true);*/

        Toast.makeText(this,getString(R.string.plsEnterPhoneNumber),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessEmail(String msg) {
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.emailVerification),msg,getString(R.string.oK));
    }

    @Override
    public void onEmailError() {
        Toast.makeText(this,getString(R.string.enterValidEmail),Toast.LENGTH_SHORT).show();
    }

}
