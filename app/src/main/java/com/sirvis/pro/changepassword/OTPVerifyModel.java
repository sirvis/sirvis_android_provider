package com.sirvis.pro.changepassword;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.signup.LoginData;
import com.sirvis.pro.pojo.signup.LoginPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 12-Sep-17.
 * <h1>OTPVerifyModel</h1>
 * OTPVerifyModel model for OTPVerify Activity
 * @see OTPVerifyActivity
 */

public class OTPVerifyModel
{
    private static final String TAG = "ForgotPasswordVerify";
    private OTPVerifyModelImple modelImplement;

    OTPVerifyModel(OTPVerifyModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }


    /**
     * <h1>resendOtp</h1>
     * method for calling api for resend the otp
     * @param userId userId
     * @param otpOption 1 => Signup verificaiton, 2 => Forgot Password Verification , 3 => Change Number
     */
    void resendOtp(String userId,int otpOption)
    {
        String serviceUrl = ServiceUrl.FORGOT_PASSWORD;
        JSONObject jsonObject = new JSONObject();

        try {
            serviceUrl = ServiceUrl.RESENT_OTP_FORGOT_PASSWORD;
            jsonObject.put("userId", userId);
            jsonObject.put("userType", VariableConstant.USER_TYPE);
            jsonObject.put("trigger", otpOption);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", serviceUrl , OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Utility.printLog(TAG, "onSuccess resendOtp: "+statusCode+"\n"+ result);
                        JSONObject jsonObject = new JSONObject(result);

                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessResendCode(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     *<h1>verifyOTP</h1>
     * method for calling api for verifying the otp
     * @param otp otp
     * @param userId userid
     * @param otpOption 1 => Signup verificaiton, 2 => Forgot Password Verification , 3 => Change Number
     */
    void verifyOTP(String otp, String userId, final int otpOption)
    {
        if(otp.length() != 4)
        {
            modelImplement.onVerfyiCodeError();
            return;
        }
        else
        {
            modelImplement.onVerfyiCodeCorrect();
        }

        String serviceUrl = ServiceUrl.VERIFY_OTP_SIGNUP;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("code",otp );
            switch (otpOption)
            {
                case 1:
                    serviceUrl = ServiceUrl.VERIFY_OTP_SIGNUP;
                    jsonObject.put("providerId", userId);
                    break;

                case 2:
                case 3:
                    serviceUrl = ServiceUrl.VERIFY_OTP_FORGOT_PASSWORD_CHANGE_PHONE_NUMBER;
                    jsonObject.put("userId", userId);
                    jsonObject.put("userType", VariableConstant.USER_TYPE);
                    jsonObject.put("trigger", otpOption);
                    break;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", serviceUrl, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Utility.printLog(TAG, "onSuccess verifyOTP: "+statusCode+"\n"+ result);

                        JSONObject jsonObject = new JSONObject(result);

                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            switch (otpOption)
                            {
                                case 1:
                                    Gson gson = new Gson();
                                    LoginPojo loginPojo = gson.fromJson(result,LoginPojo.class);
                                    modelImplement.onSuccessSignupOtp(loginPojo.getData());
                                    break;

                                case 2:
                                    modelImplement.onSuccessForgotPasswordOTP(jsonObject.getString("message"));
                                    break;

                                case 3:
                                    modelImplement.onSuccessChangeNumberdOTP(jsonObject.getString("message"));
                                    break;

                                default:
                                    modelImplement.onSuccessChangeNumberdOTP(jsonObject.getString("message"));
                                    break;
                            }
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    }
                    else
                    {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * <h1>OTPVerifyModelImple</h1>
     * interface for Presenter implementation
     * @see OTPVerifyPresenter
     */
    interface OTPVerifyModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccessResendCode(String msg);
        void onSuccessSignupOtp(LoginData loginData);
        void onSuccessForgotPasswordOTP(String msg);
        void onSuccessChangeNumberdOTP(String msg);
        void onVerfyiCodeError();
        void onVerfyiCodeCorrect();
    }

}
