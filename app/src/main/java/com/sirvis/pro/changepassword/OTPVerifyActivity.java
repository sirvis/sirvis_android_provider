package com.sirvis.pro.changepassword;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.EventStartedCompletedActivity;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.pojo.signup.LoginData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.List;
import java.util.concurrent.TimeUnit;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>OTPVerifyActivity</h1>
 * OTPVerifyActivity Activity is used to enter the otp for follwing option
 *  otpOption 1 => Signup verificaiton, 2 => Forgot Password Verification , 3 => Change Number
 *
 */
/**************************************************************************************************/
public class OTPVerifyActivity extends AppCompatActivity implements View.OnClickListener, OTPVerifyPresenter.OTPVerifyPresenterImple, TextWatcher/*,EasyPermissions.PermissionCallbacks*/ {

    private static final String TAG = OTPVerifyActivity.class.getName();

    private EditText etOtp1,etOtp2,etOtp3,etOtp4;
    private String phone,countryCode,userId;
    private TextView tvTimer,tvResendCode;
    private ProgressDialog progressDialog;
    private OTPVerifyPresenter presenter;
    private int otpOption = 0;


    private SessionManager sessionManager;
    private String expireOtp="60";
    private int normalTextColor,lightestTextColor;

    /**********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        init();
        startTimer(Long.parseLong(expireOtp));

    }


   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (EasyPermissions.hasPermissions(this, VariableConstant.SMS_PERMISSION)) {
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }*/


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * <h1>initializeViews</h1>
     * <p>this is the method, for initialize the views</p>
     */
    private void init() {

        sessionManager = SessionManager.getSessionManager(this);
        normalTextColor = ContextCompat.getColor(OTPVerifyActivity.this, R.color.normalTextColor);
        lightestTextColor = ContextCompat.getColor(OTPVerifyActivity.this, R.color.lightestTextColor);

        Intent intent = getIntent();
        phone = intent.getStringExtra("phone");
        countryCode = intent.getStringExtra("countryCode");
        userId = intent.getStringExtra("userId");
        expireOtp = intent.getStringExtra("expireOtp");
        otpOption = intent.getIntExtra("otpOption",0);

        presenter = new OTPVerifyPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.verifyphoneNumber));
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.verifyYourNumber));
        tvTitle.setTypeface(fontBold);

        etOtp1 = findViewById(R.id.etOtp1);
        etOtp2 = findViewById(R.id.etOtp2);
        etOtp3 = findViewById(R.id.etOtp3);
        etOtp4 = findViewById(R.id.etOtp4);

        tvTimer = findViewById(R.id.tvTimer);
        tvResendCode = findViewById(R.id.tvResendCode);
        TextView tvVerifyMsg = findViewById(R.id.tvVerifyMsg);
        TextView tvVerifyMsg2 = findViewById(R.id.tvVerifyMsg2);
        TextView tvVerify = findViewById(R.id.tvVerify);

        etOtp1.setTypeface(fontRegular);
        etOtp2.setTypeface(fontRegular);
        etOtp3.setTypeface(fontRegular);
        etOtp4.setTypeface(fontRegular);

        tvTimer.setTypeface(fontRegular);
        tvResendCode.setTypeface(fontMedium);
        tvVerifyMsg.setTypeface(fontBold);
        tvVerifyMsg2.setTypeface(fontBold);
        tvVerify.setTypeface(fontBold);

        tvVerify.setOnClickListener(this);

       /* etOtp1.setText("1");
        etOtp2.setText("1");
        etOtp3.setText("1");
        etOtp4.setText("1");
        etOtp4.requestFocus();
        verifYOTP();*/

        etOtp1.addTextChangedListener(this);
        etOtp2.addTextChangedListener(this);
        etOtp3.addTextChangedListener(this);
        etOtp4.addTextChangedListener(this);

        tvVerifyMsg2.setText("("+countryCode+"-"+ phone + ")" + getString(R.string.verify_msg2));

        etOtp1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                //Utility.showKeyBoard(OTPVerifyActivity.this,etOtp1);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    /**********************************************************************************************/
    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.tvVerify:
                progressDialog.setMessage(getString(R.string.verifyphoneNumber));
                verifYOTP();
                break;

            /**********************************************************************************************/
            /**
             * <h1>ResendOTP</h1>
             * <p>if the otp need to receive again this service will call, it is the same service when we call for OTP. </p>
             */
            /**********************************************************************************************/
            case R.id.tvResendCode:
                progressDialog.setMessage(getString(R.string.resendingCode));
                presenter.resendOtp(userId,otpOption);
                break;

        }
    }

    /**
     * <h1>OTPValidation</h1>
     * <p>this is the method is call from when the OTP verify is click,
     * the method first check whether the otp entered or not if the OTP is empty then will show the error Toast message,
     * else the service call. the service call used for forgot password , Signup time and change phone number time also.
     * </p>
     */
    private void verifYOTP()
    {
        String otp = etOtp1.getText().toString()+etOtp2.getText().toString()+etOtp3.getText().toString()+etOtp4.getText().toString();
        presenter.verifyOTP(otp,userId,otpOption);
    }

    private void startTimer(long j) {

        final long finalTime = j;
        CountDownTimer countDownTimer = new CountDownTimer(finalTime * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                int barVal = (int) seconds;
                if (seconds % 2 == 0) {
                    tvTimer.setTextColor(normalTextColor);
                } else {
                    tvTimer.setTextColor(lightestTextColor);
                }
                tvTimer.setText(getDurationString(seconds));
            }

            public void onFinish() {
                tvResendCode.setOnClickListener(OTPVerifyActivity.this);
                tvResendCode.setTextColor(ContextCompat.getColor(OTPVerifyActivity.this,R.color.darkTextColor));

                tvTimer.setText("");
            }

        }.start();

    }
    /**
     * method for returning hours and minutes from seconds
     *
     * @param seconds second
     * @return hours and minutes
     */
    String getDurationString(long seconds) {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day * 24));
        int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60));
        int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60));

        String timer = String.format("%02d", hours) + " : " + String.format("%02d", minute) + " : " + String.format("%02d", second);

        return timer;
    }
    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessResendCode(String msg) {
        tvResendCode.setOnClickListener(null);
        tvResendCode.setTextColor(ContextCompat.getColor(this,R.color.lightTextColor));
        startTimer(Long.parseLong(expireOtp));
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onSuccessSignupOtp(LoginData loginData) {
        sessionManager.setProviderId(loginData.getId());
        sessionManager.setEmail(loginData.getEmail());
        sessionManager.setFirstName(loginData.getFirstName());
        sessionManager.setLastName(loginData.getLastName());
        sessionManager.setProfilePic(loginData.getProfilePic());
        sessionManager.setPhoneNumber(loginData.getMobile());
        sessionManager.setCountryCode(loginData.getCountryCode());
        sessionManager.setReferalCode(loginData.getReferralCode());
        sessionManager.setIsDriverLogin(true);
        sessionManager.setDeviceId(Utility.getDeviceId(this));
        sessionManager.setFCMTopic(loginData.getFcmTopic());
        sessionManager.setZendeskRequesterId(loginData.getRequester_id());
        sessionManager.setIsBidBooking(loginData.isBid());
        sessionManager.setCallToken(loginData.getCall().getAuthToken());
        sessionManager.setCallWillTopic(loginData.getCall().getWillTopic());

        FirebaseMessaging.getInstance().subscribeToTopic(/*"/topics/" + */sessionManager.getFCMTopic());

        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.Login.value);

        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , loginData.getToken());

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
        startActivity(intent);

    }


    @Override
    public void onSuccessForgotPasswordOTP(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        finish();
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        intent.putExtra("phone",phone);
        intent.putExtra("countryCode",countryCode);
        intent.putExtra("userId",userId);
        intent.putExtra("isForgotPassword",true);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else
        {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
        }
    }

    @Override
    public void onSuccessChangeNumberdOTP(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    @Override
    public void onVerfyiCodeError() {
        // Toast.makeText(this,getString(R.string.enterVerifyicationCode),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVerfyiCodeCorrect() {
        Utility.hideKeyboad(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        try {
            View et;
            if(count > before)
            {
                et= getCurrentFocus().focusSearch(View.FOCUS_RIGHT);
            }
            else
            {
                et= getCurrentFocus().focusSearch(View.FOCUS_LEFT);
            }

            if(et instanceof EditText)
            {
                et.requestFocus();
            }

            if(et.getId() == R.id.etOtp4)
            {
                verifYOTP();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        View et = getCurrentFocus();
        if(et !=null && et.getId() == R.id.etOtp4)
        {
            verifYOTP();
        }
    }
}
