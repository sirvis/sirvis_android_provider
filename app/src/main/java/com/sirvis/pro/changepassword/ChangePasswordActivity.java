package com.sirvis.pro.changepassword;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.sirvis.pro.R;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ChangePasswordActivity</h1>
 * ChangePassword Activity is used to change the password of the user or reset the password of the user once he forgot
 */

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener, ChangePasswordPresenter.ChangePasswordPresenterImple {

    private TextInputLayout tilOldPassword,tilNewPassword,tilConfirmPassword;
    private EditText etOldPassword, etNewPassword,etConfirmPassword;
    private String phone="",countryCode="",userId="";
    private boolean isForgotPassword;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ChangePasswordPresenter presenter;

    /**********************************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_change_pass);

       init();
    }


    /**
     * Initialize the views
     */
    private void init() {
        presenter = new ChangePasswordPresenter(this);
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.resetPassword));
        tvTitle.setTypeface(fontBold);

        tilOldPassword = findViewById(R.id.tilOldPassword);
        tilNewPassword = findViewById(R.id.tilNewPassword);
        tilConfirmPassword = findViewById(R.id.tilConfirmPassword);
        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        TextView tvConfirm = findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(this);
        tvConfirm.setTypeface(fontBold);

        TextView tvMobileVerifiedMsg = findViewById(R.id.tvMobileVerifiedMsg);
        tvMobileVerifiedMsg.setTypeface(fontBold);

        etOldPassword.setTypeface(fontRegular);
        etNewPassword.setTypeface(fontRegular);
        etConfirmPassword.setTypeface(fontRegular);

        Intent intent = getIntent();
        isForgotPassword = intent.getBooleanExtra("isForgotPassword",false);

        if(isForgotPassword)
        {
            tvTitle.setText(getString(R.string.yourNewPassword).toLowerCase());
            phone = intent.getStringExtra("phone");
            countryCode = intent.getStringExtra("countryCode");
            userId = intent.getStringExtra("userId");
            tilOldPassword.setVisibility(View.GONE);
            tvMobileVerifiedMsg.setVisibility(View.VISIBLE);
            tilNewPassword.setHint(getString(R.string.yourNewPassword));
            tilConfirmPassword.setHint(getString(R.string.reenterpassword));
            progressDialog.setMessage(getString(R.string.changingPassword));
        }
        else
        {
            tvTitle.setText(getString(R.string.changePassword).toLowerCase());
            tilNewPassword.setHint(getString(R.string.newPassword));
            tilConfirmPassword.setHint(getString(R.string.confirmNewPassword));
            progressDialog.setMessage(getString(R.string.updatingPassword));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * close the acvitity with transition
     */
    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }
    /**********************************************************************************************/


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvConfirm:
                if(!isForgotPassword)
                {
                    presenter.changePassword(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),etOldPassword.getText().toString(), etNewPassword.getText().toString(),etConfirmPassword.getText().toString());
                }
                else
                {
                    presenter.forgotPassword(userId,etNewPassword.getText().toString(),etConfirmPassword.getText().toString());
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String msg) {
        if(!isForgotPassword)
        {
            sessionManager.setPassword(etNewPassword.getText().toString());
        }

        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.message),msg,getString(R.string.oK));
    }

    @Override
    public void onOldPasswordError() {
        resetTil(tilOldPassword,getString(R.string.enterPassword));
    }

    @Override
    public void onPasswordError() {
        resetTil(tilNewPassword,getString(R.string.enterValidPassword));
    }

    @Override
    public void onConfirmPasswordError() {
        resetTil(tilConfirmPassword,getString(R.string.enterConfirmPassword));
    }

    @Override
    public void onPasswordMismatch() {
        resetTil(tilConfirmPassword,getString(R.string.passwordMistach));
    }

    /**
     * <h1>resetTil</h1>
     * @param til TextInputLayout of error field
     * @param error Error string
     */
    private void resetTil(TextInputLayout til,String error)
    {
        tilOldPassword.setErrorEnabled(false);
        tilNewPassword.setErrorEnabled(false);
        tilConfirmPassword.setErrorEnabled(false);

        if(til !=null )
        {
            til.setErrorEnabled(true);
            til.setError(error);
        }
    }
}
