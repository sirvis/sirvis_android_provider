package com.sirvis.pro.changepassword;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ChangePasswordPresenter</h1>
 * Presenter for Change Password Activity
 */

public class ChangePasswordPresenter implements ChangePasswordModel.ChangePasswordModelImple {
    private ChangePasswordModel model;
    private ChangePasswordPresenterImple presenterImple;

    ChangePasswordPresenter(ChangePasswordPresenterImple presenterImple) {
        model = new ChangePasswordModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * <h1>changePassword</h1>
     * Method for passing values from activity to model to changePassword
     * @param sessiontoken session token
     * @param oldPassword oldPassword
     * @param password newPassword
     * @param conformPassword conformPassword
     */
    void changePassword(String sessiontoken,String oldPassword,String password,String conformPassword) {
        presenterImple.startProgressBar();
        model.changePassword(sessiontoken,oldPassword,password,conformPassword);
    }

    /**
     * <h1>changePassword</h1>
     * Method for passing values from activity to model to reset password
     * @param userId user token
     * @param password newPassword
     * @param conformPassword conformPassword
     */
    void forgotPassword(String userId,String password,String conformPassword)
    {
        presenterImple.startProgressBar();
        model.forgotPassword(userId,password,conformPassword);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg);
    }

    @Override
    public void onOldPasswordError() {
        presenterImple.stopProgressBar();
        presenterImple.onOldPasswordError();
    }

    @Override
    public void onPasswordError() {
        presenterImple.stopProgressBar();
        presenterImple.onPasswordError();

    }

    @Override
    public void onConfirmPasswordError() {
        presenterImple.stopProgressBar();
        presenterImple.onConfirmPasswordError();
    }

    @Override
    public void onPasswordMismatch() {
        presenterImple.stopProgressBar();
        presenterImple.onPasswordMismatch();
    }

    /**
     * <h1>ChangePasswordPresenterImple</h1>
     * interface for view implementation
     * @see ChangePasswordActivity
     */
    interface ChangePasswordPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(String msg);
        void onOldPasswordError();
        void onPasswordError();
        void onConfirmPasswordError();
        void onPasswordMismatch();
    }
}
