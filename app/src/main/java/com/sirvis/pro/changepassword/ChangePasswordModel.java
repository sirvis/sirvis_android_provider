package com.sirvis.pro.changepassword;

import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ChangePasswordModel</h1>
 * Model for changePasswordActivity  for calling api for change password and reset password
 * @see ChangePasswordActivity
 */

public class ChangePasswordModel {

    private static final String TAG = "LoginModel";
    private ChangePasswordModelImple modelImplement;

    ChangePasswordModel(ChangePasswordModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * Method for calinng api to change password
     * and local validation
     * @param sessiontoken session token
     * @param oldPassword oldPassword
     * @param password newPassword
     * @param conformPassword conformPassword
     */
    void changePassword(String sessiontoken,String oldPassword,String password,String conformPassword)
    {
        if(oldPassword.equals(""))
        {
            modelImplement.onOldPasswordError();
            return;
        }
        if(!isValidPassword(password))
        {
            modelImplement.onPasswordError();
            return;
        }
        else if(conformPassword.equals(""))
        {
            modelImplement.onConfirmPasswordError();
            return;
        }
        else if (!password.equals(conformPassword))
        {
            modelImplement.onPasswordMismatch();
            return;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("oldPassword", oldPassword);
            jsonObject.put("newPassword", password);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE_CHANGE_PASSWORD , OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    private boolean isValidPassword(String pass)
    {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);

        if(pass.length() < 7)
        {
            return false;
        }
        else if(pass.equals(pass.toLowerCase()))
        {
            return false;
        }
        else if(pass.equals(pass.toUpperCase()))
        {
            return false;
        }
        else if(!matcher.matches())
        {
            return false;
        }
        return true;
    }

    /**
     * Method for reset the password
     * @param userId userID
     * @param password new password
     * @param conformPassword conform password
     */
    void forgotPassword(String userId,String password,String conformPassword)
    {
        if(!isValidPassword(password))
        {
            modelImplement.onPasswordError();
            return;
        }
        else if(conformPassword.equals(""))
        {
            modelImplement.onConfirmPasswordError();
            return;
        }
        else if (!password.equals(conformPassword))
        {
            modelImplement.onPasswordMismatch();
            return;
        }

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", userId);
            jsonObject.put("userType", VariableConstant.USER_TYPE);
            jsonObject.put("password",password);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.FORGOT_RESET_PASSWORD , OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * <h1>ChangePasswordModelImple</h1>
     * interface for presenter Implementation
     * @see ChangePasswordPresenter
     */
    interface ChangePasswordModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(String msg);
        void onOldPasswordError();
        void onPasswordError();
        void onConfirmPasswordError();
        void onPasswordMismatch();
    }

}
