package com.sirvis.pro.changepassword;

import com.sirvis.pro.pojo.signup.LoginData;

/**
 * Created by murashid on 12-Sep-17.
 * <h1>OTPVerifyPresenter</h1>
 * OTPVerifyPresenter presenter for OtpVerifyiActivity
 * @see OTPVerifyActivity
 */

public class OTPVerifyPresenter implements OTPVerifyModel.OTPVerifyModelImple {

    private OTPVerifyModel model;
    private OTPVerifyPresenterImple presenterImple;


    OTPVerifyPresenter(OTPVerifyPresenterImple presenterImple) {
        model = new OTPVerifyModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for sending values from activity to model
     * @param userId userId
     * @param otpOption otpOption
     */
    void resendOtp(String userId,int otpOption) {
        presenterImple.startProgressBar();
        model.resendOtp(userId,otpOption);
    }

    /**
     * method for sending values from activity to model
     * @param userid userId
     * @param otpOption otpOption
     * @param otp otp
     */

    void verifyOTP(String otp,String userid,int otpOption)
    {
        presenterImple.startProgressBar();
        model.verifyOTP(otp,userid,otpOption);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccessResendCode(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessResendCode(msg);
    }

    @Override
    public void onSuccessSignupOtp(LoginData loginData) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessSignupOtp(loginData);
    }

    @Override
    public void onSuccessForgotPasswordOTP(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessForgotPasswordOTP(msg);
    }

    @Override
    public void onSuccessChangeNumberdOTP(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessChangeNumberdOTP(msg);
    }


    @Override
    public void onVerfyiCodeError() {
        presenterImple.stopProgressBar();
        presenterImple.onVerfyiCodeError();
    }

    @Override
    public void onVerfyiCodeCorrect() {
        presenterImple.onVerfyiCodeCorrect();
    }

    /**
     * <h1>OTPVerifyPresenterImple</h1>
     * interface for view implementation
     * @see OTPVerifyActivity
     */
    interface OTPVerifyPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccessResendCode(String msg);
        void onSuccessSignupOtp(LoginData loginData);
        void onSuccessForgotPasswordOTP(String msg);
        void onSuccessChangeNumberdOTP(String msg);
        void onVerfyiCodeError();
        void onVerfyiCodeCorrect();
    }
}
