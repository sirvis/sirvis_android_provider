package com.sirvis.pro.changepassword;

import android.util.Log;
import android.util.Patterns;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 12-Sep-17.
 * <h1>HelpWithPassworModel</h1>
 * Model for Help with password activity
 */

public class HelpWithPassworModel {

    private static final String TAG = "LoginModel";
    private HelpWithPassworModelImple modelImplement;

    HelpWithPassworModel(HelpWithPassworModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * <h1>forgotPassword</h1>
     * method for requesting api to change password
     * @param countryCode country code
     * @param emailPhone email or phone
     * @param type email or phone
     */
    void forgotPassword(String countryCode, String emailPhone, final String type)
    {
        if(type.equals("1"))
        {
            if(emailPhone.equals(""))
            {
                modelImplement.onPhoneNumberError();
                return;
            }
        }
        else
        {
            if(emailPhone.equals(""))
            {
                modelImplement.onEmailError();
                return;
            }
            else if(!Patterns.EMAIL_ADDRESS.matcher(emailPhone).matches())
            {
                modelImplement.onEmailError();
                return;
            }
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emailOrPhone",emailPhone);
            if(type.equals("1"))
            {
                jsonObject.put("countryCode", countryCode);
            }
            jsonObject.put("userType", VariableConstant.USER_TYPE);
            jsonObject.put("type",type);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.FORGOT_PASSWORD , OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);

                        Gson gson = new Gson();
                        PhoneValidationPojo phoneValidationPojo = gson.fromJson(result,PhoneValidationPojo.class);

                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            if(type.equals("1"))
                            {
                                modelImplement.onSuccessPhone(phoneValidationPojo);
                            }
                            else
                            {
                                modelImplement.onSuccessEmail(phoneValidationPojo.getMessage());
                            }

                        } else {
                            modelImplement.onFailure(phoneValidationPojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * <h1>HelpWithPassworModelImple</h1>
     * Interface for presenter implementation
     * @see HelpWithPassworPresenter
     */
    interface HelpWithPassworModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccessPhone(PhoneValidationPojo phoneValidationPojo);
        void onPhoneNumberError();
        void onSuccessEmail(String msg);
        void onEmailError();

    }

}
