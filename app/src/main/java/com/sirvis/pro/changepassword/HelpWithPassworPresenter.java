package com.sirvis.pro.changepassword;

import com.sirvis.pro.pojo.phonevalidation.PhoneValidationPojo;

/**
 * Created by murashid on 12-Sep-17.
 * <h1>HelpWithPassworPresenter</h1>
 * Presenter for HelpWithPassword Activity
 * @see HelpWithPasswordActivity
 */

public class HelpWithPassworPresenter implements HelpWithPassworModel.HelpWithPassworModelImple {
    private HelpWithPassworModel model;
    private HelpWithPassworPresenterImple presenterImple;

    HelpWithPassworPresenter(HelpWithPassworPresenterImple presenterImple) {
        model = new HelpWithPassworModel(this);
        this.presenterImple = presenterImple;
    }


    /**
     * method for passing values from activity to model
     * @param countryCode countrycode
     * @param emailPhone email or phone
     * @param type email or phone
     */
    void forgotPassword(String countryCode, String emailPhone, String type)
    {
        presenterImple.startProgressBar();
        model.forgotPassword(countryCode,emailPhone,type);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccessPhone(PhoneValidationPojo phoneValidationPojo) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessPhone(phoneValidationPojo);
    }

    @Override
    public void onPhoneNumberError() {
        presenterImple.stopProgressBar();
        presenterImple.onPhoneNumberError();
    }

    @Override
    public void onSuccessEmail(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessEmail(msg);
    }

    @Override
    public void onEmailError() {
        presenterImple.stopProgressBar();
        presenterImple.onEmailError();
    }

    /**
     * <h1>HelpWithPassworPresenterImple</h1>
     * interface for View Implementation
     * @see HelpWithPasswordActivity
     */
    interface HelpWithPassworPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccessPhone(PhoneValidationPojo phoneValidationPojo);
        void onPhoneNumberError();
        void onSuccessEmail(String msg);
        void onEmailError();

    }
}
