package com.sirvis.pro.pojo.appconfig;

/**
 * Created by murashid on 25-Oct-17.
 */

public class ProviderFrequency {
    private String locationPublishInterval;
    private String liveTrackInterval;
    private String proTimeOut;

    public String getLocationPublishInterval() {
        return locationPublishInterval;
    }

    public void setLocationPublishInterval(String locationPublishInterval) {
        this.locationPublishInterval = locationPublishInterval;
    }

    public String getLiveTrackInterval() {
        return liveTrackInterval;
    }

    public void setLiveTrackInterval(String liveTrackInterval) {
        this.liveTrackInterval = liveTrackInterval;
    }

    public String getProTimeOut() {
        return proTimeOut;
    }

    public void setProTimeOut(String proTimeOut) {
        this.proTimeOut = proTimeOut;
    }

}
