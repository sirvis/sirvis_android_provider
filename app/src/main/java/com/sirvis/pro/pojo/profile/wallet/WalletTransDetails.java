package com.sirvis.pro.pojo.profile.wallet;

import java.io.Serializable;

/**
 * @since  19/09/17.
 */

public class WalletTransDetails
        implements Serializable
{

    private String txnId, trigger, txnType, comment, currency;
    private String openingBal, amount, closingBal,timestamp, tripId;
    private String paymentType, paymentTxnId, intiatedBy;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(String openingBal) {
        this.openingBal = openingBal;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getClosingBal() {
        return closingBal;
    }

    public void setClosingBal(String closingBal) {
        this.closingBal = closingBal;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentTxnId() {
        return paymentTxnId;
    }

    public void setPaymentTxnId(String paymentTxnId) {
        this.paymentTxnId = paymentTxnId;
    }

    public String getIntiatedBy() {
        return intiatedBy;
    }

    public void setIntiatedBy(String intiatedBy) {
        this.intiatedBy = intiatedBy;
    }

    @Override
    public String toString() {
        return "WalletTransDetails{" +
                "txnId='" + txnId + '\'' +
                ", trigger='" + trigger + '\'' +
                ", txnType='" + txnType + '\'' +
                ", comment='" + comment + '\'' +
                ", currency='" + currency + '\'' +
                ", openingBal='" + openingBal + '\'' +
                ", amount='" + amount + '\'' +
                ", closingBal='" + closingBal + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", tripId='" + tripId + '\'' +
                ", paymentType='" + paymentType + '\'' +
                ", paymentTxnId='" + paymentTxnId + '\'' +
                ", intiatedBy='" + intiatedBy + '\'' +
                '}';
    }
}
