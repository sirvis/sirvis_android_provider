package com.sirvis.pro.pojo.profile.wallet;

/**
 * Created by PrashantSingh on 27/09/17.
 */

public class WalletPojo
{

    private String message;
    private WalletData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WalletData getData() {
        return data;
    }

    public void setData(WalletData data) {
        this.data = data;
    }
}
