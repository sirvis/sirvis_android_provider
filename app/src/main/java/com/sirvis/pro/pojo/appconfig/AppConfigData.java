package com.sirvis.pro.pojo.appconfig;

/**
 * Created by murashid on 25-Oct-17.
 */

public class AppConfigData {
    private String currencySymbol ;
    private String currency ;
    private String currencyAbbr ;//1 = > prefix 2 = > suffix
    private String distanceMatrix ; // 0 => kilometer 1  = > Miles
    private String latLongDisplacement="10" ;
    private String stripeKeys ;
    private String appVersion ;
    private String mandatory ;

    private ProviderFrequency providerFrequency;
    private PushTopics pushTopics;
    private WalletDataAppConfic walletData;

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyAbbr() {
        return currencyAbbr;
    }

    public void setCurrencyAbbr(String currencyAbbr) {
        this.currencyAbbr = currencyAbbr;
    }

    public String getDistanceMatrix() {
        return distanceMatrix;
    }

    public void setDistanceMatrix(String distanceMatrix) {
        this.distanceMatrix = distanceMatrix;
    }

    public String getLatLongDisplacement() {
        return latLongDisplacement;
    }

    public void setLatLongDisplacement(String latLongDisplacement) {
        this.latLongDisplacement = latLongDisplacement;
    }

    public String getStripeKeys() {
        return stripeKeys;
    }

    public void setStripeKeys(String stripeKeys) {
        this.stripeKeys = stripeKeys;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public ProviderFrequency getProviderFrequency() {
        return providerFrequency;
    }

    public void setProviderFrequency(ProviderFrequency providerFrequency) {
        this.providerFrequency = providerFrequency;
    }

    public PushTopics getPushTopics() {
        return pushTopics;
    }

    public void setPushTopics(PushTopics pushTopics) {
        this.pushTopics = pushTopics;
    }

    public WalletDataAppConfic getWalletData() {
        return walletData;
    }

    public void setWalletData(WalletDataAppConfic walletData) {
        this.walletData = walletData;
    }
}
