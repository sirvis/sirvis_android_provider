package com.sirvis.pro.pojo.chat;

import java.util.ArrayList;

/**
 * Created by murashid on 05-Apr-18.
 */

public class ChatApiResponse {

    private String message ;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private ArrayList<ChatData> data;

    public ArrayList<ChatData> getData() {
        return data;
    }

    public void setData(ArrayList<ChatData> data) {
        this.data = data;
    }

}
