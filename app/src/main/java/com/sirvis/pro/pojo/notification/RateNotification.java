package com.sirvis.pro.pojo.notification;

public class RateNotification {
    private String errNum;

    @Override
    public String toString() {
        return "{" +
                "errNum='" + errNum + '\'' +
                ", data=" + data +
                '}';
    }

    public String getErrNum() {
        return errNum;
    }

    public Data getData() {
        return data;
    }

    private Data data;

 public    class Data {
        private String bookingId;

        @Override
        public String toString() {
            return "{" +
                    "bookingId='" + bookingId + '\'' +
                    '}';
        }

        public String getBookingId() {
            return bookingId;
        }
    }

}
