package com.sirvis.pro.pojo.chat;

/**
 * Created by murashid on 10-Apr-18.
 */

public class ChatCustomerListPojo {
    private String message;
    private ChatCustomerListData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ChatCustomerListData getData() {
        return data;
    }

    public void setData(ChatCustomerListData data) {
        this.data = data;
    }
}
