package com.sirvis.pro.pojo.profile.metaData;

import java.io.Serializable;

/**
 * Created by murashid on 02-Apr-18.
 */

public class PreDefined implements Serializable {
    private String _id;
    private String name;
    private String icon;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
