package com.sirvis.pro.pojo.phonevalidation;

/**
 * Created by murashid on 06-Oct-17.
 */

public class PhoneValidationData {
    private String sid;

    public String getExpireOtp() {
        return expireOtp;
    }

    @Override
    public String toString() {
        return "{" +
                "sid='" + sid + '\'' +
                ", expireOtp='" + expireOtp + '\'' +
                '}';
    }

    private String expireOtp;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
