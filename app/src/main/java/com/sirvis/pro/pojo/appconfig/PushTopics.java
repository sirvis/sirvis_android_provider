package com.sirvis.pro.pojo.appconfig;

/**
 * Created by murashid on 21-Dec-17.
 */

public class PushTopics {
    private String city;
    private String allProvider;
    private String allCities;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAllProvider() {
        return allProvider;
    }

    public void setAllProvider(String allProvider) {
        this.allProvider = allProvider;
    }

    public String getAllCities() {
        return allCities;
    }

    public void setAllCities(String allCities) {
        this.allCities = allCities;
    }

}
