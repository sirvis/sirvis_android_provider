package com.sirvis.pro.pojo.profile.helpcenter;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Dec-17.
 */

public class HelpCenterData {
    private ArrayList<Ticket> open;
    private ArrayList<Ticket> close;

    public ArrayList<Ticket> getOpen() {
        return open;
    }

    public void setOpen(ArrayList<Ticket> open) {
        this.open = open;
    }

    public ArrayList<Ticket> getClose() {
        return close;
    }

    public void setClose(ArrayList<Ticket> close) {
        this.close = close;
    }
}
