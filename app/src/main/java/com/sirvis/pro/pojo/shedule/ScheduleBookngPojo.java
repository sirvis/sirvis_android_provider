package com.sirvis.pro.pojo.shedule;

import com.sirvis.pro.pojo.booking.Booking;

import java.io.Serializable;

/**
 * Created by murashid on 11-Jan-18.
 */

public class ScheduleBookngPojo implements Serializable{
    private String message;
    private Booking data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Booking getData() {
        return data;
    }

    public void setData(Booking data) {
        this.data = data;
    }
}
