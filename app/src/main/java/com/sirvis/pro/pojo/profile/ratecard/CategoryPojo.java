package com.sirvis.pro.pojo.profile.ratecard;

import java.util.ArrayList;

/**
 * Created by murashid on 13-Feb-18.
 */

public class CategoryPojo {
    private String message;
    private ArrayList<CategoryData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CategoryData> getData() {
        return data;
    }

    public void setData(ArrayList<CategoryData> data) {
        this.data = data;
    }
}
