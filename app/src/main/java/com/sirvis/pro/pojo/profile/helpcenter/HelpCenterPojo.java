package com.sirvis.pro.pojo.profile.helpcenter;

/**
 * Created by murashid on 29-Dec-17.
 */

public class HelpCenterPojo {
   private HelpCenterData data;

    public HelpCenterData getData() {
        return data;
    }

    public void setData(HelpCenterData data) {
        this.data = data;
    }
}
