package com.sirvis.pro.pojo.booking;

import java.io.Serializable;

/**
 * Created by murashid on 21-Jun-18.
 */

public class QuestionAnswer implements Serializable {
    private String answer;
    private String name;
    private String _id;
    private String questionType= "5"; // check only photo and price

    /*0. PromoCode
    1. Price
    2. StartDate
    3. Address
    4. PaymentMethod
    5. TextView
    6. RadioButton - One Answer
    7. checkBox - ManyAns seprated by ,
    8. start to end Date
    9. end to start Date
    10. Photo */

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }
}
