package com.sirvis.pro.pojo.shedule;

/**
 * Created by murashid on 05-Jul-17.
 */

public class ScheduleViewPojo
{
    private String message;

    private ScheduleData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ScheduleData getData() {
        return data;
    }

    public void setData(ScheduleData data) {
        this.data = data;
    }
}
