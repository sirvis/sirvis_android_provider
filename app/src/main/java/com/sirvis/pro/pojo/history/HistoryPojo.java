package com.sirvis.pro.pojo.history;

import com.sirvis.pro.pojo.booking.Booking;

import java.util.ArrayList;

/**
 * Created by murashid on 13-Nov-17.
 */

public class HistoryPojo {
    private String message;
    private ArrayList<Booking> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Booking> getData() {
        return data;
    }

    public void setData(ArrayList<Booking> data) {
        this.data = data;
    }
}
