package com.sirvis.pro.pojo.booking;

import java.io.Serializable;
import java.util.ArrayList;

public class AcceptedBookingPojo implements Serializable {

    private String message;
    private ArrayList<Booking> data;

    @Override
    public String toString() {
        return "{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Booking> getData() {
        return data;
    }

    public void setData(ArrayList<Booking> data) {
        this.data = data;
    }
}
