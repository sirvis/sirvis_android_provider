package com.sirvis.pro.pojo.signup;

/**
 * Created by murashid on 07-Oct-17.
 */

public class SignUpData {

    private String providerId;
    private String expireOtp;

    @Override
    public String toString() {
        return "{" +
                "providerId='" + providerId + '\'' +
                ", expireOtp='" + expireOtp + '\'' +
                '}';
    }

    public String getExpireOtp() {
        return expireOtp;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }
}
