package com.sirvis.pro.pojo.booking;

import java.io.Serializable;

/**
 * Created by murashid on 07-Nov-17.
 */

public class BookingTimer implements Serializable {
    private String status;
    private String second;
    private String startTimeStamp;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }

    public String getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(String startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }
}
