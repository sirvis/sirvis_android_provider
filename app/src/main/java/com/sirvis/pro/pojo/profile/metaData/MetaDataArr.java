package com.sirvis.pro.pojo.profile.metaData;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 16-Feb-18.
 */

public class MetaDataArr implements Serializable{
    private String _id;
    private String fieldName;
    private String fieldType;
    private String isManadatory;
    private String Description;
    private String data;
    private ArrayList<PreDefined> preDefined;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getIsManadatory() {
        return isManadatory;
    }

    public void setIsManadatory(String isManadatory) {
        this.isManadatory = isManadatory;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ArrayList<PreDefined> getPreDefined() {
        return preDefined;
    }

    public void setPreDefined(ArrayList<PreDefined> preDefined) {
        this.preDefined = preDefined;
    }
}
