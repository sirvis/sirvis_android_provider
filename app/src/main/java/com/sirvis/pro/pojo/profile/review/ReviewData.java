package com.sirvis.pro.pojo.profile.review;

import java.util.ArrayList;

/**
 * Created by murashid on 14-Sep-17.
 */

public class ReviewData {
    private ArrayList<Reviews> reviews;
    private String averageRating="";
    private String reviewCount="";
    private String about="";

    public ArrayList<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Reviews> reviews) {
        this.reviews = reviews;
    }

    public String getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(String averageRating) {
        this.averageRating = averageRating;
    }

    public String getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
