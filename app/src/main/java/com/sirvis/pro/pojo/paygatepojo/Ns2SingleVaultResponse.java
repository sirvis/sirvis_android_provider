package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ns2SingleVaultResponse {

    @SerializedName("ns2:CardVaultResponse")
    @Expose
    private Ns2CardVaultResponse ns2CardVaultResponse;
    @SerializedName("xmlns:ns2")
    @Expose
    private String xmlnsNs2;

    public Ns2CardVaultResponse getNs2CardVaultResponse() {
        return ns2CardVaultResponse;
    }

    public void setNs2CardVaultResponse(Ns2CardVaultResponse ns2CardVaultResponse) {
        this.ns2CardVaultResponse = ns2CardVaultResponse;
    }

    public String getXmlnsNs2() {
        return xmlnsNs2;
    }

    public void setXmlnsNs2(String xmlnsNs2) {
        this.xmlnsNs2 = xmlnsNs2;
    }

}

