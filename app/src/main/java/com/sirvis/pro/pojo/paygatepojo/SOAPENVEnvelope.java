package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVEnvelope {

    @SerializedName("SOAP-ENV:Body")
    @Expose
    private SOAPENVBody sOAPENVBody;
    @SerializedName("xmlns:SOAP-ENV")
    @Expose
    private String xmlnsSOAPENV;
    @SerializedName("SOAP-ENV:Header")
    @Expose
    private String sOAPENVHeader;

    public SOAPENVBody getSOAPENVBody() {
        return sOAPENVBody;
    }

    public void setSOAPENVBody(SOAPENVBody sOAPENVBody) {
        this.sOAPENVBody = sOAPENVBody;
    }

    public String getXmlnsSOAPENV() {
        return xmlnsSOAPENV;
    }

    public void setXmlnsSOAPENV(String xmlnsSOAPENV) {
        this.xmlnsSOAPENV = xmlnsSOAPENV;
    }

    public String getSOAPENVHeader() {
        return sOAPENVHeader;
    }

    public void setSOAPENVHeader(String sOAPENVHeader) {
        this.sOAPENVHeader = sOAPENVHeader;
    }

}
