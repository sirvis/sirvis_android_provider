package com.sirvis.pro.pojo.signup;


/**
 * Created by murashid on 08-Sep-17.
 */

public class LoginPojo {
    private String message;
    private LoginData data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
}
