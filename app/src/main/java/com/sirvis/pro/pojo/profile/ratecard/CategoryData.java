package com.sirvis.pro.pojo.profile.ratecard;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 13-Feb-18.
 */

public class CategoryData implements Serializable{

    private String _id;
    private String city_id;
    private String service_type;
    private String billing_model;
    private String cat_name;
    private String cat_desc;
    private String minimum_fees="0";
    private String miximum_fees="0";
    private String price_per_fees="0";

    private ArrayList<CategoryService> service;
    private ArrayList<SubCategoryRateCard> subCatArr;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getBilling_model() {
        return billing_model;
    }

    public void setBilling_model(String billing_model) {
        this.billing_model = billing_model;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_desc() {
        return cat_desc;
    }

    public void setCat_desc(String cat_desc) {
        this.cat_desc = cat_desc;
    }

    public String getMinimum_fees() {
        return minimum_fees;
    }

    public void setMinimum_fees(String minimum_fees) {
        this.minimum_fees = minimum_fees;
    }

    public String getMiximum_fees() {
        return miximum_fees;
    }

    public void setMiximum_fees(String miximum_fees) {
        this.miximum_fees = miximum_fees;
    }

    public String getPrice_per_fees() {
        return price_per_fees;
    }

    public void setPrice_per_fees(String price_per_fees) {
        this.price_per_fees = price_per_fees;
    }

    public ArrayList<CategoryService> getService() {
        return service;
    }

    public void setService(ArrayList<CategoryService> service) {
        this.service = service;
    }

    public ArrayList<SubCategoryRateCard> getSubCatArr() {
        return subCatArr;
    }

    public void setSubCatArr(ArrayList<SubCategoryRateCard> subCatArr) {
        this.subCatArr = subCatArr;
    }
}
