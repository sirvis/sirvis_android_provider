package com.sirvis.pro.pojo.profile.service;

/**
 * Created by murashid on 04-Oct-17.
 */

public class MyListServiceEdit {
    private String id;
    private String price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
