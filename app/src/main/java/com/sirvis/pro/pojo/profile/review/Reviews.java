package com.sirvis.pro.pojo.profile.review;

/**
 * Created by murashid on 14-Sep-17.
 */

public class Reviews {
    private String review;
    private String rating;
    private String reviewBy;
    private String profilePic;
    private String reviewAt;

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getReviewAt() {
        return reviewAt;
    }

    public void setReviewAt(String reviewAt) {
        this.reviewAt = reviewAt;
    }
}
