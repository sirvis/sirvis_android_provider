package com.sirvis.pro.pojo.profile.calltype;

import com.sirvis.pro.pojo.profile.document.ProfileDocumentCategory;
import com.sirvis.pro.pojo.profile.events.MyListEvents;
import com.sirvis.pro.pojo.profile.metaData.MetaDataArr;
import com.sirvis.pro.pojo.profile.service.MyListService;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 10-Sep-17.
 */

public class CallTypeData implements Serializable{
    private String categoryId;
    private String categoryName;
    private String inCall;
    private String outCall;
    private String teleCall;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getInCall() {
        return inCall;
    }

    public void setInCall(String inCall) {
        this.inCall = inCall;
    }

    public String getOutCall() {
        return outCall;
    }

    public void setOutCall(String outCall) {
        this.outCall = outCall;
    }

    public String getTeleCall() {
        return teleCall;
    }

    public void setTeleCall(String teleCall) {
        this.teleCall = teleCall;
    }
}
