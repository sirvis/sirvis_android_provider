package com.sirvis.pro.pojo.history;

import java.io.Serializable;

/**
 * Created by murashid on 27-Dec-17.
 */

public class EventType implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
