package com.sirvis.pro.pojo.profile.review;

/**
 * Created by murashid on 14-Sep-17.
 */

public class ReviewPojo {

    private String message;

    private ReviewData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReviewData getData() {
        return data;
    }

    public void setData(ReviewData data) {
        this.data = data;
    }
}
