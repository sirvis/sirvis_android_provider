package com.sirvis.pro.pojo.booking;

import java.io.Serializable;

public class ImgUrls implements Serializable {
    private int isImsge=1;//1=>true,0=>false
    private String imageURl;

    public int getIsImsge() {
        return isImsge;
    }

    public ImgUrls(int isImsge, String imageURl) {
        this.isImsge = isImsge;
        this.imageURl = imageURl;
    }

    public String getImageURl() {
        return imageURl;
    }
}
