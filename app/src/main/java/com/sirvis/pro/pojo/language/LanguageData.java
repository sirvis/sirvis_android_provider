package com.sirvis.pro.pojo.language;

/**
 * Created by murashid on 24-Apr-18.
 */

public class LanguageData {
    private String lan_name;
    private String code;

    public String getLan_name() {
        return lan_name;
    }

    public void setLan_name(String lan_name) {
        this.lan_name = lan_name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
