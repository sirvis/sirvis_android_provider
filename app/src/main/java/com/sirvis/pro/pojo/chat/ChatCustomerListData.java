package com.sirvis.pro.pojo.chat;

import java.util.ArrayList;

/**
 * Created by murashid on 10-Apr-18.
 */

public class ChatCustomerListData {
    private ArrayList<ChatCutomerList> past;
    private ArrayList<ChatCutomerList> accepted;

    public ArrayList<ChatCutomerList> getPast() {
        return past;
    }

    public void setPast(ArrayList<ChatCutomerList> past) {
        this.past = past;
    }

    public ArrayList<ChatCutomerList> getAccepted() {
        return accepted;
    }

    public void setAccepted(ArrayList<ChatCutomerList> accepted) {
        this.accepted = accepted;
    }
}
