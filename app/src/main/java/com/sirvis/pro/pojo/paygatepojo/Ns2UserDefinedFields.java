package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ns2UserDefinedFields {

    @SerializedName("ns2:key")
    @Expose
    private String ns2Key;
    @SerializedName("ns2:value")
    @Expose
    private String ns2Value;

    public String getNs2Key() {
        return ns2Key;
    }

    public void setNs2Key(String ns2Key) {
        this.ns2Key = ns2Key;
    }

    public String getNs2Value() {
        return ns2Value;
    }

    public void setNs2Value(String ns2Value) {
        this.ns2Value = ns2Value;
    }

}
