package com.sirvis.pro.pojo.profile.events;

import java.io.Serializable;

public class MyListEvents implements Serializable {
    private String _id;
    private String name;
    private String status;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
