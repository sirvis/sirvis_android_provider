package com.sirvis.pro.pojo.booking;

import java.io.Serializable;

/**
 * Created by murashid on 07-Nov-17.
 */

public class GigTime implements Serializable {
    private String id;
    private String name;
    private String unit;
    private String price;
    private String second;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second) {
        this.second = second;
    }
}
