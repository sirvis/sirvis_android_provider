package com.sirvis.pro.pojo.profile.ratecard;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 03-Apr-18.
 */

public class SubCategoryRateCard implements Serializable{
    private String subCategoryId;
    private String sub_cat_name;
    private String sub_cat_desc;

    private ArrayList<CategoryService> service;

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getSub_cat_desc() {
        return sub_cat_desc;
    }

    public void setSub_cat_desc(String sub_cat_desc) {
        this.sub_cat_desc = sub_cat_desc;
    }

    public ArrayList<CategoryService> getService() {
        return service;
    }

    public void setService(ArrayList<CategoryService> service) {
        this.service = service;
    }
}
