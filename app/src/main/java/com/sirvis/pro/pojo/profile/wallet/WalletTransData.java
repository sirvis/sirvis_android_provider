package com.sirvis.pro.pojo.profile.wallet;

import java.util.ArrayList;

/**
 * @since 19/09/17.
 */

public class WalletTransData
{
   /* "data":{
    "debitArr":[],
    "creditArr":[],
    "creditDebitArr":[]}*/

   private ArrayList<WalletTransDetails> debitArr;
   private ArrayList<WalletTransDetails> creditArr;

   private ArrayList<WalletTransDetails> paymentArr;
   private ArrayList<WalletTransDetails> creditDebitArr;


    public ArrayList<WalletTransDetails> getDebitArr() {
        return debitArr;
    }

    public void setDebitArr(ArrayList<WalletTransDetails> debitArr) {
        this.debitArr = debitArr;
    }

    public ArrayList<WalletTransDetails> getCreditArr() {
        return creditArr;
    }

    public void setCreditArr(ArrayList<WalletTransDetails> creditArr) {
        this.creditArr = creditArr;
    }

    public ArrayList<WalletTransDetails> getCreditDebitArr() {
        return creditDebitArr;
    }

    public ArrayList<WalletTransDetails> getPaymentArr() {
        return paymentArr;
    }

    public void setCreditDebitArr(ArrayList<WalletTransDetails> creditDebitArr) {
        this.creditDebitArr = creditDebitArr;
    }


}
