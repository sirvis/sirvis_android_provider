package com.sirvis.pro.pojo.history;

import java.io.Serializable;

/**
 * Created by murashid on 27-Dec-17.
 */

public class ReviewByProvider implements Serializable {
    private String rating;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
