package com.sirvis.pro.pojo.signup;

import java.io.Serializable;

/**
 * Created by murashid on 31-Mar-18.
 */

public class SubCategory implements Serializable{
    private String _id;
    private String city_id;
    private String cat_id;
    private String sub_cat_name;
    private String sub_cat_desc;
    private String num;
    private boolean alreadySelected = false;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getSub_cat_desc() {
        return sub_cat_desc;
    }

    public void setSub_cat_desc(String sub_cat_desc) {
        this.sub_cat_desc = sub_cat_desc;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public boolean isAlreadySelected() {
        return alreadySelected;
    }

    public void setAlreadySelected(boolean alreadySelected) {
        this.alreadySelected = alreadySelected;
    }
}
