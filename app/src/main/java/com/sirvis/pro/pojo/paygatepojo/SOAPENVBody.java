package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SOAPENVBody {

    @SerializedName("ns2:SingleVaultResponse")
    @Expose
    private Ns2SingleVaultResponse ns2SingleVaultResponse;

    public Ns2SingleVaultResponse getNs2SingleVaultResponse() {
        return ns2SingleVaultResponse;
    }

    public void setNs2SingleVaultResponse(Ns2SingleVaultResponse ns2SingleVaultResponse) {
        this.ns2SingleVaultResponse = ns2SingleVaultResponse;
    }


}
