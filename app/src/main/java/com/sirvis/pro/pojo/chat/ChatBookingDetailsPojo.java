package com.sirvis.pro.pojo.chat;

import com.sirvis.pro.pojo.booking.Booking;

/**
 * Created by murashid on 31-May-18.
 */

public class ChatBookingDetailsPojo {
    private String message;
    private Booking data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Booking getData() {
        return data;
    }

    public void setData(Booking data) {
        this.data = data;
    }
}
