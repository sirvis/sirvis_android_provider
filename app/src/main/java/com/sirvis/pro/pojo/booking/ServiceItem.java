package com.sirvis.pro.pojo.booking;

import java.io.Serializable;

/**
 * Created by murashid on 19-Feb-18.
 */

public class ServiceItem implements Serializable {
    private String serviceId;
    private String serviceName;
    private String unit;
    private String unitPrice;
    private String plusOneCost;
    private String additionalPrice;
    private String maxquantity;
    private String quntity;
    private String amount;
    private String quantityAction;
    private String addedToCartOn;
    private String status;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getPlusOneCost() {
        return plusOneCost;
    }

    public void setPlusOneCost(String plusOneCost) {
        this.plusOneCost = plusOneCost;
    }

    public String getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(String additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public String getMaxquantity() {
        return maxquantity;
    }

    public void setMaxquantity(String maxquantity) {
        this.maxquantity = maxquantity;
    }

    public String getQuntity() {
        return quntity;
    }

    public void setQuntity(String quntity) {
        this.quntity = quntity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getQuantityAction() {
        return quantityAction;
    }

    public void setQuantityAction(String quantityAction) {
        this.quantityAction = quantityAction;
    }

    public String getAddedToCartOn() {
        return addedToCartOn;
    }

    public void setAddedToCartOn(String addedToCartOn) {
        this.addedToCartOn = addedToCartOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
