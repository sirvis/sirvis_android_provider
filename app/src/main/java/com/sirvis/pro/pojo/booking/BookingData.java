package com.sirvis.pro.pojo.booking;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Oct-17.
 */

public class BookingData implements Serializable {
    private String status;
    private String profileActivationStatus;
    private String profileStatus;
    private String walletAmount;
    private boolean bid;

    public boolean isBid() {
        return bid;
    }

    private String notificationUnreadCount;
    private ArrayList<Booking> request;
    private ArrayList<Booking> accepted;
    private ArrayList<Booking> activeBid;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Booking> getRequest() {
        return request;
    }

    public void setRequest(ArrayList<Booking> request) {
        this.request = request;
    }

    public ArrayList<Booking> getAccepted() {
        return accepted;
    }

    public void setAccepted(ArrayList<Booking> accepted) {
        this.accepted = accepted;
    }

    public ArrayList<Booking> getActiveBid() {
        return activeBid;
    }

    public void setActiveBid(ArrayList<Booking> activeBid) {
        this.activeBid = activeBid;
    }

    public String getProfileActivationStatus() {
        return profileActivationStatus;
    }

    public void setProfileActivationStatus(String profileActivationStatus) {
        this.profileActivationStatus = profileActivationStatus;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getNotificationUnreadCount() {
        return notificationUnreadCount;
    }

    public void setNotificationUnreadCount(String notificationUnreadCount) {
        this.notificationUnreadCount = notificationUnreadCount;
    }
}
