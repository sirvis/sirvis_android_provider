package com.sirvis.pro.pojo.profile.wallet;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 25/11/15.
 */
public class CardPojo implements Serializable {

    private String message;

    private ArrayList<CardData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CardData> getData() {
        return data;
    }

    public void setData(ArrayList<CardData> data) {
        this.data = data;
    }
}
