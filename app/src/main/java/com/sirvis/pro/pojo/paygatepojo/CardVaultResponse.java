package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CardVaultResponse {

    @SerializedName("SOAP-ENV:Envelope")
    @Expose
    private SOAPENVEnvelope sOAPENVEnvelope;

    public SOAPENVEnvelope getSOAPENVEnvelope() {
        return sOAPENVEnvelope;
    }

    public void setSOAPENVEnvelope(SOAPENVEnvelope sOAPENVEnvelope) {
        this.sOAPENVEnvelope = sOAPENVEnvelope;
    }

}