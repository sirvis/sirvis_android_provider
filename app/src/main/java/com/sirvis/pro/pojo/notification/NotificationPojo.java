package com.sirvis.pro.pojo.notification;

import java.util.ArrayList;

/**
 * Created by murashid on 09-Jun-18.
 */

public class NotificationPojo {
    private String message;
    private ArrayList<NotificationData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<NotificationData> getData() {
        return data;
    }

    public void setData(ArrayList<NotificationData> data) {
        this.data = data;
    }
}
