package com.sirvis.pro.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CognitoIdRespomse implements Serializable {

    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("data")
    Data data;

    public Data getData() {
        return data;
    }

    public class Data{
        @Expose
        @SerializedName("IdentityId")
        public String IdentityId;
        @Expose
        @SerializedName("bucket")
        public String bucket;
        @Expose
        @SerializedName("Token")
        public String Token;
        @Expose
        @SerializedName("region")
        public String region;

        public String getToken() {
            return Token;
        }

        public String getIdentityId() {
            return IdentityId;
        }

        public String getBucket() {
            return bucket;
        }

        public String getRegion() {
            return region;
        }
    }
}