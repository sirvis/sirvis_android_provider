package com.sirvis.pro.pojo.address;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 */

public class AddressPojo {
    private String message;

    private ArrayList<AddressData> data;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AddressData> getData() {
        return data;
    }

    public void setData(ArrayList<AddressData> data) {
        this.data = data;
    }

}
