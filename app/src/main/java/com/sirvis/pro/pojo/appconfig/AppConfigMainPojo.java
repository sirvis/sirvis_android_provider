package com.sirvis.pro.pojo.appconfig;

/**
 * Created by murashid on 07-Nov-17.
 */

public class AppConfigMainPojo {
    private String message;
    private AppConfigData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AppConfigData getData() {
        return data;
    }

    public void setData(AppConfigData data) {
        this.data = data;
    }
}
