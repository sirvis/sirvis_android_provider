package com.sirvis.pro.pojo.signup;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CityPojo {
    private String message;
    private ArrayList<CityData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CityData> getData() {
        return data;
    }

    public void setData(ArrayList<CityData> data) {
        this.data = data;
    }
}
