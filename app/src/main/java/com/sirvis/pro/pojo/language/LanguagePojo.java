package com.sirvis.pro.pojo.language;

import java.util.ArrayList;

/**
 * Created by murashid on 24-Apr-18.
 */

public class LanguagePojo {
    private String message;
    private ArrayList<LanguageData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<LanguageData> getData() {
        return data;
    }

    public void setData(ArrayList<LanguageData> data) {
        this.data = data;
    }
}
