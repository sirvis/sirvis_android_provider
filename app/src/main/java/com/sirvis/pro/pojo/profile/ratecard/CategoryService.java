package com.sirvis.pro.pojo.profile.ratecard;

import java.io.Serializable;

/**
 * Created by murashid on 13-Feb-18.
 */

public class CategoryService implements Serializable {

    private String _id;
    private String city_id;
    private String cat_id;
    private String sub_cat_id;
    private String ser_name;
    private String ser_desc;
    private String unit;
    private String is_unit;
    private String bannerImageApp;
    private String bannerImageWeb;
    private String quantity;
    private String plusOneCost;
    private String additionalPrice;
    private String maxquantity;
    private String minFees;
    private String maxFees;
    private String status;
    private String serviceCompletionTime;

    private boolean isSubCategoryHead = false;
    private String subCatName = "";
    private boolean isServiceUnderSubCat = false;
    private int subCatPosition = 0;
    private int servicePosition = 0;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getSer_name() {
        return ser_name;
    }

    public void setSer_name(String ser_name) {
        this.ser_name = ser_name;
    }

    public String getSer_desc() {
        return ser_desc;
    }

    public void setSer_desc(String ser_desc) {
        this.ser_desc = ser_desc;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIs_unit() {
        return is_unit;
    }

    public void setIs_unit(String is_unit) {
        this.is_unit = is_unit;
    }

    public String getBannerImageApp() {
        return bannerImageApp;
    }

    public void setBannerImageApp(String bannerImageApp) {
        this.bannerImageApp = bannerImageApp;
    }

    public String getBannerImageWeb() {
        return bannerImageWeb;
    }

    public void setBannerImageWeb(String bannerImageWeb) {
        this.bannerImageWeb = bannerImageWeb;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPlusOneCost() {
        return plusOneCost;
    }

    public void setPlusOneCost(String plusOneCost) {
        this.plusOneCost = plusOneCost;
    }

    public String getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(String additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public String getMaxquantity() {
        return maxquantity;
    }

    public void setMaxquantity(String maxquantity) {
        this.maxquantity = maxquantity;
    }

    public String getMinFees() {
        return minFees;
    }

    public void setMinFees(String minFees) {
        this.minFees = minFees;
    }

    public String getMaxFees() {
        return maxFees;
    }

    public void setMaxFees(String maxFees) {
        this.maxFees = maxFees;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServiceCompletionTime() {
        return serviceCompletionTime;
    }

    public void setServiceCompletionTime(String serviceCompletionTime) {
        this.serviceCompletionTime = serviceCompletionTime;
    }


    public boolean isSubCategoryHead() {
        return isSubCategoryHead;
    }

    public void setSubCategoryHead(boolean subCategoryHead) {
        isSubCategoryHead = subCategoryHead;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public boolean isServiceUnderSubCat() {
        return isServiceUnderSubCat;
    }

    public void setServiceUnderSubCat(boolean serviceUnderSubCat) {
        isServiceUnderSubCat = serviceUnderSubCat;
    }

    public int getSubCatPosition() {
        return subCatPosition;
    }

    public void setSubCatPosition(int subCatPosition) {
        this.subCatPosition = subCatPosition;
    }

    public int getServicePosition() {
        return servicePosition;
    }

    public void setServicePosition(int servicePosition) {
        this.servicePosition = servicePosition;
    }
}
