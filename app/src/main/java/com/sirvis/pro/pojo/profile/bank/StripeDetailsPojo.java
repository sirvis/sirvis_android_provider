package com.sirvis.pro.pojo.profile.bank;

/**
 * Created by murashid on 29-Aug-17.
 */

public class StripeDetailsPojo {

    private String message;
    private LegalEntity legal_entity;
    private ExternalAccounts external_accounts;
    private Accounts data;


    public String getMessage(){
        return message;
    }

    public Accounts getData() {
        return data;
    }

    public LegalEntity getLegal_entity() {
        return legal_entity;
    }

    public void setLegal_entity(LegalEntity legal_entity) {
        this.legal_entity = legal_entity;
    }

    public ExternalAccounts getExternal_accounts() {
        return external_accounts;
    }

    public void setExternal_accounts(ExternalAccounts external_accounts) {
        this.external_accounts = external_accounts;
    }

    public class Accounts {

        private String city;
        private String country;
        private String first_name;
        private String last_name;
        private String bankName;
        private String branch;
        private String accountNumber;
        private String iban;
        private String ip;
        private String document;
        private String providerId;

        private String _id;

        public String get_id() {
            return _id;
        }

        public String getCity() {
            return city;
        }

        public String getCountry() {
            return country;
        }

        public String getFirst_name() {
            return first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public String getBankName() {
            return bankName;
        }

        public String getBranch() {
            return branch;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public String getIban() {
            return iban;
        }

        public String getIp() {
            return ip;
        }

        public String getDocument() {
            return document;
        }

        public String getProviderId() {
            return providerId;
        }


    }
}
