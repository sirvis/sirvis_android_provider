package com.sirvis.pro.pojo.history;

import java.util.ArrayList;

/**
 * Created by murashid on 08-Dec-17.
 */

public class HistoryWeekPojo {
    private String message;
    private ArrayList<HistoryWeekData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<HistoryWeekData> getData() {
        return data;
    }

    public void setData(ArrayList<HistoryWeekData> data) {
        this.data = data;
    }
}
