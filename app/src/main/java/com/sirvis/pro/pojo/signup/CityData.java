package com.sirvis.pro.pojo.signup;

import java.io.Serializable;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CityData implements Serializable {
    private String id;
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
