package com.sirvis.pro.pojo.history;

import java.io.Serializable;

/**
 * Created by murashid on 07-Mar-18.
 */

public class AdditionalService implements Serializable{
    private String serviceName;
    private String price;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
