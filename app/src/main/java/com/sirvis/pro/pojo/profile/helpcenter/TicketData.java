package com.sirvis.pro.pojo.profile.helpcenter;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Dec-17.
 */

public class TicketData {
    private String ticket_id;
    private String timeStamp;
    private String subject;
    private String type;
    private String priority;
    private ArrayList<TicketEvents> events;

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public ArrayList<TicketEvents> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<TicketEvents> events) {
        this.events = events;
    }
}
