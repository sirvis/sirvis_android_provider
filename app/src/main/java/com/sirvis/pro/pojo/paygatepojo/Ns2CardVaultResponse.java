package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ns2CardVaultResponse {

    @SerializedName("ns2:Status")
    @Expose
    private Ns2Status ns2Status;
    @SerializedName("ns2:UserDefinedFields")
    @Expose
    private Ns2UserDefinedFields ns2UserDefinedFields;

    public Ns2Status getNs2Status() {
        return ns2Status;
    }

    public void setNs2Status(Ns2Status ns2Status) {
        this.ns2Status = ns2Status;
    }

    public Ns2UserDefinedFields getNs2UserDefinedFields() {
        return ns2UserDefinedFields;
    }

    public void setNs2UserDefinedFields(Ns2UserDefinedFields ns2UserDefinedFields) {
        this.ns2UserDefinedFields = ns2UserDefinedFields;
    }

}

