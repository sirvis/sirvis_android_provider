package com.sirvis.pro.pojo.shedule;

import java.util.ArrayList;

/**
 * Created by murashid on 23-Oct-17.
 */

public class ScheduleMonthData {
    private String _id;
    private String date;
    private ArrayList<Schedule> schedule;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Schedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(ArrayList<Schedule> schedule) {
        this.schedule = schedule;
    }
}
