package com.sirvis.pro.pojo.profile.document;

import com.sirvis.pro.pojo.signup.CategoryDocument;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 22-Mar-18.
 */

public class ProfileDocumentCategory implements Serializable{
    private String categoryId;
    private String categoryName;

    private ArrayList<CategoryDocument> documents;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<CategoryDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<CategoryDocument> documents) {
        this.documents = documents;
    }
}
