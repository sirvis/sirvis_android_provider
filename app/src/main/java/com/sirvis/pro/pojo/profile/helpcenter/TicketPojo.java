package com.sirvis.pro.pojo.profile.helpcenter;

/**
 * Created by murashid on 29-Dec-17.
 */

public class TicketPojo {
    private TicketData data;

    public TicketData getData() {
        return data;
    }

    public void setData(TicketData data) {
        this.data = data;
    }
}
