package com.sirvis.pro.pojo.chat;

import java.io.Serializable;

/**
 * Created by Ali on 12/22/2017.
 */

public class ChatMqttResponce implements Serializable
{

    private ChatData data;

    public ChatData getData() {
        return data;
    }

    public void setData(ChatData data) {
        this.data = data;
    }


}
