package com.sirvis.pro.pojo.signup;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by murashid on 07-Oct-17.
 */


public class SignUpPojo implements Serializable {

    private String message;

    @Override
    public String toString() {
        return "{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    private SignUpData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SignUpData getData() {
        return data;
    }

    public void setData(SignUpData data) {
        this.data = data;
    }
}
