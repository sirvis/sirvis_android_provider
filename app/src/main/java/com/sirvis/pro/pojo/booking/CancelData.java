package com.sirvis.pro.pojo.booking;

/**
 * Created by murashid on 08-Nov-17.
 */

public class CancelData {
    private String res_id;
    private String reason;
    public String getRes_id() {
        return res_id;
    }

    public void setRes_id(String res_id) {
        this.res_id = res_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
