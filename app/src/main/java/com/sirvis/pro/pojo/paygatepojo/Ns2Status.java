package com.sirvis.pro.pojo.paygatepojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ns2Status {

    @SerializedName("ns2:StatusName")
    @Expose
    private String ns2StatusName;
    @SerializedName("ns2:VaultId")
    @Expose
    private String ns2VaultId;
    @SerializedName("ns2:StatusDetail")
    @Expose
    private String ns2StatusDetail;

    public String getNs2StatusName() {
        return ns2StatusName;
    }

    public void setNs2StatusName(String ns2StatusName) {
        this.ns2StatusName = ns2StatusName;
    }

    public String getNs2VaultId() {
        return ns2VaultId;
    }

    public void setNs2VaultId(String ns2VaultId) {
        this.ns2VaultId = ns2VaultId;
    }

    public String getNs2StatusDetail() {
        return ns2StatusDetail;
    }

    public void setNs2StatusDetail(String ns2StatusDetail) {
        this.ns2StatusDetail = ns2StatusDetail;
    }

}

