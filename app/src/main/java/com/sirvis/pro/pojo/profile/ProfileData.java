package com.sirvis.pro.pojo.profile;

import com.sirvis.pro.pojo.profile.document.ProfileDocumentCategory;
import com.sirvis.pro.pojo.profile.events.MyListEvents;
import com.sirvis.pro.pojo.profile.metaData.MetaDataArr;
import com.sirvis.pro.pojo.profile.service.MyListService;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 10-Sep-17.
 */

public class ProfileData implements Serializable{
    private String firstName;
    private String lastName;
    private String mobile;
    private String countryCode;
    private String dob;
    private String profilePic;
    private String email;
    private String gender;

    private String about;
    private String link;
    private String rules;
    private String instruments;
    private String profileStatus;
    private String radius;
    private String address;
    private String musicGeners;
    private String catName;
    private String minRadius;
    private String maxRadius;

    private ArrayList<String> catNameArr;
    private ArrayList<MyListEvents> events;
    private ArrayList<MyListService> services;

    private ArrayList<MetaDataArr> metaDataArr;
    private ArrayList<String> workImage;

    private ArrayList<ProfileDocumentCategory> documents;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getInstruments() {
        return instruments;
    }

    public void setInstruments(String instruments) {
        this.instruments = instruments;
    }

    public String getProfileStatus() {
        return profileStatus;
    }

    public void setProfileStatus(String profileStatus) {
        this.profileStatus = profileStatus;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMusicGeners() {
        return musicGeners;
    }

    public void setMusicGeners(String musicGeners) {
        this.musicGeners = musicGeners;
    }

    public ArrayList<MyListEvents> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<MyListEvents> events) {
        this.events = events;
    }

    public ArrayList<MyListService> getServices() {
        return services;
    }

    public void setServices(ArrayList<MyListService> services) {
        this.services = services;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getMinRadius() {
        return minRadius;
    }

    public void setMinRadius(String minRadius) {
        this.minRadius = minRadius;
    }

    public String getMaxRadius() {
        return maxRadius;
    }

    public void setMaxRadius(String maxRadius) {
        this.maxRadius = maxRadius;
    }

    public ArrayList<MetaDataArr> getMetaDataArr() {
        return metaDataArr;
    }

    public void setMetaDataArr(ArrayList<MetaDataArr> metaDataArr) {
        this.metaDataArr = metaDataArr;
    }

    public ArrayList<String> getWorkImage() {
        return workImage;
    }

    public void setWorkImage(ArrayList<String> workImage) {
        this.workImage = workImage;
    }

    public ArrayList<ProfileDocumentCategory> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<ProfileDocumentCategory> documents) {
        this.documents = documents;
    }

    public ArrayList<String> getCatNameArr() {
        return catNameArr;
    }

    public void setCatNameArr(ArrayList<String> catNameArr) {
        this.catNameArr = catNameArr;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
