package com.sirvis.pro.pojo.history;

import java.util.ArrayList;

/**
 * Created by murashid on 08-Dec-17.
 */

public class HistoryWeekData {
    private String startDate;
    private String endDate;
    private String sDate;
    private ArrayList<Integer> count;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public ArrayList<Integer> getCount() {
        return count;
    }

    public void setCount(ArrayList<Integer> count) {
        this.count = count;
    }

}
