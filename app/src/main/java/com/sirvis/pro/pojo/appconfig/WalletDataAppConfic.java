package com.sirvis.pro.pojo.appconfig;

/**
 * Created by murashid on 26-Apr-18.
 */

public class WalletDataAppConfic {
    private String enableWallet;
    private String walletAmount;
    private String softLimit;
    private String hardLimit;
    private String reachedSoftLimit;
    private String reachedHardLimit;

    public String getEnableWallet() {
        return enableWallet;
    }

    public void setEnableWallet(String enableWallet) {
        this.enableWallet = enableWallet;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public String getSoftLimit() {
        return softLimit;
    }

    public void setSoftLimit(String softLimit) {
        this.softLimit = softLimit;
    }

    public String getHardLimit() {
        return hardLimit;
    }

    public void setHardLimit(String hardLimit) {
        this.hardLimit = hardLimit;
    }

    public String getReachedSoftLimit() {
        return reachedSoftLimit;
    }

    public void setReachedSoftLimit(String reachedSoftLimit) {
        this.reachedSoftLimit = reachedSoftLimit;
    }

    public String getReachedHardLimit() {
        return reachedHardLimit;
    }

    public void setReachedHardLimit(String reachedHardLimit) {
        this.reachedHardLimit = reachedHardLimit;
    }
}
