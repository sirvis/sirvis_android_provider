package com.sirvis.pro.pojo.signup;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 */

public class CategoriesData implements Serializable {
    private String catId;
    private String catName;
    private ArrayList<CategoryDocument> document;
    private ArrayList<SubCategory> subCategory;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public ArrayList<CategoryDocument> getDocument() {
        return document;
    }

    public void setDocument(ArrayList<CategoryDocument> document) {
        this.document = document;
    }

    public ArrayList<SubCategory> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(ArrayList<SubCategory> subCategory) {
        this.subCategory = subCategory;
    }
}
