package com.sirvis.pro.pojo.shedule;

import java.util.ArrayList;

/**
 * Created by murashid on 17-May-18.
 */

public class ScheduleData {
    private ArrayList<ScheduleViewData> active;
    private ArrayList<ScheduleViewData> past;

    public ArrayList<ScheduleViewData> getActive() {
        return active;
    }

    public void setActive(ArrayList<ScheduleViewData> active) {
        this.active = active;
    }

    public ArrayList<ScheduleViewData> getPast() {
        return past;
    }

    public void setPast(ArrayList<ScheduleViewData> past) {
        this.past = past;
    }
}
