package com.sirvis.pro.pojo.booking;

import java.util.ArrayList;

/**
 * Created by murashid on 08-Nov-17.
 */

public class CancelPojo {
    private String message;
    ArrayList<CancelData> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CancelData> getData() {
        return data;
    }

    public void setData(ArrayList<CancelData> data) {
        this.data = data;
    }

}
