package com.sirvis.pro.pojo.booking;

/**
 * Created by murashid on 07-Oct-17.
 */

public class BookingPojo {
    private String message;

    private BookingData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BookingData getData() {
        return data;
    }

    public void setData(BookingData data) {
        this.data = data;
    }
}
