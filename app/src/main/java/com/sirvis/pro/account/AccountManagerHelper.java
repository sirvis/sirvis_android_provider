package com.sirvis.pro.account;

import android.accounts.Account;
import android.accounts.AccountManager;

import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.VariableConstant;

import java.util.Arrays;
import java.util.List;

/**
 * Created by murashid on 22-May-18.
 */

public class AccountManagerHelper {
    public static final String TAG = AccountManagerHelper.class.getSimpleName();

    private static AccountManager mAccountManager;

    public AccountManagerHelper(AppController appController)
    {
        mAccountManager = AccountManager.get(appController.getApplicationContext());
    }

    /**
     * <h2>addAccount</h2>
     This method is used to set the auth token by creating the account manager with the account
     @param emailID email ID to be added **/

    public void setAuthToken(String emailID, String password, String authToken) {
        Account account = new Account(emailID, VariableConstant.ACCOUNT_TYPE);
        mAccountManager.addAccountExplicitly(account, password, null);
        mAccountManager.setAuthToken(account, VariableConstant.AUTHTOKEN_TYPE_FULL_ACCESS, authToken);
    }

    /** <h2>getAuthToken</h2>
     This method is used to get the auth token from the created account
     @return auth token store **/

    public String getAuthToken(String emailID) {
        Account[] account = mAccountManager.getAccountsByType(VariableConstant.ACCOUNT_TYPE);
        List<Account> accounts = Arrays.asList(account);
        if (accounts.size() > 0) {
            for (int i = 0; i < accounts.size(); i++) {
                if (accounts.get(i).name.equals(emailID))
                    return mAccountManager.peekAuthToken(accounts.get(i), VariableConstant.AUTHTOKEN_TYPE_FULL_ACCESS);
            }
        }
        return "xyz";
    }

}
