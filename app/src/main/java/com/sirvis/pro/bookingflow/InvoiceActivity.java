package com.sirvis.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.review.CustomerReviewsActivity;
import com.sirvis.pro.bookingflow.review.RateCustomerActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.pojo.booking.ServiceItem;
import com.sirvis.pro.pojo.history.Accounting;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.DecimalDigitsInputFilter;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>InvoiceActivity</h1>
 * InvoiceActivity for showing the invoice detatils and take signature from customer
 */
public class InvoiceActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = "InvoiceActivity";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private SeekBar seekBar;

    private SignaturePad signaturePad;
    private File signatureFile;
    private Bitmap signatureBitmap;
    private boolean isFromBookingList = false;
    private Booking booking;

    private Typeface fontRegular;
    private LinearLayout llAddExtraItem, llSignature, llPayment, llQuestionAnswer;
    private LayoutInflater inflater;
    private TextView tvAddNewItem, tvConfirm, tvTotalBillAmount, tvPaymentMethod, tvTotal;
    private ArrayList<EditText> etAmounts, etExtraFees;
    private double total = 0;
    private double totalAmount;

    private BroadcastReceiver receiver;
    private SimpleDateFormat displayDateFormat, serverFormat, displayHourFormat;
    private TextView tvDiscount, tvLastDue, tvVisitFee, tvTravelFee, tvTime, tvDate;
    private LinearLayout llService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);

        initView();
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        isFromBookingList = getIntent().getBooleanExtra("isFromBookingList", false);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        displayHourFormat = new SimpleDateFormat("hh:mm a", Locale.US);


        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatusRaiseInvoice));
        progressDialog.setCancelable(false);

        Typeface fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.cusomerSignature));
        tvTitle.setTypeface(fontRegular);

        ImageView ivCustomer = findViewById(R.id.ivCustomer);

        TextView tvCustomerName = findViewById(R.id.tvCustomerName);
        TextView tvCategory = findViewById(R.id.tvCategory);

        etAmounts = new ArrayList<>();
        etExtraFees = new ArrayList<>();

        TextView tvSignatureHeader = findViewById(R.id.tvSignatureHeader);
        TextView tvRetake = findViewById(R.id.tvRetake);
        seekBar = findViewById(R.id.seekBar);
        signaturePad = findViewById(R.id.signaturePad);
        TextView tvSeekbarText = findViewById(R.id.tvSeekbarText);

        llService = findViewById(R.id.llService);
        TextView tvTotalBillAmountLabel = findViewById(R.id.tvTotalBillAmountLabel);
        tvTotalBillAmount = findViewById(R.id.tvTotalBillAmount);
        tvDate = findViewById(R.id.tvDate);
        tvTime = findViewById(R.id.tvTime);
        TextView tvSelectedServiceLabel = findViewById(R.id.tvSelectedServiceLabel);
        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        tvLastDue = findViewById(R.id.tvLastDue);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        tvDiscount = findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);
        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);

        llAddExtraItem = findViewById(R.id.llAddExtraItem);
        llSignature = findViewById(R.id.llSignature);
        llPayment = findViewById(R.id.llPayment);
        tvConfirm = findViewById(R.id.tvConfirm);
        tvAddNewItem = findViewById(R.id.tvAddNewItem);
        inflater = LayoutInflater.from(this);

        tvSignatureHeader.setTypeface(fontMedium);
        tvRetake.setTypeface(fontMedium);
        tvCustomerName.setTypeface(fontMedium);
        tvCategory.setTypeface(fontRegular);
        tvSeekbarText.setTypeface(fontBold);

        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontRegular);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvSelectedServiceLabel.setTypeface(fontMedium);
        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvAddNewItem.setTypeface(fontMedium);
        tvConfirm.setTypeface(fontMedium);


        tvAddNewItem.setVisibility(View.VISIBLE);
        tvAddNewItem.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);
        tvRetake.setOnClickListener(this);


        if (!booking.getProfilePic().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getProfilePic())
                    .into(ivCustomer);
        }
        tvCustomerName.setText(booking.getFirstName() + " " + booking.getLastName());
        tvCategory.setText(booking.getCategory());
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        final File dir = myImageHandler.getAlbumStorageDir(this, VariableConstant.SIGNATURE_PIC_DIR, true);
        signatureFile = new File(dir, booking.getBookingId() + ".jpg");

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                signatureBitmap = signaturePad.getSignatureBitmap();
            }

            @Override
            public void onClear() {
                Toast.makeText(InvoiceActivity.this, getString(R.string.cleared), Toast.LENGTH_SHORT).show();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                if (seekBar.getProgress() > 75) {
                    if (!signaturePad.isEmpty()) {
                        seekBar.setProgress(100);
                        JSONObject jsonObject = new JSONObject();
                        try {
                            String BUCKETSUBFOLDER = VariableConstant.SIGNATURE_UPLOAD;
                            String signatureUrl = "https://" + VariableConstant.BUCKET_NAME + "." + BuildConfig.AMAZON_BASE_URL + BUCKETSUBFOLDER +"/"+ signatureFile.getName();                            Utility.saveImage(signatureBitmap, signatureFile);

                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            Uri contentUri = Uri.fromFile(signatureFile);
                            mediaScanIntent.setData(contentUri);
                            sendBroadcast(mediaScanIntent);

                            jsonObject.put("bookingId", booking.getBookingId());
                            jsonObject.put("status", VariableConstant.JOB_COMPLETED_RAISE_INVOICE);
                            jsonObject.put("latitude", sessionManager.getCurrentLat());
                            jsonObject.put("longitude", sessionManager.getCurrentLng());
                            jsonObject.put("longitude", sessionManager.getCurrentLng());
                            jsonObject.put("signatureUrl", signatureUrl);

                            JSONArray jsonArray = new JSONArray();
                            for (int i = 0; i < etAmounts.size(); i++) {
                                JSONObject jsonObjectExtra = new JSONObject();
                                jsonObjectExtra.put("serviceName", etExtraFees.get(i).getText().toString());
                                jsonObjectExtra.put("price", etAmounts.get(i).getText().toString());
                                jsonArray.put(jsonObjectExtra);
                            }

                            jsonObject.put("additionalService", jsonArray);

                            Log.d(TAG, "onStopTrackingTouch: " + jsonObject);

                            UploadFileAmazonS3 amazonS3 = UploadFileAmazonS3.getInstance(InvoiceActivity.this);
                            presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject, amazonS3, signatureFile);
                        } catch (Exception e) {
                            e.printStackTrace();
                            seekBar.setProgress(0);
                        }
                    } else {
                        seekBar.setProgress(0);
                        Toast.makeText(InvoiceActivity.this, getString(R.string.plsProvideSignature), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    seekBar.setProgress(0);
                }

            }
        });

        sessionManager.clearDistance(booking.getBookingId());

        EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                1000, VariableConstant.STORAGE_CAMERA_PERMISSION);

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String cancelId = intent.getStringExtra("cancelid");
                String msg = intent.getStringExtra("msg");
                String header = intent.getStringExtra("header");
                if (cancelId.equals(booking.getBookingId())) {
                    VariableConstant.IS_BOOKING_UPDATED = true;
                    closeActivity();
                    // DialogHelper.customAlertDialogCloseActivity(EventStartedCompletedActivity.this, header, msg, getString(R.string.oK));
                }
            }
        };

        registerReceiver(receiver, filter);
        Log.d(TAG, "isFromBookingList: " + isFromBookingList);
        if (isFromBookingList) {

            onSuccess("", true);
        }
        if (!isFromBookingList) {
            presenter.getBookingById(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), booking.getBookingId());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String result, boolean bookingDetailsRefreshed) {
        Log.d(TAG, "isFromBookingList: " + isFromBookingList);
        if (bookingDetailsRefreshed) {
            try {
                if (!isFromBookingList) {
                    booking = new Gson().fromJson(result, Booking.class);
                }
                Accounting accounting = booking.getAccounting();

                try{
                    totalAmount = Double.parseDouble(accounting.getTotal()) - Double.parseDouble(accounting.getLastDues());
                }
                catch (Exception e){
                    e.getMessage();
                }

                tvTotalBillAmount.setText(Utility.getPrice(String.valueOf(totalAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                tvTotal.setText(Utility.getPrice(String.valueOf(totalAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                total = Double.parseDouble(accounting.getTotal()) - Double.parseDouble(accounting.getLastDues());

                if (accounting.getTravelFee() != null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.0") && !accounting.getTravelFee().equals("0.00")) {
                    findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
                    tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                }
                if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.0") && !accounting.getVisitFee().equals("0.00")) {
                    findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
                    tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                }
               /* if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.0") && !accounting.getLastDues().equals("0.00")) {
                    findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
                    tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                }*/

                if (accounting.getPaymentMethod().equals("1")) {
                    tvPaymentMethod.setText(getString(R.string.cash));
                    tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
                } else {
                    tvPaymentMethod.setText(getString(R.string.card) + "  " + accounting.getLast4());
                }

                if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
                    tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + " + getString(R.string.wallet));
                }

                if (!booking.getBookingModel().equals("3")) {
                    if (booking.getServiceType().equals("1")) {
                        for (ServiceItem serviceItem : booking.getCartData()) {
                            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                            tvServiceFeeLabel.setTypeface(fontRegular);
                            tvServiceFee.setTypeface(fontRegular);
                            tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                            tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                            llService.addView(serviceView);
                        }
                    } else if (!booking.getCallType().equals("1")) {
                        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                        tvServiceFeeLabel.setTypeface(fontRegular);
                        tvServiceFee.setTypeface(fontRegular);
                        tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
                        tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                        llService.addView(serviceView);
                    } else {
                        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                        tvServiceFeeLabel.setTypeface(fontRegular);
                        tvServiceFee.setTypeface(fontRegular);
                        tvServiceFeeLabel.setText(getString(R.string.consultingfee));
                        tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                        llService.addView(serviceView);
                    }
                } else {
                    View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                    TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                    TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                    tvServiceFeeLabel.setTypeface(fontRegular);
                    tvServiceFee.setTypeface(fontRegular);
                    tvServiceFeeLabel.setText(getString(R.string.serviceFee));
                    tvServiceFee.setText(Utility.getPrice(String.valueOf(totalAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    llService.addView(serviceView);
                }

                try {
                    tvDate.setText(getString(R.string.date) + " : " + displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                    tvTime.setText(getString(R.string.time) + " : " + displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {


            VariableConstant.IS_BOOKING_UPDATED = true;

            AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingCancelled.value, booking.getBookingId());

            finish();
            sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(), VariableConstant.JOB_COMPLETED_RAISE_INVOICE));
            booking.getAccounting().setTotal(String.valueOf(total));

            Intent intent = new Intent(this, RateCustomerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("rateNotfcnCustomer", false);
            bundle.putSerializable("booking", booking);
            intent.putExtras(bundle);

            startActivity(intent);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);

            AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.RaiseInvoice.value, booking.getBookingId());
        }
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCutomerDetails:
                Intent intent = new Intent(this, CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.tvRetake:
                signaturePad.clear();
                break;

            case R.id.tvDone:
                break;

            case R.id.tvAddNewItem:
                llSignature.setVisibility(View.GONE);
                llPayment.setVisibility(View.GONE);
                tvConfirm.setVisibility(View.VISIBLE);
                if (llQuestionAnswer != null) {
                    llQuestionAnswer.setVisibility(View.GONE);
                }
                addRemoveDynamicView();
                break;

            case R.id.tvConfirm:
                if (isValidExtraFees()) {
                    tvConfirm.setVisibility(View.GONE);
                    llSignature.setVisibility(View.VISIBLE);
                    llPayment.setVisibility(View.VISIBLE);
                    tvAddNewItem.setVisibility(View.GONE);
                    if (llQuestionAnswer != null) {
                        llQuestionAnswer.setVisibility(View.VISIBLE);
                    }

                    addStaticView();
                }
                break;

        }
    }

    private boolean isValidExtraFees() {
        for (int i = 0; i < etAmounts.size(); i++) {
            if (etExtraFees.get(i).getText().toString().equals("")) {
                Toast.makeText(this, getString(R.string.plsEnterServiceName), Toast.LENGTH_SHORT).show();
                return false;
            } else if (etAmounts.get(i).getText().toString().equals("")) {
                Toast.makeText(this, getString(R.string.plsEnterPrice), Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    private void addRemoveDynamicView() {
        final View view = inflater.inflate(R.layout.add_new_item_invoice_view, null);
        llAddExtraItem.addView(view);
        ImageView ivDelete = view.findViewById(R.id.ivDelete);
        final EditText etAmount = view.findViewById(R.id.etAmount);
        final EditText etExtraFee = view.findViewById(R.id.etExtraFee);
        final TextView tvCurrencySymbol = view.findViewById(R.id.tvCurrencySymbol);
        final TextView tvCurrencySymbolSuffix = view.findViewById(R.id.tvCurrencySymbolSuffix);
        etAmount.setTypeface(fontRegular);
        etAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(15, 2)});
        etExtraFee.setTypeface(fontRegular);
        tvCurrencySymbol.setTypeface(fontRegular);
        tvCurrencySymbolSuffix.setTypeface(fontRegular);
        tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
        tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());

        etAmounts.add(etAmount);
        etExtraFees.add(etExtraFee);

        if (sessionManager.getCurrencyAbbrevation().equals("2")) {
            tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
            tvCurrencySymbol.setVisibility(View.GONE);
        }

        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddExtraItem.removeView(view);
                etAmounts.remove(etAmount);
                etExtraFees.remove(etExtraFee);

                if (etAmounts.size() == 0) {
                    tvConfirm.setVisibility(View.GONE);
                    llSignature.setVisibility(View.VISIBLE);
                    llPayment.setVisibility(View.VISIBLE);
                    if (llQuestionAnswer != null) {
                        llQuestionAnswer.setVisibility(View.VISIBLE);
                    }
                }

                calculateTotal();
            }
        });

        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                calculateTotal();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    etAmount.setText(Utility.getFormattedPrice(etAmount.getText().toString()));
                } else {
                    etAmount.setCursorVisible(true);
                }
            }
        });

        etAmount.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    etAmount.setCursorVisible(false);
                    return true;
                }
                return false;
            }
        });

    }

    private void addStaticView() {
        llAddExtraItem.removeAllViews();
        for (int i = 0; i < etAmounts.size(); i++) {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(etExtraFees.get(i).getText().toString());
            tvServiceFee.setText(Utility.getPrice(etAmounts.get(i).getText().toString(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llAddExtraItem.addView(serviceView);
        }

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < etAmounts.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();

                jsonObject1.put("serviceName", etExtraFees.get(i).getText().toString());
                jsonObject1.put("price", etAmounts.get(i).getText().toString());
                jsonArray.put(jsonObject1);
            }
            jsonObject.put("additionalService", jsonArray);
            Log.d(TAG, "addStaticView: " + jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void calculateTotal() {
        double serviceTotal = 0;
        for (EditText etAmount : etAmounts) {
            if (!etAmount.getText().toString().equals("")) {
                try {
                    serviceTotal += Double.parseDouble(etAmount.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        tvTotal.setText(Utility.getPrice(String.valueOf(total + serviceTotal), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotalBillAmount.setText(tvTotal.getText().toString());
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_blue_dot_icon)));
        googleMap.getUiSettings().setCompassEnabled(false);
    }
}
