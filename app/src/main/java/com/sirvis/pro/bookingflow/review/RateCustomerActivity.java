package com.sirvis.pro.bookingflow.review;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.ReceiptDialogFragment;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.history.Accounting;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.DataBaseChat;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.sirvis.pro.utility.Utility.convertUTCToServerFormat;

public class RateCustomerActivity extends AppCompatActivity implements RateCustomerPresenter.RateCustomerActivityImple, View.OnClickListener {

    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private RateCustomerPresenter presenter;

    private EditText etReview;
    private RatingBar ratingStar;

    private String name = "";
    private Booking booking;
    private boolean isFromNotification = true, isRefreshing = true;
    private ImageView ivCustomer;
    private TextView tvPrice, tvPriceLabel, tvSubmit, tvDate, rateTitle1, tvCustomerName,tvReceipt;
    private SimpleDateFormat serverFormat, displayDateFormat;
    private String bookingId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_customer);

        init();
    }

    private void init() {
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        displayDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new RateCustomerPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.submitting));
        progressDialog.setCancelable(false);

        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        rateTitle1 = findViewById(R.id.rateTitle1);
        tvDate = findViewById(R.id.tvDate);
        tvPriceLabel = findViewById(R.id.tvPriceLabel);
        tvPrice = findViewById(R.id.tvPrice);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        tvSubmit = findViewById(R.id.tvSubmit);
        etReview = findViewById(R.id.etReview);
        ratingStar = findViewById(R.id.ratingStar);
        ivCustomer = findViewById(R.id.ivCustomer);
         tvReceipt = findViewById(R.id.tvReceipt);
        rateTitle1.setTypeface(fontMedium);
        tvDate.setTypeface(fontBold);
        tvPriceLabel.setTypeface(fontRegular);
        tvPrice.setTypeface(fontBold);
        tvCustomerName.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        etReview.setTypeface(fontMedium);

        tvSubmit.setOnClickListener(this);
        tvReceipt.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFromNotification = bundle.getBoolean("rateNotfcnCustomer", false);
        }

        if (isFromNotification) {
            try {
                bookingId = bundle.getString("bookingId");
            } catch (Exception e) {
                e.printStackTrace();
            }
            //call api
            presenter.getBookingById(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), bookingId);
        } else {
//            Toast.makeText(this,"sdjhf",Toast.LENGTH_LONG).show();
            booking = (Booking) bundle.getSerializable("booking");
            bookingId=booking.getBookingId();
            //call api
            presenter.getBookingById(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), bookingId);

//            onSuccess("", isFromNotification, isRefreshing);

        }

    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("showDialog", false);
        startActivity(intent);
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                isFromNotification = false;
                isRefreshing = false;
                try {
                    Log.d("mura", "onClick: " + ratingStar.getRating());
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("bookingId", bookingId);
                    jsonObject.put("rating", "" + ratingStar.getRating());
                    jsonObject.put("review", etReview.getText().toString());

                    presenter.setCustomerRating(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.tvReceipt:
                if(booking !=null )
                {
                    ReceiptDialogFragment.newInstance(booking).show(getSupportFragmentManager(),"");
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    public void onSuccess(String msg, boolean isFromNotification, boolean ispageRefreshing) {
        if (isFromNotification) {
            booking = new Gson().fromJson(msg, Booking.class);
        }
        if (ispageRefreshing) {


            Accounting accounting = booking.getAccounting();

            if (!booking.getProfilePic().equals("")) {
                Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                        .error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(this))
                        .placeholder(R.drawable.profile_default_image))
                        .load(booking.getProfilePic())
                        .into(ivCustomer);
            }

            bookingId = booking.getBookingId();
            name = booking.getFirstName() + " " + booking.getLastName();

            tvCustomerName.setText(booking.getFirstName() + " " + booking.getLastName());

            try {
                String endDate = displayDateFormat.format(serverFormat.parse(convertUTCToServerFormat(booking.getBookingRequestedFor())));
                String[] endDatearr = endDate.split(" ");
                tvDate.setText(Utility.getDayOfMonthSuffix(Integer.parseInt(endDatearr[0]), endDatearr[1] + " " + endDatearr[2]));

                sessionManager.clearChatCountPreference(bookingId);
                DataBaseChat db = new DataBaseChat(this);
                db.deleteData(bookingId);
                double totalAmount  = Double.parseDouble(accounting.getTotal()) - Double.parseDouble(accounting.getLastDues());
                tvPrice.setText(Utility.getPrice(String.valueOf(totalAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            AppController.getInstance().getMixpanelHelper().postingReview(name, bookingId);
            closeActivity();
        }

    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEmptyReview() {
        Toast.makeText(this, getString(R.string.plsEnterReview), Toast.LENGTH_SHORT).show();
    }

}
