package com.sirvis.pro.bookingflow;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.JobImageListAdapter;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.ImgUrls;
import com.sirvis.pro.pojo.booking.QuestionAnswer;
import com.sirvis.pro.pojo.booking.ServiceItem;
import com.sirvis.pro.pojo.history.Accounting;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.sirvis.pro.utility.Utility.getDayNumberSuffix;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link BookingDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingDetailsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Booking booking;
    private boolean isAcceptReject;
    private Handler handler;
    private Runnable runnable;
    private long totalSecs, hours, minutes, seconds;

    public LinearLayout llAddress;
    private double totalAmount;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment BookingDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingDetailsFragment newInstance(Booking param1, boolean isAcceptReject) {
        BookingDetailsFragment fragment = new BookingDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, isAcceptReject);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            booking = (Booking) getArguments().getSerializable(ARG_PARAM1);
            isAcceptReject = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_booking_details, container, false);
        init(rootView);
        return rootView;
    }

    @SuppressLint("SetTextI18n")
    private void init(View rootView) {
        Typeface fontMedium = Utility.getFontMedium(getContext());
        Typeface fontRegular = Utility.getFontRegular(getContext());
        SessionManager sessionManager = SessionManager.getSessionManager(getContext());
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayReapeatDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        SimpleDateFormat displayHourFormat = new SimpleDateFormat("hh:mm a", Locale.US);


        TextView tvAddress = rootView.findViewById(R.id.tvAddress);
        TextView tvDistance = rootView.findViewById(R.id.tvDistance);
        TextView tvCustomerName = rootView.findViewById(R.id.tvCustomerName);
        TextView tvCategory = rootView.findViewById(R.id.tvCategory);
        TextView tvBookingId = rootView.findViewById(R.id.tvBookingId);
        TextView tvRemainingTime = rootView.findViewById(R.id.tvRemainingTime);
        TextView tvTotalBillAmountLabel = rootView.findViewById(R.id.tvTotalBillAmountLabel);
        TextView tvTotalBillAmount = rootView.findViewById(R.id.tvTotalBillAmount);
        TextView tvDate = rootView.findViewById(R.id.tvDate);
        TextView tvTime = rootView.findViewById(R.id.tvTime);
        TextView tvslotTime = rootView.findViewById(R.id.tvslotTime);
        TextView tvSelectedServiceLabel = rootView.findViewById(R.id.tvSelectedServiceLabel);
        TextView tvPaymentMethodLabel = rootView.findViewById(R.id.tvPaymentMethodLabel);
        TextView tvPaymentMethod = rootView.findViewById(R.id.tvPaymentMethod);
        TextView tvJobDescriptionLabel = rootView.findViewById(R.id.tvJobDescriptionLabel);
        TextView tvJobDescription = rootView.findViewById(R.id.tvJobDescription);
        TextView tvJobPhotosLabel = rootView.findViewById(R.id.tvJobPhotosLabel);

        TextView tvScheduleStartTime = rootView.findViewById(R.id.tvScheduleStartTime);
        TextView tvScheduleStartDate = rootView.findViewById(R.id.tvScheduleStartDate);
        TextView tvScheduleEndTime = rootView.findViewById(R.id.tvScheduleEndTime);
        TextView tvScheduleEndDate = rootView.findViewById(R.id.tvScheduleEndDate);
        TextView tvTo = rootView.findViewById(R.id.tvTo);
        TextView tvTotalNoOfShift = rootView.findViewById(R.id.tvTotalNoOfShift);
        TextView tvShift = rootView.findViewById(R.id.tvShift);

        llAddress = rootView.findViewById(R.id.llAddress);

        RecyclerView rvJobPhotos = rootView.findViewById(R.id.rvJobPhotos);
        rvJobPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        //rvJobPhotos.setAdapter(new JobImageListAdapter(getActivity(),null));

        tvAddress.setTypeface(fontRegular);
        tvDistance.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvCategory.setTypeface(fontRegular);
        tvBookingId.setTypeface(fontRegular);
        tvRemainingTime.setTypeface(fontRegular);
        tvTotalBillAmountLabel.setTypeface(fontMedium);
        tvSelectedServiceLabel.setTypeface(fontMedium);
        tvTotalBillAmount.setTypeface(fontMedium);
        tvDate.setTypeface(fontRegular);
        tvTime.setTypeface(fontRegular);
        tvslotTime.setTypeface(fontRegular);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontRegular);
        tvJobDescriptionLabel.setTypeface(fontMedium);
        tvJobDescription.setTypeface(fontRegular);
        tvJobPhotosLabel.setTypeface(fontMedium);

        tvScheduleStartTime.setTypeface(fontRegular);
        tvScheduleStartDate.setTypeface(fontMedium);
        tvScheduleEndTime.setTypeface(fontRegular);
        tvScheduleEndDate.setTypeface(fontMedium);
        tvTo.setTypeface(fontRegular);
        tvTotalNoOfShift.setTypeface(fontMedium);
        tvShift.setTypeface(fontMedium);

        Accounting accounting = booking.getAccounting();

        tvBookingId.setText(getString(R.string.bookingID) + booking.getBookingId());
        if(booking.getCallType().equals("2"))
        {
            tvAddress.setText(booking.getAddLine1() + " " + booking.getAddLine2());
            tvDistance.setText("" + Utility.getFormattedPrice(String.valueOf((Double.parseDouble(booking.getDistance()) * sessionManager.getDistanceUnitConverter()))) + " " +
                    sessionManager.getDistanceUnit() + "  " + getString(R.string.away));
        }
        else{
            llAddress.setVisibility(View.GONE);
        }
        tvCustomerName.setText(booking.getFirstName() + " " + booking.getLastName());
        tvCategory.setText(booking.getCategory());

        try{
            totalAmount = Double.parseDouble(accounting.getTotal()) - Double.parseDouble(accounting.getLastDues());
        }
        catch (Exception e){
            e.getMessage();
        }

        tvTotalBillAmount.setText(Utility.getPrice(String.valueOf(totalAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        if (accounting.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
            tvPaymentMethod.setText(getString(R.string.card) + "  " + accounting.getLast4());
        }

        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
            tvPaymentMethod.setText(tvPaymentMethod.getText().toString() + " + " + getString(R.string.wallet));
        }

        if (booking.getJobDescription() != null && !booking.getJobDescription().equals("")) {
            tvJobDescription.setText(booking.getJobDescription());
        } else {
            rootView.findViewById(R.id.llJobDescription).setVisibility(View.GONE);
        }

        try {

            SimpleDateFormat getDateOnly = new SimpleDateFormat("dd", Locale.US);
            int day=Integer.parseInt(getDateOnly.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            String dayNumberSuffix = getDayNumberSuffix(day);
            SimpleDateFormat displayDateFormat = new SimpleDateFormat(" MMMM yyyy | hh:mm a",Locale.US);
            tvDate.setText(/*getString(R.string.date) + " : " +*/ day+dayNumberSuffix+displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
            /* tvTime.setText(getString(R.string.time) + " : "*/
            //" | "+ displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));*/
            if(booking.getBookingEndtime().trim().isEmpty()||booking.getEventStartTime().trim().isEmpty())
            {
                tvslotTime.setText(Utility.converSecondToHourMinute(booking.getScheduleTime()));
            }else {
                tvslotTime.setText(Utility.converSecondToHourMinute(String.valueOf((Integer.valueOf(booking.getBookingEndtime())-Integer.valueOf(booking.getEventStartTime()))/60)));

            }
            if (getActivity().getIntent().getBooleanExtra("isFromChat", false)) {
                tvCustomerName.setText(booking.getCustomerData().getFirstName() + " " + booking.getCustomerData().getLastName());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView tvDiscountLabel = rootView.findViewById(R.id.tvDiscountLabel);
        TextView tvDiscount = rootView.findViewById(R.id.tvDiscount);
        TextView tvTotalLabel = rootView.findViewById(R.id.tvTotalLabel);
        TextView tvTotal = rootView.findViewById(R.id.tvTotal);
        TextView tvVisitFeeLabel = rootView.findViewById(R.id.tvVisitFeeLabel);
        TextView tvVisitFee = rootView.findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = rootView.findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = rootView.findViewById(R.id.tvLastDue);

        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontRegular);
        tvTotal.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);


        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            rootView.findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        /*if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            rootView.findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }*/

        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(String.valueOf(totalAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));


        LayoutInflater inflater = LayoutInflater.from(getActivity());
        ;

        if (!booking.getBookingModel().equals("3")) {
            rootView.findViewById(R.id.llServiceParent).setVisibility(View.VISIBLE);
            LinearLayout llService = rootView.findViewById(R.id.llService);
            if (booking.getServiceType().equals("1")) {
                for (ServiceItem serviceItem : booking.getCartData()) {
                    View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                    TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                    TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                    tvServiceFeeLabel.setTypeface(fontRegular);
                    tvServiceFee.setTypeface(fontRegular);
                    tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                    tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    llService.addView(serviceView);
                }
            }


            else if (!booking.getCallType().equals("1")&&!booking.getCallType().equals("3")) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            } else {
                /*
                 * Bug id : Trello 188
                 * Bug Description : Incall booking | For consultaning fee | it's showing service as 1 hr on going booking flow screen
                 * Fixed Description : Checked the call type if Call type is InCall then showing Consulting Fee
                 * Dev : Murashid
                 * Date : 29-11-2018
                 * */
                /*
                 *
                 * Bug Description : TeleCall booking | For consultaning fee | it's showing service as 1 hr on going booking flow screen
                 * Fixed Description : Checked the call type if Call type is InCall then showing Consulting Fee
                 * Dev : Yogesh
                 * Date : 15-05-2019
                 * */


                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(getString(R.string.consultingfee));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }

        if (booking.getQuestionAndAnswer() != null && booking.getQuestionAndAnswer().size() > 0) {
            LinearLayout llQuestionAnswer = rootView.findViewById(R.id.llQuestionAnswer);
            llQuestionAnswer.setVisibility(View.VISIBLE);

            for (QuestionAnswer questionAnswer : booking.getQuestionAndAnswer()) {
                View serviceView = inflater.inflate(R.layout.single_row_question_answer, null);
                TextView tvQuestion = serviceView.findViewById(R.id.tvQuestion);
                TextView tvAnswer = serviceView.findViewById(R.id.tvAnswer);
                tvQuestion.setTypeface(fontMedium);
                tvAnswer.setTypeface(fontRegular);
                tvQuestion.setText("Q. " + questionAnswer.getName());

                if (!questionAnswer.getQuestionType().equals("10")) {
                    tvAnswer.setText(questionAnswer.getAnswer());
                } else {
                    tvAnswer.setVisibility(View.GONE);
                    RecyclerView rvJobPhotosQuestion = serviceView.findViewById(R.id.rvJobPhotosQuestion);
                    rvJobPhotosQuestion.setVisibility(View.VISIBLE);
                    rvJobPhotosQuestion.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
                    ArrayList<String> imageUrls = new ArrayList<>();
                    imageUrls.addAll(Arrays.asList(questionAnswer.getAnswer().split(",")));
                    ArrayList<ImgUrls>jobimageUrls= new ArrayList<>();
                    if (imageUrls.size() > 0) {

                        for (String img : imageUrls) {
                            ImgUrls imgUrls = new ImgUrls(1, img);
                            jobimageUrls.add(imgUrls);
                        }
                    } else {
                        ImgUrls imgUrls = new ImgUrls(0, "");
                        jobimageUrls.add(imgUrls);
                    }
                    rvJobPhotosQuestion.setAdapter(new JobImageListAdapter(getActivity(), jobimageUrls));

                }
                llQuestionAnswer.addView(serviceView);
            }
        }

        if (isAcceptReject) {
            setTimer(tvRemainingTime, booking.getBookingRequestedForProvider(), booking.getBookingExpireForProvider(), booking.getCurrentTime());
            if (booking.getBookingType().equals("3")) {
                rootView.findViewById(R.id.llSchedule).setVisibility(View.VISIBLE);
                LinearLayout llSheduleDays = rootView.findViewById(R.id.llSheduleDays);
                tvDate.setVisibility(View.GONE);
                tvTime.setVisibility(View.GONE);
                tvTotalBillAmountLabel.setVisibility(View.GONE);
                tvTotalBillAmount.setVisibility(View.GONE);

                TextView tvHoursPerShiftLabel = rootView.findViewById(R.id.tvHoursPerShiftLabel);
                TextView tvHoursPerShift = rootView.findViewById(R.id.tvHoursPerShift);
                TextView tvCostPerShiftLabel = rootView.findViewById(R.id.tvCostPerShiftLabel);
                TextView tvCostPerShift = rootView.findViewById(R.id.tvCostPerShift);
                TextView tvTotalShiftLabel = rootView.findViewById(R.id.tvTotalShiftLabel);
                TextView tvTotalShift = rootView.findViewById(R.id.tvTotalShift);

                tvHoursPerShiftLabel.setTypeface(fontRegular);
                tvHoursPerShift.setTypeface(fontMedium);
                tvCostPerShiftLabel.setTypeface(fontRegular);
                tvCostPerShift.setTypeface(fontMedium);
                tvTotalShiftLabel.setTypeface(fontRegular);
                tvTotalShift.setTypeface(fontMedium);

                try {
                    tvScheduleStartTime.setText(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                    tvScheduleStartDate.setText(displayReapeatDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getEventStartTime()))));
                    tvScheduleEndTime.setText(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingEndtime()))));
                    tvScheduleEndDate.setText(displayReapeatDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingEndtime()))));
                    tvTotalNoOfShift.setText(booking.getAccounting().getTotalShiftBooking());
                    for (String days : booking.getDaysArr()) {
                        TextView tvDay = (TextView) inflater.inflate(R.layout.single_row_text_schedule_day, null);
                        tvDay.setText(days.substring(0, 1));
                        llSheduleDays.addView(tvDay);
                    }
                    double perShiftAmount, totalShiftAmount, perShiftHour;
                    int totalNoOfShift;
                    if (!booking.getBookingModel().equals("3")) {
                        perShiftAmount = Double.parseDouble(booking.getAccounting().getTotal()) - Double.parseDouble(booking.getAccounting().getLastDues());
                        perShiftHour = (Double.parseDouble(booking.getAccounting().getTotalActualJobTimeMinutes()) / 60);
                    } else {
                        perShiftAmount = Double.parseDouble(booking.getAccounting().getBidPrice());
                        perShiftHour = (Double.parseDouble(booking.getScheduleTime()) / 60);
                    }

                    totalNoOfShift = (int) Double.parseDouble(booking.getAccounting().getTotalShiftBooking());
                    totalShiftAmount = totalNoOfShift * perShiftAmount;

                    tvTotalShift.setText(Utility.getPrice(String.valueOf(totalShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    tvHoursPerShift.setText(String.valueOf(perShiftHour) + " " + getString(R.string.hrsCaps));
                    tvCostPerShift.setText(Utility.getPrice(String.valueOf(perShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            tvRemainingTime.setVisibility(View.GONE);
        }

        if (booking.getStatus().equals(VariableConstant.JOB_TIMER_STARTED)) {
            llAddress.setVisibility(View.GONE);
        }

        if (booking.getStatus().equals(VariableConstant.JOB_TIMER_COMPLETED)) {
//            rootView.findViewById(R.id.payment_ll).setVisibility(View.GONE);
            rootView.findViewById(R.id.llJobDescription).setVisibility(View.GONE);
        }
    }

    private void setTimer(final TextView tvRemainingTime, String bookingStartTime, String bookingEndTime, long current) {
        long start = Utility.convertUTCToTimeStamp(bookingStartTime);
        long end = Utility.convertUTCToTimeStamp(bookingEndTime);

        totalSecs = (end - start) / 1000;
        //totalSecs[0] for Maximum Progress
        totalSecs = (end - current) / 1000;
        hours = TimeUnit.SECONDS.toHours(totalSecs);
        minutes = TimeUnit.SECONDS.toMinutes(totalSecs) % 60;
        seconds = totalSecs % 60;

        tvRemainingTime.setText(String.format("%02d", hours) + " H : " + String.format("%02d", minutes) + " M : " + String.format("%02d", seconds) + " S");

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                totalSecs = totalSecs - 1;
                hours = TimeUnit.SECONDS.toHours(totalSecs);
                minutes = TimeUnit.SECONDS.toMinutes(totalSecs) % 60;
                seconds = totalSecs % 60;

                tvRemainingTime.setText(String.format("%02d", hours) + " H : " + String.format("%02d", minutes) + " M : " + String.format("%02d", seconds) + " S");

                handler.postDelayed(this, 1000);
            }
        };

        handler.post(runnable);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }
}
