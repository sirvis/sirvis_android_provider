package com.sirvis.pro.bookingflow.addPhotoActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.EventStartedCompletedActivity;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class CourierFlowAddPhotoActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, CourierAddImageListAdapter.NewItemClickListener, ImageSelectionUploadHandler.ImageSelectionListener {

    private CourierAddImageListAdapter courierAddImageListAdapter;
    private EditText etNotes;
    private ArrayList<DynamicImgaeUrl> dynamicImgaeUrls;
    private ArrayList<String> imageUrl;
    private ImageSelectionUploadHandler imageSelectionUploadHandler;
    private String imageneeded="";
    private RecyclerView rvJobPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier_flow_add_photo);

        initView();
    }

    private void initView() {
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.jobDescription));
        tvTitle.setTypeface(fontBold);

        TextView tvPhotosLabel = findViewById(R.id.tvPhotosLabel);
        TextView tvNotesLabel = findViewById(R.id.tvNotesLabel);
        TextView tvConfirm = findViewById(R.id.tvConfirm);
        etNotes = findViewById(R.id.etNotes);

        tvPhotosLabel.setTypeface(fontMedium);
        tvNotesLabel.setTypeface(fontMedium);
        tvConfirm.setTypeface(fontMedium);
        etNotes.setTypeface(Utility.getFontRegular(this));
        tvConfirm.setOnClickListener(this);

        dynamicImgaeUrls = new ArrayList<>();
        Intent intent = getIntent();
        imageUrl = intent.getStringArrayListExtra("imageUrl");
        etNotes.setText(intent.getStringExtra("notes"));
        imageneeded=intent.getStringExtra("imageneeded");
        for(String imageUrl : imageUrl)
        {
            DynamicImgaeUrl dynamicImgaeUrl = new DynamicImgaeUrl();
            dynamicImgaeUrl.setImageUrl(imageUrl);
            dynamicImgaeUrls.add(dynamicImgaeUrl);
        }
         rvJobPhotos = findViewById(R.id.rvJobPhotos);
        courierAddImageListAdapter = new CourierAddImageListAdapter(this, dynamicImgaeUrls, this );
        imageSelectionUploadHandler = new ImageSelectionUploadHandler(this, VariableConstant.DOCUMENTS, this );
        rvJobPhotos.setAdapter(courierAddImageListAdapter);
        rvJobPhotos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        finish();
        overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.tvConfirm:
                Intent intent=new Intent(this, EventStartedCompletedActivity.class);
                intent.putExtra("imageUrl", imageUrl);
                intent.putExtra("notes", etNotes.getText().toString());
                intent.putExtra("imageneeded",imageneeded);
                setResult(RESULT_OK,intent);
                closeActivity();
                break;
        }
    }

    @Override
    public void onAdd() {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            imageSelectionUploadHandler.openCamera(VariableConstant.DOCUMENT_PIC_DIR, VariableConstant.DOCUMENT_FILE_NAME+ System.currentTimeMillis() + ".png", false);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                    1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
        }
    }

    @Override
    public void remove(int position) {
        if(dynamicImgaeUrls!=null  && dynamicImgaeUrls.size()>position)
        {
            imageUrl.remove(position);
            dynamicImgaeUrls.clear();
            for(String imageUrl : imageUrl)
            {
                DynamicImgaeUrl dynamicImgaeUrl = new DynamicImgaeUrl();
                dynamicImgaeUrl.setImageUrl(imageUrl);
                dynamicImgaeUrls.add(dynamicImgaeUrl);
            }
            courierAddImageListAdapter=new CourierAddImageListAdapter(this, dynamicImgaeUrls, this );
            rvJobPhotos.setAdapter(courierAddImageListAdapter);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            imageSelectionUploadHandler.openCamera(VariableConstant.DOCUMENT_PIC_DIR, VariableConstant.DOCUMENT_FILE_NAME+ System.currentTimeMillis() + ".png", false);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageSelectionUploadHandler.onResult(requestCode, resultCode, data);
    }

    @Override
    public void loadImage(String path) {
        DynamicImgaeUrl dynamicImgaeUrl = new DynamicImgaeUrl();
        dynamicImgaeUrl.setImageUrl(path);
        dynamicImgaeUrl.setType(2);
        dynamicImgaeUrls.add(dynamicImgaeUrl);
        courierAddImageListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessImageUpload(String url) {
        imageUrl.add(url);
    }

}
