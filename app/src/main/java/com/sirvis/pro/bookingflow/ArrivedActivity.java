package com.sirvis.pro.bookingflow;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CancelListAdapter;
import com.sirvis.pro.bookingflow.review.CustomerReviewsActivity;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CalendarEventHelper;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.GoogleRoute;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;
import com.sirvis.pro.utility.WorkaroundMapFragment;

import org.json.JSONObject;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>ArrivedActivity</h1>
 * Activity for updating status
 */
public class ArrivedActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, OnMapReadyCallback, AppBarLayout.OnOffsetChangedListener, CancelListAdapter.CancelSelected {

    private static final String TAG = "ArrivedActivity";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ImageView ivUpDownButton;

    private SeekBar seekBar;
    private AppBarLayout appBarLayout;
    private boolean isExpanded = true;
    private CardView cvGoogleNavigation,cvWazeNavigation;

    private Typeface fontMedium;
    private Typeface fontBold;
    private String cancelId = "";

    private Booking booking;

    private double lat=0,lng=0 ;
    private LayoutInflater  inflater;

    private GoogleMap googleMap;
    private LatLng providerLatLng;
    //private Marker providerMarker

    private BroadcastReceiver receiver;

    private FloatingActionButton fabCommunicate,fabCall,fabMsg;
    private Animation fade_open, fade_close,rotate_forward,rotate_backward;
    private boolean isFabOpen = false;
    private TextView tvMessageCountInPlus, tvMessageCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrived);

        initView();
    }

    @SuppressLint("SetTextI18n")
    private void initView()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatusArrived));
        progressDialog.setCancelable(false);
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setTypeface(fontMedium);
        tvCancel.setOnClickListener(this);

        ivUpDownButton = findViewById(R.id.ivUpDownButton);


        TextView tvSeekbarText = findViewById(R.id.tvSeekbarText);
        TextView tvGoogleNavigation = findViewById(R.id.tvGoogleNavigation);
        TextView tvWazeNavigation = findViewById(R.id.tvWazeNavigation);
        seekBar = findViewById(R.id.seekBar);

        tvSeekbarText.setTypeface(fontBold);
        tvGoogleNavigation.setTypeface(fontRegular);
        tvWazeNavigation.setTypeface(fontRegular);

        tvMessageCountInPlus = findViewById(R.id.tvMessageCountInPlus);
        tvMessageCount = findViewById(R.id.tvMessageCount);
        tvMessageCountInPlus.setTypeface(fontRegular);
        tvMessageCount.setTypeface(fontRegular);

        appBarLayout = findViewById(R.id.appBarLayout);
        final AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        WorkaroundMapFragment sMapFrag = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        // Getting GoogleMap object from the fragment
        sMapFrag.getMapAsync(this);
        sMapFrag.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                appBarLayout.requestDisallowInterceptTouchEvent(true);
            }
        });

        cvGoogleNavigation = findViewById(R.id.cvGoogleNavigation);
        cvWazeNavigation = findViewById(R.id.cvWazeNavigation);

        RelativeLayout rlUpDownButton = findViewById(R.id.rlUpDownButton);
        ImageView ivLocation = findViewById(R.id.ivLocation);

        appBarLayout.addOnOffsetChangedListener(this);
        ivLocation.setOnClickListener(this);
        rlUpDownButton.setOnClickListener(this);
        cvGoogleNavigation.setOnClickListener(this);
        cvWazeNavigation.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");

        getSupportFragmentManager().beginTransaction().replace(R.id.fragBooking, BookingDetailsFragment.newInstance(booking,false)).commit();

        if(!booking.getLatitude().equals(""))
        {
            lat = Double.parseDouble(booking.getLatitude());
            lng = Double.parseDouble(booking.getLongitude());
        }

        CollapsingToolbarLayout ctlMyBooking = findViewById(R.id.ctlMyBooking);

        ctlMyBooking.setTitle(getText(R.string.jobId)+" "+booking.getBookingId());
        ctlMyBooking.setCollapsedTitleTextAppearance(R.style.TextCollapsedEventId);
        ctlMyBooking.setExpandedTitleTextAppearance(R.style.TextExpandHide);
        ctlMyBooking.setCollapsedTitleGravity(Gravity.CENTER);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75)
                {
                    progressDialog.setMessage(getString(R.string.updatingStatusArrived));
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("bookingId",booking.getBookingId());
                        jsonObject.put("status",VariableConstant.ARRIVED);
                        jsonObject.put("latitude",sessionManager.getCurrentLat());
                        jsonObject.put("longitude",sessionManager.getCurrentLng());
                        jsonObject.put("distance",""+sessionManager.getArrivedDistance(booking.getBookingId()));
                        Log.d(TAG, "initView: "+jsonObject);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject,false);
                }
                else
                {
                    seekBar.setProgress(0);
                }
            }
        });

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT))
                {
                    setChatCount();
                }
                else
                {
                    String cancelId = intent.getStringExtra("cancelid");
                    String header = intent.getStringExtra("header");
                    String msg = intent.getStringExtra("msg");
                    if (cancelId.equals(booking.getBookingId()))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        //DialogHelper.customAlertDialogCloseActivity(ArrivedActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);


        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(this,R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(this,R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(this,R.anim.rotate_left_to_center);

        fabCommunicate = findViewById(R.id.fabCommunicate);
        fabCall = findViewById(R.id.fabCall);
        fabMsg = findViewById(R.id.fabMsg);

        fabCommunicate.setOnClickListener(this);
        fabMsg.setOnClickListener(this);
        fabCall.setOnClickListener(this);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setChatCount();
    }

    private void setChatCount() {
        if(sessionManager.getChatCount(booking.getBookingId()) != 0)
        {
            tvMessageCountInPlus.setText(""+sessionManager.getChatCount(booking.getBookingId()));
            tvMessageCount.setText(""+sessionManager.getChatCount(booking.getBookingId()));
            if(isFabOpen)
            {
                tvMessageCount.setVisibility(View.VISIBLE);
                tvMessageCountInPlus.setVisibility(View.GONE);
            }
            else
            {
                tvMessageCountInPlus.setVisibility(View.VISIBLE);
                tvMessageCount.setVisibility(View.GONE);
            }
        }
        else
        {
            tvMessageCountInPlus.setVisibility(View.GONE);
            tvMessageCount.setVisibility(View.GONE);
        }
    }



    @Override
    protected void onDestroy() {

        unregisterReceiver(receiver);

        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        finish();

        sessionManager.setBookingStr(presenter.getBookingStr(booking,sessionManager.getBookingStr(),VariableConstant.ARRIVED));

        Intent intent = new Intent(this,EventStartedCompletedActivity.class);
        Bundle bundle = new Bundle();
        booking.setStatus(VariableConstant.ARRIVED);
        bundle.putSerializable("booking",booking);
        intent.putExtras(bundle);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }

        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.Arrived.value,booking.getBookingId());

    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cancel,null);
        alertDialogBuilder.setView(view);
        TextView tvTitle= view.findViewById(R.id.tvTitle);
        TextView tvSubmit= view.findViewById(R.id.tvSubmit);
        ImageView ivClose  = view.findViewById(R.id.ivClose);

        tvTitle.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
        rvCancel.setLayoutManager(new LinearLayoutManager(this));
        CancelListAdapter cancelListAdapter = new CancelListAdapter(this,cancelPojo.getData(),this);
        rvCancel.setAdapter(cancelListAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cancelId.equals(""))
                {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("bookingId",booking.getBookingId());
                        jsonObject.put("resonId",cancelId);
                        progressDialog.setMessage(getString(R.string.loading));
                        presenter.cancelBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
                        alertDialog.dismiss();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(ArrivedActivity.this,getString(R.string.plsSelectCancel),Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelId ="";
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.show();

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.alert),msg,getString(R.string.oK));
        try {
            if(booking.getReminderId() !=null && !booking.getReminderId().equals(""))
            {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(booking.getReminderId());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvCancel:
                progressDialog.setMessage(getString(R.string.loading));
                presenter.getCancelReason(booking.getBookingId());
                break;

            case R.id.llCutomerDetails:
                Intent cutomerReviewIntent = new Intent(this,CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(cutomerReviewIntent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else {
                    startActivity(cutomerReviewIntent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.rlUpDownButton:
                if(!isExpanded)
                {
                    isExpanded = true;
                    ivUpDownButton.setImageResource(R.drawable.arrived_up_arrow_icon);
                    appBarLayout.setExpanded(true,true);
                    cvGoogleNavigation.setVisibility(View.VISIBLE);
                    cvWazeNavigation.setVisibility(View.VISIBLE);
                }
                else
                {
                    isExpanded = false;
                    ivUpDownButton.setImageResource(R.drawable.arrived_down_arrow_icon);
                    appBarLayout.setExpanded(false,true);
                    cvGoogleNavigation.setVisibility(View.GONE);
                    cvWazeNavigation.setVisibility(View.GONE);
                }
                break;

            case R.id.cvGoogleNavigation:
                String muri = "google.navigation:q=" + lat + "," + lng;
                Intent  googleIntent= new Intent(Intent.ACTION_VIEW, Uri.parse(muri));
                googleIntent.setPackage("com.google.android.apps.maps");
                startActivity(googleIntent);
                break;

            case R.id.cvWazeNavigation:
                try {
                    String url = "waze://?ll=" + lat + "," + lng + "&navigate=yes";
                    Intent wazeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(wazeIntent);
                } catch (ActivityNotFoundException ex) {
                    Intent intent =
                            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
                    startActivity(intent);
                }

            case R.id.ivLocation:
                if(googleMap != null)
                {
                    LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng()));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
                break;

            case R.id.fabCommunicate:
                animateFAB();
                break;

            case R.id.fabCall:

                animateFAB();
                String uri = "tel:"+booking.getPhone() ;
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
                break;

            case R.id.fabMsg:
                animateFAB();
                sessionManager.setChatBookingID(booking.getBookingId());
                sessionManager.setChatCustomerName(booking.getFirstName() + " "+booking.getLastName());
                sessionManager.setChatCustomerPic(booking.getProfilePic());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this,ChattingActivity.class);
                startActivity(chatIntent);
                break;

        }
    }

    public void animateFAB(){

        if(isFabOpen){
            isFabOpen = false;
            fabCommunicate.startAnimation(rotate_backward);
            fabMsg.startAnimation(fade_close);
            fabCall.startAnimation(fade_close);
            fabMsg.setClickable(false);
            fabCall.setClickable(false);
            setChatCount();
        }
        else
        {
            isFabOpen = true;
            fabCommunicate.startAnimation(rotate_forward);
            fabMsg.startAnimation(fade_open);
            fabCall.startAnimation(fade_open);
            fabMsg.setClickable(true);
            fabCall.setClickable(true);
            setChatCount();
        }
    }


    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        LatLng customerLatLng = new LatLng(lat,lng);
        providerLatLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng()));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(customerLatLng, 15));

        googleMap.addMarker(new MarkerOptions().position(customerLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_blue_dot_icon)));
        googleMap.getUiSettings().setCompassEnabled(false);
        String url = GoogleRoute.makeURL(providerLatLng.latitude,providerLatLng.longitude,customerLatLng.latitude,customerLatLng.longitude);
        GoogleRoute.startPlotting(googleMap,url);

        final View customerMarkerView = inflater.inflate(R.layout.custom_marker, null);
        final ImageView ivProfile = customerMarkerView.findViewById(R.id.ivProfile);
        final Bitmap customerMarker = Utility.createDrawableFromView(ArrivedActivity.this,customerMarkerView);
        final Marker marker = googleMap.addMarker(new MarkerOptions().position(providerLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(customerMarker))
                .flat(false));

        Glide.with(this).setDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.profile_default_image)
                .transform(new CircleTransform(this))
                .error(R.drawable.profile_default_image))
                .asBitmap() .load(booking.getProfilePic())
                .into(new BitmapImageViewTarget(ivProfile) {
                    @Override
                    public void onResourceReady(Bitmap  drawable, Transition anim) {
                        super.onResourceReady(drawable, anim);
                        ivProfile.setImageBitmap(drawable);
                        Bitmap customerMarker = Utility.createDrawableFromView(ArrivedActivity.this,customerMarkerView);
                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(customerMarker));
                    }
                });

        final LatLngBounds bounds = new LatLngBounds.Builder()
                .include(providerLatLng)
                .include(customerLatLng)
                .build();

        final int padding = getResources().getDimensionPixelSize(R.dimen.dimen_45dp);

        this.googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds,padding));
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (verticalOffset == 0)
        {
            cvGoogleNavigation.setVisibility(View.VISIBLE);
            cvWazeNavigation.setVisibility(View.VISIBLE);
            isExpanded = true;
            ivUpDownButton.setImageResource(R.drawable.arrived_up_arrow_icon);
        }
        else
        {
            cvGoogleNavigation.setVisibility(View.GONE);
            cvWazeNavigation.setVisibility(View.GONE);
            isExpanded = false;
            ivUpDownButton.setImageResource(R.drawable.arrived_down_arrow_icon);
        }
    }

    @Override
    public void onCancelSeleted(String id) {
        cancelId = id;
    }
}
