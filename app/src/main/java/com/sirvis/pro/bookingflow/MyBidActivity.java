package com.sirvis.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DecimalDigitsInputFilter;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

public class MyBidActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, View.OnFocusChangeListener, CompoundButton.OnCheckedChangeListener {

    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private EditText etPrice, etMessage;
    private EditText etPriceTotal, etShiftHourPrice;
    private TextView tvNoOfHourShift;
    private RelativeLayout rlPerShiftHour;
    private LinearLayout llTotalShift;
    private NestedScrollView nsvMyBid;

    private int totalNoOfShift = 0;
    private double totalNoOfHour= 0, perShiftHour = 0;


    private String bookidId ="";
    private Booking booking;

    private BroadcastReceiver receiver;

    private int type = 1; // 1 => for single shift, 2 = > per shift, 3 => per hour , 4 => total

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bid);

        initView();
    }

    /**
     * initialize the views
     */
    @SuppressLint("SetTextI18n")
    private void initView()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.myBid));
        tvTitle.setTypeface(fontBold);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        bookidId = booking.getBookingId();

        TextView tvEnterBidLabel = findViewById(R.id.tvEnterBidLabel);
        TextView tvCurrencySymbol = findViewById(R.id.tvCurrencySymbol);
        TextView tvCurrencySymbolSuffix = findViewById(R.id.tvCurrencySymbolSuffix);
        etPrice = findViewById(R.id.etPrice);
        etPrice.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        etMessage = findViewById(R.id.etMessage);
        TextView tvSubmit = findViewById(R.id.tvSubmit);


        tvEnterBidLabel.setTypeface(fontBold);
        tvCurrencySymbol.setTypeface(fontRegular);
        tvCurrencySymbolSuffix.setTypeface(fontRegular);
        etPrice.setTypeface(fontRegular);
        etMessage.setTypeface(fontRegular);
        tvSubmit.setTypeface(fontRegular);

        tvSubmit.setOnClickListener(this);

        tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
        tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());

        if(sessionManager.getCurrencyAbbrevation().equals("2"))
        {
            tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
            tvCurrencySymbol.setVisibility(View.GONE);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("cancelid"))
                {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(booking.getBookingId()))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        //DialogHelper.customAlertDialogCloseActivity(MyBidActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);

        etPrice.requestFocus();
        etPrice.setOnFocusChangeListener(this);

        if(booking.getBookingType().equals("3"))
        {
            type = 2 ;
            findViewById(R.id.llMultipleShift).setVisibility(View.VISIBLE);
            findViewById(R.id.llSingleShift).setVisibility(View.GONE);

            TextView tvHoursPerShiftLabel = findViewById(R.id.tvHoursPerShiftLabel);
            TextView tvHoursPerShift = findViewById(R.id.tvHoursPerShift);
            TextView tvCostPerShiftLabel = findViewById(R.id.tvCostPerShiftLabel);
            TextView tvCostPerShift = findViewById(R.id.tvCostPerShift);
            TextView tvTotalShiftLabel = findViewById(R.id.tvTotalShiftLabel);
            TextView tvTotalShift = findViewById(R.id.tvTotalShift);

            TextView tvEnterBidLabelMultiple = findViewById(R.id.tvEnterBidLabelMultiple);
            RadioButton rbPerShift = findViewById(R.id.rbPerShift);
            RadioButton rbPerHour = findViewById(R.id.rbPerHour);
            RadioButton rbTotal = findViewById(R.id.rbTotal);

            tvHoursPerShiftLabel.setTypeface(fontRegular);
            tvHoursPerShift.setTypeface(fontMedium);
            tvCostPerShiftLabel.setTypeface(fontRegular);
            tvCostPerShift.setTypeface(fontMedium);
            tvTotalShiftLabel.setTypeface(fontRegular);
            tvTotalShift.setTypeface(fontMedium);

            tvEnterBidLabelMultiple.setTypeface(fontMedium);
            rbPerShift.setTypeface(fontRegular);
            rbPerHour.setTypeface(fontRegular);
            rbTotal.setTypeface(fontRegular);

            rbPerShift.setOnCheckedChangeListener(this);
            rbPerHour.setOnCheckedChangeListener(this);
            rbTotal.setOnCheckedChangeListener(this);

            TextView tvTotalCurrencySymbol = findViewById(R.id.tvTotalCurrencySymbol);
            TextView tvTotalCurrencySymbolSuffix = findViewById(R.id.tvTotalCurrencySymbolSuffix);
            TextView tvPerCurrencySymbol = findViewById(R.id.tvPerCurrencySymbol);
            TextView tvPerCurrencySymbolSuffix = findViewById(R.id.tvPerCurrencySymbolSuffix);

            rlPerShiftHour = findViewById(R.id.rlPerShiftHour);
            llTotalShift = findViewById(R.id.llTotalShift);
            nsvMyBid = findViewById(R.id.nsvMyBid);

            tvTotalCurrencySymbol.setTypeface(fontRegular);
            tvTotalCurrencySymbolSuffix.setTypeface(fontRegular);
            tvPerCurrencySymbol.setTypeface(fontRegular);
            tvPerCurrencySymbolSuffix.setTypeface(fontRegular);

            tvTotalCurrencySymbol.setText(sessionManager.getCurrencySymbol());
            tvTotalCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());
            tvPerCurrencySymbol.setText(sessionManager.getCurrencySymbol());
            tvPerCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());

            if(sessionManager.getCurrencyAbbrevation().equals("2"))
            {
                tvTotalCurrencySymbolSuffix.setVisibility(View.VISIBLE);
                tvPerCurrencySymbolSuffix.setVisibility(View.VISIBLE);
                tvTotalCurrencySymbol.setVisibility(View.GONE);
                tvPerCurrencySymbolSuffix.setVisibility(View.GONE);
            }

            etPriceTotal = findViewById(R.id.etPriceTotal);
            etShiftHourPrice = findViewById(R.id.etShiftHourPrice);
            tvNoOfHourShift = findViewById(R.id.tvNoOfHourShift);

            etPriceTotal.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
            etShiftHourPrice.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});

            etPriceTotal.setTypeface(fontRegular);
            etShiftHourPrice.setTypeface(fontRegular);
            tvNoOfHourShift.setTypeface(fontRegular);

            etPriceTotal.setOnFocusChangeListener(this);
            etShiftHourPrice.setOnFocusChangeListener(this);
            rbPerShift.setChecked(true);
            try {
                etShiftHourPrice.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if(!editable.toString().equals(""))
                        {
                            double singlePrice = Double.parseDouble(editable.toString());
                            double totalPrice = 0;
                            if(type == 2)
                            {
                                totalPrice = singlePrice * totalNoOfShift;
                                etPriceTotal.setText(Utility.getFormattedPrice(String.valueOf(totalPrice)));
                            }
                            else if(type == 3)
                            {
                                totalPrice = singlePrice * totalNoOfHour;
                                etPriceTotal.setText(Utility.getFormattedPrice(String.valueOf(totalPrice)));
                            }
                        }
                        else
                        {
                            etPriceTotal.setText("0");
                        }
                    }
                });

                double perShiftAmount, totalShiftAmount, perHourAmount;
                if(!booking.getBookingModel().equals("3"))
                {
                    perShiftAmount = Double.parseDouble(booking.getAccounting().getTotal()) -Double.parseDouble(booking.getAccounting().getLastDues());
                    perShiftHour = (Double.parseDouble(booking.getAccounting().getTotalActualJobTimeMinutes())/60) ;
                }
                else
                {
                    perShiftAmount = Double.parseDouble(booking.getAccounting().getBidPrice());
                    perShiftHour = (Double.parseDouble(booking.getScheduleTime())/60) ;
                }

                totalNoOfShift = (int) Double.parseDouble(booking.getAccounting().getTotalShiftBooking());
                totalNoOfHour = totalNoOfShift * perShiftHour;

                perHourAmount = perShiftAmount / perShiftHour;
                totalShiftAmount = totalNoOfShift * perShiftAmount;

                tvTotalShift.setText(Utility.getPrice(String.valueOf(totalShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                tvHoursPerShift.setText(String.valueOf(perShiftHour) +" "+ getString(R.string.hrsCaps));
                tvCostPerShift.setText(Utility.getPrice(String.valueOf(perShiftAmount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

                etShiftHourPrice.requestFocus();
                showKeyboard(etShiftHourPrice);

                tvNoOfHourShift.setText( " X "+String.valueOf(totalNoOfShift));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else
        {
            Utility.showKeyBoard(this,etPrice);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvSubmit:
                try {

                    double price = getPrice();

                    Log.d("Priceeeeeeeee", "onClick: "+price);

                    if(price == 0)
                    {
                        Toast.makeText(this,getString(R.string.plsEnterPrice),Toast.LENGTH_SHORT).show();
                        return;
                    }

                    JSONObject acceptJsonObject = new JSONObject();
                    acceptJsonObject.put("bookingId",bookidId);
                    acceptJsonObject.put("status",VariableConstant.ACCEPT);
                    acceptJsonObject.put("latitude",sessionManager.getCurrentLat());
                    acceptJsonObject.put("longitude",sessionManager.getCurrentLng());
                    acceptJsonObject.put("quotedPrice",String.valueOf(price));
                    acceptJsonObject.put("bidDescription",etMessage.getText().toString());

                    VariableConstant.IS_BOOKING_UPDATED = true;
                    VariableConstant.IS_BID_BOOKING_ACCEPTED = true;

                    progressDialog.setMessage(getString(R.string.updatingStatusForBidding));
                    presenter.acceptRejectJob(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),acceptJsonObject);

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {

        switch (view.getId())
        {
            case R.id.etPrice:
                if(!b)
                {
                    etPrice.setText(Utility.getFormattedPrice(etPrice.getText().toString()));
                }
                else
                {
                    etPrice.setSelection(etPrice.getText().length());
                }
                break;

            case R.id.etPriceTotal:
                if(!b)
                {
                    etPriceTotal.setText(Utility.getFormattedPrice(etPriceTotal.getText().toString()));
                }
                else
                {
                    if(etPriceTotal.getText().length() > 3)
                    {
                        etPriceTotal.setSelection(etPriceTotal.getText().length()-3);
                        Utility.showKeyBoard(this,etPriceTotal);
                    }
                }
                break;


            case R.id.etShiftHourPrice:
                if(!b)
                {
                    etShiftHourPrice.setText(Utility.getFormattedPrice(etShiftHourPrice.getText().toString()));
                }
                else
                {
                    etShiftHourPrice.setSelection(etShiftHourPrice.getText().length());
                }
                break;

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
        {
            switch (buttonView.getId())
            {
                case R.id.rbPerShift:
                    type = 2;
                    if(!etPriceTotal.getText().toString().equals(""))
                    {
                        double total = Double.parseDouble(etPriceTotal.getText().toString());
                        double singleValue = total / totalNoOfShift;
                        etShiftHourPrice.setText(Utility.getFormattedPrice(String.valueOf(singleValue)));
                    }
                    rlPerShiftHour.setVisibility(View.VISIBLE);
                    tvNoOfHourShift.setText( " X "+String.valueOf(totalNoOfShift));
                    etPriceTotal.setEnabled(false);
                    showKeyboard(etShiftHourPrice);
                    llTotalShift.setBackground(null);
                    break;

                case R.id.rbPerHour:
                    type = 3;
                    if(!etPriceTotal.getText().toString().equals(""))
                    {
                        double total = Double.parseDouble(etPriceTotal.getText().toString());
                        double singleValue = total / totalNoOfHour;
                        etShiftHourPrice.setText(Utility.getFormattedPrice(String.valueOf(singleValue)));
                    }
                    rlPerShiftHour.setVisibility(View.VISIBLE);
                    tvNoOfHourShift.setText( " X "+String.valueOf(totalNoOfHour));
                    etPriceTotal.setEnabled(false);
                    showKeyboard(etShiftHourPrice);
                    llTotalShift.setBackground(null);
                    break;

                case R.id.rbTotal:
                    type = 4;
                    rlPerShiftHour.setVisibility(View.GONE);
                    etPriceTotal.setEnabled(true);
                    showKeyboard(etPriceTotal);
                    llTotalShift.setBackground(ContextCompat.getDrawable(this,R.drawable.rectangle_corner_white_background_with_grey_stroke));
                    break;
            }
        }
    }

    private void showKeyboard(final EditText editText) {
        try {
            nsvMyBid.postDelayed(new Runnable() {
                @Override
                public void run() {
                    editText.requestFocus();
                    nsvMyBid.scrollTo(0, nsvMyBid.getBottom());
                    Utility.showKeyBoard(MyBidActivity.this, editText);
                }
            }, 400);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private double getPrice() {
        double price = 0;
        try {
            switch (type)
            {
                case 1:
                    price = Double.parseDouble(etPrice.getText().toString());
                    break;

                case 2:
                    price = Double.parseDouble(etShiftHourPrice.getText().toString());
                    break;

                case 3:
                case 4:
                    price = Double.parseDouble(etPriceTotal.getText().toString()) / totalNoOfShift;
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
       return price;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        finish();
        overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        Intent intent=new Intent(this,AcceptRejectActivity.class);
        setResult(RESULT_OK,intent);
        closeActivity();
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.alert),msg,getString(R.string.oK));
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {

    }


}
