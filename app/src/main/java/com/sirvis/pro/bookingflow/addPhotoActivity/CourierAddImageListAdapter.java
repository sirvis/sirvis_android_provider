package com.sirvis.pro.bookingflow.addPhotoActivity;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sirvis.pro.R;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by murashid on 20/5/18.
 */
public class CourierAddImageListAdapter extends RecyclerView.Adapter<CourierAddImageListAdapter.ViewHolder> {

    private ArrayList<DynamicImgaeUrl> dynamicImgaeUrls;
    private Activity mAcitvity;
    private NewItemClickListener newItemClickListener;

    public CourierAddImageListAdapter(Activity mAcitvity, ArrayList<DynamicImgaeUrl> dynamicImgaeUrls, NewItemClickListener newItemClickListener) {
        this.mAcitvity = mAcitvity;
        this.dynamicImgaeUrls = dynamicImgaeUrls;
        this.newItemClickListener = newItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_image_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if (position != dynamicImgaeUrls.size()) {
            holder.delete_photo.setVisibility(View.VISIBLE);
            holder.delete_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newItemClickListener.remove(position);
                }
            });
            if (dynamicImgaeUrls.get(position).getType() == 1) {
                Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions()
                        .error(R.drawable.vector_app_logo_profile)
                        .placeholder(R.drawable.vector_app_logo_profile)).
                        load(dynamicImgaeUrls.get(position).getImageUrl())
                        .into(holder.ivJobPhoto);
            } else {
                Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions().
                        error(R.drawable.vector_app_logo_profile)
                        .placeholder(R.drawable.vector_app_logo_profile))
                        .load(new File(dynamicImgaeUrls.get(position).getImageUrl()))
                        .into(holder.ivJobPhoto);
            }
        } else {
            holder.ivJobPhoto.setImageResource(R.drawable.take_photo_deafult_image);
        }
    }

    @Override
    public int getItemCount() {
        return dynamicImgaeUrls.size() + 1;
    }

    public interface NewItemClickListener {
        void onAdd();

        void remove(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivJobPhoto, delete_photo;

        public ViewHolder(View itemView) {
            super(itemView);
            ivJobPhoto = (ImageView) itemView.findViewById(R.id.ivJobPhoto);
            delete_photo = (ImageView) itemView.findViewById(R.id.delete_photo);

            ivJobPhoto.setOnClickListener(this);
//            delete_photo.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.delete_photo) {
                //delete the selected item from the adapter
               // removeAt(getAdapterPosition());
            } else {
                if (getAdapterPosition() == dynamicImgaeUrls.size()) {
                    newItemClickListener.onAdd();
                }
            }

        }
    }

    private void removeAt(int position) {

       /* dynamicImgaeUrls.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dynamicImgaeUrls.size());
        notii*/
    }
}
