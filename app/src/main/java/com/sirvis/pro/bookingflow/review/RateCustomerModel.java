package com.sirvis.pro.bookingflow.review;

import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 06-Nov-17.
 * Model for RaterCustomerActivity
 *
 * @see RateCustomerActivity
 */

public class RateCustomerModel {
    private static final String TAG = "ReviewModel";
    private RateCustomerModelImple modelImplement;

    RateCustomerModel(RateCustomerModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * /**
     * method for calinng api for geting details for particular booking
     *
     * @param sessiontoken session Token
     * @param bookingId    booking id
     */
    void getBookingById(String sessiontoken, final String bookingId) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.BOOKING + "/" + bookingId,
                OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);

                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    modelImplement.onSuccess(jsonObject.getString("data"), true, true);
                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE)) {
                                    RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getBookingById(newToken, bookingId);
                                            modelImplement.onNewToken(newToken);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            modelImplement.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            modelImplement.sessionExpired(msg);
                                        }
                                    });

                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                                    modelImplement.sessionExpired(jsonObject.getString("message"));
                                } else {
                                    modelImplement.onFailure(jsonObject.getString("message"));
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * method for calling api for set the customer reviews based on the page index and customer details
     *
     * @param sessiontoken session Token
     */
    void setCustomerRating(String sessiontoken, JSONObject jsonObject) {
        try {
          /*  if(jsonObject.getString("review").equals(""))
            {
                modelImplement.onEmptyReview();
                return;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.REVIEW_AND_RATING, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                        JSONObject jsonResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            modelImplement.onSuccess(jsonResponse.getString("message"), false, false);
                        } else {
                            modelImplement.onFailure(jsonResponse.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ReviewModelImple interface for Presenter implementation
     */
    interface RateCustomerModelImple {
        void onFailure(String failureMsg);

        void onFailure();

        void onEmptyReview();

        void onSuccess(String msg, boolean isFromNotification, boolean isPageRefresh);

        void onNewToken(String newToken);

        void sessionExpired(String msg);
    }
}
