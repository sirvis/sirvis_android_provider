package com.sirvis.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CancelListAdapter;
import com.sirvis.pro.bookingflow.addPhotoActivity.CourierFlowAddPhotoActivity;
import com.sirvis.pro.bookingflow.review.CustomerReviewsActivity;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.BookingTimer;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CalendarEventHelper;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>EventStartedCompletedActivity</h1>
 * Activity for starting and enting the event time
 */
public class EventStartedCompletedActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, CancelListAdapter.CancelSelected {

    private static final String TAG = "EventStarted";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private TextView tvStartPauseTimer, tvTimer, tvStatus;
    private TextView tvSeekbarText;
    private SeekBar seekBar;

    private long timerSecond = 0;
    private Handler handler;
    private Runnable myRunnable;
    boolean runTimer = false, isRunning = false;

    private Booking booking;
    private BookingTimer bookingTimer;

    private String bookidId = "", timerStatus = "0", updatingStatus = "";
    private String cancelId = "";
    private Typeface fontBold;
    private Typeface fontMedium;

    private BroadcastReceiver receiver;

    private int normalTextColor, lightestTextColor;

    private double hourFee = 0;

    private FloatingActionButton fabCommunicate, fabCall, fabMsg , fabPhotes;
    private Animation fade_open, fade_close, rotate_forward, rotate_backward;
    private boolean isFabOpen = false;
    private TextView tvMessageCountInPlus, tvMessageCount;
    private BookingDetailsFragment bookingDetailsFragment;
    private ArrayList startimageUrl, endimageUrl;
    private boolean imageStartNeed = true, imagesEndNeed = true;
    private String pickupNotes = "", dropupNotes = "";
    private boolean isTimerStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_started_completed);

        initView();
    }


    @SuppressLint("SetTextI18n")
    private void initView() {
        normalTextColor = ContextCompat.getColor(EventStartedCompletedActivity.this, R.color.normalTextColor);
        lightestTextColor = ContextCompat.getColor(EventStartedCompletedActivity.this, R.color.lightestTextColor);
        startimageUrl = new ArrayList();
        endimageUrl = new ArrayList();
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatus));
        progressDialog.setCancelable(false);

        fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontLight = Utility.getFontLight(this);

        TextView tvBookingTime = findViewById(R.id.tvBookingTime);
        tvTimer = findViewById(R.id.tvTimer);
        TextView tvHour = findViewById(R.id.tvHour);
        TextView tvMinute = findViewById(R.id.tvMinute);
        TextView tvSecond = findViewById(R.id.tvSecond);
        tvStartPauseTimer = findViewById(R.id.tvStartPauseTimer);
        tvSeekbarText = findViewById(R.id.tvSeekbarText);
        TextView tveventId = findViewById(R.id.tveventId);
        tvStatus = findViewById(R.id.tvStatus);
        seekBar = findViewById(R.id.seekBar);


        tveventId.setTypeface(fontMedium);
        tvStatus.setTypeface(fontMedium);
        tvBookingTime.setTypeface(fontMedium);
        tvTimer.setTypeface(fontLight);
        tvHour.setTypeface(fontRegular);
        tvMinute.setTypeface(fontRegular);
        tvSecond.setTypeface(fontRegular);
        tvStartPauseTimer.setTypeface(fontBold);
        tvSeekbarText.setTypeface(fontBold);

        tvMessageCountInPlus = findViewById(R.id.tvMessageCountInPlus);
        tvMessageCount = findViewById(R.id.tvMessageCount);
        tvMessageCountInPlus.setTypeface(fontRegular);
        tvMessageCount.setTypeface(fontRegular);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivBackButton.setOnClickListener(this);
        tvStartPauseTimer.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        bookingTimer = booking.getBookingTimer();

        bookingDetailsFragment = BookingDetailsFragment.newInstance(booking, false);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragBooking, bookingDetailsFragment).commit();
        bookidId = booking.getBookingId();

        if (booking.getServiceType().equals("2")) {
            hourFee = Double.parseDouble(booking.getAccounting().getTotalActualHourFee());
        }

        initTimer();
        if (booking.getStatus().equals(VariableConstant.ARRIVED)) {
            progressDialog.setMessage(getString(R.string.updatingStatusEventStarted));
            updatingStatus = VariableConstant.JOB_TIMER_STARTED;
            timerStatus = VariableConstant.TIMER_STARTED;
            tvTimer.setText(presenter.getDurationString(timerSecond));

            tvStatus.setText(getString(R.string.Arrived));
        } else {
            progressDialog.setMessage(getString(R.string.updatingStatusEventCompleted));
            updatingStatus = VariableConstant.JOB_TIMER_COMPLETED;
            timerStatus = VariableConstant.TIMER_PAUSED;
            jobStarted(true);

            tvStatus.setText(getString(R.string.eventStarted));
        }
        tveventId.setText(getText(R.string.jobId) + " " + booking.getBookingId());


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


                if (seekBar.getProgress() > 75) {
                    updateBookingStatus();
                    isTimerStarted = true;
                } else {
                    seekBar.setProgress(0);
                }





               /* if (seekBar.getProgress() > 75)
                {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject jsonObjectTimer = new JSONObject();
                    try {

                        jsonObject.put("bookingId",bookidId);
                        jsonObject.put("status", updatingStatus);
                        jsonObject.put("latitude",sessionManager.getCurrentLat());
                        jsonObject.put("longitude",sessionManager.getCurrentLng());

                        if(updatingStatus.equals(VariableConstant.JOB_TIMER_COMPLETED))
                        {
                            jsonObject.put("distance",""+sessionManager.getOnJobDistance(booking.getBookingId()));
                            jsonObject.put("second",timerSecond);
                        }
                        Log.d(TAG, "c: "+jsonObject);

                        jsonObjectTimer.put("bookingId",bookidId);
                        jsonObjectTimer.put("status",timerStatus);
                        jsonObjectTimer.put("second", timerSecond);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject, jsonObjectTimer);
                }
                else
                {
                    seekBar.setProgress(0);
                }*/
            }
        });


        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT)) {
                    setChatCount();
                } else {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(booking.getBookingId())) {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        // DialogHelper.customAlertDialogCloseActivity(EventStartedCompletedActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);

        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(this, R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(this, R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(this, R.anim.rotate_left_to_center);

        fabCommunicate = findViewById(R.id.fabCommunicate);
        fabCall = findViewById(R.id.fabCall);
        fabMsg = findViewById(R.id.fabMsg);
        fabPhotes = findViewById(R.id.fabPhotes);

        fabCommunicate.setOnClickListener(this);
        fabMsg.setOnClickListener(this);
        fabCall.setOnClickListener(this);
        fabPhotes.setOnClickListener(this);

    }

    //if outcall(2) make upload image pop-up
    private void updateBookingStatus() {
        imageStartNeed = false;
        imagesEndNeed = false;

        if (imageStartNeed && booking.getCallType().equalsIgnoreCase("2") && booking.getStatus().equals(VariableConstant.ARRIVED) && startimageUrl.size() == 0) {
            seekBar.setProgress(0);
            showImageUploadWarning(VariableConstant.ARRIVED);
            return;
        }
        if (imagesEndNeed && booking.getCallType().equalsIgnoreCase("2") && booking.getStatus().equals(VariableConstant.JOB_TIMER_STARTED) && endimageUrl.size() == 0) {
            seekBar.setProgress(0);
            showImageUploadWarning(VariableConstant.JOB_TIMER_STARTED);
            return;
        }

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObjectTimer = new JSONObject();
        try {

            jsonObject.put("bookingId", bookidId);
            jsonObject.put("status", updatingStatus);
            jsonObject.put("latitude", sessionManager.getCurrentLat());
            jsonObject.put("longitude", sessionManager.getCurrentLng());

            if (updatingStatus.equals(VariableConstant.JOB_TIMER_COMPLETED)) {
                jsonObject.put("distance", "" + sessionManager.getOnJobDistance(booking.getBookingId()));
                jsonObject.put("second", timerSecond);
                jsonObject.put("dropNotes", dropupNotes);
                if (booking.getStatus().equals(VariableConstant.JOB_TIMER_STARTED) && endimageUrl.size() > 0) {
                    jsonObject.put("dropImages", new JSONArray(endimageUrl));
                }
            } else {
                jsonObject.put("pickupNotes", pickupNotes);
                if (booking.getStatus().equals(VariableConstant.ARRIVED) && startimageUrl.size() > 0) {
                    jsonObject.put("pickupImages", new JSONArray(startimageUrl));
                }
            }
            Log.d(TAG, "c: " + jsonObject);

            jsonObjectTimer.put("bookingId", bookidId);
            jsonObjectTimer.put("status", timerStatus);
            jsonObjectTimer.put("second", timerSecond);
        } catch (Exception e) {
            e.printStackTrace();
        }
        seekBar.setProgress(100);
        presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject, jsonObjectTimer);

    }

    private void showImageUploadWarning(final String jobTimerStarted) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_app_update_non_mandatory, null);
        alertDialogBuilder.setView(view);
        TextView tvUpdate = view.findViewById(R.id.tvUpdate);
        TextView tvLater = view.findViewById(R.id.tvLater);
        TextView tvMsg = view.findViewById(R.id.tvMsg);
        ImageView ivClose = view.findViewById(R.id.ivClose);
        ivClose.setVisibility(View.GONE);

        tvUpdate.setText(getString(R.string.continueText));
        tvLater.setText(getString(R.string.addPhoto));
        if(jobTimerStarted.equalsIgnoreCase("7"))
        {
            tvMsg.setText(getString(R.string.imageUploadWarning));
        }

        else{
            tvMsg.setText(getString(R.string.imageUploadWarningDrop));
        }
        imageStartNeed = false;
        imagesEndNeed = false;
        /*final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
        alertDialog.show();*/

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // alertDialog.dismiss();
                if (jobTimerStarted.equalsIgnoreCase(VariableConstant.ARRIVED)) {
                    imageStartNeed = false;
                }
                if (jobTimerStarted.equalsIgnoreCase(VariableConstant.JOB_TIMER_STARTED)) {
                    imagesEndNeed = false;
                }
                updateBookingStatus();
            }
        });

        tvLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //alertDialog.dismiss();
                openAddPhoto(jobTimerStarted);
            }
        });
    }


    private void openAddPhoto(String imageStartNeed) {
        Intent courierAddPhotoIntent = new Intent(this, CourierFlowAddPhotoActivity.class);
        if (imageStartNeed.equalsIgnoreCase(VariableConstant.JOB_TIMER_COMPLETED)) {
            courierAddPhotoIntent.putExtra("imageUrl", startimageUrl);
            courierAddPhotoIntent.putExtra("notes", pickupNotes);
        } else {
            courierAddPhotoIntent.putExtra("imageUrl", endimageUrl);
            courierAddPhotoIntent.putExtra("notes", dropupNotes);
        }
        courierAddPhotoIntent.putExtra("imageneeded", imageStartNeed);
        startActivityForResult(courierAddPhotoIntent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case VariableConstant.REQUEST_CODE_CROP_IMAGE:

                String imageneeded = data.getStringExtra("imageneeded");
                if (imageneeded.equalsIgnoreCase("7")) {
                    startimageUrl = data.getStringArrayListExtra("imageUrl");
                    pickupNotes = data.getStringExtra("notes");
                } else {
                    endimageUrl = data.getStringArrayListExtra("imageUrl");
                    dropupNotes = data.getStringExtra("notes");
                }
                /*if(imageUrl.size() > 0)
                {
                    ivAddPhotoTick.setVisibility(View.VISIBLE);
                }*/
                break;
        }
    }

    private void jobStarted(boolean isAlreadyStarted) {
        tvSeekbarText.setText(R.string.eventCompleted);
        tvStartPauseTimer.setVisibility(View.VISIBLE);
        tvStartPauseTimer.setEnabled(true);
        tvStartPauseTimer.setText(getString(R.string.pauseTimer));
        runTimer = true;

        if (isAlreadyStarted) {
            timerSecond = Integer.parseInt(bookingTimer.getSecond());
            if (bookingTimer.getStatus().equals("0")) {
                runTimer = false;
                tvTimer.setText("" + presenter.getDurationString(timerSecond));
                tvStartPauseTimer.setText(getString(R.string.startTimer));
                return;
            }
            long timeElapsed = (System.currentTimeMillis() - Utility.convertUTCToTimeStamp(bookingTimer.getStartTimeStamp())) / 1000;
            timerSecond = timerSecond + timeElapsed;
        }
        if (!isRunning) {
            handler.post(myRunnable);
        }
    }

    private void initTimer() {
        handler = new Handler();
        myRunnable = new Runnable() {
            @Override
            public void run() {
                isRunning = true;
                if (runTimer) {
                    timerSecond = timerSecond + 1;

                    if (timerSecond % 2 == 0) {
                        tvTimer.setTextColor(normalTextColor);
                    } else {
                        tvTimer.setTextColor(lightestTextColor);
                    }

                    tvTimer.setText("" + presenter.getDurationString(timerSecond));
                    handler.postDelayed(this, 1000);
                } else {
                    tvTimer.setTextColor(normalTextColor);
                    isRunning = false;
                }
            }
        };
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setChatCount();

        if (isRunning && sessionManager.getElapsedTime() != 0) {
            timerSecond = presenter.getTimeWhileInBackground(sessionManager);
        }
    }

    private void setChatCount() {
        if (sessionManager.getChatCount(booking.getBookingId()) != 0) {
            tvMessageCountInPlus.setText("" + sessionManager.getChatCount(booking.getBookingId()));
            tvMessageCount.setText("" + sessionManager.getChatCount(booking.getBookingId()));
            if (isFabOpen) {
                tvMessageCount.setVisibility(View.VISIBLE);
                tvMessageCountInPlus.setVisibility(View.GONE);
            } else {
                tvMessageCountInPlus.setVisibility(View.VISIBLE);
                tvMessageCount.setVisibility(View.GONE);
            }
        } else {
            tvMessageCountInPlus.setVisibility(View.GONE);
            tvMessageCount.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (isRunning) {
            sessionManager.setElapsedTime(timerSecond);
            sessionManager.setTimeWhilePaused(System.currentTimeMillis());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sessionManager.setElapsedTime(0);
        unregisterReceiver(receiver);
        handler.removeCallbacks(myRunnable);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        seekBar.setProgress(0);


        if (updatingStatus.equals(VariableConstant.JOB_TIMER_STARTED)) {
            progressDialog.setMessage(getString(R.string.updatingStatusEventCompleted));
            updatingStatus = VariableConstant.JOB_TIMER_COMPLETED;
            timerStatus = VariableConstant.JOB_TIMER_STARTED;
            jobStarted(false);

            sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(), VariableConstant.JOB_TIMER_STARTED));

            booking.setStatus(VariableConstant.JOB_TIMER_STARTED);

            tvStatus.setText(getString(R.string.eventStarted));

            AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.GigStarts.value, bookidId);

            bookingDetailsFragment.llAddress.setVisibility(View.GONE);
        } else {
            double travelFee = 0;
            double total = 0;
            try {

                JSONObject jsonObjectResponse = new JSONObject(msg);
                JSONObject jsonData = jsonObjectResponse.getJSONObject("data");

                total = Double.parseDouble(booking.getAccounting().getTotal()) - Double.parseDouble(booking.getAccounting().getLastDues());
                travelFee = Double.parseDouble(jsonData.getString("travelFee"));
                total += travelFee;

                if (booking.getServiceType().equals("2")) {
                    total = total - hourFee;
                    booking.getAccounting().setTotalActualJobTimeMinutes(jsonData.getString("totalActualJobTimeMinutes"));
                    booking.getAccounting().setTotalActualHourFee(jsonData.getString("totalActualHourFee"));
                    total = total + Double.parseDouble(jsonData.getString("totalActualHourFee"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            booking.getAccounting().setTravelFee("" + travelFee);
            booking.getAccounting().setTotal("" + total);

            sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(), VariableConstant.JOB_TIMER_COMPLETED));

            booking.setStatus(VariableConstant.JOB_TIMER_COMPLETED);
            finish();
            Intent intent = new Intent(this, InvoiceActivity.class);
            Bundle bundle = new Bundle();
            intent.putExtra("isFromBookingList",false);
            bundle.putSerializable("booking", booking);
            intent.putExtras(bundle);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
            } else {
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
            }

            AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.JobCompleted.value, bookidId);

        }
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cancel, null);
        alertDialogBuilder.setView(view);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvSubmit = view.findViewById(R.id.tvSubmit);
        ImageView ivClose = view.findViewById(R.id.ivClose);

        tvTitle.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
        rvCancel.setLayoutManager(new LinearLayoutManager(this));
        CancelListAdapter cancelListAdapter = new CancelListAdapter(this, cancelPojo.getData(), this);
        rvCancel.setAdapter(cancelListAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cancelId.equals("")) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("bookingId", bookidId);
                        jsonObject.put("resonId", cancelId);
                        progressDialog.setMessage(getString(R.string.loading));
                        presenter.cancelBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                        alertDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(EventStartedCompletedActivity.this, getString(R.string.plsSelectCancel), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelId = "";
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this, getString(R.string.alert), msg, getString(R.string.oK));
        try {
            if (booking.getReminderId() != null && !booking.getReminderId().equals("")) {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(booking.getReminderId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvStartPauseTimer:
                VariableConstant.IS_BOOKING_UPDATED = true;
                if (runTimer) {
                    tvStartPauseTimer.setText(getString(R.string.startTimer));
                    runTimer = false;
                    startStopTimer(VariableConstant.TIMER_PAUSED);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.GigPause.value, bookidId);
                } else {
                    jobStarted(false);
                    startStopTimer(VariableConstant.TIMER_STARTED);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.GigStarts.value, bookidId);

                }
                break;

            case R.id.llCutomerDetails:
                Intent intent = new Intent(this, CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivBackButton:
                closeActivity();
                break;

            case R.id.fabCommunicate:
                animateFAB();
                break;

            case R.id.fabCall:
                animateFAB();
                String uri = "tel:" + booking.getPhone();
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
                break;

            case R.id.fabMsg:
                animateFAB();
                sessionManager.setChatBookingID(booking.getBookingId());
                sessionManager.setChatCustomerName(booking.getFirstName() + " " + booking.getLastName());
                sessionManager.setChatCustomerPic(booking.getProfilePic());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this, ChattingActivity.class);
                startActivity(chatIntent);
                break;

            case R.id.fabPhotes:
                animateFAB();
                if(!isTimerStarted)
                openAddPhoto(VariableConstant.ARRIVED);
                else
                    openAddPhoto(VariableConstant.JOB_TIMER_STARTED);
                break;

            case R.id.tvCancel:
                progressDialog.setMessage(getString(R.string.loading));
                presenter.getCancelReason(booking.getBookingId());
                break;

        }
    }

    public void animateFAB() {

        if (isFabOpen) {
            isFabOpen = false;
            fabCommunicate.startAnimation(rotate_backward);
            fabMsg.startAnimation(fade_close);
            fabCall.startAnimation(fade_close);
            fabPhotes.startAnimation(fade_close);
            fabMsg.setClickable(false);
            fabCall.setClickable(false);
            fabPhotes.setClickable(false);
            setChatCount();
        } else {
            isFabOpen = true;
            fabCommunicate.startAnimation(rotate_forward);
            fabMsg.startAnimation(fade_open);
            fabCall.startAnimation(fade_open);
            fabPhotes.startAnimation(fade_open);
            fabMsg.setClickable(true);
            fabCall.setClickable(true);
            fabPhotes.setClickable(true);
            setChatCount();
        }
    }

    /**
     * method for calling presenter method for start and stop the timer
     *
     * @param status 0 => Stop 1 => Start
     */
    private void startStopTimer(String status) {
        try {

            long consumedTime = timerSecond;
            Log.d(TAG, "startStopTimer: " + consumedTime);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("bookingId", bookidId);
            jsonObject.put("status", status);
            jsonObject.put("second", consumedTime);

            presenter.updateTimer(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCancelSeleted(String id) {
        cancelId = id;
    }
}
