package com.sirvis.pro.bookingflow;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by murashid on 31-Oct-17.
 * <h1>UpdateStatusModel</h1>
 * Model for Update Status in AcceptRejectActivity
 * @see AcceptRejectActivity
 */

public class UpdateStatusModel {
    private static final String TAG = "MainActivityModel";
    private UpdateStatusModelImple imple;
    UpdateStatusModel( UpdateStatusModelImple imple)
    {
        this.imple = imple;
    }



    /**
     * method for calinng api for geting details for particular booking
     *
     * @param sessiontoken session Token
     * @param bookingId    booking id
     */
    void getBookingById(String sessiontoken, final String bookingId) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.BOOKING + "/" + bookingId,
                OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);

                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    imple.onSuccess(jsonObject.getString("data"), true);
                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE)) {
                                    RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getBookingById(newToken, bookingId);
                                            imple.onNewToken(newToken);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            imple.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            imple.sessionExpired(msg);
                                        }
                                    });

                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                                    imple.sessionExpired(jsonObject.getString("message"));
                                } else {
                                    imple.onFailure(jsonObject.getString("message"));
                                }
                            } else {
                                imple.onFailure();
                            }
                        } catch (Exception e) {
                            imple.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        imple.onFailure();
                    }
                });
    }





    /**
     * Method for calling api for update the job status whether accept or reject
     * @param sessionToken session Token
     * @param jsonObject required field in json object
     */
    void acceptRejectJob(String sessionToken, final JSONObject jsonObject)
    {
        Log.d(TAG, "acceptRejectJob: "+jsonObject);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_RESPONSE , OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            imple.onSuccess(jsonObjectResponse.getString("message"),false);
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND))
                        {
                            imple.onCancelBooking(jsonObjectResponse.getString("message"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    acceptRejectJob(newToken,jsonObject);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });

    }



    /**
     * Method for calling api for update the job status in ArrivedActivity and OnTheWayActivity
     * @param sessionToken session Token
     * @param jsonObject required field in json object
     */
    void updateStaus(String sessionToken, final JSONObject jsonObject, final boolean isTelecallBooking)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_STATUS , OkHttp3ConnectionStatusCode.Request_type.PATCH,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            if(isTelecallBooking)
                            {
                                imple.onSuccess(result,false);
                            }
                            else{
                                imple.onSuccess(jsonObjectResponse.getString("message"),false);
                            }

                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND))
                        {
                            imple.onCancelBooking(jsonObjectResponse.getString("message"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    updateStaus(newToken,jsonObject,isTelecallBooking);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
    }

    /**
     * Method for calling api for update the job status in EvenStartedActivity
     * it internally calling timer api if its job started status
     * @param sessionToken session Token
     * @param jsonObject required field in json object
     * @param jsonObjectTimer required field in json object for starting timer api
     * @param isFromUpdateStatus boolean for checking is from updateStatus or normal start and stop the timer
     */
    void updateStaus(final String sessionToken, final JSONObject jsonObject, final  JSONObject jsonObjectTimer, final boolean isFromUpdateStatus)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_STATUS , OkHttp3ConnectionStatusCode.Request_type.PATCH,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            if(jsonObject.getString("status").equals(VariableConstant.JOB_TIMER_STARTED))
                            {
                                updateTimer(sessionToken,jsonObjectTimer,isFromUpdateStatus);
                            }
                            else
                            {
                                imple.onSuccess(result,false);
                            }
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND))
                        {
                            imple.onCancelBooking(jsonObjectResponse.getString("message"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    updateStaus(newToken,jsonObject,jsonObjectTimer,isFromUpdateStatus);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
    }

    /**
     * Method for calling api for update the job status in invoice screen and internally calling the method <h1>amazonUpload</h1> for upload singutre picture
     * @param sessionToken sessionToken
     * @param jsonObject required field in jsonobject
     * @param amazonS3 UploadFileAmazonS3 object
     * @param mFileTemp signature file
     */
    void updateStaus(final String sessionToken, final JSONObject jsonObject, final UploadFileAmazonS3 amazonS3, final File mFileTemp)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_STATUS , OkHttp3ConnectionStatusCode.Request_type.PATCH,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            imple.onSuccess(jsonObjectResponse.getString("message"),false);
                            amazonUpload(amazonS3,mFileTemp);
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND))
                        {
                            imple.onCancelBooking(jsonObjectResponse.getString("message"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    updateStaus(newToken,jsonObject,amazonS3,mFileTemp);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
    }


    /**
     * method for calling api for start and stop the timer
     * @param sessionToken sessionToken
     * @param jsonObject jsonObject that contain required field
     * @param isFromUpdateStatus check its from update status or normal start and stop the timer
     */
    void updateTimer(String sessionToken, final JSONObject jsonObject, final boolean isFromUpdateStatus)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_TIMER , OkHttp3ConnectionStatusCode.Request_type.PATCH,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS) || statusCode.equals(VariableConstant.RESPONSE_CODE_INTERNAL_SERVER_ERROR))
                        {
                            if(isFromUpdateStatus)
                            {
                                imple.onSuccess(jsonObjectResponse.getString("message"),false);
                            }
                            else
                            {
                                imple.onSuccessTimer();
                            }
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    updateTimer(newToken,jsonObject,isFromUpdateStatus);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
    }

    /**
     * Method for calling api for getting the cancel reasion
     * @param bookingId
     */
    void getCancelReasons(final String bookingId)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.CANCEL_REASONS+"/"+bookingId, OkHttp3ConnectionStatusCode.Request_type.GET,new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            CancelPojo cancelPojo = gson.fromJson(result,CancelPojo.class);
                            imple.onSuccessCancelReason(cancelPojo);
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    getCancelReasons(bookingId);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
    }

    /**
     * method for uploading image to amazon
     * @param amazonS3 object of the UploadFileAmazonS3
     * @param mFileTemp file which has to been upload in amazon
     */
    void amazonUpload(UploadFileAmazonS3 amazonS3, final File mFileTemp)
    {
        String BUCKETSUBFOLDER = VariableConstant.SIGNATURE_UPLOAD;

        final String imageUrl = "https://" + VariableConstant.BUCKET_NAME + "." + BuildConfig.AMAZON_BASE_URL
                + BUCKETSUBFOLDER +"/"
                + mFileTemp.getName();

        Log.d(TAG, "amzonUpload: " + imageUrl);

        amazonS3.Upload_data(AppController.getInstance(),BUCKETSUBFOLDER, mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d(TAG, "sucess: " + success);
                mFileTemp.delete();
            }

            @Override
            public void error(String errormsg) {
                Log.d(TAG, "error: " + errormsg);
            }
        });
    }


    /**
     * Method for calling api for update the job status
     * @param sessionToken session Token
     */
    void cancelBooking(String sessionToken, final JSONObject jsonObject)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CANCEL_BOOKING , OkHttp3ConnectionStatusCode.Request_type.PATCH,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            imple.onCancelBooking(jsonObjectResponse.getString("message"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    cancelBooking(newToken,jsonObject);
                                    imple.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    imple.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    imple.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            imple.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        imple.onFailure();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailure();
            }
        });
    }



    /**
     * interface for Presenter Implementation
     */
    interface UpdateStatusModelImple
    {
        void onSuccess(String eMsg,boolean bookingDetailsRefreshed);
        void onSuccessCancelReason(CancelPojo cancelPojo);
        void onCancelBooking(String msg);
        void onSuccessTimer();
        void onFailure(String failureMsg);
        void onFailure();
        void onNewToken(String newToken);
        void sessionExpired(String msg);
        void initCallApi(String sessionToken, String roomId, String customerId,String callType,String bookingID);
    }
}
