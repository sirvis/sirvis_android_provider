package com.sirvis.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CancelListAdapter;
import com.sirvis.pro.bookingflow.review.CustomerReviewsActivity;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.telecall.utility.CallingApis;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CalendarEventHelper;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>InCallActivity</h1>
 * Activity for starting and enting the event time
 */
public class TeleCallActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, CancelListAdapter.CancelSelected, EasyPermissions.PermissionCallbacks {

    private static final String TAG = "EventStarted";
    private final int CALL_REQUEST_CODE = 109;
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private SeekBar seekBar;
    private Booking booking;
    private String bookidId = "";
    private String bookingEndtime = "";
    private String cancelId = "";
    private Typeface fontBold;
    private Typeface fontMedium;
    private BroadcastReceiver receiver;
    private double hourFee = 0;
    private FloatingActionButton fabCall, fabMsg, fabVideoCall;
    private Animation fade_open, fade_close, rotate_forward, rotate_backward;
    private TextView tvMessageCount;
    private String callType = "0";
    private TextView tvSeekbarText;
    private SimpleDateFormat serverFormat, displayDateFormat;
    private TextView tvCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tele_call);

        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @SuppressLint("SetTextI18n")
    private void initView() {

        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        displayDateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm a", Locale.US);
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.refreshing));
        progressDialog.setCancelable(false);

        fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontLight = Utility.getFontLight(this);

        TextView tveventId = findViewById(R.id.tveventId);
        tveventId.setTypeface(fontBold);

        tvMessageCount = findViewById(R.id.tvMessageCount);
        tvSeekbarText = findViewById(R.id.tvSeekbarText);
        seekBar = findViewById(R.id.seekBar);
        tvMessageCount.setTypeface(fontRegular);
        tvSeekbarText.setTypeface(fontRegular);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivBackButton.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");

        BookingDetailsFragment bookingDetailsFragment = BookingDetailsFragment.newInstance(booking, false);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragBooking, bookingDetailsFragment).commit();
        bookidId = booking.getBookingId();
        bookingEndtime = booking.getBookingEndtime();

        if (booking.getServiceType().equals("2")) {
            hourFee = Double.parseDouble(booking.getAccounting().getTotalActualHourFee());
        }

        tveventId.setText(getText(R.string.jobId) + " " + booking.getBookingId());

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("bookingId", bookidId);
                        jsonObject.put("status", VariableConstant.JOB_TIMER_COMPLETED);
                        jsonObject.put("latitude", sessionManager.getCurrentLat());
                        jsonObject.put("longitude", sessionManager.getCurrentLng());
                        jsonObject.put("distance", "" + sessionManager.getOnJobDistance(booking.getBookingId()));
                        Log.d(TAG, "c: " + jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject, true);
                } else {
                    seekBar.setProgress(0);
                }
            }
        });


        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT)) {
                    setChatCount();
                } else {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(booking.getBookingId())) {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        // DialogHelper.customAlertDialogCloseActivity(InCallActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);

        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(this, R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(this, R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(this, R.anim.rotate_left_to_center);

        fabCall = findViewById(R.id.fabCall);
        fabMsg = findViewById(R.id.fabMsg);
        fabVideoCall = findViewById(R.id.fabVideoCall);

        fabMsg.setOnClickListener(this);
        fabCall.setOnClickListener(this);
        fabVideoCall.setOnClickListener(this);

        /*
         * Bug id : Trello 189
         * Bug Description : There is no option to cancel a job with reason for InCall
         * Fixed Description : Added Cancel textview in layout and handled the action.
         * Dev : Murashid
         * Date : 29-11-2018
         * */
        tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setTypeface(fontMedium);
        tvCancel.setOnClickListener(this);

        if (booking.getStatus().equals("9")) {
            findViewById(R.id.seekBar).setVisibility(View.GONE);

        }
        if (booking.getStatus().equals(VariableConstant.JOB_TIMER_COMPLETED)) {
            seekBar.setVisibility(View.GONE);
            tvCancel.setVisibility(View.GONE);
            fabCall.setVisibility(View.GONE);
            fabMsg.setVisibility(View.GONE);
            fabVideoCall.setVisibility(View.GONE);
            tvSeekbarText.setText(booking.getStatusMsg());
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);

        enableDisableCommunication(true);
    }


    @Override
    protected void onResume() {
        super.onResume();
        JSONObject tempObj = new JSONObject();
        setChatCount();

        try {
            tempObj.put("status", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "onResume: " + AppController.getInstance().getMqttHelper().isMqttConnected());

        if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
            AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getProviderId(), false);
            AppController.getInstance().getMqttHelper().setUserId(AppController.getInstance().getCallHelper().getUserId(), tempObj);
        }/* else {
            AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value, tempObj, 1, false);
        }*/
    }


    private void setChatCount() {
        if (sessionManager.getChatCount(booking.getBookingId()) != 0) {
            tvMessageCount.setText("" + sessionManager.getChatCount(booking.getBookingId()));
            tvMessageCount.setVisibility(View.VISIBLE);
        } else {
            tvMessageCount.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.setMessage(getString(R.string.refreshing));
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        seekBar.setProgress(0);

        double travelFee = 0;
        double total = 0;
        try {

            JSONObject jsonObjectResponse = new JSONObject(msg);
            JSONObject jsonData = jsonObjectResponse.getJSONObject("data");

            total = Double.parseDouble(booking.getAccounting().getTotal()) - Double.parseDouble(booking.getAccounting().getLastDues());
            travelFee = Double.parseDouble(jsonData.getString("travelFee"));
            total += travelFee;

            if (booking.getServiceType().equals("2")) {
                total = total - hourFee;
                booking.getAccounting().setTotalActualJobTimeMinutes(jsonData.getString("totalActualJobTimeMinutes"));
                booking.getAccounting().setTotalActualHourFee(jsonData.getString("totalActualHourFee"));
                total = total + Double.parseDouble(jsonData.getString("totalActualHourFee"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        booking.getAccounting().setTravelFee("" + travelFee);
        booking.getAccounting().setTotal("" + total);
        booking.setStatus(VariableConstant.JOB_TIMER_COMPLETED);
        sessionManager.setBookingStr(presenter.getBookingStr(booking, sessionManager.getBookingStr(), VariableConstant.JOB_TIMER_COMPLETED));


//        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.JobCompleted.value, bookidId);
        seekBar.setVisibility(View.GONE);
        tvCancel.setVisibility(View.GONE);
        fabCall.setVisibility(View.GONE);
        fabMsg.setVisibility(View.GONE);
        fabVideoCall.setVisibility(View.GONE);
        tvSeekbarText.setText("Job completed, payment pending");

       /* finish();
        Intent intent = new Intent(this, InvoiceActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking", booking);
        intent.putExtras(bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
*/

    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cancel, null);
        alertDialogBuilder.setView(view);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvSubmit = view.findViewById(R.id.tvSubmit);
        ImageView ivClose = view.findViewById(R.id.ivClose);

        tvTitle.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
        rvCancel.setLayoutManager(new LinearLayoutManager(this));
        CancelListAdapter cancelListAdapter = new CancelListAdapter(this, cancelPojo.getData(), this);
        rvCancel.setAdapter(cancelListAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cancelId.equals("")) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("bookingId", bookidId);
                        jsonObject.put("resonId", cancelId);
                        progressDialog.setMessage(getString(R.string.loading));
                        presenter.cancelBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                        alertDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(TeleCallActivity.this, getString(R.string.plsSelectCancel), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelId = "";
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this, getString(R.string.alert), msg, getString(R.string.oK));
        try {
            if (booking.getReminderId() != null && !booking.getReminderId().equals("")) {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(booking.getReminderId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {
        CallingApis.initiateCall(this, booking.getCustomerId(), booking.getFirstName()
                        + " " + booking.getLastName(), booking.getProfilePic(),
                callType, booking.getPhone(), roomId, callId, bookidId, bookingEndtime,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCutomerDetails:
                Intent intent = new Intent(this, CustomerReviewsActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                } else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivBackButton:
                closeActivity();
                break;


            case R.id.fabCall:

                if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) < ((Long.parseLong(booking.getEventStartTime())) - (Long.parseLong(booking.getCallButtonEnableForProvider())))) {
                    try {
                        DialogHelper.customAlertDialogServiceDetails(TeleCallActivity.this, "Alert...!", "Your tele call will be enabled on and after " +
                                displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getEventStartTime()) - (Long.parseLong(booking.getCallButtonEnableForProvider())))))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) > ((Long.parseLong(booking.getBookingEndtime())) /*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*/)) {
                    try {
                        DialogHelper.customAlertDialogServiceDetails(TeleCallActivity.this, "Alert...!", "Sorry your call slot has expired,You cannot call after " +
                                displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getBookingEndtime()) /*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*/)))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressDialog.setMessage(getString(R.string.calling));
                    enableDisableCommunication(false);
                    callType = "0";
                    openCall(callType);
                }



              /*  String uri = "tel:"+booking.getPhone() ;
                Intent callIntent = new Intent(Intent.ACTION_DIAL);)
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);*/
                break;

            case R.id.fabVideoCall:

                if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) < ((Long.parseLong(booking.getEventStartTime())) - (Long.parseLong(booking.getCallButtonEnableForProvider())))) {
                    try {
                        DialogHelper.customAlertDialogServiceDetails(TeleCallActivity.this, "Alert...!", "Your tele call will be enabled on and after " +
                                displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getEventStartTime()) - (Long.parseLong(booking.getCallButtonEnableForProvider())))))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if ((TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())) > ((Long.parseLong(booking.getBookingEndtime())) /*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*/)) {
                    try {
                        DialogHelper.customAlertDialogServiceDetails(TeleCallActivity.this, "Alert...!", "Sorry your call slot has expired,You cannot call after " +
                                displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(String.valueOf(Integer.parseInt(booking.getBookingEndtime()) /*+ (Long.parseLong(booking.getCallButtonEnableForProvider()))*/)))));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    progressDialog.setMessage(getString(R.string.calling));
                    enableDisableCommunication(false);
                    callType = "1";
                    openCall(callType);
                }

                break;

            case R.id.fabMsg:
                enableDisableCommunication(false);
                sessionManager.setChatBookingID(booking.getBookingId());
                sessionManager.setChatCustomerName(booking.getFirstName() + " " + booking.getLastName());
                sessionManager.setChatCustomerPic(booking.getProfilePic());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this, ChattingActivity.class);
                startActivity(chatIntent);
                break;

            case R.id.tvCancel:
                progressDialog.setMessage(getString(R.string.loading));
                presenter.getCancelReason(booking.getBookingId());
                break;

        }
    }

    private void enableDisableCommunication(boolean b) {
        if (fabCall != null) {
            fabCall.setEnabled(b);
            fabVideoCall.setEnabled(b);
            fabVideoCall.setEnabled(b);
        }
    }

    private void openCall(String callType) {
        sessionManager.setChatBookingID(booking.getBookingId());
        sessionManager.setChatCustomerName(booking.getFirstName() + " " + booking.getLastName());
        sessionManager.setChatCustomerPic(booking.getProfilePic());
        sessionManager.setChatCustomerID(booking.getCustomerId());

        if (Build.VERSION.SDK_INT >= 23) {

            if (!Settings.System.canWrite(this) || !Settings.canDrawOverlays(this)) {
                if (!Settings.System.canWrite(this)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                }
                //If the draw over permission is not available open the settings screen
                //to grant the permission.

                if (!Settings.canDrawOverlays(this)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                }
            } else {
                if (isPermissionGranted()) {
                    presenter.initCallApi(sessionManager.getCallToken(), String.valueOf(System.currentTimeMillis()), booking.getCustomerId(), (callType.equals("1") ? "video" : "audio"), booking.getBookingId());
//                    CallingApis.initiateCall(this, booking.getCustomerId(), booking.getFirstName()
//                                    + " "+booking.getLastName(), booking.getProfilePic(),
//                            callType, booking.getPhone(),String.valueOf(System.currentTimeMillis()),randomString(),bookidId,false);

                }
            }
        } else {
            if (isPermissionGranted()) {
                presenter.initCallApi(sessionManager.getCallToken(), String.valueOf(System.currentTimeMillis()), booking.getCustomerId(), (callType.equals("1") ? "video" : "audio"), booking.getBookingId());

                //                    CallingApis.initiateCall(this, booking.getCustomerId(), booking.getFirstName());

            }
        }
    }

    //From CallHelper
    public String randomString() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }


        sb.append("PnPLabs3Embed");
        return sb.toString();
    }

    private boolean isPermissionGranted() {
        if (EasyPermissions.hasPermissions(this, VariableConstant.CALL_PERMISSIONS)) {
            return true;
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                    CALL_REQUEST_CODE, VariableConstant.CALL_PERMISSIONS);
            return false;
        }
    }

    @Override
    public void onCancelSeleted(String id) {
        cancelId = id;
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }
}
