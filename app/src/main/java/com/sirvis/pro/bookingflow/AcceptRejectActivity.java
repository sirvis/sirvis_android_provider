package com.sirvis.pro.bookingflow;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.review.CustomerReviewsActivity;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CalendarEventHelper;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;
import com.sirvis.pro.utility.WorkaroundMapFragment;

import org.json.JSONObject;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>AcceptRejectActivity</h1>
 * Activity for accept and reject jobs
 */
public class AcceptRejectActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, OnMapReadyCallback, EasyPermissions.PermissionCallbacks  {

    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private double lat=0,lng=0 ;
    private String bookidId ="";
    private Booking booking;

    private final int REQUEST_CODE_FOR_BID = 121;

    private BroadcastReceiver receiver;
    private GoogleMap googleMap;

    private FloatingActionButton fabCommunicate,fabCall,fabMsg;
    private Animation fade_open, fade_close,rotate_forward,rotate_backward;
    private boolean isFabOpen = false;
    private TextView tvMessageCountInPlus, tvMessageCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_reject);

        initView();
    }

    /**
     * initialize the views
     */
    @SuppressLint("SetTextI18n")
    private void initView()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        final AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        WorkaroundMapFragment sMapFrag = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        // Getting GoogleMap object from the fragment
        sMapFrag.getMapAsync(this);
        sMapFrag.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                appBarLayout.requestDisallowInterceptTouchEvent(true);
            }
        });

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");
        bookidId = booking.getBookingId();
        if(!booking.getLatitude().equals(""))
        {
            lat = Double.parseDouble(booking.getLatitude());
            lng = Double.parseDouble(booking.getLongitude());
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragBooking, BookingDetailsFragment.newInstance(booking,true)).commit();

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        TextView tvAccept = findViewById(R.id.tvAccept);
        TextView tvReject = findViewById(R.id.tvReject);
        TextView tvToBid = findViewById(R.id.tvToBid);
        TextView tvMyQuoteLabel = findViewById(R.id.tvMyQuoteLabel);
        TextView tvMyQuote = findViewById(R.id.tvMyQuote);
        ImageView ivLocation = findViewById(R.id.ivLocation);
        tvAccept.setTypeface(fontRegular);
        tvReject.setTypeface(fontRegular);
        tvToBid.setTypeface(fontBold);
        tvMyQuoteLabel.setTypeface(fontBold);
        tvMyQuote.setTypeface(fontBold);
        tvAccept.setOnClickListener(this);
        tvReject.setOnClickListener(this);
        ivLocation.setOnClickListener(this);
        tvToBid.setOnClickListener(this);

        if(booking.getBookingModel().equals("3"))//isBidBooking
        {
            findViewById(R.id.llAcceptReject).setVisibility(View.GONE);
            if(booking.getStatus().equals(VariableConstant.RECEIVED) || booking.getStatus().equals(VariableConstant.NEW_BOOKING))
            {
                findViewById(R.id.llToBid).setVisibility(View.VISIBLE);
                String bidCredit = Utility.getPrice(booking.getAccounting().getBidCredit(),sessionManager.getCurrencySymbol(),sessionManager.getCurrencyAbbrevation());
                tvToBid.setText(bidCredit +" "+getString(R.string.toBidForThisJob));
            }
            else
            {
                tvMessageCountInPlus = findViewById(R.id.tvMessageCountInPlus);
                tvMessageCount = findViewById(R.id.tvMessageCount);
                tvMessageCountInPlus.setTypeface(fontRegular);
                tvMessageCount.setTypeface(fontRegular);

                fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
                fade_close = AnimationUtils.loadAnimation(this,R.anim.fade_close);
                rotate_forward = AnimationUtils.loadAnimation(this,R.anim.rotate_center_to_left);
                rotate_backward = AnimationUtils.loadAnimation(this,R.anim.rotate_left_to_center);

                fabCommunicate = findViewById(R.id.fabCommunicate);
                fabCall = findViewById(R.id.fabCall);
                fabMsg = findViewById(R.id.fabMsg);

                fabCommunicate.setOnClickListener(this);
                fabMsg.setOnClickListener(this);
                fabCall.setOnClickListener(this);

                findViewById(R.id.rlMyQuote).setVisibility(View.VISIBLE);
                fabCommunicate.setVisibility(View.VISIBLE);
                tvMyQuote.setText(Utility.getPrice(booking.getQuotedPrice(),sessionManager.getCurrencySymbol(),sessionManager.getCurrencyAbbrevation()));
            }

        }

        CollapsingToolbarLayout ctlMyBooking = findViewById(R.id.ctlMyBooking);
        ctlMyBooking.setTitle(getText(R.string.jobId)+" "+booking.getBookingId());
        ctlMyBooking.setCollapsedTitleTextAppearance(R.style.TextCollapsedEventId);
        ctlMyBooking.setExpandedTitleTextAppearance(R.style.TextExpandHide);
        ctlMyBooking.setCollapsedTitleGravity(Gravity.CENTER);

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT))
                {
                    setChatCount();
                }
                else if(intent.hasExtra("cancelid"))
                {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(booking.getBookingId()))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        //closeActivity();
                        DialogHelper.customAlertDialogCloseActivity(AcceptRejectActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setChatCount();
    }

    private void setChatCount() {
        if(booking.getBookingModel().equals("3") && booking.getStatus().equals(VariableConstant.ACTIVE_BIDS) && tvMessageCount !=null)
        {
            if(sessionManager.getChatCount(bookidId) != 0)
            {
                tvMessageCountInPlus.setText(""+sessionManager.getChatCount(bookidId));
                tvMessageCount.setText(""+sessionManager.getChatCount(bookidId));
                if(isFabOpen)
                {
                    tvMessageCount.setVisibility(View.VISIBLE);
                    tvMessageCountInPlus.setVisibility(View.GONE);
                }
                else
                {
                    tvMessageCountInPlus.setVisibility(View.VISIBLE);
                    tvMessageCount.setVisibility(View.GONE);
                }
            }
            else
            {
                tvMessageCountInPlus.setVisibility(View.GONE);
                tvMessageCount.setVisibility(View.GONE);
            }
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        String[] perms = {Manifest.permission.WRITE_CALENDAR,Manifest.permission.READ_CALENDAR};

        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.write_calendar_permission_message),
                    1000, perms);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            DialogHelper.aDialogOnPermissionDenied(this);
        }
        else
        {
            EasyPermissions.requestPermissions(this, getString(R.string.write_calendar_permission_message),
                    1000,  perms.toArray(new String[perms.size()]));
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        closeActivity();
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.alert),msg,getString(R.string.oK));
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvAccept:
                try {

                    String reminderId = "";
                    if(booking.getBookingType().equals("2"))
                    {
                        CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                        reminderId = String.valueOf(calendarEventHelper.addEvent(booking));
                    }

                    JSONObject acceptJsonObject = new JSONObject();
                    acceptJsonObject.put("bookingId",bookidId);
                    acceptJsonObject.put("status",VariableConstant.ACCEPT);
                    acceptJsonObject.put("latitude",sessionManager.getCurrentLat());
                    acceptJsonObject.put("longitude",sessionManager.getCurrentLng());
                    acceptJsonObject.put("reminderId",reminderId);

                    VariableConstant.IS_BOOKING_UPDATED = true;
                    VariableConstant.IS_BOOKING_ACCEPTED = true;

                    progressDialog.setMessage(getString(R.string.updatingStatusForAccept));
                    presenter.acceptRejectJob(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),acceptJsonObject);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingAccept.value,bookidId);

                    if(booking.getBookingType().equals("1"))
                    {
                        AppController.getInstance().getMixpanelHelper().timeEndForNowBookingRespond(booking.getFirstName()+" "+booking.getLastName()
                                ,bookidId,"Accepted");
                    }
                    else
                    {
                        AppController.getInstance().getMixpanelHelper().timeEndForLaterBookingRespond(booking.getFirstName()+" "+booking.getLastName()
                                ,bookidId,"Accepted");
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case R.id.tvReject:
                try {
                    JSONObject rejectJsonObject = new JSONObject();
                    rejectJsonObject.put("bookingId",bookidId);
                    rejectJsonObject.put("status",VariableConstant.REJECT);
                    rejectJsonObject.put("latitude",sessionManager.getCurrentLat());
                    rejectJsonObject.put("longitude",sessionManager.getCurrentLng());

                    VariableConstant.IS_BOOKING_UPDATED = true;

                    progressDialog.setMessage(getString(R.string.updatingStatusForDecline));
                    presenter.acceptRejectJob(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),rejectJsonObject);

                    AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.BookingDecling.value,bookidId);
                    if(booking.getBookingType().equals("1"))
                    {
                        AppController.getInstance().getMixpanelHelper().timeEndForNowBookingRespond(booking.getFirstName()+" "+booking.getLastName()
                                ,bookidId,"Rejected");
                    }
                    else
                    {
                        AppController.getInstance().getMixpanelHelper().timeEndForLaterBookingRespond(booking.getFirstName()+" "+booking.getLastName()
                                ,bookidId,"Rejected");
                    }

                    sessionManager.setLastBooking("");

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

                break;

            case R.id.tvToBid:
                Intent bidIntent = new Intent(this,MyBidActivity.class);
                Bundle bidBundle = new Bundle();
                bidBundle.putSerializable("booking",booking);
                bidIntent.putExtras(bidBundle);
                startActivityForResult(bidIntent, REQUEST_CODE_FOR_BID);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;

            case R.id.llCutomerDetails:
                Intent intent = new Intent(this,CustomerReviewsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking",booking);
                intent.putExtras(bundle);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivLocation:
                if(googleMap != null)
                {
                    LatLng latLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng()));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                }
                break;

            case R.id.fabCommunicate:
                animateFAB();
                break;

            case R.id.fabCall:
                animateFAB();
                String uri = "tel:"+booking.getPhone() ;
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
                break;

            case R.id.fabMsg:
                animateFAB();
                sessionManager.setChatBookingID(bookidId);
                sessionManager.setChatCustomerName(booking.getFirstName() + " "+booking.getLastName());
                sessionManager.setChatCustomerPic(booking.getProfilePic());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this,ChattingActivity.class);
                startActivity(chatIntent);
                break;

        }
    }

    public void animateFAB(){
        if(isFabOpen){
            isFabOpen = false;
            fabCommunicate.startAnimation(rotate_backward);
            fabMsg.startAnimation(fade_close);
            fabCall.startAnimation(fade_close);
            fabMsg.setClickable(false);
            fabCall.setClickable(false);
            setChatCount();
        }
        else
        {
            isFabOpen = true;
            fabCommunicate.startAnimation(rotate_forward);
            fabMsg.startAnimation(fade_open);
            fabCall.startAnimation(fade_open);
            fabMsg.setClickable(true);
            fabCall.setClickable(true);
            setChatCount();
        }
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        this.googleMap = Map;
        LatLng latLng = new LatLng(lat,lng);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        googleMap.getUiSettings().setCompassEnabled(false);

        final View customerMarkerView = getLayoutInflater().inflate(R.layout.custom_marker, null);
        final ImageView ivProfile = customerMarkerView.findViewById(R.id.ivProfile);

        final Bitmap customerMarker = Utility.createDrawableFromView(this, customerMarkerView);
        final Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(customerMarker))
                .flat(false));

        Glide.with(this).setDefaultRequestOptions(new RequestOptions()  .placeholder(R.drawable.profile_default_image)
                .transform(new CircleTransform(this))
                .error(R.drawable.profile_default_image))
                .asBitmap()
                .load(booking.getProfilePic())


                .into(new BitmapImageViewTarget(ivProfile) {
                    @Override
                    public void onResourceReady(Bitmap drawable, Transition anim) {
                        super.onResourceReady(drawable, anim);
                        ivProfile.setImageBitmap(drawable);
                        Bitmap customerMarker = Utility.createDrawableFromView(AcceptRejectActivity.this, customerMarkerView);
                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(customerMarker));
                    }
                });

        LatLng providerLatLng = new LatLng(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
        final LatLngBounds bounds = new LatLngBounds.Builder()
                .include(providerLatLng)
                .include(latLng)
                .build();

        final int padding = getResources().getDimensionPixelSize(R.dimen.dimen_45dp);

        this.googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_FOR_BID )
        {
            finish();
        }
    }
}
