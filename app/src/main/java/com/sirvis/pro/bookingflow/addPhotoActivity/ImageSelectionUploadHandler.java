package com.sirvis.pro.bookingflow.addPhotoActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;


import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;


public class ImageSelectionUploadHandler {
    private static final String TAG = "ImageHandler";
    private Activity activity;
    private File mFileTemp;
    private LayoutInflater inflater;
    private Typeface fontRegular, fontMedium;
    private UploadFileAmazonS3 amazonS3;
    private ImageSelectionListener imageSelectionListener;
    private String BUCKETSUBFOLDER;

    public ImageSelectionUploadHandler(Activity activity, String BUCKETSUBFOLDER, ImageSelectionListener imageSelectionListener)
    {
        this.activity = activity;
        this.inflater = LayoutInflater.from(activity);
        this.fontRegular = Utility.getFontRegular(activity);
        this.fontMedium = Utility.getFontMedium(activity);;
        amazonS3 = UploadFileAmazonS3.getInstance(activity);
        this.BUCKETSUBFOLDER = BUCKETSUBFOLDER;
        this.imageSelectionListener = imageSelectionListener;
    }

    public void selectImage( String filePath, String fileName, boolean isToclearFile) {
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(activity, filePath, isToclearFile),fileName);

        final BottomSheetDialog mDialog = new BottomSheetDialog(activity);
        View view = inflater.inflate(R.layout.bottom_sheet_picture_selection, null);
        mDialog.setContentView(view);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvImageSelectionHeader = view.findViewById(R.id.tvImageSelectionHeader);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvGallery = view.findViewById(R.id.tvGallery);

        tvImageSelectionHeader.setTypeface(fontRegular);
        tvCamera.setTypeface(fontMedium);
        tvGallery.setTypeface(fontMedium);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    public void openCamera( String filePath, String fileName, boolean isToclearFile) {
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(activity, filePath, isToclearFile),fileName);
        takePicture();
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            activity.startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        activity.startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    public void onResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode != activity.RESULT_OK) {
                return;
            }
            switch (requestCode) {
                case VariableConstant.REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = activity.getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        startCropImage();
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    startCropImage();
                    break;

                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    imageSelectionListener.loadImage(path);
                    amazonUpload(amazonS3,mFileTemp);
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for starting CropImage Activity for crop the selected image
     */
    private void startCropImage()
    {
        Intent intent = new Intent(activity, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        activity.startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }

    /**
     * method for uploading image to amazon
     * @param amazonS3 object of the UploadFileAmazonS3
     * @param mFileTemp file which has to been upload in amazon
     */
    private void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp)
    {

        final String imageUrl ="https://" + VariableConstant.BUCKET_NAME + "." + BuildConfig.AMAZON_BASE_URL
                + BUCKETSUBFOLDER + "/"
                + mFileTemp.getName();
        Log.d(TAG, "amzonUpload: " + imageUrl);
        imageSelectionListener.onSuccessImageUpload(imageUrl);

        amazonS3.Upload_data(AppController.getInstance(),BUCKETSUBFOLDER, mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d(TAG, "sucess: "+success);
            }

            @Override
            public void error(String errormsg) {
                Log.d(TAG, "error: " + errormsg);
            }
        });
    }

    public interface ImageSelectionListener
    {
        void loadImage(String imagePath);
        void onSuccessImageUpload(String url);
    }
}
