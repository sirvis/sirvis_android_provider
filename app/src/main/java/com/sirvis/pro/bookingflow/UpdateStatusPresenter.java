package com.sirvis.pro.bookingflow;


import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.sirvis.pro.mqtt.MqttHelper;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.telecall.callservice.CallingModel;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static com.sirvis.pro.utility.VariableConstant.RESPONSE_CODE_ALREADY_IN_CALL;


/**
 * Created by murashid on 31-Oct-17.
 * <h1>UpdateStatusPresenter</h1>
 * UpdateStatusPresenter presenter for update status in AcceptRejectAcitivty
 *
 * @see AcceptRejectActivity
 */

public class UpdateStatusPresenter implements UpdateStatusModel.UpdateStatusModelImple {
    private String TAG = UpdateStatusPresenter.class.getSimpleName();
    private UpdateStatusModel model;
    private UpdateStatusPresenterImple presenterImple;
    private CallingModel callingModel = new CallingModel();
    private MqttHelper mqttHelper;

    UpdateStatusPresenter(UpdateStatusPresenterImple presenterImple) {
        model = new UpdateStatusModel(this);
        this.presenterImple = presenterImple;
        mqttHelper = AppController.getInstance().getMqttHelper();
    }
    public void getBookingById(String sessiontoken,String bookingId) {
        presenterImple.startProgressBar();
        model.getBookingById(sessiontoken,bookingId);
    }
    void acceptRejectJob(String sessionToken, JSONObject jsonObject) {
        presenterImple.startProgressBar();
        model.acceptRejectJob(sessionToken, jsonObject);
    }

    void updateTimer(String sessionToken, final JSONObject jsonObject) {
        presenterImple.startProgressBar();
        model.updateTimer(sessionToken, jsonObject, false);
    }

    /**
     * Method for calling api for update the job status
     *
     * @param sessionToken session Token
     */
    void updateStaus(final String sessionToken, final JSONObject jsonObject, final JSONObject jsonObjectTimer) {
        presenterImple.startProgressBar();
        model.updateStaus(sessionToken, jsonObject, jsonObjectTimer, true);
    }

    void updateStaus(final String sessionToken, final JSONObject jsonObject, final UploadFileAmazonS3 amazonS3, final File mFileTemp) {
        presenterImple.startProgressBar();
        model.updateStaus(sessionToken, jsonObject, amazonS3, mFileTemp);
    }

    void updateStaus(final String sessionToken, final JSONObject jsonObject,boolean isTelecallBooking) {
        presenterImple.startProgressBar();
        model.updateStaus(sessionToken, jsonObject,isTelecallBooking);
    }

    String getBookingStr(Booking booking, String oldStr, String updatedStatus) {
        return oldStr.replace(booking.getBookingId() + "|" + booking.getStatus(), booking.getBookingId() + "|" + updatedStatus);
    }

    /**
     * method for returning hours and minutes from seconds
     *
     * @param seconds second
     * @return hours and minutes
     */
    String getDurationString(long seconds) {
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        int hours = (int) (TimeUnit.SECONDS.toHours(seconds) - (day * 24));
        int minute = (int) (TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60));
        int second = (int) (TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60));

        String timer = String.format("%02d", hours) + " : " + String.format("%02d", minute) + " : " + String.format("%02d", second);

        return timer;
    }

    long getTimeWhileInBackground(SessionManager sessionManager) {
        return sessionManager.getElapsedTime() + (long) (Math.round((System.currentTimeMillis() - sessionManager.getTimeWhilePaused()) / 1000f));
    }

    void getCancelReason(String bookingId) {
        presenterImple.startProgressBar();
        model.getCancelReasons(bookingId);
    }

    void cancelBooking(String sessionToken, JSONObject jsonObject) {
        model.cancelBooking(sessionToken, jsonObject);
        presenterImple.startProgressBar();
    }

    @Override
    public void onSuccess(String msg,boolean bookingDetailsRefreshed) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg,bookingDetailsRefreshed);
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessCancelReason(cancelPojo);
    }

    @Override
    public void onCancelBooking(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onCancelBooking(msg);
    }

    @Override
    public void onSuccessTimer() {
        presenterImple.stopProgressBar();
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onNewToken(String newToken) {
        presenterImple.onNewToken(newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.sessionExpired(msg);
    }

    @Override
    public void initCallApi(String calltoken, final String currentRoomId, String customerId, final String callType, String bookingId) {

//        presenterImple.startProgressBar();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("room", currentRoomId);
            jsonObject.put("to", customerId);
            jsonObject.put("type", callType);
            jsonObject.put("bookingId", bookingId);

        } catch (JSONException e) {
            presenterImple.stopProgressBar();
            Toast.makeText(((TeleCallActivity) presenterImple).getApplicationContext(), "Some things went wrong", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(calltoken, ServiceUrl.CALL, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
//                        presenterImple.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        String callId = "";
                                        try {
                                            callId = callingModel.parseCallerId(result);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (!TextUtils.isEmpty(callId)) {
                                            if (mqttHelper.isMqttConnected()) {
                                                if (presenterImple != null)
                                                    presenterImple.launchCallsScreen(currentRoomId, callId, callType);
                                            } else {
                                                Log.d(TAG, "onNext: can not subscribe to callId mqtt is not connected");
                                            }
                                        }
                                        break;
                                    case RESPONSE_CODE_ALREADY_IN_CALL:
                                        presenterImple.onFailure(jsonObject.getString("message"));
                                        break;
                                    default:
                                        Log.d(TAG, "ondefault: " + result);
                                        break;
                                }
                            } else {
                                Log.d(TAG, "ondefault: " + result);
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "ondefault: " + result);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        presenterImple.stopProgressBar();
                        Toast.makeText(((TeleCallActivity) presenterImple).getApplicationContext(), "Some things went wrong", Toast.LENGTH_LONG).show();
                        Log.d(TAG, "onErroir: " + error);
                    }
                });
    }

    interface UpdateStatusPresenterImple {
        void startProgressBar();

        void stopProgressBar();

        void onSuccess(String msg, boolean bookingDetailsRefreshed);

        void onSuccessCancelReason(CancelPojo cancelPojo);

        void onCancelBooking(String msg);

        void onFailure(String failureMsg);

        void onFailure();

        void onNewToken(String newToken);

        void sessionExpired(String msg);

        void launchCallsScreen(String roomId, String callId, String callType);
    }
}
