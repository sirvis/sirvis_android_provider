package com.sirvis.pro.bookingflow;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CancelListAdapter;
import com.sirvis.pro.bookingflow.review.CustomerReviewsActivity;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CalendarEventHelper;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;


/**
 * Created by murashid on 29-Sep-17.
 * <h1>InCallActivity</h1>
 * Activity for starting and enting the event time
 */
public class InCallActivity extends AppCompatActivity implements UpdateStatusPresenter.UpdateStatusPresenterImple, View.OnClickListener, CancelListAdapter.CancelSelected {

    private static final String TAG = "EventStarted";
    private UpdateStatusPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private SeekBar seekBar;

    private Booking booking;

    private String bookidId ="";
    private String cancelId = "";
    private Typeface fontBold ;
    private Typeface fontMedium ;

    private BroadcastReceiver receiver;

    private double hourFee = 0;

    private FloatingActionButton fabCommunicate,fabCall,fabMsg;
    private Animation fade_open, fade_close,rotate_forward,rotate_backward;
    private boolean isFabOpen = false;
    private TextView tvMessageCountInPlus, tvMessageCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_call);

        initView();
    }


    @SuppressLint("SetTextI18n")
    private void initView()
    {

        sessionManager = SessionManager.getSessionManager(this);
        presenter = new UpdateStatusPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updatingStatus));
        progressDialog.setCancelable(false);

        fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontLight = Utility.getFontLight(this);

        TextView tveventId = findViewById(R.id.tveventId);
        tveventId.setTypeface(fontBold);

        tvMessageCountInPlus = findViewById(R.id.tvMessageCountInPlus);
        tvMessageCount = findViewById(R.id.tvMessageCount);
        TextView tvSeekbarText = findViewById(R.id.tvSeekbarText);
        seekBar = findViewById(R.id.seekBar);
        tvMessageCountInPlus.setTypeface(fontRegular);
        tvMessageCount.setTypeface(fontRegular);
        tvSeekbarText.setTypeface(fontRegular);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivBackButton.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        booking = (Booking) bundle.getSerializable("booking");

        BookingDetailsFragment bookingDetailsFragment = BookingDetailsFragment.newInstance(booking, false);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragBooking, bookingDetailsFragment).commit();
        bookidId = booking.getBookingId();

        if(booking.getServiceType().equals("2")) {
            hourFee = Double.parseDouble(booking.getAccounting().getTotalActualHourFee());
        }

        tveventId.setText(getText(R.string.jobId)+" "+booking.getBookingId());

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() > 75)
                {
                    JSONObject jsonObject = new JSONObject();
                    try {

                        jsonObject.put("bookingId",bookidId);
                        jsonObject.put("status", VariableConstant.JOB_TIMER_COMPLETED);
                        jsonObject.put("latitude",sessionManager.getCurrentLat());
                        jsonObject.put("longitude",sessionManager.getCurrentLng());
                        jsonObject.put("distance",""+sessionManager.getOnJobDistance(booking.getBookingId()));
                        Log.d(TAG, "c: "+jsonObject);
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    seekBar.setProgress(100);
                    presenter.updateStaus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject,false);
                }
                else
                {
                    seekBar.setProgress(0);
                }
            }
        });


        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT))
                {
                    setChatCount();
                }
                else
                {
                    String cancelId = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    if (cancelId.equals(booking.getBookingId()))
                    {
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        closeActivity();
                        // DialogHelper.customAlertDialogCloseActivity(InCallActivity.this, header, msg, getString(R.string.oK));
                    }
                }
            }
        };

        registerReceiver(receiver, filter);

        fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        fade_close = AnimationUtils.loadAnimation(this,R.anim.fade_close);
        rotate_forward = AnimationUtils.loadAnimation(this,R.anim.rotate_center_to_left);
        rotate_backward = AnimationUtils.loadAnimation(this,R.anim.rotate_left_to_center);

        fabCommunicate = findViewById(R.id.fabCommunicate);
        fabCall = findViewById(R.id.fabCall);
        fabMsg = findViewById(R.id.fabMsg);

        fabCommunicate.setOnClickListener(this);
        fabMsg.setOnClickListener(this);
        fabCall.setOnClickListener(this);

        /*
         * Bug id : Trello 189
         * Bug Description : There is no option to cancel a job with reason for InCall
         * Fixed Description : Added Cancel textview in layout and handled the action.
         * Dev : Murashid
         * Date : 29-11-2018
         * */
        TextView tvCancel = findViewById(R.id.tvCancel);
        tvCancel.setTypeface(fontMedium);
        tvCancel.setOnClickListener(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setChatCount();
    }

    private void setChatCount() {
        if(sessionManager.getChatCount(booking.getBookingId()) != 0)
        {
            tvMessageCountInPlus.setText(""+sessionManager.getChatCount(booking.getBookingId()));
            tvMessageCount.setText(""+sessionManager.getChatCount(booking.getBookingId()));
            if(isFabOpen)
            {
                tvMessageCount.setVisibility(View.VISIBLE);
                tvMessageCountInPlus.setVisibility(View.GONE);
            }
            else
            {
                tvMessageCountInPlus.setVisibility(View.VISIBLE);
                tvMessageCount.setVisibility(View.GONE);
            }
        }
        else
        {
            tvMessageCountInPlus.setVisibility(View.GONE);
            tvMessageCount.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String msg, boolean bookingDetailsRefreshed) {
        VariableConstant.IS_BOOKING_UPDATED = true;

        seekBar.setProgress(0);

        double travelFee = 0;
        double total = 0;
        try {
            Log.d("InCallActivity", "onSuccess: "+msg);
            JSONObject jsonObjectResponse = new JSONObject(msg);
            JSONObject jsonData = jsonObjectResponse.getJSONObject("data");

            total = Double.parseDouble(booking.getAccounting().getTotal())- Double.parseDouble(booking.getAccounting().getLastDues());
            travelFee = Double.parseDouble(jsonData.getString("travelFee"));
            total += travelFee;

            if(booking.getServiceType().equals("2"))
            {
                total = total - hourFee;
                booking.getAccounting().setTotalActualJobTimeMinutes(jsonData.getString("totalActualJobTimeMinutes"));
                booking.getAccounting().setTotalActualHourFee(jsonData.getString("totalActualHourFee"));
                total = total + Double.parseDouble(jsonData.getString("totalActualHourFee"));
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        booking.getAccounting().setTravelFee(""+travelFee);
        booking.getAccounting().setTotal(""+total);

        sessionManager.setBookingStr(presenter.getBookingStr(booking,sessionManager.getBookingStr(),VariableConstant.JOB_TIMER_COMPLETED));

        booking.setStatus(VariableConstant.JOB_TIMER_COMPLETED);
        finish();
        Intent intent = new Intent(this,InvoiceActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking",booking);
        intent.putExtras(bundle);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }

        AppController.getInstance().getMixpanelHelper().bookingStatus(MixpanelEvents.JobCompleted.value,bookidId);

    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cancel, null);
        alertDialogBuilder.setView(view);
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvSubmit = view.findViewById(R.id.tvSubmit);
        ImageView ivClose = view.findViewById(R.id.ivClose);

        tvTitle.setTypeface(fontMedium);
        tvSubmit.setTypeface(fontBold);
        RecyclerView rvCancel = view.findViewById(R.id.rvCancel);
        rvCancel.setLayoutManager(new LinearLayoutManager(this));
        CancelListAdapter cancelListAdapter = new CancelListAdapter(this, cancelPojo.getData(), this);
        rvCancel.setAdapter(cancelListAdapter);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!cancelId.equals("")) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("bookingId", bookidId);
                        jsonObject.put("resonId", cancelId);
                        progressDialog.setMessage(getString(R.string.loading));
                        presenter.cancelBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                        alertDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(InCallActivity.this, getString(R.string.plsSelectCancel), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancelId = "";
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onCancelBooking(String msg) {
        VariableConstant.IS_BOOKING_UPDATED = true;
        DialogHelper.customAlertDialogCloseActivity(this,getString(R.string.alert),msg,getString(R.string.oK));
        try {
            if(booking.getReminderId() !=null && !booking.getReminderId().equals(""))
            {
                CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                calendarEventHelper.deleteEvent(booking.getReminderId());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        seekBar.setProgress(0);
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        seekBar.setProgress(0);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void launchCallsScreen(String roomId, String callId, String callType) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.llCutomerDetails:
                Intent intent = new Intent(this,CustomerReviewsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else {
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                }
                break;

            case R.id.ivBackButton:
                closeActivity();
                break;

            case R.id.fabCommunicate:
                animateFAB();
                break;

            case R.id.fabCall:
                animateFAB();
                String uri = "tel:"+booking.getPhone() ;
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse(uri));
                startActivity(callIntent);
                break;

            case R.id.fabMsg:
                animateFAB();
                sessionManager.setChatBookingID(booking.getBookingId());
                sessionManager.setChatCustomerName(booking.getFirstName() + " "+booking.getLastName());
                sessionManager.setChatCustomerPic(booking.getProfilePic());
                sessionManager.setChatCustomerID(booking.getCustomerId());

                Intent chatIntent = new Intent(this,ChattingActivity.class);
                startActivity(chatIntent);
                break;

            case R.id.tvCancel:
                progressDialog.setMessage(getString(R.string.loading));
                presenter.getCancelReason(booking.getBookingId());
                break;

        }
    }

    public void animateFAB(){

        if(isFabOpen){
            isFabOpen = false;
            fabCommunicate.startAnimation(rotate_backward);
            fabMsg.startAnimation(fade_close);
            fabCall.startAnimation(fade_close);
            fabMsg.setClickable(false);
            fabCall.setClickable(false);
            setChatCount();
        }
        else
        {
            isFabOpen = true;
            fabCommunicate.startAnimation(rotate_forward);
            fabMsg.startAnimation(fade_open);
            fabCall.startAnimation(fade_open);
            fabMsg.setClickable(true);
            fabCall.setClickable(true);
            setChatCount();
        }
    }

    @Override
    public void onCancelSeleted(String id) {
        cancelId = id;
    }
}
