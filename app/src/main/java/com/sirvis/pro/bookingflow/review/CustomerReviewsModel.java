package com.sirvis.pro.bookingflow.review;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.review.ReviewData;
import com.sirvis.pro.pojo.profile.review.ReviewPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 01-Nov-17.
 * Model for CustomerReviewsActivity
 * @see CustomerReviewsActivity
 */

public class CustomerReviewsModel {
    private static final String TAG = "ReviewModel";
    private CustomerReviewsModelImple modelImplement;

    CustomerReviewsModel(CustomerReviewsModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for geting customer reviews based on the page index and customer details
     * @param sessiontoken session Token
     * @param pageNo page index
     */
    void getCustomerDetails(String sessiontoken,int pageNo,String customerId)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.CUSTOMER_REVIEW_AND_RATING +"/"+pageNo+"/"+customerId, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        ReviewPojo reviewPojo = gson.fromJson(result,ReviewPojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(reviewPojo.getData());
                        } else {
                            modelImplement.onFailure(reviewPojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ReviewModelImple interface for Presenter implementation
     */
    interface CustomerReviewsModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ReviewData reviewData);
    }
}
