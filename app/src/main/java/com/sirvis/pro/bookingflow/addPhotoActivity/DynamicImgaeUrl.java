package com.sirvis.pro.bookingflow.addPhotoActivity;

public class DynamicImgaeUrl {
    private int type = 1 ;// 1 => from url, 2 => from file
    private String imageUrl;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
