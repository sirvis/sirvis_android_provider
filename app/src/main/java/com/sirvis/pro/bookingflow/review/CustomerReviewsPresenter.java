package com.sirvis.pro.bookingflow.review;

import com.sirvis.pro.pojo.profile.review.ReviewData;

/**
 * Created by murashid on 01-Nov-17.
 * Presenter for customerReviewActivity
 * @see CustomerReviewsActivity
 */

public class CustomerReviewsPresenter implements CustomerReviewsModel.CustomerReviewsModelImple {
    private CustomerReviewsModel model;
    private CustomerReviewsPresenterImple presenterImple;

    CustomerReviewsPresenter(CustomerReviewsPresenterImple presenterImple) {
        model = new CustomerReviewsModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session Token
     * @param pageNo index of the page
     */
    void getReview(String sessionToken,int pageNo,String customerId){
        presenterImple.startProgressBar();
        model.getCustomerDetails(sessionToken,pageNo, customerId);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(ReviewData reviewData) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(reviewData);
    }

    /**
     * ReviewPresenterImple interface for view implementation
     */
    interface CustomerReviewsPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(ReviewData reviewData);
    }

}
