package com.sirvis.pro.telecall.callservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.sirvis.pro.mqtt.MqttEvents;
import com.sirvis.pro.mqtt.MqttHelper;
import com.sirvis.pro.telecall.UtilityVideoCall;
import com.sirvis.pro.utility.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;


public class OnMyService extends Service {
    SessionManager sharedPrefs;

    MqttHelper manager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPrefs = SessionManager.getSessionManager(this);
        Log.e("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("ClearFromRecentService", "Service Destroyed");
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", 1);
            manager.publish(MqttEvents.CallsAvailability.value + "/" + sharedPrefs.getProviderId(), obj, 0, true);//UserId
            UtilityVideoCall.getInstance().setActiveOnACall(false, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        try {
            String cid = sharedPrefs.getProviderId();
            Log.e("ClearFromRecentService", "END " + cid + " MQTTRESPO " + manager.isMqttConnected());
            //Code here

            if (manager.isMqttConnected()) {
                if (!cid.equals("")) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("status", 1);
                        manager.publish(MqttEvents.CallsAvailability.value + "/" + sharedPrefs.getProviderId(), obj, 0, true);//UserId
                        UtilityVideoCall.getInstance().setActiveOnACall(false, false);
                        //    manager.subscribeToTopic(MqttEvents.Calls.value+"/"+sharedPrefs.getSID(),1);
                        //  mqttManager.subscribeToTopic(MqttEvents.Calls.value+"/5c177bf2f56745d4b143e1a6",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                 /*   manager.unsubscribeToTopic(MqttEvents.JobStatus.value + "/" +cid);
                    manager.unsubscribeToTopic(MqttEvents.Provider.value + "/" +cid);
                    manager.unsubscribeToTopic(MqttEvents.Message.value + "/" + cid);
                    //  manager.publish(MqttEvents.CallsAvailability.value+"/"+cid);
                    if(!VariableConstant.LiveTrackBookingPid.equals(""))
                        manager.unsubscribeToTopic(MqttEvents.LiveTrack.value + "/" + VariableConstant.LiveTrackBookingPid);
                    VariableConstant.LiveTrackBookingPid = "";*/
                    manager.disconnect(/*cid*/);
                }
            } else {
                if (!cid.equals("")) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("status", 1);
                        manager.publish(MqttEvents.CallsAvailability.value + "/" + sharedPrefs.getProviderId(), obj, 0, true);//UserId
                        UtilityVideoCall.getInstance().setActiveOnACall(false, false);
                        //    manager.subscribeToTopic(MqttEvents.Calls.value+"/"+sharedPrefs.getSID(),1);
                        //  mqttManager.subscribeToTopic(MqttEvents.Calls.value+"/5c177bf2f56745d4b143e1a6",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            /*if(!sharedPrefs.getGuestLogin())
            {
                sharedPrefs.setAppOpen(false);
            }
*/
            stopSelf();
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

}
