package com.sirvis.pro.telecall.callservice;

import android.app.Service;
import android.util.Log;

import com.sirvis.pro.mqtt.MqttHelper;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

public class VideoCallPresenter implements VideoCallContract.Presenter {

    private String TAG = VideoCallPresenter.class.getSimpleName();

    private VideoCallContract.View view;

    private MqttHelper mqttHelper;


    public VideoCallPresenter(Service service) {

        this.view = (VideoCallContract.View) service;
//        compositeDisposable = new CompositeDisposable();
        mqttHelper = AppController.getInstance().getMqttHelper();
    }

    @Override
    public void initCall(String sessionToken, String currentRoomId, String callerId, String video) {

    }

    @Override
    public void endCall(String callToken, String callID, String callFrom) {
        Log.d(TAG, "endCall: "+callToken+"-"+callID+"-"+callFrom);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("callId", callID);
            jsonObject.put("callFrom", callFrom);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(callToken, ServiceUrl.CALL, OkHttp3ConnectionStatusCode.Request_type.DELETE, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        try {
                                            Log.d(TAG, "onNext: endCallResponse: " + result);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (view != null)
                                            view.onRejectSuccess();
                                        break;
                                    default:
                                        if (view != null)
                                            view.onRejectSuccess();
                                        Log.d(TAG, "ondefault: " + result);
                                        break;
                                }
                            } else {
                                Log.d(TAG, "ondefault: " + result);
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "ondefault: " + result);
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(String error) {
                        if (view != null)
                            view.onRejectSuccess();
                        Log.d(TAG, "onErroir: " + error);
                    }
                });
    }


    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void dispose() {
//        compositeDisposable.clear();
    }


}
