package com.sirvis.pro.telecall;

import java.util.ArrayList;

/**
 * Created by Ali on 1/7/2019.
 */
public class UtilityVideoCall
{

    private boolean callMinimized = false;
    private ArrayList<String> colors;
    private boolean activeOnACall = false;
    private boolean firstTimeAfterCallMinimized = false;
    private int activeActivitiesCount;


    public static UtilityVideoCall utilityVideoCall;
    private String activeCallId, activeCallerId;


    public static UtilityVideoCall getInstance()
    {
        if(utilityVideoCall == null)
        {
            utilityVideoCall = new UtilityVideoCall();
            return utilityVideoCall;
        }else
            return utilityVideoCall;
    }

    private UtilityVideoCall()
    {
        if(colors==null)
            setBackgroundColorArray();

    }

    private void setBackgroundColorArray() {

        colors = new ArrayList<>();

        colors.add("#FFCDD2");
        colors.add("#D1C4E9");
        colors.add("#B3E5FC");
        colors.add("#C8E6C9");
        colors.add("#FFF9C4");
        colors.add("#FFCCBC");
        colors.add("#CFD8DC");
        colors.add("#F8BBD0");
        colors.add("#C5CAE9");
        colors.add("#B2EBF2");
        colors.add("#DCEDC8");
        colors.add("#FFECB3");
        colors.add("#D7CCC8");
        colors.add("#F5F5F5");
        colors.add("#FFE0B2");
        colors.add("#F0F4C3");
        colors.add("#B2DFDB");
        colors.add("#BBDEFB");
        colors.add("#E1BEE7");


    }

    public String getColorCode(int position) {
        return colors.get(position);

    }

    public boolean isCallMinimized() {
        return callMinimized;
    }

    public void setCallMinimized(boolean callMinimized) {
        this.callMinimized = callMinimized;


    }

    public boolean isActiveOnACall() {
        return activeOnACall;
    }

    public void setActiveOnACall(boolean activeOnACall, boolean notCallCut) {
        this.activeOnACall = activeOnACall;

        if (!activeOnACall && notCallCut) {
            this.callMinimized = false;
        }
    }

    public void setActiveCallId(String callId)
    {
        activeCallId = callId;
    }

    public String getActiveCallId() {
        return activeCallId;
    }

    public void setActiveCallerId(String activeCallerId) {

        this.activeCallerId = activeCallerId;
    }

    public boolean isFirstTimeAfterCallMinimized() {
        return firstTimeAfterCallMinimized;
    }

    public void setFirstTimeAfterCallMinimized(boolean firstTimeAfterCallMinimized) {
        this.firstTimeAfterCallMinimized = firstTimeAfterCallMinimized;
    }

    public int getActiveActivitiesCount() {
        return activeActivitiesCount;
    }

}
