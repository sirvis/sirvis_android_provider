package com.sirvis.pro.telecall.callservice;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.sirvis.pro.R;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.mqtt.MqttEvents;
import com.sirvis.pro.mqtt.MqttHelper;
import com.sirvis.pro.pojo.callpojo.CallActions;
import com.sirvis.pro.telecall.RxCallInfo;
import com.sirvis.pro.telecall.UtilityVideoCall;
import com.sirvis.pro.telecall.utility.WebRTC.AppRTCAudioManager;
import com.sirvis.pro.telecall.utility.WebRTC.AppRTCClient;
import com.sirvis.pro.telecall.utility.WebRTC.DirectRTCClient;
import com.sirvis.pro.telecall.utility.WebRTC.PeerConnectionClient;
import com.sirvis.pro.telecall.utility.WebRTC.TextDrawable;
import com.sirvis.pro.telecall.utility.WebRTC.WebSocketRTCClient;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.EglBase;
import org.webrtc.FileVideoCapturer;
import org.webrtc.IceCandidate;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.RendererCommon;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSink;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.reactivex.Observer;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;

import static android.support.v4.app.NotificationCompat.DEFAULT_SOUND;
import static android.support.v4.app.NotificationCompat.DEFAULT_VIBRATE;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_AECDUMP_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_AUDIOCODEC;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_AUDIO_BITRATE;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_CAMERA2;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_CAPTURETOTEXTURE_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_DATA_CHANNEL_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_DISABLE_BUILT_IN_AEC;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_DISABLE_BUILT_IN_AGC;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_DISABLE_BUILT_IN_NS;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_DISABLE_WEBRTC_AGC_AND_HPF;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_ENABLE_RTCEVENTLOG;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_FLEXFEC_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_HWCODEC_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_ID;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_LOOPBACK;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_MAX_RETRANSMITS;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_MAX_RETRANSMITS_MS;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_NEGOTIATED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_NOAUDIOPROCESSING_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_OPENSLES_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_ORDERED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_PROTOCOL;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_ROOMID;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_SAVE_INPUT_AUDIO_TO_FILE_ENABLED;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_TRACING;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_URLPARAMETERS;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEOCODEC;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEO_BITRATE;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEO_CALL;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEO_FILE_AS_CAMERA;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEO_FPS;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEO_HEIGHT;
import static com.sirvis.pro.telecall.utility.WebRTC.Constants.EXTRA_VIDEO_WIDTH;


/**
 * Created by moda on 16/08/17.
 */

public class VideoCallService extends Service implements AppRTCClient.SignalingEvents, PeerConnectionClient.PeerConnectionEvents, VideoCallEvents,
        VideoCallContract.View {


    /* public static final String EXTRA_CALLID = "org.appspot.apprtc.CALLID";
     public static final String EXTRA_ROOMID = "org.appspot.apprtc.ROOMID";
     public static final String EXTRA_LOOPBACK = "org.appspot.apprtc.LOOPBACK";
     public static final String EXTRA_VIDEO_CALL = "org.appspot.apprtc.VIDEO_CALL";
     public static final String EXTRA_VIDEO_WIDTH = "org.appspot.apprtc.VIDEO_WIDTH";
     public static final String EXTRA_VIDEO_HEIGHT = "org.appspot.apprtc.VIDEO_HEIGHT";
     public static final String EXTRA_VIDEO_FPS = "org.appspot.apprtc.VIDEO_FPS";
     public static final String EXTRA_VIDEO_CAPTUREQUALITYSLIDER_ENABLED =
             "org.appsopt.apprtc.VIDEO_CAPTUREQUALITYSLIDER";
     public static final String EXTRA_VIDEO_BITRATE = "org.appspot.apprtc.VIDEO_BITRATE";
     public static final String EXTRA_VIDEOCODEC = "org.appspot.apprtc.VIDEOCODEC";
     public static final String EXTRA_HWCODEC_ENABLED = "org.appspot.apprtc.HWCODEC";
     public static final String EXTRA_CAPTURETOTEXTURE_ENABLED = "org.appspot.apprtc.CAPTURETOTEXTURE";
     public static final String EXTRA_AUDIO_BITRATE = "org.appspot.apprtc.AUDIO_BITRATE";
     public static final String EXTRA_AUDIOCODEC = "org.appspot.apprtc.AUDIOCODEC";
     public static final String EXTRA_NOAUDIOPROCESSING_ENABLED =
             "org.appspot.apprtc.NOAUDIOPROCESSING";
     public static final String EXTRA_AECDUMP_ENABLED = "org.appspot.apprtc.AECDUMP";
     public static final String EXTRA_OPENSLES_ENABLED = "org.appspot.apprtc.OPENSLES";
     public static final String EXTRA_DISPLAY_HUD = "org.appspot.apprtc.DISPLAY_HUD";
     public static final String EXTRA_TRACING = "org.appspot.apprtc.TRACING";
     public static final String EXTRA_CMDLINE = "org.appspot.apprtc.CMDLINE";
     public static final String EXTRA_RUNTIME = "org.appspot.apprtc.RUNTIME";*/
    private static final long MAX_RECONNECT_TIME = 90000;//90 secs
    private static final String TAG = "CallRTCClient";
    // List of mandatory application permissions.
    private static final String[] MANDATORY_PERMISSIONS = {
            "android.permission.RECORD_AUDIO", "android.permission.MODIFY_AUDIO_SETTINGS",
            "android.permission.INTERNET"
    };
    // Peer connection statistics callback period in ms.
    private static final int STAT_CALLBACK_PERIOD = 1000;
    private final ProxyVideoSink remoteProxyRenderer = new ProxyVideoSink();
    private final ProxyVideoSink localProxyVideoSink = new ProxyVideoSink();
    private final List<VideoSink> remoteSinks = new ArrayList<>();
    private final EglBase rootEglBase = EglBase.create();
    // public static Bus bus = AppController.getBus();
    public boolean isVideoAvailable = true;
    VideoCallContract.Presenter presenter;
    // Controls
    boolean disconnect;
    MediaPlayer mp, reconnectTonePlayer;
    CountDownTimer timer;
    /******************************I am doing*************************/

    MqttHelper mqttManager;
    SessionManager manager;
    private boolean cleanUpRequested;
    private String roomId;
    private boolean timerAlreadyStarted;
    private TextView tvReconnecting;
    private boolean connected;
    private CountDownTimer reconnectTimer;
    @Nullable
    private PeerConnectionClient peerConnectionClient;
    @Nullable
    private AppRTCClient appRtcClient;
    @Nullable
    private AppRTCClient.SignalingParameters signalingParameters;
    @Nullable
    private AppRTCAudioManager audioManager;
    private Toast logToast;
    private AppRTCClient.RoomConnectionParameters roomConnectionParameters;
    @Nullable
    private PeerConnectionClient.PeerConnectionParameters peerConnectionParameters;
    ;
    private SurfaceViewRenderer localRender;
    private SurfaceViewRenderer remoteRender;
    private RendererCommon.ScalingType scalingType;
    private boolean commandLineRun;
    private int runTimeMs;
    private boolean iceConnected;
    private boolean isError;
    private boolean callControlFragmentVisible = true;
    private long callStartedTimeMs = 0;
    private FrameLayout root;
    private Context mContext;
    private View videoCallView, callHeaderView;
    private WindowManager windowManager;
    private TextView callHeaderTv;
    private Intent intent;
    private WindowManager.LayoutParams params;
    private Handler handler;
    /**
     * For the fragment controls which are now moved into the UI
     */


    private ImageView mute, video;
    private TextView tvStopWatch, tvCallerName;
    private boolean isMute = false, isVideoShow = false;
    private ImageView callerImage;
    /**
     * Call control interface for container activity.
     */

    private String imageUrl = "";
    private ImageView initiateChat;
    private boolean isFrontCamera = false;
    private Chronometer stopWatchHeader;
    private long countUp, countUpHeader;
    private RelativeLayout parentRl;
    private SurfaceViewRenderer remoteRenderHeader;
    private String currentRoomId;
    private String callerId;
    private boolean isSwappedFeeds = true;
    private SessionManager sesionManager;
    private String bookingEndtime = "";
    private CountDownTimer disconnectTimer;

    private static void setNotificationChannel(NotificationManager notificationManager, String id, String name, String description) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(mChannel);

        }
    }

    /******************************End doing*************************/

    @Override
    public void onCreate() {
        super.onCreate();
        mqttManager = AppController.getInstance().getMqttHelper();
        manager = SessionManager.getSessionManager(this);
        manager.setisTeleCallRunning(true);
        mContext = this;
        sesionManager = SessionManager.getSessionManager(mContext);
        presenter = new VideoCallPresenter(this);
        // Create video renderers.
        startService(new Intent(this, OnMyService.class));
        videoCallView = LayoutInflater.from(this).inflate(R.layout.video_call_service, null);
        //  bus.register(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            videoCallView
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else {
            videoCallView
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        }
        // Set window styles for fullscreen-window size. Needs to be done before
        // adding content.

        tvStopWatch = (TextView) videoCallView.findViewById(R.id.tvStopWatch);
        /*
         * For showing own camera view while making a call
         */

        Utility.printLog(TAG, "onCreate: ");
        addMainLayout();
        // Create UI controls.
        localRender = (SurfaceViewRenderer) videoCallView.findViewById(R.id.local_video_view);
        remoteRender = (SurfaceViewRenderer) videoCallView.findViewById(R.id.remote_video_view);

        // Swap feeds on pip view click.

        if (localRender != null) {
            localRender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setSwappedFeeds(!isSwappedFeeds);
                }
            });
        }

        // Show/hide call control fragment on view click.

        if (remoteRender != null) {
            remoteRender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleCallControlFragmentVisibility();
                }
            });
        }

//        tvReconnecting = (TextView) videoCallView.findViewById(R.id.tvReconnecting);

        remoteSinks.add(remoteProxyRenderer);
//        remoteSinks.add(remoteHeaderProxyRenderer);


        // Create video renderers.
        localRender.init(rootEglBase.getEglBaseContext(), null);
//        localRender.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);

        remoteRender.init(rootEglBase.getEglBaseContext(), null);
//        remoteRender.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);

        localRender.setZOrderMediaOverlay(true);
        localRender.setEnableHardwareScaler(true /* enabled */);
        remoteRender.setEnableHardwareScaler(false /* enabled */);
        // Start with local feed in fullscreen and swap it to the pip when the call is connected.
        setSwappedFeeds(false /* isSwappedFeeds */);

        // Check for mandatory permissions.
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {

                stopSelf();
                return;
            }
        }
//        Logging.enableLogToDebugOutput(Logging.Severity.LS_ERROR);
//        Thread.setDefaultUncaughtExceptionHandler(new UnhandledExceptionHandler(this));


        handler = new Handler(Looper.getMainLooper());
        root = (FrameLayout) videoCallView.findViewById(R.id.root);
        /*
         * Mqtt
         */


        /*
         * To check if the call screen has been opened for the incoming call or the outgoing call
         *
         */
        /*
         * For the outgoing call have to check for the
         */


        iceConnected = false;
        signalingParameters = null;
        scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FILL;

        // Create UI controls.
        localRender = (SurfaceViewRenderer) videoCallView.findViewById(R.id.local_video_view);
        localRender.setMirror(true);
        remoteRender = (SurfaceViewRenderer) videoCallView.findViewById(R.id.remote_video_view);
        remoteRender.setMirror(true);


        // Show/hide call control fragment on view click.
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCallControlFragmentVisibility();
            }
        };

        try {
            // localRender.setOnClickListener(listener);
            remoteRender.setOnClickListener(listener);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        localRender.requestLayout();

        // Check for mandatory permissions.
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                logAndToast("Permission " + permission + " is not granted");
                stopSelf();

                return;
            }


        }

        initializeRxJava();
    }

    private void setSwappedFeeds(boolean isSwappedFeeds) {
        this.isSwappedFeeds = isSwappedFeeds;


        if (remoteRender != null) {
            remoteRender.setMirror(isSwappedFeeds);
        }

        if (localRender != null) {
            localRender.setMirror(!isSwappedFeeds);
        }

        //For the remote header
        if (remoteRenderHeader != null)
            remoteRenderHeader.setMirror(isSwappedFeeds);

        localProxyVideoSink.setTarget(isSwappedFeeds ? remoteRender : localRender);
        remoteProxyRenderer.setTarget(isSwappedFeeds ? localRender : remoteRender);
    }

    @Override
    public IBinder onBind(Intent intent) {


        this.intent = intent;
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        this.intent = intent;

        if (intent != null) {

            if (intent.getExtras() != null && !intent.getExtras().getBoolean("isIncomingCall", true)) {

                mqttManager.subscribeToTopic(MqttEvents.Call.value + "/" + intent.getExtras().getString(EXTRA_ROOMID), 0);
                mqttManager.subscribeToTopic(MqttEvents.CallsAvailability.value + "/" + intent.getExtras().getString(EXTRA_ROOMID), 0);
                mp = MediaPlayer.create(this, R.raw.calling);
                mp.setLooping(true);
                mp.start();
                startCallProcedure(false);

            } else {
                tvStopWatch.setText(getString(R.string.connecting));
                startCallProcedure(true);
            }


        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (disconnectTimer != null) {
            disconnectTimer.cancel();
            disconnectTimer = null;
        }
        /*if (!mqttManager.isMqttConnected()) {*/

        //disconnect from mqtt server with whatever mqttoptoins setting and create a new connection with setWill.
//        mqttManager.disconnect();
        mqttManager.createMQttConnection(manager.getProviderId(), true);
        /*}*/
        if (!disconnect) {

            onCallHangUp(2, false);
        }
//        destroyOnAppCrashed(false);

        try {
            Utility.printLog(TAG, "log111");
            // disconnect();
            //  bus.unregister(this);
            presenter.dispose();
            presenter.dropView();
            removeViews();
            manager.setisTeleCallRunning(false);
        } catch (Exception e) {
            manager.setisTeleCallRunning(false);
            e.printStackTrace();
        }
        super.onDestroy();
//        if (windowManager == null) {
//            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
//        }
//        try {
//
////            if (com.telecall.UtilityVideoCall.getInstance().isCallMinimized()) {
////
////                if (callHeaderView != null) {
////                    windowManager.removeView(callHeaderView);
////                }
//            if (com.telecall.UtilityVideoCall.getInstance().isCallMinimized()) {
//                try {
//                    if (callHeaderView != null) {
//                        windowManager.removeView(callHeaderView);
//                    }
//
//
//                } catch (IllegalArgumentException e) {
//
//                    if (videoCallView != null) {
//
//
//                        /*
//                         * For clearing of the full screen UI mode
//                         */
//                        videoCallView
//                                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
//
//                        windowManager.removeView(videoCallView);
//                    }
//
//
//                }
//            } else {
//
//                try {
//                    if (videoCallView != null) {
//
//
//                        /*
//                         * For clearing of the full screen UI mode
//                         */
//                        videoCallView
//                                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
//
//                        windowManager.removeView(videoCallView);
//                    }
//                } catch (IllegalArgumentException e) {
//
//
//                    if (callHeaderView != null) {
//                        windowManager.removeView(callHeaderView);
//                    }
//
//                }
//            }
//
////            } else {
////                if (videoCallView != null) {
////
////
////                    /*
////                     * For clearing of the full screen UI mode
////                     */
////                    videoCallView
////                            .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
////
////                    windowManager.removeView(videoCallView);
////                }
////            }
//
//
//            rootEglBase.release();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        // hideCameraView(false);
    }

    private void removeViews() {

        if (windowManager == null) {
            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        }
        try {

            if (UtilityVideoCall.getInstance().isCallMinimized()) {
                try {
                    if (callHeaderView != null) {
                        windowManager.removeView(callHeaderView);
                    }


                } catch (IllegalArgumentException e) {

                    if (videoCallView != null) {


                        /*
                         * For clearing of the full screen UI mode
                         */
                        videoCallView
                                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                        try {
                            windowManager.removeView(videoCallView);
                        } catch (IllegalArgumentException ef) {
                        }
                    }


                }
            } else {

                try {
                    if (videoCallView != null) {


                        /*
                         * For clearing of the full screen UI mode
                         */
                        videoCallView
                                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

                        windowManager.removeView(videoCallView);
                    }
                } catch (IllegalArgumentException e) {


                    if (callHeaderView != null) {
                        try {
                            windowManager.removeView(callHeaderView);
                        } catch (IllegalArgumentException ef) {
                        }
                    }

                }
            }

           /* try {
                //To handle already released exception

                rootEglBase.release();
            } catch (Exception e) {
                e.printStackTrace();
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // OnCallEvents interface implementation.
    @Override
    public void onCallHangUp(int val, boolean received) {

        if (!disconnect) {
            disconnect = true;

            /*
             * Timeout from the sender side
             */

            if (intent != null && intent.getExtras() != null) {
                try {


                    if (!received) {

                        JSONObject obj = new JSONObject();

                        obj.put("callId", intent.getExtras().getString(EXTRA_ROOMID));
                        obj.put("userId", sesionManager.getProviderId());
                        obj.put("type", val);


                        mqttManager.publish(MqttEvents.Calls.value + "/" + intent.getExtras().getString("callerId"), obj, 0, false);
//                        AppController.getInstance().removeCurrentCallDetails();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        disconnect();
        /*try {
            Handler handler = new Handler();
            handler.post(new Runnable() {
                @Override
                public void run() {

                        if (windowManager == null) {
                            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                        }
                    if (callHeaderView != null) {

                        windowManager.removeViewImmediate(callHeaderView);
                    } if (callHeaderView != null) {
                        windowManager.removeViewImmediate(videoCallView);
                    }

                }
            });
        }  catch (final IllegalArgumentException e) {
            Utility.printLog(TAG, "onCallHangUp: crashed");
        } catch (final Exception e) {
            e.printStackTrace();
        }
        finally {
            if(windowManager!=null)
            {
                windowManager=null;
            }
        }*/
    }

    @Override
    public void onCameraSwitch() {
        /*if (peerConnectionClient != null) {
            if (!isFrontCamera) {
                localRender.setMirror(false);
            } else {
                localRender.setMirror(true);
            }
            peerConnectionClient.switchCamera();
        }*/
        if (peerConnectionClient != null) {
            peerConnectionClient.switchCamera();
        }
    }

    @Override
    public void onMute() {
        isMute = !isMute;
        audioManager.setSpeakerphoneOn(isMute);
        audioManager.setMicrophoneMute(isMute);
    }

    @Override
    public void onVideoShow() {
        isVideoAvailable = !isVideoAvailable;
        if (isVideoAvailable) {


            if (isSwappedFeeds) {

                if (remoteRender != null)
                    remoteRender.setBackgroundColor(Color.parseColor("#00000000"));
            } else {

                if (localRender != null)
                    localRender.setBackgroundColor(Color.parseColor("#00000000"));
            }


            if (peerConnectionClient != null) {
                peerConnectionClient.startVideoSource();
            }


            if (intent != null && intent.getExtras() != null) {
                try {
                    JSONObject obj = new JSONObject();

                    obj.put("callId", intent.getExtras().getString(EXTRA_ROOMID));
                    obj.put("userId", sesionManager.getProviderId());
                    obj.put("type", 5);


                    mqttManager.publish(MqttEvents.Calls.value + "/" + intent.getExtras().getString("callerId"), obj, 0, false);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        } else {


            if (isSwappedFeeds) {
                if (remoteRender != null)
                    remoteRender.setBackgroundColor(Color.parseColor("#000000"));
            } else {
                if (localRender != null)
                    localRender.setBackgroundColor(Color.parseColor("#000000"));
            }

            if (peerConnectionClient != null) {
                peerConnectionClient.stopVideoSource();
            }


            if (intent != null && intent.getExtras() != null) {
                try {


                    JSONObject obj = new JSONObject();
                    obj.put("callId", intent.getExtras().getString(EXTRA_ROOMID));
                    obj.put("userId", sesionManager.getProviderId());
                    obj.put("type", 6);


                    mqttManager.publish(MqttEvents.Calls.value + "/" + intent.getExtras().getString("callerId"), obj, 0, false);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void toggleCallControlFragmentVisibility() {


       /* if (!iceConnected) {
            return;
        }*/

        callControlFragmentVisible = !callControlFragmentVisible;

        try {
            if (callControlFragmentVisible) {
                parentRl.setVisibility(View.VISIBLE);
            } else {
                parentRl.setVisibility(View.GONE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void startCall() {


        if (appRtcClient != null) {

            // Start room connection.

            appRtcClient.connectToRoom(roomConnectionParameters);

            // Create and audio manager that will take care of audio routing,
            // audio modes, audio device enumeration etc.
            audioManager = AppRTCAudioManager.create(getApplicationContext());
            // Store existing audio settings and change audio mode to
            // MODE_IN_COMMUNICATION for best possible VoIP performance.

            audioManager.start(new AppRTCAudioManager.AudioManagerEvents() {
                // This method will be called each time the number of available audio
                // devices has changed.
                @Override
                public void onAudioDeviceChanged(
                        AppRTCAudioManager.AudioDevice audioDevice, Set<AppRTCAudioManager.AudioDevice> availableAudioDevices) {
                    onAudioManagerDevicesChanged(audioDevice, availableAudioDevices);
                }
            });

        } else {
            Log.e(TAG, "AppRTC client is not allocated for a call.");

        }


    }

    // This method is called when the audio manager reports audio device change,
    // e.g. from wired headset to speakerphone.
    private void onAudioManagerDevicesChanged(
            final AppRTCAudioManager.AudioDevice device, final Set<AppRTCAudioManager.AudioDevice> availableDevices) {
        Utility.printLog(TAG, "onAudioManagerDevicesChanged: " + availableDevices + ", "
                + "selected: " + device);
        // TODO(henrika): add callback handler.
    }

    // Should be called from UI thread
    private void callConnected() {
/*


//        final long delta = System.currentTimeMillis() - callStartedTimeMs;
//        Log.i(TAG, "Call connected: delay=" + delta + "ms");
        if (peerConnectionClient == null || isError) {
            Log.w(TAG, "Call is connected in closed or error state");
            return;
        }
        setSwappedFeeds(false
         isSwappedFeeds
);*/

        // Update video view.

        remoteRender.requestLayout();
        /*
         * Have been put after intentionally to avoid momentary flick on updating renderer state
         */

        remoteRender.setVisibility(View.VISIBLE);

        /*
         * To hide the controls
         *//*
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callControlFragmentVisible = false;
                parentRl.setVisibility(View.GONE);
            }
        }, 2000);


        // Enable statistics callback.
        peerConnectionClient.enableStatsEvents(true, STAT_CALLBACK_PERIOD);
        */
        /* Start the stop watch *//*


         */
        /*
         * By default video call is put on loudspeaker,unless speaker is moved near the ears.
         *//*

        audioManager.setSpeakerphoneOn(true);

        timer.cancel();
        */
        /* Stop the calling sound *//*


        try {
            if (mp != null)
                mp.stop();
        } catch (Exception e) {
        }

        startStopWatch();
*/        // hideCameraView(true);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) localRender.getLayoutParams();

        float density = mContext.getResources().getDisplayMetrics().density;
        params.height = (int) ((density) * 150);

        params.width = (int) ((density) * 150);

        int margin = (int) density * 10;
        params.setMargins(margin, margin, margin, margin);
        localRender.setLayoutParams(params);

        if (peerConnectionClient == null) {
            Log.w(TAG, "Call is connected in closed or error state" + peerConnectionClient + " ");
            return;
        }

        setSwappedFeeds(false /* isSwappedFeeds */);



        /*
         * To hide the controls
         */

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callControlFragmentVisible = false;
                parentRl.setVisibility(View.GONE);
            }
        }, 2000);


        // Disable statistics callback.
        peerConnectionClient.enableStatsEvents(false, STAT_CALLBACK_PERIOD);
        /* Start the stop watch */

        /*
         * By default video call is put on loudspeaker,unless speaker is moved near the ears.
         */
        if (audioManager != null)
            audioManager.setSpeakerphoneOn(true);


        try {
            if (timer != null)
                timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            /* Stop the calling sound */
            if (mp != null) {

                try {
                    if (mp.isPlaying()) {

                        mp.stop();
                        mp.release();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!timerAlreadyStarted) {
            timerAlreadyStarted = true;
            startStopWatch();
        }
    }

    private void onAudioManagerChangedState() {
        // TODO(henrika): disable video if AppRTCAudioManager.AudioDevice.EARPIECE
        // is active.
    }

    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect() {
        if (!cleanUpRequested) {
            cleanUpRequested = true;
            try {
                /*
                 * To make myself available for receiving the new call
                 */
                JSONObject obj = new JSONObject();
                obj.put("status", 1);

                mqttManager.publish(MqttEvents.CallsAvailability.value + "/" + sesionManager.getProviderId(), obj, 0, true);
                UtilityVideoCall.getInstance().setActiveOnACall(false, false);


                try {
                    if (mp != null) {

                        try {
                            if (mp.isPlaying()) {

                                mp.stop();
                                mp.release();
//                                mp.reset();
                            }
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (reconnectTonePlayer != null) {

                    try {
                        if (reconnectTonePlayer.isPlaying()) {
                            try {
                                reconnectTonePlayer.stop();

                                reconnectTonePlayer.release();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    if (timer != null)
                        timer.cancel();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                remoteProxyRenderer.setTarget(null);
                localProxyVideoSink.setTarget(null);

                if (appRtcClient != null) {
                    appRtcClient.disconnectFromRoom();
                    appRtcClient = null;
                }
                if (localRender != null) {
                    localRender.release();
                    localRender = null;
                }

                if (remoteRender != null) {
                    remoteRender.release();
                    remoteRender = null;
                }


                if (remoteRenderHeader != null) {
                    remoteRenderHeader.release();
                    remoteRenderHeader = null;
                }

                if (peerConnectionClient != null) {
                    peerConnectionClient.close();
                    peerConnectionClient = null;
                }
                if (audioManager != null) {
                    audioManager.stop();
                    audioManager = null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            removeViews();
            stopSelf();


        }
    }

    // Log |msg| and Toast about it.
    private void logAndToast(String msg) {
        Utility.printLog(TAG + 238, msg);
        if (logToast != null) {
            logToast.cancel();
        }
        logToast = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        logToast.show();
    }

    // -----Implementation of AppRTCClient.AppRTCSignalingEvents ---------------
    // All callbacks are invoked from websocket signaling looper thread and
    // are routed to UI thread.
    private void onConnectedToRoomInternal(final AppRTCClient.SignalingParameters params) {

        signalingParameters = params;

        VideoCapturer videoCapturer = null;
        if (peerConnectionParameters != null && peerConnectionParameters.videoCallEnabled) {
            videoCapturer = createVideoCapturer();
        }

        if (peerConnectionClient != null) {
            peerConnectionClient.createPeerConnection(
                    localProxyVideoSink, remoteSinks, videoCapturer, signalingParameters);


            if (signalingParameters != null && signalingParameters.initiator) {

                // Create offer. Offer SDP will be sent to answering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createOffer();
            } else {
                if (params.offerSdp != null) {
                    peerConnectionClient.setRemoteDescription(params.offerSdp);

                    // Create answer. Answer SDP will be sent to offering client in
                    // PeerConnectionEvents.onLocalDescription event.
                    peerConnectionClient.createAnswer();
                }
                if (params.iceCandidates != null) {
                    // Add remote ICE candidates from room.
                    for (IceCandidate iceCandidate : params.iceCandidates) {
                        peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                    }
                }
            }
        }
    }

    private @Nullable
    VideoCapturer createVideoCapturer() {
        final VideoCapturer videoCapturer;
        String videoFileAsCamera = intent.getStringExtra(EXTRA_VIDEO_FILE_AS_CAMERA);
        if (videoFileAsCamera != null) {
            try {
                videoCapturer = new FileVideoCapturer(videoFileAsCamera);
            } catch (IOException e) {
                reportError("Failed to open video file for emulated camera");
                return null;
            }
        } else if (useCamera2()) {
            if (!captureToTexture()) {
                reportError(getString(R.string.camera2_texture_only_error));
                return null;
            }


            videoCapturer = createCameraCapturer(new Camera2Enumerator(this));
        } else {

            videoCapturer = createCameraCapturer(new Camera1Enumerator(captureToTexture()));
        }
        if (videoCapturer == null) {
            reportError("Failed to open camera");
            return null;
        }
        return videoCapturer;
    }

    private boolean useCamera2() {
        return Camera2Enumerator.isSupported(this) && intent.getBooleanExtra(EXTRA_CAMERA2, true);
    }

    private boolean captureToTexture() {
        return intent.getBooleanExtra(EXTRA_CAPTURETOTEXTURE_ENABLED, false);
    }

    private @Nullable
    VideoCapturer createCameraCapturer(CameraEnumerator enumerator) {
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera

        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {

                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else

        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {

                VideoCapturer videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        return null;
    }

    private void reportError(final String description) {
        handler.post(new Runnable() {
            @Override
            public void run() {

                Utility.printLog("1234975", description);
                //   Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void createOffer(/*AppRTCClient.SignalingParameters signalingParameters*/) {
        if (signalingParameters.initiator) {
            //logAndToast("Creating OFFER...");
            // Create offer. Offer SDP will be sent to answering client in
            // PeerConnectionEvents.onLocalDescription event.
            peerConnectionClient.createOffer();
        } else {
            if (signalingParameters.offerSdp != null) {
                peerConnectionClient.setRemoteDescription(signalingParameters.offerSdp);
                // logAndToast("Creating ANSWER...");
                // Create answer. Answer SDP will be sent to offering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createAnswer();
            }
            if (signalingParameters.iceCandidates != null) {
                // Add remote ICE candidates from room.
                for (IceCandidate iceCandidate : signalingParameters.iceCandidates) {
                    peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                }
            }
        }
    }

    @Override
    public void onConnectedToRoom(final AppRTCClient.SignalingParameters params) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                onConnectedToRoomInternal(params);
            }
        });
    }

    @Override
    public void onRemoteDescription(final SessionDescription sdp) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient != null) {
                    peerConnectionClient.setRemoteDescription(sdp);
                    if (signalingParameters != null && !signalingParameters.initiator) {

                        // Create answer. Answer SDP will be sent to offering client in
                        // PeerConnectionEvents.onLocalDescription event.
                        peerConnectionClient.createAnswer();
                    }
                } else {
                    Log.e(TAG, "Received remote SDP for non-initilized peer connection.");

                }


            }
        });
    }

    @Override
    public void onRemoteIceCandidate(final IceCandidate candidate) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient != null) {
                    peerConnectionClient.addRemoteIceCandidate(candidate);
                } else {
                    Log.e(TAG, "Received ICE candidate for a non-initialized peer connection.");

                }

            }
        });
    }

    @Override
    public void onRemoteIceCandidatesRemoved(final IceCandidate[] candidates) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient != null) {
                    peerConnectionClient.removeRemoteIceCandidates(candidates);

                } else {

                    Log.e(TAG, "Received ICE candidate removals for a non-initialized peer connection.");

                }

            }
        });
    }

    @Override
    public void onChannelClose() {
        /*Utility.printLog(TAG, "onChannelClose: " + 1);

        handler.post(new Runnable() {
            @Override
            public void run() {
                //   logAndToast("Remote end hung up; dropping PeerConnection");


                disconnect();


            }
        });*/
    }

    @Override
    public void onChannelError(final String description) {
        Utility.printLog(TAG, "onChannelError: " + 1);

        reportError(description);
    }

    // -----Implementation of PeerConnectionClient.PeerConnectionEvents.---------
    // Send local peer connection SDP and ICE candidates to remote party.
    // All callbacks are invoked from peer connection client looper thread and
    // are routed to UI thread.
    @Override
    public void onLocalDescription(final SessionDescription sdp) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {

                    if (signalingParameters != null && signalingParameters.initiator) {
                        appRtcClient.sendOfferSdp(sdp);
                    } else {
                        appRtcClient.sendAnswerSdp(sdp);
                    }
                }
                if (peerConnectionParameters != null && peerConnectionParameters.videoMaxBitrate > 0) {

                    if (peerConnectionClient != null) {
                        peerConnectionClient.setVideoMaxBitrate(peerConnectionParameters.videoMaxBitrate);
                    }
                }
            }
        });
    }

    @Override
    public void onIceCandidate(final IceCandidate candidate) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    appRtcClient.sendLocalIceCandidate(candidate);
                }
            }
        });
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] candidates) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    appRtcClient.sendLocalIceCandidateRemovals(candidates);
                }
            }
        });
    }

    @Override
    public void onIceConnected() {
        Utility.printLog(TAG, "onIceConnected: " + 1);

        // final long delta = System.currentTimeMillis() - callStartedTimeMs;
      /*  handler.post(new Runnable() {
            @Override
            public void run() {
                //      logAndToast("ICE connected, delay=" + delta + "ms");
                iceConnected = true;
                callConnected();
            }
        });*/
    }

    @Override
    public void onIceDisconnected() {
        Utility.printLog(TAG, "onIceDisconnected: " + 1);

      /*  handler.post(new Runnable() {
            @Override
            public void run() {
                // logAndToast("ICE disconnected");
                iceConnected = false;

                disconnect();
                Utility.printLog(TAG, "run:");


            }
        });*/
    }

    @Override
    public void onConnected() {
        handler.post(new Runnable() {
            @Override
            public void run() {


                tvReconnecting.setVisibility(View.GONE);
                connected = true;

                if (reconnectTonePlayer != null) {

                    try {
                        if (reconnectTonePlayer.isPlaying()) {
                            try {
                                reconnectTonePlayer.stop();

                                reconnectTonePlayer.release();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }


                callConnected();
                stopReconnectTimer();
            }
        });
    }

    private void stopReconnectTimer() {
        if (reconnectTimer != null) {
            try {
                reconnectTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onDisconnected() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                tvReconnecting.setVisibility(View.VISIBLE);
                connected = false;

                if (reconnectTonePlayer != null) {
                    try {
                        if (reconnectTonePlayer.isPlaying()) {
                            try {
                                reconnectTonePlayer.stop();

                                reconnectTonePlayer.release();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }


                reconnectTonePlayer = MediaPlayer.create(mContext, R.raw.cancel_ringtone);
                reconnectTonePlayer.setLooping(true);

                if (!connected) {

                    try {
                        reconnectTonePlayer.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    initializeReconnectTimer();
                }
            }
        });
    }

    private void initializeReconnectTimer() {


        if (reconnectTimer != null) {
            try {
                reconnectTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        reconnectTimer = new CountDownTimer(MAX_RECONNECT_TIME, 1000) {
            public void onTick(long millisUntilFinished) {

                if (connected) {
                    reconnectTimer.cancel();

                }
            }

            public void onFinish() {
                reconnectTimer.cancel();

                Utility.printLog(TAG, "onFinish:" + getResources().getString(R.string.reconnect_failed));
                onCallHangUp(2, false);
            }
        };
        reconnectTimer.start();

    }

    @Override
    public void onPeerConnectionClosed() {
        Utility.printLog(TAG, "onPeerConnectionClosed: " + 1);

    }

    @Override
    public void onPeerConnectionStatsReady(final StatsReport[] reports) {
        Utility.printLog(TAG, "onPeerConnectionStatsReady: " + 1);

    }

    @Override
    public void onPeerConnectionError(final String description) {
        reportError(description);
    }

    private void initializeRxJava() {
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(String objects) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(objects);
                    onMessage(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        RxCallInfo.getInstance().subscribe(observer);
    }

    public void onMessage(JSONObject object) {
        Utility.printLog(TAG, "onMessage: " + object);
        try {
            if (object.has("action")) {

                if (object.optString("action").equals(CallActions.NOT_ANSWER_OR_LEFT.value)) {
                    Utility.printLog("onMessage:action2 ", object.toString());
                    if (roomId.equals(intent.getStringExtra(EXTRA_ROOMID))) {
//                            tvStopWatch.setText(getString(R.string.connecting));
                        UtilityVideoCall.getInstance().setActiveOnACall(true, true);
                    } else {
                        onRejectSuccess();
                        UtilityVideoCall.getInstance().setActiveOnACall(false, true);
                    }
                } else if (object.optString("action").equals(CallActions.JOIN_ON_CALL.value)) {
                    /*  if (roomId.equals(intent.getStringExtra(EXTRA_ROOMID))) {*/

                    bookingEndtime = object.optString("bookingEndtime", "");
                    Utility.printLog(TAG, "onTickbookingEndtime= " + bookingEndtime);
                    setdisconnectTime(bookingEndtime);
                    /*  }*/
                    tvStopWatch.setText(getString(R.string.connecting));
                    UtilityVideoCall.getInstance().setActiveOnACall(true, true);
                } else if (object.optString("action").equals(CallActions.CALL_ENDED.value)) {
                    bookingEndtime = "";
                    onRejectSuccess();
                    UtilityVideoCall.getInstance().setActiveOnACall(false, true);
                }


            } else {
                if (object.getString("eventName").equals(MqttEvents.CallsAvailability.value)) {

                    if (intent != null) {
                        Bundle extras = intent.getExtras();

                        if (object.getInt("status") == 0) {
                            /*
                             * Receiver is busy
                             */


                            if (root != null) {
                                Utility.printLog(TAG, "onMessagebusy: ");
                                // alertProgress.alertinfo(this,extras.getString("callerName") + " " + getString(R.string.busy));

                            /*Snackbar snackbar = Snackbar.make(root, extras.getString("callerName") + " " + getString(R.string.busy), Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);*/

                                Toast.makeText(getApplicationContext(), extras.getString("callerName") + " " + getString(R.string.busy), Toast.LENGTH_SHORT).show();
                            }
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    stopSelf();
                                }
                            }, 2000);

                        } else {
                            //  Bundle extras = intent.getExtras();

                            try {
                                /*
                                 * I put myself as busy and make the call request to the receiver
                                 */
                                JSONObject obj = new JSONObject();
                                obj.put("status", 0);


                                mqttManager.publish(MqttEvents.CallsAvailability.value + "/" + manager.getProviderId(), obj, 0, true);
                                UtilityVideoCall.getInstance().setActiveOnACall(true, true);

                                obj = new JSONObject();
                                obj.put("callerId", manager.getProviderId());
                                obj.put("callId", extras.getString(EXTRA_ROOMID));
                                obj.put("bookingId", extras.getString("BookingID"));
                                obj.put("callType", "1");
                                obj.put("callerName", manager.getFirstName() + " " + manager.getLastName());
                                obj.put("callerImage", manager.getProfilePic());
                                obj.put("callerIdentifier", extras.getString("callerIdentifier"));

                                obj.put("type", 0);
                                obj.put("userType", 1);

                                /*
                                 * CalleeId although not required but can be used in future on server so using it
                                 *
                                 *
                                 * CalleeId contains the receiverUid,to whom the call has been made
                                 *
                                 * *//*

                                 *//*
                         * Type-0---call initiate request,on receiving the call initiate request,receiver will set his status as busy,so nobody else can call him
                         *
                         * /


                        obj.put("type", 0);
/*
 * Not making any message of call signalling as being persistent intentionally
 */

                                Utility.printLog(TAG, "onMessageMESSAGE: " + obj.toString());
                                mqttManager.publish(MqttEvents.Calls.value + "/" + extras.getString("callerId"), obj, 0, false);

                                UtilityVideoCall.getInstance().setActiveCallId(extras.getString(EXTRA_ROOMID));
                                UtilityVideoCall.getInstance().setActiveCallerId(extras.getString("callerId"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else if (object.getString("eventName").equals(MqttEvents.Calls.value)) {

                    switch (object.getInt("type")) {

                        case 1:
                            if (roomId != null) {


                                if (object.getString("callId").equals(roomId)) {

                                    tvStopWatch.setText(getString(R.string.connecting));
                                }
                            } else {
                                tvStopWatch.setText(getString(R.string.connecting));

                            }


                            break;


                        case 2:
                            /*if (roomId != null) {


                                if (object.getString("callId").equals(roomId)) {

                                   // onCallHangUp(2, true);
                                    Toast.makeText(mContext, "not hangup", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                onCallHangUp(2, true);

                            }

                            break;*/
                        case 3:


                            if (roomId != null) {


                                if (object.getString("callId").equals(roomId)) {

                                    if (intent.getExtras() != null) {
                                        Utility.printLog("1234975", getString(R.string.NoAnswer) + " " + intent.getExtras().getString("callerName"));
//                                        Toast.makeText(this, getString(R.string.NoAnswer) + " " + intent.getExtras().getString("callerName"), Toast.LENGTH_SHORT).show();
                                    }

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            stopSelf();
                                        }
                                    }, 2000);
                                }
                            } else {
                                if (intent.getExtras() != null) {

                                    Utility.printLog("" + 1234875, getString(R.string.NoAnswer) + " " + intent.getExtras().getString("callerName"));
//                                    Toast.makeText(this, getString(R.string.NoAnswer) + " " + intent.getExtras().getString("callerName"), Toast.LENGTH_SHORT).show();


                                }

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        stopSelf();
                                    }
                                }, 2000);

                            }


                            break;
                        case 5:


                            if (roomId != null) {


                                if (object.getString("callId").equals(roomId)) {

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (remoteRenderHeader != null) {
                                                remoteRenderHeader.setBackgroundColor(Color.parseColor("#00000000"));

                                            }
                                            if (isSwappedFeeds) {
                                                if (localRender != null)
                                                    localRender.setBackgroundColor(Color.parseColor("#00000000"));
                                            } else {
                                                if (remoteRender != null)
                                                    remoteRender.setBackgroundColor(Color.parseColor("#00000000"));


                                            }
                                        }
                                    });
                                }
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (remoteRenderHeader != null) {
                                            remoteRenderHeader.setBackgroundColor(Color.parseColor("#00000000"));

                                        }

                                        if (isSwappedFeeds) {
                                            if (localRender != null)
                                                localRender.setBackgroundColor(Color.parseColor("#00000000"));
                                        } else {
                                            if (remoteRender != null)
                                                remoteRender.setBackgroundColor(Color.parseColor("#00000000"));


                                        }
                                    }
                                });

                            }


                            break;
                        case 6:


                            if (roomId != null) {


                                if (object.getString("callId").equals(roomId)) {

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (remoteRenderHeader != null) {
                                                remoteRenderHeader.setBackgroundColor(Color.parseColor("#4e4e4e"));
                                            }

                                            if (isSwappedFeeds) {

                                                if (localRender != null)
                                                    localRender.setBackgroundColor(Color.parseColor("#4e4e4e"));
                                            } else {

                                                if (remoteRender != null)
                                                    remoteRender.setBackgroundColor(Color.parseColor("#4e4e4e"));


                                            }


                                        }
                                    });
                                }
                            } else {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (remoteRenderHeader != null) {
                                            remoteRenderHeader.setBackgroundColor(Color.parseColor("#4e4e4e"));
                                        }
                                        if (isSwappedFeeds) {

                                            if (localRender != null)
                                                localRender.setBackgroundColor(Color.parseColor("#4e4e4e"));
                                        } else {

                                            if (remoteRender != null)
                                                remoteRender.setBackgroundColor(Color.parseColor("#4e4e4e"));
                                        }
                                    }
                                });
                            }
                            break;
                        case 7:
                            if (roomId != null) {
                                if (object.getString("callId").equals(roomId)) {
                                    onCallHangUp(7, true);
                                }
                            } else {
                                onCallHangUp(7, true);
                            }
                            break;
                    }
                } else {
                    try {
                        if (object.getString("eventName").equals("appCrashed")) {
//                            destroyOnAppCrashed(true);
                            System.exit(1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setdisconnectTime(String bookingEndtime) {
        long futureduration;
        if (!bookingEndtime.isEmpty()) {
            futureduration = (Long.parseLong(bookingEndtime) * 1000) - ((System.currentTimeMillis()));
            Utility.printLog(TAG, "onTickfutureduration= " + futureduration);
            if (futureduration > 0) {
                if (disconnectTimer != null) {
                    disconnectTimer.cancel();
                    disconnectTimer = null;
                }
                disconnectTimer = new CountDownTimer(futureduration, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        // TODO Auto-generated method stub
                        Utility.printLog(TAG, "onTick:" + millisUntilFinished / 1000);
                        if ((millisUntilFinished < 121000) &&
                                (millisUntilFinished > 119000)) {

                            Utility.printLog("onTickSystem: ", "" + System.currentTimeMillis());
                            //show notification
                            Bitmap icon = BitmapFactory.decodeResource(getResources(),
                                    R.mipmap.ic_launcher);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(VideoCallService.this, "foreground")
                                    .setContentTitle(getString(R.string.telecallalert))
                                    .setTicker("")
                                    .setContentText(getString(R.string.two_min_call_cut))
                                    .setSmallIcon(R.drawable.notification)
                                    .setLargeIcon(icon)
                                    .setChannelId("CallCut")
                                    .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE) //Important for heads-up notification
                                    .setPriority(Notification.PRIORITY_MAX); //Important for heads-up notification;

//                                    .setContentIntent(pendingIntent)
//                                    .setOngoing(true);
                            Notification notification = notificationBuilder.build();
                            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            setNotificationChannel(notificationManager, "CallCut", getString(R.string.notificationChannelNameCall), getString(R.string.notificationChannelDescriptionChat));
                            notificationManager.notify(137, notification);
                        }
                    }

                    @Override
                    public void onFinish() {
                        // TODO Auto-generated method stub
                        //cut the call at this time.
                        Utility.printLog(TAG, "onTick: onFinish");
                        Utility.printLog(TAG, "onTick: onFinish" + System.currentTimeMillis());

                        presenter.endCall(sesionManager.getCallToken(), intent.getExtras().getString(EXTRA_ROOMID), "join");
                        onCallHangUp(2, false);
                    }
                }.start();
            }
        }


    }

    private CountDownTimer setReverseTimer(TextView tv, int Seconds) {
        return new CountDownTimer(Seconds * 1000 + 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                tv.setText(getString(R.string.connecting) + ": " + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
            }

            public void onFinish() {
                onRejectSuccess();
                UtilityVideoCall.getInstance().setActiveOnACall(false, true);
            }
        }.start();
    }

    private void startCallProcedure(boolean isIncoming) {


        // Get Intent parameters.
        if (intent != null) {


            Uri roomUri = intent.getData();
            if (roomUri == null) {


                stopSelf();
                return;
            }

            // Get Intent parameters.
            roomId = intent.getStringExtra(EXTRA_ROOMID);
            if (roomId == null || roomId.length() == 0) {

                stopSelf();
                return;
            }


//            AppController.getInstance().saveCurrentCallDetails(roomId, intent.getStringExtra("callerId"));

            boolean loopback = intent.getBooleanExtra(EXTRA_LOOPBACK, false);
            boolean tracing = intent.getBooleanExtra(EXTRA_TRACING, false);

            int videoWidth = intent.getIntExtra(EXTRA_VIDEO_WIDTH, 0);
            int videoHeight = intent.getIntExtra(EXTRA_VIDEO_HEIGHT, 0);

            PeerConnectionClient.DataChannelParameters dataChannelParameters = null;
            if (intent.getBooleanExtra(EXTRA_DATA_CHANNEL_ENABLED, false)) {
                dataChannelParameters = new PeerConnectionClient.DataChannelParameters(intent.getBooleanExtra(EXTRA_ORDERED, true),
                        intent.getIntExtra(EXTRA_MAX_RETRANSMITS_MS, -1),
                        intent.getIntExtra(EXTRA_MAX_RETRANSMITS, -1), intent.getStringExtra(EXTRA_PROTOCOL),
                        intent.getBooleanExtra(EXTRA_NEGOTIATED, false), intent.getIntExtra(EXTRA_ID, -1));
            }


            peerConnectionParameters =
                    new PeerConnectionClient.PeerConnectionParameters(intent.getBooleanExtra(EXTRA_VIDEO_CALL, true), loopback,
                            tracing, videoWidth, videoHeight, intent.getIntExtra(EXTRA_VIDEO_FPS, 0),
                            intent.getIntExtra(EXTRA_VIDEO_BITRATE, 0), intent.getStringExtra(EXTRA_VIDEOCODEC),
                            intent.getBooleanExtra(EXTRA_HWCODEC_ENABLED, true),
                            intent.getBooleanExtra(EXTRA_FLEXFEC_ENABLED, false),
                            intent.getIntExtra(EXTRA_AUDIO_BITRATE, 0), intent.getStringExtra(EXTRA_AUDIOCODEC),
                            intent.getBooleanExtra(EXTRA_NOAUDIOPROCESSING_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_AECDUMP_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_SAVE_INPUT_AUDIO_TO_FILE_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_OPENSLES_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_AEC, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_AGC, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_NS, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_WEBRTC_AGC_AND_HPF, false),
                            intent.getBooleanExtra(EXTRA_ENABLE_RTCEVENTLOG, false), dataChannelParameters);


            // Create connection client. Use DirectRTCClient if room name is an IP otherwise use the
            // standard WebSocketRTCClient.
            if (loopback || !DirectRTCClient.IP_PATTERN.matcher(roomId).matches()) {
                appRtcClient = new WebSocketRTCClient(this);
            } else {
                Log.i(TAG, "Using DirectRTCClient because room name looks like an IP.");
                appRtcClient = new DirectRTCClient(this);
            }
            // Create connection parameters.
            String urlParameters = intent.getStringExtra(EXTRA_URLPARAMETERS);
            roomConnectionParameters =
                    new AppRTCClient.RoomConnectionParameters(roomUri.toString(), roomId, loopback, urlParameters);


            // Create peer connection client.
            peerConnectionClient = new PeerConnectionClient(
                    getApplicationContext(), rootEglBase, peerConnectionParameters, this);
            PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
            if (loopback) {
                options.networkIgnoreMask = 0;
            }

            peerConnectionClient.createPeerConnectionFactory(options);
            handler = new Handler(Looper.getMainLooper());

            showCallControlsUi();
            startCall();



            /* Implement the count down timer */

            if (!isIncoming) {
                timer = new CountDownTimer(60000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        /* Perform the click of cancel button here */
                        Toast.makeText(mContext, getResources().getString(R.string.Timeout), Toast.LENGTH_LONG).show();
                        onCallHangUp(7, false);
                    }
                };
                timer.start();
            }
        }
    }

    private void addMainLayout() {
        if (params == null) {
            params = new WindowManager.LayoutParams(


                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);


            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                params.type = WindowManager.LayoutParams.TYPE_PHONE;
            } else {
                params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            }
        }

        /********For Samsung device to diasble lock**************/

        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        @SuppressLint("InvalidWakeLockTag")
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "INFO");
        wl.acquire();

        KeyguardManager km = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("name");
        kl.disableKeyguard();
        /********For Samsung device to diasble lock**************/

        //Add the view to the window

        if (windowManager == null) {
            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        }

        try {
            windowManager.addView(videoCallView, params);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private void showCallControlsUi() {


        callHeaderView = LayoutInflater.from(this).inflate(R.layout.video_call_floating_widget, null);


        remoteRenderHeader = (SurfaceViewRenderer) callHeaderView.findViewById(R.id.remoteVideoHeader);

        remoteRenderHeader.init(rootEglBase.getEglBaseContext(), null);

        remoteProxyRenderer.setTarget(remoteRenderHeader);

        callHeaderTv = (TextView) callHeaderView.findViewById(R.id.duration);


        parentRl = (RelativeLayout) videoCallView.findViewById(R.id.container_rl);
        parentRl.setVisibility(View.VISIBLE);


        initiateChat = (ImageView) videoCallView.findViewById(R.id.initiateChat);
        // Create UI controls.
        final ImageButton camera = (ImageButton) videoCallView.findViewById(R.id.camera);
        ImageView cancelCall = (ImageButton) videoCallView.findViewById(R.id.cancelCall);
        mute = (ImageButton) videoCallView.findViewById(R.id.mute);
        video = (ImageButton) videoCallView.findViewById(R.id.video);
        camera.getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);

        tvCallerName = (TextView) videoCallView.findViewById(R.id.tvCallerName);
        tvReconnecting = (TextView) videoCallView.findViewById(R.id.tvReconnecting);
        callerImage = (ImageView) videoCallView.findViewById(R.id.userImage);

        final String caller_id = intent.getStringExtra("callerId");


        /*Map<String, Object> contactInfo = AppController.getInstance().getDbController().getContactInfoFromUid(AppController.getInstance().getContactsDocId(), caller_id);

        if (contactInfo != null) {


            tvCallerName.setText((String) contactInfo.get("contactName"));


            imageUrl = (String) contactInfo.get("contactPicUrl");

            if (imageUrl == null || imageUrl.isEmpty()) {


                callerImage.setImageDrawable(TextDrawable.builder()


                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24 * (int) getResources().getDisplayMetrics().density) *//* size in px *//*
                        .bold()
                        .toUpperCase()
                        .endConfig()


                        .buildRound(((String) contactInfo.get("contactName")).trim().charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(5))));


            } else {

                try {
                    Glide.with(mContext).load(imageUrl).asBitmap()

                            .centerCrop().placeholder(R.drawable.chat_attachment_profile_default_image_frame).
                            into(new BitmapImageViewTarget(callerImage) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    callerImage.setImageDrawable(circularBitmapDrawable);
                                }
                            });

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }
        } else {*/


        /*
         * If userId doesn't exists in contact
         */
        if (intent != null) {
            Bundle extras = intent.getExtras();
            tvCallerName.setText(extras.getString("callerName"));

            imageUrl = extras.getString("callerImage");

            if (imageUrl == null || imageUrl.isEmpty()) {

                String callerIdentifier = extras.getString("callerIdentifier");

                if (callerIdentifier != null)
                    callerIdentifier = (callerIdentifier.trim()).charAt(0) + "";
                else
                    callerIdentifier = "+";


                callerImage.setImageDrawable(TextDrawable.builder()


                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)
                        .fontSize(24 * (int) getResources().getDisplayMetrics().density) /* size in px */
                        .bold()
                        .toUpperCase()
                        .endConfig()


                        .buildRound(callerIdentifier, ContextCompat.getColor(this, R.color.colorPrimary)/*Color.parseColor(AppController.getInstance().getColorCode(5))*/));


            } else {

                try {
                    Glide.with(mContext).setDefaultRequestOptions(
                            new RequestOptions().centerCrop().
                                    placeholder(R.drawable.profile_default_image)).asBitmap().
                            load(imageUrl).
                            into(new BitmapImageViewTarget(callerImage) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    callerImage.setImageDrawable(circularBitmapDrawable);
                                }
                            });
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }
        }
//        }


        // Add buttons click events.
        cancelCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//join=>when in call request=>when not in call
                presenter.endCall(sesionManager.getCallToken(), intent.getExtras().getString(EXTRA_ROOMID), "join");

                onCallHangUp(2, false);
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCameraSwitch();
                isFrontCamera = !isFrontCamera;

                camera.setSelected(isFrontCamera);


            }
        });

        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMute();
                mute.setSelected(isMute);
            }
        });


        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onVideoShow();
                isVideoShow = !isVideoShow;
                video.setSelected(isVideoShow);
            }
        });


        final WindowManager.LayoutParams params;// = new WindowManager.LayoutParams();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);

        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);


        }


        // final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        //Specify the view position
        params.gravity = Gravity.TOP | Gravity.START;        //Initially view will be added to top-left corner
        params.x = 0;
        params.y = 100;


        callHeaderView.setOnTouchListener(new View.OnTouchListener() {
            private int lastAction;
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {


                    case MotionEvent.ACTION_DOWN:


                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        lastAction = event.getAction();
                        return true;
                    case MotionEvent.ACTION_UP:
                        //As we implemented on touch listener with ACTION_MOVE,
                        //we have to check if the previous action was ACTION_DOWN
                        //to identify if the user clicked the view or not.


                        if (lastAction == MotionEvent.ACTION_DOWN) {
                            //Open the chat conversation click.


                            //close the service and remove the chat heads


                            windowManager.removeView(callHeaderView);

                            remoteProxyRenderer.setTarget(isSwappedFeeds ? localRender : remoteRender);


                            addMainLayout();
                            UtilityVideoCall.getInstance().setCallMinimized(false);
                        }
                        lastAction = event.getAction();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        //Calculate the X and Y coordinates of the view.


                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);


                        if ((int) (event.getRawX() - initialTouchX) == 0 && (int) (event.getRawY() - initialTouchY) == 0) {
                            lastAction = 0;
                        } else {
                            lastAction = event.getAction();
                        }

                        try {
                            //Update the layout with new X & Y coordinate
                            windowManager.updateViewLayout(callHeaderView, params);
                        } catch (NullPointerException e) {


                            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                            windowManager.updateViewLayout(callHeaderView, params);
                        }
                        return true;
                }
                return true;
            }
        });


        /*
         *To open the chatscreen activity with the given user
         *
         */

        initiateChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                windowManager.removeView(videoCallView);

                remoteProxyRenderer.setTarget(remoteRenderHeader);

                windowManager.addView(callHeaderView, params);
                /*
                 * To open the chats fragment,we add to the container
                 */
               /* String docId = AppController.findDocumentIdOfReceiver(caller_id, Utilities.tsInGmt(), tvCallerName.getText().toString(),
                        imageUrl, "", false, intent.getExtras().getString("callerIdentifier"), "", false);
*/

                UtilityVideoCall.getInstance().setCallMinimized(true);


                UtilityVideoCall.getInstance().setFirstTimeAfterCallMinimized(true);
                if (UtilityVideoCall.getInstance().getActiveActivitiesCount() == 0) {


                    try {
                        Intent intent = new Intent(mContext, ChattingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        /*intent.putExtra("receiverUid", caller_id);
                        intent.putExtra("receiverName", tvCallerName.getText().toStr
                        ing());
                        //  intent.putExtra("documentId", docId);
                        intent.putExtra("receiverImage", imageUrl);
                        intent.putExtra("colorCode", com.telecall.UtilityVideoCall.getInstance().getColorCode(5));*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {

                   /* JSONObject obj = new JSONObject();
                    try {
                        obj.put("eventName", "callMinimized");


                        obj.put("receiverUid", caller_id);
                        obj.put("receiverName", tvCallerName.getText().toString());
                        obj.put("documentId", docId);
                        obj.put("receiverImage", imageUrl);
//                        obj.put("colorCode", AppController.getInstance().getColorCode(5));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
//                    bus.post(obj);
                }
            }
        });


    }

    public void startStopWatch() {
        setCallDuration();

        if (initiateChat != null)

            initiateChat.setVisibility(View.VISIBLE);
    }

    private void setCallDuration() {

        try {
            CountDownTimer cTimer = new CountDownTimer(3600000, 1000) {

                public void onTick(long millisUntilFinished) {
                    long milliSec = (3600000 - millisUntilFinished) / 1000;

                    long sec = milliSec % 60;
                    long min = milliSec / 60;
                    if (min < 10) {
                        if (sec < 10) {
                            tvStopWatch.setText("00:0" + min + ":0" + sec);
                            callHeaderTv.setText("00:0" + min + ":0" + sec);
                        } else {
                            tvStopWatch.setText("00:0" + min + ":" + sec);

                            callHeaderTv.setText("00:0" + min + ":" + sec);
                        }

                    } else {
                        if (sec < 10) {
                            tvStopWatch.setText("00:" + min + ":0" + sec);
                            callHeaderTv.setText("00:" + min + ":0" + sec);


                        } else {
                            tvStopWatch.setText("00:" + min + ":" + sec);

                            callHeaderTv.setText("00:" + min + ":" + sec);


                        }

                    }


                }

                @Override
                public void onFinish() {

                }
            };

            cTimer.start();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void setupCallHeaderDuration() {

        stopWatchHeader.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer arg0) {
                countUpHeader = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;


                try {
                    Utility.printLog(TAG, "onChronometerTick: started ");
                    callHeaderTv.setText(String.format(Locale.US, "%02d:%02d:%02d", countUpHeader / 3600, countUpHeader / 60, countUpHeader % 60));
                    Utility.printLog(TAG, "onChronometerTick: " + callHeaderTv.getText().toString());

                } catch (NullPointerException e) {
                    Utility.printLog(TAG, "null onChronometerTick");
                    e.printStackTrace();
                }
            }
        });


        stopWatchHeader.setBase(SystemClock.elapsedRealtime());
        stopWatchHeader.start();
    }

    @Override
    public void onAnswerSuccess() {

    }

    @Override
    public void onRejectSuccess() {
        onCallHangUp(2, false);
    }

    private static class ProxyVideoSink implements VideoSink {
        private VideoSink target;

        @Override
        synchronized public void onFrame(VideoFrame frame) {
            if (target == null) {
                Utility.printLog(TAG, "Dropping frame in proxy because target is null.");
                return;
            }

            target.onFrame(frame);
        }

        synchronized public void setTarget(VideoSink target) {
            this.target = target;
        }
    }
}