package com.sirvis.pro.telecall.callservice;

public interface VideoCallContract {
    interface Presenter {
        void initCall(String sessionToken, String currentRoomId, String callerId, String video);

        void endCall(String sessionToken, String callID, String callFrom);

        void dropView();

        void dispose();


    }

    interface View {
        void onAnswerSuccess();

        void onRejectSuccess();
    }
}