package com.sirvis.pro.telecall.utility.incomming_call;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Ali on 10/29/2018.
 */
public class InComingCallPresenter implements IncomingCallContract.Presenter {

    private String TAG = InComingCallPresenter.class.getSimpleName();


    private SessionManager manager;
    private Gson gson;
    private IncomingCallContract.View view;

    private Context context;
    private CompositeDisposable compositeDisposable;


    public InComingCallPresenter(IncomingCallContract.View view) {
        this.context = (Context) view;
        this.view = view;
        manager = SessionManager.getSessionManager(context);
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void ansDecline(String call_id, int i) {

//        Observable<Response<ResponseBody>> observable = apiService.telCallAnsDec(manager.getAUTH(), Constants.selLang
//        ,call_id,i);
//
//        observable.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
//                    @Override
//                    public void onNext(Response<ResponseBody> responseBodyResponse) {
//
//                        int code = responseBodyResponse.code();
//                        String response;
//                        try
//                        {
//
//
//
//                        if(code == 200)
//                        {
//                            response = responseBodyResponse.body().string();
//                            if(i ==2)
//                                view.onSuccessAns();
//                            else
//                                view.onSuccessDec();
//                        }else
//                        {
//                            response = responseBodyResponse.errorBody().string();
//
//                            view.onError(new JSONObject(response).getString("message"));
//                        }
//                        }catch (Exception e)
//                        {
//
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }

    @Override
    public void getCallDetails() {

//        Observable<Response<ResponseBody>> observable = apiService.getCallDetails(manager.getAUTH(),Constants.selLang);
//
//        observable.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
//                    @Override
//                    public void onNext(Response<ResponseBody> responseBodyResponse) {
//
//                        int code = responseBodyResponse.code();
//
//                        try
//                        {
//
//
//                        switch (code)
//                        {
//                            case 200:
//
//                                break;
//                                default:
//                                    view.onError("Call Over");
//                                    break;
//                        }
//                        }catch (Exception e)
//                        {
//
//                            e.printStackTrace();
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }

    @Override
    public void setWindow(Activity mActivity) {


        Window window = mActivity.getWindow();

        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_FULLSCREEN);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            window.getDecorView()
                    .setSystemUiVisibility(android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
                            | android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else {
            window.getDecorView()
                    .setSystemUiVisibility(android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
                    );

        }


        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @Override
    public void checkIsCallerStillWaiting(String callID) {
        JSONObject jsonObject = new JSONObject();

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(manager.getCallToken(), ServiceUrl.CALL + "/" + callID, OkHttp3ConnectionStatusCode.Request_type.GET, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        //let user answer
                                        break;
                                    default:
                                        Log.d(TAG, "ondefault: " + result);
                                        view.onCallNotFound(callID,jsonObject.optString("message","Call Not Found"));
                                        break;
                                }
                            } else {
                                Log.d(TAG, "ondefault: " + result);
                                view.onSuccessDec();
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "ondefault: " + result);
                            view.onSuccessDec();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {


                        Log.d(TAG, "onErroir: " + error);
                    }
                });
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }


    @Override
    public void endCall(String callID, String callFrom) {

        JSONObject jsonObject = new JSONObject();
        try {
            Log.d(TAG, "endCall: "+"-"+callID+"-"+callFrom);

            jsonObject.put("callId", callID);
            jsonObject.put("callFrom", callFrom);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(manager.getCallToken(), ServiceUrl.CALL, OkHttp3ConnectionStatusCode.Request_type.DELETE, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        try {
                                            String response = result;
                                            Log.d(TAG, "onNext: endCallResponse: " + response);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        if (view != null)
                                            view.onSuccessDec();
                                        break;
                                    default:
                                        Log.d(TAG, "ondefault: " + result);
                                        break;
                                }
                            } else {
                                Log.d(TAG, "ondefault: " + result);
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "ondefault: " + result);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {


                        Log.d(TAG, "onErroir: " + error);
                    }
                });
    }

    @Override
    public void answerCall(String call_id) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(manager.getCallToken(), ServiceUrl.CALL + "/" + call_id, OkHttp3ConnectionStatusCode.Request_type.PUT, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        if (view != null)
                                            view.onSuccessAns();
                                        break;
                                    default:
                                        if (view != null)
                                            view.onSuccessDec();
                                        Log.d(TAG, "ondefault: " + result);
                                        break;
                                }
                            } else {
                                Log.d(TAG, "ondefault: " + result);
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "ondefault: " + result);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {


                        Log.d(TAG, "onErroir: " + error);
                    }
                });
    }
}
