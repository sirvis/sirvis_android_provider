package com.sirvis.pro.address;

import android.util.Log;

import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressSaveModel</h1>
 * AddressSaveModel model for AddressSaveActivity
 * @see AddressSaveActivity
 */

public class AddressSaveModel {
    private static final String TAG = "AddressSaveModel";
    private AddressSaveModellImple modelImplement;

    AddressSaveModel(AddressSaveModellImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    void getAddress(String latitude, String longitude)
    {
        String url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&sensor=true&key="+ BuildConfig.GOOGLE_API_KEY;
        Log.d(TAG,url);

        OkHttp3ConnectionStatusCode.doOkHttp3Connection("",url , OkHttp3ConnectionStatusCode.Request_type.GET,
                new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {

                                Log.d(TAG, "onSuccess: "+result);
                                searchZipCode(result);
                            } else {
                                modelImplement.onFailureFetchingAddresss();
                            }
                        }
                        catch (Exception e)
                        {
                            modelImplement.onFailureFetchingAddresss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailureFetchingAddresss();
                    }
                });
    }

    private String searchZipCode(String address_info) {
        String zipcode="";

        try {

            JSONObject jsonObject = new JSONObject(address_info);
            JSONObject jsonObject1=null;
            JSONArray jsonArray,jsonArray1,jsonArray2;
            boolean found=false;

            if (jsonObject.has("results")) {
                jsonArray=jsonObject.getJSONArray("results");
                Log.d("zip_response123","results: "+jsonArray.length());

                if(jsonArray.length()>0) {
                    String formatted_address = null,place_id = null;
                    if(jsonArray.getJSONObject(0).has("formatted_address")) {
                        formatted_address = jsonArray.getJSONObject(0).getString("formatted_address");
                        place_id = jsonArray.getJSONObject(0).getString("place_id");
                    }

                    if(jsonArray.getJSONObject(0).has("geometry")) {
                        JSONObject geometry = jsonArray.getJSONObject(0).getJSONObject("geometry");
                        if(geometry.has("location")) {
                            JSONObject location = geometry.getJSONObject("location");
                            double lat =location.getDouble("lat");
                            double lng =location.getDouble("lng");


                            if(formatted_address!=null && lat!=0 && lng!=0 ){
                                LocationPlaces places=new LocationPlaces();
                                places.setAddress(formatted_address);
                                places.setPlace_id(place_id);
                                places.setLatitude(lat);
                                places.setLongitude(lng);
                                places.setState("");
                                places.setCity("");
                                places.setPincode("");

                                if(jsonArray.getJSONObject(0).has("address_components"))
                                {
                                    JSONArray address_components = jsonArray.getJSONObject(0).getJSONArray("address_components");

                                    for(int i = 0 ; i< address_components.length(); i++)
                                    {
                                        JSONObject address_component = address_components.getJSONObject(i);

                                        JSONArray types = address_component.getJSONArray("types");

                                        if(types.get(0).equals("administrative_area_level_1"))
                                        {
                                            places.setState(address_component.getString("long_name"));
                                        }

                                        if(types.get(0).equals("administrative_area_level_2"))
                                        {
                                            places.setCity(address_component.getString("long_name"));
                                        }

                                        if(types.get(0).equals("postal_code"))
                                        {
                                            places.setPincode(address_component.getString("long_name"));
                                        }
                                    }
                                }

                                modelImplement.onAddress(places);
                            }else {
                                modelImplement.onFailureFetchingAddresss();
                            }
                        }else {
                            modelImplement.onFailureFetchingAddresss();
                        }
                    }else {
                        modelImplement.onFailureFetchingAddresss();
                    }
                }else {
                    modelImplement.onFailureFetchingAddresss();
                }
            }
            else {
                modelImplement.onFailureFetchingAddresss();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            modelImplement.onFailureFetchingAddresss();
        }

        return zipcode;
    }

    /**
     * method for calling the api for saving the address
     * @param isEdit option for edit or save
     * @param addressId if its's edit then api need address id
     * @param sessiontoken session token
     * @param jsonObject required field
     */
    void saveEditAddress(boolean isEdit, String addressId, String sessiontoken, JSONObject jsonObject)
    {
        try {
            if(jsonObject.getString("taggedAs").equals(""))
            {
                modelImplement.onEmptyTagged();
                return;
            }
            else if(jsonObject.getString("addLine1").equals(""))
            {
                modelImplement.onEmptyAddressLine();
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.Request_type methodType = OkHttp3ConnectionStatusCode.Request_type.POST;

        if(isEdit)
        {
            methodType = OkHttp3ConnectionStatusCode.Request_type.PATCH;
            try {
                jsonObject.put("id",addressId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.ADDRESS, methodType, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * AddressSaveModellImple interface for view implementation
     */
    interface AddressSaveModellImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(String msg);

        void onEmptyTagged();
        void onEmptyAddressLine();

        void onFailureFetchingAddresss();
        void onAddress(LocationPlaces places);
    }
}
