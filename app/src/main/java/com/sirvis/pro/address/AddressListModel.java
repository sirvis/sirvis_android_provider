package com.sirvis.pro.address;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.address.AddressData;
import com.sirvis.pro.pojo.address.AddressPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressListModel</h1>
 * AddressListModel model AddressListActivity
 * @see AddressListActivity
 */

public class AddressListModel {
    private static final String TAG = "AddressListModel";
    private AddressListModellImple modelImplement;

    AddressListModel(AddressListModellImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for getting the address list
     * @param sessiontoken session Token
     */
    void getAddress(String sessiontoken)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.ADDRESS , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        AddressPojo addressPojo = gson.fromJson(result,AddressPojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(addressPojo.getData());
                        } else {
                            modelImplement.onFailure(addressPojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * method for calling api to delete the addresss based on the id
     * @param sessiontoken session Token
     * @param id address id
     */
    void deleteAddress(String sessiontoken,String id)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",id);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.ADDRESS+"/"+id , OkHttp3ConnectionStatusCode.Request_type.DELETE, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if(statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessDeleteAddress(jsonObject.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }



    interface AddressListModellImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ArrayList<AddressData> addressDatas);
        void onSuccessDeleteAddress(String msg);
    }
}
