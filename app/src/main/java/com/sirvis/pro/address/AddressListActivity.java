package com.sirvis.pro.address;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.AddressListAdapter;
import com.sirvis.pro.main.schedule.addschedule.ScheduleSelectionActivity;
import com.sirvis.pro.pojo.address.AddressData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressListActivity</h1>
 * AddressListActivity for showing the list of address
 */

public class AddressListActivity extends AppCompatActivity implements View.OnClickListener, AddressListPresenter.AddressListPresenterImple, AddressListAdapter.AddressEditListener {

    private static final String TAG = "AddressList";
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private AddressListPresenter addressListPresenter;
    private ArrayList<AddressData> addressDatas;
    private AddressListAdapter addressListAdapter;

    private static final int PLACE_PICKER_REQUEST = 1;
    private int deletePosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);

        init();
    }

    /**
     * init the views
     */
    private void init()
    {
        addressListPresenter = new AddressListPresenter(this);
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingAddress));
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.yourAddresses));
        tvTitle.setTypeface(fontBold);

        TextView tvAddaddress = findViewById(R.id.tvAddaddress);
        tvAddaddress.setTypeface(fontBold);
        tvAddaddress.setOnClickListener(this);

        boolean isFromProfile = getIntent().getBooleanExtra("isFromProfile",false);

        RecyclerView rvAddress = findViewById(R.id.rvAddress);
        rvAddress.setLayoutManager(new LinearLayoutManager(this));
        addressDatas = new ArrayList<>();
        addressListAdapter = new AddressListAdapter(this,addressDatas,this,isFromProfile);
        rvAddress.setAdapter(addressListAdapter);

        addressListPresenter.getAddress(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(VariableConstant.IS_ADDRESS_LIST_CHANGED)
        {
            VariableConstant.IS_ADDRESS_LIST_CHANGED = false;
            progressDialog.setMessage(getString(R.string.gettingAddress));
            addressListPresenter.getAddress(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvAddaddress:

               /* try {
                    LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds
                            (new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng())),
                                    new LatLng(Double.parseDouble(sessionManager.getCurrentLat()),Double.parseDouble(sessionManager.getCurrentLng())));

                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }*/

                Intent intent = new Intent(this,AddressSaveActivity.class);
                intent.putExtra("isSignup",false);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(intent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
/*
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Place place = PlacePicker.getPlace(this, data);
            CharSequence name = place.getName();
            CharSequence address = place.getAddress();
            String latitude = String.valueOf(place.getLatLng().latitude);
            String longitude = String.valueOf(place.getLatLng().longitude);
            String id = place.getId();

            Log.d(TAG, "onActivityResult: "+place + "\n"+ name + address + latitude + longitude + id);

            Intent intent = new Intent(this,AddressSaveActivity.class);
            intent.putExtra("id",id);
            intent.putExtra("name",name);
            intent.putExtra("address",address);
            intent.putExtra("latitude",latitude);
            intent.putExtra("longitude",longitude);
            intent.putExtra("taggedAs","home");
            intent.putExtra("isEdit",false);
            startActivity(intent);
        } else {*/
            super.onActivityResult(requestCode, resultCode, data);
        /*}*/
    }

    @Override
    public void onAddressEdit(int position) {
        Intent intentEdit = new Intent(AddressListActivity.this,AddressSaveActivity.class);
        intentEdit.putExtra("addressId",addressDatas.get(position).get_id());
        intentEdit.putExtra("id",addressDatas.get(position).getPlaceId());
        intentEdit.putExtra("name","");
        intentEdit.putExtra("address",addressDatas.get(position).getAddLine1()+" "+addressDatas.get(position).getAddLine2());
        intentEdit.putExtra("latitude",addressDatas.get(position).getLatitude());
        intentEdit.putExtra("longitude",addressDatas.get(position).getLongitude());
        intentEdit.putExtra("taggedAs",addressDatas.get(position).getTaggedAs());
        intentEdit.putExtra("isEdit",true);
        startActivity(intentEdit);
    }

    @Override
    public void onAddressDelete(final int position) {
        deletePosition = position;

        if(addressDatas.get(position).getDefaultAddress().equals("1"))
        {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_deleting_address,null);
            alertDialogBuilder.setView(view);
            TextView tvTitle= view.findViewById(R.id.tvTitle);
            TextView tvGotIt= view.findViewById(R.id.tvGotIt);
            ImageView ivClose  = view.findViewById(R.id.ivClose);

            /*tvTitle.setTypeface(fontMedium);
            tvSubmit.setTypeface(fontBold);*/
            final AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            tvGotIt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.alert));
            builder.setMessage(getString(R.string.addressDeleteMsg));
            builder.setPositiveButton(getString(R.string.oK), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progressDialog.setMessage(getString(R.string.deletingAddress));
                    addressListPresenter.deleteAddress(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),addressDatas.get(position).get_id());
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
    }

    @Override
    public void onAddressSelect(int position) {
        Intent intentAddress=new Intent(this,ScheduleSelectionActivity.class);
        intentAddress.putExtra("SELECTED_ADDRESS_ID", addressDatas.get(position).get_id());
        intentAddress.putExtra("SELECTED_ADDRESS_NAME", addressDatas.get(position).getAddLine1()+" "+addressDatas.get(position).getAddLine2());
        setResult(RESULT_OK,intentAddress);
        closeActivity();
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(ArrayList<AddressData> addressDatas) {
        this.addressDatas.clear();
        this.addressDatas.addAll(addressDatas);
        addressListAdapter.notifyDataSetChanged();

        if(addressDatas.size() == 0)
        {
            Toast.makeText(this,getString(R.string.noAddressFound),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessDeleteAddress(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        addressDatas.remove(deletePosition);
        addressListAdapter.notifyDataSetChanged();
    }


}
