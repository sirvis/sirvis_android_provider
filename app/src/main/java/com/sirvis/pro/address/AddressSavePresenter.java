package com.sirvis.pro.address;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressSavePresenter</h1>
 * AddressSavePresenter presenter for AddressSaveActivity
 * @see AddressSaveActivity
 */

public class AddressSavePresenter implements AddressSaveModel.AddressSaveModellImple {
    private AddressSaveModel model;
    private AddressSavePresenterImple presenterImple;
    private Geocoder geocoder;

    AddressSavePresenter(Context context, AddressSavePresenterImple presenterImple) {
        model = new AddressSaveModel(this);
        this.presenterImple = presenterImple;
        geocoder = new Geocoder(context, Locale.ENGLISH);
    }

    void getAddress(String latitude, String longitude)
    {
        //model.getAddress(latitude,longitude);
        new GetAddress(latitude, longitude).execute();
    }


    @SuppressLint("StaticFieldLeak")
    private class GetAddress extends AsyncTask<String, Void, String> {

        String lat, lng;

        GetAddress(String lat, String lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        @Override
        protected String doInBackground(String[] params) {
            Log.d("Addresss", "doInBackground: "+lat + "   "+lng);
            String address="";
            try {
                try {
                    List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lng), 1);
                    if (addresses.size() > 0) {
                        Address add = addresses.get(0);
                        address = add.getAddressLine(0);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return address;
        }

        @Override
        protected void onPostExecute(String address) {
            Log.d("Addresss", "onPostExecute: "+address);
            presenterImple.onAddress(address);
        }
    }


    /**
     * method for passing values from view to model
     * @param isEdit option for edit or save
     * @param addressId if its's edit then api need address id
     * @param sessionToken session token
     * @param jsonObject required field
     */
    void saveEditAddress(boolean isEdit, String addressId, String sessionToken, JSONObject jsonObject){
        presenterImple.startProgressBar();
        model.saveEditAddress(isEdit,addressId,sessionToken,jsonObject);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg);
    }

    @Override
    public void onEmptyTagged() {
        presenterImple.stopProgressBar();
        presenterImple.onEmptyTagged();
    }

    @Override
    public void onEmptyAddressLine() {
        presenterImple.stopProgressBar();
        presenterImple.onEmptyAddressLine();
    }

    @Override
    public void onFailureFetchingAddresss() {
        presenterImple.onFailureFetchingAddresss();
    }

    @Override
    public void onAddress(LocationPlaces places) {
        presenterImple.onAddress(places);
    }

    /**
     * AddressSavePresenterImple interface for view implementation
     */
    interface AddressSavePresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(String msg);

        void onEmptyTagged();
        void  onEmptyAddressLine();

        void onFailureFetchingAddresss();
        void onAddress(LocationPlaces places);
        void onAddress(String address);
    }

}
