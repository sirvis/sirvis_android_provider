package com.sirvis.pro.address;

import android.annotation.TargetApi;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by embed on 21/9/16.
 * <h1>APICallerForAddress</h1>
 * APICallerForAddress Google api call for address
 */
public class APICallerForAddress {

    private String TAG = "APICall";
    static Call call;
    public static OkHttpClient client = new OkHttpClient();
    Context context;
    Location location;
    double latitude,longitude;
    AddressProvider addressProvider;

    public APICallerForAddress(){

    }

    public void setForAddress(Location location, AddressProvider addressProvider) {
        this.location = location;
        latitude=location.getLatitude();
        longitude=location.getLongitude();
        this.addressProvider = addressProvider;
        new Fourquare().execute();
    }

    public void setForAddress(double latitude,double longitude, AddressProvider addressProvider) {
        this.latitude=latitude;
        this.longitude=longitude;
        this.addressProvider = addressProvider;

    }


    public class Fourquare extends AsyncTask<View, Void, String> {

        @Override
        protected void onPreExecute() {
            // we can start a progress bar here
        }

        @Override
        protected String doInBackground(View... urls) {
            // make Call to the url
//			String url="https://api.foursquare.com/v2/venues/explore?client_id=PH1WFYBTT14OBUMZWMQEKIQDVL2JIM3YDWZE1DKGCX4KWXHC&client_secret=0ROGHJXSBMOEPQKX4KJQJINSA2CGDJJOFZCK5ZRV3ZKUOBTL&v=20160727&ll=47.606208,-122.332068&radius=80000&sortByDistance=1&venuePhotos=1&query="+type;
//            String url="http://maps.googleapis.com/maps/api/geocode/json?latlng=15.842507,74.511666&sensor=true";
            String url="http://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&sensor=true";
            Log.d("zip_response123",url);
            String resp=makeCall(url);
            String zip = searchZipCode(resp);
//            Log.d("zip_response123",zip);
            return "";
        }

        @Override
        protected void onPostExecute(String result) {

        }
    }

    private String searchZipCode(String address_info) {
        String zipcode="";

        try {

            JSONObject jsonObject = new JSONObject(address_info);
            JSONObject jsonObject1=null;
            JSONArray jsonArray,jsonArray1,jsonArray2;
            boolean found=false;

            if (jsonObject.has("results")) {
                jsonArray=jsonObject.getJSONArray("results");
                Log.d("zip_response123","results: "+jsonArray.length());

                if(jsonArray.length()>0) {
                    String formatted_address = null,place_id = null;
                    if(jsonArray.getJSONObject(0).has("formatted_address")) {
                        formatted_address = jsonArray.getJSONObject(0).getString("formatted_address");
                        place_id = jsonArray.getJSONObject(0).getString("place_id");
                    }

                    if(jsonArray.getJSONObject(0).has("geometry")) {
                        JSONObject geometry = jsonArray.getJSONObject(0).getJSONObject("geometry");
                        if(geometry.has("location")) {
                            JSONObject location = geometry.getJSONObject("location");
                            double lat =location.getDouble("lat");
                            double lng =location.getDouble("lng");
                            if(formatted_address!=null && lat!=0 && lng!=0 ){
                                LocationPlaces places=new LocationPlaces();
                                places.setAddress(formatted_address);
                                places.setPlace_id(place_id);
                                places.setLatitude(lat);
                                places.setLongitude(lng);
                                addressProvider.onAddress(places);
                            }else {
                                addressProvider.onError("No Location Found!");
                            }
                        }else {
                            addressProvider.onError("No Location Found!");
                        }
                    }else {
                        addressProvider.onError("No Location Found!");
                    }
                }else {
                    addressProvider.onError("No Location Found!");
                }
            }
            else {
                addressProvider.onError("No Location Found!");
            }

        } catch (JSONException e) {
            e.printStackTrace();
            addressProvider.onError("No Location Found!");
        }

        return zipcode;
    }

    public static String makeCall(String url) {
        try {
            String response = run(url);
//            System.out.println(response);
//            Log.d("okhttp_respo",response);
            return response.trim();
        }catch (IOException e){
            Log.d("okhttp_respo","crashed response");
            return "";
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

//        call = client.newCall(request);
        try {
            Log.d("response_obt","\t 1");
            Response response = null;// = call.execute();
            if(call!=null) {
                Log.d("response_obt","\t 2");
                if (call.isExecuted()) {
                    Log.d("response_obt","\t 3");
                    call = client.newCall(request);
                    response=call.execute();
                    Log.d("response_obt","\t 4");
                }else {
                    Log.d("response_obt","\t 5");
                    if(!call.isCanceled()) {
                        Log.d("response_obt","\t 6");
                        call.cancel();
                        Log.d("response_obt","\t 7");
                        call = client.newCall(request);
                        response = call.execute();
                        Log.d("response_obt","\t 8");
                    }
                }
            }else {
                Log.d("response_obt","\t 9");
                call = client.newCall(request);
                response=call.execute();
                Log.d("response_obt","\t 10");
            }
            return response.body().string();
        }catch (Exception e){
//            Log.d("response_obt",e.getMessage());
            return "";
        }
    }

    public interface AddressProvider {
        void onAddress(LocationPlaces places);
        void onError(String error);
    }
}
