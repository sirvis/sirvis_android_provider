package com.sirvis.pro.address;

import com.sirvis.pro.pojo.address.AddressData;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressListPresenter</h1>
 * AddressListPresenter presenter for AddressListActivity
 * @see AddressListActivity
 */

public class AddressListPresenter implements AddressListModel.AddressListModellImple
{
    private AddressListModel model;
    private AddressListPresenterImple presenterImple;

    AddressListPresenter(AddressListPresenterImple presenterImple) {
        model = new AddressListModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing values from view to model
     * @param sessionToken session Token
     */
    void getAddress(String sessionToken){
        presenterImple.startProgressBar();
        model.getAddress(sessionToken);
    }

    /**
     * method for passing values from view to model
     * @param sessionToken
     * @param id
     */
    void deleteAddress(String sessionToken,String id)
    {
        presenterImple.startProgressBar();
        model.deleteAddress(sessionToken,id);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(ArrayList<AddressData> addressDatas) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(addressDatas);
    }

     @Override
    public void onSuccessDeleteAddress(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessDeleteAddress(msg);
    }

    /**
     * AddressListPresenterImple interface for View Implementation
     */
    interface AddressListPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(ArrayList<AddressData> addressDatas);
        void onSuccessDeleteAddress(String msg);
    }

}
