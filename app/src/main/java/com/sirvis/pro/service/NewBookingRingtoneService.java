package com.sirvis.pro.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;

import com.sirvis.pro.R;

/**
 * Created by murashid on 23-Mar-18.
 */

public class NewBookingRingtoneService  extends Service{

    private Handler vibrateHandler;
    private Runnable vibrateRunnable;
    private Vibrator vibrator;
    private int vibrateSecond =0;
    private int vibrateTiming = 0;
    private Ringtone bookingTone;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        vibrateTiming = (int) (29/2.5); // ringtone time divided by runnable second

        bookingTone =  RingtoneManager.getRingtone(this, Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.booking_tone));
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrateHandler = new Handler();
        vibrateRunnable = new Runnable() {
            @Override
            public void run() {
                if(vibrateHandler !=null && vibrateRunnable!=null && vibrator != null && vibrateSecond < vibrateTiming)
                {
                    vibrator.vibrate(1000);
                    vibrateHandler.postDelayed(vibrateRunnable,2500);
                    vibrateSecond += 1;
                }
            }
        };
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if(bookingTone !=null && !bookingTone.isPlaying())
        {
            bookingTone.play();

            if(vibrateHandler !=null && vibrateRunnable !=null )
            {
                vibrateSecond = 0;
                vibrateHandler.post(vibrateRunnable);
            }
        }

        return START_NOT_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
    }
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public void onStop() {

    }
    public void onPause() {

    }
    @Override
    public void onDestroy() {
        if(bookingTone !=null)
        {
            bookingTone.stop();
            bookingTone = null;
        }
        if(vibrateHandler !=null && vibrateRunnable !=null)
        {
            vibrateHandler.removeCallbacks(vibrateRunnable);
            vibrator = null;
            vibrateHandler = null;
            vibrateRunnable = null;
        }
    }

    @Override
    public void onLowMemory() {

    }
}
