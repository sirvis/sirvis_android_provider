package com.sirvis.pro.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.LocationUtil;
import com.sirvis.pro.mqtt.MqttEvents;
import com.sirvis.pro.utility.OkHttp3ConnectionUpdateLocation;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by murashid on 06-Mar-18.
 */

public class OfflineLocationPublishService extends Service implements LocationUtil.GetLocationListener {

    private SessionManager sessionManager;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private String TAG="OfflineLocation";
    private LocationUtil locationUtil;

    private BroadcastReceiver mBatInfoReceiver;
    private String batteryLevel = "50";

    private boolean isLocationLogDpUpdated = false;
    private boolean isLocationLogApiCalling = false;

    private double prevLat, prevLng, strayLat = -1.0, strayLng = -1.0;

    String transit = "0";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        mBatInfoReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent intent) {
                // TODO Auto-generated method stub
                int level = intent.getIntExtra("level", 0);
                batteryLevel = String.valueOf(level);
            }
        };

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(mBatInfoReceiver, ifilter);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sessionManager = SessionManager.getSessionManager(this);

        try {
            if(intent.getAction() != null && intent.getAction().equals(VariableConstant.ACTION.STARTOFFLINELOCATIONSERVICE))
            {
                if(locationUtil == null)
                {
                    locationUtil = new LocationUtil(this,this);
                }
                if(!locationUtil.isGoogleApiClientConnected())
                {
                    locationUtil.connectGoogleApiClient();
                }
                AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getProviderId(),false);
                startPublishingWithTimer();
            }
            else if(intent.getAction() != null && intent.getAction().equals(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE))
            {
                if(locationUtil !=null )
                {
                    locationUtil.disconnectGoogleApiClient();
                }

                stopSelf();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return START_REDELIVER_INTENT;
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if(mTimer != null)
        {
            mTimer.cancel();
        }
        if(mTimerTask != null)
        {
            mTimerTask.cancel();
        }

        unregisterReceiver(mBatInfoReceiver);
    }

    /**
     * method for creating Timer and TimerTask for contineous updating latlng
     * period is based on api response in appconfig
     */
    private void startPublishingWithTimer()
    {
        long countDown = 1000 * Long.parseLong(sessionManager.getLocationPublishInterval());
        mTimer=new Timer();
        mTimerTask= new TimerTask() {
            @Override
            public void run() {
                if(Utility.isNetworkAvailable(getApplicationContext()))
                {
                    if(!isLocationLogDpUpdated)
                    {
                        updateLocation();
                    }
                    else if(!isLocationLogApiCalling)
                    {
                        updateLocationLogs();
                    }
                }
                else
                {
                    isLocationLogDpUpdated = true;
                    sessionManager.updateLocationArray(sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                }

                calculateRouteDistance();
            }
        };
        mTimer.schedule(mTimerTask,000,countDown);
    }

    /**
     * method for calling api to update the updateLocation
     */
    private void updateLocation()
    {
        String locationCheck = "0";
        if(Utility.isGpsEnabled(this))
        {
            locationCheck = "1";
        }
        JSONObject jsonObject=new JSONObject();
        try{

            jsonObject.put("latitude",Double.parseDouble(sessionManager.getCurrentLat()));
            jsonObject.put("longitude",Double.parseDouble(sessionManager.getCurrentLng()));
            jsonObject.put("status",VariableConstant.OFFLINE_STATUS);
            jsonObject.put("batteryPercentage",batteryLevel);
            jsonObject.put("locationHeading","car");
            jsonObject.put("locationCheck",locationCheck);
            jsonObject.put("appVersion",VariableConstant.APP_VERSION);
            jsonObject.put("transit", transit);
            jsonObject.put("bookingStr",sessionManager.getBookingStr());

            if(sessionManager.getIsDriverOnJob())
            {
                JSONObject mqttJson = new JSONObject(jsonObject.toString());
                mqttJson.put("pid",sessionManager.getProviderId());
                AppController.getInstance().getMqttHelper().publish(MqttEvents.LiveTrack.value+"/"+sessionManager.getProviderId(),mqttJson,1,false);
            }

            //Log.d(TAG, "updateLocation: request "+jsonObject);

            OkHttp3ConnectionUpdateLocation.doOkHttp3Connection(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), ServiceUrl.UPDATE_LOCATION , OkHttp3ConnectionUpdateLocation.Request_type.PATCH, jsonObject, new OkHttp3ConnectionUpdateLocation.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String statusCode, String result) {
                    try {
                        if (result != null)
                        {
                            //Log.d(TAG, "updateLocation: response "+statusCode);

                            final JSONObject jsonObject = new JSONObject(result);
                            if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                            {
                                RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                                                sessionManager.getPassword() , newToken);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        try {
                                            AppController.getInstance().toast(jsonObject.getString("message"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        Utility.logoutSessionExiperd(sessionManager,OfflineLocationPublishService.this);
                                    }
                                });
                            }
                            else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                            {
                                try {
                                    AppController.getInstance().toast(jsonObject.getString("message"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Utility.logoutSessionExiperd(sessionManager,OfflineLocationPublishService.this);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error) {
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void calculateRouteDistance() {

        if (prevLat == 0.0 || prevLng == 0.0) {
            prevLat = Double.parseDouble(sessionManager.getCurrentLat());
            prevLng = Double.parseDouble(sessionManager.getCurrentLng());
        }

        double curLat = Double.parseDouble(sessionManager.getCurrentLat());
        double curLong = Double.parseDouble(sessionManager.getCurrentLng());

        double dis = Utility.distanceInMeter(prevLat, prevLng, curLat, curLong);
        double distanceInMtr = 0.0;
        double disFromStaryPts = -1.0;

        if (strayLat != -1.0 && strayLng != -1.0) {
            disFromStaryPts = Utility.distanceInMeter(strayLat, strayLng, curLat, curLong);
        }

        if (((dis >= sessionManager.getLatLongDisplacement()) && (dis <= 300)) || (disFromStaryPts != -1.0 && ((disFromStaryPts >= sessionManager.getLatLongDisplacement()) && (disFromStaryPts <= 300)))) {

            transit = "1";

            strayLat = prevLat = curLat;
            strayLng = prevLng = curLong;

            distanceInMtr = (dis > disFromStaryPts) ? dis : disFromStaryPts;

            String bookingIdWithStatus[] = sessionManager.getBookingStr().split(",");

            for(int i=0 ; i < bookingIdWithStatus.length ; i++)
            {
                String splitBookingIdStatus[] = bookingIdWithStatus[i].split("\\|");
                if(splitBookingIdStatus.length >= 2)
                {
                    String bookingId =  splitBookingIdStatus[0];
                    String status =  splitBookingIdStatus[1];

                    if(status.equals(VariableConstant.ON_THE_WAY))
                    {
                        //Distance in Arrived screen
                        double distance = distanceInMtr + sessionManager.getArrivedDistance(bookingId);
                        sessionManager.setArrivedDistance(bookingId,""+distance);
                    }
                    else if(status.equals(VariableConstant.JOB_TIMER_STARTED))
                    {
                        //Distance between Event stated and event completed
                        double distance = distanceInMtr + sessionManager.getOnJobDistance(bookingId);
                        sessionManager.setOnJobDistance(bookingId,""+distance);
                    }
                }
            }
        }
        else if(dis > 300 && (disFromStaryPts > 300 || disFromStaryPts == -1.0))
        {
            strayLat = curLat;
            strayLng = curLong;
        }

    }

    private void updateLocationLogs() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("latitude_longitude",sessionManager.getLocationArray());

            Log.d(TAG, "updateLocationLogs: jsonArray "+jsonObject);
            isLocationLogApiCalling = true;

            OkHttp3ConnectionUpdateLocation.doOkHttp3Connection(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), ServiceUrl.UPDATE_LOCATION_LOGS , OkHttp3ConnectionUpdateLocation.Request_type.PATCH,
                    jsonObject, new OkHttp3ConnectionUpdateLocation.OkHttp3RequestCallback() {
                        @Override
                        public void onSuccess(String statusCode, String result) {
                            try {
                                Log.d(TAG, "onSuccess: "+statusCode +"  "+result);
                                if (result != null)
                                {
                                    if(statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                                    {
                                        isLocationLogDpUpdated = false;
                                        isLocationLogApiCalling = false;
                                        sessionManager.clearLocationArray();
                                    }
                                    else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                                    {
                                        JSONObject jsonObject = new JSONObject(result);
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                                                        sessionManager.getPassword() , newToken);
                                            }
                                            @Override
                                            public void onFailureRefreshToken() {

                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                try {
                                                    AppController.getInstance().toast(msg);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                Utility.logoutSessionExiperd(sessionManager, OfflineLocationPublishService.this);
                                            }
                                        });
                                    }
                                    else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                                    {
                                        JSONObject jsonObject = new JSONObject(result);
                                        try {
                                            AppController.getInstance().toast(jsonObject.getString("message"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Utility.logoutSessionExiperd(sessionManager,OfflineLocationPublishService.this);
                                    }
                                    else
                                    {
                                        isLocationLogApiCalling = false;
                                    }
                                }
                                else
                                {
                                    isLocationLogApiCalling = false;
                                }
                            }
                            catch (Exception e)
                            {
                                isLocationLogApiCalling = false;
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(String error) {
                            isLocationLogApiCalling = false;
                        }
                    });
        }
        catch (Exception e)
        {
            isLocationLogApiCalling = false;
            e.printStackTrace();
        }
    }

    @Override
    public void updateLocation(Location location) {
        sessionManager.setCurrentLat(String.valueOf(location.getLatitude()));
        sessionManager.setCurrentLng(String.valueOf(location.getLongitude()));
    }

    @Override
    public void location_Error(String error) {

    }
}
