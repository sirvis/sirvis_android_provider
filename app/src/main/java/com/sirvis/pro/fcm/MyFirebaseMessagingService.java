package com.sirvis.pro.fcm;

import android.content.ContentResolver;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.main.chats.ChatOnMessageCallback;
import com.sirvis.pro.mqtt.MqttEvents;
import com.sirvis.pro.pojo.chat.ChatData;
import com.sirvis.pro.service.LocationPublishService;
import com.sirvis.pro.telecall.utility.CallingApis;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CalendarEventHelper;
import com.sirvis.pro.utility.DataBaseChat;
import com.sirvis.pro.utility.NotificationHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murshid on 18/9/16.
 * <h2>MyFirebaseMessagingService</h2>
 * MyFirebaseMessagingService is used for receiveng messsage from fcm
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static ChatOnMessageCallback chatListener;


    public static void setChatListener(ChatOnMessageCallback chatList) {
        chatListener = chatList;
    }

    /**
     *
     * Called when message is jsonRemoteMessage.
     *
     * @param remoteMessage Object representing the message jsonRemoteMessage from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived Called" + remoteMessage.toString());
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. AppConfigData messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. AppConfigData messages are the type
        // traditionally used with GCM. Notification messages are only jsonRemoteMessage here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        SessionManager sessionManager = SessionManager.getSessionManager(this);
        DataBaseChat db = new DataBaseChat(this);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            try {
                JSONObject jsonRemoteMessage = new JSONObject(remoteMessage.getData());
                String action = jsonRemoteMessage.getString("action");
                String message = jsonRemoteMessage.optString("msg", "");
                String title = jsonRemoteMessage.optString("title", "");

                Gson gson = new Gson();

                if (action != null) {
                    if (action.equals("112") || action.equals("1") && jsonRemoteMessage.has("pushType") && jsonRemoteMessage.getString("pushType").equals("2")) {
                        ChatData chatData = gson.fromJson(jsonRemoteMessage.getString("data"), ChatData.class);
                        if (!VariableConstant.IS_CHATTING_OPENED) {
                            db.addNewChat(String.valueOf(chatData.getBid()), chatData.getContent(), sessionManager.getProviderId(),
                                    chatData.getFromID(), String.valueOf(chatData.getTimestamp()), chatData.getType(), "1");
                        }

                        if (chatData.getTimestamp() > sessionManager.getLastTimeStampMsg()) {
                            sessionManager.setLastTimeStampMsg(chatData.getTimestamp());

                            if (chatListener != null) {
                                chatListener.onMessageReceived(chatData);
                            }

                            if (!VariableConstant.IS_CHATTING_RESUMED) {
                                sessionManager.setChatCount(String.valueOf(chatData.getBid()), sessionManager.getChatCount(String.valueOf(chatData.getBid())) + 1);
                                sessionManager.setChatBookingID(String.valueOf(chatData.getBid()));
                                sessionManager.setChatCustomerName(chatData.getName());
                                sessionManager.setChatCustomerID(chatData.getFromID());

                                NotificationHelper.sendChatNotification(this, chatData.getName(), message, sessionManager.getChatNotificationId());
                                sessionManager.setChatNotificationId(sessionManager.getChatNotificationId() + 1);

                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_NEW_CHAT);
                                sendBroadcast(intent);

                            } else if (VariableConstant.IS_IN_CALL) {
                                NotificationHelper.sendChatNotification(this, chatData.getName(), chatData.getContent(), sessionManager.getChatNotificationId());
                                sessionManager.setChatNotificationId(sessionManager.getChatNotificationId() + 1);
                            }
                        }

                        return;
                    }

                    /*
                     * Bug id : Trello 187
                     * Bug Description : Getting chat notification 2 times
                     * Fixed Description :  Handled the action 112. Before for chat notification the action was 1 but now it was 112.
                     * Dev : Murashid
                     * Date : 29-11-2018
                     * */

                    if (!action.equals("25") && !action.equals("10") && !action.equals("1") && !action.equals("12") && !action.equals("5") && !action.equals("112") && !action.equals("120")) {
                        NotificationHelper.sendNotification(this, action, title, message);
                    }

                    /*
                    1 => New BookingStatus , 18 => BookingAccepted or rejected by email
                    5 = > Expired 12 => Cancel,Unassign 22 => ban, 23 => logout , 21 => offline
                    25 = > Profile activation 26 = > Booking updated from super admin
                    10=>cuatomer done the payment for booking,rate the customer now.
                    201=>Telle-Call Booking End alert notification

                    * */
                    if (action.equals("1")) {
                        VariableConstant.IS_BOOKING_UPDATED = true;


                        NotificationHelper.sendNotification(this,action,title,message);

                        JSONObject jsonObjectBooking = new JSONObject(jsonRemoteMessage.optString("data", ""));

                        if (!jsonObjectBooking.equals(sessionManager.getLastBooking()) && !jsonObjectBooking.equals("")) {
                            sessionManager.setLastBooking(jsonObjectBooking.toString());
                            sessionManager.setIsNewBooking(true);
                            Log.d(TAG, "Booking from Fcm");
                           /* boolean isAssignedBooking = false;
                            if (jsonObjectBooking.has("status") && jsonObjectBooking.getString("status").equals("3")) {
                                sessionManager.setIsAssignedBooking(true);
                                isAssignedBooking = true;
                            }*/
                            if (VariableConstant.IS_MYBOOKING_OPENED /*&& !isAssignedBooking*/) {
                                //justRefresh();
                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                                sendBroadcast(intent);
                            } else if (VariableConstant.IS_ACCEPTEDBOOKING_OPENED /*&& isAssignedBooking*/) {
                                //justRefresh();
                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
                                sendBroadcast(intent);
                            } else {
                                sessionManager.setNewBookingFromMain(true);
                                VariableConstant.IS_BOOKING_UPDATED = true;
                                Intent intentOpen = new Intent(this, MainActivity.class);
                                intentOpen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                startActivity(intentOpen);
                            }

                            Utility.updateBookingAck(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObjectBooking.getString("bookingId"), sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                        }
                        if (jsonObjectBooking.getString("bookingType").equals("1")) {
                            sessionManager.setIsNowBooking(true);
                        } else {
                            sessionManager.setIsNowBooking(false);
                        }

                    } else if (action.equals("12") || action.equals("5") || action.equals("18")) {
                        JSONObject expireCancelJsonObject = new JSONObject(jsonRemoteMessage.optString("data", ""));
                        if (expireCancelJsonObject != null && !expireCancelJsonObject.equals("")) {
                            String bookingId = expireCancelJsonObject.getString("bookingId");
                            String header = expireCancelJsonObject.has("statusMsg") ? expireCancelJsonObject.getString("statusMsg") :
                                    title;
                            String msg = expireCancelJsonObject.has("cancellationReason") ? expireCancelJsonObject.getString("cancellationReason") :
                                    message;

                            if (expireCancelJsonObject.has("msg")) {
                                msg = expireCancelJsonObject.getString("msg");
                            }

                            if (!bookingId.equals(sessionManager.getLastBookingIdCancel())) {
                                sessionManager.setLastBookingIdCancel(bookingId);
                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                                intent.putExtra("cancelid", bookingId);
                                intent.putExtra("header", header);
                                intent.putExtra("msg", msg);
                                intent.putExtra("action", action);
                                sendBroadcast(intent);

                                VariableConstant.IS_BOOKING_UPDATED = true;
                                NotificationHelper.sendNotification(this, action, header, msg);

                                Log.d(TAG, "Booking cancel from Fcm");
                            }

                            if (expireCancelJsonObject.has("reminderId")) {
                                if (!expireCancelJsonObject.getString("reminderId").equals("")) {
                                    CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                                    calendarEventHelper.deleteEvent(expireCancelJsonObject.getString("reminderId"));
                                }
                            }
                        }

                    } else if (action.equals("21")) {
                        if (Utility.isMyServiceRunning(this, LocationPublishService.class)) {
                            Intent stopIntent = new Intent(this, LocationPublishService.class);
                            stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
                            startService(stopIntent);
                            AppController.getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
                        }
                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_MAKE_OFFLINE);
                        sendBroadcast(intent);
                    } else if (action.equals("23") || action.equals("22")) {
                        Utility.logoutSessionExiperd(sessionManager, this);
                    } else if (action.equals("26")) {
                        JSONObject expireCancelJsonObject = new JSONObject(jsonRemoteMessage.getString("data"));
                        String bookingId = expireCancelJsonObject.getString("bookingId");
                        String header = expireCancelJsonObject.has("statusMsg") ? expireCancelJsonObject.getString("statusMsg") :
                                title;
                        String msg = expireCancelJsonObject.has("cancellationReason") ? expireCancelJsonObject.getString("cancellationReason") :
                                message;

                        if (expireCancelJsonObject.has("msg")) {
                            msg = expireCancelJsonObject.getString("msg");
                        }

                        Intent intent = new Intent();
                        intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                        intent.putExtra("cancelid", bookingId);
                        intent.putExtra("header", header);
                        intent.putExtra("msg", msg);
                        intent.putExtra("action", action);
                        sendBroadcast(intent);
                        VariableConstant.IS_BOOKING_UPDATED = true;
                        NotificationHelper.sendNotification(this, action, header, msg);

                        Log.d(TAG, "Booking updated from Fcm");
                    } else if (action.equals("25")) {
                        sessionManager.setIsProfileAcivated(true);
                        if (VariableConstant.IS_MYBOOKING_OPENED || VariableConstant.IS_ACCEPTEDBOOKING_OPENED) {
                            Intent intent = new Intent();
                            intent.setAction(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION);
                            sendBroadcast(intent);
                        } else {
                            NotificationHelper.sendProfileActivationNotification(this, action, title, message);
                        }

                    } else if (action.equals("120")) {
                        Log.d(TAG, "Calling received: 1 " + jsonRemoteMessage.getString("data"));
                        JSONObject obj = new JSONObject(jsonRemoteMessage.optString("data", ""));
                        if (obj != null && !obj.equals("")) {
                            sessionManager.setChatCount(obj.getString("bookingId"), sessionManager.getChatCount(obj.getString("bookingId") + 1));
                            sessionManager.setChatBookingID(obj.getString("bookingId"));
                            sessionManager.setChatCustomerName(obj.getString("callerName"));
                            sessionManager.setChatCustomerID(obj.getString("callerId"));

                            JSONObject tempObj = new JSONObject();
                            tempObj.put("status", 0);

                            Log.d(TAG, "Calling received: 2 ");

                            if (AppController.getInstance().getMqttHelper().isMqttConnected()) {
                                AppController.getInstance().getMqttHelper().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getCallHelper().getUserId(), tempObj, 0, true);
                            } else {
                                AppController.getInstance().getMqttHelper().setUserId(AppController.getInstance().getCallHelper().getUserId(), tempObj);
                                AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getPhoneNumber(), false);
                            }
                            AppController.getInstance().getCallHelper().setActiveOnACall(true, true);
                            CallingApis.OpenIncomingCallScreen(obj, this);

                            Log.d(TAG, "Calling received: 3");
                        }

                    } else if (action.equals("10")) {
                        try {
                            Log.d(TAG, "onMessageReceived: 10");
                            JSONObject obj = new JSONObject(jsonRemoteMessage.getString("data"));
                            final String bookingId = obj.optString("bookingId");
                            if (Utility.isApplicationSentToBackground(this)) {
                                Log.d(TAG, "onMessageReceived: appbckgrnd");
                                NotificationHelper.sendRateCustomerNotification(this, bookingId, title, message);
                            } else {
                                Log.d(TAG, "onMessageReceived: noappbckgrnd");
                                Uri notification = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                                        + "://" + getPackageName() + "/raw/incoming");
                                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                r.play();
                                Intent intent = new Intent(this, MessageNotificationActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putBoolean("rateNotfcnCustomer", true);
                                bundle.putString("bookingId", bookingId);
                                bundle.putString("RemoteMessage", message);
                                bundle.putString("RemoteTitle", title);
                                intent.putExtras(bundle);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    else if(action.equals("201"))
                    {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}