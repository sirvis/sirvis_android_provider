package com.sirvis.pro.fcm;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.review.RateCustomerActivity;
import com.sirvis.pro.utility.DialogHelper;

public class MessageNotificationActivity extends AppCompatActivity {
    private Bundle bundle = null;
    private String RemoteMessage;
    private String RemoteTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notfcatn);
        bundle = getIntent().getExtras();

        try {
//            bookingId = bundle.getString("bookingId");
            RemoteMessage = bundle.getString("RemoteMessage");
            RemoteTitle = bundle.getString("RemoteTitle");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Dialog dialog = DialogHelper.showPopupWithOneButton(this);
        dialog.setCancelable(false);
        TextView textTitle = dialog.findViewById(R.id.title_popup);
        TextView text_for_popup = dialog.findViewById(R.id.text_for_popup);
        TextView yes_button = dialog.findViewById(R.id.yes_button);

        textTitle.setText(RemoteTitle);
        text_for_popup.setText(RemoteMessage);
        yes_button.setText(getResources().getString(R.string.oK));
        final Intent intent = new Intent(this, RateCustomerActivity.class);
        yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtras(bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
        if(this!=null && !isFinishing())
        {
            dialog.show();
        }
    }
}
