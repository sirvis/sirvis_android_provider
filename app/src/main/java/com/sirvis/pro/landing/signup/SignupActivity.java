package com.sirvis.pro.landing.signup;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.address.AddressSaveActivity;
import com.sirvis.pro.changepassword.OTPVerifyActivity;
import com.sirvis.pro.pojo.signup.CategoriesData;
import com.sirvis.pro.pojo.signup.CityData;
import com.sirvis.pro.pojo.signup.SignUpPojo;
import com.sirvis.pro.pojo.signup.SubCategory;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DatePickerCommon;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;
import com.sirvis.pro.utility.WebViewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.sirvis.pro.utility.VariableConstant.READ_PHONE_STATE_PERMISSION;


/**
 * Created by murashid on 07-Sep-17.
 * <h1>SignupActivity</h1>
 * SignupActivity activity for signup
 */

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, SignupPresenter.SignPresenterImple, View.OnFocusChangeListener, EasyPermissions.PermissionCallbacks, SignupTextWatcher.onSeekBarProgressListener, DatePickerCommon.DateSelected, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "SignupActivity";
    private TextInputLayout tilName, tilLastName, tilEmail, tilCity, tilPassword, tilDob;
    private EditText etFirstName, etLastName, etEmail, etCity, etPassword, etPhone;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvNoServiceLabel;
    private EditText etDob, etLocation;
    private EditText etReferalCode;
    private ImageView ivProfile;
    private ProgressDialog progressDialog;
    private NestedScrollView nsvSignup;
    private SessionManager sessionManager;

    private String imageUrl = "";
    private boolean isPicturetaken;
    private File mFileTemp;
    private Typeface fontRegular, fontMedium;

    private DatePickerCommon datePickerFragment;
    private UploadFileAmazonS3 amazonS3;
    private SignupPresenter signupPresenter;
    private ArrayList<CityData> cityDataArrayList;

    private ArrayList<CategoriesData> categoriesData;

    private String cityId = "", gender = "";
    private ArrayList<String> categoriseIds;
    private ArrayList<String> subCategoriesIds;
    private ArrayList<String> subCategoriesIdsRestore;
    private JSONObject documentJsonObject;
    private JSONObject documentTempJsonObject;
    private TextView tvTempCategory = null;
    private String sentingDate = "1980-01-01";
    private String lastSelectedCategoryId = "";
    private int lastSelectedCategoryIndex = 0;

    private double[] location;
    private String latitude = "", longitude = "", taggedAs = "others";

    private LinearLayout llServiceMain;
    private LayoutInflater inflater;

    private String city, state, pincode;

    private SeekBar seekBarButton;

    private CountryCodePicker ccp;

    private int oldProgress = 0;
    private int noOfFields = 5; //Maximum progress
    private boolean isMultipleCategories = true; // able to select multiple categories
    private String deviceID = "";
    RadioButton rbyes,rbNo;
    TextInputLayout tilvat;
    EditText etVat;
    int vat=0;
    private boolean isVatReselected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (EasyPermissions.hasPermissions(this, READ_PHONE_STATE_PERMISSION)) {
            if(deviceID.isEmpty())
            {
                deviceID = Utility.getDeviceId(SignupActivity.this);
            }

        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.phone_state_permission),
                    2000, READ_PHONE_STATE_PERMISSION);
        }
    }

    /**
     * <h>init</h>
     * Initlize the views
     *
     * @since v1.0
     */
    private void init() {
        rbyes = findViewById(R.id.rbYes);
        rbNo = findViewById(R.id.rbNo);
        rbyes.setOnCheckedChangeListener(this);
        rbNo.setOnCheckedChangeListener(this);
        tilvat = findViewById(R.id.tilvat);
        etVat = findViewById(R.id.etVat);

        signupPresenter = new SignupPresenter(this);
        datePickerFragment = new DatePickerCommon();
        datePickerFragment.setDatePickerType(2);
        datePickerFragment.setCallBack(this);
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        cityDataArrayList = new ArrayList<>();
        inflater = LayoutInflater.from(this);

        amazonS3 = UploadFileAmazonS3.getInstance(this);
        categoriseIds = new ArrayList<>();
        subCategoriesIds = new ArrayList<>();
        subCategoriesIdsRestore = new ArrayList<>();
        categoriesData = new ArrayList<>();

        documentJsonObject = new JSONObject();
        documentTempJsonObject = new JSONObject();

        fontRegular = Utility.getFontRegular(this);
        fontMedium = Utility.getFontMedium(this);
        Typeface fontLight = Utility.getFontLight(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.signupWithus));
        tvTitle.setTypeface(fontBold);
        tvTitle.setTextColor(ContextCompat.getColor(this, R.color.normalTextColor));

        TextView tvSubmit = findViewById(R.id.tvSubmit);
        TextView tvTermsOfService1 = findViewById(R.id.tvTermsOfService1);
        TextView tvTermsOfService2 = findViewById(R.id.tvTermsOfService2);
        TextView tvPhoneNumberLabel = findViewById(R.id.tvPhoneNumberLabel);
        TextView tvLocationLabel = findViewById(R.id.tvLocationLabel);
        SpannableString content = new SpannableString(getResources().getString(R.string.termsNcondtns2));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvTermsOfService2.setText(content);
        tvTermsOfService1.setTypeface(fontRegular);
        tvTermsOfService2.setTypeface(fontMedium);
        tvPhoneNumberLabel.setTypeface(fontMedium);
        tvLocationLabel.setTypeface(fontMedium);

        tvSubmit.setTypeface(fontBold);
        tvSubmit.setOnClickListener(this);

        tilName = findViewById(R.id.tilName);
        tilLastName = findViewById(R.id.tilLastName);
        tilEmail = findViewById(R.id.tilEmail);
        tilCity = findViewById(R.id.tilCity);
        tilPassword = findViewById(R.id.tilPassword);
        tilDob = findViewById(R.id.tilDob);

        TextView tvServiceLabel = findViewById(R.id.tvServiceLabel);
        tvNoServiceLabel = findViewById(R.id.tvNoServiceLabel);
        coordinatorLayout = findViewById(R.id.coordinatorLayout);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etCity = findViewById(R.id.etCity);
        etPhone = findViewById(R.id.etPhone);
        etPassword = findViewById(R.id.etPassword);
        etDob = findViewById(R.id.etDob);
        etLocation = findViewById(R.id.etLocation);
        etReferalCode = findViewById(R.id.etReferalCode);

        ivProfile = findViewById(R.id.ivProfile);

        LinearLayout llTerms = findViewById(R.id.llTerms);
        llServiceMain = findViewById(R.id.llServiceMain);

        nsvSignup = findViewById(R.id.nsvSignup);

        tvServiceLabel.setTypeface(fontMedium);
        etFirstName.setTypeface(fontMedium);
        etLastName.setTypeface(fontMedium);
        etEmail.setTypeface(fontMedium);
        etCity.setTypeface(fontMedium);
        etPhone.setTypeface(fontMedium);
        etPassword.setTypeface(fontMedium);
        etDob.setTypeface(fontMedium);
        etLocation.setTypeface(fontMedium);
        etReferalCode.setTypeface(fontMedium);

        ivProfile.setOnClickListener(this);

        etCity.setOnClickListener(this);
        etDob.setOnClickListener(this);
        etLocation.setOnClickListener(this);
        llTerms.setOnClickListener(this);

        etEmail.setOnFocusChangeListener(this);
        etPhone.setOnFocusChangeListener(this);
        etFirstName.setOnFocusChangeListener(this);
        etLastName.setOnFocusChangeListener(this);
        etCity.setOnFocusChangeListener(this);
        etPassword.setOnFocusChangeListener(this);
        etDob.setOnFocusChangeListener(this);
        etLocation.setOnFocusChangeListener(this);
        etReferalCode.setOnFocusChangeListener(this);
        etVat.setOnFocusChangeListener(this);

        seekBarButton = findViewById(R.id.seekBarButton);
        seekBarButton.setMax(noOfFields);

        SignupTextWatcher signupTextWatcher = new SignupTextWatcher(this, etFirstName, etLastName, etEmail, etCity,
                etPassword, etPhone, etDob, etLocation,etVat);

        etFirstName.addTextChangedListener(signupTextWatcher);
        etLastName.addTextChangedListener(signupTextWatcher);
        etCity.addTextChangedListener(signupTextWatcher);
        etPassword.addTextChangedListener(signupTextWatcher);
        etEmail.addTextChangedListener(signupTextWatcher);
        etPhone.addTextChangedListener(signupTextWatcher);
        etDob.addTextChangedListener(signupTextWatcher);
        etLocation.addTextChangedListener(signupTextWatcher);
        etVat.addTextChangedListener(signupTextWatcher);


        ccp = findViewById(R.id.ccp);


        location = Utility.getLocation(this);
        sessionManager.setCurrentLat(String.valueOf(location[0]));
        sessionManager.setCurrentLng(String.valueOf(location[1]));

        latitude = String.valueOf(location[0]);
        longitude = String.valueOf(location[1]);

        TextView tvGenderLabel = findViewById(R.id.tvGenderLabel);
        tvGenderLabel.setTypeface(fontMedium);
        RadioButton rbMale = findViewById(R.id.rbMale);
        rbMale.setTypeface(fontLight);
        rbMale.setOnCheckedChangeListener(this);
        RadioButton rbFemale = findViewById(R.id.rbFemale);
        rbFemale.setTypeface(fontLight);
        rbFemale.setOnCheckedChangeListener(this);
        RadioButton rbOthers = findViewById(R.id.rbOthers);
        rbOthers.setTypeface(fontLight);
        rbOthers.setOnCheckedChangeListener(this);
        //signupPresenter.getServiceCategories(sessionManager.getCurrentLat(),sessionManager.getCurrentLng());

        AppController.getInstance().getMixpanelHelper().commonTrackBeforeLogin(MixpanelEvents.SignupOpen.value);

        try {
           // ccp.setCountryForNameCode(Utility.getCurrentCountryCode(this));
            ccp.setCountryForNameCode("27");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("RestrictedApi")
    private void startAddressPickup() {
        Intent intent = new Intent(this, AddressSaveActivity.class);
        intent.putExtra("isSignup", true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_LOCATION, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_LOCATION);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * Close the activity when user click back button
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                    doSignup();
                break;

            case R.id.ivProfile:
                doSelectImage();
                break;

            case R.id.etCity:
                if (cityDataArrayList.size() > 0) {
                    selectCity();
                } else {
                    progressDialog.setMessage(getString(R.string.getingCity));
                    signupPresenter.getCityList();
                }
                break;

            case R.id.etDob:
                if (!datePickerFragment.isResumed()) {
                    datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                }
                break;

            case R.id.etLocation:
                startAddressPickup();
                break;

            case R.id.llTerms:
                Intent webIntent = new Intent(this, WebViewActivity.class);
                webIntent.putExtra("URL", BuildConfig.TERMS_OF_SERVICE);
                webIntent.putExtra("title", getString(R.string.termsOfService));
                startActivity(webIntent);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;

        }
    }

    /*public  boolean validateVatNum(String vatNumber)
    {
        if(vatNumber.length()==10 && vatNumber.substring(0,1).equals("4"))
            return true;
        else{
            Toast.makeText(this, "Invalid vat Number", Toast.LENGTH_SHORT).show();
            return false;
        }
    }*/

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        if (!hasFocus) {
            switch (v.getId()) {
                case R.id.etEmail:
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("email", etEmail.getText().toString());
                        progressDialog.setMessage(getString(R.string.validatingEmail));
                        signupPresenter.verifyEmail(jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.etPhone:
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("mobile", etPhone.getText().toString());
                        jsonObject.put("countryCode", ccp.getSelectedCountryCodeWithPlus());
                        progressDialog.setMessage(getString(R.string.validatingPhone));
                        signupPresenter.
                                verifyPhone(jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case R.id.etFirstName:
                    signupPresenter.verifyFirstName(etFirstName.getText().toString());
                    break;

                case R.id.etLastName:
                    signupPresenter.verifyLastName(etLastName.getText().toString());
                    break;


                case R.id.etPassword:
                    signupPresenter.verifyPassword(etPassword.getText().toString());
                    break;
            }
        } else {
            switch (v.getId()) {
                case R.id.etDob:
                    if (!datePickerFragment.isResumed()) {
                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                    }
                    break;

                case R.id.etCity:
                    if (cityDataArrayList.size() > 0) {
                        selectCity();
                    } else {
                        progressDialog.setMessage(getString(R.string.getingCity));
                        signupPresenter.getCityList();
                    }

                    break;

                case R.id.etLocation:
                    //startAddressPickup();
                    break;
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.rbMale:
                    gender = "1";
                    break;

                case R.id.rbFemale:
                    gender = "2";
                    break;

                case R.id.rbOthers:
                    gender = "3";
                    break;

                case R.id.rbYes:
                    vat = 1;
                    tilvat.setVisibility(View.VISIBLE);
                    break;

                case R.id.rbNo:
                    vat = 0;
                    onSeekBarProgress(oldProgress);
                    tilvat.setVisibility(View.GONE);
                    break;

            }
            if(buttonView.getId() ==R.id.rbMale || buttonView.getId()== R.id.rbFemale || buttonView.getId()==R.id.rbOthers) {
                onSeekBarProgress(oldProgress);

                if (etLocation.getText().toString().equals("")) {
                    startAddressPickup();
                }
            }
        }
    }


    private void doSelectImage() {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                    1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
        }
    }


    /**
     * Start the ProgressDialog when calling the api
     */
    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    /**
     * Stop the ProgressDialog when got the response
     */
    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }


    /**
     * @param msg Error Msg from api response
     */
    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * <h1>onFailure</h1>
     * During server error or network problem
     */
    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    /**
     * <h1>onSuccessUpdateProfile</h1>
     * Close the Activity when user click the ok button of Custom Dialog
     *
     * @param signUpPojo signupresponse pojo
     */
    @Override
    public void onSuccessSignUp(SignUpPojo signUpPojo) {
       Utility.printLog(TAG, "onSuccessSignUp: "+signUpPojo.toString());
        //Utility.customAlertDialogCloseActivity(this,getString(R.string.message),msg,getString(R.string.oK));
        Intent intent = new Intent(this, OTPVerifyActivity.class);
        intent.putExtra("phone", etPhone.getText().toString());
        intent.putExtra("countryCode", ccp.getSelectedCountryCodeWithPlus());
        intent.putExtra("userId", signUpPojo.getData().getProviderId());
        intent.putExtra("expireOtp", signUpPojo.getData().getExpireOtp());
        intent.putExtra("otpOption", 1);
        finish();
        startActivity(intent);
        int age = 18;
        try {
            int year = Integer.parseInt(sentingDate.split("-")[0]);
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            age = currentYear - year;
        } catch (Exception e) {
            e.printStackTrace();
        }
        int year = Integer.parseInt(sentingDate.split("-")[0]);
        int currentYearyear = Calendar.getInstance().get(Calendar.YEAR);


        AppController.getInstance().getMixpanelHelper().sigunUpComplete(etFirstName.getText().toString() + " " + etLastName.getText().toString(),
                ccp.getSelectedCountryCodeWithPlus() + etPhone.getText().toString(), "" + age);
    }

    /**
     * <h1>onSuccessCity</h1>
     *
     * @param cityDataArrayList list of city from api response
     */
    @Override
    public void onSuccessCity(ArrayList<CityData> cityDataArrayList) {
        this.cityDataArrayList = cityDataArrayList;
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        selectCity();

    }

    /**
     * method for start the SignupSelectionActivity for select the city list
     */
    private void selectCity() {
        Intent intentCity = new Intent(this, SignupSelectionActivity.class);
        intentCity.putExtra("citylist", cityDataArrayList);
        intentCity.putExtra("type", "city");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            startActivityForResult(intentCity, VariableConstant.REQUEST_CODE_CITY, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivityForResult(intentCity, VariableConstant.REQUEST_CODE_CITY);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        }
    }


    /**
     * <h1>onSuccessServiceCategories</h1>
     *
     * @param categoriesData list of genre for specific city
     */
    @Override
    public void onSuccessServiceCategories(ArrayList<CategoriesData> categoriesData,String cityId) {
        this.categoriesData.clear();
        this.categoriesData.addAll(categoriesData);
        this.cityId = cityId;

        if (this.categoriesData.size() == 0) {
            showNoServiceWarning();
        } else {
            tvNoServiceLabel.setVisibility(View.GONE);
            createServiceView(categoriesData);
        }
    }

    private void createServiceView(final ArrayList<CategoriesData> categoriesData) {
        llServiceMain.removeAllViews();

       /* Collections.sort(categoriesData, new Comparator<CategoriesData>() {
            @Override
            public int compare(CategoriesData o1, CategoriesData o2) {
                return o1.getCatName().length()-(o2.getCatName().length());
            }
        });*/

        for (int i = 0; i < categoriesData.size(); i++) {
            LinearLayout llServiceSub = (LinearLayout) inflater.inflate(R.layout.single_row_service_categories_horizontal, null);
            View categoryView = inflater.inflate(R.layout.single_row_category, null);
            final TextView tvCategory = categoryView.findViewById(R.id.tvCategory);
            tvCategory.setText(categoriesData.get(i).getCatName());
            llServiceSub.addView(categoryView);

            final int finalI = i;
            tvCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    handeCategorySelection(tvCategory, finalI, categoriesData.get(finalI).getCatId());
                }
            });

            i++;
            if (i < categoriesData.size() && categoriesData.get(i - 1).getCatName().length() + categoriesData.get(i).getCatName().length() < 26) {
                View categoryView2 = inflater.inflate(R.layout.single_row_category, null);
                final TextView tvCategory2 = categoryView2.findViewById(R.id.tvCategory);
                tvCategory2.setText(categoriesData.get(i).getCatName());
                llServiceSub.addView(categoryView2);

                final int finalI1 = i;
                tvCategory2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        handeCategorySelection(tvCategory2, finalI1, categoriesData.get(finalI1).getCatId());
                    }
                });

                i++;
                if (i < categoriesData.size() && categoriesData.get(i - 2).getCatName().length() + categoriesData.get(i - 1).getCatName().length() + categoriesData.get(i).getCatName().length() < 20) {
                    View categoryView3 = inflater.inflate(R.layout.single_row_category, null);
                    final TextView tvCategory3 = categoryView3.findViewById(R.id.tvCategory);
                    tvCategory3.setText(categoriesData.get(i).getCatName());
                    llServiceSub.addView(categoryView3);
                    final int finalI2 = i;
                    tvCategory3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            handeCategorySelection(tvCategory3, finalI2, categoriesData.get(finalI2).getCatId());
                        }
                    });
                } else {
                    i--;
                }
            } else {
                i--;
            }
            llServiceMain.addView(llServiceSub);
        }
    }

    private void handeCategorySelection(TextView tvCategory, int categoryIndex, String categoryId) {
        if (isMultipleCategories) {
            if (categoriseIds.contains(categoryId)) {
                tvCategory.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_white_with_border));
                tvCategory.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
                categoriseIds.remove(categoryId);
                if (documentJsonObject.has(categoryId)) {
                    documentJsonObject.remove(categoryId);
                }
                if (VariableConstant.getHashMapIsMandatoryUploaded().containsKey(categoriesData.get(categoryIndex).getCatName())) {
                    VariableConstant.getHashMapIsMandatoryUploaded().remove(categoriesData.get(categoryIndex).getCatName());
                }
                if (VariableConstant.getHashMapIsSubCategorySelected().containsKey(categoriesData.get(categoryIndex).getCatName())) {
                    VariableConstant.getHashMapIsSubCategorySelected().remove(categoriesData.get(categoryIndex).getCatName());
                }

                for (SubCategory subCategory : categoriesData.get(categoryIndex).getSubCategory()) {
                    if (subCategoriesIds.contains(subCategory.get_id())) {
                        subCategoriesIds.remove(subCategory.get_id());
                    }
                }
            } else {
                tvCategory.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_button));
                tvCategory.setTextColor(ContextCompat.getColor(this, R.color.white));

                categoriseIds.add(categoryId);
                startSubCategorySelectionActivity(categoryId, categoryIndex);
            }
        } else {
            VariableConstant.getHashMapIsMandatoryUploaded().clear();
            VariableConstant.getHashMapIsSubCategorySelected().clear();

            tvCategory.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_button));
            tvCategory.setTextColor(ContextCompat.getColor(this, R.color.white));
            if (tvTempCategory != null && tvTempCategory != tvCategory) {
                tvTempCategory.setBackground(ContextCompat.getDrawable(this, R.drawable.rectangle_white_with_border));
                tvTempCategory.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            }
            tvTempCategory = tvCategory;

            subCategoriesIds.clear();
            categoriseIds.clear();
            categoriseIds.add(categoryId);
            startSubCategorySelectionActivity(categoryId, categoryIndex);
        }

        onSeekBarProgress(oldProgress);
    }

    public void startSubCategorySelectionActivity(String id, int categoryIndex) {
        if (categoriesData.get(categoryIndex).getSubCategory().size() > 0) {
            lastSelectedCategoryId = id;
            lastSelectedCategoryIndex = categoryIndex;
            Intent subCateogyIntent = new Intent(this, SignupSelectionActivity.class);
            subCateogyIntent.putExtra("type", "category");
            subCateogyIntent.putExtra("category", categoriesData.get(categoryIndex));
            subCateogyIntent.putExtra("subCategoriesIdsRestore", subCategoriesIdsRestore);
            subCateogyIntent.putExtra("documentSize", categoriesData.get(categoryIndex).getDocument().size());

            startActivityForResult(subCateogyIntent, VariableConstant.REQUEST_CODE_SUB_CATEGORY);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
        } else {
            startDocumentUploadActivity(id, categoryIndex);
        }
    }


    public void startDocumentUploadActivity(String id, int position) {
        if (categoriesData.get(position).getDocument().size() > 0) {
            Intent documentIntent = new Intent(this, SignupDocumentActivity.class);
            documentIntent.putExtra("category", categoriesData.get(position));

            try {
                if (documentTempJsonObject.has(id) && documentTempJsonObject.get(id) instanceof JSONArray) {
                    JSONArray documentArray = documentTempJsonObject.getJSONArray(id);
                    documentIntent.putExtra("documentArray", documentArray.toString());
                    documentIntent.putExtra("isAlreadySaved", true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            startActivityForResult(documentIntent, VariableConstant.REQUEST_CODE_CATEGORY_DOCUMENT);
            overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);

        }
    }

    @Override
    public void onSuccessImageUpload(String imgUrl) {
        progressDialog.dismiss();
        imageUrl = imgUrl;
        onSeekBarProgress(oldProgress);
    }

    @Override
    public void onFailureImageUpload(){
        progressDialog.dismiss();
        Toast.makeText(this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureServiceCategories() {
        showNoServiceWarning();
    }

    private void showNoServiceWarning() {
        this.categoriesData.clear();
        llServiceMain.removeAllViews();
        tvNoServiceLabel.setVisibility(View.VISIBLE);
        tvNoServiceLabel.setText(getString(R.string.noServiceUnderThisCity));
        tvNoServiceLabel.setVisibility(View.INVISIBLE);
        final Snackbar snackbar = Snackbar.make(coordinatorLayout, getString(R.string.noServiceUnderTheCity), Snackbar.LENGTH_LONG);
        /*snackbar.setAction(getString(R.string.close), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

    @Override
    public void onSuccessPhoneEmailValidation() {
        tilEmail.setErrorEnabled(false);
    }

    /**
     * @param msg
     */
    @Override
    public void onFailurePhoneValidation(String msg) {
        etPhone.setText("");
        etPhone.requestFocus();
        nsvSignup.scrollTo(0, 0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureEmailValidation(String msg) {
        etEmail.setText("");
        etEmail.requestFocus();
        nsvSignup.scrollTo(0, 0);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureReferalValidation(String msg) {
        etReferalCode.setText("");
        etReferalCode.requestFocus();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProfilePicError() {
        nsvSignup.scrollTo(0, 0);
        doSelectImage();
        Toast.makeText(this, getString(R.string.plsUploadImage), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserNameError() {
        nsvSignup.scrollTo(0, 0);
        etFirstName.requestFocus();
        resetTil(tilName, getString(R.string.enterName));
    }

    @Override
    public void onUserLastNameError() {
        nsvSignup.scrollTo(0, 0);
        etLastName.requestFocus();
        resetTil(tilLastName, getString(R.string.enterlastname));
    }

    @Override
    public void onInValidEmailError() {
        nsvSignup.scrollTo(0, 0);
        etEmail.requestFocus();
        resetTil(tilEmail, getString(R.string.enterValidEmail));
    }

    @Override
    public void onEmailError() {
        nsvSignup.scrollTo(0, 0);
        etEmail.requestFocus();
        resetTil(tilEmail, getString(R.string.enterEmail));
    }

    @Override
    public void onCitySelectionError() {
        nsvSignup.scrollTo(0, 0);
        if (cityDataArrayList.size() > 0) {
            selectCity();
        } else {
            signupPresenter.getCityList();
        }

        resetTil(tilCity, getString(R.string.selectCity));
    }

    @Override
    public void onCategorySelectionError() {
        resetTil(null, "");
        Toast.makeText(this, getString(R.string.plsSelectCategory), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSubCategorySelectionError(String category) {
        resetTil(null, "");
        Toast.makeText(this, getString(R.string.plsAtleatOneSubCategory) + " " + getString(R.string.in) + " " + category,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCategoryDocumentUploadError(String category) {
        resetTil(null, getString(R.string.selectGenre));
        Toast.makeText(this, getString(R.string.plsUploadMandatoryDocument) + " " + getString(R.string.in) + " " + category,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPasswordError() {
        nsvSignup.scrollTo(0, 0);
        etPassword.setText("");
        etPassword.requestFocus();
        resetTil(tilPassword, getString(R.string.enterValidPassword));
    }

    @Override
    public void onPhoneNumberError() {
        nsvSignup.scrollTo(0, 0);
        etPhone.requestFocus();
        Toast.makeText(this, getString(R.string.enterPhone), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDobError() {
        etDob.requestFocus();
        resetTil(tilDob, getString(R.string.selectDob));
    }

    @Override
    public void onGenderError() {
        Toast.makeText(this, getString(R.string.plsSelectGender), Toast.LENGTH_SHORT).show();
        resetTil(null, "");
    }

    @Override
    public void onLocationError() {
        etLocation.requestFocus();
        Toast.makeText(this, getString(R.string.plsSelectLocation), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onVatNumberError(){
        tilvat.setError(getString(R.string.invalidVatNumber));
    }

    @Override
    public void onVatSelectError(){
        Toast.makeText(this, getString(R.string.plsSelectVat), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onSuccesLocalValidation() {
        resetTil(null, "");
    }

    @Override
    public void onUserNameInCorrect() {
        setTilError(tilName, getString(R.string.enterName));
    }

    @Override
    public void onUserNameCorrect() {
        tilName.setErrorEnabled(false);
    }

    @Override
    public void onUserLastNameInCorrect() {
        setTilError(tilLastName, getString(R.string.enterlastname));
    }

    @Override
    public void onUserLastNameCorrect() {
        tilLastName.setErrorEnabled(false);
    }

    @Override
    public void onEmailInCorrect() {
        setTilError(tilEmail, getString(R.string.enterValidEmail));
    }

    @Override
    public void onEmailCorrect() {
        tilEmail.setErrorEnabled(false);
    }

    @Override
    public void onPhoneNumberInCorrect() {
        Toast.makeText(this, getString(R.string.enterPhone), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPhoneNumberCorrect() {
        //tilName.setErrorEnabled(false);
    }


    @Override
    public void onPasswordInCorrect() {
        etPassword.setText("");
        setTilError(tilPassword, getString(R.string.enterValidPassword));
    }


    @Override
    public void onPasswordCorrect() {
        tilPassword.setErrorEnabled(false);
    }

    /**
     * set the error for empty field
     *
     * @param textInputLayout empty field
     * @param err             error msg
     */
    private void resetTil(TextInputLayout textInputLayout, String err) {
        tilName.setErrorEnabled(false);
        tilLastName.setErrorEnabled(false);
        tilEmail.setErrorEnabled(false);
        tilCity.setErrorEnabled(false);
        tilPassword.setErrorEnabled(false);
        tilDob.setErrorEnabled(false);

        try {
            if (textInputLayout != null) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(err);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTilError(TextInputLayout textInputLayout, String err) {
        try {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(err);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for creating json object and sent to presenter for signup
     */
    private void doSignup() {
        try {

            JSONObject singupJsonObject = new JSONObject();
            singupJsonObject.put("firstName", etFirstName.getText().toString());
            singupJsonObject.put("lastName", etLastName.getText().toString());
            singupJsonObject.put("email", etEmail.getText().toString());
            singupJsonObject.put("password", etPassword.getText().toString());
            singupJsonObject.put("countryCode", ccp.getSelectedCountryCodeWithPlus());
            singupJsonObject.put("mobile", etPhone.getText().toString());
            singupJsonObject.put("dob", sentingDate);
            singupJsonObject.put("catlist","0");
            singupJsonObject.put("subCatlist","0");
            singupJsonObject.put("cityId", "0");
            singupJsonObject.put("latitude", latitude);
            singupJsonObject.put("longitude", longitude);
            singupJsonObject.put("profilePic", "https://trello-attachments.s3.amazonaws.com/5f5f69209e891d418be8f051/1024x1024/4e7daf171cf60335c8612f75e61ce973/image.png");
            singupJsonObject.put("deviceId", deviceID);
            singupJsonObject.put("deviceType", VariableConstant.DEVICE_TYPE);
            singupJsonObject.put("deviceMake", VariableConstant.DEVICE_MAKER);
            singupJsonObject.put("deviceModel", VariableConstant.DEVICE_MODEL);
            singupJsonObject.put("deviceOsVersion", VariableConstant.OS_VERSION);
            singupJsonObject.put("appVersion", VariableConstant.APP_VERSION);
            singupJsonObject.put("pushToken", sessionManager.getPushToken());
            singupJsonObject.put("deviceTime", Utility.getCurrentTime());
            singupJsonObject.put("addLine1", "Johannesburg South Africa");
            singupJsonObject.put("city", "Johannesburg");
            singupJsonObject.put("state", "Johannesburg");
            singupJsonObject.put("pincode", "2000");
            singupJsonObject.put("taggedAs", "0");
            singupJsonObject.put("document", documentJsonObject);
            singupJsonObject.put("gender", "3");
            singupJsonObject.put("referralCode", etReferalCode.getText().toString());
            singupJsonObject.put("vatApplicable",vat);
            if(vat==1)
            singupJsonObject.put("vatNumber",etVat.getText().toString());

            Log.i( "vatApplicable123",vat+"  "+etVat.getText().toString());

            Log.d(TAG, "doSignup: " + singupJsonObject);

            JSONObject referalJsonObject = new JSONObject();
            referalJsonObject.put("code", etReferalCode.getText().toString());
            referalJsonObject.put("userType", VariableConstant.USER_TYPE);
            referalJsonObject.put("lat",  "26.2041");
            referalJsonObject.put("long",  "28.0473");

            signupPresenter.verifyReferalCodeAndSignup(singupJsonObject, referalJsonObject);

            progressDialog.setMessage(getString(R.string.signingup));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        switch (requestCode) {
            case 2000:
                if(deviceID.isEmpty())
                {
                    deviceID = Utility.getDeviceId(SignupActivity.this);
                }
                break;
            case 1000:
                if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
                    selectImage();
                }
                break;
        }

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private void selectImage() {

        if (mFileTemp == null) {
            String filename = VariableConstant.PROFILE_FILE_NAME + System.currentTimeMillis() + ".png";
            MyImageHandler myImageHandler = MyImageHandler.getInstance();
            mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.CROP_PIC_DIR, true), filename);
        }

        final BottomSheetDialog mDialog = new BottomSheetDialog(this);
        View view = inflater.inflate(R.layout.bottom_sheet_picture_selection, null);
        mDialog.setContentView(view);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvImageSelectionHeader = view.findViewById(R.id.tvImageSelectionHeader);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvGallery = view.findViewById(R.id.tvGallery);

        tvImageSelectionHeader.setTypeface(fontRegular);
        tvCamera.setTypeface(fontMedium);
        tvGallery.setTypeface(fontMedium);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {

                case VariableConstant.REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        isPicturetaken = true;
                        startCropImage();
                    } catch (Exception e) {

                        e.printStackTrace();
                        Toast.makeText(SignupActivity.this,"There was a problem while fetching the image ,please try another one.",Toast.LENGTH_LONG).show();
                    }

                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    isPicturetaken = true;
                    startCropImage();
                    break;

                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    ivProfile.setImageBitmap(Utility.getCircleCroppedBitmap(BitmapFactory.decodeFile(path)));
                    progressDialog.setMessage(getString(R.string.uploading));
                    progressDialog.show();
                    signupPresenter.amazonUpload(mFileTemp);
                    break;

                case VariableConstant.REQUEST_CODE_CITY:
                    for (String categoryId : categoriseIds) {
                        if (documentJsonObject.has(categoryId)) {
                            documentJsonObject.remove(categoryId);
                        }
                    }
                    subCategoriesIds.clear();
                    categoriseIds.clear();
                    VariableConstant.getHashMapIsMandatoryUploaded().clear();
                    VariableConstant.getHashMapIsSubCategorySelected().clear();

                    //cityId = data.getStringExtra("SELECTED_ID");
                    etCity.setText(data.getStringExtra("SELECTED_NAME"));


                    Log.d(TAG, "onActivityResult: " + data.getStringExtra("SELECTED_NAME"));
                    progressDialog.setMessage(getString(R.string.gettingCategories));
                    //signupPresenter.getServiceCategories(sessionManager.getCurrentLat(),sessionManager.getCurrentLng());

                    if (getCurrentFocus() != null)
                        getCurrentFocus().clearFocus();
                    etPassword.requestFocus();
                    Utility.showKeyBoard(this, etPassword);
                    break;

                case VariableConstant.REQUEST_CODE_CATEGORY_DOCUMENT:
                    JSONArray documentArray = new JSONArray(data.getStringExtra("documentArray"));
                    String catergoryId = data.getStringExtra("catergoryId");
                    Log.d(TAG, "onActivityResult: " + documentArray);
                    documentJsonObject.put(catergoryId, documentArray);
                    documentTempJsonObject.put(catergoryId, documentArray);
                    break;


                case VariableConstant.REQUEST_CODE_SUB_CATEGORY:
                    ArrayList<String> subCategoriesIdsTemp = (ArrayList<String>) data.getSerializableExtra("SELECTED_ID");
                    for (String subId : subCategoriesIdsTemp) {
                        if (!subCategoriesIds.contains(subId)) {
                            subCategoriesIds.add(subId);
                        }
                    }
                    subCategoriesIdsRestore.addAll(subCategoriesIds);
                    startDocumentUploadActivity(lastSelectedCategoryId, lastSelectedCategoryIndex);
                    break;


                case VariableConstant.REQUEST_CODE_LOCATION:
                    etLocation.setText(data.getStringExtra("address"));
                    pincode = data.getStringExtra("pincode");
                    city = data.getStringExtra("city");
                    state = data.getStringExtra("state");

                    taggedAs = data.getStringExtra("taggedAs");
                    latitude = data.getStringExtra("latitude");
                    longitude = data.getStringExtra("longitude");
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for starting CropImage Activity for crop the selected image
     */
    private void startCropImage() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        sentingDate = sendingFormat;
        etDob.setText(displayFormat);

        if (etLocation.getText().toString().equals("")) {
            etLocation.requestFocus();
        }
        Log.d(TAG, "onDateSelected: " + sendingFormat + "  " + displayFormat);

    }


    @Override
    public void onSeekBarProgress(int progress) {

        oldProgress = progress;

        if (isPicturetaken) {
            progress += 1;
        }

        if (categoriseIds.size() > 0) {
            progress += 1;
        }

        if (!gender.equals("")) {
            progress += 1;
        }

        if(vat==0)
        {
            progress += 1;
        }
        if(isVatReselected)
        {
            progress -= 1;
        }

        if (progress >= 100) {
            signupPresenter.setSeekBarProgress(seekBarButton, 100);
        } else {
            signupPresenter.setSeekBarProgress(seekBarButton, (int) progress);
        }
    }
}
