package com.sirvis.pro.landing.splash;

import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 24-Apr-18.
 */

public class SplashPresenter {

    private View view;

    SplashPresenter(View view)
    {
        this.view = view;
    }

    void getLanguage()
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.LANGUAGE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {

                        Log.d("Splash", "onSuccess: "+ statusCode +"\n "+ result);
                        try {
                            if (result != null && statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                view.onSuccess(result);
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                    }
                });
    }

    interface View
    {
        void onSuccess(String result);
    }
}
