package com.sirvis.pro.landing.signup;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by murashid on 22-Jan-18.
 */

public class SignupTextWatcher implements TextWatcher {

    private EditText etFirstName,etLastName,etEmail,etCity,etPassword,etPhone,etDob,etLocation,etVat;
    private onSeekBarProgressListener listener;

    public SignupTextWatcher(onSeekBarProgressListener listener, EditText etFirstName, EditText etLastName, EditText etEmail, EditText etCity, EditText etPassword, EditText etPhone, EditText etDob, EditText etLocation,EditText etVat) {
        this.listener = listener;
        this.etFirstName = etFirstName;
        this.etLastName = etLastName;
        this.etEmail = etEmail;
        this.etCity = etCity;
        this.etPassword = etPassword;
        this.etPhone = etPhone;
        this.etDob = etDob;
        this.etLocation = etLocation;
        this.etVat = etVat;

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        int seekProgres = 0;

        if(etFirstName.getText().length() > 0)
        {
            seekProgres += 1;
        }
        if(etLastName.getText().length() > 0)
        {
            seekProgres += 1;
        }
        if(Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches())
        {
            seekProgres += 1;
        }
        if(etCity.getText().length() > 0)
        {
            seekProgres += 1;
        }
        if(etPassword.getText().length() > 0 && isValidPassword(etPassword.getText().toString()))
        {
            seekProgres += 1;
        }
        if(etPhone.getText().length() > 0)
        {
            seekProgres += 1;
        }
        if(etDob.getText().length() > 0)
        {
            seekProgres += 1;
        }
        if(etLocation.getText().length() > 0)
        {
            seekProgres += 1;
        }
        if(etVat.getText().length() > 0)
        {
            seekProgres += 1;
        }
        listener.onSeekBarProgress(seekProgres);
    }

    private boolean isValidPassword(String pass) {
        String regex = "(.)*(\\d)(.)*";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);

        if(pass.length() < 7)
        {
            return false;
        }
        else if(pass.equals(pass.toLowerCase()))
        {
            return false;
        }
        else if(pass.equals(pass.toUpperCase()))
        {
            return false;
        }
        else if(!matcher.matches())
        {
            return false;
        }
        return true;
    }

    interface onSeekBarProgressListener
    {
        void onSeekBarProgress(int progress);
    }
}
