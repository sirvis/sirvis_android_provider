package com.sirvis.pro.landing.signup;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.SignupSelectionListAdapter;
import com.sirvis.pro.adapters.SignupSelectionListCheckBoxAdapter;
import com.sirvis.pro.pojo.signup.CategoriesData;
import com.sirvis.pro.pojo.signup.CityData;
import com.sirvis.pro.pojo.signup.SubCategory;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>SignupSelectionActivity</h1>
 * SignupSelectionActivity for selecting the city list
 */

public class SignupSelectionActivity extends AppCompatActivity implements SignupSelectionListener{

    private static final String TAG = "SignupSelection";
    private String type = "",categoryName ="";
    private boolean isAleardySaved = false;
    private ArrayList<String> subCategoriesIds;
    private ArrayList<String> subCategoriesIdsTemp;

    private boolean isCheckBoxUi = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_selection);

        init();
    }

    /**
     * init the views
     */
    void init()
    {
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);

        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);
        tvTitle.setTextColor(ContextCompat.getColor(this,R.color.normalTextColor));

        TextView tvSave = findViewById(R.id.tvSave);
        tvSave.setTypeface(fontBold);

        subCategoriesIds = new ArrayList<>();
        subCategoriesIdsTemp = new ArrayList<>();

        Intent intent = getIntent();
        type = intent.getStringExtra("type");

        RecyclerView rvSignupSelection = findViewById(R.id.rvSignupSelection);
        rvSignupSelection.setLayoutManager(new LinearLayoutManager(this));

        if(type.equals("city"))
        {
            tvTitle.setText(getString(R.string.selectCity));
            ArrayList<CityData> cityDatas = (ArrayList<CityData>) intent.getSerializableExtra("citylist");

            if(isCheckBoxUi)
            {
                SignupSelectionListCheckBoxAdapter  signupSelectionListAdapter = new SignupSelectionListCheckBoxAdapter(this,cityDatas,null,type, this);
                rvSignupSelection.setAdapter(signupSelectionListAdapter);
            }
            else
            {
                SignupSelectionListAdapter  signupSelectionListAdapter = new SignupSelectionListAdapter(this,cityDatas,null,type, this);
                rvSignupSelection.setAdapter(signupSelectionListAdapter);
            }

        }
        else
        {
            tvSave.setVisibility(View.VISIBLE);
            if(intent.getIntExtra("documentSize",0) > 0)
            {
                tvSave.setText(getText(R.string.next));
            }
            CategoriesData categoriesData = (CategoriesData) intent.getSerializableExtra("category");
            categoryName = categoriesData.getCatName();
            tvTitle.setText(categoriesData.getCatName());
            ArrayList<SubCategory> subCategories = categoriesData.getSubCategory();
            ArrayList<String> subCategoriesIdsAll = (ArrayList<String>) intent.getSerializableExtra("subCategoriesIdsRestore");

            for (SubCategory subCategory : subCategories)
            {
                if(subCategoriesIdsAll.contains(subCategory.get_id()))
                {
                    subCategoriesIds.add(subCategory.get_id());
                    subCategoriesIdsTemp.add(subCategory.get_id());
                    subCategory.setAlreadySelected(true);
                    isAleardySaved = true;
                }
            }
            if(isCheckBoxUi)
            {
                SignupSelectionListCheckBoxAdapter signupSelectionListAdapter = new SignupSelectionListCheckBoxAdapter(this,null, subCategories,type,this);
                rvSignupSelection.setAdapter(signupSelectionListAdapter);
            }
            else
            {
                SignupSelectionListAdapter signupSelectionListAdapter = new SignupSelectionListAdapter(this,null, subCategories,type,this);
                rvSignupSelection.setAdapter(signupSelectionListAdapter);
            }

        }


        TextView tvDone = findViewById(R.id.tvDone);
        tvDone.setText(getString(R.string.done));
        tvDone.setTypeface(fontBold);
        tvDone.setVisibility(View.GONE);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(type.equals("category"))
                {
                    if(subCategoriesIds.size() > 0)
                    {
                        VariableConstant.getHashMapIsSubCategorySelected().put(categoryName,true);
                        Intent intent=new Intent(SignupSelectionActivity.this,SignupActivity.class);
                        intent.putExtra("SELECTED_ID", subCategoriesIds);
                        setResult(RESULT_OK,intent);
                        closeActivity(true);
                    }
                    else
                    {
                        Toast.makeText(SignupSelectionActivity.this,getString(R.string.plsAtleatOneSubCategory),Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home)
        {
            closeActivity(false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity(false);
    }

    private void closeActivity(boolean isFromSave)
    {
        if(!isFromSave && type.equals("category"))
        {
            if(!isAleardySaved)
            {
                VariableConstant.getHashMapIsSubCategorySelected().put(categoryName,false);
            }
            else
            {
                Intent intent=new Intent(SignupSelectionActivity.this,SignupActivity.class);
                intent.putExtra("SELECTED_ID", subCategoriesIdsTemp);
                setResult(RESULT_OK,intent);
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
        }
    }


    @Override
    public void onCitySelecting(String id, String name) {
        Intent intent=new Intent(SignupSelectionActivity.this,SignupActivity.class);
        intent.putExtra("SELECTED_ID", id);
        intent.putExtra("SELECTED_NAME", name);
        setResult(RESULT_OK,intent);
        closeActivity(true);

    }

    @Override
    public void onSubCategorySelecting(String id, String name) {
        if(subCategoriesIds.contains(id))
        {
            subCategoriesIds.remove(id);
        }
        else
        {
            subCategoriesIds.add(id);
        }
    }
}
