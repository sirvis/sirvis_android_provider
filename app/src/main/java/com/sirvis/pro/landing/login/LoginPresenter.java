package com.sirvis.pro.landing.login;

import android.animation.ObjectAnimator;
import android.view.animation.DecelerateInterpolator;
import android.widget.SeekBar;

import com.sirvis.pro.pojo.signup.LoginData;

import org.json.JSONObject;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>LoginPresenter</h1>
 * LoginPresenter presenter for LoginActivity
 * @see LoginActivity
 */

class LoginPresenter implements LoginModel.LoginModelImple
{
    private LoginModel loginModel;
    private LoginPresenterImple loginPresenterImple;

    LoginPresenter(LoginPresenterImple loginPresenterImple)
    {
        loginModel = new LoginModel(this);
        this.loginPresenterImple = loginPresenterImple;
    }

    /**
     * <h1>doLogin</h1>
     * method for passing values from activity for model
     * @param userName email or phonenumber
     * @param password password
     * @param jsonObject required fiels
     */
    void doLogin(String userName, String password, JSONObject jsonObject) {
        loginPresenterImple.startProgressBar();
        loginModel.login(userName,password,jsonObject);
    }

    void setSeekBarProgress(SeekBar seekBar, int progress)
    {
        ObjectAnimator animation = ObjectAnimator.ofInt(seekBar, "progress", progress);
        animation.setDuration(500); // 0.5 second
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    @Override
    public void onUsernameError() {
        loginPresenterImple.onUsernameError();
        loginPresenterImple.stopProgressBar();
    }

    @Override
    public void onPasswordError() {
        loginPresenterImple.onPasswordError();
        loginPresenterImple.stopProgressBar();
    }


    @Override
    public void onFailure(String failureMsg) {
        loginPresenterImple.stopProgressBar();
        loginPresenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        loginPresenterImple.stopProgressBar();
        loginPresenterImple.onFailure();
    }

    @Override
    public void onSuccess(LoginData loginData) {
        loginPresenterImple.stopProgressBar();
        loginPresenterImple.onSuccess(loginData);
    }

    @Override
    public void onForgotPasswordEmailError() {
        loginPresenterImple.stopProgressBar();
        loginPresenterImple.onForgotPasswordEmailError();
    }

    @Override
    public void onForgotPasswordInvalidEmailError() {
        loginPresenterImple.stopProgressBar();
        loginPresenterImple.onForgotPasswordInvalidEmailError();
    }

    @Override
    public void onSuccessForgotPassword(String msg) {
        loginPresenterImple.onSuccessForgotPassword(msg);
    }

    /**
     *<h1>LoginPresenterImple</h1>
     * LoginPresenterImple interface for View implementation
     * @see LoginActivity
     */
    interface LoginPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onUsernameError();
        void onPasswordError();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(LoginData loginData);
        void onForgotPasswordEmailError();
        void onForgotPasswordInvalidEmailError();
        void onSuccessForgotPassword(String msg);
    }
}
