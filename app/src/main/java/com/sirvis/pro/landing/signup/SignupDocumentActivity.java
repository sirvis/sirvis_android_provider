package com.sirvis.pro.landing.signup;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.pojo.signup.CategoriesData;
import com.sirvis.pro.pojo.signup.CategoryDocument;
import com.sirvis.pro.pojo.signup.CategoryDocumentField;
import com.sirvis.pro.utility.DatePickerCommon;
import com.sirvis.pro.utility.ImageViewActivity;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>SignupDocumentActivity</h1>
 * SignupDocumentActivity activity for uploading the documents for required services that was selected in signup screen
 */


public class SignupDocumentActivity extends AppCompatActivity implements DatePickerCommon.DateSelected, EasyPermissions.PermissionCallbacks {

    private static final String TAG = "CategoryDocument";
    ArrayList<CategoryDocumentField> lastUploadedDocuments;
    private ArrayList<CategoryDocument> document;
    private ArrayList<EditText> etDocuments, etDates;
    private ArrayList<ImageView> ivDocuments;
    private EditText tempDates;
    private ImageView tempPicture;
    private DatePickerCommon datePickerFragment;
    private UploadFileAmazonS3 amazonS3;
    private File mFileTemp;
    private Typeface fontRegular;
    private ProgressDialog progressDialog;
    private String catergoryId = "", categoryName = "";
    private boolean isAlreadySaved = false;
    private String document_upload_photo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_document);

        init();
    }

    /**
     * initialize the values
     */
    private void init() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.uploading));
        progressDialog.setCancelable(false);
        etDocuments = new ArrayList<>();
        etDates = new ArrayList<>();
        ivDocuments = new ArrayList<>();
        lastUploadedDocuments = new ArrayList<>();

        datePickerFragment = new DatePickerCommon();
        datePickerFragment.setCallBack(this);
        amazonS3 = UploadFileAmazonS3.getInstance(this);

        Intent intent = getIntent();
        CategoriesData categoriesData = (CategoriesData) intent.getSerializableExtra("category");
        document = categoriesData.getDocument();
        catergoryId = categoriesData.getCatId();
        categoryName = categoriesData.getCatName();
        isAlreadySaved = intent.getBooleanExtra("isAlreadySaved", false);

        if (isAlreadySaved) {
            String values = intent.getStringExtra("documentArray");

            try {
                Gson gson = new Gson();
                Type collectionType = new TypeToken<ArrayList<CategoryDocumentField>>() {
                }.getType();
                lastUploadedDocuments = gson.fromJson(values, collectionType);

                //Log.d(TAG, "init: "+lastUploadedDocuments.get(0).getFid());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String filename = VariableConstant.DOCUMENT_FILE_NAME + System.currentTimeMillis() + ".png";
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.DOCUMENT_PIC_DIR, true), filename);

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);
        tvTitle.setText(categoriesData.getCatName());

        TextView tvSave = findViewById(R.id.tvSave);
        tvSave.setTypeface(fontBold);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDocumentArry();
            }
        });

        LinearLayout llSericeDocument = findViewById(R.id.llSericeDocument);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (CategoryDocument categoryDocument : document) {
            TextView textView = (TextView) inflater.inflate(R.layout.signup_document_single_item_text_view_header, null);
            textView.setText(categoryDocument.getDocName());
            textView.setTypeface(fontMedium);

            llSericeDocument.addView(textView);

            if (categoryDocument.getField() != null) {
                for (CategoryDocumentField categoryDocumentField : categoryDocument.getField()) {
                    View viewServiceField;
                    String hint = categoryDocumentField.getfName();

                    // 1 => EditText, 2 = > Date-Time (from current to future) 3 => Upload Document 4 => Date-Time (from past to current)
                    switch (categoryDocumentField.getfType()) {

                        case "1":
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_edit_text, null);
                            EditText etDocument = viewServiceField.findViewById(R.id.etDocument);
                            TextInputLayout tilDocument = viewServiceField.findViewById(R.id.tilDocument);
                            etDocument.setTypeface(fontMedium);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tilDocument.setHint(hint);

                            if (isAlreadySaved) {
                                for (CategoryDocumentField documentUpload : lastUploadedDocuments) {
                                    if (documentUpload.getFid().equals(categoryDocumentField.getFid())) {
                                        etDocument.setText(documentUpload.getData());
                                    }
                                }
                            }

                            etDocuments.add(etDocument);
                            break;

                        case "2":
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_date, null);
                            final EditText etDate = viewServiceField.findViewById(R.id.etDate);
                            TextInputLayout tilDob = viewServiceField.findViewById(R.id.tilDob);
                            etDate.setTypeface(fontMedium);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tilDob.setHint(hint);

                            etDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
                                        if (!datePickerFragment.isResumed()) {
                                            datePickerFragment.setDatePickerType(3);
                                            tempDates = etDate;
                                            datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                        }
                                    }
                                }
                            });
                            etDate.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!datePickerFragment.isResumed()) {
                                        datePickerFragment.setDatePickerType(3);
                                        tempDates = etDate;
                                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                    }
                                }
                            });


                            if (isAlreadySaved) {
                                for (CategoryDocumentField documentUpload : lastUploadedDocuments) {
                                    if (documentUpload.getFid().equals(categoryDocumentField.getFid())) {
                                        etDate.setText(datePickerFragment.returmSendingToDisplayDate(documentUpload.getData()));
                                        etDate.setTag(documentUpload.getData());
                                    }
                                }
                            }
                            etDates.add(etDate);
                            break;


                        case "4":
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_date, null);
                            final EditText etDatePast = viewServiceField.findViewById(R.id.etDate);
                            TextInputLayout tilDobPast = viewServiceField.findViewById(R.id.tilDob);
                            etDatePast.setTypeface(fontMedium);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tilDobPast.setHint(hint);

                            etDatePast.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
                                        if (!datePickerFragment.isResumed()) {
                                            datePickerFragment.setDatePickerType(4);
                                            tempDates = etDatePast;
                                            datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                        }
                                    }
                                }
                            });
                            etDatePast.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!datePickerFragment.isResumed()) {
                                        datePickerFragment.setDatePickerType(4);
                                        tempDates = etDatePast;
                                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                    }
                                }
                            });
                            if (isAlreadySaved) {
                                for (CategoryDocumentField documentUpload : lastUploadedDocuments) {
                                    if (documentUpload.getFid().equals(categoryDocumentField.getFid())) {
                                        etDatePast.setText(datePickerFragment.returmSendingToDisplayDate(documentUpload.getData()));
                                        etDatePast.setTag(documentUpload.getData());
                                    }
                                }
                            }
                            etDates.add(etDatePast);
                            break;


                        default:
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_image, null);

                            final ImageView ivDocument = viewServiceField.findViewById(R.id.ivDocument);
                            final TextView upload_button = viewServiceField.findViewById(R.id.upload_button);
                            TextView tvImageLabel = viewServiceField.findViewById(R.id.tvImageLabel);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tvImageLabel.setHint(hint);


                            upload_button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    tempPicture = ivDocument;

                                    if (EasyPermissions.hasPermissions(SignupDocumentActivity.this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
                                        selectImage();
                                    } else {
                                        EasyPermissions.requestPermissions(SignupDocumentActivity.this, getString(R.string.read_storage_and_camera_state_permission_message),
                                                1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
                                    }
                                }
                            });

                            if (isAlreadySaved) {
                                for (CategoryDocumentField documentUpload : lastUploadedDocuments) {
                                    if (documentUpload.getFid().equals(categoryDocumentField.getFid())) {
                                        if (!documentUpload.getData().equals("")) {
                                            Glide.with(this).setDefaultRequestOptions(
                                                    new RequestOptions().error(R.drawable.upload_take_photo_icon)
                                                            .placeholder(R.drawable.upload_take_photo_icon))
                                                    .load(documentUpload.getData())
                                                    .into(ivDocument);
                                        }
                                        ivDocument.setTag(documentUpload.getData());
                                    }
                                }
                            }

                            ivDocuments.add(ivDocument);
                            break;
                    }
                    llSericeDocument.addView(viewServiceField);
                }
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (EditText editText : etDates) {
                    editText.setFocusable(true);
                    editText.setFocusableInTouchMode(true);
                }
            }
        }, 300);

    }

    /**
     * method for creating values from the dynamic fields and check the validation for mantadatory field
     */
    private void createDocumentArry() {
        ArrayList<CategoryDocumentField> documentUploads = new ArrayList<>();

        int etDocumetIndex = 0;
        int etDateIndex = 0;
        int ivDocumetIndex = 0;

        for (CategoryDocument categoryDocument : document) {
            if (categoryDocument.getField() != null) {
                for (CategoryDocumentField categoryDocumentField : categoryDocument.getField()) {
                    CategoryDocumentField documentUpload = new CategoryDocumentField();
                    ;
                    documentUpload.setFid(categoryDocumentField.getFid());
                    documentUpload.setfType(categoryDocumentField.getfType());
                    documentUpload.setfName(categoryDocumentField.getfName());
                    documentUpload.setIsManadatory(categoryDocumentField.getIsManadatory());

                    switch (categoryDocumentField.getfType()) {
                        case "1":
                            EditText etDocument = etDocuments.get(etDocumetIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && etDocument.getText().toString().equals("")) {
                                Toast.makeText(this, categoryDocumentField.getfName() + " " + getString(R.string.isMandatory), Toast.LENGTH_SHORT).show();
                                return;
                            } else if (!etDocument.getText().toString().equals("")) {
                                documentUpload.setData(etDocument.getText().toString());
                            } else {
                                documentUpload = null;
                            }

                            etDocumetIndex++;
                            break;

                        case "2":
                        case "4":
                            EditText etDate = etDates.get(etDateIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && etDate.getText().toString().equals("")) {
                                Toast.makeText(this, categoryDocumentField.getfName() + " " + getString(R.string.isMandatory), Toast.LENGTH_SHORT).show();
                                return;
                            } else if (!etDate.getText().toString().equals("")) {
                                documentUpload.setData(etDate.getTag().toString());
                            } else {
                                documentUpload = null;
                            }
                            etDateIndex++;
                            break;

                        default:
                            ImageView ivDocument = ivDocuments.get(ivDocumetIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && ivDocument.getTag() == null) {
                                Toast.makeText(this, categoryDocumentField.getfName() + " " + getString(R.string.isMandatory), Toast.LENGTH_SHORT).show();
                                return;
                            } else if (ivDocument.getTag() != null) {
                                documentUpload.setData(ivDocument.getTag().toString());
                            } else {
                                documentUpload = null;
                            }
                            ivDocumetIndex++;
                            break;
                    }

                    if (documentUpload != null) {
                        documentUploads.add(documentUpload);
                    }
                }
            }
        }

        /**
         * creating json Array from pojo class
         */
        try {
            VariableConstant.getHashMapIsMandatoryUploaded().put(categoryName, true);


            Gson gson = new Gson();
            String listString = gson.toJson(documentUploads, new TypeToken<ArrayList<CategoryDocument>>() {
            }.getType());

            Log.d(TAG, "createDocumentArry: " + listString);

            Intent intent = new Intent(this, SignupActivity.class);
            intent.putExtra("documentArray", listString);
            intent.putExtra("catergoryId", catergoryId);
            setResult(RESULT_OK, intent);
            closeActivity(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            closeActivity(false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity(false);
    }

    private void closeActivity(boolean isFromSave) {
        chekMandatoryUploaded();
        if (!isFromSave && isAlreadySaved) {
            /**
             * creating json Array from pojo class
             */
            try {
                VariableConstant.getHashMapIsMandatoryUploaded().put(categoryName, true);

                Gson gson = new Gson();
                String listString = gson.toJson(lastUploadedDocuments, new TypeToken<ArrayList<CategoryDocument>>() {
                }.getType());

                Log.d(TAG, "createDocumentArry: " + listString);

                Intent intent = new Intent(this, SignupActivity.class);
                intent.putExtra("documentArray", listString);
                intent.putExtra("catergoryId", catergoryId);
                setResult(RESULT_OK, intent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
        }
    }

    /**
     * check the mandatory fields are filled that will be check during signup click
     */
    private void chekMandatoryUploaded() {
        int etDocumetIndex = 0;
        int etDateIndex = 0;
        int ivDocumetIndex = 0;
        for (CategoryDocument categoryDocument : document) {
            if (categoryDocument.getField() != null) {
                for (CategoryDocumentField categoryDocumentField : categoryDocument.getField()) {
                    switch (categoryDocumentField.getfType()) {
                        case "1":
                            EditText etDocument = etDocuments.get(etDocumetIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && etDocument.getText().toString().equals("")) {
                                VariableConstant.getHashMapIsMandatoryUploaded().put(categoryName, false);
                                return;
                            }
                            etDocumetIndex++;
                            break;

                        case "2":
                        case "4":
                            EditText etDate = etDates.get(etDateIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && etDate.getText().toString().equals("")) {
                                VariableConstant.getHashMapIsMandatoryUploaded().put(categoryName, false);
                                return;
                            }
                            etDateIndex++;
                            break;

                        default:
                            ImageView ivDocument = ivDocuments.get(ivDocumetIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && ivDocument.getTag() == null) {
                                VariableConstant.getHashMapIsMandatoryUploaded().put(categoryName, false);
                                return;
                            }
                            ivDocumetIndex++;
                            break;
                    }
                }
            }
        }

        VariableConstant.getHashMapIsMandatoryUploaded().put(categoryName, true);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    /**
     * method for creating the alert dialog for showing option for selecting image
     */
    private void selectImage() {

        if (mFileTemp == null) {
            String filename = VariableConstant.DOCUMENT_FILE_NAME + System.currentTimeMillis() + ".png";
            MyImageHandler myImageHandler = MyImageHandler.getInstance();
            mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.DOCUMENT_PIC_DIR, true), filename);
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.profile_pic_options, null);
        alertDialogBuilder.setView(view);

        final AlertDialog mDialog = alertDialogBuilder.create();
        mDialog.setCancelable(false);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button btnCamera = view.findViewById(R.id.camera);
        Button btnCancel = view.findViewById(R.id.cancel);
        Button btnGallery = view.findViewById(R.id.gallery);
        Button btnRemove = view.findViewById(R.id.removephoto);
        TextView tvHeader = view.findViewById(R.id.tvHeader);

        btnCamera.setTypeface(fontRegular);
        btnCancel.setTypeface(fontRegular);
        btnGallery.setTypeface(fontRegular);
        btnRemove.setTypeface(fontRegular);
        tvHeader.setTypeface(fontRegular);

        btnRemove.setVisibility(View.GONE);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {

                case VariableConstant.REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        amazonUpload();
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    amazonUpload();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for uploading image to amazon
     */
    private void amazonUpload() {
        //progressDialog.show();
        final String filename = VariableConstant.DOCUMENT_FILE_NAME + System.currentTimeMillis() + ".png";

      /*  BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 5;*/
        Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(mFileTemp)/*, options*/);
        tempPicture.setImageBitmap(bitmap);
//        sizeOf(bitmap);
        tempPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!document_upload_photo.isEmpty()) {
                    Intent intent = new Intent(SignupDocumentActivity.this, ImageViewActivity.class);
                    intent.putExtra("ivDocument_url", mFileTemp.getAbsolutePath());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(SignupDocumentActivity.this, (ImageView) view, getString(R.string.activity_image_trans));
                        startActivity(intent, options.toBundle());
                    } else {
                        startActivity(intent);
                    }
                }

            }
        });

        document_upload_photo = filename;

        progressDialog.setMessage(getString(R.string.uploading));
        progressDialog.show();
        new UploadFileToServer().execute(mFileTemp.getPath());

    }


    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        tempDates.setText(displayFormat);
        tempDates.setTag(sendingFormat);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            Log.d("sizeOf1:", data.getRowBytes() * data.getHeight() + "");
            return data.getRowBytes() * data.getHeight();
        } else {
            Log.d("sizeOf2:", data.getByteCount() + "");
            return data.getByteCount();
        }
    }

    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String[] doInBackground(String... params)
        {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result,responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("uploadTo","1")
                        .addFormDataPart("folder",VariableConstant.DOCUMENT_FILE_NAME)
                        .addFormDataPart("file", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.SIGNUPIMAGE)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: "+responseCode);
                Log.d(TAG, "doInBackground: "+result);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            progressDialog.dismiss();
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    JSONObject dataObject = jsonObject.getJSONObject("data");
                    String imageUrl = dataObject.getString("imageUrl");
                    Log.d(TAG, "onPostExecute: "+imageUrl+"   "+result.length);
                    tempPicture.setTag(imageUrl);
                }
                else
                    Toast.makeText(SignupDocumentActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();


            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(SignupDocumentActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);
        }

    }
}
