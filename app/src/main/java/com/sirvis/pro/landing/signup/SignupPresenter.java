package com.sirvis.pro.landing.signup;

import android.animation.ObjectAnimator;
import android.util.Patterns;
import android.view.animation.DecelerateInterpolator;
import android.widget.SeekBar;

import com.sirvis.pro.pojo.signup.CityData;
import com.sirvis.pro.pojo.signup.CategoriesData;
import com.sirvis.pro.pojo.signup.SignUpPojo;
import com.sirvis.pro.utility.UploadFileAmazonS3;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>SignupPresenter</h1>
 * SignupPresenter presenter for SignupActivity
 * @see SignupActivity
 */

public class SignupPresenter implements SignupModel.SignupModelImple {
    private SignupModel signupModel;
    private SignPresenterImple signPresenterImple;

    SignupPresenter(SignPresenterImple signPresenterImple) {
        signupModel = new SignupModel(this);
        this.signPresenterImple = signPresenterImple;
    }

    /**
     * method for calling getCity in model class from view class
     */
    void getCityList()
    {
        signPresenterImple.startProgressBar();
        signupModel.getCity();
    }

    /**
     * method for passing values from view to model
     */
    void getServiceCategories(String latitude, String longitude)
    {
        signPresenterImple.startProgressBar();
        signupModel.getSeriviceCategories(latitude,longitude);
    }

    /**
     * method for passing values form view to model
     * @param jsonObject required field
     */
    void verifyPhone( JSONObject jsonObject) {
        try {
            if(jsonObject.getString("mobile").equals(""))
            {
                signPresenterImple.onPhoneNumberInCorrect();
            }
            else
            {
                signPresenterImple.onPhoneNumberCorrect();
                signupModel.verifyPhone(jsonObject);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * method for passing values form view to model
     * @param jsonObject required field
     */
    void verifyEmail( JSONObject jsonObject) {
        try {
            if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches())
            {
                signPresenterImple.onEmailInCorrect();
                return;
            }
            else
            {
                signPresenterImple.onEmailCorrect();
                signupModel.verifyEmail(jsonObject);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    void verifyPassword(String pass) {
        if(!signupModel.isValidPassword(pass))
        {
            signPresenterImple.onPasswordInCorrect();
        }
        else
        {
            signPresenterImple.onPasswordCorrect();
        }
    }

    void verifyFirstName(String val) {
        if(val.equals(""))
        {
            signPresenterImple.onUserNameInCorrect();
        }
        else
        {
            signPresenterImple.onUserNameCorrect();
        }
    }

    void verifyLastName(String val) {
        if(val.equals(""))
        {
            signPresenterImple.onUserLastNameInCorrect();
        }
        else
        {
            signPresenterImple.onUserLastNameCorrect();
        }
    }

    /**
     * method for passing values form view to model
     * @param mFileTemp file which has to been upload in server
     */
    void amazonUpload( File mFileTemp)
    {
        signupModel.amazonUpload(mFileTemp);
    }

    /**
     * method for passing values form view to model
     * @param singupJsonObject required field for signup
     * @param referalJsonObject required field for referal code
     */
    void verifyReferalCodeAndSignup(final JSONObject singupJsonObject, JSONObject referalJsonObject) {
        signupModel.verifyReferalCodeAndSignup(singupJsonObject,referalJsonObject);
    }


    void setSeekBarProgress(SeekBar seekBar, int progress)
    {
        ObjectAnimator animation = ObjectAnimator.ofInt(seekBar, "progress", progress);
        animation.setDuration(500); // 0.5 second
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    @Override
    public void startProgressBar() {
        signPresenterImple.startProgressBar();
    }


    @Override
    public void onFailure(String failureMsg) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailure();
    }

    @Override
    public void onSuccessSignUp(SignUpPojo signUpPojo) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onSuccessSignUp(signUpPojo);
    }

    @Override
    public void onSuccessCity(ArrayList<CityData> cityDataArrayList) {
        signPresenterImple.onSuccessCity(cityDataArrayList);
    }

    @Override
    public void onSuccessServiceCategories(ArrayList<CategoriesData> categoriesData,String cityId) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onSuccessServiceCategories(categoriesData,cityId);
    }

    @Override
    public void onFailureServiceCategories() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailureServiceCategories();
    }

    @Override
    public void onSuccessImageUpload(String imgUrl) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onSuccessImageUpload(imgUrl);
    }

    @Override
    public void onFailurePhoneValidation(String msg) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailurePhoneValidation(msg);
    }

    @Override
    public void onFailureEmailValidation(String msg) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailureEmailValidation(msg);
    }

    @Override
    public void onSuccessPhoneEmailValidation() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onSuccessPhoneEmailValidation();
    }

    @Override
    public void onFailureReferalValidation(String msg) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailureReferalValidation(msg);
    }

    @Override
    public void nonValidateField() {
        signPresenterImple.stopProgressBar();
    }

    @Override
    public void onUserNameError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onUserNameError();
    }

    @Override
    public void onUserLastNameError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onUserLastNameError();
    }

    @Override
    public void onProfilePicError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onProfilePicError();
    }

    @Override
    public void onInValidEmailError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onInValidEmailError();
    }

    @Override
    public void onEmailError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onEmailError();
    }

    @Override
    public void onCitySelectionError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onCitySelectionError();
    }

    @Override
    public void onCategorySelectionError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onCategorySelectionError();
    }

    @Override
    public void onSubCategorySelectionError(String category) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onSubCategorySelectionError(category);
    }

    @Override
    public void onCategoryDocumentUploadError(String category) {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onCategoryDocumentUploadError(category);
    }

    @Override
    public void onPasswordError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onPasswordError();
    }

    @Override
    public void onPhoneNumberError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onPhoneNumberError();
    }

    @Override
    public void onDobError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onDobError();
    }

    @Override
    public void onGenderError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onGenderError();
    }

    @Override
    public void onLocationError() {
        signPresenterImple.stopProgressBar();
        signPresenterImple.onLocationError();
    }

    @Override
    public void onVatSelectError(){
        signPresenterImple.stopProgressBar();
        signPresenterImple.onVatSelectError();
    }

    @Override
    public void onVatNumberError(){
        signPresenterImple.stopProgressBar();
        signPresenterImple.onVatNumberError();
    }

    @Override
    public void onFailureImageUpload(){
        signPresenterImple.stopProgressBar();
        signPresenterImple.onFailureImageUpload();
    }



    @Override
    public void onSuccesLocalValidation() {
        signPresenterImple.onSuccesLocalValidation();
    }


    String getCategoryList(ArrayList<String> serviceCategoriseIds) {
        String selelectedId="",prefix = "";
        for(String id: serviceCategoriseIds)
        {
            selelectedId = selelectedId + prefix + id;
            prefix = ",";
        }
        return selelectedId;
    }

    interface SignPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccessSignUp(SignUpPojo signUpPojo);
        void onSuccessCity(ArrayList<CityData> cityDataArrayList);
        void onSuccessServiceCategories(ArrayList<CategoriesData> categoriesData,String cityId);
        void onSuccessImageUpload(String imgUrl);
        void onFailureServiceCategories();
        void onSuccessPhoneEmailValidation();
        void onFailurePhoneValidation(String msg);
        void onFailureEmailValidation(String msg);
        void onFailureReferalValidation(String msg);

        void onProfilePicError();
        void onUserNameError();
        void onUserLastNameError();
        void onInValidEmailError();
        void onEmailError();
        void onCitySelectionError();
        void onCategorySelectionError();
        void onSubCategorySelectionError(String category);
        void onCategoryDocumentUploadError(String category);
        void onPasswordError();
        void onPhoneNumberError();
        void onDobError();
        void onGenderError();
        void onLocationError();
        void onSuccesLocalValidation();

        void onVatNumberError();
        void onVatSelectError();

        void onUserNameInCorrect();
        void onUserNameCorrect();
        void onUserLastNameInCorrect();
        void onUserLastNameCorrect();
        void onEmailInCorrect();
        void onEmailCorrect();
        void onPhoneNumberInCorrect();
        void onPhoneNumberCorrect();
        void onPasswordInCorrect();
        void onPasswordCorrect();
        void onFailureImageUpload();

    }
}
