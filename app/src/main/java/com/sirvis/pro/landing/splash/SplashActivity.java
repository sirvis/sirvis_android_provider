package com.sirvis.pro.landing.splash;

import android.Manifest;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.LanguageAdapter;
import com.sirvis.pro.landing.login.LoginActivity;
import com.sirvis.pro.landing.signup.SignupActivity;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.pojo.language.LanguageData;
import com.sirvis.pro.pojo.language.LanguagePojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>SplashActivity</h1>
 */
public class SplashActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, SplashPresenter.View, LanguageAdapter.OnLanguageSelectListener, AdapterView.OnItemClickListener {

    private Button btnLogin, btnSignup;
    private String msg = null;
    private boolean isResume = false;

    private String result;
    private SessionManager sessionManager;
    private Gson gson;
    private TextView tvLanguage;
    private RelativeLayout rlLanguage;
    private ListPopupWindow listPopupWindow;
    private ArrayList<LanguageData> languageData;
    private String[] languages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        Utility.checkAndShowNetworkError(this);
    }


    /**
     * BookingStatus Permission for Location and Phone State
     */
    @Override
    protected void onStart() {
        super.onStart();

        String[] perms = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.read_phone_and_location_permission_message),
                    1000, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            DialogHelper.aDialogOnPermissionDenied(this);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_phone_and_location_permission_message),
                    1000, perms.toArray(new String[perms.size()]));
        }
    }

    /**
     * Initialize the Views
     * If Driver already login then open Main Activity after 5 seconds
     *
     * @see MainActivity
     */
    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        gson = new Gson();
        if (sessionManager.getPushToken().equals("")) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            sessionManager.setPushToken(refreshedToken);
        }

        Log.d("PUSH_TOKEN ", "init: " + sessionManager.getPushToken());

        final TextView tvSplashHead = findViewById(R.id.tvSplashHead);
        final TextView tvSplashMsg = findViewById(R.id.tvSplashMsg);
        rlLanguage = findViewById(R.id.rlLanguage);
        tvLanguage = findViewById(R.id.tvLanguage);

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        btnLogin = findViewById(R.id.btnLogin);
        btnSignup = findViewById(R.id.btnSignup);

        btnLogin.setTypeface(fontBold);
        btnSignup.setTypeface(fontBold);
        tvSplashHead.setTypeface(fontBold);
        tvSplashMsg.setTypeface(fontMedium);
        tvLanguage.setTypeface(fontMedium);

        btnLogin.setOnClickListener(this);
        btnSignup.setOnClickListener(this);
        rlLanguage.setOnClickListener(this);

        languageData = new ArrayList<>();
        if (sessionManager.getIsDriverLogin()) {
            //VariableConstant.LANGUAGE = sessionManager.getLanguageCode();

            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            finish();
            startActivity(intent);
            overridePendingTransition(R.anim.stay, R.anim.fade_open);

            AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.AppOpenAfterLogin.value);
        } else {
            onSuccess(sessionManager.getLanguageList());
            new SplashPresenter(this).getLanguage();
            AppController.getInstance().getMixpanelHelper().commonTrackBeforeLogin(MixpanelEvents.AppOpenBeforeLogin.value);
        }

        msg = getIntent().getStringExtra("msg");

        final ImageView ivSplashLogo = findViewById(R.id.ivSplashLogo);
        final ImageView ivSplashLogo2 = findViewById(R.id.ivSplashLogo2);
//        findViewById(R.id.ivSplash).setVisibility(View.VISIBLE);
        final LinearLayout llLoginSignup = findViewById(R.id.llLoginSignup);
        final Animation right_to_center = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.slide_right_to_center);
        final Animation left_to_center = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.slide_left_to_center);
        final Animation current_to_top = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.current_to_top_splash);
        final Animation fade_open = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_open);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ivSplashLogo.startAnimation(current_to_top);
            }
        }, 1500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                btnLogin.setVisibility(View.VISIBLE);
                btnSignup.setVisibility(View.VISIBLE);
                llLoginSignup.setVisibility(View.VISIBLE);
                ivSplashLogo.setVisibility(View.GONE);
//                ivSplashLogo2.setVisibility(View.VISIBLE);

                tvSplashHead.setVisibility(View.INVISIBLE);
                tvSplashMsg.setVisibility(View.INVISIBLE);
                btnLogin.startAnimation(left_to_center);
                btnSignup.startAnimation(right_to_center);
                tvSplashHead.startAnimation(fade_open);
                tvSplashMsg.startAnimation(fade_open);
            }
        }, 2000);

    }

    @Override
    protected void onResume() {
        super.onResume();
        isResume = true;

        if (msg != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (isResume)
                            DialogHelper.customAlertDialog(SplashActivity.this, getString(R.string.message), msg, getString(R.string.oK));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    msg = null;
                }
            }, 500);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        isResume = false;
    }

    /**
     * Open LoginActivity when user click login
     * Open SignupActivity when user click signup
     *
     * @param v view
     * @see LoginActivity,SignupActivity
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                Intent loginIntent = new Intent(this, LoginActivity.class);
                startActivity(loginIntent);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;

            case R.id.btnSignup:

                Intent signupIntent = new Intent(this, SignupActivity.class);
                startActivity(signupIntent);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;


            case R.id.rlLanguage:
                if (languages != null && languages.length > 0) {
                    listPopupWindow.show();
                }
                break;
        }
    }

    @Override
    public void onSuccess(String result) {
        try {
            if (this.result == null || !result.equals(this.result)) {
                this.result = result;
                sessionManager.setLanguageList(result);
                LanguagePojo languagePojo = gson.fromJson(result, LanguagePojo.class);
                languageData.clear();

                languageData.addAll(languagePojo.getData());
                if (languageData.size() > 1) {
                    rlLanguage.setVisibility(View.VISIBLE);
                } else {
                    rlLanguage.setVisibility(View.GONE);
                }
                languages = new String[languageData.size()];
                for (int i = 0; i < languageData.size(); i++) {
                    languages[i] = languageData.get(i).getLan_name();
                }
                ListAdapter listAdapter = new ArrayAdapter<>(this, R.layout.single_row_text_view_language, languages);
                listPopupWindow = new ListPopupWindow(this);
                //listPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                listPopupWindow.setAnchorView(rlLanguage);
                listPopupWindow.setAdapter(listAdapter);
                listPopupWindow.setOnItemClickListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onLanguageSelect(String language, String code) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        tvLanguage.setText(languages[i]);
        sessionManager.setLanguageCode(languageData.get(i).getCode());
        //VariableConstant.LANGUAGE = sessionManager.getLanguageCode();
        listPopupWindow.dismiss();
    }
}

