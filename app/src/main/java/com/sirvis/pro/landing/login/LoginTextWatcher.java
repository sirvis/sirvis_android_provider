package com.sirvis.pro.landing.login;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

/**
 * Created by ads on 11/05/17.
 * <h1>LoginTextWatcher</h1>
 * LoginTextWatcher is used to check the user enter the field
 */

public class LoginTextWatcher implements TextWatcher {

    private EditText emailPhone;
    private EditText password;
    private LoginTextError loginTextError;

    public LoginTextWatcher(EditText emailPhone, EditText password, LoginTextError loginTextError) {
        this.emailPhone = emailPhone;
        this.password = password;
        this.loginTextError = loginTextError;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {

        boolean isValidEmail = Patterns.EMAIL_ADDRESS.matcher(emailPhone.getText().toString()).matches() || Patterns.PHONE.matcher(emailPhone.getText().toString()).matches();

        if (isValidEmail && password.length() > 0)
        {
            loginTextError.enableFull();
            loginTextError.setDisableError(password);
            loginTextError.setDisableError(emailPhone);
        }
        else if(isValidEmail || password.length() > 0)
        {
            loginTextError.enableHalf();

            if(!isValidEmail)
            {
                loginTextError.onUsernameError();
                loginTextError.setDisableError(password);
            }
            else if(password.length() < 0)
            {
                loginTextError.onPasswordError();
                loginTextError.setDisableError(emailPhone);
            }
            else
            {
                loginTextError.setDisableError(emailPhone);
            }
        }
        else
        {
            loginTextError.disableLoginIn();
        }

    }

    /**
     * <h2>LoginTextError</h2>
     * <p>this method is used to give callback to the view
     * to disabling the error that is currently showing</p>
     *
     */
    interface LoginTextError {

        void setDisableError(View view);

        void enableFull();

        void enableHalf();

        void disableLoginIn();

        void onUsernameError();

         void onPasswordError();

    }

}