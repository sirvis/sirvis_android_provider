package com.sirvis.pro.landing.signup;

/**
 * <h1>SignupSelectionListener</h1>
 * Interface for selected the city callback
 * */
public interface SignupSelectionListener
{
    void onCitySelecting(String id, String name);
    void onSubCategorySelecting(String id, String name);
}