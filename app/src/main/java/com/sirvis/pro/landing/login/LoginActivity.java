package com.sirvis.pro.landing.login;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.sirvis.pro.R;
import com.sirvis.pro.changepassword.HelpWithPasswordActivity;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.pojo.signup.LoginData;
import com.sirvis.pro.landing.signup.SignupActivity;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>LoginActivity</h1>
 * LoginActivity for user Login
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginTextWatcher.LoginTextError, LoginPresenter.LoginPresenterImple {

    private TextInputLayout tilEmailPhone,tilPassword;
    private EditText etEmailPhone,etPassword;
    private TextView tvLogin;
    private LoginPresenter loginPresenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private TextView tvHelpWithPassword;

    private CheckBox cbRememberMe;
    private SeekBar seekBarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }

    /**
     * init the local values
     */
    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        loginPresenter = new LoginPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.logingIn));
        progressDialog.setCancelable(false);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.login));
        tvTitle.setTypeface(fontBold);

        tilEmailPhone = findViewById(R.id.tilEmailPhone);
        tilPassword = findViewById(R.id.tilPassword);
        etEmailPhone = findViewById(R.id.etEmailPhone);
        etPassword = findViewById(R.id.etPassword);

        tvLogin = findViewById(R.id.tvLogin);
        tvHelpWithPassword = findViewById(R.id.tvHelpWithPassword);
        cbRememberMe = findViewById(R.id.cbRememberMe);

        seekBarButton = findViewById(R.id.seekBarButton);

        etEmailPhone.setTypeface(fontMedium);
        etPassword.setTypeface(fontMedium);
        tvLogin.setTypeface(fontBold);
        tvHelpWithPassword.setTypeface(fontMedium);
        cbRememberMe.setTypeface(fontMedium);

        etEmailPhone.addTextChangedListener(new LoginTextWatcher(etEmailPhone,etPassword,this));
        etPassword.addTextChangedListener(new LoginTextWatcher(etEmailPhone,etPassword,this));
        tvHelpWithPassword.setOnClickListener(this);
        tvLogin.setOnClickListener(null);

        if(sessionManager.getIsRemembered())
        {
            cbRememberMe.setChecked(true);
            etEmailPhone.setText(sessionManager.getUserName());
            etPassword.setText(sessionManager.getPassword());
        }

        LinearLayout llSignup = findViewById(R.id.llSignup);
        llSignup.setOnClickListener(this);
        TextView tvSignup1 = findViewById(R.id.tvSignup1);
        tvSignup1.setTypeface(fontRegular);

        TextView tvSignup2 = findViewById(R.id.tvSignup2);
        tvSignup2.setTypeface(fontRegular);

        double[] location = Utility.getLocation(this);
        sessionManager.setCurrentLat(String.valueOf(location[0]));
        sessionManager.setCurrentLng(String.valueOf(location[1]));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvLogin:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("mobileOrEmail",etEmailPhone.getText().toString());
                    jsonObject.put("password",etPassword.getText().toString());
                    jsonObject.put("deviceType", VariableConstant.DEVICE_TYPE);
                    jsonObject.put("deviceId",Utility.getDeviceId(this));
                    jsonObject.put("pushToken",sessionManager.getPushToken());
                    jsonObject.put("appVersion",VariableConstant.APP_VERSION);
                    jsonObject.put("deviceMake",VariableConstant.DEVICE_MAKER);
                    jsonObject.put("deviceModel",VariableConstant.DEVICE_MODEL);
                    jsonObject.put("deviceOsVersion",VariableConstant.OS_VERSION);
                    jsonObject.put("deviceTime",Utility.getCurrentTime());

                    loginPresenter.doLogin(etEmailPhone.getText().toString(),etPassword.getText().toString(),jsonObject);

                    Utility.hideKeyboad(this);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case R.id.tvHelpWithPassword:
                Intent forgetIntent = new Intent(LoginActivity.this, HelpWithPasswordActivity.class);
                overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    overridePendingTransition(android.R.transition.slide_left,0);
                    Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(
                           this,
                            tvHelpWithPassword,
                            tvHelpWithPassword.getTransitionName())
                            .toBundle();
                    startActivity(forgetIntent, bundle);
                }
                else
                {
                    startActivity(forgetIntent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.llSignup:
                finish();
                Intent signupIntent = new Intent(this,SignupActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(signupIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(signupIntent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;
        }
    }

    @Override
    public void setDisableError(View view) {
        if(view.getId() == R.id.etEmailPhone)
        {
            tilEmailPhone.setErrorEnabled(false);
        }
        else
        {
            tilPassword.setErrorEnabled(false);
        }
    }

    @Override
    public void enableFull() {
        //tvLogin.setBackground(ContextCompat.getDrawable(this,R.drawable.selector_button));
        //tvLogin.setTextColor(ContextCompat.getColor(this,R.color.selector_button));
        tvLogin.setOnClickListener(this);
        loginPresenter.setSeekBarProgress(seekBarButton,100);
    }

    @Override
    public void enableHalf() {
        loginPresenter.setSeekBarProgress(seekBarButton,50);
    }

    @Override
    public void disableLoginIn() {
        //tvLogin.setBackground(ContextCompat.getDrawable(this,R.drawable.non_selected_background_for_button));
        //tvLogin.setTextColor(ContextCompat.getColor(this,R.color.white));
        loginPresenter.setSeekBarProgress(seekBarButton,0);
        tvLogin.setOnClickListener(null);
    }



    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onUsernameError() {
        tilEmailPhone.setErrorEnabled(true);
        tilEmailPhone.setError(getString(R.string.invalidEmailPhone));
    }

    @Override
    public void onPasswordError() {
        tilPassword.setErrorEnabled(true);
        tilPassword.setError(getString(R.string.invalidPassword));
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(LoginData loginData) {

        if(cbRememberMe.isChecked())
        {
            sessionManager.setIsRemembered(true);
        }
        else
        {
            sessionManager.setIsRemembered(false);
        }
        sessionManager.setUserName(etEmailPhone.getText().toString());
        sessionManager.setPassword(etPassword.getText().toString());
        sessionManager.setProviderId(loginData.getId());
        sessionManager.setEmail(loginData.getEmail());
        sessionManager.setFirstName(loginData.getFirstName());
        sessionManager.setLastName(loginData.getLastName());
        sessionManager.setProfilePic(loginData.getProfilePic());
        sessionManager.setPhoneNumber(loginData.getMobile());
        sessionManager.setCountryCode(loginData.getCountryCode());
        sessionManager.setReferalCode(loginData.getReferralCode());
        sessionManager.setIsDriverLogin(true);
        sessionManager.setDeviceId(Utility.getDeviceId(this));
        sessionManager.setFCMTopic(loginData.getFcmTopic());
        sessionManager.setZendeskRequesterId(loginData.getRequester_id());
        sessionManager.setIsBidBooking(loginData.isBid());
        sessionManager.setCallToken(loginData.getCall().getAuthToken());
        sessionManager.setCallWillTopic(loginData.getCall().getWillTopic());


        AppController.getInstance().getAccountManagerHelper().setAuthToken(loginData.getEmail(),
                etPassword.getText().toString(), loginData.getToken());

        FirebaseMessaging.getInstance().subscribeToTopic(/*"/topics/" + */sessionManager.getFCMTopic());

        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.Login.value);

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
        startActivity(intent);

    }

    @Override
    public void onForgotPasswordEmailError() {
        Toast.makeText(this,getString(R.string.enterEmail),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onForgotPasswordInvalidEmailError() {
        Toast.makeText(this,getString(R.string.invalidEmail),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessForgotPassword(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();

    }
}
