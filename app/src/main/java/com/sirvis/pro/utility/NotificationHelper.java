package com.sirvis.pro.utility;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.review.RateCustomerActivity;
import com.sirvis.pro.landing.splash.SplashActivity;
import com.sirvis.pro.main.chats.ChattingActivity;
import com.sirvis.pro.main.notification.NotificationActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by murashid on 25-Nov-17.
 */

public class NotificationHelper {

    private static final String notificationChannelIdNormal = "1";
    private static final String notificationChannelIdChat = "2";

    /**
     * Create and show a simple notification based on the action.
     *
     * @param action      based on the action make the sound, open the activity
     * @param title       Notification title
     * @param messageBody message content
     */
    public static void sendNotification(Context context, String action, String title, String messageBody) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri cancel_tone = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/cancel_ringtone");
        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/booking_tone");
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, notificationChannelIdNormal)
                .setSmallIcon(R.drawable.notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVibrate(new long[]{1000, 1000})
                .setAutoCancel(true)

                .setContentIntent(pendingIntent);
        SessionManager sessionManager = SessionManager.getSessionManager(context);

        if (!sessionManager.getIsTeleCallRunning()) {
            if (action.equals("12") || action.equals("15")) {
                notificationBuilder.setSound(cancel_tone);
            } else {
                notificationBuilder.setSound(defaultSoundUri);
            }
        }


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        setNotificationChannel(notificationManager, notificationChannelIdNormal, context.getString(R.string.notificationChannelName), context.getString(R.string.notificationChannelDescription));
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Create and show a simple notification based on the action.
     *
     * @param action      based on the action make the sound, open the activity
     * @param title       Notification title
     * @param messageBody message content
     */
    public static void sendProfileActivationNotification(Context context, String action, String title, String messageBody) {
        Intent intent = new Intent(context, NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri profileactivation = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/incoming");
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, notificationChannelIdNormal)
                .setSmallIcon(R.drawable.notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVibrate(new long[]{1000, 1000})
                .setAutoCancel(true)

                .setContentIntent(pendingIntent);
        SessionManager sessionManager = SessionManager.getSessionManager(context);

        if (!sessionManager.getIsTeleCallRunning()) {
            notificationBuilder.setSound(profileactivation);
        }
        notificationBuilder.setOngoing(true);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        setNotificationChannel(notificationManager, notificationChannelIdNormal, context.getString(R.string.notificationChannelName), context.getString(R.string.notificationChannelDescription));
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Create and show a simple notification of Booking Payment Confirmation
     *
     * @param title       Notification title
     * @param messageBody message content
     */
    public static void sendRateCustomerNotification(Context context, String bookingId, String title, String messageBody) {
        Intent intent = new Intent(context, RateCustomerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean("rateNotfcnCustomer", true);
        bundle.putString("bookingId", bookingId);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/incoming");
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, notificationChannelIdNormal)
                .setSmallIcon(R.drawable.notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVibrate(new long[]{1000, 1000})
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        SessionManager sessionManager = SessionManager.getSessionManager(context);

        if (!sessionManager.getIsTeleCallRunning()) {
            notificationBuilder.setSound(defaultSoundUri);
        }
        notificationBuilder.setOngoing(true);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        setNotificationChannel(notificationManager, notificationChannelIdNormal, context.getString(R.string.notificationChannelName), context.getString(R.string.notificationChannelDescription));
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Create and show a simple notification based on the action.
     *
     * @param title         Notification title
     * @param messageBody   message content
     * @param notificaionId id of the notification
     */
    public static void sendChatNotification(Context context, String title, String messageBody, int notificaionId) {
        Intent intent = new Intent(context, ChattingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_RECEIVER_FOREGROUND);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + context.getPackageName() + "/raw/incoming");
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, notificationChannelIdChat)
                .setSmallIcon(R.drawable.notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(title)

                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setVibrate(new long[]{1000, 1000})
                .setAutoCancel(true)

                .setTicker(context.getString(R.string.newMessageFrom))
                .setGroup("chat")
                .setGroupSummary(true)
                .setContentIntent(pendingIntent);

        if (messageBody.startsWith("http")) {
            if (messageBody.endsWith(".png") || messageBody.endsWith(".jpg") || messageBody.endsWith(".jpeg"))
                notificationBuilder.setContentText("Image");
            else if (messageBody.endsWith(".mp4")) {
                notificationBuilder.setContentText("Video");
            } else {
                notificationBuilder.setContentText(messageBody);
            }
        } else {
            notificationBuilder.setContentText(messageBody);
        }
        SessionManager sessionManager = SessionManager.getSessionManager(context);

        if (!sessionManager.getIsTeleCallRunning()) {
            notificationBuilder.setSound(defaultSoundUri);
        }
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        setNotificationChannel(notificationManager, notificationChannelIdChat, context.getString(R.string.notificationChannelNameChat), context.getString(R.string.notificationChannelDescriptionChat));
        notificationManager.notify(50, notificationBuilder.build());
    }


    private static void setNotificationChannel(NotificationManager notificationManager, String id, String name, String description) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(mChannel);

        }
    }

    public static void playSentTone(Context context) {
        final Ringtone bookingTone = RingtoneManager.getRingtone(context, Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.send_message));
        bookingTone.play();

    }

    public static void playIncomingTone(Context context) {
        final Ringtone bookingTone = RingtoneManager.getRingtone(context, Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.incoming));
        bookingTone.play();

    }
}
