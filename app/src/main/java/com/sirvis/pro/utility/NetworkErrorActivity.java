package com.sirvis.pro.utility;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sirvis.pro.R;

public class NetworkErrorActivity extends AppCompatActivity {

    private Handler handlerNetworkCheck;
    private Runnable runnableNetworkCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_error);
        overridePendingTransition(R.anim.fade_open, R.anim.fade_close);

        VariableConstant.IS_NETWORK_ERROR_SHOWED = true;
        handlerNetworkCheck = new Handler();
        runnableNetworkCheck = new Runnable() {
            @Override
            public void run() {
                if(!Utility.isNetworkAvailable(NetworkErrorActivity.this))
                {
                    handlerNetworkCheck.postDelayed(this,2000);
                }
                else
                {
                    finish();
                }
            }
        };
        handlerNetworkCheck.postDelayed(runnableNetworkCheck,3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handlerNetworkCheck.removeCallbacks(runnableNetworkCheck);
        VariableConstant.IS_NETWORK_ERROR_SHOWED = false;
    }

    @Override
    public void onBackPressed() {

    }
}
