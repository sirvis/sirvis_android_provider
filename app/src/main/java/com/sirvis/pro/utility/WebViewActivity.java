package com.sirvis.pro.utility;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.sirvis.pro.R;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>WebViewActivity</h1>
 * WebViewActivity for displaying the webview
 */
public class WebViewActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private WebView webView;
    private String url,title;
    private boolean isFromSupport;
    private String TAG = "WebViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

        url = getIntent().getStringExtra("URL");
        title = getIntent().getStringExtra("title");
        isFromSupport = getIntent().getBooleanExtra("isFromSupport",false);

        initActionBar();
        initializeViews();

    }

    /**********************************************************************************************/
    /**
     * <h1>initActionBar</h1>
     * initilize the action bar
     */
    private void initActionBar() {
        Toolbar toolbar;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(Utility.getFontBold(this));
        TextView tvSubTitle = findViewById(R.id.tvSubTitle);
        tvTitle.setText(title);
        if(isFromSupport)
        {
            tvTitle.setText(getString(R.string.support));
            tvSubTitle.setTypeface(Utility.getFontMedium(this));
            tvSubTitle.setText(title);
            tvSubTitle.setVisibility(View.VISIBLE);
        }

    }

    /**********************************************************************************************/

    /**
     * initialize the views
     */
    private void initializeViews() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.setCancelable(true);

        webView = findViewById(R.id.webView);

        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                pDialog.dismiss();
            }

        });

        webView.loadUrl(url);

    }


    /**********************************************************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
        return super.onOptionsItemSelected(item);
    }
}
