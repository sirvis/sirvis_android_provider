package com.sirvis.pro.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.sirvis.pro.pojo.chat.ChatData;

import java.util.ArrayList;

/**
 * Created by Ali on 12/22/2017.
 */

public class DataBaseChat extends SQLiteOpenHelper
{


    private static final int DATABASE_VERSION = 3;

    private static final String DATABASE_NAME= "ChatDB";


    private static final String TABLE_NAME = "chatResult";
    //Coulomn name
    private static final String KEY_ID = "proId";
    private static final String BOOKING_ID = "BookingId";
    private static final String CHAT_CONTENT = "chat_content";
    private static final String CHAT_PROVIDER_ID = "chat_providerId";
    private static final String CHAT_CUSTOMER_ID = "chat_customerId";
    private static final String CHAT_TIMESTAMP = "chat_timestamp";
    private static final String CHAT_TYPE = "chat_type";
    private static final String CHAT_CUST_PRO_TYPE = "chat_cust_pro_type";
    public DataBaseChat(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_ADDRESS_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + KEY_ID + " INTEGER PRIMARY KEY," + BOOKING_ID + " TEXT," + CHAT_CONTENT + " TEXT," + CHAT_PROVIDER_ID + " TEXT," + CHAT_CUSTOMER_ID + " TEXT," +
                CHAT_TYPE + " TEXT," + CHAT_CUST_PRO_TYPE + " TEXT," + CHAT_TIMESTAMP + " TEXT" + ")";//REAL
        sqLiteDatabase.execSQL(CREATE_ADDRESS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void addNewChat(String bookingId, String chat_content, String providerId, String customerId, String chat_timestamp,String chat_type,String chat_cust_proType)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(BOOKING_ID, bookingId);
        contentValues.put(CHAT_CONTENT, chat_content);
        contentValues.put(CHAT_PROVIDER_ID, providerId);
        contentValues.put(CHAT_CUSTOMER_ID, customerId);
        contentValues.put(CHAT_TIMESTAMP, chat_timestamp);
        contentValues.put(CHAT_TYPE, chat_type);
        contentValues.put(CHAT_CUST_PRO_TYPE, chat_cust_proType);
        db.insert(TABLE_NAME,null,contentValues);
        db.close();
    }

    public ArrayList<ChatData> fetchData(String proId,String custId)
    {

        SQLiteDatabase db=getWritableDatabase();
        ArrayList<ChatData> dbSavedChats = new ArrayList<>();

        String[] columns={CHAT_CONTENT, CHAT_PROVIDER_ID, CHAT_CUSTOMER_ID,CHAT_TIMESTAMP,CHAT_TYPE,CHAT_CUST_PRO_TYPE};
        String[] nm={proId,custId};
        Cursor cursor = db.query(TABLE_NAME,columns, CHAT_PROVIDER_ID +"=?" +" AND " + CHAT_CUSTOMER_ID +"=?",nm,null,null,null);
        if(cursor.moveToFirst())
        {
            do
            {
                ChatData dbSavedChat = new ChatData();
                dbSavedChat.setContent(cursor.getString(0));
                dbSavedChat.setFromID(cursor.getString(1));
                dbSavedChat.setTargetId(cursor.getString(2));
                dbSavedChat.setTimestamp(Long.parseLong(cursor.getString(3)));
                dbSavedChat.setType(cursor.getString(4));
                dbSavedChat.setCustProType(cursor.getString(5));


                dbSavedChats.add(dbSavedChat);
            }while (cursor.moveToNext());
        }

        db.close();
        cursor.close();
        return dbSavedChats;
    }

    public ArrayList<ChatData> fetchData(String bookingId)
    {
        SQLiteDatabase db=getWritableDatabase();
        ArrayList<ChatData> dbSavedChats = new ArrayList<>();

        String[] columns={CHAT_CONTENT, CHAT_PROVIDER_ID, CHAT_CUSTOMER_ID,CHAT_TIMESTAMP,CHAT_TYPE,CHAT_CUST_PRO_TYPE};
        String[] nm={bookingId};
        Cursor cursor = db.query(TABLE_NAME,columns, BOOKING_ID +"=?",nm,null,null,null);
        if(cursor.moveToFirst())
        {
            do
            {
                ChatData dbSavedChat = new ChatData();
                dbSavedChat.setContent(cursor.getString(0));
                dbSavedChat.setFromID(cursor.getString(1));
                dbSavedChat.setTargetId(cursor.getString(2));
                dbSavedChat.setTimestamp(Long.parseLong(cursor.getString(3)));
                dbSavedChat.setType(cursor.getString(4));
                dbSavedChat.setCustProType(cursor.getString(5));

                dbSavedChats.add(dbSavedChat);
            }while (cursor.moveToNext());
        }

        db.close();
        cursor.close();
        return dbSavedChats;
    }

    public void deleteData(String bookinId)
    {
        SQLiteDatabase db=getWritableDatabase();
        String[] nm={bookinId};
        db.delete(TABLE_NAME,BOOKING_ID +"=?",nm);
    }
}
