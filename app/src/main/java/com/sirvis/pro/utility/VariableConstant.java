package com.sirvis.pro.utility;


import android.Manifest;
import android.os.Build;

import com.amazonaws.regions.Regions;
import com.sirvis.pro.BuildConfig;

import java.util.HashMap;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>VariableConstant</h1>
 * The class that contain all varible contant field for whole app
 */

public class VariableConstant {


    /*               preference name and Parent folder in phone*/
    public static final String PREF_NAME = "Sirvis";
    public static final String PARENT_FOLDER = "Sirvis";

    /*                 Amazon Details              */
    public static String COGNITO_POOL_ID = "";
    public static String AmazoncognitoToken = "";
    public static Regions AMAZON_REGION = Regions.EU_CENTRAL_1;
    public static String BUCKET_NAME = "bucketsirvis2";
    private static final String PARENT_AMAZON_FOLDER = "Provider/";
    public static final String PROFILE_PIC = PARENT_AMAZON_FOLDER + "ProfilePics";
    public static final String DOCUMENTS = PARENT_AMAZON_FOLDER + "Documents/";
    public static final String WORK_IMAGE = PARENT_AMAZON_FOLDER + "WorkImage";
    public static final String SIGNATURE_UPLOAD = PARENT_AMAZON_FOLDER + "Signature/";
    public static final String BANK_PROOF = PARENT_AMAZON_FOLDER + "Bankproof/";
    /*                         Custom Intent Action            */
    public static final String INTENT_ACTION_REFRESH_BOOKING = "com.servicegenie.provider.refreshbooking";
    public static final String INTENT_ACTION_CANCEL_BOOKING = "com.servicegenie.provider.cancelbooking";
    public static final String INTENT_ACTION_BOOKING_UPDATED = "com.servicegenie.provider.bookingupdated";
    public static final String INTENT_ACTION_NEW_CHAT = "com.servicegenie.provider.newchat";
    public static final String INTENT_ACTION_MAKE_ONLINE = "com.servicegenie.provider.makeonline";
    public static final String INTENT_ACTION_MAKE_OFFLINE = "com.servicegenie.provider.makeoffline";
    public static final String INTENT_ACTION_PROFILE_ACTIVATION = "com.servicegenie.provider.profileactivation";
    public static final String INTENT_ACTION_GPS_OFF = "com.servicegenie.provider.gpsoff";
    public static final String INTENT_ACTION_CALL = "com.servicegenie.provider.call";

    public interface ACTION {
        String STARTFOREGROUND_ACTION = "com.servicegenie.foregroundservice.action.startforeground";
        String STOPFOREGROUND_ACTION = "com.servicegenie.foregroundservice.action.stopforeground";

        String STARTOFFLINELOCATIONSERVICE = "com.servicegenie.offlinelocationservice.action.start";
        String STOPOFFLINELOCATIONSERVICE = "com.servicegenie.offlinelocationservice.action.stop";
    }

    /*                          Constant for Every App            */

    /*                          AccountManager           */
    public static final String ACCOUNT_TYPE = BuildConfig.APPLICATION_ID;
    public static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";

    /*                                Play store link      */
    public static final String PLAY_STORE_LINK = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;

    /*                          File Names      */
    public static final String PROFILE_FILE_NAME = "Profile";
    public static final String WORK_IMAGE_FILE_NAME = "WorkImage";
    public static final String DOCUMENT_FILE_NAME = "Document";
    public static final String BANK_FILE_NAME = "Bank";

    /*                        App details    */
    public static final int USER_TYPE = 2;
    public static final String DEVICE_TYPE = "2";
    public static final String APP_VERSION = BuildConfig.VERSION_NAME;
    public static final String DEVICE_MODEL = android.os.Build.MODEL;
    public static final String DEVICE_MAKER = android.os.Build.MANUFACTURER;
    public static final String OS_VERSION = Build.VERSION.RELEASE;

    /*                    Folder Names */
    public static final String CROP_PIC_DIR = PARENT_FOLDER + "/Media/Images/CropedPics";
    public static final String PROFILE_PIC_DIR = PARENT_FOLDER + "/Media/Images/ProfilePictures";
    public static final String DOCUMENT_PIC_DIR = PARENT_FOLDER + "/Media/Images/Documents";
    public static final String SIGNATURE_PIC_DIR = PARENT_FOLDER + "/Media/Images/Signatures";
    public static final String BANK_PIC_DIR = PARENT_FOLDER + "/Media/Images/Signatures";
    public static final String SIGNUP_PROFILE_IMAGES = "SignupProfileImages";
    public static final String CHAT_IMAGES = "/Media/Images/Chat";

    /*                      Response Code*/
    public static final String RESPONSE_CODE_SUCCESS = "200";
    public static final String RESPONSE_CODE_NO_CONTENT = "204";
    public static final String RESPONSE_CODE_INTERNAL_SERVER_ERROR = "500";
    public static final String RESPONSE_CODE_NOT_FOUND = "404";
    public static final String RESPONSE_CODE_NO_ACCOUNT_FOUND = "401";
    public static final String RESPONSE_CODE_ALREADY_IN_CALL = "409";
    public static final String RESPONSE_CODE_NO_STRIPE_FOUND = "400";
    public static final String RESPONSE_CODE_TOKEN_EXPIRE = "440"; //Need to call refresh token
    public static final String RESPONSE_CODE_INVALID_TOKEN = "498"; // logout
    public static String LiveTrackBookingPid = "";

    /*                     Provider stats*/
    public static final String ONLINE_STATUS = "3";
    public static final String OFFLINE_STATUS = "4";
    public static final String TIME_OUT = "5";
    public static final String PROFLIE_ACTIVE_STATUS = "1";
    public static final String PROFILE_IN_ACTIVE_STATUS = "0";

    /*                         CATEGORIES TYPE   (Fixed and Hourly)   */
    public static final String CATEGORY_FIXED_NON_EDIT = "1";
    public static final String CATEGORY_FIXED_HOURLY_NON_EDIT = "2";
    public static final String CATEGORY_HOURLY_NON_EDIT = "3";
    public static final String CATEGORY_HOURLY_EDIT = "4";
    public static final String CATEGORY_FIXED_EDIT = "5";
    public static final String CATEGORY_FIXED_HOURLY_EDIT = "6";

    /*                        BookingStatus         */
    public static final String TIMER_STARTED = "1";
    public static final String TIMER_PAUSED = "0";
    public static final String NEW_BOOKING = "1";
    public static final String RECEIVED = "2";
    public static final String ACCEPT = "3";
    public static final String REJECT = "4";
    public static final String BOOKING_EXPIRED = "5";
    public static final String ON_THE_WAY = "6";
    public static final String ARRIVED = "7";
    public static final String JOB_TIMER_STARTED = "8";
    public static final String JOB_TIMER_COMPLETED = "9";
    public static final String JOB_COMPLETED_RAISE_INVOICE = "10";
    public static final String CANCELLED_BY_PROVIDER = "11";
    public static final String CANCELLED_BY_CUTOMER = "12";
    public static final String JOB_TIMER_INCOMPLETE = "15";
    public static final String ACTIVE_BIDS = "17";


    /*                          Request code              */
    public static final int REQUEST_CODE_GALLERY = 101;
    public static final int REQUEST_CODE_TAKE_PICTURE = 102;
    public static final int REQUEST_CODE_CROP_IMAGE = 103;
    public static final int REQUEST_CODE_CITY = 104;
    public static final int REQUEST_CODE_LOCATION = 105;
    public static final int REQUEST_CODE_SUB_CATEGORY = 106;
    public static final int REQUEST_CODE_CATEGORY_DOCUMENT = 107;
    public static final int REQUEST_CODE_ADDRESS = 108;
    public static final int REQUEST_CODE_REPEAT_MODE = 109;
    public static final int REQUEST_CODE_DAYS = 110;
    public static final int REQUEST_CODE_MONTH = 111;
    public static final int REQUEST_CODE_SLOT_DURATION = 112;
    public static final int REQUEST_CODE_GPS = 113;
    public static final int REQUEST_CODE_SERVICE_CHANGED = 114;

    public static final String[] STORAGE_CAMERA_PERMISSION = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    public static final String READ_PHONE_STATE_PERMISSION = Manifest.permission.READ_PHONE_STATE;
    //    public static final String[] SMS_PERMISSION ={Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS};
    public static final String[] CALL_PERMISSIONS = {
            "android.permission.RECORD_AUDIO", "android.permission.MODIFY_AUDIO_SETTINGS",
            "android.permission.INTERNET"
    };

    public static String LANGUAGE = "en";

    /*                         for refreshing api     */
    public static boolean IS_NETWORK_ERROR_SHOWED = false;
    public static boolean IS_APPLICATION_RUNNING = false;
    public static boolean IS_MYBOOKING_OPENED = false;
    public static boolean IS_ACCEPTEDBOOKING_OPENED = false;
    public static boolean IS_BOOKING_UPDATED = false;
    public static boolean IS_WALLET_UPDATED = false;
    public static boolean IS_BOOKING_ACCEPTED = false;
    public static boolean IS_BID_BOOKING_ACCEPTED = false;
    public static boolean IS_MY_LIST_EDITED = false;
    public static boolean IS_PROFILE_PHOTO_UPDATED = false;
    public static boolean IS_PROFILE_EDITED = false;
    public static boolean IS_ADDRESS_LIST_CHANGED = false;
    public static boolean IS_STRIPE_ADDED = false;
    public static boolean IS_SHEDULE_EDITED = false;
    public static boolean IS_DOCUMENT_UPDATED = false;
    public static boolean IS_CALLTYPE_UPDATED = false;
    public static boolean IS_TICKET_UPDATED = false;
    public static boolean IS_CARD_UPDATED = false;
    public static boolean IS_CHATTING_OPENED = false;
    public static boolean IS_CHATTING_RESUMED = false;
    public static boolean IS_IN_CALL = false;

    /*           To check mantadory document uploaded */
    private static HashMap<String, Boolean> hashMapIsMandatoryUploaded;

    public static HashMap<String, Boolean> getHashMapIsMandatoryUploaded() {
        if (hashMapIsMandatoryUploaded == null) {
            hashMapIsMandatoryUploaded = new HashMap<>();
        }
        return hashMapIsMandatoryUploaded;
    }

    /*           To check subcategory selected     */
    private static HashMap<String, Boolean> hashMapIsSubCategorySelected;

    public static HashMap<String, Boolean> getHashMapIsSubCategorySelected() {
        if (hashMapIsSubCategorySelected == null) {
            hashMapIsSubCategorySelected = new HashMap<>();
        }
        return hashMapIsSubCategorySelected;
    }
}
