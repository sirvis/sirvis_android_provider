package com.sirvis.pro.utility;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.sirvis.pro.R;

/**
 * Created by murashid on 25-Nov-17.
 */

public class DialogHelper {
    /**
     * method for showing simple alertDialog for message
     * @param context context
     * @param message message
     */
    public static void showWaringMessage(Context context, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.alert));
        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.oK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * method for showing custome alertDialog
     * @param mActivity context
     * @param title title
     * @param msg message
     * @param action action
     */
    public static void customAlertDialog(final Activity mActivity, String title, String msg, String action)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_simple_message,null);
        alertDialogBuilder.setView(view);
        TextView tvTitle= view.findViewById(R.id.tvTitle);
        TextView tvMsg= view.findViewById(R.id.tvMsg);
        TextView tvOk= view.findViewById(R.id.tvOk);

        tvTitle.setText(title);
        tvMsg.setText(msg);
        tvOk.setText(action);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = view.getWidth() / 2;
                    int cy = view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();

                }
            });
        }
        else
        {
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
            alertDialog.show();
        }

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();

                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                }
            }
        });

        ImageView ivClose  = view.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();

                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                }
            }
        });
        //alertDialog.setCancelable(false);
    }


    /**
     * method for showing custome alertDialog and closing the current activity
     * @param mActivity context
     * @param title title
     * @param msg message
     * @param action action
     */

    public static void customAlertDialogCloseActivity(final Activity mActivity, String title, String msg, String action)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_simple_message,null);
        alertDialogBuilder.setView(view);
        TextView tvTitle= view.findViewById(R.id.tvTitle);
        TextView tvMsg= view.findViewById(R.id.tvMsg);
        TextView tvOk= view.findViewById(R.id.tvOk);
        ImageView ivClose  = view.findViewById(R.id.ivClose);

        tvTitle.setText(title);
        tvMsg.setText(msg);
        tvOk.setText(action);
        ivClose.setVisibility(View.GONE);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = view.getWidth() / 2;
                    int cy = view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();

                }
            });
        }
        else
        {
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
            alertDialog.show();
        }

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();
                                mActivity.finishAfterTransition();
                            }
                        });
                        if(animHide[0].isStarted())
                        {
                            animHide[0].cancel();
                        }
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                        mActivity.finishAfterTransition();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                    mActivity.finish();
                    mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                }
            }
        });

        alertDialog.setCancelable(false);
    }

    /**
     * method for showing custome alertDialog for service info
     * @param context context
     */

    public static void customAlertDialogServiceDetails(final Context context, String title, String msg) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        final View view = LayoutInflater.from(context).inflate(R.layout.alert_dialog_service_info,null);
        alertDialogBuilder.setView(view);
        TextView tvTitle= view.findViewById(R.id.tvTitle);
        TextView tvMsg= view.findViewById(R.id.tvMsg);
        ImageView ivClose  = view.findViewById(R.id.ivClose);

        tvMsg.setText(msg);
        tvTitle.setText(title);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = view.getWidth() / 2;
                    int cy = view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();

                }
            });
        }
        else
        {
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
            alertDialog.show();
        }

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();
                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                }
            }
        });

    }


    /**
     * method for showing custome alertDialog and closing the current activity
     * @param mActivity context
     */

    public static void appUpdateMandatory(final Activity mActivity)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_app_update_mandatory,null);
        alertDialogBuilder.setView(view);
        TextView tvUpdate= view.findViewById(R.id.tvUpdate);
        final AlertDialog alertDialog = alertDialogBuilder.create();
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = view.getWidth() / 2;
                    int cy = view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();

                }
            });
        }
        else
        {
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
            alertDialog.show();
        }

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();
                                mActivity.finish();
                                mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                                openPlayStore(mActivity);

                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                        mActivity.finish();
                        mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                        openPlayStore(mActivity);
                    }
                }
                else
                {
                    alertDialog.dismiss();
                    mActivity.finish();
                    mActivity.overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
                    openPlayStore(mActivity);
                }
            }
        });

        alertDialog.setCancelable(false);
    }

    /**
     * method for showing custome alertDialog and closing the current activity
     * @param mActivity context
     */

    public static void appUpdateNonMandatory(final Activity mActivity)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_app_update_non_mandatory,null);
        alertDialogBuilder.setView(view);
        TextView tvUpdate= view.findViewById(R.id.tvUpdate);
        TextView tvLater= view.findViewById(R.id.tvLater);
        ImageView ivClose  = view.findViewById(R.id.ivClose);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        final Animator[] animHide = {null};
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            alertDialog.show();
            view.post(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    int cx = view.getWidth() / 2;
                    int cy = view.getHeight() / 2;
                    float finalRadius = (float) Math.hypot(cx, cy);
                    Animator animVisible = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
                    animHide[0] = ViewAnimationUtils.createCircularReveal(view, cx, cy, finalRadius, 0);
                    animVisible.start();

                }
            });
        }
        else
        {
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
            alertDialog.show();
        }

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();
                                openPlayStore(mActivity);
                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                        openPlayStore(mActivity);
                    }
                }
                else
                {
                    alertDialog.dismiss();
                    openPlayStore(mActivity);
                }
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();

                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                }
            }
        });


        tvLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    if ( animHide[0]!=null)
                    {
                        animHide[0].addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                alertDialog.dismiss();

                            }
                        });
                        animHide[0].start();
                    }
                    else
                    {
                        alertDialog.dismiss();
                    }
                }
                else
                {
                    alertDialog.dismiss();
                }
            }
        });
    }


   /* *//**
     * method for showing alertDialog for rating the app i.e opening the play store
     * @param mActivity context
     *//*

    public static void rateApp(final Activity mActivity, final SessionManager sessionManager)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        final View view = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_rating_app,null);
        alertDialogBuilder.setView(view);
        TextView tvNoThanks= view.findViewById(R.id.tvNoThanks);
        TextView tvRemindLater= view.findViewById(R.id.tvRemindLater);
        TextView tvRateNow  = view.findViewById(R.id.tvRateNow);

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
        alertDialog.show();

  tvNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.setDontShowRate(true);
                alertDialog.dismiss();
                openPlayStore(mActivity);
            }
        });

         tvRemindLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        tvRateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.setDontShowRate(true);
                alertDialog.dismiss();
                openPlayStore(mActivity);
            }
        });



    }*/

    /**
     * method for showing alertDialog for rating the app i.e opening the play store
     * @param mActivity context
     */

    public static void rateApp(final Activity mActivity, final SessionManager sessionManager)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        //alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(mActivity.getString(R.string.headRatingApp));
        alertDialog.setMessage(mActivity.getString(R.string.msgRatingApp));

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, mActivity.getString(R.string.noThanks), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                sessionManager.setDontShowRate(true);
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, mActivity.getString(R.string.remindLater), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, mActivity.getString(R.string.rateNow), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                sessionManager.setDontShowRate(true);
                openPlayStore(mActivity);
            }
        });

        alertDialog.show();
    }

    private static void openPlayStore(Activity mActivity)
    {
        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
        i.setData(Uri.parse(VariableConstant.PLAY_STORE_LINK));
        mActivity.startActivity(i);
    }

    /**
     * custom method to show alert dialog
     * mContext: reference of calling activity
     */
    public static void aDialogOnPermissionDenied(final Context mContext)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mContext.getResources().getString(R.string.alert));
        alertDialogBuilder.setMessage(mContext.getResources().getString(R.string.reGrantPermissionMsg));
        alertDialogBuilder.setPositiveButton(mContext.getResources().getString(R.string.appSetting),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        Uri uri = Uri.fromParts("package", mContext.getPackageName(), null);
                        Intent settingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, uri);
                        settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(settingsIntent);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    /******************************************************/

    public static AlertDialog adEnableGPS(final Activity mActivity)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
        alertDialogBuilder.setTitle(mActivity.getString(R.string.enableGPS));
        alertDialogBuilder.setMessage(mActivity.getString(R.string.plzEnableGPS));

        alertDialogBuilder.setPositiveButton(mActivity.getResources().getString(R.string.setting),
                new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mActivity.startActivity(callGPSSettingIntent);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(false);
        return alertDialog;
    }
/*Yogesh
    5/7/2019
    returns a dialog with a action button*/
    public static Dialog showPopupWithOneButton(Context context)
    {
        final Dialog dialog= new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_dialog_with_1button);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
        TextView title_popup= (TextView) dialog.findViewById(R.id.title_popup);
        TextView text_for_popup= (TextView) dialog.findViewById(R.id.text_for_popup);
        TextView yes_button= (TextView) dialog.findViewById(R.id.yes_button);
        yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        return dialog;
    }
    interface DialongOkClickListener
    {
        void onClick();
    }
}
