package com.sirvis.pro.utility;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.landing.splash.SplashActivity;
import com.sirvis.pro.service.LocationPublishService;
import com.stripe.android.model.Card;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>Utility</h1>
 * The class contain the methods thats used more than one times in the whole app
 */

public class Utility {
    private static final String TAG = "Utility";
    private static long lastRefreshTime = 0;

    /**
     * method for printing the log
     *
     * @param TAG tag
     * @param log log message
     */
    public static void printLog(String TAG, String log) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, log);
        }

    }

    public static void setAlarm(Context context, String bookingEndTime) {
        Intent intent = new Intent("com.sirvis.pro.alarmForTelecalls");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis((Long.parseLong(bookingEndTime) * 1000L) - 45 *60000);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 1000 * 60, pendingIntent);
    }


    /**
     * method for create Light font Typeface from assets folder
     *
     * @param context context
     * @return light Typeface
     */
    public static Typeface getFontLight(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.light.ttf");
    }

    /**
     * method for create Regular font Typeface from assets folder
     *
     * @param context context
     * @return Regular Typeface
     */
    public static Typeface getFontRegular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.regular.ttf");
    }

    /**
     * method for create Bold font Typeface from assets folder
     *
     * @param context context
     * @return Bold Typeface
     */
    public static Typeface getFontBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.semibold.ttf");
    }

    /**
     * method for create Medium font Typeface from assets folder
     *
     * @param context context
     * @return Medium Typeface
     */
    public static Typeface getFontMedium(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/hind.medium.ttf");
    }


    /**
     * method for geting the device id
     *
     * @param context context
     * @return device id
     */
    public static String getDeviceId(Context context) {
        try {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "1234567890";
            } else {
                if (mTelephony.getDeviceId() != null) {

                    return mTelephony.getDeviceId();
                } else {
                    return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "12346666";
        }
    }

    /**
     * method for get location
     *
     * @param context context
     * @return latitude and longitude
     */
    public static double[] getLocation(Context context) {
        double[] latLng = new double[2];
        LocationManager mLocationManager;
        Location location = null;
        try {
            mLocationManager = (LocationManager) context.getSystemService(Service.LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            latLng[0] = 0.0;
            latLng[1] = 0.0;

            if (isGPSEnabled && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.d("Utility", "Gps Enabled");


                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    latLng[0] = location.getLatitude();
                    latLng[1] = location.getLongitude();
                }
                Log.i("Utility", "Gps 1  latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);

                if (latLng[0] == 0.0) {
                    List<String> providers = mLocationManager.getProviders(true);
                    for (int i = providers.size() - 1; i >= 0; i--) {
                        Log.d(TAG, "getLocation: " + providers.get(i));
                        location = mLocationManager.getLastKnownLocation(providers.get(i));
                        if (location != null) break;
                    }

                    if (location != null) {
                        latLng[0] = location.getLatitude();
                        latLng[1] = location.getLongitude();
                    }

                }
                Log.i("Utility", "Gps 2  latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);
            } else {
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latLng[0] = location.getLatitude();
                    latLng[1] = location.getLongitude();
                }
                Log.i("Utility", "Network  latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Utility", "getAddress: " + e);
        }
        Log.i("Utility", "latLng[0]: " + latLng[0] + "  latLng[1]: " + latLng[1]);
        return latLng;
    }
    /******************************************************/

    /**
     * method for creating circle bitmap and normal bitmap
     *
     * @param bitmap input values
     * @return rounded bitmap
     */
    public static Bitmap getCircleCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    /**
     * method for returing current time
     *
     * @return current time
     */
    public static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance(Locale.US);
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return formater.format(date);
    }


    public static String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "'st";
            case 2:
                return "'nd";
            case 3:
                return "'rd";
            default:
                return "'th";
        }
    }

    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToServerFormat(String time) {
        long timestamp = Long.parseLong(time) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }

    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToServerFormat(String time, String format) {
        long timestamp = Long.parseLong(time) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }


    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToDateFormat(String time) {
        long timestamp = Long.parseLong(time) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }

    public static String convertUTCToTodayDateFormat(String time) {
        long timestamp = Long.parseLong(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }


    /**
     * method for conveting utctime to custom time format
     *
     * @param time utc time
     * @return custom time format
     */
    public static String convertUTCToYesterdayDateFormat(String time) {
        long timestamp = Long.parseLong(time);
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        sdf.setTimeZone(AppController.getInstance().getTimeZone());
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp);
        //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        cal.add(Calendar.DATE, -1);
        Date currenTimeZone = cal.getTime();
        return sdf.format(currenTimeZone);
    }


    /**
     * method for converting utc time to timeStamp
     *
     * @param time utc time
     * @return timestamp
     */
    public static long convertUTCToTimeStamp(String time) {

        try {
            long timestamp = Long.parseLong(time) * 1000;
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timestamp);
            //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            cal.setTimeZone(AppController.getInstance().getTimeZone());

            return cal.getTimeInMillis();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }


    /**
     * method for returing custome date format
     *
     * @param n         days in month
     * @param monthyear month of the year
     * @return custom date format
     */
    public static String getDayOfMonthSuffix(int n, String monthyear) {
        if (n >= 11 && n <= 13) {
            return n + "th " + monthyear;
        }
        switch (n % 10) {
            case 1:
                return n + "st " + monthyear;
            case 2:
                return n + "nd " + monthyear;
            case 3:
                return n + "rd " + monthyear;
            default:
                return n + "th " + monthyear;
        }
    }

    /**
     * custom method to check internet connectivity
     *
     * @param context context
     * @return boolean: value base on the connectivity
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }
    /*****************************************************/

    /**
     * custom method to check particular is Service running or not
     *
     * @param context context
     * @return boolean: value base on the Running
     */
    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * custom method to check Application is background or foreground
     *
     * @param context context
     * @return boolean: value based on the Running
     */
    public static boolean isApplicationSentToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        //If your app is the process in foreground, then it's not in running in background
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * custom method to check Gps connectivity
     *
     * @param context context
     * @return boolean: value base on the connectivity
     */
    public static boolean isGpsEnabled(Context context) {
        LocationManager service = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = service.isProviderEnabled(LocationManager.GPS_PROVIDER) && service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return service.isProviderEnabled(LocationManager.GPS_PROVIDER) && service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Custom method for clearing session and stop the service if its running during logout and session expired
     *
     * @param sessionManager session manager
     * @param context        context
     */
    public static void logoutSessionExiperd(SessionManager sessionManager, Context context) {
        if (Utility.isMyServiceRunning(context, LocationPublishService.class)) {
            Intent stopIntent = new Intent(context, LocationPublishService.class);
            stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
            context.startService(stopIntent);
            AppController.getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
        }
        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(/*"/topics/" +*/ sessionManager.getFCMTopic());

        } catch (Exception e) {
            e.printStackTrace();
        }
        sessionManager.clearSharedPredf();
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * Custom method for clearing session and stop the service if its running during logout and session expired from fcm message
     *
     * @param sessionManager session manager
     * @param context        context
     */
    public static void logoutSessionExiperd(SessionManager sessionManager, Context context, String msg) {
        if (Utility.isMyServiceRunning(context, LocationPublishService.class)) {
            Intent stopIntent = new Intent(context, LocationPublishService.class);
            stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
            context.startService(stopIntent);
            AppController.getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
        }
        String unsubcribeTopic = sessionManager.getFCMTopic();
        sessionManager.clearSharedPredf();
        Intent intent = new Intent(context, SplashActivity.class);
        intent.putExtra("msg", msg);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(/*"/topics/" + */unsubcribeTopic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * method for returning the price
     *
     * @param tempPrice input price
     * @return formated price
     */
    public static String getFormattedPrice(String tempPrice) {
        String price = "";
        Double dPrice = 0.00;
        if (tempPrice != null && !"".equals(tempPrice) && !"0".equals(tempPrice) && !"00".equals(tempPrice)
                && !"0.00".equals(tempPrice) && !"0.0".equals(tempPrice) && !".00".equals(tempPrice) && !"NaN".equals(tempPrice) && !"NA".equals(tempPrice)) {
            DecimalFormat priceFormat = new DecimalFormat("#.00");
            dPrice = Double.parseDouble(tempPrice);
        }

        NumberFormat nf_out = NumberFormat.getInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setMinimumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        price = nf_out.format(dPrice);

        return price;
    }

    /**
     * @param tempPrice      price
     * @param currencySymbol currency symbol
     * @param currencyAbbr   1 => prefix 2 => suffix
     * @return
     */
    public static String getPrice(String tempPrice, String currencySymbol, String currencyAbbr) {
        String price = getFormattedPrice(tempPrice);
        if (currencyAbbr.equals("1")) {
            price = currencySymbol + " " + price;
        } else {
            price = price + " " + currencySymbol;
        }
        return price;
    }

    public static String roundString(String text, int roundTodecimalPlace) {
        if (text.matches("\\d*\\.?\\d+")) {
            BigDecimal b = new BigDecimal(text);
            b = b.setScale(roundTodecimalPlace, BigDecimal.ROUND_HALF_UP);
            return String.valueOf(b);
        }
        return "0.0";
    }

    /**
     * method for geting the Unix time
     *
     * @return unix time
     */
    public static int GetUnixTime() {
        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();
        int utc = (int) (now / 1000);
        return (utc);
    }

    /**
     * method for copty stream from inputstream to outputstream
     *
     * @param input  inpurstream
     * @param output outputstream
     * @throws IOException IO exeption
     */
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    /**
     * method for getting the youtubeID from url
     *
     * @param url url of the youtube video
     * @return youtube id
     */
    public static String getYoutubeIdFromUrl(String url) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(url);

        if (matcher.find()) {
            return matcher.group();
        }
        return "";
    }

    /**
     * method for creating bitmap from vector drawable file
     *
     * @param context    context
     * @param drawableId vector drawable file
     * @return bitmap
     */
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static Bitmap createDrawableFromView(Context context, View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (context != null) {
            if (context.getApplicationContext() != null) {
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            }
        }
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public static void saveImage(Bitmap finalBitmap, File file) {
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Method for calling api for update the bookingAck
     *
     * @param sessionToken session token
     * @param bookingid    booking id
     * @param lat          latitude
     * @param lng          longitude
     */
    public static void updateBookingAck(final String sessionToken, final String bookingid, final String lat, final String lng) {
        if (lastRefreshTime + 700 < System.currentTimeMillis()) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("bookingId", bookingid);
                jsonObject.put("latitude", lat);
                jsonObject.put("longitude", lng);
            } catch (Exception e) {
                e.printStackTrace();
            }
            OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_ACK, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                @Override
                public void onSuccess(String statusCode, String result) {
                    try {
                        if (result != null) {
                            Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                            JSONObject jsonObjectResponse = new JSONObject(result);
                            if (statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE)) {
                                RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        updateBookingAck(sessionToken, bookingid, lat, lng);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {
                                    }

                                    @Override
                                    public void sessionExpired(String msg) {

                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String error) {
                }
            });

        }
        lastRefreshTime = System.currentTimeMillis();
    }


    public static void hideKeyboad(Activity activity) {
        // Check if no view has focus:
        try {
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double distanceInMeter(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist * 1000);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static void checkAndShowNetworkError(Context context) {
        if (!isNetworkAvailable(context) && !VariableConstant.IS_NETWORK_ERROR_SHOWED) {
            Intent intent = new Intent(context, NetworkErrorActivity.class);
            context.startActivity(intent);
        }
    }

    public static boolean isLatestVersion(String latestVersion) {
        String s1 = normalisedVersion(VariableConstant.APP_VERSION);
        String s2 = normalisedVersion(latestVersion);
        return 0 > s1.compareTo(s2);
    }

    private static String normalisedVersion(String version) {
        String[] split = Pattern.compile(".", Pattern.LITERAL).split(version);
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            sb.append(String.format("%" + 10 + 's', s));
        }

        return sb.toString();
    }

    public static void showKeyBoard(final Context context, final EditText editText) {
        try {
            editText.postDelayed(new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.showSoftInput(editText, 0);
                }
            }, 50);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCurrentCountryCode(Context context) {
        String locale = "IN";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0).getCountry();
        } else {
            locale = context.getResources().getConfiguration().locale.getCountry();
        }

        try {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            String countryCodeValue = tm.getNetworkCountryIso();

            if (countryCodeValue != null && !countryCodeValue.equals("")) {
                return countryCodeValue.toUpperCase();
            } else {
                return locale;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return locale;
        }
    }

    public static String converArrayListToStringComma(ArrayList<String> stringArrayList) {
        String value = "", prefix = "";
        for (String va : stringArrayList) {
            value = value + prefix + va;
            prefix = ",";
        }
        return value;
    }

    public static void setSpannableString(Context context, TextView txtSpan, String firstString, String lastString) {
        String totalString = firstString + " " + lastString;
        ForegroundColorSpan normalTextColor = new ForegroundColorSpan(context.getResources().getColor(R.color.normalTextColor));
        ForegroundColorSpan colorPrimary = new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary));
        Spannable spanText = new SpannableString(totalString);
        spanText.setSpan(normalTextColor, 0, firstString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanText.setSpan(colorPrimary, firstString.length() + 1, totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpan.setText(spanText);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static int getCardBrand(String brand) {
        switch (brand) {
            case Card.AMERICAN_EXPRESS:
                return com.stripe.android.R.drawable.ic_amex_template_32;

            case Card.DINERS_CLUB:
                return com.stripe.android.R.drawable.ic_diners_template_32;

            case Card.DISCOVER:
                return com.stripe.android.R.drawable.ic_discover_template_32;

            case Card.JCB:
                return com.stripe.android.R.drawable.ic_jcb_template_32;

            case Card.MASTERCARD:
                return com.stripe.android.R.drawable.ic_mastercard_template_32;

            case Card.VISA:
                return com.stripe.android.R.drawable.ic_visa_template_32;

            case Card.UNKNOWN:
                return com.stripe.android.R.drawable.ic_unknown;

            default:
                return com.stripe.android.R.drawable.ic_unknown;
        }
    }

    public static String converSecondToHourMinute(String totalActualJobTimeMinutes) {
        try {
            double t = Double.parseDouble(totalActualJobTimeMinutes);
            int hours = (int) (t / 60); //since both are ints, you get an int
            int minutes = (int) (t % 60);
            return String.format("%02d", hours) + " hrs : " + String.format("%02d", minutes) + " mins";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0 hrs 0 mins";
    }

}
