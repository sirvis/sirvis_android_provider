package com.sirvis.pro.utility;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkConnectivityObserver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent arg1) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm !=null)
        {
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

            Log.d("mura", "onReceive: "+isConnected);

            if(!isConnected && !VariableConstant.IS_NETWORK_ERROR_SHOWED && VariableConstant.IS_APPLICATION_RUNNING)
            {
                Intent intentNetworkError = new Intent(context,NetworkErrorActivity.class);
                intentNetworkError.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intentNetworkError);
            }

        }
    }
}