package com.sirvis.pro.utility;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by murashid on 16-Oct-17.
 * <h1>RefreshToken</h1>
 * wrapper class for refreshing token
 */

public class RefreshToken {
    private static String TAG = "RefreshToken";

    /**
     * method for calling api for  refresh token
     * @param token date token
     * @param imple RefreshTokenImple success , failure callback
     */
    public static void onRefreshToken(final String token, final RefreshTokenImple imple)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.REFRESH_TOKEN , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            imple.onSuccessRefreshToken(jsonObject.getString("data"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            imple.sessionExpired(jsonObject.getString("message"));
                        }
                        else
                        {
                            imple.onFailureRefreshToken();
                        }
                    }
                    else
                    {
                        imple.onFailureRefreshToken();
                    }
                }
                catch (Exception e)
                {
                    imple.onFailureRefreshToken();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                imple.onFailureRefreshToken();
            }
        });
    }

    /**
     * RefreshTokenImple callback for failure and succes refresh token
     */
    public interface RefreshTokenImple {
        void onSuccessRefreshToken(String newToken);
        void onFailureRefreshToken();
        void sessionExpired(String msg);
    }
}
