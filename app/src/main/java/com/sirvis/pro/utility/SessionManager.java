package com.sirvis.pro.utility;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>SessionManager</h1>
 * SinleTon shared preference class used for storing required fields for whole app
 */

public class SessionManager
{
    private static SessionManager sessionManager;
    private static SharedPreferences sharedPreferences = null;
    private static SharedPreferences.Editor editor = null;

    public static SessionManager getSessionManager(Context context) {
        if (sessionManager == null) {
            sessionManager = new SessionManager(context);

        }
        return sessionManager;
    }

    public SessionManager(Context context) {
        sharedPreferences = getSharedPref(context);
        editor = getEditor();
    }

    private static SharedPreferences getSharedPref(Context context) {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(VariableConstant.PREF_NAME, Context.MODE_MULTI_PROCESS);

        }
        return sharedPreferences;
    }

    private static SharedPreferences.Editor getEditor() {
        if (editor == null) {
            editor = sharedPreferences.edit();
            editor.commit();
        }
        return editor;
    }

    public void clearSharedPredf() {
        String userName = getUserName();
        String password = getPassword();
        boolean isRemembered = getIsRemembered();
        editor.clear();
        editor.apply();
        setUserName(userName);
        setPassword(password);
        setIsRemembered(isRemembered);
        editor.apply();
    }

    public void setPushToken(String pushToken)
    {
        editor.putString("PUSH_TOKEN",pushToken);
        editor.apply();
    }

    public String getPushToken()
    {
        return sharedPreferences.getString("PUSH_TOKEN","");
    }

    public void setLanguageList(String languageList)
    {
        editor.putString("LanguageList",languageList);
        editor.apply();
    }

    public String getLanguageList()
    {
        return sharedPreferences.getString("LanguageList","");
    }

    public void setLanguageCode(String languageCode)
    {
        editor.putString("LanguageCode",languageCode);
        editor.apply();
    }

    public String getLanguageCode()
    {
        return sharedPreferences.getString("LanguageCode","en");
    }

    public void setUserName(String userName)
    {
        editor.putString("USER_NAME",userName);
        editor.apply();
    }

    public String getUserName()
    {
        return sharedPreferences.getString("USER_NAME","");
    }

    public void setIsRemembered(boolean isRemembered)
    {
        editor.putBoolean("IsRemembered",isRemembered);
        editor.apply();
    }

    public boolean getIsRemembered()
    {
        return sharedPreferences.getBoolean("IsRemembered",false);
    }


    public void setPassword(String password)
    {
        editor.putString("PASSWORD",password);
        editor.apply();
    }

    public String getPassword()
    {
        return sharedPreferences.getString("PASSWORD","");
    }

    public String getFCMTopic() {
        return sharedPreferences.getString("PUSH_TOPIC", "");
    }

    public void setFCMTopic(String sessionToken) {
        editor.putString("PUSH_TOPIC", sessionToken);
        editor.commit();
    }

    public void setProviderId(String id)
    {
        editor.putString("PROVIDER_ID",id);
        editor.apply();
    }

    public String getProviderId()
    {
        return sharedPreferences.getString("PROVIDER_ID","");
    }


    public void setIsDriverLogin(boolean isDriverLogin)
    {
        editor.putBoolean("IS_DRIVER_LOGIN",isDriverLogin);
        editor.apply();
    }

    public boolean getIsDriverLogin()
    {
        return sharedPreferences.getBoolean("IS_DRIVER_LOGIN",false);
    }

    public void setFirstName(String nmae)
    {
        editor.putString("FIRST_NAME",nmae);
        editor.apply();
    }

    public String getFirstName()
    {
        return sharedPreferences.getString("FIRST_NAME","");
    }

    public void setLastName(String nmae)
    {
        editor.putString("LAST_NAME",nmae);
        editor.apply();
    }

    public String getLastName()
    {
        return sharedPreferences.getString("LAST_NAME","");
    }

    public void setProfilePic(String profilePic)
    {
        editor.putString("PROFILE_PIC",profilePic);
        editor.apply();
    }

    public String getProfilePic()
    {
        return sharedPreferences.getString("PROFILE_PIC","");
    }

    public void setEmail(String email)
    {
        editor.putString("EMAIL",email);
        editor.apply();
    }

    public String getEmail()
    {
        return sharedPreferences.getString("EMAIL","");
    }

    public void setPhoneNumber(String phoneNumber)
    {
        editor.putString("PHONE_NUMBER",phoneNumber);
        editor.apply();
    }

    public String getPhoneNumber()
    {
        return sharedPreferences.getString("PHONE_NUMBER","");
    }


    public void setGender(String phoneNumber)
    {
        editor.putString("Gender",phoneNumber);
        editor.apply();
    }

    public String getGender()
    {
        return sharedPreferences.getString("Gender","");
    }


    public void setCountryCode(String countryCode)
    {
        editor.putString("COUNTRY_CODE",countryCode);
        editor.apply();
    }

    public String getCountryCode()
    {
        return sharedPreferences.getString("COUNTRY_CODE","");
    }


    public void setDob(String countryCode)
    {
        editor.putString("DATE_OF_BIRTH",countryCode);
        editor.apply();
    }

    public String getDob()
    {
        return sharedPreferences.getString("DATE_OF_BIRTH","");
    }

    public void setIsBidBooking(boolean isBidBooking) {
        editor.putBoolean("IsBidBooking",isBidBooking);
        editor.apply();
    }

    public String getWalletAmount()
    {
        return sharedPreferences.getString("WalletAmount","0");
    }

     public void setWalletAmount(String walletAmount) {
        editor.putString("WalletAmount",walletAmount);
        editor.apply();
    }

    public boolean getIsBidBooking()
    {
        return sharedPreferences.getBoolean("IsBidBooking",false);
    }

    public void setDistanceUnit(String matrix) {

        if(matrix.equals("0"))
        {
            editor.putString("DistanceUnit","Km");
            editor.putFloat("DistanceUnitConverter",0.001f);
        }
        else
        {
            editor.putString("DistanceUnit","Miles");
            editor.putFloat("DistanceUnitConverter",0.000621f);
        }
        editor.apply();
    }

    public String getDistanceUnit()
    {
        return sharedPreferences.getString("DistanceUnit","Km");
    }

    public float getDistanceUnitConverter()
    {
        return sharedPreferences.getFloat("DistanceUnitConverter",0);
    }

    public void setCurrencySymbol(String currencySymbol)
    {
        editor.putString("CurrencySymbol",currencySymbol);
        editor.apply();
    }

    public String getCurrencySymbol()
    {
        return sharedPreferences.getString("CurrencySymbol","");
    }

    public String getCurrency() {
        return sharedPreferences.getString("Currency","");
    }

    public void setCurrency(String currency) {
        editor.putString("Currency",currency);
        editor.apply();
    }

    public String getCurrencyAbbrevation() {
        return sharedPreferences.getString("CurrencyAbbrevation","1");
    }

    public void setCurrencyAbbrevation(String currencyAbbrevation) {
        editor.putString("CurrencyAbbrevation",currencyAbbrevation);
        editor.apply();
    }

    public void setWalletEnable(String enable)
    {
        editor.putString("WalletEnable",enable);
        editor.apply();
    }

    public String getWalletEnable()
    {
        return sharedPreferences.getString("WalletEnable","false");
    }

    public void setReferalCode(String referalCode)
    {
        editor.putString("REFERAL_CODE",referalCode);
        editor.apply();
    }

    public String getReferalCode()
    {
        return sharedPreferences.getString("REFERAL_CODE","");
    }

    public void setIsNewBooking(boolean b)
    {
        editor.putBoolean("IsNewBooking",b);
        editor.apply();
    }

    public boolean getIsNewBooking()
    {
        return sharedPreferences.getBoolean("IsNewBooking",false);
    }

    public void setDeviceId(String deviceId) {
        editor.putString("DEVICE_ID",deviceId);
        editor.apply();
    }

    public String getDeviceId()
    {
        return sharedPreferences.getString("DEVICE_ID","");
    }

    public void setZendeskRequesterId(String deviceId) {
        editor.putString("ZENDESK_ID",deviceId);
        editor.apply();
    }

    public String getZendeskRequesterId()
    {
        return sharedPreferences.getString("ZENDESK_ID","");
    }



    public void setCurrentLat(String currentLat) {
        editor.putString("CURRENT_LAT",currentLat);
        editor.apply();
    }

    public String getCurrentLat()
    {
        return sharedPreferences.getString("CURRENT_LAT","0.0");
    }

    public void setCurrentLng(String currentLng) {
        editor.putString("CURRENT_LNG",currentLng);
        editor.apply();
    }

    public String getCurrentLng()
    {
        return sharedPreferences.getString("CURRENT_LNG","0.0");
    }


    public void setLastBooking(String lastBooking) {
        editor.putString("LAST_BOOKING",lastBooking);
        editor.apply();
    }

    public String getLastBooking()
    {
        return sharedPreferences.getString("LAST_BOOKING","");
    }

    public void setLastTimeStampMsg(long lastBookingId) {
        editor.putLong("LastTimeStampMsg",lastBookingId);
        editor.apply();
    }

    public long getLastTimeStampMsg()
    {
        return sharedPreferences.getLong("LastTimeStampMsg",0);
    }

 /*   public void setIsAssignedBooking(boolean isAssignedBooking) {
        editor.putBoolean("IsAssignedBooking",isAssignedBooking);
        editor.apply();
    }

    public boolean getIsAssignedBooking()
    {
        return sharedPreferences.getBoolean("IsAssignedBooking",false);
    }*/

    public void setLastBookingIdCancel(String lastBookingId) {
        editor.putString("LastBookingIdCancel",lastBookingId);
        editor.apply();
    }

    public String getLastBookingIdCancel()
    {
        return sharedPreferences.getString("LastBookingIdCancel","0");
    }

    public void setIsNowBooking(boolean isNowBooking)
    {
        editor.putBoolean("IS_NOW_BOOKING",isNowBooking);
        editor.apply();
    }

    public boolean getIsNowBooking()
    {
        return sharedPreferences.getBoolean("IS_NOW_BOOKING",false);
    }

    public void setIsDriverOnline(boolean isDriverLogin)
    {
        editor.putBoolean("IS_DRIVER_ONLINE",isDriverLogin);
        editor.apply();
    }

    public boolean getIsDriverOnline()
    {
        return sharedPreferences.getBoolean("IS_DRIVER_ONLINE",false);
    }

    public void setIsDriverDeactive(boolean isDriverLogin)
    {
        editor.putBoolean("IsDriverDeactive",isDriverLogin);
        editor.apply();
    }

    public boolean getIsDriverDeactive()
    {
        return sharedPreferences.getBoolean("IsDriverDeactive",false);
    }

    public void setIsProfileAcivated(boolean isDriverLogin)
    {
        editor.putBoolean("IsProfileAcivated",isDriverLogin);
        editor.apply();
    }

    public boolean getIsProfileAcivated()
    {
        return sharedPreferences.getBoolean("IsProfileAcivated",true);
    }


    public void setIsDriverOnJob(boolean isDriverOnJob)
    {
        editor.putBoolean("IS_DRIVER_ON_JOB",isDriverOnJob);
        editor.apply();
    }

    public boolean getIsDriverOnJob()
    {
        return sharedPreferences.getBoolean("IS_DRIVER_ON_JOB",false);
    }

    /*           App Config     */


    public void setLatLongDisplacement(String latLongDisplacement) {
        int unit = 10;
        if(latLongDisplacement.matches("[0-9]+"))
        {
            unit = Integer.parseInt(latLongDisplacement);
        }
        editor.putInt("LAT_LONG_DISPLACEMENT",unit);
        editor.apply();
    }

    public int getLatLongDisplacement()
    {
        return sharedPreferences.getInt("LAT_LONG_DISPLACEMENT",10);
    }

    public void setLocationPublishInterval(String interval)
    {
        editor.putString("LocationPublishInterval",interval);
        editor.apply();
    }

    public String getLocationPublishInterval()
    {
        return sharedPreferences.getString("LocationPublishInterval","10");
    }

    public void setLiveTrackInterval(String interval)
    {
        editor.putString("LiveTrackInterval",interval);
        editor.apply();
    }

    public String getLiveTrackInterval()
    {
        return sharedPreferences.getString("LiveTrackInterval","10");
    }

    public void setProTimeOut(String timeOut)
    {
        editor.putString("ProTimeOut",timeOut);
        editor.apply();
    }

    public String getProTimeOut()
    {
        return sharedPreferences.getString("ProTimeOut","50");
    }


    public long getElapsedTime()
    {
        return sharedPreferences.getLong("TIME_ELAPSED",0);
    }

    public void setElapsedTime(long time)
    {
        editor.putLong("TIME_ELAPSED",time);
        editor.commit();
    }

    public long getTimeWhilePaused()
    {
        return sharedPreferences.getLong("TIME_WHILE_PAUSED",0);
    }

    public void setTimeWhilePaused(long time)
    {
        editor.putLong("TIME_WHILE_PAUSED",time);
        editor.commit();
    }


    public String getDocumentID() {
        return sharedPreferences.getString("COUCH_DOCUMENT_ID", "");
    }

    public void setDocumentID(String id) {
        editor.putString("COUCH_DOCUMENT_ID", id);
        editor.commit();
    }

    public String getBookingStr() {
        return sharedPreferences.getString("BOOKING_STR", "");
    }

    public void setisTeleCallRunning(boolean isTeleCallrunning) {
        editor.putBoolean("IS_TELECALL_RUNNING", isTeleCallrunning);
        editor.commit();
    }

    public String getIsCallNotResponded() {
        return sharedPreferences.getString("CALL_RESPONDED", "");
    }

    public void setIsCallNotResponded(String callId) {
        editor.putString("CALL_RESPONDED", callId);
        editor.commit();
    }


    public boolean getIsTeleCallRunning() {
        return sharedPreferences.getBoolean("IS_TELECALL_RUNNING", false);
    }

    public void setBookingStr(String bookingStr) {
        editor.putString("BOOKING_STR", bookingStr);
        editor.commit();
    }


    public String getChatCustomerName() {
        return sharedPreferences.getString("ChatCustomerName", "");
    }

    public void setChatCustomerName(String bookingStr) {
        editor.putString("ChatCustomerName", bookingStr);
        editor.commit();
    }

    public String getChatCustomerPic() {
        return sharedPreferences.getString("ChatCustomerPic", "");
    }

    public void setChatCustomerPic(String pic) {
        editor.putString("ChatCustomerPic", pic);
        editor.commit();
    }


    public String getChatCustomerID() {
        return sharedPreferences.getString("ChatCustomerID", "");
    }

    public void setChatCustomerID(String bookingStr) {
        editor.putString("ChatCustomerID", bookingStr);
        editor.commit();
    }


    public String getChatBookingID() {
        return sharedPreferences.getString("ChatBookingID", "");
    }

    public void setChatBookingID(String bookingStr) {
        editor.putString("ChatBookingID", bookingStr);
        editor.commit();
    }


    public int getChatNotificationId() {
        return sharedPreferences.getInt("ChatNotificationId", 0);
    }

    public void setChatNotificationId(int notificationId) {
        editor.putInt("ChatNotificationId", notificationId);
        editor.commit();
    }

    public int getChatCount(String bookingId) {
        return sharedPreferences.getInt("ChatCount"+bookingId, 0);
    }

    public void setChatCount(String bookingId,int chatCount) {
        editor.putInt("ChatCount"+bookingId, chatCount);
        editor.commit();
    }

    public void clearChatCountPreference(String bookigID)
    {
        editor.remove("ChatCount"+bookigID).commit();
    }

    public void setMyBooking(String myEventsData)
    {
        editor.putString("MyBooking",myEventsData);
        editor.commit();
    }

    public String getMyBooking()
    {
        return sharedPreferences.getString("MyBooking","");
    }

     public void setAcceptedBooking(String acceptedBooking)
    {
        editor.putString("AcceptedBooking",acceptedBooking);
        editor.commit();
    }

    public String getAcceptedBooking()
    {
        return sharedPreferences.getString("AcceptedBooking","");
    }

    public void setHistoryWeekData(String data)
    {
        editor.putString("HistoryWeekData",data);
        editor.commit();
    }

    public String getHistoryWeekData()
    {
        return sharedPreferences.getString("HistoryWeekData","");
    }

    public void setHistoryData(String data)
    {
        editor.putString("HistoryData",data);
        editor.commit();
    }

    public String getHistoryData()
    {
        return sharedPreferences.getString("HistoryData","");
    }

    public void setScheduleData(String data)
    {
        editor.putString("ScheduleData",data);
        editor.commit();
    }

    public String getScheduleData()
    {
        return sharedPreferences.getString("ScheduleData","");
    }

    public void setChatCustomerList(String data)
    {
        editor.putString("ChatCustomerList",data);
        editor.commit();
    }

    public String getChatCustomerList()
    {
        return sharedPreferences.getString("ChatCustomerList","");
    }

    public void setIsMyDocumentPresent(int data)
    {
        editor.putInt("IsMyDocumentPresent",data);
        editor.commit();
    }

    //0 => not yet called , 1 => present , 2 => not present
    public int getIsMyDocumentPresent()
    {
        return sharedPreferences.getInt("IsMyDocumentPresent",0);
    }

    public void setArrivedDistance(String bookingId, String distance) {
        editor.putString("ArrivedDistance"+bookingId, distance);
        editor.commit();
    }

    public double getArrivedDistance(String bookingId) {
        String distancestr = sharedPreferences.getString("ArrivedDistance"+bookingId, "0.0");
        double distanceDouble = 0.0;
        try {
            distanceDouble = Double.parseDouble(distancestr);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return distanceDouble;
    }


    public void setOnJobDistance(String bookingId, String distance) {
        editor.putString("OnJobDistance"+bookingId, distance);
        editor.commit();
    }

    public double getOnJobDistance(String bookingId) {
        String distancestr = sharedPreferences.getString("OnJobDistance"+bookingId, "0.0");
        double distanceDouble = 0.0;
        try {
            distanceDouble = Double.parseDouble(distancestr);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return distanceDouble;
    }

    public void clearDistance(String bookigID)
    {
        editor.remove("ArrivedDistance"+bookigID).commit();
        editor.remove("OnJobDistance"+bookigID).commit();
    }

    /**
     * This method is used to store the stripeKey
     * @param stripeKey, containing stripeKey.
     */
    public void setStripeKey(String stripeKey)
    {
        editor.putString("stripeKey", stripeKey);
        editor.commit();
    }


    /**
     * This method is used to getting the stripeKey.
     * @return, returns stripeKey.
     */
    public String getStripeKey()
    {
        return sharedPreferences.getString("stripeKey", "");
    }

    /**
     * This method is used to getting the LastCard ID.
     * @return, returns LastCard ID.
     */
    public String getSelectedCardId()
    {
        return sharedPreferences.getString("SelectedCardId", "");
    }

    /**
     * This method is used to store the LastCard id.
     * @param LastCard, containing lastCard id.
     */
    public void setSelectedCardId(String LastCard)
    {
        editor.putString("SelectedCardId", LastCard);
        editor.commit();
    }

    /**
     * This method is used to getting the LastCard Number.
     * @return, returns LastCard Number.
     */
    public String getSelectedCardNumber()
    {
        return sharedPreferences.getString("SelectedCardNumber", "");
    }

    /**
     * This method is used to store the LastCard Number.
     * @param LastCardNumber, containing lastCard Number.
     */
    public void setSelectedCardNumber(String LastCardNumber)
    {
        editor.putString("SelectedCardNumber", LastCardNumber);
        editor.commit();
    }

    /**
     * This method is used to store the CardType.
     * @param cardBrand, containing CardType, debit/ credit card type.
     */
    public void setCardBrand(String cardBrand)
    {
        editor.putString("CardBrand", cardBrand);
        editor.commit();
    }
    /**
     * This method is used to getting the LastCard Image.
     * @return, returns CardType menase it is credit/ debit card..
     */
    public String getCardBrand()
    {
        return sharedPreferences.getString("CardBrand", "");
    }

    public void setAppOpenTime(int appOpenTime)
    {
        editor.putInt("AppOpenTime", appOpenTime);
        editor.commit();
    }

    public int getAppOpenTime()
    {
        return sharedPreferences.getInt("AppOpenTime", 0);
    }

    public void setDontShowRate(boolean dontOpenRate)
    {
        editor.putBoolean("DontOpenRate", dontOpenRate);
        editor.commit();
    }

    public boolean getDontShowRate()
    {
        return sharedPreferences.getBoolean("DontOpenRate", false);
    }

    public JSONArray getLocationArray() {
        JSONArray jsonLocationArray = new JSONArray();
        String stringLocationArray = sharedPreferences.getString("LocationArray", "[]");
        try {
            jsonLocationArray = new JSONArray(stringLocationArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonLocationArray;
    }

    public void updateLocationArray(String lat, String lng)
    {
        try {
            JSONArray jsonLocationArray = new JSONArray(sharedPreferences.getString("LocationArray", "[]"));
            jsonLocationArray.put(lat+","+lng);
            editor.putString("LocationArray", String.valueOf(jsonLocationArray));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void clearLocationArray()
    {
        editor.putString("LocationArray", "[]");
    }

    public void setCallToken(String callToken)
    {
        editor.putString("CALL_TOKEN", callToken);
        editor.commit();

    }
    public String getCallToken()
    {
        return sharedPreferences.getString("CALL_TOKEN", "");
    }

    public void setCallWillTopic(String willTopic) {
        editor.putString("WILL_TOPIC", willTopic);
        editor.commit();
    }
    public String getWillTopic()
    {
        return sharedPreferences.getString("WILL_TOPIC", "");
    }


    public void setIncmgCallBookingType(String callType) {
        editor.putString("BOOK_IMCMG_CALL_TYPE", callType);
        editor.commit();
    }
    public String getIncmgCallBookingType()
    {
        return sharedPreferences.getString("BOOK_IMCMG_CALL_TYPE", "1");
    }
  public boolean getIsNewBookingFromMain()
    {
        return sharedPreferences.getBoolean("NEW_BOOKING_FROM_MAIN", false);
    }


    public void setNewBookingFromMain(boolean b) {
        editor.putBoolean("NEW_BOOKING_FROM_MAIN", b);
        editor.commit();
    }
}