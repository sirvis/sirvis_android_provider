package com.sirvis.pro.utility;

import android.support.v4.view.ViewPager;
import android.view.View;


public class ViewPagerCustomTransformer implements ViewPager.PageTransformer
{

    @Override
    public void transformPage(View page, float position) {
        final float rotation = 90f * position;
        page.setVisibility(rotation > 90f || rotation < -90f ? View.INVISIBLE : View.VISIBLE);
        page.setPivotX(position <= 0 ? page.getWidth() : 0.0f);
        page.setPivotY(page.getHeight() * 0.5f);
        //it rotates with 90 degrees multiplied by current position
        page.setRotationY(rotation);

    }
}
