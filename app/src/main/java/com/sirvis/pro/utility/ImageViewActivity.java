package com.sirvis.pro.utility;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sirvis.pro.R;

import java.lang.ref.WeakReference;

public class ImageViewActivity extends AppCompatActivity {
    private WeakReference<TouchImageView> bitmapWeakReference;
    private Bitmap tempBitmap;
    private TouchImageView smallImageView;
    private static final int LAKH = 100000;

    private ScaleGestureDetector scaleGestureDetector;
    private Matrix matrix = new Matrix();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageview);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }
        TextView tvTitle=findViewById(R.id.tvTitle);
        tvTitle.setText("Document Upload");
      /* ImageView closeButton = findViewById(R.id.ivCloseButton);
        closeButton.setVisibility(View.VISIBLE);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        try {
            String ivDocument_url = getIntent().getStringExtra("ivDocument_url");
            smallImageView = findViewById(R.id.imageView);
            bitmapWeakReference = new WeakReference<TouchImageView>(smallImageView);
            tempBitmap = BitmapFactory.decodeFile(String.valueOf(ivDocument_url));
            int size = sizeOf(tempBitmap);
            if (size > (50 * LAKH)) {
                int optionsize = 2;
                /*int modsize = size / (50 * LAKH);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                tempBitmap.compress(Bitmap.CompressFormat.PNG, (1 / modsize) * 100, stream);*/

                while (sizeOf(tempBitmap) > (50 * LAKH)) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = optionsize;
                    if (tempBitmap != null) {
                        tempBitmap.recycle();
                        tempBitmap = null;
                    }
                    tempBitmap = BitmapFactory.decodeFile(String.valueOf(ivDocument_url), options);
                    sizeOf(tempBitmap);
                    optionsize++;
                }

            }
            bitmapWeakReference.get().setImageBitmap(tempBitmap);
//            smallImageView.setImageBitmap(tempBitmap);
            scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                smallImageView.setTransitionName(getString(R.string.activity_image_trans));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            Log.d("sizeOf1:", data.getRowBytes() * data.getHeight() + "");
            return data.getRowBytes() * data.getHeight();
        } else {
            Log.d("sizeOf2:", data.getByteCount() + "");
            return data.getByteCount();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
           finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // this redirects all touch events in the activity to the gesture detector
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        scaleGestureDetector.onTouchEvent(ev);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.
            SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
            matrix.setScale(scaleFactor, scaleFactor);
            bitmapWeakReference.get().setImageMatrix(matrix);
            return true;
        }
    }


    /**
     * reduces the size of the image
     *
     * @param image
     * @param maxSize
     * @return
     */
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}