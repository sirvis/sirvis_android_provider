package com.sirvis.pro.utility;

import android.content.Context;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.sirvis.pro.BuildConfig;

import org.json.JSONObject;

/**
 * Created by murashid on 10-Jan-18.
 */

public class MixpanelHelper {

    private MixpanelAPI mixpanel;
    private SessionManager sessionManager;

    public MixpanelHelper(Context cont) {
        if(mixpanel == null)
        {
            mixpanel = MixpanelAPI.getInstance(cont, BuildConfig.MIXPANEL_TOKEN);
        }
        sessionManager = SessionManager.getSessionManager(cont);
    }


    public void commonTrackAfterLogin(String event)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("providerId", sessionManager.getProviderId());
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());

            //mixpanel.track(event, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void commonTrackBeforeLogin(String event)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("time", System.currentTimeMillis());
            //mixpanel.track(event, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void sigunUpComplete(String name, String phone, String age)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("name", name);
            props.put("phoneNumber", phone);
            props.put("age", age);

            //mixpanel.track(MixpanelEvents.SignupComplete.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void myListUpdated(String value)
    {
        try {
            String events = "";
            switch (value)
            {
                case "link":
                    events = MixpanelEvents.MyListYoutubeLinkUpdated.value;
                    break;

                case "about":
                    events = MixpanelEvents.MyListAboutMeUpdated.value;
                    break;

                case "musicGeners":
                    events = MixpanelEvents.MyListMusicGenreUpdated.value;
                    break;

                case "events":
                    events = MixpanelEvents.MyListEventsUpdated.value;
                    break;

                case "rules":
                    events = MixpanelEvents.MyListRules.value;
                    break;

                case "instruments":
                    events = MixpanelEvents.MyListInstrumentUpdated.value;
                    break;

                case "location":
                    events = MixpanelEvents.MyListLocationUpdated.value;
                    break;

            }

            JSONObject props = new JSONObject();
            props.put("providerId", sessionManager.getProviderId());
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());

            //mixpanel.track(events, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void bookingStatus(String event,String bookingId)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            props.put("providerId", sessionManager.getProviderId());
            props.put("bookingId", bookingId);
            //mixpanel.track(event, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void postingReview(String customerName,String bookingId)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("customerName", customerName);
            props.put("bookingId", bookingId);
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            //mixpanel.track(MixpanelEvents.PostingReview.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void viewCustomerReview(String customerName)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("customerName", customerName);
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            //mixpanel.track(MixpanelEvents.PostingReview.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void scheduleCreated(String available)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            props.put("available", available);
            //mixpanel.track(MixpanelEvents.ScheduleCreated.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void online()
    {
        try {
            //mixpanel.timeEvent(MixpanelEvents.OnlineTime.value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void offline()
    {
        try {
            JSONObject props = new JSONObject();
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            //mixpanel.track(MixpanelEvents.OnlineTime.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void timeStartForNowBookingRespond()
    {
        try {
            //mixpanel.timeEvent(MixpanelEvents.TimeNowBooking.value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void timeEndForNowBookingRespond(String customerName, String bookingId, String status)
    {
        try {
            JSONObject props = new JSONObject();

            props.put("customerName", customerName);
            props.put("bookingId", bookingId);
            props.put("status", status);
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            //mixpanel.track(MixpanelEvents.TimeNowBooking.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void timeStartForLaterBookingRespond()
    {
        try {
            //mixpanel.timeEvent(MixpanelEvents.TimeLaterBooking.value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void timeEndForLaterBookingRespond(String customerName, String bookingId, String status)
    {
        try {
            JSONObject props = new JSONObject();
            props.put("customerName", customerName);
            props.put("bookingId", bookingId);
            props.put("status", status);
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            //mixpanel.track(MixpanelEvents.TimeLaterBooking.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void timeForAppClosed()
    {
        try {
            //mixpanel.timeEvent(MixpanelEvents.AppReopenTime.value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void timeForAppOpened()
    {
        try {
            JSONObject props = new JSONObject();
            props.put("name", sessionManager.getFirstName() + " "+ sessionManager.getLastName());
            props.put("providerId", sessionManager.getProviderId());
            props.put("phoneNumber", sessionManager.getCountryCode() + " "+ sessionManager.getPhoneNumber());
            //mixpanel.track(MixpanelEvents.AppReopenTime.value, props);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


}
