package com.sirvis.pro.utility;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by murashid on 16-Sep-17.
 * <h1>DatePickerAbove18</h1>
 * DatePickerAbove18 Custome DatePickerCommon dialog for showing the age about 18
 */
public class DatePickerCommon extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public DateSelected dateSelected;
    private SimpleDateFormat gettingFormat;
    private SimpleDateFormat displayFormat;
    private SimpleDateFormat sendingFormat;

    Calendar c ;
    int year ;
    int month ;
    int day ;
    int type;

    public DatePickerCommon()
    {
        gettingFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
        displayFormat = new SimpleDateFormat("MMMM yyyy",Locale.US);
        sendingFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.US);

        c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
    }



    /**
     * setDatePickerType
     * @param type picker type
     *  1 = > normal
     *  2 = > Above 18 age
     *  3 = > current to  future
     *  4 = > past to current
     */
    public  void setDatePickerType(int type)
    {
       this.type = type;
    }

    public  void setCallBack(DateSelected dateSelected) {
        this.dateSelected = dateSelected;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
        c = Calendar.getInstance();
        switch (type)
        {
            case 2:
                c.add(Calendar.YEAR,-18);
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                break;

            case 3:
                datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis() - 1000);
                break;

            case 4:
                datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                break;
        }

        return datePickerDialog;
    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        monthOfYear = monthOfYear+1;
        String month = monthOfYear+"",day = dayOfMonth+"";
        if(monthOfYear < 10)
        {
            month = 0+""+monthOfYear;
        }
        if(dayOfMonth < 10 )
        {
            day = 0+""+dayOfMonth;
        }

        formatDate(String.valueOf(year),month,day);
    }

    /**
     * method for fomate the date by year , month and day
     * @param year year
     * @param month month
     * @param day day
     */
    public void formatDate(String year,String month,String day)
    {
        try {
            Date d = gettingFormat.parse(year+""+month+""+day);
            dateSelected.onDateSelected(sendingFormat.format(d),Utility.getDayOfMonthSuffix(Integer.parseInt(day),displayFormat.format(d)));
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }

    public void convetSendingToDisplayDate(String sendingDate)
    {
        String displayDate="";
        try {
            String day = sendingDate.split("-")[2];
            Date d = sendingFormat.parse(sendingDate);
            displayDate = Utility.getDayOfMonthSuffix(Integer.parseInt(day),displayFormat.format(d));

            dateSelected.onDateSelected(sendingDate, displayDate);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public String returmSendingToDisplayDate(String sendingDate)
    {
        String displayDate="";
        try {
            String day = sendingDate.split("-")[2];
            Date d = sendingFormat.parse(sendingDate);
            displayDate = Utility.getDayOfMonthSuffix(Integer.parseInt(day),displayFormat.format(d));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return displayDate;
    }



    /**
     * DateSelected callback for date selected
     */
    public interface DateSelected
    {
        void onDateSelected(String sendingFormat, String displayFormat);
    }
}
