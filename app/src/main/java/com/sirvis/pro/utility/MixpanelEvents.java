package com.sirvis.pro.utility;

/**
 * Created by murashid on 10-Jan-18.
 */

public enum MixpanelEvents {

    AppOpenAfterLogin("Artist app open after login"),
    AppOpenBeforeLogin("Artist app open before login"),

    SignupOpen("Artist signup page opened"),
    SignupComplete("Artist signup sent"),

    MyListOpen("Artist open my listing"),
    MyListYoutubeLinkUpdated("Artist fills Youtube Link"),
    MyListAboutMeUpdated("Artist fills About Me"),
    MyListMusicGenreUpdated("Artist fills Music Genre"),
    MyListEventsUpdated("Artist fills Events"),
    MyListRules("Artist fills Rules"),
    MyListInstrumentUpdated("Artist fills Instruments"),
    MyListLocationUpdated("Artist fills Location"),

    ChatOpen("Artist Chat Open"),
    CallOpen("Artist Call Open"),

    GoesOnline("Artist goes online"),
    GoesOffline("Artist goes offline"),

    BookingAccept("Artist accepts a request"),
    BookingDecling("Artist declines a request"),
    BookingCancelled("Artist cancelled a booking"),
    OnTheWay("Artist marks on the way"),
    Arrived("Artist marks arrived"),
    GigStarts("Artist starts gig"),
    GigPause("Artist pauses gig"),
    JobCompleted("Artist marks job completed"),
    RaiseInvoice("Artist raises invoice"),
    ViewCustomerDetails("Artist open customer profile"),

    ScheduleCreated("Artist saves schedule"),

    PostingReview("Artist sends review"),
    ViewReview("Artist goes to app review"),
    SentReferal("Artist sends referral"),

    OnlineTime("Artist Online Time"),
    TimeNowBooking("Artist Time For Respond To Now Booking"),
    TimeLaterBooking("Artist Time For Respond To Later Booking"),
    AppReopenTime("Artist Time For App ReOpen"),

    Login("Login"),
    Logout("Logout");

    public String value;

    MixpanelEvents(String value) {
        this.value = value;
    }
}
