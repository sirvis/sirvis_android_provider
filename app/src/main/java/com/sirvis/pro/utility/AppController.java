package com.sirvis.pro.utility;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
//import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.account.AccountManagerHelper;
import com.sirvis.pro.mqtt.MqttHelper;
import com.sirvis.pro.service.NewBookingRingtoneService;
import com.sirvis.pro.telecall.utility.CallHelper;

import java.util.TimeZone;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.InitializationCallback;


/**
 * <h>AppController</h>
 * Created by murashid on 02-Oct-17.
 */

public class AppController extends Application {

    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

    private MqttHelper mqttHelper;

    private CallHelper callHelper;

    private MixpanelHelper mixpanelHelper;

    private AccountManagerHelper accountManagerHelper;

    private Intent newBookingRingtoneServiceIntent;

    private SessionManager sessionManager;
    private TimeZone timeZone;



    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        CrashlyticsCore core = new CrashlyticsCore.Builder()
                .disabled(BuildConfig.DEBUG)
                .build();
        Fabric.with(new Fabric.Builder(this).kits(new Crashlytics.Builder()
                .core(core)
                .build())
                .initializationCallback(new InitializationCallback<Fabric>() {
                    @Override
                    public void success(Fabric fabric) {
                        mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
                        Thread.setDefaultUncaughtExceptionHandler(mCaughtExceptionHandler);
                    }

                    @Override
                    public void failure(Exception e) {
                        e.printStackTrace();
                    }
                })


                .build());



        sessionManager = SessionManager.getSessionManager(this);
        mInstance = this;
        mixpanelHelper = new MixpanelHelper(this);
        mqttHelper = new MqttHelper(this);
    }

    public void getCorrectTimeZoneByLatLng()
    {
        if(!sessionManager.getCurrentLat().equals("0.0"))
        {
            String timeZoneString =  TimezoneMapper.latLngToTimezoneString(Double.parseDouble(sessionManager.getCurrentLat()), Double.parseDouble(sessionManager.getCurrentLng()));
            timeZone = TimeZone.getTimeZone(timeZoneString);
        }
    }

    public TimeZone getTimeZone()
    {
        if(timeZone != null)
        {
            return timeZone;
        }
        return TimeZone.getTimeZone("Etc/UTC");
    }

    public MqttHelper getMqttHelper() {
        if(mqttHelper == null)
        {
            mqttHelper = new MqttHelper(this);
        }
        return mqttHelper;
    }

    public CallHelper getCallHelper() {
        if(callHelper == null)
        {
            callHelper = new CallHelper(this);
        }
        return callHelper;
    }



    public MixpanelHelper getMixpanelHelper()
    {
        if(mixpanelHelper == null)
        {
            mixpanelHelper = new MixpanelHelper(this);
        }
        return mixpanelHelper;
    }

    public AccountManagerHelper getAccountManagerHelper()
    {
        if(accountManagerHelper == null)
        {
            accountManagerHelper = new AccountManagerHelper(this);
        }
        return accountManagerHelper;
    }

    private Intent getNewBookingRingtoneServiceIntent()
    {
        if(newBookingRingtoneServiceIntent == null)
        {
            newBookingRingtoneServiceIntent = new Intent(this, NewBookingRingtoneService.class);
        }
        return newBookingRingtoneServiceIntent;
    }

    public void startNewBookingRingtoneService()
    {
        startService(getNewBookingRingtoneServiceIntent());
    }

    public void stopNewBookingRingtoneService()
    {
        stopService(getNewBookingRingtoneServiceIntent());
    }

    public static synchronized  void toast()
    {
        Toast.makeText(getInstance(),getInstance().getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    public synchronized  void toast(String msg)
    {
        Toast.makeText(getInstance(),msg,Toast.LENGTH_SHORT).show();
    }


    private static Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;
    private static Thread.UncaughtExceptionHandler mCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {


//            if (AppController.getInstance().isActiveOnACall()) {
//                AppController.getInstance().cutCallOnKillingApp(true);
//            }
            mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
        }
    };
}

