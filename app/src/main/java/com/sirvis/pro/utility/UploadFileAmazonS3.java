package com.sirvis.pro.utility;

import android.content.Context;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.google.gson.Gson;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.pojo.CognitoIdRespomse;

import org.json.JSONObject;

import java.io.File;

public class UploadFileAmazonS3 {

    static SessionManager sessionManager;
    private static UploadFileAmazonS3 uploadAmazonS3;
    DeveloperAuthenticationProvider developerProvider;
    private CognitoCachingCredentialsProvider credentialsProvider = null;
    private AmazonS3Client s3Client = null;
    private TransferUtility transferUtility = null;

    /**
     * Creating single tone object by defining private.
     * <P>
     * At the time of creating
     * </P>
     */
    private UploadFileAmazonS3(Context context) {

        /*developerProvider = new DeveloperAuthenticationProvider(null,
                VariableConstant.COGNITO_POOL_ID, Regions.EU_WEST_2);*/

    }

    public static UploadFileAmazonS3 getInstance(Context context) {
        sessionManager = SessionManager.getSessionManager(context);
        if (uploadAmazonS3 == null) {
            uploadAmazonS3 = new UploadFileAmazonS3(context);
            return uploadAmazonS3;
        } else {
            return uploadAmazonS3;
        }
    }

    /**
     * <h3>Upload_data</h3>
     * <P>
     * Method is use to upload data in the amazone server.
     *
     * </P>
     */

    public void Upload_data(Context context, final String bukkate_name, final File file,
                            final UploadCallBack callBack) {

        getCognitoToken(context, bukkate_name, file, callBack);

    }




    /**
     * This method is used to get the CredentialProvider and we provide only context as a parameter.
     *
     * @param context Here, we are getting the context from calling Activity.
     */
    private CognitoCachingCredentialsProvider getCredentialProvider(Context context, String pool_id) {
        if (credentialsProvider == null && developerProvider == null
                && !VariableConstant.AmazoncognitoToken.isEmpty()
                && !VariableConstant.COGNITO_POOL_ID.isEmpty()) {

            developerProvider = new DeveloperAuthenticationProvider(null,
                    VariableConstant.COGNITO_POOL_ID, Regions.EU_CENTRAL_1);

            developerProvider.updateCredentials();

            credentialsProvider = new CognitoCachingCredentialsProvider(
                    context, developerProvider, Regions.EU_CENTRAL_1);

        }
        return credentialsProvider;
    }


    private void getCredentialProvider(Context context) {
        if (!VariableConstant.AmazoncognitoToken.isEmpty()
                && !VariableConstant.COGNITO_POOL_ID.isEmpty()) {

            developerProvider.updateCredentials();

            credentialsProvider = new CognitoCachingCredentialsProvider(
                    context, developerProvider, Regions.EU_CENTRAL_1);
            /* *
             * Creating the object  of the s3Client */
            s3Client = getS3Client();

            /* *
             * Creating the object of the TransferUtility of the Amazone.*/
            transferUtility = getTransferUtility(context, s3Client);
        }

    }

    /**
     * This method is used to get the AmazonS3 Client
     * and we provide only context as a parameter.
     * and from here we are calling getCredentialProvider() function.
     */
    private AmazonS3Client getS3Client() {
        s3Client = new AmazonS3Client(credentialsProvider);
        return s3Client;
    }

    /**
     * This method is used to get the Transfer Utility
     * and we provide only context as a parameter.
     * and from here we are, calling getS3Client() function.
     *
     * @param context Here, we are getting the context from calling Activity.
     */
    private TransferUtility getTransferUtility(Context context, AmazonS3Client amazonS3Client) {
        if (transferUtility == null) {
            transferUtility = new TransferUtility(amazonS3Client, context.getApplicationContext());
        }
        return transferUtility;
    }

    public void getCognitoToken(final Context context, final String bukkate_name, final File file,
                                final UploadCallBack callBack) {

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),ServiceUrl.COGNITOTOKEN, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        Log.d("UpDATEPROFILE", "onSuccess: " + result + " headerResponse " + result);
                        CognitoIdRespomse cognitoIdRespomse = new Gson().fromJson(result,
                                CognitoIdRespomse.class);
                        VariableConstant.BUCKET_NAME = cognitoIdRespomse.getData().getBucket();
                        VariableConstant.COGNITO_POOL_ID = cognitoIdRespomse.getData().getIdentityId();
                        VariableConstant.AmazoncognitoToken = cognitoIdRespomse.getData().getToken();
                        VariableConstant.AMAZON_REGION = Regions.EU_CENTRAL_1;
                        developerProvider = new DeveloperAuthenticationProvider(null,
                                VariableConstant.COGNITO_POOL_ID, Regions.EU_CENTRAL_1);
                        onSuccessCognitoTo(context, bukkate_name, file, callBack);
                    }

                    @Override
                    public void onError(String error) {
                        callBack.error(error.toString());

                    }
                });

    }

    private void onSuccessCognitoTo(Context context, final String bukkate_name, final File file,
                                    final UploadCallBack callBack) {
        getCredentialProvider(context);

        if (transferUtility != null && file != null) {
            TransferObserver observer = transferUtility.upload(VariableConstant.BUCKET_NAME,
                    bukkate_name + "/" + file.getName(), file, CannedAccessControlList.PublicRead);

            //Do something after 100ms
            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state.equals(TransferState.COMPLETED)) {


                        callBack.sucess(
                                "https://" + VariableConstant.BUCKET_NAME + "." + BuildConfig.AMAZON_BASE_URL
                                        + bukkate_name + "/"
                                        + file.getName());

                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                }

                @Override
                public void onError(int id, Exception ex) {
                    callBack.error(id + ":" + ex.toString());

                }
            });
        } else {
            callBack.error("Amamzones3 is not intialize or File is empty !");
        }

    }

    /**
     * Interface for the sucess callback fro the Amazon uploading .
     */
    public interface UploadCallBack {
        /**
         * Method for sucess .
         *
         * @param sucess it is true on sucess and false for falure.
         */
        void sucess(String sucess);

        /**
         * Method for falure.
         *
         * @param errormsg contains the error message.
         */
        void error(String errormsg);

    }

}

