package com.sirvis.pro.main.profile.calltype;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.ProfilePojo;
import com.sirvis.pro.pojo.profile.calltype.CallTypeData;
import com.sirvis.pro.pojo.profile.calltype.CallTypePojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 26-Nov-18.
 */
public class CallTypeCategoryListPresenter {
    private static final String TAG = "SelectCategory";
    private View view;

    public CallTypeCategoryListPresenter(View view)
    {
        this.view = view;
    }

    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    public void getCallType(final String sessionToken)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CALL_TYPE_SETTING, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        CallTypePojo callTypePojo = gson.fromJson(result, CallTypePojo.class);
                                        view.onSuccessGetCallType(callTypePojo.getData());
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getCallType(newToken);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }


    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    public void updateCallType(final String sessionToken, final JSONObject jsonObjectRequest)
    {

        try {
            if(!jsonObjectRequest.getString("inCall").equals("1") && !jsonObjectRequest.getString("outCall").equals("1") &&
            !jsonObjectRequest.getString("teleCall").equals("1"))
            {
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CALL_TYPE_SETTING, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObjectRequest
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccessUpdateCallType(jsonObject.getString("message"));
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                updateCallType(newToken,jsonObjectRequest );
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    public interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccessGetCallType(ArrayList<CallTypeData> callTypeData);
        void onErrorUpdateCallType();
        void onSuccessUpdateCallType(String msg);
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
