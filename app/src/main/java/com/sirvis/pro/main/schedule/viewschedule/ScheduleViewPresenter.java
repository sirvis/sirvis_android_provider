package com.sirvis.pro.main.schedule.viewschedule;

import com.sirvis.pro.pojo.shedule.ScheduleData;

/**
 * Created by murashid on 04-Oct-17.
 * <h1>ScheduleViewPresenter</h1>
 * ScheduleViewPresenter presenter for ScheduleViewActivity
 * @see ScheduleViewActivity
 */

public class ScheduleViewPresenter implements ScheduleViewModel.ScheduleViewModelImple {
    private ScheduleViewModel model;
    private ScheduleViewPresenterImple presenterImple;

    ScheduleViewPresenter(ScheduleViewPresenterImple presenterImple) {
        model = new ScheduleViewModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken sessionToken
     */
    void getShedule(String sessionToken){
        presenterImple.startProgressBar();
        model.getShedule(sessionToken);
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session token
     * @param id addresss id
     */
    void deleteSchedule(String sessionToken,String id)
    {
        presenterImple.startProgressBar();
        model.deleteSchedule(sessionToken,id);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(ScheduleData data) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(data);
    }

    @Override
    public void onSuccessDeleteSchedule(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessDeleteSchedule(msg);
    }

    /**
     * ScheduleViewPresenterImple interface for view implementation
     */
    interface ScheduleViewPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(ScheduleData data);
        void onSuccessDeleteSchedule(String msg);
    }

}
