package com.sirvis.pro.main.profile.myratecard.singleservice;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.pojo.profile.ratecard.CategoryService;
import com.sirvis.pro.utility.DecimalDigitsInputFilter;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class PriceFragment extends Fragment implements SeekBar.OnSeekBarChangeListener, View.OnFocusChangeListener {

    private SessionManager sessionManager;
    private static final String ARG_PARAM1 = "param1";
    private CategoryService serviceData;

    private View viewFirst, viewSecond, viewThird, viewFourth, viewFifth;
    private int r1,r2,r3,r4,r5,r6;
    public int minValue, maxValue;

    public EditText etPrice, etServiceTime;
    public SeekBar seekBarPrice;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param isRequest Parameter 1.
     * @return A new instance of fragment MyEventsListFragment.
     */
    public static PriceFragment newInstance(CategoryService serviceData) {
        PriceFragment fragment = new PriceFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, serviceData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            serviceData = (CategoryService) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_price, container, false);
        init(view);
        return view;
    }

    private void init(View view)
    {
        sessionManager = SessionManager.getSessionManager(getContext());
        Typeface fontMedium = Utility.getFontMedium(getContext());
        Typeface fontRegular = Utility.getFontRegular(getContext());

        TextView tvHourlyRate = view.findViewById(R.id.tvHourlyRate);
        TextView tvPriceRange = view.findViewById(R.id.tvPriceRange);
        TextView tvMinValueLabel = view.findViewById(R.id.tvMinValueLabel);
        TextView tvMinValue = view.findViewById(R.id.tvMinValue);
        TextView tvMaxValueLabel = view.findViewById(R.id.tvMaxValueLabel);
        TextView tvMaxValue = view.findViewById(R.id.tvMaxValue);
        TextView tvCurrencySymbol = view.findViewById(R.id.tvCurrencySymbol);
        TextView tvCurrencySymbolSuffix = view.findViewById(R.id.tvCurrencySymbolSuffix);
        TextView tvServiceTimeLabel = view.findViewById(R.id.tvServiceTimeLabel);
        TextView tvMinute = view.findViewById(R.id.tvMinute);
        etPrice = view.findViewById(R.id.etPrice);
        etPrice.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        etServiceTime = view.findViewById(R.id.etServiceTime);

        tvHourlyRate.setTypeface(fontMedium);
        tvServiceTimeLabel.setTypeface(fontMedium);
        tvPriceRange.setTypeface(fontMedium);
        tvMinValueLabel.setTypeface(fontRegular);
        tvMinValue.setTypeface(fontRegular);
        tvMaxValueLabel.setTypeface(fontRegular);
        tvMaxValue.setTypeface(fontRegular);
        tvCurrencySymbol.setTypeface(fontMedium);
        tvCurrencySymbolSuffix.setTypeface(fontMedium);
        tvMinute.setTypeface(fontRegular);
        etPrice.setTypeface(fontMedium);
        etServiceTime.setTypeface(fontMedium);

        seekBarPrice = view.findViewById(R.id.seekBarPrice);
        viewFirst =  view.findViewById(R.id.viewFirst);
        viewSecond =  view.findViewById(R.id.viewSecond);
        viewThird =  view.findViewById(R.id.viewThird);
        viewFourth =  view.findViewById(R.id.viewFourth);
        viewFifth =  view.findViewById(R.id.viewFifth);

        minValue = (int)Double.parseDouble(serviceData.getMinFees());
        maxValue = (int)Double.parseDouble(serviceData.getMaxFees());

        calculateCenterPointsValue();

        tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
        tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());
        tvMinValue.setText(""+minValue);
        tvMaxValue.setText(""+maxValue);

        if(sessionManager.getCurrencyAbbrevation().equals("2"))
        {
            tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
            tvCurrencySymbol.setVisibility(View.GONE);
        }

        seekBarPrice.setMax(maxValue - minValue);
        seekBarPrice.setProgress((int)Double.parseDouble(serviceData.getIs_unit()) - minValue);
        setProgress((int)Double.parseDouble(serviceData.getIs_unit()));

        seekBarPrice.setOnSeekBarChangeListener(this);
        etPrice.setOnFocusChangeListener(this);

        etPrice.setText(Utility.getFormattedPrice(serviceData.getIs_unit()));

        if(serviceData.getServiceCompletionTime() != null)
        {
            etServiceTime.setText(serviceData.getServiceCompletionTime());
        }
        else
        {
            etServiceTime.setText("30");
        }

    }


    private void calculateCenterPointsValue() {
        double commonValue =(double)(maxValue - minValue) / 5 ;
        r1 = minValue ;
        double r2 =  r1 + commonValue ;
        double r3 =  r2 + commonValue;
        double r4 =  r3 + commonValue ;
        double r5 =  r4 + commonValue ;
        this.r2 = (int) r2;
        this.r3 = (int) r3;
        this.r4 = (int) r4;
        this.r5 = (int) r5;
        r6 =  maxValue;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        etPrice.setText(Utility.getFormattedPrice(String.valueOf(seekBar.getProgress()+minValue)));
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setProgress(seekBar.getProgress()+minValue);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        switch (view.getId())
        {
            case R.id.etPrice:
                if(!b)
                {
                    etPrice.setText(Utility.getFormattedPrice(etPrice.getText().toString()));
                }
                else
                {
                    etPrice.setSelection(etPrice.getText().length());
                }
                break;

            case R.id.etServiceTime:
                if(b)
                {
                    etServiceTime.setSelection(etServiceTime.getText().length());
                }
                break;
        }




    }

    private void setProgress(int progress) {

        if(r6 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        }
        else if(r5 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider);
        }
        else if(r4 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider);
        }
        else if(r3 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
        }
        else if(r2 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
        }
        else if(r1 <= progress)
        {
            setViewColors(R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
        }
    }

    /**
     * Change the color of the views in sentingValue
     * @param v1 view of the 1
     * @param v2 view of the 2
     * @param v3 view of the 3
     * @param v4 view of the 4
     * @param v5 view of thes 5
     */
    private  void setViewColors(int v1, int v2, int v3, int v4, int v5)
    {
        viewFirst.setBackgroundColor(ContextCompat.getColor(getContext(),v1));
        viewSecond.setBackgroundColor(ContextCompat.getColor(getContext(),v2));
        viewThird.setBackgroundColor(ContextCompat.getColor(getContext(),v3));
        viewFourth.setBackgroundColor(ContextCompat.getColor(getContext(),v4));
        viewFifth.setBackgroundColor(ContextCompat.getColor(getContext(),v5));
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

}
