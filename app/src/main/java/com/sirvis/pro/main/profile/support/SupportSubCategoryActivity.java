package com.sirvis.pro.main.profile.support;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;


import com.sirvis.pro.R;
import com.sirvis.pro.adapters.SupportListAdapter;
import com.sirvis.pro.pojo.profile.support.SupportData;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportSubCategoryActivity</h1>SupportActivity
 * SupportSubCategoryActivity for showing SubCategory Support in  SupportActivity
 * @see SupportActivity
 */
public class SupportSubCategoryActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_sub_category);

        initView();
    }

    /**
     * init the views
     */
    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        Intent intent = getIntent();
        ArrayList<SupportData> supportDatas = new ArrayList<>();
        supportDatas.addAll((Collection<? extends SupportData>) intent.getSerializableExtra("data"));

        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvSubTitle = findViewById(R.id.tvSubTitle);

        tvTitle.setTypeface(Utility.getFontBold(this));
        tvSubTitle.setTypeface(Utility.getFontMedium(this));

        tvTitle.setText(getString(R.string.support));
        tvSubTitle.setText(intent.getStringExtra("title"));


        RecyclerView rvSupportSubCateogry = findViewById(R.id.rvSupportSubCateogry);
        rvSupportSubCateogry.setLayoutManager(new LinearLayoutManager(this));

        SupportListAdapter supportListAdapter = new SupportListAdapter(this, supportDatas, false);
        rvSupportSubCateogry.setAdapter(supportListAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }
}
