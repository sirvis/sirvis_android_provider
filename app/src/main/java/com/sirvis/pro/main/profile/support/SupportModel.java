package com.sirvis.pro.main.profile.support;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.support.SupportData;
import com.sirvis.pro.pojo.profile.support.SupportPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportModel</h1>
 * SupportModel model for SupportActivity
 * @see SupportActivity
 */

public class SupportModel {
    private static final String TAG = "SupportModel";
    private SupportModelImplement modelImplement;

    SupportModel(SupportModelImplement modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api to fetch the reviews
     */
    void fetchData() {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("",ServiceUrl.SUPPORT + "/2", OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                        SupportPojo supportPojo = new Gson().fromJson(result, SupportPojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            modelImplement.onSuccess(supportPojo.getData());
                        } else {
                            modelImplement.onFailure(supportPojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });

    }

    /**
     * SupportModelImplement interface of presenter implementation
     */
    interface SupportModelImplement {
        void onFailure(String failureMsg);

        void onFailure();

        void onSuccess(ArrayList<SupportData> supportDatas);
    }
}
