package com.sirvis.pro.main.profile.bank;

import android.util.Log;


import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.bank.BankList;
import com.sirvis.pro.pojo.profile.bank.LegalEntity;
import com.sirvis.pro.pojo.profile.bank.StripeDetailsPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankDetailsModel</h1>
 * BankDetailsModel model for BankDetailsAcitivity
 * @see BankDetailsActivity
 */

public class BankDetailsModel {
    private static final String TAG = "SupportFragModel";
    private BankDetailsModelImplement modelImplement;

    BankDetailsModel(BankDetailsModelImplement modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * mehthod for calling api for getting the bank details
     * @param token sessionToken
     */
    void fetchData(String token) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_CONNECT_ACCOUNT_STRIPE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccessRaiseTicket: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            StripeDetailsPojo stripeDetailsPojo = new Gson().fromJson(jsonObject.toString(), StripeDetailsPojo.class);
                            if (jsonObject.getString("message").equals("Got The Details.")) {
                                modelImplement.onSuccess(stripeDetailsPojo.getData());
                            }
                            else
                            {
                                modelImplement.onFailure(jsonObject.getString("message"));
                            }
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_NO_STRIPE_FOUND) || statusCode.equals(VariableConstant.RESPONSE_CODE_NO_ACCOUNT_FOUND) || statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND))
                        {
                            modelImplement.noStipeAccount(jsonObject.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    modelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * BankDetailsModelImplement interface for Presenter implementation
     */
    interface BankDetailsModelImplement {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(StripeDetailsPojo.Accounts accounts);
        void noStipeAccount(String msg);
    }
}

