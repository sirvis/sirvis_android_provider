package com.sirvis.pro.main.profile.bank;

import android.util.Log;


import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 29-Aug-17.
 * <h1>BankBottomSheetModel</h1>
 * BankBottomSheetModel model for BankBottomSheetFragment
 * @see BankBottomSheetFragment
 */

public class BankBottomSheetModel {

    private static final String TAG = "BankBottomSheet";

    private BankBottomSheetModelImplem bankBottomSheetModelImplem;
    BankBottomSheetModel(BankBottomSheetModelImplem bankBottomSheetModelImplem)
    {
        this.bankBottomSheetModelImplem = bankBottomSheetModelImplem;
    }

    /**
     * method for calling api for set the default bank account
     * @param token session token
     * @param jsonObject required fiels in json Object
     */
    void setDefaultAccount(String token,JSONObject jsonObject)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_EXTERNAL_ACCOUNT, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result!=null)
                    {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);

                        JSONObject jsonObject  = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            bankBottomSheetModelImplem.onSuccess(jsonObject.getString("message"));
                        }
                        else
                        {
                            bankBottomSheetModelImplem.onFailure(jsonObject.getString("message"));
                        }
                    }
                    else
                    {
                        bankBottomSheetModelImplem.onFailure();
                    }
                }
                catch (Exception e)
                {
                    bankBottomSheetModelImplem.onFailure();
                }
            }

            @Override
            public void onError(String error)
            {
                bankBottomSheetModelImplem.onFailure();
            }
        });
    }

    /**
     * method for calinng the api for deleting the bank account
     * @param token session Token
     * @param jsonObject required field in Json Object
     */
    void deleteBankAccount(String token,JSONObject jsonObject)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_EXTERNAL_ACCOUNT, OkHttp3ConnectionStatusCode.Request_type.DELETE, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result!=null)
                    {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject  = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            bankBottomSheetModelImplem.onSuccess(jsonObject.getString("message"));
                        }
                        else
                        {
                            bankBottomSheetModelImplem.onFailure(jsonObject.getString("message"));
                        }
                    }
                    else
                    {
                        bankBottomSheetModelImplem.onFailure();
                    }
                }
                catch (Exception e)
                {
                    bankBottomSheetModelImplem.onFailure();
                }
            }

            @Override
            public void onError(String error)
            {
                bankBottomSheetModelImplem.onFailure();
            }
        });
    }

    /**
     * BankBottomSheetModelImplem interface for Presenter implementation
     */
    interface BankBottomSheetModelImplem
    {
        void onFailure(String msg);
        void onFailure();
        void onSuccess(String msg);
    }
}
