package com.sirvis.pro.main.profile.bank;

import com.sirvis.pro.utility.UploadFileAmazonS3;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankNewStripePresenter</h1>
 * BankNewStripePresenter presenter for BankNewStripeActivity
 * @see  BankNewStripeActivity
 */

public class BankNewStripePresenter implements BankNewStripeModel.BankNewStripModelImplement {

    private BankNewStripePresenterImplement bankNewStripePresenterImplement;
    private BankNewStripeModel bankNewStripeModel;

    BankNewStripePresenter(BankNewStripePresenterImplement bankNewStripePresenterImplement) {
        this.bankNewStripePresenterImplement = bankNewStripePresenterImplement;
        bankNewStripeModel = new BankNewStripeModel(this);
    }

    /**
     * method for calling method in presenter
     */
    void getIp() {
        bankNewStripeModel.fetchIp();
    }

    /**
     * method for passing values from view to model
     * @param token session token
     * @param jsonObject required field in json object
     * @param isPictureTaken boolen for picture is taken or not
     * @param amazonS3 object of UploadFileAmazonS3 class
     * @param mFileTemp file that need to upload in amazon
     */
    void addBankDetails(String token, JSONObject jsonObject, boolean isPictureTaken, UploadFileAmazonS3 amazonS3, File mFileTemp) {
        bankNewStripePresenterImplement.startProgressBar();
        bankNewStripeModel.addBankAccount(token, jsonObject,isPictureTaken,amazonS3,mFileTemp);
    }

    @Override
    public void ipAddress(String ip) {
        bankNewStripePresenterImplement.ipAddress(ip);
    }

    @Override
    public void onErrorImageUpload() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorImageUpload();
    }

    @Override
    public void onErrorFirstName() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorFirstName();
    }

    @Override
    public void onErrorLastName() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorLastName();
    }

    @Override
    public void onErrorEmail() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorEmail();
    }

    @Override
    public void onErrorDob() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorDob();
    }

    @Override
    public void onErrorSSN() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorSSN();
    }

    @Override
    public void onErrorAddress() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorAddress();
    }

    @Override
    public void onErrorCity() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorCity();
    }

    @Override
    public void onErrorState() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorState();
    }

    @Override
    public void onErrorCountry() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorCountry();
    }

    @Override
    public void onErrorZipcode() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onErrorZipcode();
    }

    @Override
    public void onFailure() {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onFailure();
    }

    @Override
    public void onFailure(String msg) {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onFailure(msg);
    }

    @Override
    public void onSuccess(String msg) {
        bankNewStripePresenterImplement.stopProgressBar();
        bankNewStripePresenterImplement.onSuccess(msg);
    }


    interface BankNewStripePresenterImplement {
        void startProgressBar();
        void stopProgressBar();
        void onSuccess(String msg);
        void onFailure();
        void onFailure(String msg);
        void ipAddress(String ip);

        void onErrorImageUpload();
        void onErrorFirstName();
        void onErrorLastName();
        void onErrorEmail();
        void onErrorDob();
        void onErrorSSN();
        void onErrorAddress();
        void onErrorCity();
        void onErrorCountry();
        void onErrorState();
        void onErrorZipcode();

    }
}
