package com.sirvis.pro.main.profile.mylist;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sirvis.pro.pojo.profile.service.MyListServiceEdit;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListEditModel</h1>
 * MyListEditModel model for MyListActivity
 * @see MyListEditActivity
 */

public class MyListEditModel {
    private static final String TAG = "MyListEditModel";
    private MyListEditModelImple modelImplement;

    MyListEditModel(MyListEditModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for upadater the mylist field based on values and param
     * @param sessiontoken session Token
     * @param key key
     * @param value values
     */
    void updateMyList(String sessiontoken,String key,String value)
    {
        if(value != null && value.equals(""))
        {
            modelImplement.onEditTextEmpty();
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put(key,value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * method for calling api for upadater the mylist field based on values and param
     * @param sessiontoken session Token
     * @param jsonObject jsonObject
     */
    void updateMyList(String sessiontoken, JSONObject jsonObject)
    {
        try {
            if(jsonObject.getJSONObject("metaData").getString("data").equals(""))
            {
                modelImplement.onEditTextEmpty();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }



    /**
     * mehtod for calling api for updating service Category
     * @param sessiontoken session Token
     * @param myListServiceEdits new List
     */
    void updateServiceCategory(String sessiontoken, ArrayList<MyListServiceEdit> myListServiceEdits)
    {
        if(myListServiceEdits.size() == 0)
        {
            modelImplement.onEditTextEmpty();
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try
        {
            Gson gson = new Gson();
            String listString = gson.toJson(myListServiceEdits, new TypeToken<ArrayList<MyListServiceEdit>>() {}.getType());

            jsonObject.put("service",new JSONArray(listString));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        Log.d(TAG, "updateServiceCategory: "+myListServiceEdits);

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * MyListEditModelImple interface for Presenter implementation
     */
    interface MyListEditModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(String msg);
        void onEditTextEmpty();
    }


}
