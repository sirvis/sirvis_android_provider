package com.sirvis.pro.main.profile.mylist;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sirvis.pro.R;
import com.sirvis.pro.pojo.profile.metaData.PreDefined;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DatePickerCommon;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListEditActivity</h1>
 * MyListEditActivity acitvity for editing the Mylist fields based on the param and values
 */
public class MyListEditActivity extends AppCompatActivity implements MyListEditPresenter.MyListEditPresenterImple, View.OnClickListener, SeekBar.OnSeekBarChangeListener, DatePickerCommon.DateSelected {

    private EditText etCommon;
    private TextView tvWordCount;
    private MyListEditPresenter presenter;
    private  SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private String editableParam="";

    private View viewFirst, viewSecond, viewThird, viewFourth, viewFifth;
    private int r1,r2,r3,r4,r5,r6;
    private String sendingRadiusvalue;

    //myListType 1 => fixed field, 2 => metaDataField
    private String myListType = "1";
    /*fieldType
    1 = > normal editText,
    2 = > List of edit text,
    3) DateTime (current to  future)
    4) datetime (past to current)
    5) predefine with icon
    6) predefine without icon*/

    private String fieldType = "1"; /// fieldType

    private LinearLayout llMyListEdit;
    private LayoutInflater inflater;
    private ArrayList<EditText> etMyListEditDynamics;

    private TextInputLayout tilDate;
    private EditText etDate;
    private DatePickerCommon datePickerFragment;
    private String sendingDate ="";

    ArrayList<PreDefined> preDefineds;
    private ArrayList<String> sendingPredefinedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list_edit);

        init();
    }

    /**
     * init the views
     */
    private void init()
    {
        presenter = new MyListEditPresenter(this);
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.setCancelable(false);

        datePickerFragment = new DatePickerCommon();

        Intent intent = getIntent();

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fonRegular = Utility.getFontRegular(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);

        TextView tvDone = findViewById(R.id.tvDone);
        llMyListEdit =  findViewById(R.id.llMyListEdit);
        inflater = getLayoutInflater();
        etMyListEditDynamics = new ArrayList<>();

        tvDone.setTypeface(fontBold);
        tvDone.setText(getString(R.string.save));
        tvDone.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));

        tvDone.setOnClickListener(this);

        etCommon = findViewById(R.id.etCommon);
        tvWordCount = findViewById(R.id.tvWordCount);
        tilDate = findViewById(R.id.tilDate);
        etDate = findViewById(R.id.etDate);

        etCommon.setTypeface(fonRegular);
        tvWordCount.setTypeface(fonRegular);
        etDate.setTypeface(fonRegular);

        tvTitle.setText(intent.getStringExtra("title"));
        String data = intent.getStringExtra("data");
        myListType = intent.getStringExtra("myListType");
        fieldType = intent.getStringExtra("fieldType");
        editableParam = intent.getStringExtra("editableParam");
        etCommon.setHint(intent.getStringExtra("hint"));

        View layoutSeekBarRadius = findViewById(R.id.layoutSeekBarRadius);
        SeekBar seekBarRadius = findViewById(R.id.seekBarRadius);
        seekBarRadius.setOnSeekBarChangeListener(this);
        viewFirst =  findViewById(R.id.viewFirst);
        viewSecond =  findViewById(R.id.viewSecond);
        viewThird =  findViewById(R.id.viewThird);
        viewFourth =  findViewById(R.id.viewFourth);
        viewFifth =  findViewById(R.id.viewFifth);

        etCommon.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    etCommon.setSelection(etCommon.getText().length());
                }
            }
        });

        etCommon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvWordCount.setText(""+etCommon.getText().toString().length() +"/" + getString(R.string.twoFiftyWords));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b)
                {
                    if(!datePickerFragment.isResumed())
                    {
                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                    }
                }
            }
        });

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!datePickerFragment.isResumed())
                {
                    datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                }
            }
        });


        if(data != null && !data.equals(""))
        {
            etCommon.setText(data);
            tvWordCount.setText(""+data.length() +"/" + getString(R.string.twoFiftyWords));
        }

        if(editableParam.equals("radius"))
        {
            tvWordCount.setVisibility(View.GONE);
            etCommon.setEnabled(false);
            etCommon.setText("");
            etCommon.setHintTextColor(ContextCompat.getColor(this,R.color.lightTextColor));
            hideKeybord();
            layoutSeekBarRadius.setVisibility(View.VISIBLE);

            int minRadius = 0;
            int maxRadius = 30;

            try {
                minRadius = Integer.parseInt(intent.getStringExtra("minRadius"));
                maxRadius = Integer.parseInt(intent.getStringExtra("maxRadius"));
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            double commonValue =(double)(maxRadius - minRadius) / 5 ;
            r1 = minRadius ;
            double r2 =  r1 + commonValue ;
            double r3 =  r2 + commonValue;
            double r4 =  r3 + commonValue ;
            double r5 =  r4 + commonValue ;
            this.r2 = (int) r2;
            this.r3 = (int) r3;
            this.r4 = (int) r4;
            this.r5 = (int) r5;
            r6 =  maxRadius;

            TextView tvRadiusFirst = findViewById(R.id.tvRadiusFirst);
            TextView tvRadiusSecond = findViewById(R.id.tvRadiusSecond);
            TextView tvRadiusThree = findViewById(R.id.tvRadiusThree);
            TextView tvRadiusFour = findViewById(R.id.tvRadiusFour);
            TextView tvRadiusFive = findViewById(R.id.tvRadiusFive);
            TextView tvRadiusSix = findViewById(R.id.tvRadiusSix);

            tvRadiusFirst.setTypeface(fonRegular);
            tvRadiusSecond.setTypeface(fonRegular);
            tvRadiusThree.setTypeface(fonRegular);
            tvRadiusFour.setTypeface(fonRegular);
            tvRadiusFive.setTypeface(fonRegular);
            tvRadiusSix.setTypeface(fonRegular);

            tvRadiusFirst.setText(""+this.r1);
            tvRadiusSecond.setText(""+this.r2);
            tvRadiusThree.setText(""+this.r3);
            tvRadiusFour.setText(""+this.r4);
            tvRadiusFive.setText(""+this.r5);
            tvRadiusSix.setText(""+this.r6);

            setSeekBarValues(seekBarRadius,data);
        }
        else if(myListType.equals("2") && !fieldType.equals("1"))
        {
            etCommon.setVisibility(View.GONE);
            tvWordCount.setVisibility(View.GONE);

            switch (fieldType)
            {
                case "2":
                    String[] etFields = data.split(",");
                    for (String field : etFields) {
                        addRemoveDynamicView(field);
                    }

                    TextView tvAddDynamic = findViewById(R.id.tvAddDynamic);
                    tvAddDynamic.setVisibility(View.VISIBLE);
                    tvAddDynamic.setTypeface(Utility.getFontMedium(this));
                    tvAddDynamic.setOnClickListener(this);
                    tvAddDynamic.setText(getString(R.string.add) +" "+tvTitle.getText().toString());
                    break;

                case "3" :
                    hideKeybord();
                    etDate.setHint(intent.getStringExtra("hint"));
                    tilDate.setVisibility(View.VISIBLE);
                    datePickerFragment.setDatePickerType(3);
                    datePickerFragment.setCallBack(this);
                    break;

                case "4":
                    hideKeybord();
                    etDate.setHint(intent.getStringExtra("hint"));
                    tilDate.setVisibility(View.VISIBLE);
                    datePickerFragment.setDatePickerType(4);
                    datePickerFragment.setCallBack(this);
                    break;

                case "5":
                    hideKeybord();
                    preDefineds = new ArrayList<>();
                    sendingPredefinedValue = new ArrayList<>();
                    if(intent.getSerializableExtra("preDefined") != null)
                    {
                        preDefineds = (ArrayList<PreDefined>) intent.getSerializableExtra("preDefined");
                    }

                    sendingPredefinedValue.addAll(Arrays.asList(data.split(",")));

                    for(PreDefined preDefined : preDefineds)
                    {
                        if(sendingPredefinedValue.contains(preDefined.getName()))
                        {
                            addRemovePredefineView(preDefined.getName(),preDefined.getIcon(), true, true);
                        }
                        else
                        {
                            addRemovePredefineView(preDefined.getName(),preDefined.getIcon(), true, false);
                        }
                    }
                    break;

                case "6":
                    hideKeybord();
                    preDefineds = new ArrayList<>();
                    sendingPredefinedValue = new ArrayList<>();
                    if(intent.getSerializableExtra("preDefined") != null)
                    {
                        preDefineds = (ArrayList<PreDefined>) intent.getSerializableExtra("preDefined");
                    }

                    sendingPredefinedValue.addAll(Arrays.asList(data.split(",")));
                    for(PreDefined preDefined : preDefineds)
                    {
                        if(sendingPredefinedValue.contains(preDefined.getName()))
                        {
                            addRemovePredefineView(preDefined.getName(),"", false, true);
                        }
                        else
                        {
                            addRemovePredefineView(preDefined.getName(), "",false, false);
                        }
                    }
                    break;
            }
        }

    }

    private void hideKeybord() {
        etCommon.postDelayed(new Runnable() {
            @Override
            public void run() {
                Utility.hideKeyboad(MyListEditActivity.this);
            }
        },400);
    }

    private void addRemovePredefineView(final String field, String imgUrl,  boolean showIcon, boolean isAlreadySelected) {
        final View singleRowMyListPredefined = inflater.inflate(R.layout.single_row_my_list_predefined,null);
        TextView tvSelection = singleRowMyListPredefined.findViewById(R.id.tvSelection);
        LinearLayout llSelection = singleRowMyListPredefined.findViewById(R.id.llSelection);

        ImageView ivIcon = singleRowMyListPredefined.findViewById(R.id.ivIcon);
        final ImageView ivTick = singleRowMyListPredefined.findViewById(R.id.ivTick);
        llMyListEdit.addView(singleRowMyListPredefined);
        tvSelection.setText(field.trim());

        if(showIcon)
        {
            ivIcon.setVisibility(View.VISIBLE);
            if(!imgUrl.equals(""))
            {
                Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                        .error(R.drawable.upload_take_photo_icon)
                        .placeholder(R.drawable.upload_take_photo_icon))
                        .load(imgUrl)
                        .into(ivIcon);
            }
        }

        if(isAlreadySelected)
        {
            ivTick.setVisibility(View.VISIBLE);
        }


        llSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sendingPredefinedValue.contains(field))
                {
                    sendingPredefinedValue.remove(field);
                    ivTick.setVisibility(View.INVISIBLE);
                }
                else
                {
                    sendingPredefinedValue.add(field);
                    ivTick.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void addRemoveDynamicView(String value) {
        final View singleRowMyListEdit = inflater.inflate(R.layout.single_row_my_list_edit,null);
        final EditText etMyListEditDynamic = singleRowMyListEdit.findViewById(R.id.etMyListEditDynamic);

        ImageView ivDelete = singleRowMyListEdit.findViewById(R.id.ivDelete);
        llMyListEdit.addView(singleRowMyListEdit);
        etMyListEditDynamic.setText(value.trim());
        etMyListEditDynamics.add(etMyListEditDynamic);
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llMyListEdit.removeView(singleRowMyListEdit);
                etMyListEditDynamics.remove(etMyListEditDynamic);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    /**
     * set the values to the seekbar
     * @param seekBarRadius radius seekbar
     * @param progress radius for the exisiting value
     */
    private void setSeekBarValues(SeekBar seekBarRadius, String progress) {
        int radius = 10;

        try {
            radius = Integer.parseInt(progress);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(r1 >= radius)
        {
            seekBarRadius.setProgress(0);
            setViewColors(R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
            sendingRadiusvalue = ""+r1;
        }
        else if(r2 >= radius)
        {
            seekBarRadius.setProgress(6);
            setViewColors(R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
            sendingRadiusvalue = ""+r2;
        }
        else if(r3 >= radius)
        {
            seekBarRadius.setProgress(12);
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
            sendingRadiusvalue = ""+r3;
        }
        else if(r4 >= radius)
        {
            seekBarRadius.setProgress(18);
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider);
            sendingRadiusvalue = ""+r4;
        }
        else if(r5 >= radius)
        {
            seekBarRadius.setProgress(24);
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider);
            sendingRadiusvalue = ""+r5;
        }
        else if(r6 >= radius)
        {
            seekBarRadius.setProgress(30);
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
            sendingRadiusvalue = ""+r6;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvDone:
                try {
                    if(myListType.equals("1"))
                    {
                        if(editableParam.equals("radius"))
                        {
                            presenter.updateMyList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),editableParam,sendingRadiusvalue);
                        }
                        else
                        {
                            presenter.updateMyList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),editableParam,etCommon.getText().toString());
                        }
                    }
                    else
                    {
                        JSONObject jsonObjectRequest = new JSONObject();
                        JSONObject jsonObjectMetaData = new JSONObject();

                        switch (fieldType)
                        {
                            case "1":
                                jsonObjectMetaData.put("metaId",editableParam);
                                jsonObjectMetaData.put("data",etCommon.getText().toString());
                                break;

                            case "2":
                                jsonObjectMetaData.put("metaId",editableParam);
                                jsonObjectMetaData.put("data",presenter.getValuesFromDynamicFields(etMyListEditDynamics));
                                break;

                            case "3":
                            case "4":
                                jsonObjectMetaData.put("metaId",editableParam);
                                jsonObjectMetaData.put("data",sendingDate);
                                break;

                            case "5":
                            case "6":
                                jsonObjectMetaData.put("metaId",editableParam);
                                jsonObjectMetaData.put("data",presenter.converArrayListToStringComma(sendingPredefinedValue));
                                break;
                        }
                        jsonObjectRequest.put("metaData",jsonObjectMetaData);
                        Log.d("muras", "onClick: "+jsonObjectRequest);
                        presenter.updateMyList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObjectRequest);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case R.id.tvAddDynamic:
                addRemoveDynamicView("");
                break;

        }
    }

    @Override
    public void onSuccess(String msg) {

        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        VariableConstant.IS_MY_LIST_EDITED = true;
        closeActivity();

        AppController.getInstance().getMixpanelHelper().myListUpdated(editableParam);
    }

    @Override
    public void onEditTextEmpty() {
        if(myListType.equals("2") && fieldType.equals("2"))
        {
            Toast.makeText(this,getString(R.string.plsEnterAtleatOneValue),Toast.LENGTH_SHORT).show();
        }
        else
        {
            etCommon.setEnabled(true);
            etCommon.setError(getString(R.string.plsEnterValue));
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if(seekBar.getProgress() < 3 )
        {
            seekBar.setProgress(0);
            setViewColors(R.color.lightDivider,R.color.lightDivider,R.color.lightDivider,R.color.lightDivider,R.color.lightDivider);
            sendingRadiusvalue = ""+r1;
        }
        else if(seekBar.getProgress() >= 3 && seekBar.getProgress() < 9)
        {
            seekBar.setProgress(6);
            setViewColors(R.color.colorPrimary,R.color.lightDivider,R.color.lightDivider,R.color.lightDivider,R.color.lightDivider);
            sendingRadiusvalue = ""+r2;
        }
        else if(seekBar.getProgress() >= 9 && seekBar.getProgress() < 16)
        {
            seekBar.setProgress(12);
            setViewColors(R.color.colorPrimary,R.color.colorPrimary,R.color.lightDivider,R.color.lightDivider,R.color.lightDivider);
            sendingRadiusvalue = ""+r3;
        }
        else if(seekBar.getProgress() >= 16 && seekBar.getProgress() < 22)
        {
            seekBar.setProgress(18);
            setViewColors(R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary,R.color.lightDivider,R.color.lightDivider);
            sendingRadiusvalue = ""+r4;
        }
        else if(seekBar.getProgress() >= 22 && seekBar.getProgress() < 27)
        {
            seekBar.setProgress(24);
            setViewColors(R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary,R.color.lightDivider);
            sendingRadiusvalue = ""+r5;
        }
        else
        {
            seekBar.setProgress(30);
            setViewColors(R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary,R.color.colorPrimary);
            sendingRadiusvalue = ""+r6;
        }
    }

    /**
     * Change the color of the views in radius
     * @param v10 view of the radius 10
     * @param v15 view of the radius 15
     * @param v20 view of the radius 20
     * @param v25 view of the radius 25
     * @param v30 view of the radius 30
     */
    private  void setViewColors(int v10, int v15, int v20, int v25, int v30)
    {
        viewFirst.setBackgroundColor(ContextCompat.getColor(this,v10));
        viewSecond.setBackgroundColor(ContextCompat.getColor(this,v15));
        viewThird.setBackgroundColor(ContextCompat.getColor(this,v20));
        viewFourth.setBackgroundColor(ContextCompat.getColor(this,v25));
        viewFifth.setBackgroundColor(ContextCompat.getColor(this,v30));
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        sendingDate = sendingFormat;
        etDate.setText(displayFormat);
    }
}
