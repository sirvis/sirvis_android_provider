package com.sirvis.pro.main.schedule.addschedule;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.sirvis.pro.R;
import com.sirvis.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by murashid on 03-Oct-17.
 */

public class ScheduleSelectionPresenter implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "Presenter";
    private Context context;
    private  ScheduleSelectionPresenterImple presenterImple;
    private int datePickerVarID;
    private Date startDay;
    private long startTime;
    private SimpleDateFormat sendingFormat,displayFormat;

    ScheduleSelectionPresenter(Context context,ScheduleSelectionPresenterImple presenterImple)
    {
        this.context = context;
        this.presenterImple = presenterImple;
        datePickerVarID = 0;
        startDay = new Date();
        startTime = 0;
        sendingFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        displayFormat = new SimpleDateFormat("MMM yyyy", Locale.US);
    }

    /**
     * method for calculating the current date
     */
    void startDay()
    {
        Calendar cal = Calendar.getInstance();
        String startDate = String.format("%d-%02d-%02d",cal.get(Calendar.YEAR) ,cal.get(Calendar.MONTH)+1,cal.get(Calendar.DAY_OF_MONTH) );
        presenterImple.startDay(startDate);
    }

    /**
     * method for calculating the end days from adding remaining days of the month and param days
     * @param days days mention how many days we want to add extra with remain days of current month
     */
    void endDay(int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int monthMaxDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        int remainingdays = monthMaxDays - cal.get(Calendar.DAY_OF_MONTH);
        days = days + remainingdays;
        cal.add(Calendar.DATE, days);
        String endDate = String.format("%d-%02d-%02d",cal.get(Calendar.YEAR) ,cal.get(Calendar.MONTH)+1,cal.get(Calendar.DAY_OF_MONTH) );
        presenterImple.endDay(endDate);
    }

    /**
     * method for set minimu, time for already selected date
     * @param startDate start Date
     */
    void setMinimumDateForAlreadySelectedStartDate(String startDate)
    {
        try {
            startDay = sendingFormat.parse(startDate);
            startTime = startDay.getTime();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for showing date picker for selecting the start date and end date based on date Picker id
     * @param datePickerVarID id for datepicker 1 => start date 2=> endDate
     * @param lastSelectedDate last selected date
     */
    void selectDate(int datePickerVarID ,String lastSelectedDate) {
        this.datePickerVarID = datePickerVarID;
        Calendar calendar=Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = null;

        Log.d(TAG, "selectDate: "+lastSelectedDate);

        if(!lastSelectedDate.equals(""))
        {
            String[] lastSelected = lastSelectedDate.split("-");
            year = Integer.parseInt(lastSelected[0]);
            month = Integer.parseInt(lastSelected[1]) - 1;
            date = Integer.parseInt(lastSelected[2]);
        }

        if( datePickerVarID == 2)
        {
            if(startTime == 0)
            {
                datePickerDialog = new DatePickerDialog(context,this, year, month, date+1);
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis()-1000);
            }
            else
            {
                if(lastSelectedDate.equals(""))
                {
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    date = calendar.get(Calendar.DAY_OF_MONTH);
                }
                calendar.setTime(startDay);
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                datePickerDialog = new DatePickerDialog(context,this, year, month, date);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            }
        }
        else
        {
            datePickerDialog = new DatePickerDialog(context,this, year, month, date);
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis()-1000);
        }

        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

        if(datePickerVarID==1)
        {
            String startDate = String.format("%d-%02d-%02d",year ,monthOfYear + 1,dayOfMonth );

            datePickerVarID=0;

            try {
                startDay = sendingFormat.parse(startDate);
                startTime = startDay.getTime();
                presenterImple.startDayCalendar(startDate,Utility.getDayOfMonthSuffix(dayOfMonth,displayFormat.format(startDay)));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(datePickerVarID == 2)
        {
            String endDate = String.format("%d-%02d-%02d",year ,monthOfYear + 1,dayOfMonth );
            try {
                presenterImple.endDayCalendar(endDate,Utility.getDayOfMonthSuffix(dayOfMonth,displayFormat.format(sendingFormat.parse(endDate))));
                datePickerVarID=0;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    }

    /**
     * method for adding and removing the days as well as the change the image view
     * @param selectedDays list of already selected days
     * @param days current selected or unselected days
     * @param ivDay imageview of the current selected or unselected days
     */
    void addRemoveDays(ArrayList<String> selectedDays, String days , ImageView ivDay)
    {
        if(!selectedDays.contains(days))
        {
            presenterImple.addDays(days);
            ivDay.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.vector_color_primary_add_schedule_tick));
        }
        else
        {
            presenterImple.removeDays(days);
            ivDay.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.vector_add_schedule_tick_color_grey));
        }
    }

    /**
     * method for the sorting the days
     * @param selectedDays list of unsorted days
     * @return list of sorted days
     */
    ArrayList<String> sortDays(ArrayList<String> selectedDays) {
        ArrayList<String> sorteddays = new ArrayList<>();

        if (selectedDays.contains(context.getString(R.string.monday)))
        {
            sorteddays.add(context.getString(R.string.monday));
        }
        if(selectedDays.contains(context.getString(R.string.tuesday)))
        {
            sorteddays.add(context.getString(R.string.tuesday));
        }
        if(selectedDays.contains(context.getString(R.string.wednesday)))
        {
            sorteddays.add(context.getString(R.string.wednesday));
        }
        if(selectedDays.contains(context.getString(R.string.thursday)))
        {
            sorteddays.add(context.getString(R.string.thursday));
        }
        if(selectedDays.contains(context.getString(R.string.friday)))
        {
            sorteddays.add(context.getString(R.string.friday));
        }
        if(selectedDays.contains(context.getString(R.string.saturday)))
        {
            sorteddays.add(context.getString(R.string.saturday));
        }
        if(selectedDays.contains(context.getString(R.string.sunday)))
        {
            sorteddays.add(context.getString(R.string.sunday));
        }
       return sorteddays;
    }

    /**
     * ScheduleSelectionPresenterImple interface for view implementation
     */
    interface ScheduleSelectionPresenterImple
    {
        void startDay(String startDay);
        void endDay(String endDay);
        void startDayCalendar(String startDay, String displayStartDay);
        void endDayCalendar(String endDay, String displayEndDay);
        void addDays(String days);
        void removeDays(String days);
    }

}
