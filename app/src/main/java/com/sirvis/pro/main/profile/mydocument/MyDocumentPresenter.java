package com.sirvis.pro.main.profile.mydocument;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.ProfilePojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 22-Mar-18.
 */

public class MyDocumentPresenter {
    private static final String TAG = "SelectCategory";
    private View view;

    public MyDocumentPresenter(View view)
    {
        this.view = view;
    }

    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    public void getCategory(final String sessionToken)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        ProfilePojo profilePojo = gson.fromJson(result, ProfilePojo.class);
                                        view.onSuccess(profilePojo.getData());
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getCategory(newToken);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }


     /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    public void updateCategory(final String sessionToken, JSONObject jsonObject)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.UPDATE_DOCUMENT, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccess(jsonObject.getString("message"));
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getCategory(newToken);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    public interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ProfileData profileData);
        void onSuccess(String msg);
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
