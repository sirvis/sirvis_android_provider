package com.sirvis.pro.main.profile.helpcenter;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.helpcenter.TicketData;
import com.sirvis.pro.pojo.profile.helpcenter.TicketPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 29-Dec-17.
 */

public class NewTicketModel {

    private static final String TAG = "LoginModel";
    private NewTicketModelImple modelImplement;

    NewTicketModel(NewTicketModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    void createTicket(String sessionToken, JSONObject jsonObject)
    {
        try {
            if(jsonObject.getString("subject").equals(""))
            {
                modelImplement.onErrorSubject();
                return;
            }
            else if(jsonObject.getString("priority").equals(""))
            {
                modelImplement.onErrorPriority();
                return;
            }
            else if(jsonObject.getString("body").equals(""))
            {
                modelImplement.onErrorComment();
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.ZENDESK_RAISE_TICKET , OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccessRaiseTicket: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessRaiseTicket(jsonObject.getString("message"));

                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    void getTicketDetails(String sessionToken, String ticketId)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.ZENDESK_GET_TICKET_HISTORY+"/"+ticketId , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccessRaiseTicket: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            Gson gson = new Gson();
                            TicketPojo ticketPojo = gson.fromJson(result,TicketPojo.class);
                            modelImplement.onSuccessTicket(ticketPojo.getData());

                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    void sendComment(String sessionToken,JSONObject jsonObject)
    {
        try {
            if(jsonObject.getString("body").equals(""))
            {
                modelImplement.onErrorComment();
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.ZENDESK_PUT_TICKET_COMMENT , OkHttp3ConnectionStatusCode.Request_type.PUT, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccessRaiseTicket: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessComment();

                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    interface NewTicketModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccessRaiseTicket(String msg);
        void onSuccessComment();
        void onSuccessTicket(TicketData ticket);

        void onErrorSubject();
        void onErrorPriority();
        void onErrorComment();
    }
}
