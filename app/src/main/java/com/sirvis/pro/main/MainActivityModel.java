package com.sirvis.pro.main;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.appconfig.AppConfigData;
import com.sirvis.pro.pojo.appconfig.AppConfigMainPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 25-Oct-17.
 * Model for MainActivity
 * @see MainActivity
 */

public class MainActivityModel {

    private static final String TAG = "MainActivityModel";
    private MainActivityModelImple imple;
    MainActivityModel( MainActivityModelImple imple)
    {
        this.imple = imple;
    }

    /**
     * Method for calling api for getting the App Configuration
     * @param sessionToken session Token
     */
    void getAppConfig(String sessionToken)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CONFIG , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            AppConfigMainPojo pojo = gson.fromJson(result,AppConfigMainPojo.class);
                            imple.onSuccess(pojo.getData());
                        }
                       /* else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    getAppConfig(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {

                                }
                            });
                        }*/
                    } else {
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {

            }
        });

    }

    /**
     * interface for View Implementation in MainActivity
     */
    interface MainActivityModelImple
    {
        void onSuccess(AppConfigData appConfigData);
    }
}
