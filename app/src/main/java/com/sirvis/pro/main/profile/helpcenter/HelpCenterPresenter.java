package com.sirvis.pro.main.profile.helpcenter;

import com.sirvis.pro.pojo.profile.helpcenter.Ticket;

import java.util.ArrayList;

/**
 * Created by murashid on 10-Sep-17.
 */

public class HelpCenterPresenter implements HelpCenterModel.HelpCenterModelImple {

    private HelpCenterModel model;
    private HelpCenterPresenterImple presenterImple;

    HelpCenterPresenter(HelpCenterPresenterImple presenterImple) {
        model = new HelpCenterModel(this);
        this.presenterImple = presenterImple;
    }


    void getAllTickets(String sessionToken,String email)
    {
        presenterImple.startProgressBar();
        model.getAllTickets(sessionToken,email);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(ArrayList<Ticket> wholeTicketData) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(wholeTicketData);
    }

    interface HelpCenterPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(ArrayList<Ticket> wholeTicketData);
    }
}
