package com.sirvis.pro.main.history;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 13-Nov-17.
 * <h1>HistoryGraphModel</h1>
 * HistoryGraphModel model for history fragment
 */

public class HistoryGraphModel {
    private static final String TAG = "HistoryGraphModel";
    private HistoryModelImple modelImplement;



    HistoryGraphModel(HistoryModelImple modelImplement) {
        this.modelImplement = modelImplement;

    }

    void getHistoryWeek(final String sessionToken)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_HISTORY_BY_WEEK , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);

                        final JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.handleHistoryWeek(result);
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    getHistoryWeek(sessionToken);
                                    modelImplement.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    modelImplement.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    modelImplement.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            modelImplement.sessionExpired(jsonObject.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }




    /**
     *  Method for calling api for getting the history booking details based on the seleted week
     * @param sessionToken sessionToken
     * @param apiSelectedDate selected week
     * @see HistoryGraphFragment
     */
    void getHistory(final String sessionToken, final String apiSelectedDate)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING_HISTORY+"/"+apiSelectedDate , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        final JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessBooking(result);
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    getHistory(newToken,apiSelectedDate);
                                    modelImplement.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    modelImplement.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    modelImplement.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            modelImplement.sessionExpired(jsonObject.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    
    /**
     * MyEventsModelImple interface for presenter implementation
     */
    interface HistoryModelImple {
        void onFailure(String failureMsg);
        void onFailure();

        void onNewToken(String token);
        void sessionExpired(String msg);

        void handleHistoryWeek(String result);
        void onSuccessBooking(String result);
    }
}
