package com.sirvis.pro.main.acceptedbooking;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.BidBookingListAdapter;
import com.sirvis.pro.pojo.booking.AcceptedBookingPojo;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.model.CalendarEvent;
import devs.mulham.horizontalcalendar.utils.CalendarEventsPredicate;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;


public class AcceptedBookingFragment extends Fragment implements AcceptedBookingPresenter.View, SwipeRefreshLayout.OnRefreshListener, CalendarEventsPredicate {

    private static final String TAG = "AcceptedBooking";
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private AcceptedBookingPresenter presenter;

    private SwipeRefreshLayout srlBooking;
    private BidBookingListAdapter bookingListAdapter;
    private ArrayList<Booking> acceptedBookings;
    private ArrayList<Booking> acceptedBookingsPerWeek;
    private TextView tvNoBooking;

    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private Gson gson;

    private Calendar startDate, endDate, selectedDate;
    private List<CalendarEvent> calendarEvents;
    //private HashSet<Calendar> datesEvents;
    private HashSet<Long> bookingTimes;
    private int currentCalenderPosition = 34;
    private HorizontalCalendar horizontalCalendar;

    public static AcceptedBookingFragment newInstance() {
        return new AcceptedBookingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_accepted_booking, container, false);

        init(rootView);

        return rootView;
    }

    private void init(View rootView) {
        filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //notificationManager.cancelAll();
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_REFRESH_BOOKING)) {
                    /*// sessionManager.setIsAssignedBooking(false);
                     */
                    sessionManager.setIsNewBooking(false);
                    endDate = Calendar.getInstance();
                    startDate = Calendar.getInstance();
                    selectedDate = Calendar.getInstance();

                    Log.d(TAG, "init: " + sessionManager.getLastBooking().toString());
                    try {
                        JSONObject newBookingJson = new JSONObject(sessionManager.getLastBooking().toString());
                        String bookingRequestedFor = newBookingJson.optString("bookingRequestedFor", "");
                        selectedDate.setTimeInMillis(Long.parseLong(bookingRequestedFor) * 1000L);
                        startDate.setTimeInMillis((Long.parseLong(bookingRequestedFor) - 86400 * 3) * 1000L);
                        endDate.setTimeInMillis((Long.parseLong(bookingRequestedFor) + 86400 * 3) * 1000L);
                        Log.d("selectedTime: ", "" + selectedDate.get(Calendar.DAY_OF_MONTH));
                        int hoursOfTheDay = startDate.get(Calendar.HOUR_OF_DAY);
                        int minutesOfTheDay = startDate.get(Calendar.MINUTE);
                        startDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
                        startDate.add(Calendar.MINUTE, -minutesOfTheDay);
                        endDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
                        endDate.add(Calendar.MINUTE, -minutesOfTheDay);
                        selectedDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
                        selectedDate.add(Calendar.MINUTE, -minutesOfTheDay);
                        progressDialog.setMessage(getString(R.string.refreshing));
                        presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), String.valueOf((startDate.getTimeInMillis() / 1000)), String.valueOf((endDate.getTimeInMillis() / 1000)), false);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                horizontalCalendar.selectDate(selectedDate, true);
                            }
                        }, 500);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_CANCEL_BOOKING)) {
                    progressDialog.setMessage(getString(R.string.refreshing));
                    presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), String.valueOf((startDate.getTimeInMillis() / 1000)), String.valueOf((endDate.getTimeInMillis() / 1000)), false);
                }
            }
        };

        presenter = new AcceptedBookingPresenter(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(getActivity());
        progressDialog.setMessage(getString(R.string.gettingBookings));

        TextView tvTitle = rootView.findViewById(R.id.tvTitle);
        tvNoBooking = rootView.findViewById(R.id.tvNoBooking);
        tvNoBooking.setTypeface(Utility.getFontMedium(getActivity()));
        acceptedBookings = new ArrayList<>();
        acceptedBookingsPerWeek = new ArrayList<>();
        bookingListAdapter = new BidBookingListAdapter(getActivity(), acceptedBookings, "accepted");
        RecyclerView rvBooking = rootView.findViewById(R.id.rvBooking);
        rvBooking.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBooking.setAdapter(bookingListAdapter);
        bookingListAdapter.notifyDataSetChanged();
        gson = new Gson();

        tvTitle.setTypeface(Utility.getFontBold(getContext()));
        tvNoBooking.setTypeface(Utility.getFontRegular(getContext()));

        srlBooking = rootView.findViewById(R.id.srlBooking);
        srlBooking.setOnRefreshListener(this);

        endDate = Calendar.getInstance();
        startDate = Calendar.getInstance();

        startDate.add(Calendar.MONTH, -1);
        endDate.add(Calendar.MONTH, +1);

        calendarEvents = new ArrayList<>();
        bookingTimes = new HashSet<>();

        HorizontalCalendar.Builder horizontalCalendarBuiler = new HorizontalCalendar.Builder(rootView, R.id.calendarView)
                .range(startDate, endDate)
                //.addEvents(this)
                .datesNumberOnScreen(7);

        horizontalCalendar = horizontalCalendarBuiler.build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                selectedDate.setTime(date.getTime());
                updateBookingPerDay(false);
                if ((currentCalenderPosition - position) > 2 || (currentCalenderPosition - position) < -2) {
                    currentCalenderPosition = position;
                    startDate.setTime(date.getTime());
                    endDate.setTime(date.getTime());
                    startDate.add(Calendar.DAY_OF_MONTH, -3);
                    endDate.add(Calendar.DAY_OF_MONTH, 3);
                    presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), String.valueOf((startDate.getTimeInMillis() / 1000)), String.valueOf((endDate.getTimeInMillis() / 1000)), false);
                }
            }
        });


        if (sessionManager.getIsNewBookingFromMain()) {
            sessionManager.setNewBookingFromMain(false);
            endDate = Calendar.getInstance();
            startDate = Calendar.getInstance();
            selectedDate = Calendar.getInstance();

            Log.d(TAG, "init: " + sessionManager.getLastBooking().toString());
            try {
                JSONObject newBookingJson = new JSONObject(sessionManager.getLastBooking().toString());
                String bookingRequestedFor = newBookingJson.optString("bookingRequestedFor", "");
                selectedDate.setTimeInMillis(Long.parseLong(bookingRequestedFor) * 1000L);
                startDate.setTimeInMillis((Long.parseLong(bookingRequestedFor) - 86400 * 3) * 1000L);
                endDate.setTimeInMillis((Long.parseLong(bookingRequestedFor) + 86400 * 3) * 1000L);
                Log.d("selectedTime: ", "" + selectedDate.get(Calendar.DAY_OF_MONTH));
                int hoursOfTheDay = startDate.get(Calendar.HOUR_OF_DAY);
                int minutesOfTheDay = startDate.get(Calendar.MINUTE);
                startDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
                startDate.add(Calendar.MINUTE, -minutesOfTheDay);
                endDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
                endDate.add(Calendar.MINUTE, -minutesOfTheDay);
                selectedDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
                selectedDate.add(Calendar.MINUTE, -minutesOfTheDay);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        horizontalCalendar.selectDate(selectedDate, true);
                    }
                }, 500);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            endDate = Calendar.getInstance();
            startDate = Calendar.getInstance();
            selectedDate = Calendar.getInstance();

            int hoursOfTheDay = startDate.get(Calendar.HOUR_OF_DAY);
            int minutesOfTheDay = startDate.get(Calendar.MINUTE);
            startDate.add(Calendar.DAY_OF_MONTH, -3);
            endDate.add(Calendar.DAY_OF_MONTH, 3);
            startDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
            startDate.add(Calendar.MINUTE, -minutesOfTheDay);
            endDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
            endDate.add(Calendar.MINUTE, -minutesOfTheDay);
            selectedDate.add(Calendar.HOUR_OF_DAY, -hoursOfTheDay);
            selectedDate.add(Calendar.MINUTE, -minutesOfTheDay);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    horizontalCalendar.selectDate(selectedDate, true);
                }
            }, 500);

        }
        /*if(sessionManager.getAcceptedBooking().equals(""))
        {*/
        presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), String.valueOf((startDate.getTimeInMillis() / 1000)), String.valueOf((endDate.getTimeInMillis() / 1000)), false);
      /*  }
        else
        {
            presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),String.valueOf((startDate.getTimeInMillis()/1000)), String.valueOf((endDate.getTimeInMillis()/1000)),true);
            onSuccesBooking(sessionManager.getAcceptedBooking(),false);
       }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, filter);
        VariableConstant.IS_ACCEPTEDBOOKING_OPENED = true;
        if (VariableConstant.IS_BOOKING_UPDATED) {
            VariableConstant.IS_BOOKING_UPDATED = false;
            progressDialog.setMessage(getString(R.string.refreshing));
            presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), String.valueOf((startDate.getTimeInMillis() / 1000)), String.valueOf((endDate.getTimeInMillis() / 1000)), false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        AppController.getInstance().stopNewBookingRingtoneService();
        getActivity().unregisterReceiver(receiver);
        VariableConstant.IS_ACCEPTEDBOOKING_OPENED = false;
    }

    @Override
    public void onDestroyView() {
        presenter.detach();
        super.onDestroyView();
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
        srlBooking.setRefreshing(false);
    }


    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, getActivity());
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(getActivity(),getActivity().getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }

    @Override
    public void onSuccesBooking(final String result, boolean isFromApi) {
        AcceptedBookingPojo bookingPojo = gson.fromJson(result, AcceptedBookingPojo.class);
        if (isAdded()) {
            /*if (!sessionManager.getAcceptedBooking().equals(result)) {
                sessionManager.setAcceptedBooking(result);
            }*/
            acceptedBookingsPerWeek.clear();
            acceptedBookingsPerWeek.addAll(bookingPojo.getData());
            updateBookingPerDay(true);

            if (bookingPojo.getData().size() > 0) {
               // tvNoBooking.setVisibility(View.GONE);
                sessionManager.setIsDriverOnJob(true);
            } else {
           //     tvNoBooking.setVisibility(View.VISIBLE);
                sessionManager.setIsDriverOnJob(false);
            }
            StringBuilder bookingStr = new StringBuilder();
            String prefix = "";
            for (Booking booking : bookingPojo.getData()) {
                bookingStr.append(prefix);
                prefix = ",";
                bookingStr.append(booking.getBookingId()).append("|").append(booking.getStatus());
            }
            sessionManager.setBookingStr(bookingStr.toString());

        }
    }

    private void updateBookingPerDay(boolean isFromWeek) {
        try {
            acceptedBookings.clear();
            long selectedStatTime = (long) selectedDate.getTimeInMillis() / 1000;
            long selectedEndTime = selectedStatTime + 86400;
            for (Booking booking : acceptedBookingsPerWeek) {
                long bookingTime = Long.parseLong(booking.getEventStartTime());
                bookingTimes.add(bookingTime);
                if (bookingTime >= selectedStatTime && bookingTime <= selectedEndTime) {
                    acceptedBookings.add(booking);
                }
            }
            if (acceptedBookings.size() > 0) {
                tvNoBooking.setVisibility(View.GONE);
            } else {
                tvNoBooking.setVisibility(View.VISIBLE);
            }

            bookingListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void onRefresh() {
        presenter.getBookingByDate(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), String.valueOf((startDate.getTimeInMillis() / 1000)), String.valueOf((endDate.getTimeInMillis() / 1000)), true);
    }

    @Override
    public List<CalendarEvent> events(Calendar date) {
        Log.d(TAG, "events: " + date.getTime());
        calendarEvents.clear();

        long selectedStatTime = (long) date.getTimeInMillis() / 1000;
        long selectedEndTime = selectedStatTime + 86400;
        for (long bookingTime : bookingTimes) {
            if (bookingTime >= selectedStatTime && bookingTime <= selectedEndTime) {
                calendarEvents.add(new CalendarEvent(Color.parseColor("#2A5634"), "event"));
            }
        }
        return calendarEvents;
    }
}
