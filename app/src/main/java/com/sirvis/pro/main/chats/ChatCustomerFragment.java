package com.sirvis.pro.main.chats;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ViewPagerAdapter;
import com.sirvis.pro.pojo.chat.ChatCustomerListPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatCustomerFragment extends Fragment implements ChatCutomerPresenter.View {

    private static final String TAG = "ChatCustomerFragment";
    private Typeface fondBold, fontRegular;
    private LayoutInflater inflater;

    private ChatCustomerListFragment activeChatFragment, pastChatFragment;
    private TextView tvCountActiveChat, tvCountPastChat;

    private SessionManager sessionManager;
    private ChatCutomerPresenter presenter;
    private ProgressDialog progressDialog;
    private Gson gson;

    private BroadcastReceiver receiver;
    private TabLayout tablayoutChat;

    public static ChatCustomerFragment newInstance() {
        return new ChatCustomerFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chat_customer, container, false);
        this.inflater = inflater;
        init(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (tablayoutChat != null) {
            TabLayout.Tab tab = tablayoutChat.getTabAt(0);
            tab.select();
        }

    }

    private void init(View view) {
        presenter = new ChatCutomerPresenter(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(getActivity());
        gson = new Gson();

        IntentFilter filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_NEW_CHAT);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_NEW_CHAT)) {
                    activeChatFragment.notifyDataSetChanged();
                } else {
                    presenter.getCustomerList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
                }
            }
        };
        getActivity().registerReceiver(receiver, filter);


        fondBold = Utility.getFontBold(getActivity());
        fontRegular = Utility.getFontRegular(getActivity());

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        final SearchView svChat = view.findViewById(R.id.svChat);
        tablayoutChat = view.findViewById(R.id.tablayoutChat);
        ViewPager vpChat = view.findViewById(R.id.vpChat);

        tvTitle.setTypeface(fondBold);
        activeChatFragment = ChatCustomerListFragment.newInstance(false);
        pastChatFragment = ChatCustomerListFragment.newInstance(true);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(activeChatFragment, "");
        viewPagerAdapter.addFragment(pastChatFragment, "");
        vpChat.setAdapter(viewPagerAdapter);
        tablayoutChat.setupWithViewPager(vpChat);
        for (int i = 0; i < tablayoutChat.getTabCount(); i++) {
            tablayoutChat.getTabAt(i).setCustomView(getCustomTabView(i));
        }

        svChat.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return true;
            }
        });

        svChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                svChat.setIconified(false);
            }
        });

        svChat.postDelayed(new Runnable() {
            @Override
            public void run() {
                svChat.setIconified(false);
                svChat.clearFocus();
            }
        }, 300);


        if (sessionManager.getChatCustomerList().equals("")) {
            presenter.getCustomerList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
        } else {
            presenter.getCustomerList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onSuccess(sessionManager.getChatCustomerList(), false);
                }
            }, 750);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detach();
        getActivity().unregisterReceiver(receiver);
    }

    private void filter(String query) {
        activeChatFragment.filter(query);
        pastChatFragment.filter(query);
    }

    private View getCustomTabView(int i) {
        View view = inflater.inflate(R.layout.custom_tab_view_header_with_count, null, false);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        TextView tvCount = view.findViewById(R.id.tvCount);
        tvHeader.setTypeface(fondBold);
        tvCount.setTypeface(fontRegular);
        if (i == 0) {
            tvHeader.setText(getString(R.string.activeChats).toUpperCase());
            tvCountActiveChat = tvCount;
        } else {
            tvHeader.setText(getString(R.string.pastChats).toUpperCase());
            tvCountPastChat = tvCount;
            tvCountPastChat.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(String chatPojo, boolean isFromApi) {

        if (isFromApi && sessionManager.getChatCustomerList().equals(chatPojo)) {
            return;
        }
        sessionManager.setChatCustomerList(chatPojo);
        ChatCustomerListPojo chatCustomerListPojo = gson.fromJson(chatPojo, ChatCustomerListPojo.class);
        tvCountActiveChat.setText("" + chatCustomerListPojo.getData().getAccepted().size());
        tvCountPastChat.setText("" + chatCustomerListPojo.getData().getPast().size());
        activeChatFragment.notifyiDataChanged(chatCustomerListPojo.getData().getAccepted());
        pastChatFragment.notifyiDataChanged(chatCustomerListPojo.getData().getPast());
    }


    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, getActivity());
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(getActivity(),getActivity().getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }


    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

}
