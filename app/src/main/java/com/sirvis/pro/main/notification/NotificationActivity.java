package com.sirvis.pro.main.notification;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.NotificatonAdapter;
import com.sirvis.pro.pojo.notification.NotificationData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

public class NotificationActivity extends AppCompatActivity implements NotificationPresenter.View {

    private ProgressBar progressBar;
    private TextView tvNoNotification;
    
    private NotificationPresenter presenter;
    private SessionManager sessionManager;

    private ArrayList<NotificationData> notificationData;
    private NotificatonAdapter notificatonAdapter;

    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int skip = 0 , limit = 20;
    private boolean toLoad = true;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        init();
    }

    /**
     * <h2>initViews</h2>
     * <p>
     *     method to notify adapter or update views if the
     *     transactions list size changed
     * </p>
     */
    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new NotificationPresenter(this);
        notificationData = new ArrayList<>();

        tvNoNotification =  findViewById(R.id.tvNoNotification);
        progressBar =  findViewById(R.id.progressBar);
        tvNoNotification.setTypeface(Utility.getFontRegular(this));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.notification));
        tvTitle.setTypeface(Utility.getFontBold(this));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        RecyclerView rvNotification = (RecyclerView) findViewById(R.id.rvNotification);
        rvNotification.setLayoutManager( linearLayoutManager);
        notificatonAdapter = new NotificatonAdapter(this, notificationData);
        rvNotification.setAdapter(notificatonAdapter);

        rvNotification.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (toLoad)
                    {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            toLoad = false;
                           getNotificationList();
                        }
                    }
                }
            }
        });

        getNotificationList();
    }

    private void getNotificationList()
    {
        presenter.getNotificationList(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), skip, limit );
        Log.d("Notification", "onScrolled: "+ skip + "  "+ limit);
        skip  = skip + limit;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        finish();
        overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
    }

    @Override
    public void startProgressBar() {
        if(progressBar!=null)
        {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void stopProgressBar() {
        toLoad = true;
        if(progressBar !=null )
        {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(ArrayList<NotificationData> notificationData) {
        this.notificationData.addAll(notificationData);
        notificatonAdapter.notifyDataSetChanged();

        if(this.notificationData.size() > 0)
        {
            tvNoNotification.setVisibility(View.GONE);
        }
        else
        {
            tvNoNotification.setVisibility(View.VISIBLE);
        }
        
    }


    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

}
