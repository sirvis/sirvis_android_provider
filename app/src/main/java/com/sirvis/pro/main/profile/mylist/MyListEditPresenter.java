package com.sirvis.pro.main.profile.mylist;

import android.widget.EditText;

import com.sirvis.pro.pojo.profile.service.MyListService;
import com.sirvis.pro.pojo.profile.service.MyListServiceEdit;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListEditPresenter</h1>
 * MyListEditPresenter presenter for MyListEditActivity
 * @see MyListEditActivity
 */

public class MyListEditPresenter implements MyListEditModel.MyListEditModelImple {

    private MyListEditModel model;
    private MyListEditPresenterImple presenterImple;

    MyListEditPresenter(MyListEditPresenterImple presenterImple) {
        model = new MyListEditModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passsing values from view to model
     * @param sessiontoken session Token
     * @param key key
     * @param value values
     */
    void updateMyList(String sessiontoken,String key,String value)
    {
        presenterImple.startProgressBar();
        model.updateMyList(sessiontoken,key,value);
    }

    /**
     * method for passsing values from view to model
     * @param sessiontoken key
     * @param jsonObject jsonObject
     */
    void updateMyList(String sessiontoken, JSONObject jsonObject)
    {
        presenterImple.startProgressBar();
        model.updateMyList(sessiontoken, jsonObject);
    }

    String getValuesFromDynamicFields(ArrayList<EditText> etMyListEditDynamics) {
        StringBuilder values = new StringBuilder("");
        String prefix = "";
        for (EditText editText: etMyListEditDynamics)
        {
            if(!editText.getText().toString().equals(""))
            {
                values.append(prefix);
                prefix = ", ";
                values.append(editText.getText().toString());
            }
        }
        return values.toString();
    }

    String converArrayListToStringComma(ArrayList<String> stringArrayList) {
        String value="",prefix = "";
        for(String va: stringArrayList)
        {
            value = value + prefix + va;
            prefix = ",";
        }
        if(value.charAt(0) == 44)
        {
            value = value.substring(1);
        }
        return value;
    }


    /**
     * method for passsing values from view to model
     * @param sessiontoken session Token
     * @param myListServiceEdits new List
     */
    void updateServiceCategory(String sessiontoken, ArrayList<MyListServiceEdit> myListServiceEdits)
    {
        presenterImple.startProgressBar();
        model.updateServiceCategory(sessiontoken,myListServiceEdits);
    }

    /**
     * method for creating MyListServiceEdit
     * @param myListServices list of  MyListService
     * @return list of MyListServiceEdit
     */
    ArrayList<MyListServiceEdit> addServiceEdit(ArrayList<MyListService> myListServices)
    {
        ArrayList<MyListServiceEdit> myListServiceEdits = new ArrayList<>();
        for (MyListService myListService: myListServices)
        {
            MyListServiceEdit myListServiceEdit = new MyListServiceEdit();
            myListServiceEdit.setId(myListService.getId());
            myListServiceEdit.setPrice(myListService.getPrice());
            myListServiceEdits.add(myListServiceEdit);
        }
        return  myListServiceEdits;
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg);
    }

    @Override
    public void onEditTextEmpty() {
        presenterImple.stopProgressBar();
        presenterImple.onEditTextEmpty();
    }

    /**
     * MyListEditPresenterImple interface for view implementation
     */
    interface MyListEditPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(String msg);
        void onEditTextEmpty();
    }
}
