package com.sirvis.pro.main.profile.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CardListAdapter;
import com.sirvis.pro.pojo.profile.wallet.CardData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class CardListActivity extends AppCompatActivity implements View.OnClickListener, CardListAdapter.CardClickListener, ChangeListPresenter.View {
    private ProgressDialog progressDialog;
    private ChangeListPresenter presenter;
    private SessionManager sessionManager;

    private ArrayList<CardData> cardData;
    private CardListAdapter cardListAdapter;
    private int selectedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p>
     * initialize the views
     * </p>
     */
    private void initialize() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        presenter = new ChangeListPresenter(this);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.selectCard));
        tvTitle.setTypeface(fontBold);

        TextView tvAddCard = findViewById(R.id.tvAddCard);
        tvAddCard.setTypeface(fontMedium);
        tvAddCard.setOnClickListener(this);

        RecyclerView rvCard = findViewById(R.id.rvCard);
        rvCard.setLayoutManager(new LinearLayoutManager(this));
        cardData = new ArrayList<>();
        cardListAdapter = new CardListAdapter(this, cardData,sessionManager,this);
        rvCard.setAdapter(cardListAdapter);

        presenter.getCardDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(VariableConstant.IS_CARD_UPDATED)
        {
            VariableConstant.IS_CARD_UPDATED = false;
            presenter.getCardDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
    }

    /**
     * <h1>onClick</h1>
     * <p>
     * Called when a view has been clicked.
     * </p>
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvAddCard:
               Intent intent = new Intent(CardListActivity.this, AddCardActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onCardSelect(int position) {
        selectedPosition = position;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cardId", cardData.get(position).getId());
            presenter.selectCard(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCardDelete(int position) {
        selectedPosition = position;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cardId", cardData.get(position).getId());
            presenter.deleteCard(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccessCardDetails(ArrayList<CardData> data) {
        this.cardData.clear();
        this.cardData.addAll(data);
        if(cardData.size() == 0)
        {
            resetSelectedCard();
        }
        cardListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessSelectCard(String msg) {
        for(int i = 0; i< cardData.size() ; i++ )
        {
            if(i == selectedPosition)
            {
                cardData.get(i).setDefault(true);
            }
            else
            {
                cardData.get(i).setDefault(false);
            }
        }
        cardListAdapter.notifyDataSetChanged();
        closeActivity();
    }

    @Override
    public void onSuccessDeleteCard(String msg) {
        if(sessionManager.getSelectedCardId().equals(cardData.get(selectedPosition).getId()))
        {
            resetSelectedCard();
        }
        cardData.remove(selectedPosition);
        cardListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    private void resetSelectedCard()
    {
        sessionManager.setSelectedCardId("");
        sessionManager.setSelectedCardNumber("");
        sessionManager.setCardBrand("");
    }
}
