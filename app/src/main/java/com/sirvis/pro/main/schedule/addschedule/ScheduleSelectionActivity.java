package com.sirvis.pro.main.schedule.addschedule;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by murashid on 02-Oct-17.
 * <h1>ScheduleSelectionActivity</h1>
 * ScheduleSelectionActivity used to showing the choosable value for creating the schedule
 */

public class ScheduleSelectionActivity extends AppCompatActivity implements ScheduleSelectionPresenter.ScheduleSelectionPresenterImple {

    private static final String TAG = "SelectionActivity";
    private Typeface fontMedium;

    private int selectedMode;
    private ArrayList<String> selectedDays;

    private int selectedMonth;
    private String startDate, endDate;

    private TextView tvStartDatePicker, tvEndDatePicker;
    private ScheduleSelectionPresenter presenter;
    private String values = "";
    private String selectedDuration = "";

    private ImageView ivTempDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_selection);

        init();
    }

    /**
     * inti the values
     */
    private void init() {
        presenter = new ScheduleSelectionPresenter(this, this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }

        fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);
        selectedMode = 0;
        selectedDays = new ArrayList<>();
        selectedMonth = 0;
        startDate = "";
        endDate = "";

        CardView cvRepeat = (CardView) findViewById(R.id.cvRepeat);
        CardView cvEveryDays = (CardView) findViewById(R.id.cvEveryDays);
        CardView cvWeekDays = (CardView) findViewById(R.id.cvWeekDays);
        CardView cvWeekEnds = (CardView) findViewById(R.id.cvWeekEnds);
        CardView cvSlotDuration = (CardView) findViewById(R.id.cvSlotDuration);
        LinearLayout llDuration = (LinearLayout) findViewById(R.id.llDuration);

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvDone = (TextView) findViewById(R.id.tvDone);

        tvTitle.setTypeface(fontBold);
        tvDone.setTypeface(fontBold);
        tvDone.setText(getString(R.string.done));

        final int type = getIntent().getIntExtra("type", 0);
        values = getIntent().getStringExtra("values");

        Log.d(TAG, "init: " + values);

        switch (type) {
            case 1:
                tvTitle.setText(getString(R.string.repeat));
                tvDone.setVisibility(View.GONE);
                cvRepeat.setVisibility(View.VISIBLE);
                cvEveryDays.setVisibility(View.GONE);
                cvWeekDays.setVisibility(View.GONE);
                cvWeekEnds.setVisibility(View.GONE);
                llDuration.setVisibility(View.GONE);
                cvSlotDuration.setVisibility(View.GONE);
                initRepeatView();
                break;

            case 2:
                tvTitle.setText(getString(R.string.everyDays));
                cvRepeat.setVisibility(View.GONE);
                cvEveryDays.setVisibility(View.VISIBLE);
                cvWeekDays.setVisibility(View.GONE);
                cvWeekEnds.setVisibility(View.GONE);
                llDuration.setVisibility(View.GONE);
                cvSlotDuration.setVisibility(View.GONE);
                initEveryDays();
                break;

            case 3:
                tvTitle.setText(getString(R.string.weekDays));
                cvRepeat.setVisibility(View.GONE);
                cvEveryDays.setVisibility(View.GONE);
                cvWeekDays.setVisibility(View.VISIBLE);
                cvWeekEnds.setVisibility(View.GONE);
                llDuration.setVisibility(View.GONE);
                cvSlotDuration.setVisibility(View.GONE);
                initWeekDayView();
                break;


            case 4:
                tvTitle.setText(getString(R.string.weekEnds));
                cvRepeat.setVisibility(View.GONE);
                cvEveryDays.setVisibility(View.GONE);
                cvWeekDays.setVisibility(View.GONE);
                cvWeekEnds.setVisibility(View.VISIBLE);
                llDuration.setVisibility(View.GONE);
                cvSlotDuration.setVisibility(View.GONE);
                initWeekEndsView();
                break;

            case 5:
                tvTitle.setText(getString(R.string.duration));
                cvRepeat.setVisibility(View.GONE);
                cvEveryDays.setVisibility(View.GONE);
                cvWeekDays.setVisibility(View.GONE);
                cvWeekEnds.setVisibility(View.GONE);
                llDuration.setVisibility(View.VISIBLE);
                cvSlotDuration.setVisibility(View.GONE);
                initDurationView();
                break;

            case 6:
                tvTitle.setText(getString(R.string.slotDuration));
                tvDone.setVisibility(View.GONE);
                cvRepeat.setVisibility(View.GONE);
                cvEveryDays.setVisibility(View.GONE);
                cvWeekDays.setVisibility(View.GONE);
                cvWeekEnds.setVisibility(View.GONE);
                llDuration.setVisibility(View.GONE);
                cvSlotDuration.setVisibility(View.VISIBLE);
                initSlotDurationView();
                break;


            default:

                break;
        }


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setValues(type);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
        }
    }

    /**
     * init the Repeat mode views
     */
    private void initRepeatView() {
        TextView tvEveryDay = (TextView) findViewById(R.id.tvEveryDay);
        tvEveryDay.setTypeface(fontMedium);

        TextView tvWeekDays = (TextView) findViewById(R.id.tvWeekDays);
        tvWeekDays.setTypeface(fontMedium);

        TextView tvWeekEnds = (TextView) findViewById(R.id.tvWeekEnds);
        tvWeekEnds.setTypeface(fontMedium);

        TextView tvSelectDays = (TextView) findViewById(R.id.tvSelectDays);
        tvSelectDays.setTypeface(fontMedium);


        final ImageView ivEveryDay = (ImageView) findViewById(R.id.ivEveryDay);
        final ImageView ivWeekDays = (ImageView) findViewById(R.id.ivWeekDays);
        final ImageView ivWeekEnds = (ImageView) findViewById(R.id.ivWeekEnds);
        final ImageView ivSelectDays = (ImageView) findViewById(R.id.ivSelectDays);

        if (values != null && !values.equals("")) {
            if (tvEveryDay.getText().toString().equals(values)) {
                ivEveryDay.setVisibility(View.VISIBLE);
                ivWeekDays.setVisibility(View.GONE);
                ivWeekEnds.setVisibility(View.GONE);
                ivSelectDays.setVisibility(View.GONE);
            } else if (tvWeekDays.getText().toString().equals(values)) {
                ivEveryDay.setVisibility(View.GONE);
                ivWeekDays.setVisibility(View.VISIBLE);
                ivWeekEnds.setVisibility(View.GONE);
                ivSelectDays.setVisibility(View.GONE);
            } else if (tvWeekEnds.getText().toString().equals(values)) {
                ivEveryDay.setVisibility(View.GONE);
                ivWeekDays.setVisibility(View.GONE);
                ivWeekEnds.setVisibility(View.VISIBLE);
                ivSelectDays.setVisibility(View.GONE);
            } else {
                ivEveryDay.setVisibility(View.GONE);
                ivWeekDays.setVisibility(View.GONE);
                ivWeekEnds.setVisibility(View.GONE);
                ivSelectDays.setVisibility(View.VISIBLE);
            }
        }

        tvEveryDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivEveryDay.setVisibility(View.VISIBLE);
                ivWeekDays.setVisibility(View.GONE);
                ivWeekEnds.setVisibility(View.GONE);
                ivSelectDays.setVisibility(View.GONE);
                selectedMode = 1;
                setValues(1);
            }
        });

        tvWeekDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivEveryDay.setVisibility(View.GONE);
                ivWeekDays.setVisibility(View.VISIBLE);
                ivWeekEnds.setVisibility(View.GONE);
                ivSelectDays.setVisibility(View.GONE);
                selectedMode = 2;
                setValues(1);
            }
        });

        tvWeekEnds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivEveryDay.setVisibility(View.GONE);
                ivWeekDays.setVisibility(View.GONE);
                ivWeekEnds.setVisibility(View.VISIBLE);
                ivSelectDays.setVisibility(View.GONE);
                selectedMode = 3;
                setValues(1);
            }
        });

        tvSelectDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivEveryDay.setVisibility(View.GONE);
                ivWeekDays.setVisibility(View.GONE);
                ivWeekEnds.setVisibility(View.GONE);
                ivSelectDays.setVisibility(View.VISIBLE);
                selectedMode = 4;
                setValues(1);
            }
        });

    }

    /**
     * init the Eveyday mode views
     */
    private void initEveryDays() {
        TextView tvEveryMonday = (TextView) findViewById(R.id.tvEveryMonday);
        tvEveryMonday.setTypeface(fontMedium);
        TextView tvEveryTuesDay = (TextView) findViewById(R.id.tvEveryTuesDay);
        tvEveryTuesDay.setTypeface(fontMedium);
        TextView tvEveryWednesday = (TextView) findViewById(R.id.tvEveryWednesday);
        tvEveryWednesday.setTypeface(fontMedium);
        TextView tvEveryThursday = (TextView) findViewById(R.id.tvEveryThursday);
        tvEveryThursday.setTypeface(fontMedium);
        TextView tvEveryFriday = (TextView) findViewById(R.id.tvEveryFriday);
        tvEveryFriday.setTypeface(fontMedium);
        TextView tvEverySaturday = (TextView) findViewById(R.id.tvEverySaturday);
        tvEverySaturday.setTypeface(fontMedium);
        TextView tvEverySunday = (TextView) findViewById(R.id.tvEverySunday);
        tvEverySunday.setTypeface(fontMedium);

        final ImageView ivEveryMonday = (ImageView) findViewById(R.id.ivEveryMonday);
        final ImageView ivEveryTuesday = (ImageView) findViewById(R.id.ivEveryTuesday);
        final ImageView ivEveryWednesday = (ImageView) findViewById(R.id.ivEveryWednesday);
        final ImageView ivEveryThursday = (ImageView) findViewById(R.id.ivEveryThursday);
        final ImageView ivEveryFriday = (ImageView) findViewById(R.id.ivEveryFriday);
        final ImageView ivEverySaturday = (ImageView) findViewById(R.id.ivEverySaturday);
        final ImageView ivEverySunday = (ImageView) findViewById(R.id.ivEverySunday);


        if (values != null && !values.equals("")) {
            ArrayList<String> previousSelectedDays = new ArrayList<>(Arrays.asList(values.split(",")));
            for (String days : previousSelectedDays) {
                Log.d(TAG, "initEveryDays: " + days);
            }
            if (previousSelectedDays.contains(getString(R.string.monday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.monday), ivEveryMonday);
            }
            if (previousSelectedDays.contains(getString(R.string.tuesday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.tuesday), ivEveryTuesday);
            }
            if (previousSelectedDays.contains(getString(R.string.wednesday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.wednesday), ivEveryWednesday);
            }
            if (previousSelectedDays.contains(getString(R.string.thursday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.thursday), ivEveryThursday);
            }
            if (previousSelectedDays.contains(getString(R.string.friday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.friday), ivEveryFriday);
            }
            if (previousSelectedDays.contains(getString(R.string.saturday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.saturday), ivEverySaturday);
            }
            if (previousSelectedDays.contains(getString(R.string.sunday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.sunday), ivEverySunday);
            }
        }

        tvEveryMonday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.monday), ivEveryMonday);
            }
        });

        tvEveryTuesDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.tuesday), ivEveryTuesday);
            }
        });

        tvEveryWednesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.wednesday), ivEveryWednesday);
            }
        });

        tvEveryThursday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.thursday), ivEveryThursday);
            }
        });

        tvEveryFriday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.friday), ivEveryFriday);
            }
        });

        tvEverySaturday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.saturday), ivEverySaturday);
            }
        });

        tvEverySunday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.sunday), ivEverySunday);
            }
        });

    }

    /**
     * init the Repeat week day views
     */
    private void initWeekDayView() {
        TextView tvMonday = (TextView) findViewById(R.id.tvMonday);
        tvMonday.setTypeface(fontMedium);
        TextView tvTuesDay = (TextView) findViewById(R.id.tvTuesDay);
        tvTuesDay.setTypeface(fontMedium);
        TextView tvWednesday = (TextView) findViewById(R.id.tvWednesday);
        tvWednesday.setTypeface(fontMedium);
        TextView tvThursday = (TextView) findViewById(R.id.tvThursday);
        tvThursday.setTypeface(fontMedium);
        TextView tvFriday = (TextView) findViewById(R.id.tvFriday);
        tvFriday.setTypeface(fontMedium);

        final ImageView ivMonday = (ImageView) findViewById(R.id.ivMonday);
        final ImageView ivTuesday = (ImageView) findViewById(R.id.ivTuesday);
        final ImageView ivWednesday = (ImageView) findViewById(R.id.ivWednesday);
        final ImageView ivThursday = (ImageView) findViewById(R.id.ivThursday);
        final ImageView ivFriday = (ImageView) findViewById(R.id.ivFriday);

        if (values != null && !values.equals("")) {
            ArrayList<String> previousSelectedDays = new ArrayList<>(Arrays.asList(values.split(",")));
            if (previousSelectedDays.contains(getString(R.string.monday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.monday), ivMonday);
            }
            if (previousSelectedDays.contains(getString(R.string.tuesday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.tuesday), ivTuesday);
            }
            if (previousSelectedDays.contains(getString(R.string.wednesday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.wednesday), ivWednesday);
            }
            if (previousSelectedDays.contains(getString(R.string.thursday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.thursday), ivThursday);
            }
            if (previousSelectedDays.contains(getString(R.string.friday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.friday), ivFriday);
            }

        }

        tvMonday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.monday), ivMonday);
            }
        });

        tvTuesDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.tuesday), ivTuesday);
            }
        });

        tvWednesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.wednesday), ivWednesday);
            }
        });

        tvThursday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.thursday), ivThursday);
            }
        });

        tvFriday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.friday), ivFriday);
            }
        });
    }

    /**
     * init the Weekends mode views
     */
    private void initWeekEndsView() {
        TextView tvSaturday = (TextView) findViewById(R.id.tvSaturday);
        tvSaturday.setTypeface(fontMedium);
        TextView tvSunday = (TextView) findViewById(R.id.tvSunday);
        tvSunday.setTypeface(fontMedium);

        final ImageView ivSaturday = (ImageView) findViewById(R.id.ivSaturday);
        final ImageView ivSunday = (ImageView) findViewById(R.id.ivSunday);

        if (values != null && !values.equals("")) {
            ArrayList<String> previousSelectedDays = new ArrayList<>(Arrays.asList(values.split(",")));

            if (previousSelectedDays.contains(getString(R.string.saturday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.saturday), ivSaturday);
            }
            if (previousSelectedDays.contains(getString(R.string.sunday))) {
                presenter.addRemoveDays(selectedDays, getString(R.string.sunday), ivSunday);
            }
        }

        tvSaturday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.saturday), ivSaturday);
            }
        });

        tvSunday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addRemoveDays(selectedDays, getString(R.string.sunday), ivSunday);
            }
        });

    }

    /**
     * init the views for duration
     */
    private void initDurationView() {
        TextView tvThisMonth = (TextView) findViewById(R.id.tvThisMonth);
        tvThisMonth.setTypeface(fontMedium);
        TextView tvTwoMonth = (TextView) findViewById(R.id.tvTwoMonth);
        tvTwoMonth.setTypeface(fontMedium);
        TextView tvThreeMonth = (TextView) findViewById(R.id.tvThreeMonth);
        tvThreeMonth.setTypeface(fontMedium);
        TextView tvFourMonth = (TextView) findViewById(R.id.tvFourMonth);
        tvFourMonth.setTypeface(fontMedium);
        TextView tvCustomDate = (TextView) findViewById(R.id.tvCustomDate);
        tvCustomDate.setTypeface(fontMedium);
        TextView tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvStartDate.setTypeface(fontMedium);
        final TextView tvEndDate = (TextView) findViewById(R.id.tvEndDate);
        tvEndDate.setTypeface(fontMedium);
        tvStartDatePicker = (TextView) findViewById(R.id.tvStartDatePicker);
        tvStartDatePicker.setTypeface(fontMedium);
        tvEndDatePicker = (TextView) findViewById(R.id.tvEndDatePicker);
        tvEndDatePicker.setTypeface(fontMedium);

        final ImageView ivThisMonth = (ImageView) findViewById(R.id.ivThisMonth);
        final ImageView ivTwoMonth = (ImageView) findViewById(R.id.ivTwoMonth);
        final ImageView ivThreeMonth = (ImageView) findViewById(R.id.ivThreeMonth);
        final ImageView ivFourMonth = (ImageView) findViewById(R.id.ivFourMonth);

        final CardView cvMonth = (CardView) findViewById(R.id.cvMonth);
        final CardView cvCustomMonth = (CardView) findViewById(R.id.cvCustomMonth);

        presenter.startDay();

        if (values != null && !values.equals("")) {
            if (values.equals(getString(R.string.thisMonth))) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.VISIBLE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 1;
                presenter.startDay();
                presenter.endDay(0);
            } else if (values.equals(getString(R.string.twoMonth))) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.VISIBLE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 2;
                presenter.startDay();
                presenter.endDay(30);
            } else if (values.equals(getString(R.string.threeMonth))) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.VISIBLE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 3;
                presenter.startDay();
                presenter.endDay(61);
            } else if (values.equals(getString(R.string.fourMonth))) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.VISIBLE);
                selectedMonth = 4;
                presenter.startDay();
                presenter.endDay(91);
            } else {
                String[] duration = values.split("to");
                cvMonth.setAlpha(0.4f);
                cvCustomMonth.setAlpha(1);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 5;
                tvStartDatePicker.setText(duration[0]);
                tvEndDatePicker.setText(duration[1]);
                startDate = getIntent().getStringExtra("START_DATE");
                endDate = getIntent().getStringExtra("END_DATE");
                presenter.setMinimumDateForAlreadySelectedStartDate(startDate);
            }
        }

        tvThisMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.VISIBLE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 1;
                presenter.startDay();
                presenter.endDay(0);
            }
        });

        tvTwoMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.VISIBLE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 2;
                presenter.startDay();
                presenter.endDay(30);
            }
        });

        tvThreeMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.VISIBLE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 3;
                presenter.startDay();
                presenter.endDay(61);
            }
        });

        tvFourMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMonth.setAlpha(1);
                cvCustomMonth.setAlpha(0.4f);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.VISIBLE);
                selectedMonth = 4;
                presenter.startDay();
                presenter.endDay(91);
            }
        });

        tvStartDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMonth.setAlpha(0.4f);
                cvCustomMonth.setAlpha(1);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 5;
                endDate = "";
                tvEndDatePicker.setText("");
                presenter.selectDate(1, startDate);
            }
        });

        tvEndDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMonth.setAlpha(0.4f);
                cvCustomMonth.setAlpha(1);
                ivThisMonth.setVisibility(View.GONE);
                ivTwoMonth.setVisibility(View.GONE);
                ivThreeMonth.setVisibility(View.GONE);
                ivFourMonth.setVisibility(View.GONE);
                selectedMonth = 5;
                presenter.selectDate(2, endDate);
            }
        });
    }


    private void initSlotDurationView() {

        if (!BuildConfig.DEBUG) {
            findViewById(R.id.rl_three_min_schdl).setVisibility(View.GONE);
        }
        TextView tvThreeMins = (TextView) findViewById(R.id.tvThreeMins);
        tvThreeMins.setTypeface(fontMedium);
        TextView tvTenMins = (TextView) findViewById(R.id.tvTenMins);
        tvTenMins.setTypeface(fontMedium);
        TextView tvTwentyMins = (TextView) findViewById(R.id.tvTwentyMins);
        tvTwentyMins.setTypeface(fontMedium);
        TextView tvThirtyMins = (TextView) findViewById(R.id.tvThirtyMins);
        tvThirtyMins.setTypeface(fontMedium);
        TextView tvFourtyFiveMins = (TextView) findViewById(R.id.tvFourtyFiveMins);
        tvFourtyFiveMins.setTypeface(fontMedium);
        TextView tvOneHours = (TextView) findViewById(R.id.tvOneHours);
        tvOneHours.setTypeface(fontMedium);
        TextView tvOneHoursFifteenMins = (TextView) findViewById(R.id.tvOneHoursFifteenMins);
        tvOneHoursFifteenMins.setTypeface(fontMedium);
        TextView tvOneHoursThirtyMins = (TextView) findViewById(R.id.tvOneHoursThirtyMins);
        tvOneHoursThirtyMins.setTypeface(fontMedium);

        final ImageView ivThreeMins = (ImageView) findViewById(R.id.ivThreeMins);
        final ImageView ivTenMins = (ImageView) findViewById(R.id.ivTenMins);
        final ImageView ivTwentyMins = (ImageView) findViewById(R.id.ivTwentyMins);
        final ImageView ivThirtyMins = (ImageView) findViewById(R.id.ivThirtyMins);
        final ImageView ivFourtyFiveMins = (ImageView) findViewById(R.id.ivFourtyFiveMins);
        final ImageView ivOneHours = (ImageView) findViewById(R.id.ivOneHours);
        final ImageView ivOneHoursFiftyMins = (ImageView) findViewById(R.id.ivOneHoursFiftyMins);
        final ImageView ivOneHoursThirtyMins = (ImageView) findViewById(R.id.ivOneHoursThirtyMins);

        ivTempDuration = new ImageView(this);
        selectedDuration = getIntent().getStringExtra("SELECTED_DURATION");

        if (values != null && !values.equals("")) {
            if (values.equals("3")) {
                ivThreeMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivThreeMins;
            } else if (values.equals("10")) {
                ivTenMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivTenMins;
            } else if (values.equals("20")) {
                ivTwentyMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivTwentyMins;
            } else if (values.equals("30")) {
                ivThirtyMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivThirtyMins;
            } else if (values.equals("45")) {
                ivFourtyFiveMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivFourtyFiveMins;
            } else if (values.equals("60")) {
                ivOneHours.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivOneHours;
            } else if (values.equals("75")) {
                ivOneHoursFiftyMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivOneHoursFiftyMins;
            } else if (values.equals("90")) {
                ivOneHoursThirtyMins.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
                ivTempDuration = ivOneHoursThirtyMins;
            }
        }

        tvThreeMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "3";
                selectedDuration = getString(R.string.threeMins);
                durationSelected(ivThreeMins);
            }
        });
        tvTenMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "10";
                selectedDuration = getString(R.string.tenMins);
                durationSelected(ivTenMins);
            }
        });

        tvTwentyMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "20";
                selectedDuration = getString(R.string.twentyMins);
                durationSelected(ivTwentyMins);
            }
        });

        tvThirtyMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "30";
                selectedDuration = getString(R.string.thirtyMins);
                durationSelected(ivThirtyMins);
            }
        });

        tvFourtyFiveMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "45";
                selectedDuration = getString(R.string.fortyFiveMins);
                durationSelected(ivFourtyFiveMins);
            }
        });

        tvOneHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "60";
                selectedDuration = getString(R.string.oneHrs);
                durationSelected(ivOneHours);
            }
        });

        tvOneHoursFifteenMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "75";
                selectedDuration = getString(R.string.oneHoursFifteenMins);
                durationSelected(ivOneHoursFiftyMins);
            }
        });

        tvOneHoursThirtyMins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                values = "90";
                selectedDuration = getString(R.string.oneHrsThirtyMins);
                durationSelected(ivOneHoursThirtyMins);
            }
        });

    }

    private void durationSelected(ImageView selectedImageView) {
        selectedImageView.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_color_primary_add_schedule_tick));
        ivTempDuration.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.vector_add_schedule_tick_color_grey));
        this.ivTempDuration = selectedImageView;
        setValues(6);
    }


    /**
     * senting values to scheduleass activity
     * based on the type
     *
     * @param type type determine which one we want sent
     */
    private void setValues(int type) {
        Intent intent = new Intent(this, ScheduleAddActivity.class);
        switch (type) {
            case 1:
                if (selectedMode != 0) {
                    intent.putExtra("SELECTED_VALUE", selectedMode);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    DialogHelper.showWaringMessage(this, getString(R.string.plsSelectMode));
                }
                break;

            case 2:
            case 3:
            case 4:
                if (selectedDays.size() > 0) {
                    intent.putExtra("SELECTED_VALUE", presenter.sortDays(selectedDays));
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    DialogHelper.showWaringMessage(this, getString(R.string.plsSelectDays));
                }
                break;

            case 5:
                if (selectedMonth != 0) {
                    intent.putExtra("START_DATE", startDate);
                    intent.putExtra("END_DATE", endDate);
                    intent.putExtra("DISPLAY_START_DATE", tvStartDatePicker.getText().toString());
                    intent.putExtra("DISPLAY_END_DATE", tvEndDatePicker.getText().toString());

                    switch (selectedMonth) {
                        case 1:
                            intent.putExtra("SELECTED_VALUE", 1);
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 2:
                            intent.putExtra("SELECTED_VALUE", 2);
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 3:
                            intent.putExtra("SELECTED_VALUE", 3);
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 4:
                            intent.putExtra("SELECTED_VALUE", 4);
                            setResult(RESULT_OK, intent);
                            finish();
                            break;
                        case 5:
                            if (!endDate.equals("")) {
                                intent.putExtra("SELECTED_VALUE", 5);
                                setResult(RESULT_OK, intent);
                                finish();
                            } else {
                                DialogHelper.showWaringMessage(this, getString(R.string.plsSelectEndDate));
                            }
                            break;
                    }
                } else {
                    DialogHelper.showWaringMessage(this, getString(R.string.plsSelectMonth));
                }
                break;

            case 6:
                if (!values.equals("")) {
                    intent.putExtra("SELECTED_VALUE", values);
                    intent.putExtra("SELECTED_DURATION", selectedDuration);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    DialogHelper.showWaringMessage(this, getString(R.string.plsSelectDuration));
                }
                break;
        }
        overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
    }

    @Override
    public void startDay(String startDay) {
        this.startDate = startDay;
        //tvStartDatePicker.setText(startDay);
    }

    @Override
    public void endDay(String endDay) {
        this.endDate = endDay;
    }

    @Override
    public void startDayCalendar(String startDay, String displayStartDay) {
        this.startDate = startDay;
        tvStartDatePicker.setText(displayStartDay);
    }

    @Override
    public void endDayCalendar(String endDay, String displayEndDay) {
        this.endDate = endDay;
        tvEndDatePicker.setText(displayEndDay);
    }

    @Override
    public void addDays(String days) {
        selectedDays.add(days);
    }

    @Override
    public void removeDays(String days) {
        selectedDays.remove(days);
    }

}
