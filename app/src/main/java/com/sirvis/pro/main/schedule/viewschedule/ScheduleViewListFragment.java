package com.sirvis.pro.main.schedule.viewschedule;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ScheduleViewListAdapter;
import com.sirvis.pro.pojo.shedule.ScheduleData;
import com.sirvis.pro.pojo.shedule.ScheduleViewData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleViewListFragment extends Fragment implements ScheduleViewListAdapter.DeleteSchdule, ScheduleViewPresenter.ScheduleViewPresenterImple {

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private ScheduleViewPresenter presenter;

    private ScheduleViewListAdapter scheduleViewListAdapter;
    private ArrayList<ScheduleViewData> scheduleViewDatas;
    private int deletePosition = 0;

    public static ScheduleViewListFragment getInstance() {
        return new ScheduleViewListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_schedule_view_list, container, false);
        initView(rootView);
        return rootView;
    }


    private void initView(View rootView)
    {
        presenter = new ScheduleViewPresenter(this);
        sessionManager = SessionManager.getSessionManager(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.deletingSchedule));
        progressDialog.setCancelable(false);

        scheduleViewDatas = new ArrayList<>();
        scheduleViewListAdapter = new ScheduleViewListAdapter(getActivity(), scheduleViewDatas,this);
        RecyclerView rvScheduleView = rootView.findViewById(R.id.rvScheduleView);
        rvScheduleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvScheduleView.setAdapter(scheduleViewListAdapter);
    }

    @Override
    public void onDelete(final int position, final String slotId) {
        deletePosition = position;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(getString(R.string.slotDeleteWarning));
        builder.setPositiveButton(getString(R.string.oK), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                progressDialog.setMessage(getString(R.string.deletingSchedule));
                presenter.deleteSchedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),slotId);
            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(getActivity(),getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(ScheduleData data) {

    }

    @Override
    public void onSuccessDeleteSchedule(String msg) {
        VariableConstant.IS_SHEDULE_EDITED = true;
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        scheduleViewDatas.remove(deletePosition);
        scheduleViewListAdapter.notifyDataSetChanged();
    }

    public void setValues(ArrayList<ScheduleViewData> scheduleViewDatas)
    {
        try
        {
            this.scheduleViewDatas.clear();
            this.scheduleViewDatas.addAll(scheduleViewDatas);
            scheduleViewListAdapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
