package com.sirvis.pro.main.profile.myratecard.singleservice;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.sirvis.pro.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link DescriptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DescriptionFragment extends Fragment implements View.OnFocusChangeListener {

    private static final String ARG_PARAM1 = "param1";
    private String desc = "";

    public EditText etDesc;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment DescriptionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DescriptionFragment newInstance(String desc) {
        DescriptionFragment fragment = new DescriptionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, desc);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            desc = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_description, container, false);
        etDesc = view.findViewById(R.id.etDesc);
        if(!desc.isEmpty())
        {
            etDesc.setText(desc);
        }
        etDesc.setOnFocusChangeListener(this);
        return view;
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b)
        {
            etDesc.setSelection(etDesc.getText().length());
        }
    }
}
