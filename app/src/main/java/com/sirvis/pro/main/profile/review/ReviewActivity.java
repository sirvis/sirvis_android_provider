package com.sirvis.pro.main.profile.review;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ReviewsListAdapter;
import com.sirvis.pro.pojo.profile.review.ReviewData;
import com.sirvis.pro.pojo.profile.review.Reviews;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ReviewActivity</h1>
 * ReviewActivity for showing the reviews of the user
 */

public class ReviewActivity extends AppCompatActivity implements ReviewPresenter.ReviewPresenterImple {

    private ReviewPresenter reviewPresenter;
    private SessionManager sessionManager;
    private ReviewsListAdapter reviewsListAdapter;
    private ArrayList<Reviews> reviewses;
    private TextView tvTotalNoOfReview,tvAverageRating;
    int i = 0;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean toLoad = true;
    private ProgressBar pBReview;
    private RatingBar ratingStar;
    private LinearLayout llReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        init();
    }

    /**
     * init the views
     */
    private void init() {
        reviewPresenter = new ReviewPresenter(this);
        sessionManager  = SessionManager.getSessionManager(this);
        reviewses = new ArrayList<>();

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.reviewsHeader));
        tvTitle.setTypeface(fontBold);

        tvTotalNoOfReview = findViewById(R.id.tvTotalNoOfReview);
        tvAverageRating = findViewById(R.id.tvAverageRating);
        tvTotalNoOfReview.setTypeface(fontRegular);
        tvAverageRating.setTypeface(fontBold);

        ratingStar = findViewById(R.id.ratingStar);
        pBReview = findViewById(R.id.pBReview);
        llReview = findViewById(R.id.llReview);
        llReview.setVisibility(View.GONE);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        RecyclerView rvReviews = findViewById(R.id.rvReviews);
        rvReviews.setLayoutManager(linearLayoutManager);
        reviewsListAdapter = new ReviewsListAdapter(this,reviewses);
        rvReviews.setAdapter(reviewsListAdapter);

        rvReviews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (toLoad)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            toLoad = false;
                            i++;
                            reviewPresenter.getReview(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),i);
                        }
                    }
                }
            }
        });
        reviewPresenter.getReview(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),i);

        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.ViewReview.value);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onSuccess(ReviewData reviewData) {
        llReview.setVisibility(View.VISIBLE);
        reviewses.addAll(reviewData.getReviews());
        reviewsListAdapter.notifyDataSetChanged();
        if(i == 0)
        {
            i++;
            reviewPresenter.getReview(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),i);
            if(!reviewData.getAverageRating().equals(""))
            {
                tvAverageRating.setText(Utility.roundString(reviewData.getAverageRating(),1));
            }
            ratingStar.setRating(Float.parseFloat(reviewData.getAverageRating()));
            tvTotalNoOfReview.setText(getString(R.string.youAverageRatings)+"  |  "+reviewData.getReviewCount()+" "+getString(R.string.reviewsHeader));
        }
    }

    @Override
    public void startProgressBar() {
        pBReview.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopProgressBar() {
        toLoad = true;
        pBReview.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }


}
