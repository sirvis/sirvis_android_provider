package com.sirvis.pro.main.profile.bank;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.sirvis.pro.R;
import com.sirvis.pro.adapters.BankListAdapter;
import com.sirvis.pro.pojo.profile.bank.BankList;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import org.json.JSONObject;

/**
 * Created by murashid on 29-Aug-17.
 * <h1>BankBottomSheetFragment</h1>
 * BankBottomSheetFragment Bottom sheet fragment for showing the bank details
 */

public class BankBottomSheetFragment extends BottomSheetDialogFragment implements BankBottomSheetPresenter.BankBottomSheetPresenterImplem {

    private static final String PARAM1 = "param1";
    private static final String PARAM2 = "param2";
    BankList bankData;
    BankListAdapter.RefreshBankDetails refreshBankDetails;
    private Typeface fontRegular, fontLight;
    private ProgressDialog pDialog;
    private SessionManager sessionManager;
    private BankBottomSheetPresenter bankBottomSheetPresenter;

    private static final String TAG = "BankBottomSheetFragment";
    LayoutInflater inflater;

    public BankBottomSheetFragment()
    {

    }

    /**
     * Method for creting object of the BankBottomSheetFragment and intializing the RefreshBankDetails and bank data
     * @param bankData pojo data for bank details
     * @param refreshBankDetails interface for callback of refresh bank
     * @return object of the BankBottomSheetFragment
     */
    public static BankBottomSheetFragment newInstance(BankList bankData, BankListAdapter.RefreshBankDetails refreshBankDetails) {
        BankBottomSheetFragment fragment = new BankBottomSheetFragment();
        Bundle args = new Bundle();
        args.putSerializable(PARAM1, bankData);
        args.putSerializable(PARAM2, refreshBankDetails);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            bankData = (BankList) getArguments().getSerializable(PARAM1);
            refreshBankDetails = (BankListAdapter.RefreshBankDetails) getArguments().getSerializable(PARAM2);
        }

        fontRegular = Utility.getFontRegular(getActivity());
        fontLight = Utility.getFontRegular(getActivity());
        sessionManager = SessionManager.getSessionManager(getActivity());
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);

        bankBottomSheetPresenter = new BankBottomSheetPresenter(this);
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(final Dialog dialog, int style) {

        inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.bottom_sheet_bank_details,null);
        dialog.setContentView(view);

        TextView tvAccoutDetails= view.findViewById(R.id.tvAccoutDetails);
        TextView tvAccountHolderLabel= view.findViewById(R.id.tvAccountHolderLabel);
        TextView tvAccountHolder= view.findViewById(R.id.tvAccountHolder);
        TextView tvAccountNoLabel= view.findViewById(R.id.tvAccountNoLabel);
        TextView tvAccountNo= view.findViewById(R.id.tvAccountNo);
        TextView tvRoutingNoLabel= view.findViewById(R.id.tvRoutingNoLabel);
        TextView tvRoutinNo= view.findViewById(R.id.tvRoutinNo);
        TextView tvBankNameLabel= view.findViewById(R.id.tvBankNameLabel);
        TextView tvBankName= view.findViewById(R.id.tvBankName);
        TextView tvCurrencyLabel= view.findViewById(R.id.tvCurrencyLabel);
        TextView tvCurrency= view.findViewById(R.id.tvCurrency);
        TextView tvCountryLabel= view.findViewById(R.id.tvCountryLabel);
        TextView tvCountry= view.findViewById(R.id.tvCountry);
        TextView tvMakeDefault= view.findViewById(R.id.tvMakeDefault);
        TextView tvDeleteAccount= view.findViewById(R.id.tvDeleteAccount);
        ImageView ivCancel= view.findViewById(R.id.ivCancel);


        tvAccountHolderLabel.setTypeface(fontLight);
        tvAccountNoLabel.setTypeface(fontLight);
        tvRoutingNoLabel.setTypeface(fontLight);
        tvBankNameLabel.setTypeface(fontLight);
        tvCurrencyLabel.setTypeface(fontLight);
        tvCountryLabel.setTypeface(fontLight);

        tvMakeDefault.setTypeface(fontRegular);
        tvDeleteAccount.setTypeface(fontRegular);
        tvAccountHolder.setTypeface(fontRegular);
        tvAccountNo.setTypeface(fontRegular);
        tvRoutinNo.setTypeface(fontRegular);
        tvBankName.setTypeface(fontRegular);
        tvCurrency.setTypeface(fontRegular);
        tvCountry.setTypeface(fontRegular);
        tvAccoutDetails.setTypeface(fontRegular);

        if(bankData.getDefault_for_currency().equals("true"))
        {
            tvDeleteAccount.setVisibility(View.GONE);
            tvMakeDefault.setVisibility(View.GONE);
        }

        if(bankData!=null)
        {
            tvAccountHolder.setText(bankData.getAccount_holder_name());
            tvAccountNo.setText("xxxxxxxx"+bankData.getLast4());
            tvRoutinNo.setText(bankData.getRouting_number());
            tvBankName.setText(bankData.getBank_name());
            tvCurrency.setText(bankData.getCurrency());
            tvCountry.setText(bankData.getCountry());

        }
        else
        {
            return;
        }

        tvMakeDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("accountId",bankData.getId());

                    pDialog.setMessage(getString(R.string.updating));
                    bankBottomSheetPresenter.makeDefault(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        tvDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("accountId",bankData.getId());
                    pDialog.setMessage(getString(R.string.deleting));
                    bankBottomSheetPresenter.deleteAccount(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

    }

    @Override
    public void startProgressBar() {
        pDialog.show();
    }

    @Override
    public void stopProgressBar() {
        pDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        dismiss();
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        dismiss();
        Toast.makeText(getActivity(),getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        dismiss();
        refreshBankDetails.onRefresh();
    }
}