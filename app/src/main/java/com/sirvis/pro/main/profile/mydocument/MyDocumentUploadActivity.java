package com.sirvis.pro.main.profile.mydocument;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.document.ProfileDocumentCategory;
import com.sirvis.pro.pojo.signup.CategoryDocument;
import com.sirvis.pro.pojo.signup.CategoryDocumentField;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DatePickerCommon;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class MyDocumentUploadActivity extends AppCompatActivity implements DatePickerCommon.DateSelected, EasyPermissions.PermissionCallbacks, MyDocumentPresenter.View {

    private static final String TAG = "CategoryDocument";
    private ArrayList<CategoryDocument> document;
    private LinkedHashMap<EditText,String> etDocuments,etDates;
    private LinkedHashMap<ImageView,String> ivDocuments;
    private EditText tempDates;
    private ImageView tempPicture;
    private DatePickerCommon datePickerFragment;

    private UploadFileAmazonS3 amazonS3;

    private File mFileTemp;
    private Typeface fontRegular;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private int position  = 0;

    private MyDocumentPresenter presenter;
    private ProfileDocumentCategory profileDocumentCategory;

    private boolean isEditable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_document_upload);

        init();
    }

    /**
     * initialize the values
     */
    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new MyDocumentPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.setCancelable(false);
        etDocuments = new LinkedHashMap<>();
        etDates = new LinkedHashMap<>();
        ivDocuments = new LinkedHashMap<>();

        datePickerFragment = new DatePickerCommon();
        datePickerFragment.setCallBack(this);
        amazonS3 = UploadFileAmazonS3.getInstance(this);

        Intent intent = getIntent();
        profileDocumentCategory = (ProfileDocumentCategory) intent.getSerializableExtra("category");
        document = profileDocumentCategory.getDocuments();
        position = intent.getIntExtra("position",0);


        String filename = VariableConstant.DOCUMENT_FILE_NAME+ System.currentTimeMillis() + ".png";
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.DOCUMENT_PIC_DIR, true),filename);

        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);
        tvTitle.setText(profileDocumentCategory.getCategoryName());

        TextView tvSave = findViewById(R.id.tvDone);
        tvSave.setText(getString(R.string.save));
        tvSave.setTypeface(fontBold);

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDocumentArry();
            }
        });

        LinearLayout llSericeDocument = findViewById(R.id.llSericeDocument);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for(CategoryDocument categoryDocument : document)
        {
            TextView textView = (TextView) inflater.inflate(R.layout.signup_document_single_item_text_view_header,null);
            textView.setText(categoryDocument.getDocName());
            textView.setTypeface(fontMedium);

            llSericeDocument.addView(textView);

            if(categoryDocument.getField() != null)
            {
                for(CategoryDocumentField categoryDocumentField : categoryDocument.getField())
                {
                    View viewServiceField ;
                    String hint = categoryDocumentField.getfName();
                    switch (categoryDocumentField.getfType()) {

                        case "1":
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_edit_text, null);
                            EditText etDocument = viewServiceField.findViewById(R.id.etDocument);
                            TextInputLayout tilDocument = viewServiceField.findViewById(R.id.tilDocument);
                            etDocument.setTypeface(fontMedium);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tilDocument.setHint(hint);

                            if(categoryDocumentField.getData() !=null && !categoryDocumentField.getData().equals(""))
                            {
                                etDocument.setText(categoryDocumentField.getData());
                            }

                            etDocuments.put(etDocument,categoryDocumentField.getData());
                            break;

                        case "2":
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_date, null);
                            final EditText etDate = viewServiceField.findViewById(R.id.etDate);
                            TextInputLayout tilDob = viewServiceField.findViewById(R.id.tilDob);
                            etDate.setTypeface(fontMedium);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tilDob.setHint(hint);

                            etDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
                                        if (!datePickerFragment.isResumed()) {
                                            tempDates = etDate;
                                            datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                        }
                                    }
                                }
                            });
                            etDate.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!datePickerFragment.isResumed()) {
                                        tempDates = etDate;
                                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                    }
                                }
                            });

                            if(categoryDocumentField.getData() !=null && !categoryDocumentField.getData().equals(""))
                            {
                                etDate.setText(datePickerFragment.returmSendingToDisplayDate(categoryDocumentField.getData()));
//                                etDate.setTag(categoryDocumentField.getData());
                            }

                            etDates.put(etDate,categoryDocumentField.getData());

                            break;


                        case "4":
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_date, null);
                            final EditText etDatePast = viewServiceField.findViewById(R.id.etDate);
                            TextInputLayout tilDobPast = viewServiceField.findViewById(R.id.tilDob);
                            etDatePast.setTypeface(fontMedium);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tilDobPast.setHint(hint);

                            etDatePast.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View v, boolean hasFocus) {
                                    if (hasFocus) {
                                        if (!datePickerFragment.isResumed()) {
                                            datePickerFragment.setDatePickerType(4);
                                            tempDates = etDatePast;
                                            datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                        }
                                    }
                                }
                            });
                            etDatePast.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!datePickerFragment.isResumed()) {
                                        datePickerFragment.setDatePickerType(4);
                                        tempDates = etDatePast;
                                        datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                                    }
                                }
                            });
                            if(categoryDocumentField.getData() !=null && !categoryDocumentField.getData().equals(""))
                            {
                                etDatePast.setText(datePickerFragment.returmSendingToDisplayDate(categoryDocumentField.getData()));
//                                etDatePast.setTag(categoryDocumentField.getData());
                            }
                            etDates.put(etDatePast,categoryDocumentField.getData());
                            break;


                        default:
                            viewServiceField = inflater.inflate(R.layout.signup_document_single_item_image, null);

                            final ImageView ivDocument = viewServiceField.findViewById(R.id.ivDocument);
                            TextView tvImageLabel = viewServiceField.findViewById(R.id.tvImageLabel);
                            TextView upload_button = viewServiceField.findViewById(R.id.upload_button);
                            upload_button.setVisibility(View.GONE);

                            if (categoryDocumentField.getIsManadatory().equals("1")) {
                                hint = hint + getString(R.string.astrix);
                            }
                            tvImageLabel.setHint(hint);

                            ivDocument.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    tempPicture = ivDocument;

                                    if (EasyPermissions.hasPermissions(MyDocumentUploadActivity.this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
                                        selectImage();
                                    } else {
                                        EasyPermissions.requestPermissions(MyDocumentUploadActivity.this, getString(R.string.read_storage_and_camera_state_permission_message),
                                                1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
                                    }
                                }
                            });

                            if(categoryDocumentField.getData() !=null && !categoryDocumentField.getData().equals(""))
                            {
                                Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                                        .error(R.drawable.upload_take_photo_icon)
                                        .placeholder(R.drawable.upload_take_photo_icon))
                                        .load(categoryDocumentField.getData())
                                        .into(ivDocument);

//                                ivDocument.setTag(categoryDocumentField.getData());
                            }

                            ivDocuments.put(ivDocument,categoryDocumentField.getData());
                            break;
                    }
                    llSericeDocument.addView(viewServiceField);
                }
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (EditText editText : etDates.keySet())
                {
                    editText.setFocusable(true);
                    editText.setFocusableInTouchMode(true);
                }
            }
        },300);


        if(sessionManager.getIsProfileAcivated())
        {
            tvSave.setVisibility(View.GONE);
            for (EditText editText : etDates.keySet())
            {
                editText.setEnabled(false);
            }
            for(EditText editText : etDocuments.keySet())
            {
                editText.setEnabled(false);
            }
            for(ImageView imageView : ivDocuments.keySet())
            {
                imageView.setEnabled(false);
            }
        }
    }

    /**
     * method for creating values from the dynamic fields and check the validation for mantadatory field
     */
    private void createDocumentArry()
    {
        ArrayList<CategoryDocumentField> documentUploads = new ArrayList<>();

        int etDocumetIndex = 0;
        int etDateIndex = 0;
        int ivDocumetIndex = 0;

        for(CategoryDocument categoryDocument  : document)
        {
            if(categoryDocument.getField() != null)
            {
                for(CategoryDocumentField categoryDocumentField : categoryDocument.getField())
                {
                    CategoryDocumentField  documentUpload = new CategoryDocumentField();
                    documentUpload.setFid(categoryDocumentField.getFid());
                    documentUpload.setfType(categoryDocumentField.getfType());
                    documentUpload.setfName(categoryDocumentField.getfName());
                    documentUpload.setIsManadatory(categoryDocumentField.getIsManadatory());

                    switch (categoryDocumentField.getfType()) {
                        case "1":
                            EditText etDocument = (EditText) etDocuments.keySet().toArray()[etDocumetIndex];
                            String etDocumenturl = etDocuments.get(etDocument);
//                            EditText etDocument = etDocuments.get(etDocumetIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && etDocumenturl.equals(""))
                            {
                                Toast.makeText(this,categoryDocumentField.getfName()+ " "+getString(R.string.isMandatory),Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else if(!etDocumenturl.equals(""))
                            {
                                documentUpload.setData(etDocumenturl);
                            }
                            else
                            {
                                documentUpload = null;
                            }

                            etDocumetIndex++;
                            break;

                        case "2":
                        case "4":
                            EditText etDate = (EditText) etDates.keySet().toArray()[etDateIndex];
                            String etDateurl = etDates.get(etDate);
//                            EditText etDate = etDates.get(etDateIndex);
                            if (categoryDocumentField.getIsManadatory().equals("1") && etDateurl.equals(""))
                            {
                                Toast.makeText(this,categoryDocumentField.getfName()+ " "+getString(R.string.isMandatory),Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else if(!etDateurl.equals(""))
                            {
                                documentUpload.setData(etDateurl);
                            }
                            else
                            {
                                documentUpload = null;
                            }

                            etDateIndex++;
                            break;

                        default:
                            ImageView ivDocument = (ImageView) ivDocuments.keySet().toArray()[ivDocumetIndex];
//                            ImageView ivDocument = ivDocuments.get(ivDocumetIndex);
                            String ivDocumenturl = ivDocuments.get(ivDocument);
                            if (categoryDocumentField.getIsManadatory().equals("1")
                                    && ivDocumenturl == null)
                            {
                                Toast.makeText(this,categoryDocumentField.getfName()+ " "+getString(R.string.isMandatory),Toast.LENGTH_SHORT).show();
                                return;
                            }
                            else if(ivDocumenturl != null)
                            {
                                documentUpload.setData(ivDocumenturl);
                            }
                            else
                            {
                                documentUpload = null;
                            }

                            ivDocumetIndex++;
                            break;
                    }

                    if(documentUpload != null)
                    {
                        documentUploads.add(documentUpload);
                    }
                }
            }
        }

        /**
         * creating json Array from pojo class
         */
        try {

            Gson gson = new Gson();
            String listString = gson.toJson(documentUploads, new TypeToken<ArrayList<CategoryDocument>>() {}.getType());


            progressDialog.show();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("categoryId", profileDocumentCategory.getCategoryId());
            jsonObject.put("document",new JSONArray(listString));

            Log.d(TAG, "createDocumentArry: "+jsonObject);

            presenter.updateCategory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    /**
     * method for creating the alert dialog for showing option for selecting image
     */
    private void selectImage() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.profile_pic_options, null);
        alertDialogBuilder.setView(view);

        final AlertDialog mDialog = alertDialogBuilder.create();
        mDialog.setCancelable(false);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button btnCamera = view.findViewById(R.id.camera);
        Button btnCancel = view.findViewById(R.id.cancel);
        Button btnGallery = view.findViewById(R.id.gallery);
        Button btnRemove = view.findViewById(R.id.removephoto);
        TextView tvHeader = view.findViewById(R.id.tvHeader);

        btnCamera.setTypeface(fontRegular);
        btnCancel.setTypeface(fontRegular);
        btnGallery.setTypeface(fontRegular);
        btnRemove.setTypeface(fontRegular);
        tvHeader.setTypeface(fontRegular);

        btnRemove.setVisibility(View.GONE);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {
                case VariableConstant.REQUEST_CODE_GALLERY:
                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        amazonUpload();
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    amazonUpload();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for uploading image to amazon
     */
    private void amazonUpload()
    {
        tempPicture.setImageBitmap(BitmapFactory.decodeFile(String.valueOf(mFileTemp)));
        progressDialog.setMessage(getString(R.string.uploading));
        progressDialog.show();
        new UploadFileToServer().execute(mFileTemp.getPath());
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        tempDates.setText(displayFormat);
        etDates.put(tempDates,sendingFormat);
//        tempDates.setTag(sendingFormat);
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(ProfileData profileData) {

    }

    @Override
    public void onSuccess(String msg) {
        VariableConstant.IS_DOCUMENT_UPDATED = true;
        closeActivity();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    /**
     * Uploading the file to server
     * */
    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String[] doInBackground(String... params)
        {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result,responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("uploadTo","1")
                        .addFormDataPart("folder","Documents")
                        .addFormDataPart("file", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.SIGNUPIMAGE)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: "+responseCode);
                Log.d(TAG, "doInBackground: "+result);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            stopProgressBar();
            progressDialog.dismiss();
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    JSONObject dataObject = jsonObject.getJSONObject("data");
                    String imageUrl = dataObject.getString("imageUrl");
                    tempPicture.setTag(imageUrl);
                    Log.d(TAG, "onPostExecute: "+imageUrl+"   "+result.length);
                }
                else
                    Toast.makeText(MyDocumentUploadActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(MyDocumentUploadActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }

            super.onPostExecute(result);
        }

    }

}
