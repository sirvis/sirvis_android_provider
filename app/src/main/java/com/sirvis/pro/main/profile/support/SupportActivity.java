package com.sirvis.pro.main.profile.support;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.SupportListAdapter;
import com.sirvis.pro.pojo.profile.support.SupportData;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportActivity</h1>
 * SupportActivity for showing list of supports
 */

public class SupportActivity extends AppCompatActivity implements SupportPresenter.SupportPresenterImplement {
    private ProgressDialog pDialog;
    private String TAG = "SupportActivity";
    private ArrayList<SupportData> supportDatas;
    private SupportListAdapter supportListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support_fragment);

        initLayout();
        SupportPresenter supportPresenter = new SupportPresenter(this);
        supportPresenter.getSupport();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    /**
     * init the views
     */
    public void initLayout() {

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        /*
         * Bug id : Trello 60There is no option to cance
         * Bug Description : in my profile support should be changed to FAQ
         * Fixed Description : Changed the Text
         * Dev : Murashid
         * Date : 29-11-2018
         * */

        tvTitle.setText(getString(R.string.faq));
        tvTitle.setTypeface(fontBold);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.gettingSupport));
        pDialog.setCancelable(false);

        supportDatas = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.rvSupport);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        supportListAdapter = new SupportListAdapter(this, supportDatas, true);
        recyclerView.setAdapter(supportListAdapter);
    }

    @Override
    public void startProgressBar() {
        pDialog.show();
    }

    @Override
    public void stopProgressBar() {
        pDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSupportDetails(ArrayList<SupportData> supportDatas) {
        this.supportDatas.addAll(supportDatas);
        supportListAdapter.notifyDataSetChanged();
    }
}
