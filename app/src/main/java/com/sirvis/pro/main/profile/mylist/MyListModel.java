package com.sirvis.pro.main.profile.mylist;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.ProfilePojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListModel</h1>
 * MyListModel model for MyListAcitivty
 * @see MyListActivity
 */

public class MyListModel{
    private static final String TAG = "MyListModel";
    private MyListModelImple modelImplement;

    MyListModel(MyListModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for getting profile details
     * @param sessiontoken session Token
     */
    void getProfile(String sessiontoken)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        ProfilePojo profilePojo = gson.fromJson(result,ProfilePojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(profilePojo.getData());
                        } else {
                            modelImplement.onFailure(profilePojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * method for update the profile status
     * @param sessiontoken session token
     * @param param key of the value
     * @param value new value
     */
    void updateProfileStatus(String sessiontoken, final String param, String value)
    {
        JSONObject jsonObject = new JSONObject();
        try
        {
            jsonObject.put(param,value);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessUpdateStatus(jsonObject.getString("message"));
                        } else {
                            if(param.equals("profileStatus"))
                            {
                                modelImplement.onFailureUpdateStatus();
                            }
                            else
                            {
                                modelImplement.onFailureLocation();
                            }
                        }
                    }
                    else
                        {
                        if(param.equals("profileStatus"))
                        {
                            modelImplement.onFailureUpdateStatus();
                        }
                        else
                        {
                            modelImplement.onFailureLocation();
                        }

                    }
                }
                catch (Exception e)
                {
                    if(param.equals("profileStatus"))
                    {
                        modelImplement.onFailureUpdateStatus();
                    }
                    else
                    {
                        modelImplement.onFailureLocation();
                    }

                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                if(param.equals("profileStatus"))
                {
                    modelImplement.onFailureUpdateStatus();
                }
                else
                {
                    modelImplement.onFailureLocation();
                }

            }
        });
    }


    /**
     * method for calling api for upadater the mylist field based on values and param
     * @param sessiontoken session Token
     * @param jsonObject jsonObject
     */
    void updateWorkImage(String sessiontoken, JSONObject jsonObject)
    {

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessUpdateStatus(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }




    /**
     * MyListModelImple interface for presenter implementation
     */
    interface MyListModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onFailureUpdateStatus();
        void onFailureLocation();
        void onSuccessUpdateStatus(String msg);
        void onSuccess(ProfileData profileData);
    }
}
