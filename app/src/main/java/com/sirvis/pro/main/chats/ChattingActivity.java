package com.sirvis.pro.main.chats;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ChattingAdapter;
import com.sirvis.pro.bookingflow.OnTheWayActivity;
import com.sirvis.pro.fcm.MyFirebaseMessagingService;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.chat.ChatApiResponse;
import com.sirvis.pro.pojo.chat.ChatBookingDetailsPojo;
import com.sirvis.pro.pojo.chat.ChatData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.DataBaseChat;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.NotificationHelper;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class ChattingActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, ChatOnMessageCallback, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "Chatting";
    private Typeface fontRegular, fontSemiBold;

    private EditText etMsg;
    private SwipeRefreshLayout srlChat;
    private RecyclerView rcvChatMsg;

    private String msgTypeText = "1";
    private String msgTypeImage = "2";
    private long timeMillisImage;
    private int imagePosition = 0;

    private SessionManager sessionManager;
    private TextView tvCategory, tvPrice;
    private RelativeLayout rlJobDetails;
    private Booking booking;
    private String bid = "";
    private String customerId;

    private ArrayList<ChatData> chatDataArry;
    private ChattingAdapter chattingAdapter;
    private DataBaseChat db;

    private File mFileTemp;
    private LayoutInflater inflater;
    private NotificationManager notificationManager;

    private Gson gson;
    private int pageIndex = 0;
    private boolean isMoreDataAvailable = true;
    private String call_id;

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);

        init();

    }

    private void init() {
        gson = new Gson();
        chatDataArry = new ArrayList<>();
        sessionManager = SessionManager.getSessionManager(this);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        fontRegular = Utility.getFontRegular(this);
        fontSemiBold = Utility.getFontBold(this);
        inflater = LayoutInflater.from(this);

        bid = sessionManager.getChatBookingID();
        customerId = sessionManager.getChatCustomerID();

        String filename = VariableConstant.DOCUMENT_FILE_NAME + System.currentTimeMillis() + ".png";
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.CROP_PIC_DIR, true), filename);


        //db = new DataBaseChat(this);
        //chatDataArry = db.fetchData(bid);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ImageView ivAddFiles = findViewById(R.id.ivAddFiles);
        TextView tvReceiverName = findViewById(R.id.tvReceiverName);
        TextView tvSend = findViewById(R.id.tvSend);
        TextView tvEventId = findViewById(R.id.tvEventId);
        ImageView ivCustomer = findViewById(R.id.ivCustomer);
        tvCategory = findViewById(R.id.tvCategory);
        tvPrice = findViewById(R.id.tvPrice);
        TextView tvJobDetails = findViewById(R.id.tvJobDetails);
        rlJobDetails = findViewById(R.id.rlJobDetails);
        etMsg = findViewById(R.id.etMsg);


        srlChat = findViewById(R.id.srlChat);
        rcvChatMsg = findViewById(R.id.rcvChatMsg);
        chattingAdapter = new ChattingAdapter(this, chatDataArry, sessionManager.getProviderId());
        rcvChatMsg.setLayoutManager(new LinearLayoutManager(this));
        rcvChatMsg.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.layoutanimation_from_bottom));
        rcvChatMsg.setAdapter(chattingAdapter);

        srlChat.setOnRefreshListener(this);

        tvReceiverName.setTypeface(fontRegular);
        tvEventId.setTypeface(fontRegular);
        tvPrice.setTypeface(fontRegular);
        tvJobDetails.setTypeface(fontRegular);
        tvPrice.setTypeface(fontRegular);


        tvReceiverName.setText(sessionManager.getChatCustomerName());
        tvEventId.setText(getResources().getString(R.string.jobId) + " " + bid);

        if (!sessionManager.getChatCustomerPic().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image))
                    .load(sessionManager.getChatCustomerPic())

                    .into(ivCustomer);
        }

        etMsg.setTypeface(fontRegular);
        tvSend.setTypeface(fontSemiBold);

        ivBackButton.setOnClickListener(this);
        ivAddFiles.setOnClickListener(this);
        tvSend.setOnClickListener(this);

        VariableConstant.IS_CHATTING_OPENED = true;
        if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
            AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getProviderId(), false);
        }

        AppController.getInstance().getMqttHelper().setChatListener(this);
        MyFirebaseMessagingService.setChatListener(this);


        if (getIntent().getBooleanExtra("isActiveChat", false)) {
            getBookingDetails();
        } else {
            getMessage();
        }

        if (getIntent().getBooleanExtra("isPastChat", false)) {
            findViewById(R.id.cardViewChat).setVisibility(View.GONE);
            findViewById(R.id.booking_complete_text).setVisibility(View.VISIBLE);

        }

        etMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                scrollToBottomSlowly();
            }
        });
        etMsg.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackButton:
                onBackPressed();
                break;

            case R.id.tvSend:
                if (!etMsg.getText().toString().trim().isEmpty()) {
                    String contentMsg = etMsg.getText().toString().trim();
                    etMsg.setText("");

                    long msgId = System.currentTimeMillis();
                    sendMessage(contentMsg, msgTypeText, String.valueOf(msgId));
                }
                break;

            case R.id.ivAddFiles:
                checkPermission();
                break;

            case R.id.etMsg:
                scrollToBottomSlowly();
                break;

            case R.id.rlJobDetails:
                final Intent intent = new Intent(ChattingActivity.this, OnTheWayActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking", booking);
                bundle.putBoolean("isFromChat", true);
                intent.putExtras(bundle);
                startActivity(intent);

                       /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(ChattingActivity.this).toBundle());
                        } else {
                            startActivity(intent);
                            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                        }*/


                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        VariableConstant.IS_CHATTING_RESUMED = true;
        notificationManager.cancelAll();
        sessionManager.setChatCount(bid, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        VariableConstant.IS_CHATTING_RESUMED = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().getMqttHelper().setChatListener(null);
        MyFirebaseMessagingService.setChatListener(null);
        VariableConstant.IS_CHATTING_OPENED = false;
    }

    @Override
    public void onMessageReceived(ChatData chatData) {
        if (chatData.getBid().equals(bid)) {
            notifyAdapter(String.valueOf(chatData.getTimestamp()), chatData.getType(), chatData.getContent(), customerId, sessionManager.getProviderId(), "1");
            if (!sessionManager.getIsTeleCallRunning()) {
                NotificationHelper.playIncomingTone(this);
            }
        }
    }

    @Override
    public void onRefresh() {
        pageIndex++;
        if (isMoreDataAvailable) {
            getMessage();
        } else {
            srlChat.setRefreshing(false);
        }
    }

    private void getBookingDetails() {
        srlChat.setRefreshing(true);
        try {
            OkHttp3ConnectionStatusCode.doOkHttp3Connection(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),
                    ServiceUrl.BOOKING + "/" + bid, OkHttp3ConnectionStatusCode.Request_type.GET
                    , new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                        @Override
                        public void onSuccess(String statusCode, String result) {
                            getMessage();
                            Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                            if (result != null) {
                                try {
                                    Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                    Gson gson = new Gson();
                                    ChatBookingDetailsPojo chatBookingDetailsPojo = gson.fromJson(result, ChatBookingDetailsPojo.class);
                                    if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                        booking = chatBookingDetailsPojo.getData();
                                        rlJobDetails.setVisibility(View.VISIBLE);
                                        rlJobDetails.setOnClickListener(ChattingActivity.this);
                                        tvCategory.setText(booking.getCategory());
                                        tvPrice.setText(Utility.getPrice(String.valueOf((Double.parseDouble(booking.getAccounting().getTotal())) - Double.parseDouble(booking.getAccounting().getLastDues())) , sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(String error) {
                            getMessage();
                        }
                    });

        } catch (Exception e) {
            getMessage();
            e.printStackTrace();
        }
    }

    private void getMessage() {
        srlChat.setRefreshing(true);
        try {
            OkHttp3ConnectionStatusCode.doOkHttp3Connection(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),
                    ServiceUrl.CHAT_HISTORY + "/" + bid + "/" + pageIndex, OkHttp3ConnectionStatusCode.Request_type.GET
                    , new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                        @Override
                        public void onSuccess(String statusCode, String result) {
                            Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                            srlChat.setRefreshing(false);

                            try {
                                ChatApiResponse chatApiResponse = gson.fromJson(result, ChatApiResponse.class);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    if (chatApiResponse.getData() != null) {
                                        if (chatApiResponse.getData().size() == 0) {
                                            isMoreDataAvailable = false;
                                        }

                                        chatDataArry.addAll(chatApiResponse.getData());

                                        Collections.sort(chatDataArry, new Comparator<ChatData>() {
                                            @Override
                                            public int compare(ChatData o1, ChatData o2) {
                                                return (int) (o1.getTimestamp() - o2.getTimestamp());
                                            }
                                        });
                                        chattingAdapter.notifyDataSetChanged();

                                        if (pageIndex == 0) {
                                            scrollToBottom();
                                        }
                                    }
                                }

                                if (chatApiResponse.getMessage() != null && chatApiResponse.getMessage().equals("No Data found")) {
                                    isMoreDataAvailable = false;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(String error) {
                            srlChat.setRefreshing(false);
                        }
                    });

        } catch (Exception e) {
            srlChat.setRefreshing(false);
            e.printStackTrace();
        }
    }


    /**
     * <h2>sendmessage</h2>
     * <p>
     * sending request to channel a "Message" of socket
     *
     * @param msg     message to be send
     * @param msgType message type, i.e what type of message is it. example text message type ,image message type, video message type
     *                for txt msgtype = 1, for Image = 2;
     * @param msgId   timeStamp in GMT
     */
    private void sendMessage(String msg, String msgType, String msgId) {
        if (!sessionManager.getIsTeleCallRunning()) {
            NotificationHelper.playSentTone(ChattingActivity.this);

        }
        try {
            JSONObject jsonobj = new JSONObject();
            jsonobj.put("type", msgType);
            jsonobj.put("timestamp", msgId);
            jsonobj.put("content", msg);
            jsonobj.put("fromID", sessionManager.getProviderId());
            jsonobj.put("bid", bid + "");
            jsonobj.put("targetId", customerId);

            OkHttp3ConnectionStatusCode.doOkHttp3Connection(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),
                    ServiceUrl.CHAT_SEND, OkHttp3ConnectionStatusCode.Request_type.POST
                    , jsonobj, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                        @Override
                        public void onSuccess(String statusCode, String result) {
                            Log.d(TAG, "onSuccess: chat" + statusCode);
                            Log.d(TAG, "onSuccess: chat" + result);
                        }

                        @Override
                        public void onError(String error) {
                            Log.d(TAG, "onError: chat");
                        }
                    });

            if (msgType.equals(msgTypeText)) {
                notifyAdapter(msgId, msgType, msg, sessionManager.getProviderId(), customerId, "2");
            }

            scrollToBottom();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notifyAdapter(String msgid, final String msgtype, String msg, String fromId, String targetId, String custProType) {
        final ChatData chatData = new ChatData();
        chatData.setTimestamp(Long.parseLong(msgid));
        chatData.setType(msgtype);
        chatData.setTargetId(targetId);
        chatData.setFromID(fromId);
        chatData.setContent(msg);
        chatData.setCustProType(custProType);
        //db.addNewChat(bid,msg,fromId,targetId,msgid,msgtype,custProType);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatDataArry.add(chatData);
                chattingAdapter.notifyDataSetChanged();
                scrollToBottom();

                if (msgtype.equals(msgTypeImage)) {
                    imagePosition = chatDataArry.size() - 1;
                }
            }
        });
    }

    /*scrolling to the bottom of the recyclerview*/
    private void scrollToBottom() {
        rcvChatMsg.scrollToPosition(chattingAdapter.getItemCount() - 1);
    }

    /*scrolling to the bottom of the recyclerview*/
    private void scrollToBottomSlowly() {
        rcvChatMsg.postDelayed(new Runnable() {
            @Override
            public void run() {
                rcvChatMsg.scrollToPosition(chattingAdapter.getItemCount() - 1);
            }
        }, 200);
    }

    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
                selectImage();
            } else {
                EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                        1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
            }

        } else {
            selectImage();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private void selectImage() {

        if (mFileTemp == null) {
            String filename = VariableConstant.PROFILE_FILE_NAME + System.currentTimeMillis() + ".png";
            MyImageHandler myImageHandler = MyImageHandler.getInstance();
            mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.CROP_PIC_DIR, true), filename);
        }

        final BottomSheetDialog mDialog = new BottomSheetDialog(this);
        View view = inflater.inflate(R.layout.bottom_sheet_picture_selection, null);
        mDialog.setContentView(view);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvImageSelectionHeader = view.findViewById(R.id.tvImageSelectionHeader);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvGallery = view.findViewById(R.id.tvGallery);

        tvImageSelectionHeader.setTypeface(fontRegular);
        tvCamera.setTypeface(fontSemiBold);
        tvGallery.setTypeface(fontSemiBold);

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {

                case VariableConstant.REQUEST_CODE_GALLERY:
                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        startCropImage();
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    startCropImage();
                    break;

                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path != null) {
                        timeMillisImage = System.currentTimeMillis();
                        notifyAdapter(String.valueOf(timeMillisImage), msgTypeImage, "", sessionManager.getProviderId(), customerId, "2");
                        new UploadFileToServer().execute(path);
                    }
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for starting CropImage Activity for crop the selected image
     */
    private void startCropImage() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }


    /**
     * Uploading the file to server
     */
    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result,responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("uploadTo","1")
                        .addFormDataPart("folder",VariableConstant.CHAT_IMAGES)
                        .addFormDataPart("file", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.SIGNUPIMAGE)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: "+responseCode);
                Log.d(TAG, "doInBackground: "+result);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getJSONObject("data").getString("imageUrl");
                    sendMessage(image, msgTypeImage, String.valueOf(timeMillisImage));
                    chatDataArry.get(imagePosition).setContent(image);
                    chattingAdapter.notifyItemChanged(imagePosition);
                } else {
                    chatDataArry.remove(imagePosition);
                    chattingAdapter.notifyItemChanged(imagePosition);
                    Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                chatDataArry.remove(imagePosition);
                chattingAdapter.notifyItemChanged(imagePosition);
                Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }
}
