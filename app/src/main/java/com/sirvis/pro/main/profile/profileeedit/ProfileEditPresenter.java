package com.sirvis.pro.main.profile.profileeedit;

import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.utility.UploadFileAmazonS3;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by murashid on 10-Sep-17.
 * <h1>ProfileEditPresenter</h1>
 * ProfileEditPresenter presenter for ProfileEditActivity
 * @see ProfileEditActivity
 */

public class ProfileEditPresenter implements ProfileEditModel.ProfileEditImple {
    private ProfileEditModel model;
    private ProfileEditPresenterImple presenterImple;

    ProfileEditPresenter(ProfileEditPresenterImple presenterImple) {
        model = new ProfileEditModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session Token
     */
    void getProfileDetails(String sessionToken) {
        model.getProfile(sessionToken);
    }

    /**
     * method for passing value from view to model
     * @param amazonS3 objet of UploadFileAmazonS3
     * @param mFileTemp file has to been uploadin in amazon
     */
    void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp)
    {
        model.amazonUpload(amazonS3,mFileTemp);
    }

    /**
     * method for passing values from view to model
     * @param sessionToken session token
     * @param jsonObject reqiured fields
     */
    void updateProfileDetails(String sessionToken, JSONObject jsonObject)
    {
        presenterImple.startProgressBar();
        model.updateProfile(sessionToken,jsonObject);
    }

    /**
     * method for passing values from view to model
     * @param sessionToken session token
     */
    void logout(String sessionToken)
    {
        presenterImple.startProgressBar();
        model.logout(sessionToken);
    }

    public String getCategoryIdByComma(ArrayList<String> categories) {
        String categor="",prefix = "";
        for(String category: categories)
        {
            categor = categor + prefix + category;
            prefix = ",";
        }
        return categor;
    }


    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccessUpdateProfile(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessUpdateProfile(msg);
    }

    @Override
    public void onSuccessProfileData(ProfileData profileData) {
        presenterImple.onSuccessProfileData(profileData);
    }

    @Override
    public void onSuccessImageUpload(String imgUrl) {
     presenterImple.onSuccessImageUpload(imgUrl);
    }

    @Override
    public void onNewToken(String newToken) {
        presenterImple.onNewToken(newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.sessionExpired(msg);
    }

    @Override
    public void onFirstNameError() {
        presenterImple.stopProgressBar();
        presenterImple.onFirstNameError();
    }

    @Override
    public void onLastNameError() {
        presenterImple.stopProgressBar();
        presenterImple.onLastNameError();
    }


    interface ProfileEditPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onSuccessUpdateProfile(String msg);
        void onSuccessProfileData(ProfileData profileData);
        void onSuccessImageUpload(String imgUrl);
        void onFailure(String msg);
        void onFailure();
        void onNewToken(String newToken);
        void sessionExpired(String msg);

        void onFirstNameError();
        void onLastNameError();
    }
}
