package com.sirvis.pro.main.schedule.viewschedule;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ViewPagerAdapter;
import com.sirvis.pro.pojo.shedule.ScheduleData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

/**
 * Created by murashid on 04-Oct-17.
 * <h1>ScheduleViewActivity</h1>
 * ScheduleViewActivity for showing the list of created shcedule
 */

public class ScheduleViewActivity extends AppCompatActivity implements ScheduleViewPresenter.ScheduleViewPresenterImple {
    
    private ProgressDialog progressDialog;
    private ScheduleViewListFragment scheduleViewActive, scheduleViewPast ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_view);

        init();
    }

    /**
     * init the views
     */
    private void init()
    {
        ScheduleViewPresenter presenter = new ScheduleViewPresenter(this);
        SessionManager sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingSchedule));
        progressDialog.setCancelable(false);

        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.viewSchedule));
        tvTitle.setTypeface(fontBold);

        scheduleViewPast = ScheduleViewListFragment.getInstance();
        scheduleViewActive = ScheduleViewListFragment.getInstance();
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tablayoutScheduleView = findViewById(R.id.tablayoutScheduleView);
        ViewPager vpScheduleView = findViewById(R.id.vpScheduleView);
        viewPagerAdapter.addFragment(scheduleViewActive,getString(R.string.active));
        viewPagerAdapter.addFragment(scheduleViewPast,getString(R.string.past));
        vpScheduleView.setAdapter(viewPagerAdapter);
        tablayoutScheduleView.setupWithViewPager(vpScheduleView);
        
        presenter.getShedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess( ScheduleData data) {
        scheduleViewActive.setValues(data.getActive());
        scheduleViewPast.setValues(data.getPast());

        if(data.getPast().size() == 0 && data.getActive().size() == 0)
        {
            Toast.makeText(this,getString(R.string.noScheduleFoumd),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccessDeleteSchedule(String msg) {
      
    }

}
