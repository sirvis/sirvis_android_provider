package com.sirvis.pro.main.schedule.bookingschedule;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.pojo.shedule.ScheduleBookngPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 11-Jan-18.
 */

public class BookingScheduleModel {
    private static final String TAG = "BookingSchedule";
    private BookingModelImple modelImplement;

    BookingScheduleModel(BookingModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calinng api for geting details for particular booking
     * @param sessiontoken session Token
     * @param bookingId booking id
     */
    void getBooking(String sessiontoken,String bookingId)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken,ServiceUrl.BOOKING+"/"+bookingId,
                OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                ScheduleBookngPojo scheduleBookngPojo = gson.fromJson(result,ScheduleBookngPojo.class);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                                {
                                    modelImplement.onSuccess(scheduleBookngPojo);
                                } else {
                                    modelImplement.onFailure(scheduleBookngPojo.getMessage());
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * Method for calling api for getting the cancel reasion
     */
    void getCancelReasons(String bookingId)
    {

        Log.d(TAG, "getCancelReasons: "+bookingId);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection("", ServiceUrl.CANCEL_REASONS+"/"+bookingId , OkHttp3ConnectionStatusCode.Request_type.GET,new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            CancelPojo cancelPojo = gson.fromJson(result,CancelPojo.class);
                            modelImplement.onSuccessCancelReason(cancelPojo);
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * Method for calling api for update the job status
     * @param sessionToken session Token
     */
    void cancelBooking(String sessionToken, final JSONObject jsonObject)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CANCEL_BOOKING , OkHttp3ConnectionStatusCode.Request_type.PATCH,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        JSONObject jsonObjectResponse = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onCancelBooking(jsonObjectResponse.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObjectResponse.getString("message"));
                        }
                    }
                    else
                    {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }


    /**
     * ReviewModelImple interface for Presenter implementation
     */
    interface BookingModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ScheduleBookngPojo scheduleBookngPojo);

        void onSuccessCancelReason(CancelPojo cancelPojo);
        void onCancelBooking(String msg);
    }
}
