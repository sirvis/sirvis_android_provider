package com.sirvis.pro.main.profile.profileeedit;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.hbb20.CountryCodePicker;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.changepassword.ChangePasswordActivity;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.DatePickerCommon;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by murashid on 10-Sep-17.
 * <h1>ProfileEditActivity</h1>
 * ProfileEditActivity activity for showing and editing Profile details
 */

public class ProfileEditActivity extends AppCompatActivity implements View.OnClickListener, ProfileEditPresenter.ProfileEditPresenterImple, EasyPermissions.PermissionCallbacks, DatePickerCommon.DateSelected, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "ProfileEdit";
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private TextInputLayout tilTypeOfArtist, tilGender;
    private EditText etName,etLastName,etEmail,etPhone, etDob, etTypeOfArtist, etGender;
    private TextView tvEdit,tvChangePassword,tvLogout, tvGenderLabel;
    private ImageView ivProfile,ivImageEditButton;
    private LinearLayout llPhone,llEmail;
    private RadioButton rbMale, rbFemale, rbOthers;
    private RadioGroup rgGender;

    private DatePickerCommon datePickerFragment;

    private CountryCodePicker ccp;
    String countryCode = "";
    private String sentingDate = "";
    private SimpleDateFormat displayMonthYearFormat;
    private SimpleDateFormat dayFormat,severFormat;

    private String imageUrl = "";
    private boolean isPicturetaken;
    private File mFileTemp;
    private Typeface fontRegular;
    private UploadFileAmazonS3 amazonS3;

    private String gender = "";
    private ProfileEditPresenter profileEditPresenter;

    private CollapsingToolbarLayout ctlMyBooking;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        init();
    }

    /**
     * init the views
     */
    private void init()
    {
        datePickerFragment = new DatePickerCommon();
        datePickerFragment.setDatePickerType(2);
        datePickerFragment.setCallBack(this);
        sessionManager = SessionManager.getSessionManager(this);
        profileEditPresenter = new ProfileEditPresenter(this);
        progressDialog = new ProgressDialog(this);

        progressDialog.setCancelable(false);
        displayMonthYearFormat = new SimpleDateFormat("MMMM yyyy",Locale.US);
        dayFormat = new SimpleDateFormat("dd",Locale.US);
        severFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.US);

        amazonS3 = UploadFileAmazonS3.getInstance(this);

        fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontLight = Utility.getFontLight(this);

        ivProfile = findViewById(R.id.ivProfile);
        ivImageEditButton = findViewById(R.id.ivImageEditButton);

        tilTypeOfArtist = findViewById(R.id.tilTypeOfArtist);
        tilGender = findViewById(R.id.tilGender);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);

        etName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        etDob = findViewById(R.id.etDob);
        etTypeOfArtist = findViewById(R.id.etTypeOfArtist);
        etGender = findViewById(R.id.etGender);
        tvEdit = findViewById(R.id.tvDone);
        tvChangePassword = findViewById(R.id.tvChangePassword);
        tvLogout = findViewById(R.id.tvLogout);
        llPhone = findViewById(R.id.llPhone);
        llEmail = findViewById(R.id.llEmail);


        etName.setTypeface(fontMedium);
        etLastName.setTypeface(fontMedium);
        etEmail.setTypeface(fontMedium);
        etPhone.setTypeface(fontMedium);
        etDob.setTypeface(fontMedium);
        etTypeOfArtist.setTypeface(fontMedium);
        etGender.setTypeface(fontMedium);
        tvEdit.setTypeface(fontMedium);
        tvChangePassword.setTypeface(fontBold);
        tvLogout.setTypeface(fontBold);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
           // getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_back_button);

        }
        tvEdit.setTextColor(ContextCompat.getColor(this,R.color.colorPrimary));
        tvEdit.setText(getString(R.string.edit));

        rgGender = findViewById(R.id.rgGender);
        tvGenderLabel = findViewById(R.id.tvGenderLabel);
        tvGenderLabel.setTypeface(fontMedium);
        rbMale = findViewById(R.id.rbMale);
        rbMale.setTypeface(fontLight);
        rbMale.setOnCheckedChangeListener(this);
        rbFemale = findViewById(R.id.rbFemale);
        rbFemale.setTypeface(fontLight);
        rbFemale.setOnCheckedChangeListener(this);
        rbOthers = findViewById(R.id.rbOthers);
        rbOthers.setTypeface(fontLight);
        rbOthers.setOnCheckedChangeListener(this);

        tvEdit.setOnClickListener(this);
        ivBackButton.setOnClickListener(this);
        tvChangePassword.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
        etDob.setOnClickListener(this);

        ccp = findViewById(R.id.ccp);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });

        String filename = VariableConstant.PROFILE_FILE_NAME+ System.currentTimeMillis() + ".png";
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.CROP_PIC_DIR, true),filename);

        etLastName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                {
                    etLastName.setSelection(etLastName.getText().length());
                }
            }
        });

        profileEditPresenter.getProfileDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));

        String name = sessionManager.getFirstName();
        if(!sessionManager.getLastName().equals(""))
        {
            name = name +" "+ sessionManager.getLastName();
        }
        ctlMyBooking = findViewById(R.id.ctlMyBooking);
        ctlMyBooking.setTitle(name);
        ctlMyBooking.setCollapsedTitleTextAppearance(R.style.TextCollapedProfileName);
        ctlMyBooking.setExpandedTitleTextAppearance(R.style.TextExpandHide);
        ctlMyBooking.setCollapsedTitleGravity(Gravity.CENTER);

        setProfileOldValues();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(VariableConstant.IS_PROFILE_EDITED)
        {
            VariableConstant.IS_PROFILE_EDITED = false;
            profileEditPresenter.getProfileDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if(tvEdit.getText().toString().equals(getString(R.string.edit)))
        {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                finishAfterTransition();
            }
            else
            {
                finish();
                overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
            }
        }
        else
        {
            showConfirmationDialog();
        }
    }

    private void showConfirmationDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_app_update_non_mandatory,null);
        alertDialogBuilder.setView(view);
        TextView tvUpdate= view.findViewById(R.id.tvUpdate);
        TextView tvLater= view.findViewById(R.id.tvLater);
        TextView tvMsg= view.findViewById(R.id.tvMsg);
        ImageView ivClose  = view.findViewById(R.id.ivClose);

        tvLater.setText(getText(R.string.cancel));
        tvUpdate.setText(getText(R.string.save));
        tvMsg.setText(getText(R.string.confirmMsgForSaveProfileDetails));

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
        alertDialog.show();

        tvUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                saveProfileDetails();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                setProfileOldValues();
            }
        });

        tvLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                setProfileOldValues();
            }
        });
    }

    private void setProfileOldValues()
    {
        editEnable(false);
        tvEdit.setText(getString(R.string.edit));
        etName.setText(sessionManager.getFirstName());
        etLastName.setText(sessionManager.getLastName());
        setGender(sessionManager.getGender());

        if(!sessionManager.getProfilePic().equals(""))
        {
            Glide.with(ProfileEditActivity.this).
                    setDefaultRequestOptions(new RequestOptions()
                            .error(R.drawable.profile_default_image)
                            .transform(new CircleTransform(ProfileEditActivity.this))
                            .placeholder(R.drawable.profile_default_image))
                    .load(sessionManager.getProfilePic())
                    .into(ivProfile);
        }

        try {
            ccp.setCountryForPhoneCode(Integer.parseInt(sessionManager.getCountryCode().substring(1)));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if(!sessionManager.getDob().equals(""))
        {
            datePickerFragment.convetSendingToDisplayDate(sessionManager.getDob());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivBackButton:
                closeActivity();
                break;

            case R.id.ivProfile:
                if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
                    selectImage();
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                            1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
                }
                break;

            case R.id.tvDone:
                if(tvEdit.getText().toString().equals(getString(R.string.edit)))
                {
                    tvEdit.setText(getString(R.string.save));
                    editEnable(true);
                }
                else
                {
                   saveProfileDetails();
                }
                break;

            case R.id.tvChangePassword:
                Intent cangePasswordIntent = new Intent(this, ChangePasswordActivity.class);
                cangePasswordIntent.putExtra("isForgotPassword",false);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(cangePasswordIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(cangePasswordIntent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvLogout:
                progressDialog.setMessage(getString(R.string.loggingOut));
                profileEditPresenter.logout(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
                break;

            case R.id.etDob:
                datePickerFragment.show(getSupportFragmentManager(), "dataPicker");
                break;

            case R.id.llEmail:
                Intent emailEditIntent = new Intent(this,EditEmailPhoneActivity.class);
                emailEditIntent.putExtra("isPhoneEdit",false);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(emailEditIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(emailEditIntent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.llPhone:
                Intent phoneEditIntent = new Intent(this,EditEmailPhoneActivity.class);
                phoneEditIntent.putExtra("isPhoneEdit",true);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(phoneEditIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(phoneEditIntent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;
        }
    }

    private void saveProfileDetails() {
        try {
            if(isPicturetaken)
            {
                profileEditPresenter.amazonUpload(amazonS3,mFileTemp);
                VariableConstant.IS_PROFILE_PHOTO_UPDATED = true;
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("firstName",etName.getText().toString());
            jsonObject.put("lastName",etLastName.getText().toString());
            jsonObject.put("profilePic",imageUrl);
            jsonObject.put("dob",sentingDate);
            jsonObject.put("gender",gender);

            progressDialog.setMessage(getString(R.string.updatingProfile));

            profileEditPresenter.updateProfileDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * method for make editable and non editable the fields
     * @param editable boolen for editable and non editable
     */
    private void editEnable(boolean editable) {
        etDob.setEnabled(editable);
        etName.setEnabled(editable);
        etLastName.setEnabled(editable);
        ivProfile.setEnabled(editable);

        if(editable)
        {
            tvChangePassword.setVisibility(View.GONE);
            tvLogout.setVisibility(View.GONE);
            etTypeOfArtist.setVisibility(View.GONE);
            tilTypeOfArtist.setVisibility(View.GONE);
            tilGender.setVisibility(View.GONE);
            rgGender.setVisibility(View.VISIBLE);
            tvGenderLabel.setVisibility(View.VISIBLE);
            ivImageEditButton.setVisibility(View.VISIBLE);
            ivProfile.setOnClickListener(this);
            llPhone.setOnClickListener(this);
            llEmail.setOnClickListener(this);
            etName.setSelection(etName.getText().length());
        }
        else
        {
            tvChangePassword.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.VISIBLE);
            etTypeOfArtist.setVisibility(View.VISIBLE);
            tilTypeOfArtist.setVisibility(View.VISIBLE);
            tilGender.setVisibility(View.VISIBLE);
            rgGender.setVisibility(View.GONE);
            tvGenderLabel.setVisibility(View.GONE);
            ivImageEditButton.setVisibility(View.GONE);
            ivProfile.setOnClickListener(null);
            llPhone.setOnClickListener(null);
            llEmail.setOnClickListener(null);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        if(progressDialog.isShowing())
        {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onSuccessUpdateProfile(String msg) {
        sessionManager.setFirstName(etName.getText().toString());
        sessionManager.setLastName(etLastName.getText().toString());
        sessionManager.setProfilePic(imageUrl);
        sessionManager.setDob(sentingDate);
        sessionManager.setGender(gender);
        setGender(sessionManager.getGender());
        ctlMyBooking.setTitle(sessionManager.getFirstName());

        tvEdit.setText(getString(R.string.edit));
        editEnable(false);
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessProfileData(ProfileData profileData) {

        sessionManager.setEmail(profileData.getEmail());
        sessionManager.setPhoneNumber(profileData.getMobile());
        sessionManager.setCountryCode(profileData.getCountryCode());
        sessionManager.setFirstName(profileData.getFirstName());
        sessionManager.setLastName(profileData.getLastName());
        sessionManager.setProfilePic(profileData.getProfilePic());
        sessionManager.setDob(profileData.getDob());
        sessionManager.setGender(profileData.getGender());
        gender = profileData.getGender();

        imageUrl = profileData.getProfilePic();
        sentingDate = profileData.getDob();

        etEmail.setText(profileData.getEmail());
        etPhone.setText(profileData.getMobile());

        if(profileData.getCatNameArr() != null)
        {
            etTypeOfArtist.setText(profileEditPresenter.getCategoryIdByComma(profileData.getCatNameArr()));
        }

        try {
            ccp.setCountryForPhoneCode(Integer.parseInt(profileData.getCountryCode().substring(1)));
            String monthYear = displayMonthYearFormat.format(severFormat.parse(profileData.getDob()));
            String day = dayFormat.format(severFormat.parse(profileData.getDob()));
            etDob.setText(Utility.getDayOfMonthSuffix(Integer.parseInt(day),monthYear));

            if( profileData.getDocuments() !=null && profileData.getDocuments().size() > 0) {
                sessionManager.setIsMyDocumentPresent(1);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        setGender(sessionManager.getGender());
    }

    @Override
    public void onSuccessImageUpload(String imgUrl) {
        imageUrl = imgUrl;
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String newToken) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , newToken);
    }

    @Override
    public void sessionExpired(String msg) {
        if(!msg.isEmpty())
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.Logout.value);

        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void onFirstNameError() {
        Toast.makeText(this,getString(R.string.enterFirstName),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLastNameError() {
        Toast.makeText(this,getString(R.string.enterLastName),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    /**
     * method for creating alert dialog for choose option for select image
     */
    private void selectImage() {
        if(mFileTemp == null)
        {
            String filename = VariableConstant.PROFILE_FILE_NAME+ System.currentTimeMillis() + ".png";
            MyImageHandler myImageHandler = MyImageHandler.getInstance();
            mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.CROP_PIC_DIR, true),filename);
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.profile_pic_options, null);
        alertDialogBuilder.setView(view);

        final AlertDialog mDialog = alertDialogBuilder.create();
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button btnCamera = view.findViewById(R.id.camera);
        Button btnCancel = view.findViewById(R.id.cancel);
        Button btnGallery = view.findViewById(R.id.gallery);
        Button btnRemove = view.findViewById(R.id.removephoto);
        TextView tvHeader = view.findViewById(R.id.tvHeader);

        btnCamera.setTypeface(fontRegular);
        btnCancel.setTypeface(fontRegular);
        btnGallery.setTypeface(fontRegular);
        btnRemove.setTypeface(fontRegular);
        tvHeader.setTypeface(fontRegular);

        if (isPicturetaken) {
            btnRemove.setVisibility(View.VISIBLE);
        } else {
            btnRemove.setVisibility(View.GONE);
        }
        btnRemove.setVisibility(View.GONE);

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivProfile.setImageResource(R.drawable.profile_default_image);
                isPicturetaken = false;
                mDialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    /**
     * method for opening camera
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * mehtod for opening gallery
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (resultCode != RESULT_OK) {
                return;
            }

            switch (requestCode) {

                case VariableConstant.REQUEST_CODE_GALLERY:
                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(
                                data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        isPicturetaken = true;
                        startCropImage();

                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    isPicturetaken = true;
                    startCropImage();
                    break;

                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    Glide.with(ProfileEditActivity.this).setDefaultRequestOptions(
                            new RequestOptions()  .error(R.drawable.profile_default_image)
                                    .transform(new CircleTransform(ProfileEditActivity.this))
                                    .placeholder(R.drawable.profile_default_image))
                            .load(Uri.fromFile(new File(path)))
                            .into(ivProfile);

                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method for open the CropImage Activity for crop the image
     */
    private void startCropImage()
    {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }

    @Override
    public void onDateSelected(String sendingFormat, String displayFormat) {
        sentingDate = sendingFormat;
        etDob.setText(displayFormat);

        Log.d(TAG, "onDateSelected: "+sendingFormat + "  "+displayFormat);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(b)
        {
            switch (compoundButton.getId())
            {
                case R.id.rbMale:
                    gender = "1";
                    break;

                case R.id.rbFemale:
                    gender = "2";
                    break;

                case R.id.rbOthers:
                    gender = "3";
                    break;
            }
        }
    }

    public void setGender(String gender) {
        switch (gender)
        {
            case "1":
                etGender.setText(getString(R.string.male));
                rbMale.setChecked(true);
                break;

            case "2":
                etGender.setText(getString(R.string.feMale));
                rbFemale.setChecked(true);
                break;

            case "3":
                etGender.setText(getString(R.string.others));
                rbOthers.setChecked(true);
                break;
        }
    }


}
