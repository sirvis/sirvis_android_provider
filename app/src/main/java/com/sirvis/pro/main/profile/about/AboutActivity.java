package com.sirvis.pro.main.profile.about;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>AboutActivity</h1>
 * AboutActivity for showing the company Details
 */

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        init();
    }

    /**
     * init the views
     */
    private void init() {
        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.app_name_short));
        tvTitle.setTypeface(fontBold);

        TextView tvGoolePlay = findViewById(R.id.tvGoolePlay);
        TextView tvFacebookLike = findViewById(R.id.tvFacebookLike);
        TextView tvLegal = findViewById(R.id.tvLegal);
        TextView tvAboutMsg = findViewById(R.id.tvAboutMsg);
        TextView tvWebsite = findViewById(R.id.tvWebsite);
        TextView tvAppversion = findViewById(R.id.tvAppversion);
        TextView tvAbout = findViewById(R.id.tvAbout);


        tvGoolePlay.setTypeface(fontMedium);
        tvFacebookLike.setTypeface(fontMedium);
        tvLegal.setTypeface(fontMedium);
        tvAboutMsg.setTypeface(fontMedium);
        tvWebsite.setTypeface(fontMedium);
        tvAppversion.setTypeface(fontRegular);

        tvGoolePlay.setOnClickListener(this);
        tvFacebookLike.setOnClickListener(this);
        tvLegal.setOnClickListener(this);
        tvWebsite.setOnClickListener(this);

        tvAppversion.setText(getString(R.string.appVersion)+" "+VariableConstant.APP_VERSION);
        tvWebsite.setText(BuildConfig.WEB_SITE_LINK_SHOWING);

        setSpannableString(this,tvAbout,getString(R.string.about),"(www.getsirvis.com)");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvGoolePlay:
                openGooglePlay();
                break;

            case R.id.tvFacebookLike:
                openFaceBook();
                break;

            case R.id.tvWebsite:
                openWebSite();
                break;

            case R.id.tvLegal:
                openWebView();
                break;

        }
    }

    /**
     * method for opening website of the app
     */
    private void openWebSite()
    {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(BuildConfig.WEB_SITE_LINK));
            startActivity(i);
        }
        catch (Exception e)
        {
            Toast.makeText(this,getString(R.string.unableToOpenBrowser),Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    /**
     * method for opening current app in google play store
     */
    private void openGooglePlay()
    {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(VariableConstant.PLAY_STORE_LINK));
            startActivity(i);
        }
        catch (Exception e)
        {
            Toast.makeText(this,getString(R.string.unableToOpenPlayStore),Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * method for opening facebook page of the app
     */
    private void openFaceBook() {
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL();
        facebookIntent.setData(Uri.parse(BuildConfig.FACEBOOK_LINK));
        startActivity(facebookIntent);
    }

    /**
     * method for opening the Webview of the Legal
     */
    private void openWebView() {
        Intent intent;
        try {
            intent =new Intent(Intent.ACTION_VIEW);
            intent.setPackage("com.google.android.youtube");
            intent.setData(Uri.parse(BuildConfig.YOUTUBE_PAGE));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(BuildConfig.YOUTUBE_PAGE));
            startActivity(intent);
        }
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    /**
     * mehtod for checking the facebook app installed or not , if installed then return valid page url accoring to the facebook app
     * @return url of the Facebook page of the app
     */
    public String getFacebookPageURL() {
        PackageManager packageManager = getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + BuildConfig.FACEBOOK_LINK;
            } else { //older versions of fb app
                return "fb://page/" + BuildConfig.FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return BuildConfig.FACEBOOK_LINK; //normal web url
        }
    }

    public void setSpannableString(Context context, TextView txtSpan, String totalString, String termsString) {
        ForegroundColorSpan colorPrimaryTerms = new ForegroundColorSpan(context.getResources().getColor(R.color.red));
        Spannable spanText = new SpannableString(totalString);
        spanText.setSpan(colorPrimaryTerms, totalString.indexOf(termsString),totalString.indexOf(termsString)+termsString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ClickableSpan csterms = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View v) {
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://getsirvis.com/"));
                    startActivity(i);
                }
                catch (Exception e)
                {
                    Toast.makeText(AboutActivity.this,getString(R.string.unableToOpenBrowser),Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            } };
        spanText.setSpan(csterms, totalString.indexOf(termsString),totalString.indexOf(termsString)+termsString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtSpan.setMovementMethod(LinkMovementMethod.getInstance());
        txtSpan.setText(spanText);
    }

}
