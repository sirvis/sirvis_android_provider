package com.sirvis.pro.main.profile.profileeedit;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.utility.AppController;
import com.hbb20.CountryCodePicker;
import com.sirvis.pro.R;
import com.sirvis.pro.changepassword.OTPVerifyActivity;
import com.sirvis.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

/**
 * Created by murashid on 11-Oct-17.
 * <h1>EditEmailPhoneActivity</h1>
 * EditEmailPhoneActivity activity for changing email or phone number based on isPhoneEdit
 */

public class EditEmailPhoneActivity extends AppCompatActivity implements EditEmailPhonePresenter.EditEmailPhonePresenterImple, View.OnClickListener {

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private EditEmailPhonePresenter presenter;

    private EditText etEmail,etPhone;
    private boolean isPhoneEdit;
    private String countryCode = "";
    private CountryCodePicker ccp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email_phone);

        init();
    }

    /**
     * init the views
     */
    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new EditEmailPhonePresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fontBold);

        TextView tvSave = findViewById(R.id.tvSave);
        tvSave.setTypeface(fontBold);
        tvSave.setOnClickListener(this);

        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        ImageView ivEmailPhone = findViewById(R.id.ivEmailPhone);


        etEmail.setTypeface(fontMedium);
        etPhone.setTypeface(fontMedium);

        TextView tvChangePhoneEmailHint = findViewById(R.id.tvChangePhoneEmailHint);
        tvChangePhoneEmailHint.setTypeface(fontMedium);
        LinearLayout llPhone = findViewById(R.id.llPhone);
        ImageView ivEmailClear = findViewById(R.id.ivEmailClear);
        ivEmailClear.setOnClickListener(this);

        ccp = findViewById(R.id.ccp);
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });
        try {
            ccp.setCountryForPhoneCode(Integer.parseInt(sessionManager.getCountryCode().substring(1)));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        isPhoneEdit = getIntent().getBooleanExtra("isPhoneEdit",false);

        if(isPhoneEdit)
        {
            progressDialog.setMessage(getString(R.string.updatingPhone));
            tvTitle.setText(getString(R.string.changePhone));
            tvSave.setText(getString(R.string.save));
            tvChangePhoneEmailHint.setText(getString(R.string.phoneChangHint));
            etEmail.setVisibility(View.GONE);
            ivEmailClear.setVisibility(View.GONE);
            ivEmailPhone.setImageResource(R.drawable.change_mobile_icon);

            Utility.showKeyBoard(this,etEmail);
        }
        else
        {
            progressDialog.setMessage(getString(R.string.updadingEmail));
            tvTitle.setText(getString(R.string.changeEmail));
            tvSave.setText(getString(R.string.save));
            tvChangePhoneEmailHint.setText(getString(R.string.emailChangHint));
            llPhone.setVisibility(View.GONE);
            ivEmailPhone.setImageResource(R.drawable.change_email_icon);

            Utility.showKeyBoard(this,etPhone);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvSave:
                try {
                    if(isPhoneEdit)
                    {
                        presenter.updateValue(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),isPhoneEdit,countryCode,etPhone.getText().toString());
                    }
                    else
                    {
                        presenter.updateValue(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),isPhoneEdit,countryCode,etEmail.getText().toString());
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case R.id.ivEmailClear:
                etEmail.setText("");
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onErrorPhone() {
        etPhone.setEnabled(true);
        etPhone.setError(getString(R.string.enterPhone));
    }

    @Override
    public void onErrorEmail() {
        //etEmail.setEnabled(true);
        //etEmail.setError(getString(R.string.enterValidEmail));
        Toast.makeText(this,getString(R.string.enterValidEmail),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessPhone(PhoneValidationPojo phoneValidationPojo) {
        Utility.printLog("EmailPhone",phoneValidationPojo.toString());
        VariableConstant.IS_PROFILE_EDITED = true;
        finish();
        Intent intent = new Intent(this, OTPVerifyActivity.class);
        intent.putExtra("phone",etPhone.getText().toString());
        intent.putExtra("countryCode",countryCode);
        intent.putExtra("userId", phoneValidationPojo.getData().getSid());
        intent.putExtra("expireOtp", phoneValidationPojo.getData().getExpireOtp());
        intent.putExtra("otpOption",3);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else
        {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
        }
    }

    @Override
    public void onSuccessEmail(String msg) {
        VariableConstant.IS_PROFILE_EDITED = true;
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

}
