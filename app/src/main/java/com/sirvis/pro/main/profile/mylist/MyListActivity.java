package com.sirvis.pro.main.profile.mylist;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CommonPagerAdapter;
import com.sirvis.pro.address.AddressListActivity;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.metaData.MetaDataArr;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.MyImageHandler;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;
import com.sirvis.pro.utility.ViewPagerCustomTransformer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by murashid on 13-Sep-17.
 * <h1>MyListActivity</h1>
 * MyListActivity acitvity for showing mylist
 */

public class MyListActivity extends AppCompatActivity implements View.OnClickListener, MyListPresenter.MyListPresenterImple, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "MyListActivity";
    private static final int PLACE_PICKER_REQUEST = 408, ADDRESS_REQUEST = 500;
    private ProgressDialog progressDialog;
    private MyListPresenter presenter;
    private SessionManager sessionManager;
    private TextView tvLocation, tvAddLocation, tvRadius, tvNoImage;
    private ImageView ivDeleteIcon;

    private LinearLayout llMyList, llMyListDynamic;

    private ProfileData profileData;
    private SwitchCompat switchStatus;

    private CommonPagerAdapter commonPagerAdapter;
    private ViewPager viewPagerMyList;

    private String location = "";

    private File mFileTemp;
    private ArrayList<String> imageList;
    private UploadFileAmazonS3 amazonS3;

    private LayoutInflater inflater;
    private Typeface fonMedium, fonRegular;

    private ImageDialogFragment imageDialogFragment;


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Utility.checkAndShowNetworkError(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);

        init();
    }

    /**
     * init the views
     */
    private void init() {
        presenter = new MyListPresenter(this);
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingMyList));
        progressDialog.setCancelable(false);


        imageDialogFragment = ImageDialogFragment.newInstance();

        fonMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);
        fonRegular = Utility.getFontRegular(this);

        inflater = getLayoutInflater();
        imageList = new ArrayList<>();
        amazonS3 = UploadFileAmazonS3.getInstance(this);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.myListing));
        tvTitle.setTypeface(fontBold);

        tvLocation = findViewById(R.id.tvLocation);
        tvRadius = findViewById(R.id.tvRadius);
        switchStatus = findViewById(R.id.switchStatus);
        tvNoImage = findViewById(R.id.tvNoImage);

        llMyList = findViewById(R.id.llMyList);
        llMyListDynamic = findViewById(R.id.llMyListDynamic);
        llMyList.setVisibility(View.GONE);

        tvLocation.setTypeface(fonRegular);
        tvRadius.setTypeface(fonRegular);
        tvNoImage.setTypeface(fonRegular);

        TextView tvLocationLabel = findViewById(R.id.tvLocationLabel);
        tvAddLocation = findViewById(R.id.tvAddLocation);
        TextView tvRadiusLabel = findViewById(R.id.tvRadiusLabel);
        TextView tvAddRadius = findViewById(R.id.tvAddRadius);
        TextView tvStatusLabel = findViewById(R.id.tvStatusLabel);
        TextView tvStatusHint = findViewById(R.id.tvStatusHint);

        ImageView ivAddIcon = findViewById(R.id.ivAddIcon);
        ivDeleteIcon = findViewById(R.id.ivDeleteIcon);

        commonPagerAdapter = new CommonPagerAdapter();
        viewPagerMyList = findViewById(R.id.viewPagerMyList);
        viewPagerMyList.setPageTransformer(true, new ViewPagerCustomTransformer());
        TabLayout tablayoutMyList = findViewById(R.id.tablayoutMyList);
        viewPagerMyList.setAdapter(commonPagerAdapter);
        tablayoutMyList.setupWithViewPager(viewPagerMyList);

        tvLocationLabel.setTypeface(fonMedium);
        tvAddLocation.setTypeface(fonMedium);
        tvRadiusLabel.setTypeface(fonMedium);
        tvAddRadius.setTypeface(fonMedium);
        tvStatusLabel.setTypeface(fonMedium);
        tvStatusHint.setTypeface(fonRegular);

        tvAddLocation.setOnClickListener(this);
        tvNoImage.setOnClickListener(this);
        tvAddRadius.setOnClickListener(this);
        ivAddIcon.setOnClickListener(this);
        ivDeleteIcon.setOnClickListener(this);

        presenter.getProfile(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.MyListOpen.value);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (VariableConstant.IS_MY_LIST_EDITED) {
            VariableConstant.IS_MY_LIST_EDITED = false;
            progressDialog.setMessage(getString(R.string.gettingMyList));
            presenter.getProfile(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {

        if (profileData != null) {
            switch (v.getId()) {
                case R.id.tvAddLocation:
                    Intent intentAddress = new Intent(this, AddressListActivity.class);
                    intentAddress.putExtra("isFromProfile", true);
                    startActivityForResult(intentAddress, ADDRESS_REQUEST);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                    break;

                case R.id.tvAddRadius:
                    Intent intent = new Intent(this, MyListEditActivity.class);
                    intent.putExtra("title", getString(R.string.radiusHeader));
                    intent.putExtra("hint", getString(R.string.radiusHint));
                    intent.putExtra("data", profileData.getRadius());
                    intent.putExtra("editableParam", "radius");
                    intent.putExtra("minRadius", profileData.getMinRadius());
                    intent.putExtra("maxRadius", profileData.getMaxRadius());
                    intent.putExtra("myListType", "1");
                    startMyListEditActivity(intent);
                    break;

                case R.id.ivAddIcon:
                case R.id.tvNoImage:
                    doSelectImage();
                    break;

                case R.id.ivDeleteIcon:
                    int position = viewPagerMyList.getCurrentItem();
                    Log.d(TAG, "onClick: " + position);

                    if (imageList.size() > 0) {
                        imageList.remove(position);

                        commonPagerAdapter.removeView(viewPagerMyList, position);
                        commonPagerAdapter.notifyDataSetChanged();

                        if (imageList.size() == 0) {
                            tvNoImage.setVisibility(View.VISIBLE);
                            ivDeleteIcon.setVisibility(View.GONE);
                        }
                        try {

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("workImage", new JSONArray(imageList));
                            Log.d(TAG, "onActivityResult: " + jsonObject);
                            presenter.updateWorkImage(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    }

    private void startMyListEditActivity(Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else {
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        progressDialog.setMessage(getString(R.string.updating));
        if (isChecked) {
            presenter.updateProfileStatus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), "profileStatus", VariableConstant.PROFLIE_ACTIVE_STATUS);
        } else {
            presenter.updateProfileStatus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), "profileStatus", VariableConstant.PROFILE_IN_ACTIVE_STATUS);
        }
    }

    @Override
    public void onSuccess(final ProfileData profileData) {

        llMyList.setVisibility(View.VISIBLE);
        Animation fade_open = AnimationUtils.loadAnimation(this, R.anim.fade_open);
        ;
        llMyList.startAnimation(fade_open);

        this.profileData = profileData;
        tvLocation.setText(profileData.getAddress());
        location = profileData.getAddress();
        tvRadius.setText(profileData.getRadius() + " " + sessionManager.getDistanceUnit());

        llMyListDynamic.removeAllViews();

        ArrayList<MetaDataArr> metaDataArrs = profileData.getMetaDataArr();
        Collections.sort(metaDataArrs, new Comparator<MetaDataArr>() {
            @Override
            public int compare(MetaDataArr o1, MetaDataArr o2) {
                return o1.getFieldName().compareTo(o2.getFieldName());
            }
        });

        for (int i = 0; i < metaDataArrs.size(); i++) {
            final MetaDataArr metaDataArr = metaDataArrs.get(i);
            View view = inflater.inflate(R.layout.single_row_my_list_metadata, null);
            TextView tvDataLabel = view.findViewById(R.id.tvDataLabel);
            TextView tvEdit = view.findViewById(R.id.tvEdit);
            TextView tvData = view.findViewById(R.id.tvData);
            LinearLayout llData = view.findViewById(R.id.llData);

            tvDataLabel.setTypeface(fonMedium);
            tvEdit.setTypeface(fonMedium);
            tvData.setTypeface(fonRegular);

            tvDataLabel.setText(metaDataArr.getFieldName());

            if (metaDataArr.getData() != null && !metaDataArr.getData().equals("")) {
                switch (metaDataArr.getFieldType()) {
                    case "1":
                        tvData.setText(metaDataArr.getData());
                        break;
                    case "3":
                    case "4":
                        tvData.setText(presenter.returmSendingToDisplayDate(metaDataArr.getData()));
                        break;
                    default:
                        tvData.setVisibility(View.GONE);
                        llData.setVisibility(View.VISIBLE);
                        String[] data = metaDataArr.getData().split(",");
                        for (String value : data) {
                            View viewWithBullet = inflater.inflate(R.layout.single_row_my_list_metadata_text_view_with_bullet, null);
                            TextView tvDataWithBullet = viewWithBullet.findViewById(R.id.tvDataWithBullet);
                            tvDataWithBullet.setText(value.trim());
                            llData.addView(viewWithBullet);
                        }
                        break;
                }
            } else {
                tvData.setHint(metaDataArr.getDescription());
            }

            tvEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MyListActivity.this, MyListEditActivity.class);
                    intent.putExtra("title", metaDataArr.getFieldName());
                    if (metaDataArr.getData() != null) {
                        intent.putExtra("data", metaDataArr.getData());
                    } else {
                        intent.putExtra("data", "");
                    }
                    intent.putExtra("editableParam", metaDataArr.get_id());
                    intent.putExtra("fieldType", metaDataArr.getFieldType());
                    intent.putExtra("hint", metaDataArr.getDescription());
                    intent.putExtra("myListType", "2");
                    if (metaDataArr.getPreDefined() != null) {
                        intent.putExtra("preDefined", metaDataArr.getPreDefined());
                    }
                    startMyListEditActivity(intent);
                }
            });

            llMyListDynamic.addView(view);
        }

        if (!profileData.getAddress().equals("")) {
            tvLocation.setHint("");
            tvAddLocation.setText(getString(R.string.edit));
        }
        if (!profileData.getRadius().equals("")) {
            tvRadius.setHint("");
        }

        if (profileData.getProfileStatus().equals("1")) {
            switchStatus.setChecked(true);
        } else {
            switchStatus.setChecked(false);
        }

        imageList.clear();
        commonPagerAdapter.clear();
        for (String imageUrl : profileData.getWorkImage()) {
            tvNoImage.setVisibility(View.GONE);
            ivDeleteIcon.setVisibility(View.VISIBLE);
            imageList.add(imageUrl);
            commonPagerAdapter.addView(getImageView(imageUrl, false));
        }

        if (profileData.getDocuments() != null && profileData.getDocuments().size() > 0) {
            sessionManager.setIsMyDocumentPresent(1);
        }

        commonPagerAdapter.notifyDataSetChanged();
        switchStatus.setOnCheckedChangeListener(this);

        //Aspect ratio for My list job photos
        final RelativeLayout rlMyList = findViewById(R.id.rlMyList);
        rlMyList.postDelayed(new Runnable() {
            @Override
            public void run() {
                int width = rlMyList.getWidth();
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) rlMyList.getLayoutParams();
                lp.width = width;
                lp.height = (int) (width * 0.67);
                rlMyList.setLayoutParams(lp);
            }
        }, 500);
    }

    private View getImageView(final String image, final boolean isfile) {

        View view = inflater.inflate(R.layout.fragment_my_list_image, null);
        ImageView ivWorkImage = view.findViewById(R.id.ivWorkImage);
        if (isfile) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.upload_take_photo_icon)
            ).
                    load(Uri.fromFile(new File(image)))
                    .into(ivWorkImage);
        } else if (!image.equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.upload_take_photo_icon)).
            load(image)

                    .into(ivWorkImage);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageDialogFragment.setImageUrl(image, isfile);
                imageDialogFragment.show(getSupportFragmentManager(), "summa");
            }
        });

        return view;
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureUpdateStatus() {
        Toast.makeText(this, getString(R.string.updateStatusError), Toast.LENGTH_SHORT).show();
        switchStatus.setOnCheckedChangeListener(null);
        switchStatus.setChecked(!switchStatus.isChecked());
        switchStatus.setOnCheckedChangeListener(this);
    }


    @Override
    public void onSuccessUpdateStatus(String msg) {
        AppController.getInstance().getMixpanelHelper().myListUpdated("location");
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailureLocation() {
        tvLocation.setText(location);
        Toast.makeText(this, getString(R.string.updateLocationError), Toast.LENGTH_SHORT).show();
    }


    private void doSelectImage() {
        if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            selectImage();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                    1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
        }
    }


    private void selectImage() {

        String filename = VariableConstant.WORK_IMAGE_FILE_NAME + System.currentTimeMillis() + ".png";
        MyImageHandler myImageHandler = MyImageHandler.getInstance();
        mFileTemp = new File(myImageHandler.getAlbumStorageDir(this, VariableConstant.DOCUMENT_PIC_DIR, true), filename);
        Log.i(TAG, "selectImage: " + mFileTemp);

        final BottomSheetDialog mDialog = new BottomSheetDialog(this);
        View view = inflater.inflate(R.layout.bottom_sheet_picture_selection, null);
        mDialog.setContentView(view);
        mDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvImageSelectionHeader = view.findViewById(R.id.tvImageSelectionHeader);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvGallery = view.findViewById(R.id.tvGallery);


        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                mDialog.dismiss();
            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGallery();
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    /**
     * open camera for taking image for user profile
     */
    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            mImageCaptureUri = Uri.fromFile(mFileTemp);
            Log.i(TAG, "takePicture: " + mImageCaptureUri);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mImageCaptureUri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", mFileTemp);
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, VariableConstant.REQUEST_CODE_TAKE_PICTURE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * open gallery for selecting image for user profile image
     */
    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, VariableConstant.REQUEST_CODE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {
                case ADDRESS_REQUEST:
                    String locationId = data.getStringExtra("SELECTED_ADDRESS_ID");
                    tvLocation.setText(data.getStringExtra("SELECTED_ADDRESS_NAME"));
                    progressDialog.setMessage(getString(R.string.updating));
                    presenter.updateProfileStatus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), "address", locationId);
                    break;

                case PLACE_PICKER_REQUEST:
                   /* Place place = PlacePicker.getPlace(this, data);
                    CharSequence name = place.getName();
                    CharSequence address = place.getAddress();
                    String latitude = String.valueOf(place.getLatLng().latitude);
                    String longitude = String.valueOf(place.getLatLng().longitude);
                    String id = place.getId();

                    tvLocation.setText(String.valueOf(address));
                    presenter.updateProfileStatus(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), "address", String.valueOf(address));
                    */break;

                case VariableConstant.REQUEST_CODE_GALLERY:
                    try {
                        InputStream inputStream = this.getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                        Utility.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        //isPicturetaken = true;
                        startCropImage();
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;

                case VariableConstant.REQUEST_CODE_TAKE_PICTURE:
                    // isPicturetaken = true;
                    startCropImage();
                    break;

                case VariableConstant.REQUEST_CODE_CROP_IMAGE:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    Log.i(TAG, "onActivityResult: " + path);
                    if (path == null) {
                        return;
                    }
                    commonPagerAdapter.addView(getImageView(path, true));
                    commonPagerAdapter.notifyDataSetChanged();
                    imageList.add(presenter.amazonUpload(amazonS3, mFileTemp));

                    viewPagerMyList.setCurrentItem(imageList.size() - 1);
                    try {
                        progressDialog.setMessage(getString(R.string.updating));
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("workImage", new JSONArray(imageList));
                        Log.d(TAG, "onActivityResult: " + jsonObject);

                        presenter.updateWorkImage(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);

                        tvNoImage.setVisibility(View.GONE);
                        ivDeleteIcon.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }
    }

    /**
     * method for starting CropImage Activity for crop the selected image
     */
    private void startCropImage() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);
        intent.putExtra("locale", Locale.getDefault().getDisplayLanguage());
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_CROP_IMAGE);
    }


}
