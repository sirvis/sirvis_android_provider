package com.sirvis.pro.main.profile.myratecard;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.ratecard.CategoryService;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 13-Feb-18.
 */

public class EditCategoryServicePresenter {

    private static final String TAG = "SelectCategory";
    private View view;

    public EditCategoryServicePresenter(View view)
    {
        this.view = view;
    }


    public String getServiceIdByComma(ArrayList<CategoryService> categoryServices) {
        String selelectedId="",prefix = "";
        for(CategoryService service: categoryServices)
        {
            selelectedId = selelectedId + prefix + service.get_id();
            prefix = ",";
        }
        return selelectedId;
    }


    /**
     * method for calling api for updating the Category or services details
     * @param sessionToken session Token
     */
    public void updateCategoryService(final String sessionToken, final JSONObject jsonObject, final int min , final int max)
    {
        try {
            double amount = Double.parseDouble(jsonObject.getString("price"));
            if(amount < min || amount  > max)
            {
                view.invalidPrice();
                return;
            }
            if(jsonObject.has("serviceCompletionTime") && jsonObject.getString("serviceCompletionTime").equals(""))
            {
                view.invalidTime();
                return;
            }
            if(jsonObject.has("description") && jsonObject.getString("description").equals(""))
            {
                view.invalidDesc();
                return;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.UPDATE_CATEGORY_SERVICE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObjectResponse = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccess(jsonObjectResponse.getString("message"));
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                updateCategoryService(newToken,jsonObject, min , max);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObjectResponse.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObjectResponse.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }


 /**
     * method for calling api for updating the Category or services details
     * @param sessionToken session Token
     */
    public void updateServices(final String sessionToken, final JSONObject jsonObject)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.UPDATE_SERVICES, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObjectResponse = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccess(jsonObjectResponse.getString("message"));
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObjectResponse.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                updateServices(newToken,jsonObject);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObjectResponse.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObjectResponse.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }



    public interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void invalidPrice();
        void invalidTime();
        void invalidDesc();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(String msg);
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
