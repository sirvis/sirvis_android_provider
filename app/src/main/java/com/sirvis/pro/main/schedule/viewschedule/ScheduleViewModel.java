package com.sirvis.pro.main.schedule.viewschedule;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.shedule.ScheduleData;
import com.sirvis.pro.pojo.shedule.ScheduleViewPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 04-Oct-17.
 * <h1>ScheduleViewModel</h1>
 * ScheduleViewModel model for ScheduleViewActivity
 * @see ScheduleViewActivity
 */

public class ScheduleViewModel {
    private static final String TAG = "AddressListModel";
    private ScheduleViewModelImple modelImplement;

    ScheduleViewModel(ScheduleViewModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for getting the view schedule
     * @param sessiontoken session Token
     */
    void getShedule(String sessiontoken)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.SCHEDULE , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        ScheduleViewPojo scheduleViewPojo = gson.fromJson(result,ScheduleViewPojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(scheduleViewPojo.getData());
                        } else {
                            modelImplement.onFailure(scheduleViewPojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * method for calling the api for deleting the schedule based on the address id
     * @param sessiontoken session token
     * @param id address id
     */
    void deleteSchedule(String sessiontoken,String id)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("scheduleId",id);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.SCHEDULE , OkHttp3ConnectionStatusCode.Request_type.DELETE,jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessDeleteSchedule(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ScheduleViewModelImple interface for presenter implementation
     */
    interface ScheduleViewModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ScheduleData data);
        void onSuccessDeleteSchedule(String msg);
    }


}
