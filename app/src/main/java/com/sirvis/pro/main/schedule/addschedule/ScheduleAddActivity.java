package com.sirvis.pro.main.schedule.addschedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.address.AddressListActivity;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 02-Oct-17.
 * <h1>ScheduleAddActivity</h1>
 * ScheduleAddActivity for adding the schedule
 */

public class ScheduleAddActivity extends AppCompatActivity implements View.OnClickListener, ScheduleAddPresenter.ScheduleAddPresenterImple, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "ScheduleAdd";

    private ScheduleAddPresenter presenter;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private TextView tvRepeatSchedule, tvRepeatDays, tvDuration, tvStartTime, tvEndTime;
    private TextView tvLocation;

    private ArrayList<String> sentingDays;
    private String startDay = "",endDay ="";
    private String startTime ="",endTime = "";
    private String locationId="";


    private CheckBox cbInCall, cbTeleCall;
    private TextView tvInCallSlotDuration, tvTeleCallSlotDuration;
    private String isInCallEnabled = "0" , isOutCallEnabled="0", isTeleCallEnabled = "0", slotDuration ="", slotDurationTemp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_add);

        init();
    }

    /**
     * init the views
     */
    private void init() {
        presenter = new ScheduleAddPresenter(this,this);
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.addingSchedule));
        progressDialog.setCancelable(false);
        sentingDays = new ArrayList<>();

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);
        Typeface fontMedium = Utility.getFontMedium(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_x);
        }
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.addSchedule));
        tvTitle.setTypeface(fontBold);

        Button btnConfirm=(Button)findViewById(R.id.btnConfirm);
        btnConfirm.setTypeface(fontBold);

        TextView tvRepeatScheduleLabel = (TextView) findViewById(R.id.tvRepeatScheduleLabel);
        tvRepeatScheduleLabel.setTypeface(fontMedium);

        TextView tvRepeatDaysLabel = (TextView) findViewById(R.id.tvRepeatDaysLabel);
        tvRepeatDaysLabel.setTypeface(fontMedium);

        TextView tvDurationLabel = (TextView) findViewById(R.id.tvDurationLabel);
        tvDurationLabel.setTypeface(fontMedium);

        TextView tvStartTimeLabel = (TextView) findViewById(R.id.tvStartTimeLabel);
        tvStartTimeLabel.setTypeface(fontMedium);

        TextView tvEndTimeLabel = (TextView) findViewById(R.id.tvEndTimeLabel);
        tvEndTimeLabel.setTypeface(fontMedium);

        TextView tvLocHeader = (TextView) findViewById(R.id.tvLocHeader);
        tvLocHeader.setTypeface(fontMedium);

        TextView tvJobTypeLabel = (TextView) findViewById(R.id.tvJobTypeLabel);
        tvJobTypeLabel.setTypeface(fontMedium);

        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvLocation.setTypeface(fontRegular);

        tvRepeatSchedule=(TextView) findViewById(R.id.tvRepeatSchedule);
        tvRepeatSchedule.setTypeface(fontRegular);

        tvRepeatDays=(TextView)findViewById(R.id.tvRepeatDays);
        tvRepeatDays.setTypeface(fontRegular);

        tvDuration=(TextView)findViewById(R.id.tvDuration);
        tvDuration.setTypeface(fontRegular);

        tvStartTime=(TextView)findViewById(R.id.tvStartTime);
        tvStartTime.setTypeface(fontRegular);

        tvEndTime=(TextView) findViewById(R.id.tvEndTime);
        tvEndTime.setTypeface(fontRegular);

        cbInCall = findViewById(R.id.cbInCall);
        CheckBox cbOutCall = findViewById(R.id.cbOutCall);
        cbTeleCall = findViewById(R.id.cbTeleCall);
        tvInCallSlotDuration = findViewById(R.id.tvInCallSlotDuration);
        tvTeleCallSlotDuration = findViewById(R.id.tvTeleCallSlotDuration);

        cbInCall.setTypeface(fontMedium);
        cbOutCall.setTypeface(fontMedium);
        cbTeleCall.setTypeface(fontMedium);
        tvInCallSlotDuration.setTypeface(fontRegular);
        tvTeleCallSlotDuration.setTypeface(fontRegular);

        tvRepeatSchedule.setOnClickListener(this);
        tvRepeatDays.setOnClickListener(this);
        tvDuration.setOnClickListener(this);
        tvStartTime.setOnClickListener(this);
        tvStartTime.setOnClickListener(this);
        tvEndTime.setOnClickListener(this);
        tvLocation.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        cbInCall.setOnCheckedChangeListener(this);
        cbOutCall.setOnCheckedChangeListener(this);
        cbTeleCall.setOnCheckedChangeListener(this);

        findViewById(R.id.llIncall).setOnClickListener(this);
        findViewById(R.id.llTelecall).setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        finish();
        overridePendingTransition(R.anim.stay,R.anim.top_to_bottom);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ScheduleSelectionActivity.class);
        switch (v.getId()) {
            case R.id.tvRepeatSchedule:
                intent.putExtra("type", 1);
                intent.putExtra("values",tvRepeatSchedule.getText().toString());
                startActivityForResult(intent, VariableConstant.REQUEST_CODE_REPEAT_MODE);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;

            case R.id.tvRepeatDays:
                if (!tvRepeatSchedule.getText().toString().equals("")) {
                    intent.putExtra("values",tvRepeatDays.getText().toString());
                    intent.putExtra("type", 2);
                    startActivityForResult(intent,VariableConstant.REQUEST_CODE_DAYS);
                    overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                } else {
                    DialogHelper.showWaringMessage(this, getString(R.string.plsSelectMode));
                }
                break;

            case R.id.tvDuration:
                intent.putExtra("type", 5);
                intent.putExtra("values",tvDuration.getText().toString());
                intent.putExtra("START_DATE",startDay);
                intent.putExtra("END_DATE",endDay);
                startActivityForResult(intent, VariableConstant.REQUEST_CODE_MONTH);
                overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;

            case R.id.llIncall:
                if(cbInCall.isChecked())
                    startActivityForCallSlotDuration();
                break;

             case R.id.llTelecall:
                if(cbTeleCall.isChecked())
                    startActivityForCallSlotDuration();
                break;

            case R.id.tvStartTime:
                presenter.selectTime(0,startTime);
                break;

            case R.id.tvEndTime:
                presenter.selectTime(1,endTime);
                break;

            case R.id.tvLocation:
                Intent intentAddress = new Intent(this, AddressListActivity.class);
                intent.putExtra("isFromProfile",false);
                startActivityForResult(intentAddress, VariableConstant.REQUEST_CODE_ADDRESS);
                overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                break;

            case R.id.btnConfirm:
                addSchedule();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId())
        {
            case R.id.cbInCall:
                if(b)
                {
                    isInCallEnabled = "1";
                    tvInCallSlotDuration.setText(slotDurationTemp);
                    startActivityForCallSlotDuration();
                }
                else
                {
                    tvInCallSlotDuration.setText("");
                    isInCallEnabled = "0";
                }
                break;

             case R.id.cbOutCall:
                if(b)
                {
                    isOutCallEnabled = "1";
                }
                else
                {
                    isOutCallEnabled = "0";
                }
                break;

             case R.id.cbTeleCall:
                if(b)
                {
                    isTeleCallEnabled = "1";
                    tvTeleCallSlotDuration.setText(slotDurationTemp);
                    startActivityForCallSlotDuration();
                }
                else
                {
                    tvTeleCallSlotDuration.setText("");
                    isTeleCallEnabled = "0";
                }
                break;
        }
    }

    private void startActivityForCallSlotDuration()
    {
        Intent intent = new Intent(this, ScheduleSelectionActivity.class);
        intent.putExtra("type", 6);
        intent.putExtra("values", slotDuration);
        startActivityForResult(intent, VariableConstant.REQUEST_CODE_SLOT_DURATION);
        overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
    }
    /**
     * method  for calling method in presenter and passing required field
     */
    private void addSchedule()
    {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("repeatDay",4);
            jsonObject.put("startDate",startDay);
            jsonObject.put("endDate",endDay);
            jsonObject.put("startTime",startTime);
            jsonObject.put("endTime",endTime);
            jsonObject.put("addresssId",locationId);
            jsonObject.put("days",new JSONArray(sentingDays));
            jsonObject.put("inCall", isInCallEnabled);
            jsonObject.put("outCall",isOutCallEnabled);
            jsonObject.put("teleCall",isTeleCallEnabled);
            jsonObject.put("slotDuration", slotDuration);

            Log.d(TAG, "addSchedule: "+jsonObject);
            presenter.addSchedule(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case VariableConstant.REQUEST_CODE_ADDRESS:
                    locationId = data.getStringExtra("SELECTED_ADDRESS_ID");
                    tvLocation.setText(data.getStringExtra("SELECTED_ADDRESS_NAME"));
                    break;

                case VariableConstant.REQUEST_CODE_REPEAT_MODE:
                    presenter.selectMode(data.getIntExtra("SELECTED_VALUE",0));
                    break;

                case VariableConstant.REQUEST_CODE_DAYS:
                    presenter.sortDays(data.getStringArrayListExtra("SELECTED_VALUE"));
                    break;

                case VariableConstant.REQUEST_CODE_MONTH:
                    startDay = data.getStringExtra("START_DATE");
                    endDay = data.getStringExtra("END_DATE");
                    presenter.selectDuration(data.getIntExtra("SELECTED_VALUE",0),
                            data.getStringExtra("DISPLAY_START_DATE"),data.getStringExtra("DISPLAY_END_DATE"));
                    break;

                case VariableConstant.REQUEST_CODE_SLOT_DURATION:
                    slotDuration = data.getStringExtra("SELECTED_VALUE");
                    slotDurationTemp = data.getStringExtra("SELECTED_DURATION");
                    if(cbInCall.isChecked())
                    {
                        tvInCallSlotDuration.setText(data.getStringExtra("SELECTED_DURATION"));
                    }
                    if(cbTeleCall.isChecked())
                    {
                        tvTeleCallSlotDuration.setText(data.getStringExtra("SELECTED_DURATION"));
                    }
                    break;
            }
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String msg) {
        VariableConstant.IS_SHEDULE_EDITED = true;
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();

        AppController.getInstance().getMixpanelHelper().scheduleCreated(tvRepeatSchedule.getText().toString());

        closeActivity();
    }

    @Override
    public void onScheduleError() {
        Toast.makeText(this,getString(R.string.plsSelectMode),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDaysError() {
        Toast.makeText(this,getString(R.string.plsSelectDays),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDurationError() {
        Toast.makeText(this,getString(R.string.plsSelectDuration),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStartTimeError() {
        Toast.makeText(this,getString(R.string.plsSelectStartTime),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEndTimeError() {
        Toast.makeText(this,getString(R.string.plsSelectEndTime),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeSelectedError() {
        Toast.makeText(this,getString(R.string.theScheduleTimeError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onJobTypeError() {
        Toast.makeText(this,getString(R.string.plsSelectJobType),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSlotDurationError() {
        Toast.makeText(this,getString(R.string.plsSelectJobDurationForIncall),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationError() {
        Toast.makeText(this,getString(R.string.plsSelectLocation),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void selectedMode(String mode) {
        sentingDays.clear();
        tvRepeatDays.setText("");
        tvRepeatDays.setOnClickListener(this);
        tvRepeatSchedule.setText(mode);
    }

    @Override
    public void selectedModeConstantDays(String mode, String days, ArrayList<String> sentingDays) {
        tvRepeatSchedule.setText(mode);
        tvRepeatDays.setOnClickListener(null);
        tvRepeatDays.setText(days);
        this.sentingDays.clear();
        this.sentingDays.addAll(sentingDays);
    }

    @Override
    public void selectedDays(String days, ArrayList<String> sentingDays) {
        tvRepeatDays.setText(days);
        this.sentingDays.clear();
        this.sentingDays.addAll(sentingDays);
    }

    @Override
    public void selectedDuration(String duration) {
        tvDuration.setText(duration);
    }

    @Override
    public void seletedStartTime(String displayTime, String sentingTime) {
        tvStartTime.setText(displayTime);
        startTime = sentingTime;
    }

    @Override
    public void seletedEndTime(String displayTime, String sentingTime) {
        tvEndTime.setText(displayTime);
        endTime = sentingTime;
    }

}
