package com.sirvis.pro.main.profile.helpcenter;

import com.sirvis.pro.pojo.profile.helpcenter.TicketData;

import org.json.JSONObject;

/**
 * Created by murashid on 29-Dec-17.
 */

public class NewTicketPresenter  implements NewTicketModel.NewTicketModelImple {

    private NewTicketModel model;
    private NewTicketPresenterImple presenterImple;

    NewTicketPresenter(NewTicketPresenterImple presenterImple) {
        model = new NewTicketModel(this);
        this.presenterImple = presenterImple;
    }

    void createTicket(String sessionToken, JSONObject jsonObject) {
        presenterImple.startProgressBar();
        model.createTicket(sessionToken,jsonObject);
    }

    void sendComment(String sessionToken, JSONObject jsonObject) {
        presenterImple.startProgressBar();
        model.sendComment(sessionToken, jsonObject);
    }

    void getTicketDetails(String sessionToken, String ticketId) {
        presenterImple.startProgressBar();
        model.getTicketDetails(sessionToken, ticketId);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccessRaiseTicket(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessRaiseTicket(msg);
    }

    @Override
    public void onSuccessComment() {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessComment();
    }

    @Override
    public void onSuccessTicket(TicketData ticket) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessTicket(ticket);
    }

    @Override
    public void onErrorSubject() {
        presenterImple.stopProgressBar();
        presenterImple.onErrorSubject();
    }

    @Override
    public void onErrorPriority() {
        presenterImple.stopProgressBar();
        presenterImple.onErrorPriority();
    }

    @Override
    public void onErrorComment() {
        presenterImple.stopProgressBar();
        presenterImple.onErrorComment();
    }

    interface NewTicketPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccessRaiseTicket(String msg);
        void onSuccessComment();
        void onSuccessTicket(TicketData ticket);

        void onErrorSubject();
        void onErrorPriority();
        void onErrorComment();
    }
}
