package com.sirvis.pro.main.profile.wallet;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.wallet.CardData;
import com.sirvis.pro.pojo.profile.wallet.CardPojo;
import com.sirvis.pro.pojo.profile.wallet.WalletData;
import com.sirvis.pro.pojo.profile.wallet.WalletPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 28-Mar-18.
 */

public class WalletPresenter {
    private static final String TAG = "WalletPresenter";

    private View view;
    WalletPresenter(View view)
    {
        this.view = view;
    }
    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * </p>
     */
    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    void getWalletDetails(final String sessionToken)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.GET_WALLET_PAYMENT_SETTINGS, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        WalletPojo walletPojo = new Gson().fromJson(result, WalletPojo.class);
                                        view.onSuccessWalletDetails(walletPojo.getData());
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getWalletDetails(newToken);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    void rechargeWallet(final String sessionToken, final JSONObject jsonObjectRequest)
    {
        try {
            if(jsonObjectRequest.getString("cardId").equals(""))
            {
                view.onErrorCardSelection();
            }
            else
            {
                view.startProgressBar();
                OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.WALLET_RECHARGE, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObjectRequest
                        , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                            @Override
                            public void onSuccess(String statusCode,String result) {
                                view.stopProgressBar();
                                try {
                                    if (result != null) {
                                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                        final JSONObject jsonObject = new JSONObject(result);
                                        switch (statusCode) {
                                            case VariableConstant.RESPONSE_CODE_SUCCESS:
                                                view.onSuccessWalletRecharge(jsonObject.getString("message"));
                                                break;

                                            case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                                RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                                    @Override
                                                    public void onSuccessRefreshToken(String newToken) {
                                                        rechargeWallet(newToken,jsonObjectRequest);
                                                        view.onNewToken(newToken);
                                                    }

                                                    @Override
                                                    public void onFailureRefreshToken() {
                                                        view.onFailure();
                                                    }

                                                    @Override
                                                    public void sessionExpired(String msg) {
                                                        view.sessionExpired(msg);
                                                    }
                                                });
                                                break;

                                            case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                                view.sessionExpired(jsonObject.getString("message"));
                                                break;
                                            default:
                                                view.onFailure(jsonObject.getString("message"));
                                                break;
                                        }
                                    } else {
                                        view.onFailure();
                                    }
                                }
                                catch (Exception e)
                                {
                                    view.onFailure();
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(String error) {
                                view.stopProgressBar();
                                view.onFailure();
                            }
                        });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * </p>
     */
    /**
     * method for calling api for geting the Card details
     * @param sessionToken session Token
     */
    void getCardDetails(final String sessionToken)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CARD, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        CardPojo card_pojo = new Gson().fromJson(result, CardPojo.class);
                                        view.onSuccessCardDetails(card_pojo.getData());
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getCardDetails(newToken);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }



    public void validate(String selectedCardNumber, String amount) {
        if(selectedCardNumber.equals(""))
        {
            view.onErrorCardSelection();
        }
        else if(amount.equals(""))
        {
            view.onErrorAmount();
        }
        else
        {
            view.successValidation();
        }
    }

    interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccessWalletDetails(WalletData walletData);
        void onSuccessWalletRecharge(String msg);
        void onSuccessCardDetails(ArrayList<CardData> data);
        void onErrorCardSelection();
        void onErrorAmount();
        void successValidation();
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
