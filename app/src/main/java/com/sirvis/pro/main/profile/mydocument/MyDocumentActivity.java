package com.sirvis.pro.main.profile.mydocument;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.MyDocumentCategoryAdapter;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.document.ProfileDocumentCategory;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MyDocumentActivity extends AppCompatActivity implements MyDocumentPresenter.View {

    private static final String TAG = "MyDocumentActivity";
    private MyDocumentCategoryAdapter myDocumentCategoryAdapter;
    private ArrayList<ProfileDocumentCategory> profileDocumentCategories;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private MyDocumentPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.selectCategory));
        Typeface fontBold = Utility.getFontBold(this);
        tvTitle.setTypeface(fontBold);

        profileDocumentCategories = new ArrayList<>();
        RecyclerView selectCategory = findViewById(R.id.recycler_select_category);
        selectCategory.setLayoutManager(new LinearLayoutManager(this));
        myDocumentCategoryAdapter = new MyDocumentCategoryAdapter(this, profileDocumentCategories);
        selectCategory.setAdapter(myDocumentCategoryAdapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingCategory));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new MyDocumentPresenter(this);
        presenter.getCategory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(VariableConstant.IS_DOCUMENT_UPDATED)
        {
            VariableConstant.IS_DOCUMENT_UPDATED = false;
            presenter.getCategory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(ProfileData profileData) {
        ArrayList<ProfileDocumentCategory> profileDocumentCategories = profileData.getDocuments();

        this.profileDocumentCategories.clear();
        Collections.sort(profileDocumentCategories, new Comparator<ProfileDocumentCategory>() {
            @Override
            public int compare(ProfileDocumentCategory o1, ProfileDocumentCategory o2) {
                return o1.getCategoryName().compareTo(o2.getCategoryName());
            }
        });

        this.profileDocumentCategories.addAll(profileDocumentCategories);
        myDocumentCategoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }
}
