package com.sirvis.pro.main.booking;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.BidBookingListAdapter;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;


public class BookingListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "MyEventRequest";

    private static final String ARG_PARAM1 = "param1";
    private String type;
    private BidBookingListAdapter bookingListAdapter;
    private ArrayList<Booking> bookings;
    private TextView tvNoBooking;

    private SwipeRefreshLayout srlBooking;
    private RecyclerView rvBooking;
    private BookingListFragmentInteraction bookingListFragmentInteraction;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param type Parameter 1.
     * @return A new instance of fragment BookingListFragment.
     */
    public static BookingListFragment newInstance(String type) {

        BookingListFragment fragment = new BookingListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_booking_list, container, false);
        init(rootView);
        return rootView;
    }

    /**
     * init  the views
     * @param rootView   parent View
     */
    private void init(View rootView)
    {
        tvNoBooking = rootView.findViewById(R.id.tvNoBooking);
        tvNoBooking.setTypeface(Utility.getFontMedium(getActivity()));
        bookings = new ArrayList<>();
        bookingListAdapter = new BidBookingListAdapter(getActivity(),this.bookings, type);
        rvBooking = rootView.findViewById(R.id.rvBooking);
        rvBooking.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBooking.setAdapter(bookingListAdapter);
        bookingListAdapter.notifyDataSetChanged();

        srlBooking = rootView.findViewById(R.id.srlBooking);
        srlBooking.setOnRefreshListener(this);
    }

    void notifyiDataChanged(ArrayList<Booking> bookings)
    {
        if(bookings.size() == 0)
        {
            tvNoBooking.setVisibility(View.VISIBLE);
        }
        else
        {
            tvNoBooking.setVisibility(View.GONE);
        }

        srlBooking.setRefreshing(false);
        bookingListAdapter.clearTimer();
        this.bookings.clear();
        this.bookings.addAll(bookings);

        bookingListAdapter = new BidBookingListAdapter(getActivity(),this.bookings, type);
        rvBooking.setAdapter(bookingListAdapter);
        bookingListAdapter.notifyDataSetChanged();

    }

    void stopRefreshing()
    {
        try {
            srlBooking.setRefreshing(false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setBookingListFragmentInteraction(BookingListFragmentInteraction bookingListFragmentInteraction) {
        this.bookingListFragmentInteraction = bookingListFragmentInteraction;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRefresh() {
        if(bookingListFragmentInteraction != null)
            bookingListFragmentInteraction.onRefresh();
    }

    interface BookingListFragmentInteraction
    {
        void onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(bookingListAdapter !=null )
        {
            bookingListAdapter.clearHandlers();
            bookingListAdapter.clearTimer();
        }
    }
}
