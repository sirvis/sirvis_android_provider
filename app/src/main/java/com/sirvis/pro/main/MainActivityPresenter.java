package com.sirvis.pro.main;

import android.util.Log;

import com.sirvis.pro.pojo.appconfig.AppConfigData;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 25-Oct-17.
 * MainActivityPresenter presenter for MainActivity
 */

public class MainActivityPresenter implements MainActivityModel.MainActivityModelImple {

    private MainActivityModel model;
    private MainActivityPresenterImple presenterImple;

    MainActivityPresenter(MainActivityPresenterImple presenterImple)
    {
        model = new MainActivityModel(this);
        this.presenterImple = presenterImple;
    }

    void getAppConfig(String sessionToken)
    {
        model.getAppConfig(sessionToken);
    }
    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    void getBookingByDate(final String sessionToken, final String startDate, final String endDate, final boolean isBackground)
    {
       /* if(!isBackground)
        {
            view.startProgressBar();
        }*/
        Log.d("TAG", "getBookingByDate: "+startDate +" "+endDate);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKINGS +"/"+startDate+"/"+endDate, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {

                        try {
                            if (result != null) {
                                Log.d("TAG", "onSuccess: "+statusCode+"\n"+result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        presenterImple.onSuccesBooking(result,true);
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getBookingByDate(newToken, startDate, endDate,isBackground);
                                                presenterImple.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                presenterImple.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                presenterImple.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        presenterImple.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        presenterImple.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                presenterImple.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            presenterImple.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {

                        presenterImple.onFailure();
                    }
                });
    }


    @Override
    public void onSuccess(AppConfigData appConfigData) {
        presenterImple.onSuccess(appConfigData);
    }

    interface  MainActivityPresenterImple
    {
        void onSuccess(AppConfigData appConfigData);

        void onFailure();

        void sessionExpired(String msg);

        void onNewToken(String newToken);

        void onFailure(String message);

        void onSuccesBooking(String result, boolean b);
    }

}
