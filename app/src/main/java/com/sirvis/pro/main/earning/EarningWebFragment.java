package com.sirvis.pro.main.earning;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.R;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.SessionManager;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>EarningWebFragment</h1>
 * EarningWebFragment for showing Earning Details
 */

public class EarningWebFragment extends Fragment {

    public final String TAG = "EarningWebFragment";
    private ProgressBar pgEarning;
    private WebView webViewEarning;

    private ArrayList<String> urls;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_earning_web, container, false);

        init(rootView);
        return rootView;
    }

    public static EarningWebFragment newInstance() {
        return new EarningWebFragment();
    }

    /**
     * init  the views
     * @param rootView   parent View
     */
    private void init(View rootView)
    {
        SessionManager sessionManager = SessionManager.getSessionManager(getActivity());
        urls = new ArrayList<>();
        TextView tvTitle = rootView.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.earnings));

        pgEarning = rootView.findViewById(R.id.pgEarning);

        webViewEarning = rootView.findViewById(R.id.webViewEarning);
        webViewEarning.setHorizontalScrollBarEnabled(false);
        webViewEarning.getSettings().setJavaScriptEnabled(true);
        webViewEarning.getSettings().setUseWideViewPort(false);
        webViewEarning.getSettings().setDomStorageEnabled(true);

        webViewEarning.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                Log.d(TAG, "shouldOverrideUrlLoading: "+url);
                if(!urls.contains(url))
                {
                    urls.add(url);
                }
                return true;
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pgEarning.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                pgEarning.setVisibility(View.GONE);
            }
        });

        Log.d(TAG, "init: "+ BuildConfig.EARNING_URL+sessionManager.getProviderId());


        webViewEarning.loadUrl(BuildConfig.EARNING_URL+sessionManager.getProviderId());
        urls.add(BuildConfig.EARNING_URL+sessionManager.getProviderId());

        ImageView ivBackButton = rootView.findViewById(R.id.ivBackButton);

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                canGoBack();
            }
        });
    }

    public boolean canGoBack()
    {
        if(urls.size() == 1)
        {
            return false;
        }
        else
        {
            webViewEarning.loadUrl(urls.get(urls.size()-2));
            urls.remove(urls.size()-1);
            return true;
        }
    }

}
