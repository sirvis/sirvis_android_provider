package com.sirvis.pro.main.profile.myratecard;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.ratecard.CategoryData;
import com.sirvis.pro.pojo.profile.ratecard.CategoryPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 13-Feb-18.
 */

public class MyRateCardPresenter {

    private static final String TAG = "SelectCategory";
    private View view;

    MyRateCardPresenter(View view)
    {
        this.view = view;
    }

    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    void getCategory(final String sessionToken)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.GET_CATEGORY, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        CategoryPojo categoryPojo = gson.fromJson(result, CategoryPojo.class);
                                        view.onSuccess(categoryPojo.getData());
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getCategory(newToken);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ArrayList<CategoryData> categoryData);
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
