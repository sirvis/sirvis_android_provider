package com.sirvis.pro.main.profile.bank;

import org.json.JSONObject;

/**
 * Created by murashid on 29-Aug-17.
 * <h1>BankBottomSheetPresenter</h1>
 * BankBottomSheetPresenter presenter for BankBottomSheetfragment
 * @see BankBottomSheetFragment
 */

public class BankBottomSheetPresenter implements BankBottomSheetModel.BankBottomSheetModelImplem {

    private BankBottomSheetPresenterImplem bankBottomSheetPresenterImplem;
    private BankBottomSheetModel bankBottomSheetModel;
    BankBottomSheetPresenter(BankBottomSheetPresenterImplem bankBottomSheetPresenterImplem)
    {
        this.bankBottomSheetPresenterImplem = bankBottomSheetPresenterImplem;
        this.bankBottomSheetModel = new BankBottomSheetModel(this);
    }

    /**
     * method for passing values from view to model
     * @param token session token
     * @param jsonObject required fields in json object
     */
    void makeDefault(String token, JSONObject jsonObject)
    {
        bankBottomSheetPresenterImplem.startProgressBar();
        bankBottomSheetModel.setDefaultAccount(token,jsonObject);
    }

    /**
     * method for passing values from view to model
     * @param token session token
     * @param jsonObject required fields in json object
     */
    void deleteAccount(String token, JSONObject jsonObject)
    {
        bankBottomSheetPresenterImplem.startProgressBar();
        bankBottomSheetModel.deleteBankAccount(token,jsonObject);
    }


    @Override
    public void onFailure(String failureMsg) {
        bankBottomSheetPresenterImplem.stopProgressBar();
        bankBottomSheetPresenterImplem.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        bankBottomSheetPresenterImplem.stopProgressBar();
        bankBottomSheetPresenterImplem.onFailure();
    }

    @Override
    public void onSuccess(String msg) {
        bankBottomSheetPresenterImplem.stopProgressBar();
        bankBottomSheetPresenterImplem.onSuccess(msg);
    }

    /**
     * BankBottomSheetPresenterImplem interface for View implementation
     */
    interface BankBottomSheetPresenterImplem
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(String msg);
    }
}
