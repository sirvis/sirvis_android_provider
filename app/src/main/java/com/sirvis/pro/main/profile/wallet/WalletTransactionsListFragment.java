package com.sirvis.pro.main.profile.wallet;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.sirvis.pro.R;
import com.sirvis.pro.adapters.WalletTransactionsAdapter;
import com.sirvis.pro.pojo.profile.wallet.WalletTransDetails;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * <h1>WalletTransactionsListFragment</h1>
 * <p>
 *     Fragment to show wallet all transactions list
 *     according to fragment instances for the view pager of WalletTransActivity
 * </p>
 * A simple {@link Fragment} subclass.
 */
public class WalletTransactionsListFragment extends Fragment
{
    private ProgressBar progressBar;
    private TextView tvNoTransaction;

    private ArrayList<WalletTransDetails> transactionsAL;
    private WalletTransactionsAdapter walletTransactionsAdapter;

    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean toLoad = true;
    private OnFragmentInteractionListener mListener;
    /**
     * <h2>getNewInstance</h2>
     * <p>
     *     method to return the instance of this fragment
     * </p>
     * @return WalletTransactionsListFragment: Instance of the fragment
     */
    public static WalletTransactionsListFragment getNewInstance()
    {
        return new WalletTransactionsListFragment();
    }
    //====================================================================


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wallet_tansactions_list, container, false);

        init(view);
        return view;
    }
    //====================================================================


    /**
     * <h2>initViews</h2>
     * <p>
     *     method to notify adapter or update views if the
     *     transactions list size changed
     * </p>
     * @param view: reference of root view of this fragment
     */
    private void init(View view)
    {
        transactionsAL = new ArrayList<>();

        tvNoTransaction =  view.findViewById(R.id.tvNoTransaction);
        progressBar =  view.findViewById(R.id.progressBar);
        tvNoTransaction.setTypeface(Utility.getFontRegular(getActivity()));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView rvTransactions = (RecyclerView) view.findViewById(R.id.rvTransactions);
        rvTransactions.setLayoutManager( linearLayoutManager);
        walletTransactionsAdapter = new WalletTransactionsAdapter(getActivity(), transactionsAL);
        rvTransactions.setAdapter(walletTransactionsAdapter);

        rvTransactions.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (toLoad)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            toLoad = false;
                            if (mListener != null) {
                                mListener.onReachedBottom();
                            }
                        }
                    }
                }
            }
        });

    }
    //====================================================================


    public void showProgress()
    {
        if(progressBar!=null)
        {
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    public void hideProgress()
    {
        toLoad = true;
        if(progressBar !=null )
        {
            progressBar.setVisibility(View.GONE);
        }
    }
    /**
     * <h2>notifyDataSetChangedCustom</h2>
     * <p>
     *     method to notify adapter or update views if the
     *     transactions list size changed
     * </p>
     */
    public void notifyDataSetChanged(ArrayList<WalletTransDetails> _transactionsAL)
    {
        transactionsAL.addAll(_transactionsAL);
        updateView();
    }
    //====================================================================

    /**
     * <h2>updateView</h2>
     * <p>
     *     method to show or hide the list and notItems views
     *     according to the size of the list
     * </p>
     */
    private void updateView()
    {
        if(transactionsAL == null)
        {
            return;
        }
        if(transactionsAL.size() > 0)
        {
            tvNoTransaction.setVisibility(View.GONE);
        }
        else
        {
            tvNoTransaction.setVisibility(View.VISIBLE);
        }
        walletTransactionsAdapter.notifyDataSetChanged();
    }
    //====================================================================

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onReachedBottom();
    }

}
