package com.sirvis.pro.main.schedule;

import android.graphics.Color;

import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.gson.Gson;
import com.sirvis.pro.pojo.shedule.Booked;
import com.sirvis.pro.pojo.shedule.Schedule;
import com.sirvis.pro.pojo.shedule.ScheduleMonthData;
import com.sirvis.pro.pojo.shedule.ScheduleMonthPojo;
import com.sirvis.pro.pojo.shedule.Slot;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by murashid on 23-Oct-17.
 * <h1>ScheduleFragmentPresenter</h1>
 * ScheduleFragmentPresenter presenter for ScheduleFragment
 * @see ScheduleFragment
 */

public class ScheduleFragmentPresenter implements ScheduleFramentModel.ScheduleModelImple {

    private ScheduleFramentModel model;
    private SchedulePresenterImple presenterImple;
    private SimpleDateFormat serverFormat,displayHourFormat, displayHourFormatInBooked, displayPeriodFormat;
    private SessionManager sessionManager;
    private Gson gson;
    private boolean isFragmentAttached = false, isCurrentMonth = false;
    private String scheduleData="";

    ScheduleFragmentPresenter(SessionManager sessionManager, SchedulePresenterImple presenterImple) {
        model = new ScheduleFramentModel(this);
        this.presenterImple = presenterImple;
        this.sessionManager = sessionManager;
        gson = new Gson();
        isFragmentAttached = true;

        displayHourFormat = new SimpleDateFormat("h:mm", Locale.US);
        displayHourFormatInBooked = new SimpleDateFormat("hh:mm a", Locale.US);
        displayPeriodFormat = new SimpleDateFormat("a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);

    }

    void detach()
    {
        isFragmentAttached = false;
    }
    /**
     * methof for passing value from view to model
     * @param sessionToken session token
     * @param date date of selected month
     */
    void getSchedule(String sessionToken, String date, boolean isCurrentMonth) {
        this.isCurrentMonth = isCurrentMonth;
        if(!isCurrentMonth || sessionManager.getScheduleData().equals(""))
        {
            presenterImple.startProgressBar();
        }
        else
        {
            onSuccessGetSchedule(sessionManager.getScheduleData());
        }
        model.getShedule(sessionToken,date);
    }

    @Override
    public void onFailure(String failureMsg) {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
            presenterImple.onFailure(failureMsg);
        }
    }

    @Override
    public void onFailure() {
        if(isFragmentAttached) {
            presenterImple.onFailure();
            presenterImple.stopProgressBar();
        }
    }

    @Override
    public void onSuccessGetSchedule(String result) {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
        }
        if(scheduleData.equals(result))
        {
            return;
        }
        scheduleData = result;
        if(isCurrentMonth)
        {
            sessionManager.setScheduleData(result);
        }
        ScheduleMonthPojo scheduleMonthPojo = gson.fromJson(result,ScheduleMonthPojo.class);
        if(isFragmentAttached)
        {
            presenterImple.onSuccessGetSchedule(scheduleMonthPojo);
        }
    }

    @Override
    public void onNewToken(String token) {
        presenterImple.onNewToken(token);
    }

    @Override
    public void sessionExpired(String msg) {
        if(isFragmentAttached)
        {
            presenterImple.stopProgressBar();
            presenterImple.sessionExpired(msg);
        }
    }

    /**
     * Returing the created events based on the sheduleMontDatas
     * create blue color for schedule day and differrent color for booked color
     * @param scheduleMonthDatas list of scheduleMonthDatas
     * @param calendarEvents List of calendar events
     * @return calendarEvents
     */
    ArrayList<Event> getEvents(ArrayList<ScheduleMonthData> scheduleMonthDatas,ArrayList<Event> calendarEvents)
    {
        for (ScheduleMonthData scheduleMonthData : scheduleMonthDatas)
        {
            for (Schedule schedule : scheduleMonthData.getSchedule())
            {
                Event event;
                if(schedule.getBooked().size() == 0)
                {
                    //color primary for created schedule
                    event = new Event(Color.parseColor("#01B5F6"), Utility.convertUTCToTimeStamp(scheduleMonthData.getDate()),schedule);
                }
                else
                {
                    if(!schedule.getBooked().get(schedule.getBooked().size()-1).getStatus().equals(VariableConstant.JOB_COMPLETED_RAISE_INVOICE))
                    {
                        //color green for upcoming and ongoing shedule booking,
                        event = new Event(Color.parseColor("#5BC24F"), Utility.convertUTCToTimeStamp(scheduleMonthData.getDate()),schedule);
                    }
                    else
                    {
                        //color red for completed booking
                        event = new Event(Color.parseColor("#EB4942"), Utility.convertUTCToTimeStamp(scheduleMonthData.getDate()),schedule);
                    }
                }
                calendarEvents.add(event);
            }
        }
        return calendarEvents;
    }
    /**
     * Create slot for Each based on booked and arrange according to the time
     * @param eventList calendar event list
     * @return list of Slot
     */
    ArrayList<Slot> createSlotFromSchedules(List<Event> eventList)
    {
        ArrayList<Slot> slots = new ArrayList<>();
        ArrayList<Schedule> schedules = new ArrayList<>();
        for(Event event : eventList)
        {
            schedules.add((Schedule) event.getData());
        }

        try {
            for(Schedule schedule : schedules)
            {
                Slot slotStartTime = new Slot();
                slotStartTime.setSlotHour(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getStartTime()))));
                slotStartTime.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getStartTime()))));
                slotStartTime.setStartTimeStamp(schedule.getStartTime());
                slots.add(slotStartTime);
                if(schedule.getBooked().size() > 0)
                {
                    for(Booked booked : schedule.getBooked())
                    {
                        Slot bookedSlot = new Slot();
                        bookedSlot.setStatus(booked.getStatus());
                        bookedSlot.setBookingId(booked.getBookingId());
                        bookedSlot.setCustomerId(booked.getCustomerId());
                        bookedSlot.setSlotHour(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setSlotEndHourBooking(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setSlotEndPeriodBooking(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setBookedStartHour(displayHourFormatInBooked.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getStart()))));
                        bookedSlot.setBookedEndHour(displayHourFormatInBooked.format(serverFormat.parse(Utility.convertUTCToServerFormat(booked.getEnd()))));
                        bookedSlot.setEvent(booked.getEvent());
                        bookedSlot.setCutomerName(booked.getFirstName() +" "+ booked.getLastName());
                        bookedSlot.setStartTimeStamp(booked.getStart());
                        slots.add(bookedSlot);
                    }
                }
                Slot slotEndTime = new Slot();
                slotEndTime.setSlotHour(displayHourFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getEndTime()))));
                slotEndTime.setSlotPeriod(displayPeriodFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(schedule.getEndTime()))));
                slotEndTime.setStartTimeStamp(schedule.getEndTime());
                slots.add(slotEndTime);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


        Collections.sort(slots, new Comparator<Slot>() {
            @Override
            public int compare(Slot o1, Slot o2) {
                return o1.getStartTimeStamp().compareTo(o2.getStartTimeStamp());
            }
        });

        //Remove repeated Slot time (i.e) if 5 to 6 and 6 to 8 then show only 5 6 8 no need to show 6 again
       /*

       ArrayList<Integer> removablePosition = new ArrayList<>();
        int j = 0;
        for( int i=0 ; i < slots.size()-1 ; i++)
        {
            if(slots.get(i).getSlotHour().equals(slots.get(i+1).getSlotHour()))
            {
                removablePosition.add(i);
            }
        }

        for(int i = 0 ; i < removablePosition.size() ; i++)
        {
            slots.remove((int)removablePosition.get(i) - j);
            j++;
        }*/

        return slots;
    }

    /**
     * SchedulePresenterImple interface for View implementatin
     */
    interface SchedulePresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo);
        void onNewToken(String token);
        void sessionExpired(String msg);
    }
}
