package com.sirvis.pro.main.profile.profileeedit;

import com.sirvis.pro.pojo.phonevalidation.PhoneValidationPojo;

/**
 * Created by murashid on 11-Oct-17.
 * EditEmailPhonePresenter presenter for EditEmailPhoneActivity
 * @see EditEmailPhoneActivity
 */

public class EditEmailPhonePresenter implements EditEmailPhoneModel.EditEmailPhoneModelImple {
    private EditEmailPhoneModel model;
    private EditEmailPhonePresenterImple presenterImple;

    EditEmailPhonePresenter(EditEmailPhonePresenterImple presenterImple) {
        model = new EditEmailPhoneModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session Token
     * @param isPhone true for phone and false for email
     * @param countyCode country code
     * @param value value email or phone
     */
    void updateValue(String sessionToken, boolean isPhone, String countyCode,String value) {
        presenterImple.startProgressBar();
        model.updateValue(sessionToken,isPhone,countyCode,value);
    }


    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onErrorPhone() {
        presenterImple.stopProgressBar();
        presenterImple.onErrorPhone();
    }

    @Override
    public void onErrorEmail() {
        presenterImple.stopProgressBar();
        presenterImple.onErrorEmail();
    }

    @Override
    public void onSuccessPhone(PhoneValidationPojo phoneValidationPojo) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessPhone(phoneValidationPojo);
    }

    @Override
    public void onSuccessEmail(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessEmail(msg);
    }

    interface EditEmailPhonePresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onErrorPhone();
        void onErrorEmail();
        void onSuccessPhone(PhoneValidationPojo phoneValidationPojo);
        void onSuccessEmail(String msg);
        void onFailure(String msg);
        void onFailure();
    }
}
