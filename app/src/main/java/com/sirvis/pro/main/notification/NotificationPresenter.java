package com.sirvis.pro.main.notification;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.notification.NotificationData;
import com.sirvis.pro.pojo.notification.NotificationPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 09-Apr-18.
 */

public class NotificationPresenter {
    private static final String TAG = "ChatCutomer";

    private View view;
    private Gson gson;
    private boolean isFragmentAttached = false;

    NotificationPresenter(View view)
    {
        this.view = view;
        gson = new Gson();
        isFragmentAttached = true;
    }

    void detach()
    {
        isFragmentAttached = false;
    }

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * </p>
     */
    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    void getNotificationList(final String sessionToken, final int skip, final int limit)
    {
        view.startProgressBar();
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.NOTIFICATION+"/"+skip+"/"+limit, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        if(!isFragmentAttached)
                        {
                            return;
                        }
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                NotificationPojo notificationPojo = gson.fromJson(result,NotificationPojo.class);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccess(notificationPojo.getData());
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(new JSONObject(result).getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getNotificationList(newToken, skip, limit);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(new JSONObject(result).getString("message"));
                                        break;

                                    default:
                                        view.onFailure(new JSONObject(result).getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        if(!isFragmentAttached)
                        {
                            return;
                        }
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ArrayList<NotificationData> notificationData);
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
