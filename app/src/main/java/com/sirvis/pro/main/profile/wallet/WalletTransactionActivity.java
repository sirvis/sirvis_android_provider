package com.sirvis.pro.main.profile.wallet;


import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ViewPagerAdapter;
import com.sirvis.pro.pojo.profile.wallet.WalletTransData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;


public class WalletTransactionActivity extends AppCompatActivity implements WalletTransactionPresenter.View, WalletTransactionsListFragment.OnFragmentInteractionListener {

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private WalletTransactionPresenter presenter;
    private int index = 0;

    private WalletTransactionsListFragment walletTransAllFrag, walletTransDebitFrag, walletTransCreditFrag,paymentTransactionFrag ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transactions);

        init();
    }
    //====================================================================


    /* <h2>init</h2>
       * <p>
       *     method to initialize customer toolbar
       * </p>
       */
    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        presenter = new WalletTransactionPresenter(this);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null)
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.recentTransactions));
        tvTitle.setTypeface(fontBold);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tablayoutWalletTransaction = findViewById(R.id.tablayoutWalletTransaction);
        ViewPager vpWalletTransaction = findViewById(R.id.vpWalletTransaction);
        vpWalletTransaction.setOffscreenPageLimit(3);

        walletTransAllFrag = WalletTransactionsListFragment.getNewInstance();
        walletTransDebitFrag = WalletTransactionsListFragment.getNewInstance();
        walletTransCreditFrag = WalletTransactionsListFragment.getNewInstance();
        paymentTransactionFrag = WalletTransactionsListFragment.getNewInstance();
        viewPagerAdapter.addFragment(walletTransAllFrag, getString(R.string.all).toUpperCase());
        viewPagerAdapter.addFragment(walletTransDebitFrag, getString(R.string.debit));
        viewPagerAdapter.addFragment(walletTransCreditFrag, getString(R.string.credit));
        viewPagerAdapter.addFragment(paymentTransactionFrag, getString(R.string.paymentTrans));
        vpWalletTransaction.setAdapter(viewPagerAdapter);
        tablayoutWalletTransaction.setupWithViewPager(vpWalletTransaction);

        presenter.getTransaction(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),index);

    }


    @Override
    public void onReachedBottom() {
        index ++;
        presenter.getTransaction(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),index);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        walletTransAllFrag.showProgress();
        paymentTransactionFrag.showProgress();
        walletTransCreditFrag.showProgress();
        walletTransDebitFrag.showProgress();
    }

    @Override
    public void stopProgressBar() {
        walletTransAllFrag.hideProgress();
        paymentTransactionFrag.hideProgress();
        walletTransCreditFrag.hideProgress();
        walletTransDebitFrag.hideProgress();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(WalletTransData walletTransData) {
        paymentTransactionFrag.notifyDataSetChanged(walletTransData.getPaymentArr());
        walletTransAllFrag.notifyDataSetChanged(walletTransData.getCreditDebitArr());
        walletTransDebitFrag.notifyDataSetChanged(walletTransData.getDebitArr());
        walletTransCreditFrag.notifyDataSetChanged(walletTransData.getCreditArr());

    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }


}
