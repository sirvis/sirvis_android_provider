package com.sirvis.pro.main.profile.support;


import com.sirvis.pro.pojo.profile.support.SupportData;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportPresenter</h1>
 * SupportPresenter presenter for SupportActivity
 * @see SupportActivity
 */

public class SupportPresenter implements SupportModel.SupportModelImplement {

    private SupportModel supportModel;
    private SupportPresenterImplement presenterImplement;

    SupportPresenter(SupportPresenterImplement presenterImplement) {
        supportModel = new SupportModel(this);
        this.presenterImplement = presenterImplement;
    }

    /**
     * method for calinng method in model for fetcing support list
     */
    void getSupport() {
        presenterImplement.startProgressBar();
        supportModel.fetchData();
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImplement.stopProgressBar();
        presenterImplement.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImplement.stopProgressBar();
        presenterImplement.onFailure();
    }

    @Override
    public void onSuccess(ArrayList<SupportData> supportDatas) {
        presenterImplement.stopProgressBar();
        presenterImplement.getSupportDetails(supportDatas);
    }

    interface SupportPresenterImplement {
        void startProgressBar();

        void stopProgressBar();

        void onFailure(String msg);

        void onFailure();

        void getSupportDetails(ArrayList<SupportData> supportDatas);
    }
}

