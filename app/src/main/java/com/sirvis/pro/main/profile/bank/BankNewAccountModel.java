package com.sirvis.pro.main.profile.bank;

import android.util.Log;
import android.util.Patterns;


import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 26-Aug-17.
 * <h1>BankNewAccountModel</h1>
 * BankNewAccountModel model for BankNewAccountActivity
 * @see BankNewAccountActivity
 */

public class BankNewAccountModel {
    private static final String TAG = "BankNewStripeModel";
    private BankNewAccountModelImplement bankNewAccountModelImplement;

    BankNewAccountModel(BankNewAccountModelImplement bankNewAccountModelImplement)
    {
        this.bankNewAccountModelImplement = bankNewAccountModelImplement;
    }

    /**
     * method for calling api for adding bank account and check the field is empty or not
     * @param token session Token
     * @param jsonObject required field in json objec
     */
    void addBankAccount(String token,JSONObject jsonObject)
    {
        try {
            if(jsonObject.getString("account_holder_name").equals(""))
            {
                bankNewAccountModelImplement.onNameError();
                return;
            }
            else if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches()) {
                bankNewAccountModelImplement.onErrorEmail();
                return;
            }
            else if(jsonObject.getString("account_number").equals(""))
            {
                bankNewAccountModelImplement.onAccountNumberError();
                return;
            }
            else if(jsonObject.getString("routing_number").equals(""))
            {
                bankNewAccountModelImplement.onRoutingNumberError();
                return;
            }
             else if(jsonObject.getString("country").equals(""))
            {
                bankNewAccountModelImplement.onCountryError();
                return;
            }



            bankNewAccountModelImplement.onNoError();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_EXTERNAL_ACCOUNT, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result!=null)
                    {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject  = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            bankNewAccountModelImplement.onSuccess(jsonObject.getString("message"));
                        }
                        else
                        {
                            bankNewAccountModelImplement.onFailure(jsonObject.getString("message"));
                        }
                    }
                    else
                    {
                        bankNewAccountModelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    bankNewAccountModelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error)
            {
                bankNewAccountModelImplement.onFailure();
            }
        });
    }

    /**
     * BankNewAccountModelImplement interface for Presenter implentation
     */
    interface  BankNewAccountModelImplement
    {
        void onFailure();
        void onFailure(String failureMsg);
        void onSuccess(String failureMsg);

        void onNameError();
        void onErrorEmail();
        void onAccountNumberError();
        void onRoutingNumberError();
        void onCountryError();
        void onNoError();
    }

}
