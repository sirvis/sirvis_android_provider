package com.sirvis.pro.main.profile.helpcenter;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.helpcenter.HelpCenterData;
import com.sirvis.pro.pojo.profile.helpcenter.HelpCenterPojo;
import com.sirvis.pro.pojo.profile.helpcenter.Ticket;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 10-Sep-17.
 */

public class HelpCenterModel {
    private static final String TAG = "LoginModel";
    private HelpCenterModelImple modelImplement;

    HelpCenterModel(HelpCenterModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }
    void getAllTickets(String sessionToken,String email)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.ZENDESK_GET_ALL_TICKET+"/"+email , OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccessRaiseTicket: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            Gson gson = new Gson();
                            HelpCenterPojo helpCenterPojo = gson.fromJson(result,HelpCenterPojo.class);
                            HelpCenterData data = helpCenterPojo.getData();

                            ArrayList<Ticket> wholeTicketData = new ArrayList<>();

                            for (int i= 0 ; i < data.getOpen().size() ; i++)
                            {
                                if(i==0)
                                {
                                    data.getOpen().get(i).setHeader(true);
                                    data.getOpen().get(i).setHeadername("open");
                                }
                                wholeTicketData.add(data.getOpen().get(i));
                            }
                            for (int i= 0 ; i < data.getClose().size() ; i++)
                            {
                                if(i==0)
                                {
                                    data.getClose().get(i).setHeader(true);
                                    data.getClose().get(i).setHeadername("close");
                                }
                                wholeTicketData.add(data.getClose().get(i));
                            }

                            modelImplement.onSuccess(wholeTicketData);

                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    interface HelpCenterModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ArrayList<Ticket> wholeTicketData);
    }
}
