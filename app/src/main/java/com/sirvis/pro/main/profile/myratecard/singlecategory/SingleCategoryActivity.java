package com.sirvis.pro.main.profile.myratecard.singlecategory;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ViewPagerAdapter;
import com.sirvis.pro.pojo.profile.ratecard.CategoryData;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

public class SingleCategoryActivity extends AppCompatActivity {

    private HourlyFragment hourlyFragment;
    private FixedFragment fixedFragment;
    private CategoryData categoryData;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_category);
        initializer();
    }

    private void initializer() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvPriceType = findViewById(R.id.tvPriceType);
        tvTitle.setTypeface(Utility.getFontBold(this));
        tvPriceType.setTypeface(Utility.getFontMedium(this));
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tablayoutMyEvents = findViewById(R.id.tablayoutMyEvents);
        ViewPager viewPagerCategory = findViewById(R.id.viewPagerCategory);

        Intent intent = getIntent();
        categoryData = (CategoryData) intent.getSerializableExtra("category");
        position = intent.getIntExtra("position",0);

        tvTitle.setText(categoryData.getCat_name());

        switch (categoryData.getBilling_model())
        {
            case VariableConstant.CATEGORY_FIXED_NON_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByAdmin));
                fixedFragment = FixedFragment.newInstance(categoryData,false);
                break;

            case VariableConstant.CATEGORY_FIXED_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByProvider));
                fixedFragment = FixedFragment.newInstance(categoryData,true);
                break;

            case VariableConstant.CATEGORY_HOURLY_NON_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByAdmin));
                hourlyFragment = HourlyFragment.newInstance(categoryData,false);
                break;

            case VariableConstant.CATEGORY_HOURLY_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByProvider));
                hourlyFragment = HourlyFragment.newInstance(categoryData,true);
                break;

            case VariableConstant.CATEGORY_FIXED_HOURLY_NON_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByAdmin));
                hourlyFragment = HourlyFragment.newInstance(categoryData,false);
                fixedFragment = FixedFragment.newInstance(categoryData,false);
                break;

            case VariableConstant.CATEGORY_FIXED_HOURLY_EDIT:
                tvPriceType.setText(getString(R.string.priceSetByProvider));
                hourlyFragment = HourlyFragment.newInstance(categoryData,true);
                fixedFragment = FixedFragment.newInstance(categoryData,true);
                break;
        }

        if(hourlyFragment !=null)
        {
            viewPagerAdapter.addFragment(hourlyFragment,getString(R.string.hourly));
        }
        if(fixedFragment != null)
        {
            viewPagerAdapter.addFragment(fixedFragment,getString(R.string.fixed));
        }

        viewPagerCategory.setAdapter(viewPagerAdapter);
        tablayoutMyEvents.setupWithViewPager(viewPagerCategory);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        boolean isUpdated = false;
        if(fixedFragment !=null &&fixedFragment.isUpdated)
        {
            isUpdated = true;
            categoryData.setService(fixedFragment.categoryServices);
            categoryData.setSubCatArr(fixedFragment.subCategoryRateCards);
        }
        if(hourlyFragment !=null && hourlyFragment.isUpdated)
        {
            isUpdated = true;
            categoryData.setPrice_per_fees(hourlyFragment.categoryData.getPrice_per_fees());
        }

        if(isUpdated)
        {
            Intent intent = new Intent();
            intent.putExtra("category",categoryData);
            intent.putExtra("position",position);
            setResult(RESULT_OK,intent);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
