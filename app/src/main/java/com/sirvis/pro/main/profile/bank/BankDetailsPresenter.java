package com.sirvis.pro.main.profile.bank;




import com.sirvis.pro.pojo.profile.bank.BankList;
import com.sirvis.pro.pojo.profile.bank.LegalEntity;
import com.sirvis.pro.pojo.profile.bank.StripeDetailsPojo;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 *<h1>BankDetailsPresenter</h1>
 * BankDetailsPresenter presenter for BankDetailsActivity
 * @see BankDetailsActivity
 */

public class BankDetailsPresenter implements BankDetailsModel.BankDetailsModelImplement {

    private BankDetailsPresenterImplement bankListFragPresenterImplement;
    private BankDetailsModel bankListFragModel;

    BankDetailsPresenter(BankDetailsPresenterImplement bankListFragPresenterImplement) {
        bankListFragModel = new BankDetailsModel(this);
        this.bankListFragPresenterImplement = bankListFragPresenterImplement;
    }

    /**
     * method for passing values from view to model
     * @param token session Token
     */
    void getBankDetails(String token) {
        bankListFragPresenterImplement.startProgressBar();
        bankListFragModel.fetchData(token);
    }

    @Override
    public void onFailure(String failureMsg) {
        bankListFragPresenterImplement.stopProgressBar();
        bankListFragPresenterImplement.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        bankListFragPresenterImplement.stopProgressBar();
        bankListFragPresenterImplement.onFailure();
    }

    @Override
    public void onSuccess(StripeDetailsPojo.Accounts accounts) {
        bankListFragPresenterImplement.stopProgressBar();
        bankListFragPresenterImplement.onSuccess(accounts);
    }

    @Override
    public void noStipeAccount(String msg) {
        bankListFragPresenterImplement.stopProgressBar();
        bankListFragPresenterImplement.showAddStipe(msg);
    }

    /**
     * BankDetailsPresenterImplement interface for View implementation
     */
    interface BankDetailsPresenterImplement {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(StripeDetailsPojo.Accounts accounts);
        void showAddStipe(String msg);
    }
}
