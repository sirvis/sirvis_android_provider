package com.sirvis.pro.main.history;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.JobImageListAdapter;
import com.sirvis.pro.main.schedule.bookingschedule.BookingSchedulePresenter;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.pojo.booking.ImgUrls;
import com.sirvis.pro.pojo.booking.QuestionAnswer;
import com.sirvis.pro.pojo.booking.ServiceItem;
import com.sirvis.pro.pojo.history.Accounting;
import com.sirvis.pro.pojo.history.AdditionalService;
import com.sirvis.pro.pojo.history.CustomerData;
import com.sirvis.pro.pojo.history.ReviewByProvider;
import com.sirvis.pro.pojo.shedule.ScheduleBookngPojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class HistoryInvoiceActivity extends AppCompatActivity implements BookingSchedulePresenter.BookingSchedulePresenterImple {

    private SessionManager sessionManager;
    private BookingSchedulePresenter presenter;
    private ProgressDialog progressDialog;

    private TextView tveventId, tvDate, tvPrice, tvStatus, tvCustomerName, tvYouRated;
    private TextView tvJobLocation;
    private TextView tvDiscount, tvCancelFee, tvTravelFee, tvVisitFee, tvLastDue;
    private TextView tvTotal, tvPaymentMethod, tvPaymentCardCash, tvPaymentWallet, tvEarnedAmount, tvAppComssion, tvCancelReason;
    private TextView tvpgComission;
    private RelativeLayout rlWalletPaymentDetails;

    private ImageView ivCustomer, ivSignature;
    private LinearLayout llRating, llAmount, llSignature, llCancel;
    private RelativeLayout rlCancelFee, rlPgCommisionn;
    private LinearLayout llService;

    private boolean isFromSchedule;
    private Booking booking;
    private ReviewByProvider reviewByProvider;
    private Accounting accounting;

    private Typeface fontRegular, fontMedium;
    private LayoutInflater inflater;
    private TextView tvJobLocationLabel;
    private TextView tvVatFee;
    private RelativeLayout rlVatFee;
    private double total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_invoice);
        init();
    }

    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new BookingSchedulePresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingDetails));

        inflater = getLayoutInflater();

        Typeface fontBold = Utility.getFontBold(this);
        fontMedium = Utility.getFontMedium(this);
        fontRegular = Utility.getFontRegular(this);

        tveventId = findViewById(R.id.tveventId);
        tvDate = findViewById(R.id.tvDate);
        tvStatus = findViewById(R.id.tvStatus);
        tvPrice = findViewById(R.id.tvPrice);
        TextView tvamountPaidLabel = findViewById(R.id.tvamountPaidLabel);
        tvCustomerName = findViewById(R.id.tvCustomerName);
        TextView tvYouRatedLabel = findViewById(R.id.tvYouRatedLabel);
        tvYouRated = findViewById(R.id.tvYouRated);
        tvJobLocationLabel = findViewById(R.id.tvJobLocationLabel);
        tvJobLocation = findViewById(R.id.tvJobLocation);
        TextView tvPaymentBreakDownLabel = findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvDiscountLabel = findViewById(R.id.tvDiscountLabel);
        TextView tvCancelFeeLabel = findViewById(R.id.tvCancelFeeLabel);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvCancelFee = findViewById(R.id.tvCancelFee);

        TextView tvTravelFeeLabel = findViewById(R.id.tvTravelFeeLabel);
        tvTravelFee = findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = findViewById(R.id.tvVisitFeeLabel);
        tvVisitFee = findViewById(R.id.tvVisitFee);
        TextView tvLastDueLabel = findViewById(R.id.tvLastDueLabel);
        tvLastDue = findViewById(R.id.tvLastDue);

        TextView tvTotalLabel = findViewById(R.id.tvTotalLabel);
        tvTotal = findViewById(R.id.tvTotal);
        TextView tvPaymentMethodLabel = findViewById(R.id.tvPaymentMethodLabel);

        tvPaymentMethod = findViewById(R.id.tvPaymentMethod);
        tvPaymentCardCash = findViewById(R.id.tvPaymentCardCash);
        TextView tvPaymentMethodWallet = findViewById(R.id.tvPaymentMethodWallet);
        tvPaymentWallet = findViewById(R.id.tvPaymentWallet);
        rlWalletPaymentDetails = findViewById(R.id.rlWalletPaymentDetails);

        TextView tvSignatureHeader = findViewById(R.id.tvSignatureHeader);
        TextView tvCancelReasonLabel = findViewById(R.id.tvCancelReasonLabel);

        TextView tvYourEarningsLabel = findViewById(R.id.tvYourEarningsLabel);
        TextView tvEarnedAmountLabel = findViewById(R.id.tvEarnedAmountLabel);
        tvEarnedAmount = findViewById(R.id.tvEarnedAmount);
        TextView tvAppComssionLabel = findViewById(R.id.tvAppComssionLabel);
        tvAppComssion = findViewById(R.id.tvAppComssion);
        tvCancelReason = findViewById(R.id.tvCancelReason);
        TextView tvpgComssionLabel = findViewById(R.id.tvpgComssionLabel);
        tvpgComission = findViewById(R.id.tvpgComission);

        ImageView ivBackButton = findViewById(R.id.ivBackButton);
        ivCustomer = findViewById(R.id.ivCustomer);
        ivSignature = findViewById(R.id.ivSignature);

        llRating = findViewById(R.id.llRating);
        llAmount = findViewById(R.id.llAmount);
        llSignature = findViewById(R.id.llSignature);
        llCancel = findViewById(R.id.llCancel);
        rlCancelFee = findViewById(R.id.rlCancelFee);
        rlPgCommisionn = findViewById(R.id.rlPgCommisionn);
        tvVatFee = findViewById(R.id.tvVatFee);
        rlVatFee = findViewById(R.id.rlVatFee);


        llService = findViewById(R.id.llService);

        tveventId.setTypeface(fontMedium);
        tvDate.setTypeface(fontBold);
        tvStatus.setTypeface(fontMedium);
        tvPrice.setTypeface(fontBold);
        tvamountPaidLabel.setTypeface(fontRegular);
        tvCustomerName.setTypeface(fontMedium);
        tvYouRatedLabel.setTypeface(fontRegular);
        tvYouRated.setTypeface(fontRegular);
        tvJobLocationLabel.setTypeface(fontMedium);
        tvJobLocation.setTypeface(fontRegular);
        tvCancelReasonLabel.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontMedium);
        tvTotal.setTypeface(fontMedium);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontMedium);
        tvPaymentCardCash.setTypeface(fontMedium);
        tvPaymentMethodWallet.setTypeface(fontMedium);
        tvPaymentWallet.setTypeface(fontMedium);
        tvSignatureHeader.setTypeface(fontMedium);

        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);

        tvYourEarningsLabel.setTypeface(fontMedium);
        tvEarnedAmountLabel.setTypeface(fontRegular);
        tvEarnedAmount.setTypeface(fontRegular);
        tvAppComssionLabel.setTypeface(fontRegular);
        tvAppComssion.setTypeface(fontRegular);
        tvCancelReason.setTypeface(fontRegular);
        tvpgComssionLabel.setTypeface(fontRegular);
        tvpgComission.setTypeface(fontRegular);

        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvDiscountLabel.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvCancelFeeLabel.setTypeface(fontRegular);
        tvCancelFee.setTypeface(fontRegular);


        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity();
            }
        });

        isFromSchedule = getIntent().getBooleanExtra("isFromSchedule", false);
        if (!isFromSchedule) {
            Bundle bundle = getIntent().getExtras();
            booking = (Booking) bundle.getSerializable("booking");
            reviewByProvider = booking.getReviewByProvider();
            accounting = booking.getAccounting();
            setValues();
        } else {
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), getIntent().getStringExtra("bookingId"));
        }
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
        }
    }


    @SuppressLint("SetTextI18n")
    private void setValues() {
        SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        SimpleDateFormat displayMonthYearFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
        SimpleDateFormat dateOftheDay = new SimpleDateFormat("dd", Locale.US);
        if(booking.getAccounting().getVatApplicable()) {
            rlVatFee.setVisibility(View.VISIBLE);
            tvVatFee.setText(Utility.getPrice(booking.getAccounting().getTotalVat(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        else
            tvVatFee.setText(Utility.getPrice("0",sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        if (booking.getCallType().equals("3")) {
//            tvJobLocationLabel.setVisibility(View.GONE);
            tvJobLocationLabel.setText(R.string.timeslot);
//            tvJobLocation.setVisibility(View.GONE);
//            SimpleDateFormat displayDateFormaconsultingfeet = new SimpleDateFormat("dd MMM yyyy", Locale.US);

//                tvJobLocation.setText(displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor()))));
            tvJobLocation.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
//            findViewById(R.id.vvjoblocation).setVisibility(View.GONE);
        }
        else {
            tvJobLocation.setText(booking.getAddLine1() + " " + booking.getAddLine2());
        }
        tveventId.setText(getText(R.string.jobId) + " " + booking.getBookingId());
        tvStatus.setText(booking.getStatusMsg());

        try{
            total = Double.parseDouble(accounting.getTotal()) - Double.parseDouble(accounting.getLastDues());
        }
        catch (Exception e){
            e.getMessage();
        }
        tvPrice.setText(Utility.getPrice(total+"", sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvCustomerName.setText(booking.getFirstName() + " " + booking.getLastName());


        tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvTotal.setText(Utility.getPrice(total+"", sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvEarnedAmount.setText(Utility.getPrice(accounting.getProviderEarning(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvAppComssion.setText(Utility.getPrice(accounting.getAppEarningPgComm(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if (reviewByProvider != null && reviewByProvider.getRating() != null) {
            tvYouRated.setText(Utility.roundString(reviewByProvider.getRating(), 1));
        }

        if (accounting.getTravelFee() != null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00")) {
            findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        /*if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }*/

        if (accounting.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
            tvPaymentMethod.setText(getString(R.string.card) + "  " + accounting.getLast4());
        }

        tvPaymentCardCash.setText(Utility.getPrice(accounting.getRemainingAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
            rlWalletPaymentDetails.setVisibility(View.VISIBLE);
            tvPaymentWallet.setText(Utility.getPrice(accounting.getCaptureAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (!accounting.getPgCommissionProvider().equals("") && !accounting.getPgCommissionProvider().equals("0")) {
            rlPgCommisionn.setVisibility(View.VISIBLE);
            tvpgComission.setText(Utility.getPrice(accounting.getPgCommissionProvider(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (!booking.getBookingModel().equals("3")) {
            if (booking.getServiceType().equals("1")) {
                if (booking.getCartData() != null) {
                    for (ServiceItem serviceItem : booking.getCartData()) {
                        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                        tvServiceFeeLabel.setTypeface(fontRegular);
                        tvServiceFee.setTypeface(fontRegular);
                        tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                        tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                        llService.addView(serviceView);
                    }
                }
            } else if (!booking.getCallType().equals("1") && !booking.getCallType().equals("3")) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            } else {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(getString(R.string.consultingfee));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        } else {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(getString(R.string.serviceFee));
            tvServiceFee.setText(Utility.getPrice(accounting.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        }

        TextView etstartNotes = findViewById(R.id.etstartNotes);
        if(booking.getPickupNotes().trim().isEmpty())
        {
            etstartNotes.setText("No Notes Found");
        }
        else{
            etstartNotes.setText(booking.getPickupNotes());
        }
        RecyclerView rvJobStartPhotos = findViewById(R.id.rvJobStartPhotos);
        rvJobStartPhotos.setVisibility(View.VISIBLE);
        rvJobStartPhotos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<ImgUrls> startjobimageUrls = new ArrayList<>();
        if (booking.getPickupImages().size() > 0) {

            for (String img : booking.getPickupImages()) {
                ImgUrls imgUrls = new ImgUrls(1, img);
                startjobimageUrls.add(imgUrls);
            }
        } else {
            ImgUrls imgUrls = new ImgUrls(0, "");
            startjobimageUrls.add(imgUrls);
        }
        rvJobStartPhotos.setAdapter(new JobImageListAdapter(this, startjobimageUrls));

        TextView etStopNotes = findViewById(R.id.etStopNotes);
        if(booking.getDropNotes().trim().isEmpty())
        {
            etStopNotes.setText("No Notes Found");
        }
        else{
            etStopNotes.setText(booking.getDropNotes());
        }
        RecyclerView rvJobStopPhotos = findViewById(R.id.rvJobStopPhotos);
        rvJobStopPhotos.setVisibility(View.VISIBLE);
        rvJobStopPhotos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<ImgUrls> stopjobimageUrls = new ArrayList<>();
        if (booking.getPickupImages().size() > 0) {

            for (String img : booking.getPickupImages()) {
                ImgUrls imgUrls = new ImgUrls(1, img);
                stopjobimageUrls.add(imgUrls);
            }
        } else {
            ImgUrls imgUrls = new ImgUrls(0, "");
            stopjobimageUrls.add(imgUrls);
        }
        rvJobStopPhotos.setAdapter(new JobImageListAdapter(this, stopjobimageUrls));

        if (booking.getQuestionAndAnswer() != null && booking.getQuestionAndAnswer().size() > 0) {
            LinearLayout llQuestionAnswer = findViewById(R.id.llQuestionAnswer);
            llQuestionAnswer.setVisibility(View.VISIBLE);
            findViewById(R.id.vQuestionAnswer).setVisibility(View.VISIBLE);
            for (QuestionAnswer questionAnswer : booking.getQuestionAndAnswer()) {
                View questionAnswerView = inflater.inflate(R.layout.single_row_question_answer, null);
                TextView tvQuestion = questionAnswerView.findViewById(R.id.tvQuestion);
                TextView tvAnswer = questionAnswerView.findViewById(R.id.tvAnswer);
                tvQuestion.setTypeface(fontMedium);
                tvAnswer.setTypeface(fontRegular);
                tvQuestion.setText("Q. " + questionAnswer.getName());
                if (!questionAnswer.getQuestionType().equals("10")) {
                    tvAnswer.setText(questionAnswer.getAnswer());
                } else {
                    tvAnswer.setVisibility(View.GONE);
                    RecyclerView rvJobPhotosQuestion = questionAnswerView.findViewById(R.id.rvJobPhotosQuestion);
                    rvJobPhotosQuestion.setVisibility(View.VISIBLE);
                    rvJobPhotosQuestion.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    ArrayList<String> imageUrls = new ArrayList<>();
                    imageUrls.addAll(Arrays.asList(questionAnswer.getAnswer().split(",")));

                    ArrayList<ImgUrls> stopjobimageUrls1 = new ArrayList<>();
                    if (imageUrls.size() > 0) {

                        for (String img :imageUrls) {
                            ImgUrls imgUrls = new ImgUrls(1, img);
                            stopjobimageUrls1.add(imgUrls);
                        }
                    } else {
                        ImgUrls imgUrls = new ImgUrls(0, "");
                        stopjobimageUrls1.add(imgUrls);
                    }
                    rvJobPhotosQuestion.setAdapter(new JobImageListAdapter(this, stopjobimageUrls1));
                }
                llQuestionAnswer.addView(questionAnswerView);
            }
        }
        if (booking.getAdditionalService() != null && booking.getAdditionalService().size() > 0) {
            for (AdditionalService serviceItem : booking.getAdditionalService()) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        }

        switch (booking.getStatus()) {
            case VariableConstant.JOB_COMPLETED_RAISE_INVOICE:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.jobStatusCompleted));
                break;

            case VariableConstant.CANCELLED_BY_PROVIDER:
            case VariableConstant.CANCELLED_BY_CUTOMER:
                llCancel.setVisibility(View.VISIBLE);
                rlCancelFee.setVisibility(View.VISIBLE);
                tvCancelReason.setText(booking.getCancellationReason());
                tvCancelFee.setText(Utility.getPrice(accounting.getCancellationFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.jobStatusCanceled));
                llRating.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;

            case VariableConstant.BOOKING_EXPIRED:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.jobStatusExpired));
                llRating.setVisibility(View.GONE);
                llAmount.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;

            case VariableConstant.REJECT:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.jobStatusDeclined));
                llRating.setVisibility(View.GONE);
                llAmount.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;

            default:
                tvStatus.setBackgroundColor(ContextCompat.getColor(this, R.color.jobStatusDeclined));
                llRating.setVisibility(View.GONE);
                llAmount.setVisibility(View.GONE);
                llSignature.setVisibility(View.GONE);
                break;
        }

        if (isFromSchedule) {
            CustomerData customerData = booking.getCustomerData();
            tvCustomerName.setText(customerData.getFirstName() + " " + customerData.getLastName());
            if (!customerData.getProfilePic().equals("")) {
                Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                        .error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(this))
                        .placeholder(R.drawable.profile_default_image))
                        .load(customerData.getProfilePic())
                        .into(ivCustomer);
            }

        } else if (!booking.getProfilePic().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions()
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(this))
                    .placeholder(R.drawable.profile_default_image))
                    .load(booking.getProfilePic())
                    .into(ivCustomer);
        }

        if (!booking.getSignatureUrl().equals("")) {
            Glide.with(this).
                    load(booking.getSignatureUrl())
                    .into(ivSignature);
        }

        try {
            String monthYear = displayMonthYearFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor())));
            int date = Integer.parseInt(dateOftheDay.format(serverFormat.parse(Utility.convertUTCToServerFormat(booking.getBookingRequestedFor()))));
            tvDate.setText(Utility.getDayOfMonthSuffix(date, monthYear));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(ScheduleBookngPojo scheduleBookngPojo) {
        booking = scheduleBookngPojo.getData();
        reviewByProvider = booking.getReviewByProvider();
        accounting = booking.getAccounting();


        setValues();
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {

    }

    @Override
    public void onCancelBooking(String msg) {
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.serverError), Toast.LENGTH_SHORT).show();
    }
}

