package com.sirvis.pro.main.history;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.HistoryListAdapter;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>HistoryGraphFragment</h1>
 * HistoryGraphActivity for showing history details
 * <P>
 *     Showing the graph and tablayout text based on api
 * </P>
 */

public class HistoryGraphActivity extends AppCompatActivity implements HistoryGraphPresenter.HistoryPresenterImple, TabLayout.OnTabSelectedListener {

    private String TAG = getClass().getName();
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private HistoryGraphPresenter presenter;

    private HistoryListAdapter historyListAdapter;
    private ArrayList<Booking> bookings;
    private ArrayList<Booking> tempBookings;

    private TextView tvWeekEarning, tvNoHistoryMsg;
    protected BarChart mChart;
    private TabLayout tabLayout;

    private String selectedWeeks;

    private ArrayList<ArrayList<BarEntry>> wholeBarEntries;
    private ArrayList<Integer> highestPositions;
    private ArrayList<String> apiFormatDates;

    private boolean isReselectCalled ;

    public static HistoryGraphFragment newInstance() {
        return new HistoryGraphFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_history_graph);

        init();
    }

    /**
     * init  the views
     */
    private void init() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingHistory));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(this);

        bookings = new ArrayList<>();
        tempBookings = new ArrayList<>();

        isReselectCalled = false;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);

        }

        Typeface fondBold = Utility.getFontBold(this);
        Typeface fondMedium = Utility.getFontMedium(this);
        Typeface fontRegular = Utility.getFontRegular(this);

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setTypeface(fondBold);
        tvTitle.setText(getString(R.string.history));

        RecyclerView rvHistory = findViewById(R.id.rvHistory);
        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        historyListAdapter = new HistoryListAdapter(this,bookings);
        rvHistory.setAdapter(historyListAdapter);

        TextView tvWeekEaringLabel = findViewById(R.id.tvWeekEaringLabel);
        TextView tvNumberOfBooking = findViewById(R.id.tvNumberOfBooking);
        tvWeekEarning = findViewById(R.id.tvWeekEarning);
        tvNoHistoryMsg = findViewById(R.id.tvNoHistoryMsg);

        tvWeekEaringLabel.setTypeface(fondMedium);
        tvWeekEarning.setTypeface(fondMedium);
        tvNoHistoryMsg.setTypeface(fondMedium);
        tvNumberOfBooking.setTypeface(fontRegular);

        tabLayout = findViewById(R.id.tabLayout);
        mChart = findViewById(R.id.mChart);

        presenter = new HistoryGraphPresenter(sessionManager,this);

        presenter.getHistoryWeek(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void initTabBarChart(ArrayList<ArrayList<BarEntry>> wholeBarEntries, ArrayList<Integer> highestPosition, ArrayList<String> apiFormatDates, ArrayList<String> tabFormatDates, ArrayList<String> days) {
        this.wholeBarEntries = wholeBarEntries;
        this.highestPositions = highestPosition;
        this.apiFormatDates = apiFormatDates;

        initBarChart(days);
        initBarChartListener();
        if(tabFormatDates.size() > 0)
        {
            initTabLayout(tabFormatDates);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int tabPosition = tab.getPosition();
        Log.d(TAG, "onTabSelected: "+tabPosition);
        if(tabPosition == apiFormatDates.size() - 1)
        {
            presenter.getHistory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),apiFormatDates.get(tabPosition),true);
        }
        else
        {
            presenter.getHistory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),apiFormatDates.get(tabPosition),false);
        }


        BarDataSet set1;
        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0)
        {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(wholeBarEntries.get(tabPosition));
            set1.setLabel(getString(R.string.theWeek)+" "+ selectedWeeks);
            set1.setHighLightColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));
            Highlight highlight = new Highlight(highestPositions.get(tabPosition),0, 0);
            mChart.highlightValue(highlight, false);

            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        }
        else
        {
            set1 = new BarDataSet(wholeBarEntries.get(tabPosition), getString(R.string.theWeek)+" "+ selectedWeeks);
            set1.setDrawIcons(false);
            set1.setColors(ContextCompat.getColor(this,R.color.colorPrimaryLight));
            set1.setHighLightColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));

            set1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return "" + ((int) value);
                }
            });

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            data.setBarWidth(0.8f);
            data.setHighlightEnabled(true);

            mChart.setData(data);

            Highlight highlight = new Highlight(highestPositions.get(tabPosition),0, 0);
            mChart.highlightValue(highlight, false);
        }
        mChart.invalidate();
        mChart.animateXY(1000,500);
    }

    @Override
    public void onSuccessBooking(ArrayList<Booking> bookings,String total) {
        this.bookings.clear();
        this.bookings.addAll(bookings);
        this.tempBookings.clear();
        this.tempBookings.addAll(bookings);
        historyListAdapter.notifyDataSetChanged();
        if(bookings.size()>0)
        {
            tvWeekEarning.setText(Utility.getPrice(total, sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            tvNoHistoryMsg.setVisibility(View.GONE);
        }
        else
        {
            tvWeekEarning.setText(Utility.getPrice("0", sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            tvNoHistoryMsg.setVisibility(View.VISIBLE);
        }

    }

    private void initTabLayout(final ArrayList<String> tabFormatDates)
    {
        tabLayout.removeAllTabs();
        for(int i = 0; i < tabFormatDates.size() ;i++)
        {
            tabLayout.addTab(tabLayout.newTab());
            tabLayout.getTabAt(i).setText(tabFormatDates.get(i));
        }

        new Handler().postDelayed(
                new Runnable(){
                    @Override
                    public void run() {
                        tabLayout.getTabAt(tabFormatDates.size() - 1).select();
                    }
                }, 100);

        selectedWeeks = tabLayout.getTabAt(tabFormatDates.size() -1).getText().toString();

        tabLayout.addOnTabSelectedListener(this);
    }


    private void initBarChart(ArrayList<String> days)
    {
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.getDescription().setEnabled(false);
        mChart.setDrawBarShadow(false);
        mChart.setDrawGridBackground(false);
        mChart.setScaleEnabled(false); //Enables/disables scaling for the chart on both axes.

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setLabelCount(7);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                //return "" + ((int) value);
                return "";
                //return getString(R.string.noOfBooking);
            }
        });
        leftAxis.setAxisMinimum(0);
    }

    private void initBarChartListener() {

        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h)
            {

            }

            @Override
            public void onNothingSelected() {

            }
        });
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }


    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }


    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Log.d(TAG, "onTabReselected: ");
        if(!isReselectCalled)
        {
            isReselectCalled = true;
            onTabSelected(tab);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

}

