package com.sirvis.pro.main.schedule.bookingschedule;

import com.sirvis.pro.pojo.booking.CancelPojo;
import com.sirvis.pro.pojo.shedule.ScheduleBookngPojo;

import org.json.JSONObject;

/**
 * Created by murashid on 11-Jan-18.
 */

public class BookingSchedulePresenter implements BookingScheduleModel.BookingModelImple {
    private BookingScheduleModel model;
    private BookingSchedulePresenterImple presenterImple;

    public BookingSchedulePresenter(BookingSchedulePresenterImple presenterImple) {
        model = new BookingScheduleModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session Token
     * @param bookingId booking id
     */
    public void getBooking(String sessionToken,String bookingId){
        presenterImple.startProgressBar();
        model.getBooking(sessionToken,bookingId);
    }

    void getCancelReason(String bookingId)
    {
        presenterImple.startProgressBar();
        model.getCancelReasons( bookingId);
    }

    void cancelBooking(String sessionToken,JSONObject jsonObject)
    {
        presenterImple.startProgressBar();
        model.cancelBooking(sessionToken,jsonObject);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(ScheduleBookngPojo scheduleBookngPojo) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(scheduleBookngPojo);
    }

    @Override
    public void onSuccessCancelReason(CancelPojo cancelPojo) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccessCancelReason(cancelPojo);
    }

    @Override
    public void onCancelBooking(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onCancelBooking(msg);
    }

    /**
     * ReviewPresenterImple interface for view implementation
     */
    public interface BookingSchedulePresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(ScheduleBookngPojo scheduleBookngPojo);

        void onSuccessCancelReason(CancelPojo cancelPojo);
        void onCancelBooking(String msg);
    }

}
