package com.sirvis.pro.main.chats;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ChatCustomerListAdapter;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.pojo.chat.ChatCutomerList;
import com.sirvis.pro.utility.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatCustomerListFragment extends Fragment implements ChatCustomerListAdapter.CustomerClickListener {

    private SessionManager sessionManager;
    private ChatCustomerListAdapter chatCustomerListAdapter;
    private ArrayList<ChatCutomerList> chatCutomerLists, itemsCopy;

    private static final String ARG_PARAM1 = "param1";
    private boolean isPastChat;
    private int selectedPosition =0;
    public static ChatCustomerListFragment newInstance(boolean isPastChat) {
        ChatCustomerListFragment fragment = new ChatCustomerListFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, isPastChat);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isPastChat = getArguments().getBoolean(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chat_customer_list, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        sessionManager = SessionManager.getSessionManager(getActivity());
        chatCutomerLists = new ArrayList<>();
        itemsCopy = new ArrayList<>();
        chatCustomerListAdapter = new ChatCustomerListAdapter(getActivity(), chatCutomerLists, this);
        RecyclerView rvChatCustomerList = view.findViewById(R.id.rvChatCustomerList);
        rvChatCustomerList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvChatCustomerList.setAdapter(chatCustomerListAdapter);
        
    }

    void notifyiDataChanged(ArrayList<ChatCutomerList> chatCutomerLists)
    {
        this.chatCutomerLists.clear();
        this.chatCutomerLists.addAll(chatCutomerLists);
        chatCustomerListAdapter.notifyDataSetChanged();

        this.itemsCopy.clear();
        this.itemsCopy.addAll(chatCutomerLists);
    }

    public void filter(String query)
    {
        chatCutomerLists.clear();
        if (query.isEmpty())
        {
            chatCutomerLists.addAll(itemsCopy);
        }
        else
        {
            query = query.toLowerCase();
            for (ChatCutomerList item : itemsCopy)
            {
                if (item.getFirstName().toLowerCase().contains(query))
                {
                    chatCutomerLists.add(item);
                }
                else if(!item.getLastName().equals("") && item.getLastName().toLowerCase().contains(query))
                {
                    chatCutomerLists.add(item);
                }
                else if(!item.getLastName().equals("") && (item.getFirstName().toLowerCase()+" "+item.getLastName().toLowerCase()).contains(query))
                {
                    chatCutomerLists.add(item);
                }
            }
        }
        chatCustomerListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCustomerClick(int position) {
        selectedPosition = position;
        sessionManager.setChatBookingID(chatCutomerLists.get(position).getBookingId());
        sessionManager.setChatCustomerName(chatCutomerLists.get(position).getFirstName() + " "+chatCutomerLists.get(position).getLastName());
        sessionManager.setChatCustomerPic(chatCutomerLists.get(position).getProfilePic());
        sessionManager.setChatCustomerID(chatCutomerLists.get(position).getCustomerId());


        Intent chatIntent = new Intent(getActivity(),ChattingActivity.class);
        chatIntent.putExtra("isPastChat",isPastChat);
        chatIntent.putExtra("isActiveChat",!isPastChat);
        startActivity(chatIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!isPastChat)
        {
            chatCustomerListAdapter.notifyItemChanged(selectedPosition);
        }
    }

    public void notifyDataSetChanged()
    {
        chatCustomerListAdapter.notifyDataSetChanged();
    }
}
