package com.sirvis.pro.main.profile.bank;

import org.json.JSONObject;

/**
 * Created by murashid on 26-Aug-17.
 * <h1>BankNewAccountPresenter</h1>
 * BankNewAccountPresenter presenter for BankNewAccountActivity
 * @see BankNewAccountActivity
 */

public class BankNewAccountPresenter implements BankNewAccountModel.BankNewAccountModelImplement {
    private BankNewAccountPresenterImple bankNewAccountPresenterImple;
    private BankNewAccountModel bankNewAccountModel;
    BankNewAccountPresenter(BankNewAccountPresenterImple bankNewAccountPresenterImple)
    {
        this.bankNewAccountPresenterImple = bankNewAccountPresenterImple;
        bankNewAccountModel = new BankNewAccountModel(this);
    }

    /**
     * method for passing values from view to model
     * @param token session token
     * @param jsonObject required field in json object
     */
    void addBankDetails(String token,JSONObject jsonObject)
    {
        bankNewAccountPresenterImple.startProgressBar();
        bankNewAccountModel.addBankAccount(token,jsonObject);
    }

    @Override
    public void onFailure() {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onFailure();
    }

    @Override
    public void onFailure(String failureMsg) {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onFailure(failureMsg);
    }

    @Override
    public void onSuccess(String msg) {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onSuccess(msg);
    }

    @Override
    public void onNameError() {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onNameError();
    }

    @Override
    public void onErrorEmail() {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onErrorEmail();
    }

    @Override
    public void onAccountNumberError() {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onAccountNumberError();
    }

    @Override
    public void onRoutingNumberError() {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onRoutingNumberError();
    }

    @Override
    public void onCountryError() {
        bankNewAccountPresenterImple.stopProgressBar();
        bankNewAccountPresenterImple.onCountryError();
    }

    @Override
    public void onNoError() {
        bankNewAccountPresenterImple.onNoError();
    }

    /**
     * BankNewAccountPresenterImple interface for view implementation
     */
    interface BankNewAccountPresenterImple
    {
        void startProgressBar();
        void stopProgressBar();
        void onSuccess(String msg);
        void onFailure(String msg);
        void onFailure();

        void onNameError();
        void onErrorEmail();
        void onAccountNumberError();
        void onRoutingNumberError();
        void onCountryError();
        void onNoError();
    }
}
