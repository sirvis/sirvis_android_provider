package com.sirvis.pro.main.profile.profileeedit;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.pojo.profile.ProfilePojo;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by murashid on 10-Sep-17.
 * <h1>ProfileEditModel</h1>
 * ProfileEditModel model for ProfileEditActivity
 *
 * @see ProfileEditActivity
 */

public class ProfileEditModel {
    private static final String TAG = "ProfileEditModel";
    private ProfileEditImple modelImplement;

    ProfileEditModel(ProfileEditImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for geting the profile details
     *
     * @param sessionToken session Token
     */
    void getProfile(final String sessionToken) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    ProfilePojo profilePojo = gson.fromJson(result, ProfilePojo.class);
                                    modelImplement.onSuccessProfileData(profilePojo.getData());
                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE)) {
                                    RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getProfile(newToken);
                                            modelImplement.onNewToken(newToken);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            modelImplement.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            modelImplement.sessionExpired(msg);
                                        }
                                    });
                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                                    modelImplement.sessionExpired(jsonObject.getString("message"));
                                } else {
                                    modelImplement.onFailure(jsonObject.getString("message"));
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * method for calling api for update the profile details and check the local validation
     *
     * @param sessionToken session token
     * @param jsonObject   required field in jsonObject
     */
    void updateProfile(String sessionToken, JSONObject jsonObject) {
        try {
            if (jsonObject.getString("firstName").equals("")) {
                modelImplement.onFirstNameError();
                return;
            } else if (jsonObject.getString("lastName").equals("")) {
                modelImplement.onLastNameError();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);

                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            modelImplement.onSuccessUpdateProfile(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }


    /**
     * method for uploading imag in amazon
     *
     * @param amazonS3  object of UploadFileAmazonS3
     * @param mFileTemp file which has been to upload
     */
    void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
        String BUCKETSUBFOLDER = VariableConstant.PROFILE_PIC;

        // final String imageUrl = VariableConstant.AMAZON_BASE_URL + VariableConstant.BUCKET_NAME + "/" + BUCKETSUBFOLDER + mFileTemp.getName();

        final String imageUrl = "https://" + VariableConstant.BUCKET_NAME + "." + BuildConfig.AMAZON_BASE_URL
                + BUCKETSUBFOLDER +"/"
                + mFileTemp.getName();

        Log.d(TAG, "amzonUpload: " + imageUrl);
        modelImplement.onSuccessImageUpload(imageUrl);

        amazonS3.Upload_data(AppController.getInstance(),BUCKETSUBFOLDER, mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d(TAG, "sucess: "+success);
            }

            @Override
            public void error(String errormsg) {
                Log.d(TAG, "error: " + errormsg);
            }
        });
    }

    /**
     * method for logout
     *
     * @param sessionToken session Token
     */
    void logout(String sessionToken) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userType", VariableConstant.USER_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.LOGOUT, OkHttp3ConnectionStatusCode.Request_type.POST, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            modelImplement.sessionExpired(jsonObject.getString("message"));
                        } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                            modelImplement.sessionExpired("");
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    modelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ProfileEditImple interface for ProfileEdit implementation
     */
    interface ProfileEditImple {
        void onFailure(String failureMsg);

        void onFailure();

        void onSuccessUpdateProfile(String msg);

        void onSuccessProfileData(ProfileData profileData);

        void onSuccessImageUpload(String imgUrl);

        void onNewToken(String newToken);

        void sessionExpired(String msg);

        void onFirstNameError();

        void onLastNameError();
    }
}
