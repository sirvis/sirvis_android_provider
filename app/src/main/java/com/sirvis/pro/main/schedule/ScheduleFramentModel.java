package com.sirvis.pro.main.schedule;

import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 23-Oct-17.
 * <h1>ScheduleFramentModel</h1>
 * ScheduleFramentModel model for ScheduleFragment
 * @see ScheduleFragment
 */

public class ScheduleFramentModel {

    private static final String TAG = "ScheduleFramentModel";
    private ScheduleModelImple modelImplement;

    ScheduleFramentModel(ScheduleModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for getting the month wise schedule based on date
     * @param sessionToken session token
     * @param date date of the selected month
     */
    void getShedule(final String sessionToken, final String date)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.SCHEDULE + "/" +date, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccessGetSchedule(result);
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    getShedule(newToken,date);
                                    modelImplement.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    modelImplement.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    modelImplement.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            modelImplement.sessionExpired(jsonObject.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    modelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ScheduleModelImple interface for presenter implementation
     */
    interface ScheduleModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccessGetSchedule(String result);
        void onNewToken(String token);
        void sessionExpired(String msg);
    }
}
