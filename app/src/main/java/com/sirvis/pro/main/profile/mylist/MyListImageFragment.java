package com.sirvis.pro.main.profile.mylist;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sirvis.pro.R;

import java.io.File;

public class MyListImageFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String imageUrl;
    private boolean isFile;

    public MyListImageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
    .. * @param title title.
     //* @param value value.
     * @return A new instance of fragment MyListImageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyListImageFragment newInstance(String imageUrl, boolean isFile) {
        MyListImageFragment fragment = new MyListImageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, imageUrl);
        args.putBoolean(ARG_PARAM2, isFile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imageUrl = getArguments().getString(ARG_PARAM1);
            isFile = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rooView = inflater.inflate(R.layout.fragment_my_list_image, container, false);

        ImageView ivWorkImage = rooView.findViewById(R.id.ivWorkImage);
        if(!imageUrl.equals(""))
        {
            if(isFile)
            {
                Glide.with(getActivity()).
                        load(Uri.fromFile(new File(imageUrl)))
                        .into(ivWorkImage);
            }
            else
            {
                Glide.with(getActivity()).
                        load(imageUrl)
                        .into(ivWorkImage);
            }
        }

        return rooView;
    }


}
