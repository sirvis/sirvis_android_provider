package com.sirvis.pro.main.booking;

import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 11-Apr-18.
 */

public class BookingPresenter {

    private static final String TAG = "BidBooking";

    private View view;
    private boolean isFragmentAttached = false;

    BookingPresenter(View view)
    {
        this.view = view;
        isFragmentAttached = true;
    }

    void detach()
    {
        isFragmentAttached = false;
    }

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * </p>
     */
    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    void getBooking(final String sessionToken, final boolean isBackground)
    {
        if(!isBackground)
        {
            view.startProgressBar();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKING, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        if(!isFragmentAttached)
                        {
                            return;
                        }
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccesBooking(result,true);
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getBooking(newToken, isBackground);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        if(!isFragmentAttached)
                        {
                            return;
                        }
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }


    /**
     * method for calling api for update the provider status
     * @param sessionToken sessionToken
     * @param status status 3 =>online 4 => offline
     * @param lat latitude of the current location
     * @param lng longitude of the current location
     */
    void updateProviderStatues(final String sessionToken, final String status, final String lat, final String lng)
    {
        view.startProgressBar();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status",status);
            jsonObject.put("latitude",lat);
            jsonObject.put("longitude",lng);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.MASTER_STATUS , OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                if(!isFragmentAttached)
                {
                    return;
                }
                view.stopProgressBar();
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            view.onSuccessUpdateProviderStatus(jsonObject.getString("message"));
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE))
                        {
                            RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                @Override
                                public void onSuccessRefreshToken(String newToken) {
                                    updateProviderStatues(newToken, status, lat, lng);
                                    view.onNewToken(newToken);
                                }
                                @Override
                                public void onFailureRefreshToken() {
                                    view.onFailure();
                                }

                                @Override
                                public void sessionExpired(String msg) {
                                    view.sessionExpired(msg);
                                }
                            });
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN))
                        {
                            view.sessionExpired(jsonObject.getString("message"));
                        }
                        else
                        {
                            view.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        view.onFailure();
                    }
                }
                catch (Exception e)
                {
                    view.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                if(!isFragmentAttached)
                {
                    view.stopProgressBar();
                }
                view.onFailure();
            }
        });
    }
    /**
     * BookingPresenterImple interface for view implementation
     */
    interface View {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccesBooking(String result,boolean isFromApi);
        void onSuccessUpdateProviderStatus(String msg);
        void onNewToken(String token);
        void sessionExpired(String msg);
    }
}
