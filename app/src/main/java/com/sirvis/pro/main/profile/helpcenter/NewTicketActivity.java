package com.sirvis.pro.main.profile.helpcenter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.TicketEventListAdapter;
import com.sirvis.pro.pojo.profile.helpcenter.TicketEvents;
import com.sirvis.pro.pojo.profile.helpcenter.TicketData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NewTicketActivity extends AppCompatActivity implements View.OnClickListener,  View.OnFocusChangeListener, NewTicketPresenter.NewTicketPresenterImple {

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private NewTicketPresenter presenter;

    private AlertDialog dialogPriority;
    private Typeface fontRegular;

    private EditText etSubject, etPriority, etComments;
    private boolean isAlreadyRaised = false;

    private String ticketId="";
    private RecyclerView rvTicketEvent;
    private TextView tvTime;
    private TicketEventListAdapter ticketEventListAdapter;
    private ArrayList<TicketEvents> ticketEvents;

    SimpleDateFormat displayFormat,serverFormat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ticket);

        init();
    }

    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingDetails));
        progressDialog.setCancelable(false);
        presenter = new NewTicketPresenter(this);

        displayFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);

        fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.newTicket));
        tvTitle.setTypeface(fontBold);

        TextView tvName = findViewById(R.id.tvName);
        TextView tvNameHead = findViewById(R.id.tvNameHead);
        tvTime = findViewById(R.id.tvTime);
        TextView tvSend = findViewById(R.id.tvSend);
        etSubject = findViewById(R.id.etSubject);
        etPriority = findViewById(R.id.etPriority);
        etComments = findViewById(R.id.etComments);

        tvName.setTypeface(fontRegular);
        tvNameHead.setTypeface(fontMedium);
        tvTime.setTypeface(fontRegular);
        etSubject.setTypeface(fontRegular);
        etPriority.setTypeface(fontRegular);
        etComments.setTypeface(fontRegular);
        tvSend.setTypeface(fontRegular);

        Intent intent = getIntent();
        isAlreadyRaised = intent.getBooleanExtra("isAlreadyRaised",false);

        tvName.setText(sessionManager.getFirstName() + " "+sessionManager.getLastName());
        tvNameHead.setText(String.valueOf(sessionManager.getFirstName().charAt(0)));

        ticketEvents = new ArrayList<>();
        ticketEventListAdapter = new TicketEventListAdapter(this,ticketEvents);
        rvTicketEvent = findViewById(R.id.rvTicketEvent);
        rvTicketEvent.setLayoutManager(new LinearLayoutManager(this));
        rvTicketEvent.setAdapter(ticketEventListAdapter);

        createDialogPriority();

        if(isAlreadyRaised)
        {
            ticketId = intent.getStringExtra("ticketId");
            etPriority.setEnabled(false);
            etSubject.setEnabled(false);
            etSubject.setFocusable(false);
            etPriority.setFocusable(false);

            tvTitle.setText(getString(R.string.ticket)+" "+ticketId);
            presenter.getTicketDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), ticketId);

            if(intent.getStringExtra("status").equals("solved"))
            {
                etComments.setVisibility(View.GONE);
                tvSend.setVisibility(View.GONE);
            }
        }
        else
        {
            priorityLevel(4);
            etPriority.setOnClickListener(this);
            etPriority.setOnFocusChangeListener(this);
            tvTime.setText(displayFormat.format(new Date()));
        }

        tvSend.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b)
        {
            switch (view.getId())
            {
                case R.id.etPriority:
                    if(!dialogPriority.isShowing() )
                    {
                        dialogPriority.show();
                    }
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.etPriority:
                if(!dialogPriority.isShowing() )
                {
                    dialogPriority.show();
                }
                break;

            case R.id.tvSend:
                if(isAlreadyRaised)
                {
                    progressDialog.setMessage(getString(R.string.sending));
                    sendComment();
                }
                else
                {
                    progressDialog.setMessage(getString(R.string.creatingTicket));
                    sendTickect();
                }
                break;
        }
    }

    private void sendComment() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",ticketId);
            jsonObject.put("body",etComments.getText().toString());
            jsonObject.put("author_id",sessionManager.getZendeskRequesterId());

            presenter.sendComment(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void sendTickect() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("subject",etSubject.getText().toString());
            jsonObject.put("body",etComments.getText().toString());
            jsonObject.put("priority","high");
            jsonObject.put("requester_id",sessionManager.getZendeskRequesterId());
            jsonObject.put("status","open");
            jsonObject.put("type","problem");
            //jsonObject.put("group_id","");
            //jsonObject.put("assignee_id","");

            presenter.createTicket(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void createDialogPriority()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        View view = LayoutInflater.from(this).inflate(R.layout.alert_dialog_ticket_priority, null);
        alertDialogBuilder.setView(view);

        dialogPriority = alertDialogBuilder.create();
        dialogPriority.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        TextView tvUrgent = view.findViewById(R.id.tvUrgent);
        TextView tvHigh = view.findViewById(R.id.tvHigh);
        TextView tvNormal = view.findViewById(R.id.tvNormal);
        TextView tvLow = view.findViewById(R.id.tvLow);

        tvUrgent.setTypeface(fontRegular);
        tvHigh.setTypeface(fontRegular);
        tvNormal.setTypeface(fontRegular);
        tvLow.setTypeface(fontRegular);

        tvUrgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priorityLevel(1);
            }
        });

        tvHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priorityLevel(2);
            }
        });

        tvNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priorityLevel(3);
            }
        });

        tvLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                priorityLevel(4);
            }
        });

    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessRaiseTicket(String msg) {
        VariableConstant.IS_TICKET_UPDATED = true;

        //Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        closeActivity();
    }

    @Override
    public void onSuccessComment() {
        TicketEvents events = new TicketEvents();
        events.setBody(etComments.getText().toString());
        events.setAuthor_id(sessionManager.getZendeskRequesterId());
        events.setCreated_at(String.valueOf(System.currentTimeMillis()/1000));
        events.setName(sessionManager.getFirstName() +" "+sessionManager.getLastName());
        events.setTimeStamp(String.valueOf(System.currentTimeMillis()/1000));

        ticketEvents.add(events);
        ticketEventListAdapter.notifyDataSetChanged();
        rvTicketEvent.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                rvTicketEvent.scrollToPosition(ticketEvents.size()-1);
            }
        },200);

        etComments.setText("");
        Utility.hideKeyboad(this);
    }

    @Override
    public void onSuccessTicket(TicketData ticket) {

        etSubject.setText(ticket.getSubject());

        ticketEvents.addAll(ticket.getEvents());
        ticketEventListAdapter.notifyDataSetChanged();

        try {
            tvTime.setText(displayFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(ticket.getTimeStamp()))));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        rvTicketEvent.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                rvTicketEvent.scrollToPosition(ticketEvents.size()-1);
            }
        },200);

        if(ticket.getPriority() !=null )
            switch (ticket.getPriority())
            {
                case "urgent":
                    priorityLevel(1);
                    break;

                case "high":
                    priorityLevel(2);
                    break;

                case "normal":
                    priorityLevel(3);
                    break;

                case "low":
                    priorityLevel(4);
                    break;
            }
        else
            priorityLevel(2);
    }

    @Override
    public void onErrorSubject() {
        Toast.makeText(this,getString(R.string.enterSubject),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorPriority() {
        Toast.makeText(this,getString(R.string.selectPriority),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorComment() {
        Toast.makeText(this,getString(R.string.enterComment),Toast.LENGTH_SHORT).show();
    }

    public void priorityLevel(int selected) {
        if(dialogPriority.isShowing())
        {
            dialogPriority.dismiss();
        }
        String priority = "";
        int priorityLevel = 0;
        switch (selected)
        {
            case 1:
                priority = getString(R.string.urgent);
                priorityLevel = R.drawable.circle_color_red;
                break;

            case 2:
                priority = getString(R.string.high);
                priorityLevel = R.drawable.circle_color_blue;
                break;

            case 3:
                priority = getString(R.string.normal);
                priorityLevel = R.drawable.circle_color_green;
                break;

            case 4:
                priority = getString(R.string.low);
                priorityLevel = R.drawable.circle_color_yellow;
                break;
        }

        if(isAlreadyRaised)
        {
            etPriority.setCompoundDrawablesWithIntrinsicBounds(priorityLevel,0,0,0);
        }
        else
        {
            etPriority.setCompoundDrawablesWithIntrinsicBounds(priorityLevel,0,R.drawable.vector_color_primary_arrow_drop_down,0);
        }
        etPriority.setText(priority);

    }
}
