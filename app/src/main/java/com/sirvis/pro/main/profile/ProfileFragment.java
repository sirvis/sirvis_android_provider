package com.sirvis.pro.main.profile;


import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.livechatinc.inappchat.ChatWindowActivity;
import com.sirvis.pro.R;
import com.sirvis.pro.main.earning.EarningWebActivity;
import com.sirvis.pro.main.history.HistoryGraphActivity;
import com.sirvis.pro.main.profile.about.AboutActivity;
import com.sirvis.pro.main.profile.bank.BankDetailsActivity;
import com.sirvis.pro.main.profile.calltype.CallTypeCategoryListActivity;
import com.sirvis.pro.main.profile.helpcenter.HelpCenterActivity;
import com.sirvis.pro.main.profile.mydocument.MyDocumentActivity;
import com.sirvis.pro.main.profile.mydocument.MyDocumentPresenter;
import com.sirvis.pro.main.profile.myratecard.MyRateCardActivity;
import com.sirvis.pro.main.profile.mylist.MyListActivity;
import com.sirvis.pro.main.profile.profileeedit.ProfileEditActivity;
import com.sirvis.pro.main.profile.review.ReviewActivity;
import com.sirvis.pro.main.profile.share.ShareActivity;
import com.sirvis.pro.main.profile.support.SupportActivity;
import com.sirvis.pro.main.profile.wallet.WalletActivity;
import com.sirvis.pro.pojo.profile.ProfileData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

/**
 * Created by murashid on 10-Sep-17.
 * <h1>ProfileFragment</h1>
 * ProfileEditActivity activity for showing Profile details
 */
public class ProfileFragment extends Fragment implements View.OnClickListener, MyDocumentPresenter.View {

    private static final String TAG = "ProfileFragment";
    private SessionManager sessionManager;
    private ImageView ivProfile;
    private  TextView tvName, tvMyDocument, tvWalletAmount;
    private View viewMyDocument;

    private ProgressDialog progressDialog;

    //sessionManager.getIsMyDocumentPresent()   0 => not yet called , 1 => present , 2 => not present

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading));
        sessionManager = SessionManager.getSessionManager(getActivity());
        Typeface fontBold = Utility.getFontBold(getActivity());
        Typeface fontRegular = Utility.getFontRegular(getActivity());

        ivProfile = rootView.findViewById(R.id.ivProfile);
        LinearLayout llProfile = rootView.findViewById(R.id.llProfile);
        tvName = rootView.findViewById(R.id.tvName);
        TextView tvViewProfile = rootView.findViewById(R.id.tvViewProfile);
        TextView tvHistory = rootView.findViewById(R.id.tvHistory);
        TextView tvEarning = rootView.findViewById(R.id.tvEarning);
        TextView tvPayout = rootView.findViewById(R.id.tvPayout);
        TextView tvMyListing = rootView.findViewById(R.id.tvMyListing);
        TextView tvMyRateCard = rootView.findViewById(R.id.tvMyRateCard);
        TextView tvCallType = rootView.findViewById(R.id.tvCallType);
        tvMyDocument= rootView.findViewById(R.id.tvMyDocument);
        TextView tvWallet= rootView.findViewById(R.id.tvWallet);
        tvWalletAmount= rootView.findViewById(R.id.tvWalletAmount);
        TextView tvSupport = rootView.findViewById(R.id.tvSupport);
        TextView tvHelpCenter = rootView.findViewById(R.id.tvHelpCenter);
        TextView tvLiveChat = rootView.findViewById(R.id.tvLiveChat);
        TextView tvShare = rootView.findViewById(R.id.tvShare);
        TextView tvLivem = rootView.findViewById(R.id.tvLivem);
        TextView tvReview = rootView.findViewById(R.id.tvReview);

        viewMyDocument = rootView.findViewById(R.id.viewMyDocument);


        tvName.setTypeface(fontBold);
        tvViewProfile.setTypeface(fontRegular);
        tvHistory.setTypeface(fontBold);
        tvEarning.setTypeface(fontBold);
        tvPayout.setTypeface(fontBold);
        tvMyListing.setTypeface(fontBold);
        tvMyRateCard.setTypeface(fontBold);
        tvCallType.setTypeface(fontBold);
        tvMyDocument.setTypeface(fontBold);
        tvWallet.setTypeface(fontBold);
        tvWalletAmount.setTypeface(fontBold);
        tvSupport.setTypeface(fontBold);
        tvHelpCenter.setTypeface(fontBold);
        tvLiveChat.setTypeface(fontBold);
        tvShare.setTypeface(fontBold);
        tvLivem.setTypeface(fontBold);
        tvReview.setTypeface(fontBold);

        llProfile.setOnClickListener(this);
        tvHistory.setOnClickListener(this);
        tvEarning.setOnClickListener(this);
        tvPayout.setOnClickListener(this);
        tvMyListing.setOnClickListener(this);
        tvMyRateCard.setOnClickListener(this);
        tvCallType.setOnClickListener(this);
        tvMyDocument.setOnClickListener(this);
        rootView.findViewById(R.id.llWallet).setOnClickListener(this);
        tvSupport.setOnClickListener(this);
        tvLiveChat.setOnClickListener(this);
        tvHelpCenter.setOnClickListener(this);
        tvShare.setOnClickListener(this);
        tvLivem.setOnClickListener(this);
        tvReview.setOnClickListener(this);

        if(!sessionManager.getProfilePic().equals(""))
        {
            Glide.with(getActivity()).setDefaultRequestOptions(new RequestOptions()
                    .error(R.drawable.profile_default_image)
                    .transform(new CircleTransform(getActivity()))
                    .placeholder(R.drawable.profile_default_image))
                    .load(sessionManager.getProfilePic())
                    .into(ivProfile);
        }

        MyDocumentPresenter presenter = new MyDocumentPresenter(this);

        if(sessionManager.getIsMyDocumentPresent() == 0)
        {
            presenter.getCategory(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
        else if(sessionManager.getIsMyDocumentPresent() == 1)
        {
            tvMyDocument.setVisibility(View.VISIBLE);
            viewMyDocument.setVisibility(View.VISIBLE);
        }

        if(sessionManager.getWalletEnable().equals("false"))
        {
            tvWallet.setVisibility(View.GONE);
            rootView.findViewById(R.id.viewWallet).setVisibility(View.GONE);
        }

        String name = sessionManager.getFirstName();
        if(!sessionManager.getLastName().equals(""))
        {
            name = name +" "+ sessionManager.getLastName();
        }
        CollapsingToolbarLayout ctlMyBooking = rootView.findViewById(R.id.ctlMyBooking);
        ctlMyBooking.setTitle(name);
        ctlMyBooking.setCollapsedTitleTextAppearance(R.style.TextCollapedProfileName);
        ctlMyBooking.setExpandedTitleTextAppearance(R.style.TextExpandHide);
        ctlMyBooking.setCollapsedTitleGravity(Gravity.CENTER);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvName.setText(sessionManager.getFirstName() +" "+sessionManager.getLastName());

        if(VariableConstant.IS_PROFILE_PHOTO_UPDATED)
        {
            VariableConstant.IS_PROFILE_PHOTO_UPDATED = false;
            if(!sessionManager.getProfilePic().equals(""))
            {
                Glide.with(getActivity()).setDefaultRequestOptions(new RequestOptions()
                        .error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(getActivity()))
                        .placeholder(R.drawable.profile_default_image))
                        .load(sessionManager.getProfilePic())
                        .into(ivProfile);
            }
        }
        tvWalletAmount.setText("("+Utility.getPrice(sessionManager.getWalletAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation())+")");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.llProfile:
                Intent profileEditIntent = new Intent(getActivity(),ProfileEditActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(profileEditIntent,ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(profileEditIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;


            case R.id.tvHistory:
                Intent historyIntent = new Intent(getActivity(), HistoryGraphActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(historyIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(historyIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvEarning:
                Intent earningIntent = new Intent(getActivity(), EarningWebActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(earningIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(earningIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvPayout:
                Intent bankIntent = new Intent(getActivity(), BankDetailsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(bankIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(bankIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvMyListing:
                Intent myListIntent = new Intent(getActivity(), MyListActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(myListIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(myListIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }

                break;

            case R.id.tvMyRateCard:
                Intent myRateCardIntent = new Intent(getActivity(), MyRateCardActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(myRateCardIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(myRateCardIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvCallType:
                Intent callTypeIntent = new Intent(getActivity(), CallTypeCategoryListActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(callTypeIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(callTypeIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;



            case R.id.tvMyDocument:
                Intent myDocumentIntent = new Intent(getActivity(), MyDocumentActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(myDocumentIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(myDocumentIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.llWallet:
                Intent walletIntent = new Intent(getActivity(), WalletActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(walletIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(walletIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;


            case R.id.tvSupport:
                Intent faqIntent = new Intent(getActivity(), SupportActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(faqIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(faqIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }

                break;

            case R.id.tvLiveChat:
                Intent intent = new Intent(getActivity(),ChatWindowActivity.class);
                intent.putExtra(ChatWindowActivity.KEY_GROUP_ID, "your_group_id");
                intent.putExtra(ChatWindowActivity.KEY_LICENCE_NUMBER, "4711811");
                startActivity(intent);
                break;

            case R.id.tvHelpCenter:
                Intent helpCenterIntent = new Intent(getActivity(), HelpCenterActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(helpCenterIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(helpCenterIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }

                break;

            case R.id.tvShare:
                Intent shareIntent = new Intent(getActivity(), ShareActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(shareIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(shareIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvReview:
                Intent reviewIntent = new Intent(getActivity(), ReviewActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(reviewIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(reviewIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;

            case R.id.tvLivem:
                Intent aboutIntent = new Intent(getActivity(), AboutActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(aboutIntent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
                else
                {
                    startActivity(aboutIntent);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(ProfileData profileData) {
        sessionManager.setDob(profileData.getDob());

        if( profileData.getDocuments() !=null &&  profileData.getDocuments().size() > 0)
        {
            sessionManager.setIsMyDocumentPresent(1);
            tvMyDocument.setVisibility(View.VISIBLE);
            viewMyDocument.setVisibility(View.VISIBLE);
        }
        else
        {
            sessionManager.setIsMyDocumentPresent(2);
        }
    }

    @Override
    public void onSuccess(String msg) {

    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,getActivity());
    }

}
