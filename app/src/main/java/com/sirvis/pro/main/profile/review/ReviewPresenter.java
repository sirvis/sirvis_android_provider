package com.sirvis.pro.main.profile.review;

import com.sirvis.pro.pojo.profile.review.ReviewData;

/**
 * Created by murashid on 11-Sep-17.
 *
 * <h1>ReviewPresenter</h1>
 * ReviewPresenter presenter for ReviewActivity
 */

public class ReviewPresenter implements ReviewModel.ReviewModelImple {
    private ReviewModel model;
    private ReviewPresenterImple presenterImple;

    ReviewPresenter(ReviewPresenterImple presenterImple) {
        model = new ReviewModel(this);
        this.presenterImple = presenterImple;
    }

    /**
     * method for passing value from view to model
     * @param sessionToken session Token
     * @param pageNo index of the page
     */
    void getReview(String sessionToken,int pageNo){
        presenterImple.startProgressBar();
        model.getReview(sessionToken,pageNo);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(ReviewData reviewData) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(reviewData);
    }

    /**
     * ReviewPresenterImple interface for view implementation
     */
    interface ReviewPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(ReviewData reviewData);
    }
}
