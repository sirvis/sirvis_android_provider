package com.sirvis.pro.main.profile.profileeedit;

import android.util.Log;
import android.util.Patterns;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.phonevalidation.PhoneValidationPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 11-Oct-17.
 * <h1>EditEmailPhoneModel</h1>
 * EditEmailPhoneModel model for EditEmailPhoneAcitivty
 * @see EditEmailPhoneActivity
 */

public class EditEmailPhoneModel {
    private static final String TAG = "ProfileEditModel";
    private EditEmailPhoneModelImple modelImplement;

    EditEmailPhoneModel(EditEmailPhoneModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for changing phone or email based on isphone value
     * @param sessionToken session Token
     * @param isPhone true for phone and false for email
     * @param countyCode country code
     * @param value value email or phone
     */
    void updateValue(String sessionToken, final boolean isPhone, String countyCode,String value)
    {
        String sericeUrl = "";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userType",VariableConstant.USER_TYPE);
            if(isPhone)
            {
                if(value.equals(""))
                {
                    modelImplement.onErrorPhone();
                    return;
                }
                jsonObject.put("countryCode",countyCode);
                jsonObject.put("phone",value);
                sericeUrl = ServiceUrl.CHANGE_PHONE_NUMBER;
            }
            else
            {
                if (!Patterns.EMAIL_ADDRESS.matcher(value).matches() && !Patterns.PHONE.matcher(value).matches())
                {
                    modelImplement.onErrorEmail();
                    return;
                }
                jsonObject.put("email",value);
                sericeUrl = ServiceUrl.CHANGE_EMAIL;
            }
            Log.d(TAG, "updateValue: "+jsonObject);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken,sericeUrl , OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                Gson gson = new Gson();
                                PhoneValidationPojo phoneValidationPojo = gson.fromJson(result,PhoneValidationPojo.class);

                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                                {
                                    if(isPhone)
                                    {
                                        modelImplement.onSuccessPhone(phoneValidationPojo);
                                    }
                                    else
                                    {
                                        modelImplement.onSuccessEmail(phoneValidationPojo.getMessage());
                                    }
                                }
                                else
                                {
                                    modelImplement.onFailure(phoneValidationPojo.getMessage());
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * EditEmailPhoneModelImple interface for presenter implementation
     */
    interface EditEmailPhoneModelImple {
        void onFailure(String failureMsg);
        void onFailure();

        void onErrorPhone();
        void onErrorEmail();
        void onSuccessPhone(PhoneValidationPojo phoneValidationPojo);
        void onSuccessEmail(String msg);
    }
}
