package com.sirvis.pro.main.profile.share;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.List;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>ShareActivity</h1>
 * ShareActivity for showing the share option
 */

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {

    private String referalCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        init();
    }

    /**
     * init the views
     */
    private void init()
    {
        SessionManager sessionManager = SessionManager.getSessionManager(this);
        referalCode = sessionManager.getReferalCode();

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.share));
        tvTitle.setTypeface(fontBold);

        TextView tvShareMsa1 = findViewById(R.id.tvShareMsa1);
        TextView tvShareCodeLabel = findViewById(R.id.tvShareCodeLabel);
        TextView tvShareCode = findViewById(R.id.tvShareCode);
        TextView tvShareMsa2 = findViewById(R.id.tvShareMsa2);
        TextView tvFacebook = findViewById(R.id.tvFacebook);
        TextView tvInstagram = findViewById(R.id.tvInstagram);
        TextView tvEmail = findViewById(R.id.tvEmail);
        TextView tvMessage = findViewById(R.id.tvMessage);
        TextView tvWatsapp = findViewById(R.id.tvWatsapp);
        TextView tvSnapChat = findViewById(R.id.tvSnapChat);
        TextView tvTwitter = findViewById(R.id.tvTwitter);

        tvShareMsa1.setTypeface(fontRegular);
        tvShareCodeLabel.setTypeface(fontRegular);
        tvShareCode.setTypeface(fontRegular);
        tvShareMsa2.setTypeface(fontRegular);
        tvFacebook.setTypeface(fontRegular);
        tvInstagram.setTypeface(fontRegular);
        tvEmail.setTypeface(fontRegular);
        tvMessage.setTypeface(fontRegular);
        tvWatsapp.setTypeface(fontRegular);
        tvSnapChat.setTypeface(fontRegular);
        tvTwitter.setTypeface(fontRegular);

        tvFacebook.setOnClickListener(this);
        tvInstagram.setOnClickListener(this);
        tvEmail.setOnClickListener(this);
        tvMessage.setOnClickListener(this);
        tvWatsapp.setOnClickListener(this);
        tvSnapChat.setOnClickListener(this);
        tvTwitter.setOnClickListener(this);

        tvShareCode.setText(referalCode);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }


    @Override
    public void onClick(View v) {

        AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.SentReferal.value);

        switch (v.getId())
        {
            case R.id.tvFacebook:
                openFaceBook();
                break;

            case R.id.tvInstagram:
                openInstagram();
                break;

            case R.id.tvEmail:
                openEmail();
                break;

            case R.id.tvMessage:
                openMessage();
                break;

            case R.id.tvWatsapp:
                openWatsapp();
                break;

            case R.id.tvSnapChat:
                openSnapChat();
                break;

            case R.id.tvTwitter:
                openTwitter();
                break;
        }
    }

    /**
     * method for opening the Facebook for sharing the app
     */
    private void openFaceBook() {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, VariableConstant.PLAY_STORE_LINK);
            boolean facebookAppFound = false;
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches)
            {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook"))
                {
                    intent.setPackage(info.activityInfo.packageName);
                    facebookAppFound = true;
                    break;
                }
            }

            if(facebookAppFound)
            {
                startActivity(intent);
            }
            else
            {
                String url = "https://facebook.com/sharer.php?u="+VariableConstant.PLAY_STORE_LINK;
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse(url));
                // Verify that the intent will resolve to an activity
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);
                }
                else{
                    DialogHelper.showWaringMessage(ShareActivity.this,"No app supports this action.");
                }

            }

        } catch (Exception e) {

            e.printStackTrace();
            Toast.makeText(this,getString(R.string.facebookNotInstalled),Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * method for opening the Instagram for sharing the app
     */
    private void openInstagram() {

        try {
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.subjectForEmailShare));
            shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                    getString(R.string.hereMyCode)+" "+
                    referalCode+".\n"+
                    VariableConstant.PLAY_STORE_LINK);
            shareIntent.setPackage("com.instagram.android");
            startActivity(shareIntent);
        }
        catch (Exception e)
        {
            Toast.makeText(this,getString(R.string.instagramNotInstalled),Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * method for opening the Emai for sharing the app
     */
    private void openEmail() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
        i.setType("message/rfc822");
        i.setData(Uri.parse("mailto:" + ""));
        i.putExtra(Intent.EXTRA_SUBJECT,  getString(R.string.subjectForEmailShare));
        i.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                getString(R.string.hereMyCode)+" "+
                referalCode+".\n"+
                VariableConstant.PLAY_STORE_LINK);
        startActivity(Intent.createChooser(i, "Send email"));
    }

    /**
     * method for opening the Message for sharing the app
     */
    private void openMessage() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("sms:"));
        intent.putExtra("sms_body", getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                getString(R.string.hereMyCode)+" "+
                referalCode+".\n"+
                VariableConstant.PLAY_STORE_LINK);
        startActivity(intent);

    }
    /**
     * method for opening the Watsapp for sharing the app
     */
    private void openWatsapp() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                getString(R.string.hereMyCode)+" "+
                referalCode+".\n"+
                VariableConstant.PLAY_STORE_LINK);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this,getString(R.string.watsappNotInstalled),Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * method for opening the SnapChat for sharing the app
     */
    private void openSnapChat()
    {
        try {
            Intent  snapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://snapchat.com"));
            snapIntent.setType("text/plain");
            snapIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                    getString(R.string.hereMyCode)+" "+
                    referalCode+".\n"+
                    VariableConstant.PLAY_STORE_LINK);
            snapIntent.setPackage("com.snapchat.android");
            startActivity(snapIntent);
        }
        catch (Exception e)
        {
            Toast.makeText(this,getString(R.string.snapchatNotInstalled),Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * method for opening the Twitter for sharing the app
     */
    private void openTwitter() {
        try {
            String url = "http://www.twitter.com/intent/tweet?url="+VariableConstant.PLAY_STORE_LINK +"&text="+getString(R.string.messageContentForSharingAppInOtherApp)+ " " +
                    getString(R.string.hereMyCode)+" "+
                    referalCode+".";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        catch (Exception e)
        {
            //Toast.makeText(this,getString(R.string.twitterNotInstalled),Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
