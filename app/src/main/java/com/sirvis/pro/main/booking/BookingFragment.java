package com.sirvis.pro.main.booking;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ViewPagerAdapter;
import com.sirvis.pro.main.MainActivity;
import com.sirvis.pro.main.notification.NotificationActivity;
import com.sirvis.pro.main.profile.wallet.WalletActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.pojo.booking.BookingData;
import com.sirvis.pro.pojo.booking.BookingPojo;
import com.sirvis.pro.service.LocationPublishService;
import com.sirvis.pro.service.OfflineLocationPublishService;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.MixpanelEvents;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>BookingFragment</h1>
 * BookingFragment for showing Events Details
 */

public class BookingFragment extends Fragment implements View.OnClickListener, BookingPresenter.View, BookingListFragment.BookingListFragmentInteraction {

    private static final String TAG = "BookingFragment";
    private Typeface fondBold, fontRegular;
    private LayoutInflater inflater;
    private TextView tvCountFirstTab, tvCountSecondTab;
    private TextView tvOnlineOffline, tvStatus;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BookingPresenter presenter;
    private BookingListFragment offersFragment, activeBidFragment;
    private ViewPager vpBooking;

    private ArrayList<Booking> offersBookings, activeBidBooking;
    private int acceptedBookingSize = 0;

    private BroadcastReceiver receiver;
    private IntentFilter filter;
    private Gson gson;

    private long lastRefreshTime;

    private BookingFragmentInteraction mListener;

    private TextView tvNotificationCount, tvWalletBalance;
    private boolean isnewBooking = false;

    public static BookingFragment newInstance() {
        return new BookingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        this.inflater = inflater;
        View rootView = inflater.inflate(R.layout.fragment_booking, container, false);

        init(rootView);

        return rootView;
    }

    /**
     * init  the views
     *
     * @param rootView parent View
     */
    private void init(View rootView) {

        filter = new IntentFilter();
        filter.addAction(VariableConstant.INTENT_ACTION_REFRESH_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
        filter.addAction(VariableConstant.INTENT_ACTION_MAKE_ONLINE);
        filter.addAction(VariableConstant.INTENT_ACTION_MAKE_OFFLINE);
        filter.addAction(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //notificationManager.cancelAll();
                if (intent.getAction().equals(VariableConstant.INTENT_ACTION_REFRESH_BOOKING)) {
                    if (lastRefreshTime + 700 < System.currentTimeMillis()) {
                        AppController.getInstance().startNewBookingRingtoneService();
                        progressDialog.setMessage(getString(R.string.refreshing));
                        presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
                        if (vpBooking != null) {
                            vpBooking.setCurrentItem(0);
                        }
                    }
                    lastRefreshTime = System.currentTimeMillis();

                    isnewBooking = true;
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_CANCEL_BOOKING)) {
                    String cancelid = intent.getStringExtra("cancelid");
                    String msg = intent.getStringExtra("msg");
                    String header = intent.getStringExtra("header");
                    for (Booking booking : offersBookings) {
                        if (booking.getBookingId().equals(cancelid)) {
                            if (lastRefreshTime + 2000 < System.currentTimeMillis()) {
                                progressDialog.setMessage(getString(R.string.refreshing));
                                presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
                            }
                            lastRefreshTime = System.currentTimeMillis();
                            return;
                        }
                    }
                    for (Booking booking : activeBidBooking) {
                        if (booking.getBookingId().equals(cancelid)) {
                            if (lastRefreshTime + 2000 < System.currentTimeMillis()) {
                                progressDialog.setMessage(getString(R.string.refreshing));
                                presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
                            }
                            lastRefreshTime = System.currentTimeMillis();
                            return;
                        }
                    }
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_MAKE_ONLINE) && !sessionManager.getIsDriverOnline()) {
                    progressDialog.setMessage(getString(R.string.goingOnline));
                    presenter.updateProviderStatues(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), VariableConstant.ONLINE_STATUS, sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_MAKE_OFFLINE)) {
                    setOnOffTheJob(false);
                } else if (intent.getAction().equals(VariableConstant.INTENT_ACTION_PROFILE_ACTIVATION)) {
//                    progressDialog.setMessage(getString(R.string.refreshing));
//                    presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);

                    Intent intentNotification = new Intent(getActivity(), NotificationActivity.class);
                    startActivity(intentNotification);
                    getActivity().overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);

                    //tvNotificationCount.setVisibility(View.GONE);

                }
            }
        };

        presenter = new BookingPresenter(this);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(getActivity());

        offersBookings = new ArrayList<>();
        activeBidBooking = new ArrayList<>();
        fondBold = Utility.getFontBold(getActivity());
        fontRegular = Utility.getFontRegular(getActivity());

        Log.d(TAG, "sessionToken: " + AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        TabLayout tablayoutBooking = rootView.findViewById(R.id.tablayoutBooking);
        vpBooking = rootView.findViewById(R.id.vpBooking);

        offersFragment = BookingListFragment.newInstance("offers");
        offersFragment.setBookingListFragmentInteraction(this);
        viewPagerAdapter.addFragment(offersFragment, "");

        if (sessionManager.getIsBidBooking()) {
            activeBidFragment = BookingListFragment.newInstance("bid");
            activeBidFragment.setBookingListFragmentInteraction(this);
            viewPagerAdapter.addFragment(activeBidFragment, "");
        } else {
            tablayoutBooking.setVisibility(View.GONE);
        }
        vpBooking.setAdapter(viewPagerAdapter);
        tablayoutBooking.setupWithViewPager(vpBooking);
        for (int i = 0; i < tablayoutBooking.getTabCount(); i++) {
            tablayoutBooking.getTabAt(i).setCustomView(getCustomTabView(i));
        }

        tvOnlineOffline = rootView.findViewById(R.id.tvOnlineOffline);
        tvOnlineOffline.setTypeface(fondBold);
        tvOnlineOffline.setOnClickListener(this);

        tvStatus = rootView.findViewById(R.id.tvStatus);
        tvStatus.setTypeface(fondBold);

        tvNotificationCount = rootView.findViewById(R.id.tvNotificationCount);
        tvWalletBalance = rootView.findViewById(R.id.tvWalletBalance);

        rootView.findViewById(R.id.rlNotification).setOnClickListener(this);
        rootView.findViewById(R.id.flWallet).setOnClickListener(this);
        tvNotificationCount.setTypeface(fontRegular);
        tvWalletBalance.setTypeface(fontRegular);

        progressDialog.setMessage(getString(R.string.gettingBookings));
        gson = new Gson();


        /*if (sessionManager.getMyBooking().equals("")) {*/
        presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),
                false);
       /* } else {
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onSuccesBooking(sessionManager.getMyBooking(), false);
                }
            }, 750);
        }*/

    }

    private View getCustomTabView(int i) {
        View view = inflater.inflate(R.layout.custom_tab_view_header_with_count, null, false);
        TextView tvHeader = view.findViewById(R.id.tvHeader);
        TextView tvCount = view.findViewById(R.id.tvCount);
        tvHeader.setTypeface(fondBold);
        tvCount.setTypeface(fontRegular);
        if (i == 0) {
            if (sessionManager.getIsBidBooking()) {
                tvHeader.setText(getString(R.string.offers));
            } else {
                tvHeader.setText(getString(R.string.requests));
            }
            tvCountFirstTab = tvCount;
        } else {
            if (sessionManager.getIsBidBooking()) {
                tvHeader.setText(getString(R.string.activeBids));
            } else {
                tvHeader.setText(getString(R.string.upcoming));
            }
            tvCountSecondTab = tvCount;
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, filter);

        if (VariableConstant.IS_BOOKING_ACCEPTED) {
            VariableConstant.IS_BOOKING_ACCEPTED = false;
            if (mListener != null) {
                mListener.setAcceptedBookingFragment();
            }
            return;
        }

        VariableConstant.IS_MYBOOKING_OPENED = true;

        if (VariableConstant.IS_BOOKING_UPDATED) {
            VariableConstant.IS_BOOKING_UPDATED = false;
            progressDialog.setMessage(getString(R.string.refreshing));
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
        } else if (VariableConstant.IS_WALLET_UPDATED) {
            VariableConstant.IS_WALLET_UPDATED = false;
            progressDialog.setMessage(getString(R.string.refreshing));
            presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), false);
        }
        if (sessionManager.getIsProfileAcivated()) {
            enableOnlineOffline(true);
        } else if (!sessionManager.getIsProfileAcivated()) {
            enableOnlineOffline(false);
        } else if (sessionManager.getIsDriverDeactive()) {
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_grey);
            tvOnlineOffline.setText(getString(R.string.deactivated));
        } else if (sessionManager.getIsDriverOnline()) {
            tvOnlineOffline.setText(getResources().getString(R.string.goOffline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_green);
        }


     /*   new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionManager.setLastBooking("");
            }
        }, 10000);*/

    }

    @Override
    public void onPause() {
        super.onPause();
        AppController.getInstance().stopNewBookingRingtoneService();

        getActivity().unregisterReceiver(receiver);
        VariableConstant.IS_MYBOOKING_OPENED = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BookingFragmentInteraction) {
            mListener = (BookingFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        presenter.detach();
        super.onDestroyView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvOnlineOffline:
                if (sessionManager.getIsDriverOnline()) {
                    progressDialog.setMessage(getString(R.string.goingOffline));
                    presenter.updateProviderStatues(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), VariableConstant.OFFLINE_STATUS, sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                    AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.GoesOffline.value);
                    AppController.getInstance().getMixpanelHelper().offline();
                } else {
                    progressDialog.setMessage(getString(R.string.goingOnline));
                    presenter.updateProviderStatues(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), VariableConstant.ONLINE_STATUS, sessionManager.getCurrentLat(), sessionManager.getCurrentLng());
                    AppController.getInstance().getMixpanelHelper().commonTrackAfterLogin(MixpanelEvents.GoesOnline.value);
                    AppController.getInstance().getMixpanelHelper().online();
                }
                break;


            case R.id.rlNotification:
                Intent intentNotification = new Intent(getActivity(), NotificationActivity.class);
                startActivity(intentNotification);
                getActivity().overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                tvNotificationCount.setVisibility(View.GONE);
                break;

            case R.id.flWallet:
                Intent intentWallet = new Intent(getActivity(), WalletActivity.class);
                intentWallet.putExtra("isFromBookingFrag", true);
                startActivity(intentWallet);
                getActivity().overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
                break;
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        if (activeBidFragment != null)
            activeBidFragment.stopRefreshing();
        if (offersFragment != null)
            offersFragment.stopRefreshing();
        progressDialog.dismiss();
    }


    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, getActivity());
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        //Toast.makeText(getActivity(),getActivity().getString(R.string.serverError),Toast.LENGTH_SHORT).show();
        AppController.toast();
    }

    @Override
    public void onSuccesBooking(String result, boolean isFromApi) {

        try {
            BookingPojo bookingPojo = gson.fromJson(result, BookingPojo.class);
            BookingData bookingData = bookingPojo.getData();
            if (!sessionManager.getMyBooking().equals(result)) {
                sessionManager.setMyBooking(result);
            }
            if (!(sessionManager.getIsBidBooking() == bookingData.isBid())) {
                sessionManager.setIsBidBooking(bookingData.isBid());
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.refreshBookingFragment(1);
            } else {
                if (isAdded()) {


                    offersBookings.clear();
                    activeBidBooking.clear();
                    offersBookings.addAll(bookingData.getRequest());
                    offersFragment.notifyiDataChanged(offersBookings);
                    if (sessionManager.getIsBidBooking()) {
                        activeBidBooking.addAll(bookingData.getActiveBid());
                        activeBidFragment.notifyiDataChanged(activeBidBooking);
                        tvCountFirstTab.setText("" + offersBookings.size());
                        tvCountSecondTab.setText("" + activeBidBooking.size());
                    }

                    if (!bookingData.getNotificationUnreadCount().equals("") && !bookingData.getNotificationUnreadCount().equals("0")) {
                        tvNotificationCount.setVisibility(View.VISIBLE);
                        tvNotificationCount.setText(bookingData.getNotificationUnreadCount());
                    } else {
                        tvNotificationCount.setVisibility(View.GONE);
                    }

                    if (!bookingData.getWalletAmount().equals("") && !bookingData.getWalletAmount().equals("0")) {
                        tvWalletBalance.setVisibility(View.VISIBLE);
                        String price = Utility.getFormattedPrice(bookingData.getWalletAmount());
                        sessionManager.setWalletAmount(price);
                        price = price.substring(0, Math.min(price.length(), 9));
                        tvWalletBalance.setText(price);
                    } else {
                        tvWalletBalance.setVisibility(View.GONE);
                    }

                    if (bookingData.getAccepted().size() > 0) {
                        sessionManager.setIsDriverOnJob(true);
                    } else {
                        sessionManager.setIsDriverOnJob(false);
                    }

                    acceptedBookingSize = bookingData.getAccepted().size();

                  /*  if (isFromApi && offersBookings.size() == 0 ) {
                        AppController.getInstance().stopNewBookingRingtoneService();
                    }*/

                    if (isFromApi) {
                        if (bookingData.getStatus().equals("3")) {
                            setOnOffTheJob(true);
                        } else {
                            setOnOffTheJob(false);
                        }

                        if (bookingData.getProfileStatus().equals("1")) {
                            sessionManager.setIsDriverDeactive(false);
                        } else {
                            sessionManager.setIsDriverDeactive(true);
                            setOnOffTheJob(false);
                            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_grey);
                            tvOnlineOffline.setText(getString(R.string.deactivated));
                        }

                        if (bookingData.getProfileActivationStatus().equals("1")) {
                            enableOnlineOffline(true);
                        } else {
                            enableOnlineOffline(false);
                        }

                    }

                    vpBooking.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (VariableConstant.IS_BID_BOOKING_ACCEPTED) {
                                VariableConstant.IS_BID_BOOKING_ACCEPTED = false;
                                vpBooking.setCurrentItem(1);
                            }
                        }
                    }, 350);

                    StringBuilder bookingStr = new StringBuilder();
                    String prefix = "";
                    for (Booking booking : bookingData.getAccepted()) {
                        bookingStr.append(prefix);
                        prefix = ",";
                        bookingStr.append(booking.getBookingId()).append("|").append(booking.getStatus());
                    }
                    sessionManager.setBookingStr(bookingStr.toString());
                }
            }

            if (isnewBooking || sessionManager.getIsNewBookingFromMain()) {
                isnewBooking = false;
                Booking lastBooking = new Booking();
                JSONObject jsonObjectBooking = new JSONObject(sessionManager.getLastBooking());
                lastBooking.setBookingId(jsonObjectBooking.getString("bookingId"));
                if (bookingData.getAccepted().size() > 0) {
                    ArrayList<Booking> accepted = bookingData.getAccepted();
                    int index = accepted.indexOf(lastBooking);

                    //get the calltype of the booking and if calltype 2 or 3 redirect to second tab
                    if (index >= 0 && accepted.get(index).getCallType().equals("3") || accepted.get(index).getCallType().equals("1")) {
                        sessionManager.setIncmgCallBookingType(jsonObjectBooking.getString("callType"));
                        MainActivity mainActivity = (MainActivity) getActivity();
                        mainActivity.refreshBookingFragment(2);

                    }
                    {

                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSuccessUpdateProviderStatus(String msg) {
        //Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        setOnOffTheJob(!sessionManager.getIsDriverOnline());
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    private void setOnOffTheJob(boolean b) {
        Log.d(TAG, "setOnOffTheJob: " + b);
        if (b) {
            sessionManager.setIsDriverOnline(true);
            tvOnlineOffline.setText(getResources().getString(R.string.goOffline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_green);

            if (!Utility.isMyServiceRunning(getActivity(), LocationPublishService.class)) {
                Intent startIntent = new Intent(getActivity(), LocationPublishService.class);
                startIntent.setAction(VariableConstant.ACTION.STARTFOREGROUND_ACTION);
                getActivity().startService(startIntent);
            }

            if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
                AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getPhoneNumber(),false);
            }

            if (Utility.isMyServiceRunning(getActivity(), OfflineLocationPublishService.class)) {
                Intent stopIntent = new Intent(getActivity(), OfflineLocationPublishService.class);
                stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
                getActivity().startService(stopIntent);
            }
        } else {
            sessionManager.setIsDriverOnline(false);
            tvOnlineOffline.setText(getResources().getString(R.string.goOnline));
            tvOnlineOffline.setBackgroundResource(R.drawable.rectangle_red);

            if (Utility.isMyServiceRunning(getActivity(), LocationPublishService.class)) {
                Intent stopIntent = new Intent(getActivity(), LocationPublishService.class);
                stopIntent.setAction(VariableConstant.ACTION.STOPFOREGROUND_ACTION);
                getActivity().startService(stopIntent);
            }

            if (acceptedBookingSize > 0) {
                if (!Utility.isMyServiceRunning(getActivity(), OfflineLocationPublishService.class)) {
                    Intent startIntent = new Intent(getActivity(), OfflineLocationPublishService.class);
                    startIntent.setAction(VariableConstant.ACTION.STARTOFFLINELOCATIONSERVICE);
                    getActivity().startService(startIntent);
                }
                if (!AppController.getInstance().getMqttHelper().isMqttConnected()) {
                    AppController.getInstance().getMqttHelper().createMQttConnection(sessionManager.getPhoneNumber(),false);
                }
            } else {
                if (AppController.getInstance().getMqttHelper().isMqttConnected()) {
                    AppController.getInstance().getMqttHelper().disconnect(/*sessionManager.getPhoneNumber()*/);
                }
                if (Utility.isMyServiceRunning(getActivity(), OfflineLocationPublishService.class)) {
                    Intent stopIntent = new Intent(getActivity(), OfflineLocationPublishService.class);
                    stopIntent.setAction(VariableConstant.ACTION.STOPOFFLINELOCATIONSERVICE);
                    getActivity().startService(stopIntent);
                }
            }


        }
    }

    private void enableOnlineOffline(boolean b) {
        if (b) {
            sessionManager.setIsProfileAcivated(true);
            tvOnlineOffline.setOnClickListener(this);
            tvStatus.setVisibility(View.GONE);
        } else {
            sessionManager.setIsProfileAcivated(false);
            tvOnlineOffline.setOnClickListener(null);
            tvStatus.setText(getResources().getString(R.string.profileUnderReview));
            tvStatus.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh: " + AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        presenter.getBooking(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), true);
    }

    public interface BookingFragmentInteraction {
        void setAcceptedBookingFragment();
    }
}
