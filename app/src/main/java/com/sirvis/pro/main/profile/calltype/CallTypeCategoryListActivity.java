package com.sirvis.pro.main.profile.calltype;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.CallTypeCategoryAdapter;
import com.sirvis.pro.adapters.MyDocumentCategoryAdapter;
import com.sirvis.pro.pojo.profile.calltype.CallTypeData;
import com.sirvis.pro.pojo.profile.document.ProfileDocumentCategory;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CallTypeCategoryListActivity extends AppCompatActivity implements CallTypeCategoryListPresenter.View {

    private static final String TAG = "MyDocumentActivity";
    private CallTypeCategoryAdapter callTypeCategoryAdapter;
    private ArrayList<CallTypeData> callTypeData;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private CallTypeCategoryListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        init();
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.selectCategory));
        Typeface fontBold = Utility.getFontBold(this);
        tvTitle.setTypeface(fontBold);

        callTypeData = new ArrayList<>();
        RecyclerView selectCategory = findViewById(R.id.recycler_select_category);
        selectCategory.setLayoutManager(new LinearLayoutManager(this));
        callTypeCategoryAdapter = new CallTypeCategoryAdapter(this, callTypeData);
        selectCategory.setAdapter(callTypeCategoryAdapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingCategory));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new CallTypeCategoryListPresenter(this);
        presenter.getCallType(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(VariableConstant.IS_DOCUMENT_UPDATED)
        {
            VariableConstant.IS_DOCUMENT_UPDATED = false;
            presenter.getCallType(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccessGetCallType(ArrayList<CallTypeData> callTypeData) {

        this.callTypeData.clear();
        Collections.sort(callTypeData, new Comparator<CallTypeData>() {
            @Override
            public int compare(CallTypeData o1, CallTypeData o2) {
                return o1.getCategoryName().compareTo(o2.getCategoryName());
            }
        });

        this.callTypeData.addAll(callTypeData);
        callTypeCategoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorUpdateCallType() {

    }

    @Override
    public void onSuccessUpdateCallType(String msg) {

    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == VariableConstant.REQUEST_CODE_SERVICE_CHANGED)
        {
            int position = data.getIntExtra("position", 0);
            CallTypeData callTypeData = (CallTypeData) data.getSerializableExtra("category");
            this.callTypeData.set(position, callTypeData);
            callTypeCategoryAdapter.notifyDataSetChanged();
        }
    }
}
