package com.sirvis.pro.main.acceptedbooking;

import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by murashid on 12-Apr-18.
 */

public class AcceptedBookingPresenter {

    private static final String TAG = "BidBooking";

    private View view;
    private boolean isFragmentAttached = false;

    AcceptedBookingPresenter(View view)
    {
        this.view = view;
        isFragmentAttached = true;
    }


    void detach()
    {
        isFragmentAttached = false;
    }

    /**
     * <h2>getWalletDetails</h2>
     * <p>
     * api call to get wallet setting details
     * </p>
     */
    /**
     * method for calling api for geting the Category details
     * @param sessionToken session Token
     */
    void getBookingByDate(final String sessionToken, final String startDate, final String endDate, final boolean isBackground)
    {
        if(!isBackground)
        {
            view.startProgressBar();
        }
        Log.d(TAG, "getBookingByDate: "+startDate +" "+endDate);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.BOOKINGS +"/"+startDate+"/"+endDate, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        if(!isFragmentAttached)
                        {
                            return;
                        }
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccesBooking(result,true);
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                getBookingByDate(newToken, startDate, endDate,isBackground);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        if(!isFragmentAttached)
                        {
                            return;
                        }
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    /**
     * BookingPresenterImple interface for view implementation
     */
    public interface View {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccesBooking(String result,boolean isFromApi);
        void onNewToken(String token);
        void sessionExpired(String msg);
    }

}
