package com.sirvis.pro.main.profile.mylist;


import android.app.Dialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.sirvis.pro.R;

import java.io.File;

/**
 * Created by murashid on 08-Mar-18.
 */

public class ImageDialogFragment extends DialogFragment {

    private String imageUrl ="";
    private boolean isfile= false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ImageDialogFragment.
     */
    public static ImageDialogFragment newInstance() {
        return new ImageDialogFragment();
    }

    public void setImageUrl(String imageUrl, boolean isfile) {
        this.imageUrl = imageUrl;
        this.isfile = isfile;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alert_dialog_image_view, container, false);
        final ImageView ivZoom = view.findViewById(R.id.ivZoom);
        final ProgressBar pgImageLoading = view.findViewById(R.id.pgImageLoading);
        if(!imageUrl.equals(""))
        {
            if(isfile)
            {
                Glide.with(this).setDefaultRequestOptions(new RequestOptions().error(R.drawable.upload_take_photo_icon)
                        .placeholder(R.drawable.upload_take_photo_icon)).
                        load(Uri.fromFile(new File(imageUrl)))

                        .into(ivZoom);
            }
            else
            {
                pgImageLoading.setVisibility(View.VISIBLE);
                Glide.with(this).setDefaultRequestOptions(new RequestOptions() .placeholder(R.drawable.upload_take_photo_icon)
                        .error(R.drawable.upload_take_photo_icon))
                        .asBitmap() .load(imageUrl)
                        .into(new BitmapImageViewTarget(ivZoom) {
                            @Override
                            public void onResourceReady(Bitmap drawable, Transition anim) {
                                pgImageLoading.setVisibility(View.GONE);
                                super.onResourceReady(drawable, anim);
                                ivZoom.setImageBitmap(drawable);
                            }
                        });
            }
        }
        return view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // The only reason you might override this method when using onCreateView() is
        // to modify any dialog characteristics. For example, the dialog includes a
        // title by default, but your custom layout might not need it. So here you can
        // remove the dialog title, but you must call the superclass to get the Dialog.
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }
}
