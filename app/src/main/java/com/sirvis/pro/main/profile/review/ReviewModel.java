package com.sirvis.pro.main.profile.review;

import android.util.Log;

import com.google.gson.Gson;
import com.sirvis.pro.pojo.profile.review.ReviewData;
import com.sirvis.pro.pojo.profile.review.ReviewPojo;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

/**
 * Created by murashid on 11-Sep-17.
 * <h1>ReviewModel</h1>
 * Model for ReviewActivity
 * @see ReviewActivity
 */

public class ReviewModel {

    private static final String TAG = "ReviewModel";
    private ReviewModelImple modelImplement;

    ReviewModel(ReviewModelImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calinng api for geting reviews based on the page index
     * @param sessiontoken session Token
     * @param pageNo page index
     */
    void getReview(String sessiontoken,int pageNo)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNo",pageNo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessiontoken, ServiceUrl.GET_REVIEW_RATING +"/"+pageNo, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                        Gson gson = new Gson();
                        ReviewPojo reviewPojo = gson.fromJson(result,ReviewPojo.class);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            modelImplement.onSuccess(reviewPojo.getData());
                        } else {
                            modelImplement.onFailure(reviewPojo.getMessage());
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                }
                catch (Exception e)
                {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * ReviewModelImple interface for Presenter implementation
     */
    interface ReviewModelImple {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(ReviewData reviewData);
    }
}
