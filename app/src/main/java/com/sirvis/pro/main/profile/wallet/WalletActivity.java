package com.sirvis.pro.main.profile.wallet;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.main.profile.bank.BankDetailsActivity;
import com.sirvis.pro.pojo.profile.wallet.CardData;
import com.sirvis.pro.pojo.profile.wallet.WalletData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DecimalDigitsInputFilter;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.util.ArrayList;


public class WalletActivity extends AppCompatActivity implements View.OnClickListener, WalletPresenter.View {

    private TextView tvWalletBalance, tvSoftLimitValue, tvHardLimitValue, tvCardNo, tvCurrencySymbol, tvCurrencySymbolSuffix;
    private EditText etPayAmountValue;

    private ProgressDialog progressDialog;
    private WalletPresenter presenter;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        init();
    }

    /**
     * <h2>initViews</h2>
     * <p>
     * custom method to load top views of the screen
     * </P>
     */
    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        presenter = new WalletPresenter(this);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.wallet));
        tvTitle.setTypeface(fontBold);

        TextView tvCurCreditLabel = (TextView) findViewById(R.id.tvCurCreditLabel);
        tvWalletBalance = (TextView) findViewById(R.id.tvWalletBalance);
        TextView tvSoftLimitLabel = (TextView) findViewById(R.id.tvSoftLimitLabel);
        tvSoftLimitValue = (TextView) findViewById(R.id.tvSoftLimitValue);
        TextView tvHardLimitLabel = (TextView) findViewById(R.id.tvHardLimitLabel);
        tvHardLimitValue = (TextView) findViewById(R.id.tvHardLimitValue);
        tvCurrencySymbol = (TextView) findViewById(R.id.tvCurrencySymbol);
        tvCurrencySymbolSuffix = (TextView) findViewById(R.id.tvCurrencySymbolSuffix);
        TextView tvPayUsingCardLabel = (TextView) findViewById(R.id.tvPayUsingCardLabel);
        TextView tvPayAmountLabel = (TextView) findViewById(R.id.tvPayAmountLabel);
        TextView tvSoftLimitMsg = (TextView) findViewById(R.id.tvSoftLimitMsg);
        TextView tvHardLimitMsg = (TextView) findViewById(R.id.tvHardLimitMsg);
        TextView tvConfirm = findViewById(R.id.tvConfirm);

        Button btnRecentTransactions = (Button) findViewById(R.id.btnRecentTransactions);
        etPayAmountValue = (EditText) findViewById(R.id.etPayAmountValue);
        etPayAmountValue.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(15, 2)});

        tvCurCreditLabel.setTypeface(fontMedium);
        tvWalletBalance.setTypeface(fontMedium);
        tvSoftLimitLabel.setTypeface(fontRegular);
        tvSoftLimitValue.setTypeface(fontRegular);
        tvHardLimitLabel.setTypeface(fontRegular);
        tvHardLimitValue.setTypeface(fontRegular);
        tvPayUsingCardLabel.setTypeface(fontMedium);
        tvCardNo = (TextView) findViewById(R.id.tvCardNo);
        tvCardNo.setTypeface(fontRegular);
        tvCardNo.setOnClickListener(this);
        tvPayAmountLabel.setTypeface(fontMedium);
        tvCurrencySymbol.setTypeface(fontMedium);
        tvCurrencySymbolSuffix.setTypeface(fontMedium);
        tvSoftLimitMsg.setTypeface(fontRegular);
        tvHardLimitMsg.setTypeface(fontRegular);
        tvConfirm.setTypeface(fontMedium);
        btnRecentTransactions.setTypeface(fontMedium);
        etPayAmountValue.setTypeface(fontMedium);


        btnRecentTransactions.setOnClickListener(this);
        tvConfirm.setOnClickListener(this);

        etPayAmountValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && !etPayAmountValue.getText().toString().equals("")) {
                    etPayAmountValue.setText(Utility.getFormattedPrice(etPayAmountValue.getText().toString()));
                }
            }
        });

        presenter.getWalletDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
    }
    //====================================================================


    @Override
    protected void onResume() {
        super.onResume();

        if (!sessionManager.getSelectedCardNumber().equals("")) {
            tvCardNo.setText(getString(R.string.cardEndingNumber) + " " + sessionManager.getSelectedCardNumber());
            tvCardNo.setCompoundDrawablesWithIntrinsicBounds(Utility.getCardBrand(sessionManager.getCardBrand()), 0, R.drawable.vector_color_primary_next_button, 0);
        }
    }

    @Override
    public void onClick(View view) {
        Utility.hideKeyboad(this);
        etPayAmountValue.clearFocus();

        switch (view.getId()) {
            case R.id.btnRecentTransactions:
                Intent intent = new Intent(this, WalletTransactionActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;

            case R.id.tvCardNo:
                Intent cardsIntent = new Intent(this, CardListActivity.class);
                startActivityForResult(cardsIntent, 1);
                overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;

            case R.id.tvConfirm:
                //presenter.validate(sessionManager.getSelectedCardId(), etPayAmountValue.getText().toString());
                Intent bankIntent = new Intent(this, BankDetailsActivity.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(bankIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(bankIntent);
                    this.overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }
                break;
        }
    }
    //====================================================================


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (getIntent().getBooleanExtra("isFromBookingFrag", false)) {
            finish();
            overridePendingTransition(R.anim.stay, R.anim.top_to_bottom);
        } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccessWalletDetails(WalletData walletData) {
        if (walletData.isReachedHardLimit()) {
            tvWalletBalance.setTextColor(getResources().getColor(R.color.customRed));
        } else if (walletData.isReachedSoftLimit()) {
            tvWalletBalance.setTextColor(getResources().getColor(R.color.yellow));
        } else {
            tvWalletBalance.setTextColor(getResources().getColor(R.color.black));
        }

       /* if (Double.valueOf(walletData.getWalletAmount())>0){
            tvalertMsg.setText(getString(R.string.positive_wallet_alert)+getString(R.string.app_name)+sessionManager.getCurrencySymbol()+walletDataPojo.getWalletAmount());
        }else if (Double.valueOf(walletDataPojo.getWalletAmount())<0){
            tvalertMsg.setText(getString(R.string.app_name)+getString(R.string.app_name)+sessionManager.getCurrencySymbol()+walletDataPojo.getWalletAmount().substring(1));
        }*/


        tvWalletBalance.setText(Utility.getPrice(walletData.getWalletAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvSoftLimitValue.setText(Utility.getPrice(String.valueOf(walletData.getSoftLimit()), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvHardLimitValue.setText(Utility.getPrice(String.valueOf(walletData.getHardLimit()), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
        tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());

        sessionManager.setWalletAmount(walletData.getWalletAmount());

        if (sessionManager.getCurrencyAbbrevation().equals("2")) {
            tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
            tvCurrencySymbol.setVisibility(View.GONE);
        }
        if (walletData.getDefaultCard() != null) {
            WalletData.DefaultCard defaultCard = walletData.getDefaultCard();
            if ((defaultCard.getId() != null && (!defaultCard.getId().trim().isEmpty())) &&
                    (defaultCard.getBrand() != null && (!defaultCard.getBrand().trim().isEmpty())) &&
                    (defaultCard.getLast4() != null && (!defaultCard.getLast4().trim().isEmpty()))) {
                sessionManager.setSelectedCardId(defaultCard.getId());
                sessionManager.setCardBrand(defaultCard.getBrand());
                sessionManager.setSelectedCardNumber(defaultCard.getLast4());

                tvCardNo.setText(getString(R.string.cardEndingNumber) + " " + sessionManager.getSelectedCardNumber());
                tvCardNo.setCompoundDrawablesWithIntrinsicBounds(Utility.getCardBrand(sessionManager.getCardBrand()), 0, R.drawable.vector_color_primary_next_button, 0);

            }
        }
        /*if(sessionManager.getSelectedCardNumber().equals(""))
        {
            presenter.getCardDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        }*/
    }

    @Override
    public void onSuccessWalletRecharge(String msg) {

        etPayAmountValue.setText("");
        presenter.getWalletDetails(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()));
        DialogHelper.customAlertDialog(this, getString(R.string.message), msg, getString(R.string.oK));
        VariableConstant.IS_WALLET_UPDATED = true;
    }

    @Override
    public void onSuccessCardDetails(ArrayList<CardData> data) {

        if (data.size() > 0) {
            for (CardData cardData : data) {
                if (cardData.isDefault()) {
                    sessionManager.setSelectedCardId(cardData.getId());
                    sessionManager.setSelectedCardNumber(cardData.getLast4());
                    sessionManager.setCardBrand(cardData.getBrand());

                    tvCardNo.setText(getString(R.string.cardEndingNumber) + " " + sessionManager.getSelectedCardNumber());
                    tvCardNo.setCompoundDrawablesWithIntrinsicBounds(Utility.getCardBrand(sessionManager.getCardBrand()), 0, R.drawable.vector_color_primary_next_button, 0);

                }
            }
        } else {
            tvCardNo.setText(getString(R.string.addcard));
        }

    }

    @Override
    public void onErrorCardSelection() {
        Toast.makeText(this, getString(R.string.plsSelectCard), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onErrorAmount() {
        Toast.makeText(this, getString(R.string.plsEnterPrice), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successValidation() {
        String amt = "";
        if (sessionManager.getCurrencyAbbrevation().equals("1")) {
            amt = sessionManager.getCurrencySymbol() + " " + etPayAmountValue.getText().toString();
        } else {
            amt = etPayAmountValue.getText().toString() + " " + sessionManager.getCurrencySymbol();
        }

        showRechargeConfirmationAlert(amt);
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    /**
     * <h2>showRechargeConfirmationAlert</h2>
     * <p>
     * method to show an alert dialog to take user
     * confirmation to proceed to recharge
     * </p>
     */
    private void showRechargeConfirmationAlert(final String amount) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getString(R.string.confirmPayment));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getString(R.string.paymentMsg1)
                + " " + getString(R.string.app_name) + " " + getString(R.string.paymentMsg2) + " " + amount);

        alertDialog.setPositiveButton(getString(
                R.string.confirm), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                try {
                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("cardId", sessionManager.getSelectedCardId());
                    jsonObject.put("amount", etPayAmountValue.getText().toString());

                    presenter.rechargeWallet(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }
    //====================================================================

}
