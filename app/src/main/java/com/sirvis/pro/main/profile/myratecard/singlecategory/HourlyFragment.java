package com.sirvis.pro.main.profile.myratecard.singlecategory;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.main.profile.myratecard.EditCategoryServicePresenter;
import com.sirvis.pro.pojo.profile.ratecard.CategoryData;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DecimalDigitsInputFilter;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class HourlyFragment extends Fragment implements SeekBar.OnSeekBarChangeListener, View.OnFocusChangeListener, EditCategoryServicePresenter.View, View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public CategoryData categoryData;
    private boolean isPriceSetByProvider = false;

    private View viewFirst, viewSecond, viewThird, viewFourth, viewFifth;
    private int r1,r2,r3,r4,r5,r6, minValue=0, maxValue=0;

    private  EditText etPrice;
    private ImageView ivEdit;
    private  SeekBar seekBarPrice;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private EditCategoryServicePresenter presenter;
    public boolean isUpdated=false;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param isRequest Parameter 1.
     * @return A new instance of fragment MyEventsListFragment.
     */
    public static HourlyFragment newInstance(CategoryData categoryData, boolean isPriceSetByProvider) {
        HourlyFragment fragment = new HourlyFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, categoryData);
        args.putBoolean(ARG_PARAM2, isPriceSetByProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryData = (CategoryData) getArguments().getSerializable(ARG_PARAM1);
            isPriceSetByProvider =  getArguments().getBoolean(ARG_PARAM2);
            if(isPriceSetByProvider)
            {
                setHasOptionsMenu(true);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_hourly, container, false);
        init(view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_menu_hourly_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case  R.id.menu_save:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("service_category_Id",categoryData.get_id());
                    jsonObject.put("categoryType","1");
                    jsonObject.put("price",""+etPrice.getText().toString());
                    presenter.updateCategoryService(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject, minValue, maxValue);

                    etPrice.clearFocus();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
        }
        return true;
    }


    private void init(View view)
    {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(getActivity());
        presenter = new EditCategoryServicePresenter(this);

        Typeface fontMedium = Utility.getFontMedium(getContext());
        Typeface fontRegular = Utility.getFontRegular(getContext());

        TextView tvHourlyRate = view.findViewById(R.id.tvHourlyRate);
        TextView tvPriceRange = view.findViewById(R.id.tvPriceRange);
        TextView tvMinValueLabel = view.findViewById(R.id.tvMinValueLabel);
        TextView tvMinValue = view.findViewById(R.id.tvMinValue);
        TextView tvMaxValueLabel = view.findViewById(R.id.tvMaxValueLabel);
        TextView tvMaxValue = view.findViewById(R.id.tvMaxValue);
        TextView tvCurrencySymbol = view.findViewById(R.id.tvCurrencySymbol);
        TextView tvCurrencySymbolSuffix = view.findViewById(R.id.tvCurrencySymbolSuffix);
        RelativeLayout rlSeekbarRange = view.findViewById(R.id.rlSeekbarRange);
        etPrice = view.findViewById(R.id.etPrice);
        etPrice.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(15,2)});
        ivEdit = view.findViewById(R.id.ivEdit);

        tvHourlyRate.setTypeface(fontMedium);
        tvPriceRange.setTypeface(fontMedium);
        tvMinValueLabel.setTypeface(fontRegular);
        tvMinValue.setTypeface(fontRegular);
        tvMaxValueLabel.setTypeface(fontRegular);
        tvMaxValue.setTypeface(fontRegular);
        tvCurrencySymbol.setTypeface(fontMedium);
        tvCurrencySymbolSuffix.setTypeface(fontMedium);
        etPrice.setTypeface(fontMedium);

        seekBarPrice = view.findViewById(R.id.seekBarPrice);
        viewFirst =  view.findViewById(R.id.viewFirst);
        viewSecond =  view.findViewById(R.id.viewSecond);
        viewThird =  view.findViewById(R.id.viewThird);
        viewFourth =  view.findViewById(R.id.viewFourth);
        viewFifth =  view.findViewById(R.id.viewFifth);

        tvCurrencySymbol.setText(sessionManager.getCurrencySymbol());
        tvCurrencySymbolSuffix.setText(sessionManager.getCurrencySymbol());

        if(sessionManager.getCurrencyAbbrevation().equals("2"))
        {
            tvCurrencySymbolSuffix.setVisibility(View.VISIBLE);
            tvCurrencySymbol.setVisibility(View.GONE);
        }

        editEnable(isPriceSetByProvider);
        etPrice.setText(Utility.getFormattedPrice(categoryData.getPrice_per_fees()));

        if(!isPriceSetByProvider)
        {
            tvHourlyRate.setText(getString(R.string.hourlyRate));
            rlSeekbarRange.setVisibility(View.GONE);
            tvPriceRange.setVisibility(View.GONE);
            ivEdit.setVisibility(View.GONE);
        }
        else
        {
            ivEdit.setVisibility(View.VISIBLE);
            minValue = (int)Double.parseDouble(categoryData.getMinimum_fees());
            maxValue = (int)Double.parseDouble(categoryData.getMiximum_fees());

            calculateCenterPointsValue();

            tvMinValue.setText(""+minValue);
            tvMaxValue.setText(""+maxValue);

            seekBarPrice.setMax(maxValue - minValue);
            seekBarPrice.setProgress((int)Double.parseDouble(categoryData.getPrice_per_fees()) - minValue);
            setProgress((int)Double.parseDouble(categoryData.getPrice_per_fees()));
            
            seekBarPrice.setOnSeekBarChangeListener(this);
            etPrice.setOnFocusChangeListener(this);

            ivEdit.setOnClickListener(this);

        }
    }

    private void editEnable(boolean isPriceSetByProvider) {
        seekBarPrice.setEnabled(isPriceSetByProvider);
        //etPrice.setEnabled(isPriceSetByProvider);
    }

    private void calculateCenterPointsValue() {
        double commonValue =(double)(maxValue - minValue) / 5 ;
        r1 = minValue ;
        double r2 =  r1 + commonValue ;
        double r3 =  r2 + commonValue;
        double r4 =  r3 + commonValue ;
        double r5 =  r4 + commonValue ;
        this.r2 = (int) r2;
        this.r3 = (int) r3;
        this.r4 = (int) r4;
        this.r5 = (int) r5;
        r6 =  maxValue;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        etPrice.setText(Utility.getFormattedPrice(String.valueOf(seekBar.getProgress()+minValue)));
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setProgress(seekBar.getProgress()+minValue);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(!b)
        {
            etPrice.setText(Utility.getFormattedPrice(etPrice.getText().toString()));
        }
        else
        {
            etPrice.setSelection(etPrice.getText().length());
        }
    }

    private void setProgress(int progress) {
        if(r6 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        }
        else if(r5 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider);
        }
        else if(r4 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider);
        }
        else if(r3 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
        }
        else if(r2 <= progress)
        {
            setViewColors(R.color.colorPrimary, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
        }
        else if(r1 <= progress)
        {
            setViewColors(R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider, R.color.lightDivider);
        }
    }

    /**
     * Change the color of the views in sentingValue
     * @param v1 view of the 1
     * @param v2 view of the 2
     * @param v3 view of the 3
     * @param v4 view of the 4
     * @param v5 view of thes 5
     */
    private  void setViewColors(int v1, int v2, int v3, int v4, int v5)
    {
        viewFirst.setBackgroundColor(ContextCompat.getColor(getContext(),v1));
        viewSecond.setBackgroundColor(ContextCompat.getColor(getContext(),v2));
        viewThird.setBackgroundColor(ContextCompat.getColor(getContext(),v3));
        viewFourth.setBackgroundColor(ContextCompat.getColor(getContext(),v4));
        viewFifth.setBackgroundColor(ContextCompat.getColor(getContext(),v5));
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void invalidPrice() {

        String msg = "";
        if(sessionManager.getCurrencyAbbrevation().equals("1"))
        {
            msg =  getString(R.string.invalidPrice)+" "+ sessionManager.getCurrencySymbol()+" "+ minValue +" "+ getString(R.string.and)+
                    " "+sessionManager.getCurrencySymbol()+" "+maxValue;
        }
        else
        {
            msg =  getString(R.string.invalidPrice)+" "+  minValue+" "+ sessionManager.getCurrencySymbol()+" "+ getString(R.string.and)
                    +" "+maxValue + " "+sessionManager.getCurrencySymbol();
        }

        DialogHelper.customAlertDialog(getActivity(),
                getString(R.string.alert),
                msg,
                getString(R.string.oK));
    }

    @Override
    public void invalidTime() {

    }

    @Override
    public void invalidDesc() {

    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(String msg) {
        seekBarPrice.setProgress((int)Double.parseDouble(etPrice.getText().toString()) - minValue);
        isUpdated=true;
        categoryData.setPrice_per_fees(etPrice.getText().toString());
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();

        etPrice.setEnabled(false);
        Utility.hideKeyboad(getActivity());
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,getActivity());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.ivEdit:
                if(!etPrice.isEnabled())
                {
                    etPrice.setEnabled(true);
                    Utility.showKeyBoard(getActivity(),etPrice);
                }
                else
                {
                    etPrice.clearFocus();
                    etPrice.setEnabled(false);
                    Utility.hideKeyboad(getActivity());
                }
                break;
        }
    }
}
