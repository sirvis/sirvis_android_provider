package com.sirvis.pro.main.profile.wallet;

import android.content.Context;
import android.util.Log;

import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.RefreshToken;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.VariableConstant;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by murashid on 28-Mar-18.
 */

public class AddCardPresenter {
    private static final String TAG = "AddCardPresenter";

    private View view;

    AddCardPresenter(View view)
    {
        this.view = view;
    }
/*

    void getStripeToken(Context context, Card card, String stripeKey, final String sessionToken, final String email)
    {
        try {
            if(card != null)
            {
                view.startProgressBar();
                new Stripe(context).createToken(card, stripeKey, new TokenCallback() {
                    @Override
                    public void onError(Exception error)
                    {
                        view.stopProgressBar();
                        view.onFailure(error.getMessage());
                    }
                    @Override
                    public void onSuccess(Token token)
                    {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("cardToken",token.getId());
                            jsonObject.put("email",email);
                            addCard(sessionToken, jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            else
            {
                view.invalidCard();
            }
        }
        catch (Exception e)
        {
            view.stopProgressBar();
            view.onFailure();
            e.printStackTrace();
        }

    }
*/

    void addCard(final String sessionToken, final JSONObject jsonObjectRequest)
    {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.CARD, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObjectRequest
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode,String result) {
                        view.stopProgressBar();
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);
                                final JSONObject jsonObject = new JSONObject(result);
                                switch (statusCode) {
                                    case VariableConstant.RESPONSE_CODE_SUCCESS:
                                        view.onSuccess(jsonObject.getString("message"));
                                        break;

                                    case VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE:
                                        RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {
                                                addCard(newToken,jsonObjectRequest);
                                                view.onNewToken(newToken);
                                            }

                                            @Override
                                            public void onFailureRefreshToken() {
                                                view.onFailure();
                                            }

                                            @Override
                                            public void sessionExpired(String msg) {
                                                view.sessionExpired(msg);
                                            }
                                        });
                                        break;

                                    case VariableConstant.RESPONSE_CODE_INVALID_TOKEN:
                                        view.sessionExpired(jsonObject.getString("message"));
                                        break;
                                    default:
                                        view.onFailure(jsonObject.getString("message"));
                                        break;
                                }
                            } else {
                                view.onFailure();
                            }
                        }
                        catch (Exception e)
                        {
                            view.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        view.stopProgressBar();
                        view.onFailure();
                    }
                });
    }

    interface View
    {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(String msg);
        void invalidCard();
        void onNewToken(String newToken);
        void sessionExpired(String msg);
    }
}
