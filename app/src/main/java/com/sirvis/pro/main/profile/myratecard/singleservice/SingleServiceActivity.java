package com.sirvis.pro.main.profile.myratecard.singleservice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.ViewPagerAdapter;
import com.sirvis.pro.main.profile.myratecard.EditCategoryServicePresenter;
import com.sirvis.pro.pojo.profile.ratecard.CategoryService;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.DialogHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;

import org.json.JSONObject;

public class SingleServiceActivity extends AppCompatActivity implements View.OnClickListener, EditCategoryServicePresenter.View {

    private PriceFragment priceFragment;
    private DescriptionFragment descriptionFragment;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private EditCategoryServicePresenter presenter;

    private CategoryService service ;
    private int position, servicePosition, subCatPosition;
    private boolean isUpdated = false;
    private boolean isServiceUnderSubCat = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_service);
        initializer();
    }

    private void initializer() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.updating));
        progressDialog.setCancelable(false);
        sessionManager = SessionManager.getSessionManager(this);
        presenter = new EditCategoryServicePresenter(this);

        Intent intent = getIntent();
        service = (CategoryService) intent.getSerializableExtra("service");
        position = intent.getIntExtra("position",0);
        servicePosition = intent.getIntExtra("servicePosition",0);
        subCatPosition = intent.getIntExtra("subCatPosition",0);
        isServiceUnderSubCat = intent.getBooleanExtra("isServiceUnderSubCat",false);

        priceFragment = PriceFragment.newInstance(service);
        descriptionFragment = DescriptionFragment.newInstance(service.getSer_desc());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }

        TextView tvTitle = findViewById(R.id.tvTitle);
        TextView tvSave = findViewById(R.id.tvSave);
        tvTitle.setText(service.getSer_name());
        tvTitle.setTypeface(Utility.getFontBold(this));
        tvSave.setTypeface(Utility.getFontRegular(this));
        tvSave.setOnClickListener(this);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        TabLayout tablayoutMyEvents = findViewById(R.id.tablayoutNotarization);
        ViewPager viewPagerService = findViewById(R.id.viewPagerService);

        viewPagerAdapter.addFragment(priceFragment,getString(R.string.price));
        viewPagerAdapter.addFragment(descriptionFragment,getString(R.string.description));
        viewPagerService.setAdapter(viewPagerAdapter);
        tablayoutMyEvents.setupWithViewPager(viewPagerService);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.tvSave:
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("service_category_Id",service.get_id());
                    jsonObject.put("categoryType","2");
                    jsonObject.put("price",""+priceFragment.etPrice.getText().toString());
                    jsonObject.put("serviceCompletionTime",priceFragment.etServiceTime.getText().toString());
                    jsonObject.put("description",descriptionFragment.etDesc.getText().toString());

                    Log.d("mura", "onClick: "+jsonObject);
                    priceFragment.etPrice.clearFocus();
                    presenter.updateCategoryService(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),jsonObject, priceFragment.minValue, priceFragment.maxValue);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if(isUpdated)
        {
            Intent intent = new Intent();
            intent.putExtra("service",service);
            intent.putExtra("position",position);
            intent.putExtra("servicePosition",servicePosition);
            intent.putExtra("subCatPosition",subCatPosition);
            intent.putExtra("isServiceUnderSubCat",isServiceUnderSubCat);
            setResult(RESULT_OK,intent);
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void invalidPrice() {

        String msg = "";
        if(sessionManager.getCurrencyAbbrevation().equals("1"))
        {
           msg =  getString(R.string.invalidPrice)+" "+ sessionManager.getCurrencySymbol()+" "+ priceFragment.minValue +" "+ getString(R.string.and)+
            " "+sessionManager.getCurrencySymbol()+" "+priceFragment.maxValue;
        }
        else
        {
            msg =  getString(R.string.invalidPrice)+" "+  priceFragment.minValue+" "+ sessionManager.getCurrencySymbol()+" "+ getString(R.string.and)
                    +" "+priceFragment.maxValue + " "+sessionManager.getCurrencySymbol();
        }

        DialogHelper.customAlertDialog(this,
                getString(R.string.alert),
                msg ,
                getString(R.string.oK));

    }

    @Override
    public void invalidTime() {
        DialogHelper.customAlertDialog(this,
                getString(R.string.alert),
                getString(R.string.invalideMinutes),
                getString(R.string.oK));
    }

    @Override
    public void invalidDesc() {
        DialogHelper.customAlertDialog(this,
                getString(R.string.alert),
                getString(R.string.plsEnterDescription),
                getString(R.string.oK));
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        AppController.toast();
    }

    @Override
    public void onSuccess(String msg) {
        priceFragment.seekBarPrice.setProgress((int)Double.parseDouble(priceFragment.etPrice.getText().toString()) - priceFragment.minValue);

        service.setIs_unit(priceFragment.etPrice.getText().toString());
        service.setServiceCompletionTime(priceFragment.etServiceTime.getText().toString());
        service.setSer_desc(descriptionFragment.etDesc.getText().toString());
        isUpdated = true;

        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword() , token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager,this);
    }
    
}
