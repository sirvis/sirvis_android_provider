package com.sirvis.pro.main.profile.helpcenter;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sirvis.pro.R;
import com.sirvis.pro.adapters.TicketListAdapter;
import com.sirvis.pro.pojo.profile.helpcenter.Ticket;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;

public class HelpCenterActivity extends AppCompatActivity implements View.OnClickListener, HelpCenterPresenter.HelpCenterPresenterImple {


    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private HelpCenterPresenter helpCenterPresenter;

    private TicketListAdapter ticketListAdapter;
    private ArrayList<Ticket> wholeTicketData;

    private ImageView ivNoTicket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_center);

        init();
    }

    private void init()
    {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.gettingTicket));
        progressDialog.setCancelable(false);
        helpCenterPresenter = new HelpCenterPresenter(this);

        Typeface fonMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() !=null )
        {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);

        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.helpCenter));
        tvTitle.setTypeface(fontBold);


        ImageView ivCloseButton = findViewById(R.id.ivCloseButton);
        ivNoTicket = findViewById(R.id.ivNoTicket);
        ivCloseButton.setVisibility(View.VISIBLE);
        ivCloseButton.setImageResource(R.drawable.vector_color_primary_plus);
        ivCloseButton.setOnClickListener(this);


        wholeTicketData = new ArrayList<>();
        ticketListAdapter = new TicketListAdapter(this,wholeTicketData);
        RecyclerView rvTickects = findViewById(R.id.rvTickects);
        rvTickects.setLayoutManager(new LinearLayoutManager(this));
        rvTickects.setAdapter(ticketListAdapter);

        helpCenterPresenter.getAllTickets(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getEmail());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(VariableConstant.IS_TICKET_UPDATED)
        {
            VariableConstant.IS_TICKET_UPDATED = false;
            helpCenterPresenter.getAllTickets(AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()),sessionManager.getEmail());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == android.R.id.home)
        {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity()
    {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            finishAfterTransition();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.activity_open_scale,R.anim.activity_close_translate);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivCloseButton:
                Intent helpCenterIntent = new Intent(this, NewTicketActivity.class);
                helpCenterIntent.putExtra("isAlreadyRaised",false);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    startActivity(helpCenterIntent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }
                else
                {
                    startActivity(helpCenterIntent);
                    overridePendingTransition(R.anim.activity_open_translate,R.anim.activity_close_scale);
                }

                break;
        }
    }


    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,getString(R.string.serverError),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(ArrayList<Ticket> wholeTicketData) {
        this.wholeTicketData.clear();
        this.wholeTicketData.addAll(wholeTicketData);
        ticketListAdapter.notifyDataSetChanged();

        if( this.wholeTicketData.size() > 0)
        {
            ivNoTicket.setVisibility(View.GONE);
        }
        else
        {
            ivNoTicket.setVisibility(View.VISIBLE);
        }
    }

}
