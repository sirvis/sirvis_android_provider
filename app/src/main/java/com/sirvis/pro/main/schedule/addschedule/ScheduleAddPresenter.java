package com.sirvis.pro.main.schedule.addschedule;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.sirvis.pro.R;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by murashid on 02-Oct-17.
 * <h1>ScheduleAddPresenter</h1>
 * ScheduleAddPresenter presenter for ScheduleAddActivity
 * @see ScheduleAddActivity
 */

public class ScheduleAddPresenter implements ScheduleAddModel.ScheduleAddModelImple {
    private Context context;
    private ScheduleAddModel model;
    private ScheduleAddPresenterImple presenterImple;
    private int TIME_PICKER_INTERVAL;

    ScheduleAddPresenter(Context context,ScheduleAddPresenterImple presenterImple) {
        this.context = context;
        model = new ScheduleAddModel(this);
        this.presenterImple = presenterImple;
        TIME_PICKER_INTERVAL = 30;
    }

    void addSchedule(String sessionToken, JSONObject jsonObject){
        presenterImple.startProgressBar();
        model.getServerTime(sessionToken,jsonObject);
    }

    @Override
    public void onFailure(String failureMsg) {
        presenterImple.stopProgressBar();
        presenterImple.onFailure(failureMsg);
    }

    @Override
    public void onFailure() {
        presenterImple.stopProgressBar();
        presenterImple.onFailure();
    }

    @Override
    public void onSuccess(String msg) {
        presenterImple.stopProgressBar();
        presenterImple.onSuccess(msg);
    }

    @Override
    public void onScheduleError() {
        presenterImple.stopProgressBar();
        presenterImple.onScheduleError();
    }

    @Override
    public void onDaysError() {
        presenterImple.stopProgressBar();
        presenterImple.onDaysError();
    }

    @Override
    public void onDurationError() {
        presenterImple.stopProgressBar();
        presenterImple.onDurationError();
    }

    @Override
    public void onStartTimeError() {
        presenterImple.stopProgressBar();
        presenterImple.onStartTimeError();
    }

    @Override
    public void onEndTimeError() {
        presenterImple.stopProgressBar();
        presenterImple.onEndTimeError();
    }

    @Override
    public void onTimeSelectedError() {
        presenterImple.stopProgressBar();
        presenterImple.onTimeSelectedError();
    }

    @Override
    public void onSlotDurationError() {
        presenterImple.stopProgressBar();
        presenterImple.onSlotDurationError();
    }

     @Override
    public void onJobTypeError() {
        presenterImple.stopProgressBar();
        presenterImple.onJobTypeError();
    }
     @Override
    public void onLocationError() {
        presenterImple.stopProgressBar();
        presenterImple.onLocationError();
    }



    void selectMode(int mode)
    {
        String selectedMode = "" ;
        String days ="";
        ArrayList<String> sentingDays = new ArrayList<>();


        switch (mode)
        {
            case 1:
                sentingDays.add("Mon");
                sentingDays.add("Tue");
                sentingDays.add("Wed");
                sentingDays.add("Thu");
                sentingDays.add("Fri");
                sentingDays.add("Sat");
                sentingDays.add("Sun");
                selectedMode = context.getString(R.string.everyDays);

                days = context.getString(R.string.monday) +"," + context.getString(R.string.tuesday) +"," + context.getString(R.string.wednesday) +","
                        +context.getString(R.string.thursday) +"," + context.getString(R.string.friday) +","+ context.getString(R.string.saturday) +","
                        +context.getString(R.string.sunday) ;

                break;
            case 2:
                sentingDays.add("Mon");
                sentingDays.add("Tue");
                sentingDays.add("Wed");
                sentingDays.add("Thu");
                sentingDays.add("Fri");
                selectedMode = context.getString(R.string.weekDays);

                days = context.getString(R.string.monday) +"," + context.getString(R.string.tuesday) +"," + context.getString(R.string.wednesday) +","
                        +context.getString(R.string.thursday) +"," + context.getString(R.string.friday) ;

                break;

            case 3:
                sentingDays.add("Sat");
                sentingDays.add("Sun");
                selectedMode = context.getString(R.string.weekEnds);
                days =  context.getString(R.string.saturday) +"," +context.getString(R.string.sunday) ;

                break;

            case 4:
                selectedMode = context.getString(R.string.selectDaysSchedule);
                presenterImple.selectedMode(selectedMode);
                break;

            default:
                break;
        }

        if(mode != 4)
        {
            presenterImple.selectedModeConstantDays(selectedMode,days,sentingDays);
        }
    }

    void sortDays(ArrayList<String> days)
    {
        String displayDays = "";
        ArrayList<String> sentingDays = new ArrayList<>();
        StringBuilder everyDayStringBuilder = new StringBuilder("");
        String everyDayprefix = "";
        for (String weeday: days)
        {
            everyDayStringBuilder.append(everyDayprefix);
            everyDayprefix = ",";
            everyDayStringBuilder.append(weeday);
        }
        displayDays = everyDayStringBuilder.toString();

        if (days.contains(context.getString(R.string.monday)))
        {
            sentingDays.add("Mon");
        }
        if(days.contains(context.getString(R.string.tuesday)))
        {
            sentingDays.add("Tue");
        }
        if(days.contains(context.getString(R.string.wednesday)))
        {
            sentingDays.add("Wed");
        }
        if(days.contains(context.getString(R.string.thursday)))
        {
            sentingDays.add("Thu");
        }
        if(days.contains(context.getString(R.string.friday)))
        {
            sentingDays.add("Fri");
        }
        if(days.contains(context.getString(R.string.saturday)))
        {
            sentingDays.add("Sat");
        }
        if(days.contains(context.getString(R.string.sunday)))
        {
            sentingDays.add("Sun");
        }

        presenterImple.selectedDays(displayDays,sentingDays);
    }


    void selectDuration(int selectedMonth,String startDay,String endDay)
    {
        String month = "";
        switch (selectedMonth)
        {
            case 1:
                month = context.getString(R.string.thisMonth);
                break;
            case 2:
                month = context.getString(R.string.twoMonth);
                break;
            case 3:
                month = context.getString(R.string.threeMonth);
                break;
            case 4:
                month = context.getString(R.string.fourMonth);
                break;
            case 5:
                month = startDay+" to "+endDay;
                break;

            default:
                break;
        }
        presenterImple.selectedDuration(month);
    }

    void selectTime(final int timePickerDifference,String lastSelectedTime)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.custom_time_picker_dialog);

        final AlertDialog timerPickerDialog = builder.create();
        //timerPickerDialog.setCancelable(false);
        timerPickerDialog.show();
        final TimePicker timePicker = (TimePicker) timerPickerDialog.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(false);

        ImageView ivClose = (ImageView) timerPickerDialog.findViewById(R.id.ivClose);
        try {
            if(!lastSelectedTime.equals(""))
            {
                String[] time = lastSelectedTime.split(":");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    timePicker.setHour(Integer.parseInt(time[0]));
                    timePicker.setMinute(Integer.parseInt(time[1]));
                }
                else
                {
                    timePicker.setCurrentHour(Integer.parseInt(time[0]));
                    timePicker.setCurrentMinute(Integer.parseInt(time[1]));
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        setTimePickerInterval(timePicker);

        TextView tvDone = (TextView)timerPickerDialog. findViewById(R.id.tvDone);
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hours24=0,hours12 = 0,mintues = 00;
                String format = "";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    hours24 = timePicker.getHour();
                    mintues = timePicker.getMinute();
                }
                else
                {
                    hours24 = timePicker.getCurrentHour();
                    mintues = timePicker.getCurrentMinute();
                }

                mintues = mintues * TIME_PICKER_INTERVAL;

                hours12 = hours24;

                if(hours24 > 12)
                {
                    format = "PM";
                    hours12 = hours24-12;
                }
                else
                {
                    format = "AM";
                }
                if(hours24 == 0)
                {
                    hours12 = 12;
                }
                if(hours24 == 12)
                {
                    format = "PM";
                }

                if(timePickerDifference == 0)
                {
                    presenterImple.seletedStartTime(String.format("%02d",hours12)+":"+String.format("%02d",mintues)+" "+format,String.format("%02d",hours24) +":"+String.format("%02d",mintues));
                }
                else
                {
                    presenterImple.seletedEndTime(String.format("%02d",hours12)+":"+String.format("%02d",mintues)+" "+format,String.format("%02d",hours24) +":"+String.format("%02d",mintues));
                }

                timerPickerDialog.dismiss();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timerPickerDialog.dismiss();
            }
        });
    }

    private void setTimePickerInterval(TimePicker timePicker) {
        try {
            Class<?> classForid = Class.forName("com.android.internal.R$id");
            // Field timePickerField = classForid.getField("timePicker");

            Field field = classForid.getField("minute");
            NumberPicker minutePicker = (NumberPicker) timePicker
                    .findViewById(field.getInt(null));

            minutePicker.setMinValue(0);
            minutePicker.setMaxValue(1);
            ArrayList<String> displayedValues = new ArrayList<String>();
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            for (int i = 0; i < 60; i += TIME_PICKER_INTERVAL) {
                displayedValues.add(String.format("%02d", i));
            }
            minutePicker.setDisplayedValues(displayedValues
                    .toArray(new String[0]));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    interface ScheduleAddPresenterImple {
        void startProgressBar();
        void stopProgressBar();
        void onFailure(String msg);
        void onFailure();
        void onSuccess(String msg);

        void onScheduleError();
        void onDaysError();
        void onDurationError();
        void onStartTimeError();
        void onEndTimeError();
        void onTimeSelectedError();
        void onJobTypeError();
        void onSlotDurationError();
        void onLocationError();

        void selectedMode(String mode);
        void selectedModeConstantDays(String mode, String days, ArrayList<String> sentingDays);
        void selectedDays(String days, ArrayList<String> sentingDays);
        void selectedDuration(String duration);
        void seletedStartTime(String displayTime, String sentingTime);
        void seletedEndTime(String displayTime, String sentingTime);
    }
}
