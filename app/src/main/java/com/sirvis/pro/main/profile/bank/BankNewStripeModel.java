package com.sirvis.pro.main.profile.bank;

import android.os.AsyncTask;
import android.util.Log;

import com.sirvis.pro.BuildConfig;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.OkHttp3ConnectionStatusCode;
import com.sirvis.pro.utility.ServiceUrl;
import com.sirvis.pro.utility.UploadFileAmazonS3;
import com.sirvis.pro.utility.VariableConstant;

import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankNewStripeModel</h1>
 * BankNewStripeModel model for BankNewStripeActivity
 *
 * @see BankNewStripeActivity
 */

public class BankNewStripeModel {

    private static final String TAG = "BankNewStripeModel";
    private BankNewStripModelImplement bankNewStripModelImplement;
    private String imageUrl = "";

    BankNewStripeModel(BankNewStripModelImplement bankNewStripModelImplement) {
        this.bankNewStripModelImplement = bankNewStripModelImplement;
    }

    /**
     * execute the getIpAddress() class
     */
    void fetchIp() {
        new getIpAddress().execute();
    }

    /**
     * Asyntask for getting the ip address of the mobile connect network
     */
    private class getIpAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            // do above Server call here
            try {
                return InetAddress.getLocalHost().toString();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return "007";
            }
        }

        @Override
        protected void onPostExecute(String message) {
            Log.d(TAG, "onPostExecute: " + message);
            bankNewStripModelImplement.ipAddress(message);
        }
    }


    /**
     * method for add bank account
     *
     * @param token          session token
     * @param jsonObject     required field in json object
     * @param isPictureTaken boolen for picture is taken or not
     * @param amazonS3       object of UploadFileAmazonS3 class
     * @param mFileTemp      file that need to upload in amazon
     */
    void addBankAccount(String token, JSONObject jsonObject, boolean isPictureTaken, UploadFileAmazonS3 amazonS3, File mFileTemp) {

        try {

            if (jsonObject.getString("accountNumber").equals("")) {
                bankNewStripModelImplement.onErrorSSN();
                return;
            }
            else if ((jsonObject.getString("bankName")).equals("")) {
                bankNewStripModelImplement.onErrorEmail();
                return;
            } else if (jsonObject.getString("branchCode").equals("")) {
                bankNewStripModelImplement.onErrorAddress();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "addBankAccount: sessionToken " + token);
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_CONNECT_ACCOUNT_STRIPE, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);

                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            bankNewStripModelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            bankNewStripModelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        bankNewStripModelImplement.onFailure();
                    }
                } catch (Exception e) {
                    bankNewStripModelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                bankNewStripModelImplement.onFailure();
            }
        });
    }

    /**
     * mehtod for uploading image in amazon
     *
     * @param amazonS3  object of UploadFileAmazonS3 class
     * @param mFileTemp file that need to upload in amazon
     */
    private void amazonUpload(UploadFileAmazonS3 amazonS3, File mFileTemp) {
        String BUCKETSUBFOLDER = VariableConstant.BANK_PROOF;
        imageUrl = "https://" + VariableConstant.BUCKET_NAME + "." + BuildConfig.AMAZON_BASE_URL
                + BUCKETSUBFOLDER + "/"
                + mFileTemp.getName();

        amazonS3.Upload_data(AppController.getInstance(), BUCKETSUBFOLDER, mFileTemp, new UploadFileAmazonS3.UploadCallBack() {
            @Override
            public void sucess(String success) {
                Log.d(TAG, "sucess: amazon " + success);

            }

            @Override
            public void error(String errormsg) {
                Log.d(TAG, "error: amazon error " + errormsg);
            }
        });
    }

    /**
     * interface for  presenter implementation
     */
    interface BankNewStripModelImplement {
        void onFailure();

        void onFailure(String msg);

        void onSuccess(String failureMsg);

        void ipAddress(String ip);

        void onErrorImageUpload();

        void onErrorFirstName();

        void onErrorLastName();

        void onErrorEmail();

        void onErrorDob();

        void onErrorSSN();

        void onErrorAddress();

        void onErrorCity();

        void onErrorState();

        void onErrorCountry();

        void onErrorZipcode();
    }

}
