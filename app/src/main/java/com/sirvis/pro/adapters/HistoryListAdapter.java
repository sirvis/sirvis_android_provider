package com.sirvis.pro.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sirvis.pro.R;
import com.sirvis.pro.main.history.HistoryInvoiceActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.utility.CircleTransform;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by murashid on 13-Nov-17.
 * <h1>HistoryListAdapter</h1>
 * History Recycler View Adapter for showing the list of items under history
 */

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder>{

    private Typeface fontMedium, fontRegular, fontLight;
    private ArrayList<Booking> bookings;
    private SimpleDateFormat serverFormat, displayFormat;
    private Activity mAcitvity;
    private SessionManager sessionManager;

    public HistoryListAdapter(Activity context, ArrayList<Booking> bookings)
    {
        this.mAcitvity = context;
        this.bookings = bookings;
        fontMedium = Utility.getFontMedium(context);
        fontRegular = Utility.getFontRegular(context);
        fontLight = Utility.getFontLight(context);
        sessionManager =SessionManager.getSessionManager(context);

        displayFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvDateTime,tvStatus,tvCustomerName,tvAddress,tvPrice;
        ImageView ivCustomer;
        LinearLayout llHistoryHeader;
        public ViewHolder(View itemView) {
            super(itemView);

            tvDateTime = itemView.findViewById(R.id.tvDateTime);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvPrice = itemView.findViewById(R.id.tvPrice);

            llHistoryHeader = itemView.findViewById(R.id.llHistoryHeader);

            ivCustomer = itemView.findViewById(R.id.ivCustomer);

            tvDateTime.setTypeface(fontRegular);
            tvStatus.setTypeface(fontMedium);
            tvCustomerName.setTypeface(fontMedium);
            tvAddress.setTypeface(fontRegular);
            tvPrice.setTypeface(fontRegular);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_history, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tvStatus.setText(bookings.get(position).getStatusMsg());
        holder.tvCustomerName.setText(bookings.get(position).getFirstName() + " "+bookings.get(position).getLastName());
        holder.tvAddress.setText(bookings.get(position).getAddLine1());
        holder.tvPrice.setText(Utility.getPrice(String.valueOf((Double.parseDouble(bookings.get(position).getAccounting().getTotal())) - Double.parseDouble(bookings.get(position).getAccounting().getLastDues()) ), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        try {

            if(!bookings.get(position).getProfilePic().equals(""))
            {
                Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions()
                        .error(R.drawable.profile_default_image)
                        .transform(new CircleTransform(mAcitvity))
                        .placeholder(R.drawable.profile_default_image))
                        .load(bookings.get(position).getProfilePic())
                        .into(holder.ivCustomer);
            }

            holder.tvDateTime.setText(displayFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getBookingRequestedFor()))));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        switch (bookings.get(position).getStatus())
        {
            case VariableConstant.JOB_COMPLETED_RAISE_INVOICE:
                holder.tvStatus.setTextColor(ContextCompat.getColor(mAcitvity,R.color.jobStatusCompleted));
                break;

            case VariableConstant.CANCELLED_BY_PROVIDER:
            case VariableConstant.CANCELLED_BY_CUTOMER:
                holder.tvStatus.setTextColor(ContextCompat.getColor(mAcitvity,R.color.jobStatusCanceled));
                break;

            case VariableConstant.BOOKING_EXPIRED:
                holder.tvStatus.setTextColor(ContextCompat.getColor(mAcitvity,R.color.jobStatusExpired));
                break;

            case VariableConstant.REJECT:
                holder.tvStatus.setTextColor(ContextCompat.getColor(mAcitvity,R.color.jobStatusDeclined));
                break;

            default:
                holder.tvStatus.setTextColor(ContextCompat.getColor(mAcitvity,R.color.jobStatusCanceled));
                break;
        }

        holder.llHistoryHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mAcitvity, HistoryInvoiceActivity.class);
                int position = holder.getAdapterPosition();
                Bundle bundle = new Bundle();
                bundle.putSerializable("booking",bookings.get(position));
                intent.putExtras(bundle);
                mAcitvity.startActivity(intent);
                mAcitvity.overridePendingTransition(R.anim.bottom_to_top, R.anim.stay);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }
}
