package com.sirvis.pro.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.sirvis.pro.R;
import com.sirvis.pro.pojo.profile.support.SupportData;
import com.sirvis.pro.main.profile.support.SupportSubCategoryActivity;
import com.sirvis.pro.utility.WebViewActivity;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>SupportListAdapter</h1>
 * Support Recycler view adapter for displaying the list of supports in  SupportActivity and SupportSubCategoryActivity
 * @see com.sirvis.pro.main.profile.support.SupportActivity
 * @see com.sirvis.pro.main.profile.support.SupportSubCategoryActivity
 */

public class SupportListAdapter extends RecyclerView.Adapter<SupportListAdapter.ViewHolder> implements View.OnClickListener {

    private ArrayList<SupportData> supportDatas;
    private boolean isMainCategory;
    private Activity mActivity;

    public SupportListAdapter(Activity mActivity, ArrayList<SupportData> supportDatas, boolean isMainCategory) {
        this.mActivity = mActivity;
        this.supportDatas = supportDatas;
        this.isMainCategory = isMainCategory;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_support_text, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(supportDatas.get(position).getName());
        holder.tvName.setOnClickListener(this);
        holder.tvName.setTag(holder);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvName) {
            int adapterPosition = ((ViewHolder) v.getTag()).getAdapterPosition();
            if (isMainCategory) {
                if (supportDatas.get(adapterPosition).getSubcat().size() > 0) {
                    Intent supportSubCatIntent = new Intent(mActivity, SupportSubCategoryActivity.class);
                    supportSubCatIntent.putExtra("data", supportDatas.get(adapterPosition).getSubcat());
                    supportSubCatIntent.putExtra("title", supportDatas.get(adapterPosition).getName());
                    mActivity.startActivity(supportSubCatIntent);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                } else {
                    Intent webIntent = new Intent(mActivity, WebViewActivity.class);
                    webIntent.putExtra("URL", supportDatas.get(adapterPosition).getLink());
                    webIntent.putExtra("title", supportDatas.get(adapterPosition).getName());
                    webIntent.putExtra("isFromSupport", true);
                    mActivity.startActivity(webIntent);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
            } else {
                Intent webIntent = new Intent(mActivity, WebViewActivity.class);
                webIntent.putExtra("URL", supportDatas.get(adapterPosition).getLink());
                webIntent.putExtra("title", supportDatas.get(adapterPosition).getName());
                webIntent.putExtra("isFromSupport", true);
                mActivity.startActivity(webIntent);
                mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
            }
        }
    }

    @Override
    public int getItemCount() {
        return supportDatas.size();
    }



}
