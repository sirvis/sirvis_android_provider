package com.sirvis.pro.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.pojo.address.AddressData;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 29-Sep-17.
 * <h1>AddressListAdapter</h1>
 * Recycler View Adapter for Address List for selecting address during schedule
 * @see com.sirvis.pro.address.AddressListActivity
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "BankListAdapter";
    private Activity activity;
    private ArrayList<AddressData> addressDatas;
    private Typeface  fontRegular,fondMedium;
    private AddressEditListener addressEditListener;
    private boolean isFromProfile;

    public AddressListAdapter(Activity activity, ArrayList<AddressData> addressDatas, AddressEditListener addressEditListener, boolean isFromProfile)
    {
        this.addressDatas = addressDatas;
        fondMedium = Utility.getFontMedium(activity);
        fontRegular = Utility.getFontRegular(activity);
        this.activity = activity;
        this.addressEditListener = addressEditListener;
        this.isFromProfile = isFromProfile;
    }


    class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView ivHomeOffice, ivDelete;
        TextView tvHome, tvAddress;
        RadioButton rbDefaultAddress;
        Boolean isDefault = false;

        public ViewHolder(View itemView)
        {
            super(itemView);

            ivHomeOffice = itemView.findViewById(R.id.ivHomeOffice);
            tvHome = itemView.findViewById(R.id.tvHome);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            rbDefaultAddress = itemView.findViewById(R.id.rbDefaultAddress);

            tvAddress.setTypeface(fontRegular);
            tvHome.setTypeface(fondMedium);
           // ivDelete.setTypeface(fondMedium);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_address, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.tvAddress.setText(addressDatas.get(position).getAddLine1()+ ","+addressDatas.get(position).getAddLine2());

        if(addressDatas.get(position).getTaggedAs().equals("home"))
        {
            holder.ivHomeOffice.setImageResource(R.drawable.vector_address_home);
            holder.tvHome.setText(activity.getString(R.string.home));
        }
        else if(addressDatas.get(position).getTaggedAs().equals("office"))
        {
            holder.ivHomeOffice.setImageResource(R.drawable.vector_address_office);
            holder.tvHome.setText(activity.getString(R.string.work));
        }
        else
        {
            holder.ivHomeOffice.setImageResource(R.drawable.vector_address_heart);
            holder.tvHome.setText(addressDatas.get(position).getTaggedAs());
        }

        if(addressDatas.get(position).getDefaultAddress().equals("1"))
        {

            holder.ivDelete.setImageResource(R.drawable.vector_delete_unselected);
            holder.ivDelete.setEnabled(true);
            holder.rbDefaultAddress.setChecked(true);
        }
        else
        {
            holder.ivDelete.setVisibility(View.VISIBLE);
            holder.ivDelete.setEnabled(true);
            holder.rbDefaultAddress.setChecked(false);
        }

        if(isFromProfile)
        {
            holder.rbDefaultAddress.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.rbDefaultAddress.setVisibility(View.GONE);
        }

        holder.tvAddress.setOnClickListener(this);
        holder.ivDelete.setOnClickListener(this);
        holder.rbDefaultAddress.setOnClickListener(this);

        holder.tvAddress.setTag(holder);
        holder.ivDelete.setTag(holder);
        holder.rbDefaultAddress.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return addressDatas.size();
    }

    @Override
    public void onClick(View v)
    {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId())
        {
            case R.id.tvAddress:
            case R.id.rbDefaultAddress:
                Log.d(TAG, "onClick: happened" + position);
                addressEditListener.onAddressSelect(position);
                break;

           /* case R.id.ivEdit:
                addressEditListener.onAddressEdit(position);
                break;*/

            case R.id.ivDelete:
                addressEditListener.onAddressDelete(position);
                break;


        }
    }

    /**
     * <h1>AddressEditListener</h1>
     * Interface for address select,edit and delete callback
     * */
    public interface AddressEditListener
    {
        void onAddressEdit(int position);
        void onAddressDelete(int position);
        void onAddressSelect(int position);
    }
}
