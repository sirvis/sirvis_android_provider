package com.sirvis.pro.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.main.profile.helpcenter.NewTicketActivity;
import com.sirvis.pro.pojo.profile.helpcenter.Ticket;
import com.sirvis.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by murashid on 28-Dec-17.
 */

public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.ViewHolder> implements View.OnClickListener {

    private Typeface fontBold,fontRegular, fontMedium;
    private Activity mActivity;
    private ArrayList<Ticket> ticketData;
    private SimpleDateFormat displayDateFormat,displayTimeFormat,serverFormat;

    public TicketListAdapter(Activity mActivity, ArrayList<Ticket> ticketData)
    {
        this.mActivity = mActivity;
        this.ticketData = ticketData;
        fontBold = Utility.getFontBold(mActivity);
        fontRegular = Utility.getFontRegular(mActivity);
        fontMedium = Utility.getFontMedium(mActivity);

        displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        displayTimeFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvStatus,tvSubjectHeader,tvSubject,tvDate,tvTime;
        LinearLayout llTicket;
        public ViewHolder(View itemView) {
            super(itemView);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvSubjectHeader = itemView.findViewById(R.id.tvSubjectHeader);
            tvSubject = itemView.findViewById(R.id.tvSubject);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);
            llTicket = itemView.findViewById(R.id.llTicket);

            tvStatus.setTypeface(fontBold);
            tvSubject.setTypeface(fontMedium);
            tvSubjectHeader.setTypeface(fontMedium);
            tvDate.setTypeface(fontRegular);
            tvTime.setTypeface(fontRegular);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_ticket, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if(!ticketData.get(position).isHeader())
        {
            holder.tvStatus.setVisibility(View.GONE);
        }
        else
        {
            holder.tvStatus.setVisibility(View.VISIBLE);
            if(ticketData.get(position).getHeadername().equals("open"))
            {
                holder.tvStatus.setText(mActivity.getString(R.string.statusOpen));
            }
            else
            {
                holder.tvStatus.setText(mActivity.getString(R.string.statusClose));
            }
        }

        holder.tvSubject.setText(ticketData.get(position).getSubject());
        try {
            holder.tvSubjectHeader.setText(String.valueOf(ticketData.get(position).getSubject().charAt(0)).toUpperCase());

            holder.tvDate.setText(displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(ticketData.get(position).getTimeStamp()))));
            holder.tvTime.setText(displayTimeFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(ticketData.get(position).getTimeStamp()))));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        holder.llTicket.setOnClickListener(this);
        holder.llTicket.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return ticketData.size() ;
    }

    @Override
    public void onClick(View view) {
        ViewHolder holder = (ViewHolder) view.getTag();
        int position = holder.getAdapterPosition();
        switch (view.getId())
        {
            case R.id.llTicket:
                Intent intent = new Intent(mActivity, NewTicketActivity.class);
                intent.putExtra("isAlreadyRaised",true);
                intent.putExtra("ticketId",ticketData.get(position).getId());
                intent.putExtra("status",ticketData.get(position).getStatus());
                mActivity.startActivity(intent);
                break;
        }
    }

}
