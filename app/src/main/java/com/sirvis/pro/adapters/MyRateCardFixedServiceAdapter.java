package com.sirvis.pro.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.main.profile.myratecard.singleservice.SingleServiceActivity;
import com.sirvis.pro.pojo.profile.ratecard.CategoryService;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.util.ArrayList;

/**
 * Created by murashid on 20-Mar-17.
 * <h1>BankListAdapter</h1>
 * BankList Recycler View adapter for bank list
 * @see com.sirvis.pro.main.profile.bank.BankDetailsActivity
 */

public class MyRateCardFixedServiceAdapter extends RecyclerView.Adapter<MyRateCardFixedServiceAdapter.ViewHolder> implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private SessionManager sessionManager;
    private static final String TAG = "CategoryListAdapter";
    private ArrayList<CategoryService> categoryServices;
    private Typeface fontRegular, fontMedium, fontBold;
    private Activity mActivity;
    private FixedServiceCallBack fixedServiceCallBack;
    private boolean isPriceSetByProvider;
    private Fragment fragment;

    public MyRateCardFixedServiceAdapter(Activity mActivity,Fragment fragment, ArrayList<CategoryService> categoryServices, boolean isPriceSetByProvider,FixedServiceCallBack fixedServiceCallBack)
    {
        this.categoryServices = categoryServices;
        this.sessionManager = SessionManager.getSessionManager(mActivity);
        this.mActivity = mActivity;
        this.fragment = fragment;
        this.isPriceSetByProvider = isPriceSetByProvider;
        this.fixedServiceCallBack = fixedServiceCallBack;
        fontMedium = Utility.getFontMedium(mActivity);
        fontBold = Utility.getFontBold(mActivity);
        fontRegular = Utility.getFontRegular(mActivity);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        CheckBox cbService;
        TextView tvHeader, tvService, tvPrice, tvServiceTimeLabel, tvServiceTime, tvServiceInfo;
        public ViewHolder(View itemView)
        {
            super(itemView);

            cbService = itemView.findViewById(R.id.cbService);
            tvHeader = itemView.findViewById(R.id.tvHeader);
            tvService = itemView.findViewById(R.id.tvService);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvServiceTimeLabel = itemView.findViewById(R.id.tvServiceTimeLabel);
            tvServiceTime =  itemView.findViewById(R.id.tvServiceTime);
            tvServiceInfo =  itemView.findViewById(R.id.tvServiceInfo);

            tvHeader.setTypeface(fontBold);
            cbService.setTypeface(fontBold);
            tvService.setTypeface(fontBold);
            tvPrice.setTypeface(fontBold);
            tvServiceTimeLabel.setTypeface(fontMedium);
            tvServiceTime.setTypeface(fontMedium);
            tvServiceInfo.setTypeface(fontRegular);
        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyRateCardFixedServiceAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_my_rate_card_fixed_service, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        final CategoryService service = categoryServices.get(position);

        holder.cbService.setOnCheckedChangeListener(null);

        if(service.isSubCategoryHead())
        {
            holder.tvHeader.setVisibility(View.VISIBLE);
            holder.tvHeader.setText(service.getSubCatName());
        }
        else
        {
            holder.tvHeader.setVisibility(View.GONE);
        }

        holder.cbService.setText(service.getSer_name());
        holder.tvService.setText(service.getSer_name());
        holder.tvPrice.setText(Utility.getPrice(service.getIs_unit(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        holder.tvServiceTime.setText(service.getServiceCompletionTime()+" "+mActivity.getString(R.string.minutes));
        holder.tvServiceInfo.setText(service.getSer_desc());

        if(service.getStatus().equals("1"))
        {
            holder.cbService.setChecked(true);
        }
        else
        {
            holder.cbService.setChecked(false);
        }

        holder.tvServiceInfo.post(new Runnable() {
            @Override
            public void run() {
                int lineCount = holder.tvServiceInfo.getLineCount();
                if(lineCount > 2 && service.getSer_desc().length() > 65)
                {
                    holder.tvServiceInfo.setOnClickListener(MyRateCardFixedServiceAdapter.this);
                    holder.tvServiceInfo.setTag(holder);
                    Utility.setSpannableString(mActivity,holder.tvServiceInfo,service.getSer_desc().substring(0,63)+"..",mActivity.getString(R.string.viewMore));
                }
                else
                {
                    holder.tvServiceInfo.setOnClickListener(null);
                }
            }
        });

        if(!isPriceSetByProvider)
        {
            holder.tvPrice.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            holder.cbService.setVisibility(View.GONE);
            holder.tvService.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.cbService.setOnCheckedChangeListener(this);
            holder.tvPrice.setOnClickListener(this);
        }
        holder.cbService.setTag(holder);
        holder.tvPrice.setTag(holder);

    }

    @Override
    public int getItemCount() {
        return categoryServices.size();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        ViewHolder viewHolder = (ViewHolder) compoundButton.getTag();
        int position = viewHolder.getAdapterPosition();

        String action = "0";
        if(b)
        {
            action = "1";
        }

        if(categoryServices.get(position).isServiceUnderSubCat())
        {
            fixedServiceCallBack.onServiceSelected(categoryServices.get(position).getServicePosition(), categoryServices.get(position).getSubCatPosition(),
                    categoryServices.get(position).get_id(),action);
        }
        else
        {
            fixedServiceCallBack.onServiceSelected(position,categoryServices.get(position).get_id(),action);
        }

    }

    @Override
    public void onClick(View v)
    {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId())
        {
            case R.id.tvPrice:
                Intent intent = new Intent(mActivity,SingleServiceActivity.class);
                intent.putExtra("service",categoryServices.get(position));
                intent.putExtra("position",position);
                intent.putExtra("servicePosition",categoryServices.get(position).getServicePosition());
                intent.putExtra("isServiceUnderSubCat",categoryServices.get(position).isServiceUnderSubCat());
                intent.putExtra("subCatPosition",categoryServices.get(position).getSubCatPosition());

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    fragment.startActivityForResult(intent, VariableConstant.REQUEST_CODE_SERVICE_CHANGED,
                            ActivityOptions.makeSceneTransitionAnimation(mActivity).toBundle());
                }
                else {
                    fragment.startActivityForResult(intent, VariableConstant.REQUEST_CODE_SERVICE_CHANGED);
                    mActivity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }

                break;

            case R.id.tvReadMoreLess:
            case R.id.tvServiceInfo:
                fixedServiceCallBack.onClickReadMore(position);
                break;
        }
    }

    public interface FixedServiceCallBack
    {
        void onClickReadMore(int position);
        void onServiceSelected(int position, String serviceId, String action);
        void onServiceSelected(int position, int subCatPosition, String serviceId, String action);
    }
}
