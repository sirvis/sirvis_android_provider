package com.sirvis.pro.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sirvis.pro.R;
import com.sirvis.pro.pojo.booking.ImgUrls;

import java.util.ArrayList;


/**
 * Created by murashid on 20/5/18.
 */
public class JobImageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ImgUrls> imageUrls;
    private Activity mAcitvity;

    public JobImageListAdapter(Activity mAcitvity, ArrayList<ImgUrls> imageUrls) {
        this.mAcitvity = mAcitvity;
        this.imageUrls = imageUrls;
    }

    @Override
    public int getItemViewType(int position) {
        return imageUrls.get(position).getIsImsge();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 0) {
            return new TextViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_text_start, parent, false));

        } else {
            return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_image_view, parent, false));

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ImageViewHolder) {
            ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
            if (!imageUrls.get(position).equals("")) {
                Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions().error(R.drawable.vector_app_logo_profile)
                        .placeholder(R.drawable.vector_app_logo_profile))
                        .load(imageUrls.get(position).getImageURl())
                        .into(imageViewHolder.ivJobPhoto);
            } else {
                Glide.with(mAcitvity)
                        .load(R.drawable.vector_app_logo_profile)
                        .into(imageViewHolder.ivJobPhoto);
            }
        } else {
            TextViewHolder textViewHolder= (TextViewHolder) holder;
            textViewHolder.textView.setText("No Images Found.");
        }


    }

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }

    private class TextViewHolder extends RecyclerView.ViewHolder {
        TextView textView;

        public TextViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text1);
        }

    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView ivJobPhoto;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ivJobPhoto = (ImageView) itemView.findViewById(R.id.ivJobPhoto);
        }
    }

}
