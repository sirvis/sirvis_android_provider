package com.sirvis.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.landing.signup.SignupSelectionListener;
import com.sirvis.pro.pojo.signup.CityData;
import com.sirvis.pro.pojo.signup.SubCategory;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 07-Sep-17.
 * <h1>CCity Recycler view adapter for displaying list of city in Signup screen</h1>
 * @see com.sirvis.pro.landing.signup.SignupActivity
 */

public class SignupSelectionListAdapter extends RecyclerView.Adapter<SignupSelectionListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CityData> cityDatas;
    private ArrayList<SubCategory> subCategories;
    private String type;
    private SignupSelectionListener signupSelectionListener;
    private Typeface fontMedium;

    public SignupSelectionListAdapter(Context context, ArrayList<CityData> cityDatas, ArrayList<SubCategory> subCategories,
                                      String type, SignupSelectionListener signupSelectionListener)
    {
        this.context = context;
        this.cityDatas = cityDatas;
        this.subCategories = subCategories;
        this.type = type;
        this.signupSelectionListener = signupSelectionListener;
        fontMedium = Utility.getFontMedium(context);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvSingupSelection;
        ImageView ivTick;
        LinearLayout llSignupSelection;
        ViewHolder(View itemView) {
            super(itemView);
            tvSingupSelection = itemView.findViewById(R.id.tvSingupSelection);
            ivTick = itemView.findViewById(R.id.ivTick);
            llSignupSelection = itemView.findViewById(R.id.llSignupSelection);
            tvSingupSelection.setTypeface(fontMedium);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_signup_selection, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if(type.equals("city"))
        {
            holder.tvSingupSelection.setText(cityDatas.get(position).getCity());
            holder.llSignupSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signupSelectionListener.onCitySelecting(cityDatas.get(holder.getAdapterPosition()).getId(),cityDatas.get(holder.getAdapterPosition()).getCity());
                    holder.tvSingupSelection.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                    holder.ivTick.setVisibility(View.VISIBLE);
                }
            });
        }
        else
        {
            holder.tvSingupSelection.setText(subCategories.get(position).getSub_cat_name());

            if(subCategories.get(position).isAlreadySelected())
            {
                holder.tvSingupSelection.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                holder.ivTick.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.tvSingupSelection.setTextColor(ContextCompat.getColor(context,R.color.darkTextColor));
                holder.ivTick.setVisibility(View.INVISIBLE);
            }

            holder.llSignupSelection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    signupSelectionListener.onSubCategorySelecting(subCategories.get(holder.getAdapterPosition()).get_id(),subCategories.get(holder.getAdapterPosition()).getSub_cat_name());
                    if(holder.ivTick.getVisibility() == View.VISIBLE)
                    {
                        holder.tvSingupSelection.setTextColor(ContextCompat.getColor(context,R.color.darkTextColor));
                        holder.ivTick.setVisibility(View.INVISIBLE);
                    }
                    else
                    {
                        holder.tvSingupSelection.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                        holder.ivTick.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if(type.equals("city"))
        {
            return cityDatas.size();
        }
        else
        {
            return subCategories.size();
        }

    }
}
