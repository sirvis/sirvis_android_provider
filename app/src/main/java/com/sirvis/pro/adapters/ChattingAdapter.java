package com.sirvis.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.sirvis.pro.R;
import com.sirvis.pro.pojo.chat.ChatData;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;


/**
 * <h>ChattingAdapter</h>
 * Created by Ali on 12/22/2017.
 */

public class ChattingAdapter extends RecyclerView.Adapter<ChattingAdapter.ViewHolder>
{
    private Context mContext;
    private ArrayList<ChatData>chatData;
    private String fromId;
    private Typeface fontRegular;

    public ChattingAdapter(Context mContext,ArrayList<ChatData> chatData, String fromId)
    {
        this.mContext = mContext;
        this.chatData = chatData;
        this.fromId = fromId;
        fontRegular = Utility.getFontRegular(mContext);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private RelativeLayout rlSentMsg, rlReceiveMsg;
        private ImageView ivSendPic, ivReceivedPic;
        private TextView tvSentMsg, tvReceiveMsg;
        private ProgressBar pBSentImage;
        
        ViewHolder(View itemView)
        {
            super(itemView);
            
            ivSendPic = itemView.findViewById(R.id.ivSendPic);
            ivReceivedPic = itemView.findViewById(R.id.ivReceivedPic);
            tvSentMsg = itemView.findViewById(R.id.tvSentMsg);
            tvReceiveMsg = itemView.findViewById(R.id.tvReceiveMsg);
            rlSentMsg = itemView.findViewById(R.id.rlSentMsg);
            rlReceiveMsg = itemView.findViewById(R.id.rlReceiveMsg);

            pBSentImage = itemView.findViewById(R.id.pBSentImage);

            tvSentMsg.setTypeface(fontRegular);
            tvReceiveMsg.setTypeface(fontRegular);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chatting_adapter,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        if(chatData.get(position).getFromID().equals(fromId))
        {
            holder.rlSentMsg.setVisibility(View.VISIBLE);
            holder.rlReceiveMsg.setVisibility(View.GONE);
            if(chatData.get(position).getType().equals("1"))
            {
                holder.tvSentMsg.setText(chatData.get(position).getContent());
                holder.tvSentMsg.setVisibility(View.VISIBLE);
                holder.ivSendPic.setVisibility(View.GONE);
                holder.pBSentImage.setVisibility(View.GONE);
            }
            else
            {
                holder.tvSentMsg.setVisibility(View.GONE);
                holder.ivSendPic.setVisibility(View.VISIBLE);
                String url = chatData.get(position).getContent();
                if(!url.equals(""))
                {
                    Glide.with(mContext)
                            .load(url)
                            .into(holder.ivSendPic);

                    holder.pBSentImage.setVisibility(View.GONE);
                }
                else
                {
                    holder.pBSentImage.setVisibility(View.VISIBLE);
                }
            }
        }else
        {
            holder.rlSentMsg.setVisibility(View.GONE);
            holder.rlReceiveMsg.setVisibility(View.VISIBLE);
            if(chatData.get(position).getType().equals("1"))
            {
                holder.tvReceiveMsg.setText(chatData.get(position).getContent());
                holder.tvReceiveMsg.setVisibility(View.VISIBLE);
                holder.ivReceivedPic.setVisibility(View.GONE);

            }
            else
            {
                holder.tvReceiveMsg.setVisibility(View.GONE);
                holder.ivReceivedPic.setVisibility(View.VISIBLE);
                String url = chatData.get(position).getContent();
                if(!url.equals(""))
                {
                    Glide.with(mContext)
                            .load(url)
                            .into(holder.ivReceivedPic);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return chatData.size();
    }
}
