package com.sirvis.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.sirvis.pro.R;
import com.sirvis.pro.main.profile.bank.BankBottomSheetFragment;
import com.sirvis.pro.pojo.profile.bank.BankList;
import com.sirvis.pro.utility.Utility;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by murashid on 20-Mar-17.
 * <h1>BankListAdapter</h1>
 * BankList Recycler View adapter for bank list
 * @see com.sirvis.pro.main.profile.bank.BankDetailsActivity
 */

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.ViewHolder> implements View.OnClickListener {

    private static final String TAG = "BankListAdapter";
    private ArrayList<BankList> bankLists;
    private Typeface fontMedium, fontLight;
    private RefreshBankDetails refreshBankDetails;
    private FragmentManager fragmentManager;
    private Context context;

    public BankListAdapter(Context context, ArrayList<BankList> bankLists, FragmentManager fragmentManager, RefreshBankDetails refreshBankDetails)
    {
        this.bankLists = bankLists;
        this.context = context;
        fontMedium = Utility.getFontMedium(context);
        fontLight = Utility.getFontRegular(context);
        this.fragmentManager = fragmentManager;
        this.refreshBankDetails = refreshBankDetails;

    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvAccountNo, tvBankName, tvRoutinNo;
        LinearLayout llBankDetails;
        ImageView ivTick;

        public ViewHolder(View itemView)
        {
            super(itemView);

            tvAccountNo = itemView.findViewById(R.id.tvAccountNo);
            tvBankName = itemView.findViewById(R.id.tvBankName);
            tvRoutinNo = itemView.findViewById(R.id.tvRoutinNo);
            llBankDetails = itemView.findViewById(R.id.llBankDetails);
            ivTick = itemView.findViewById(R.id.ivTick);

            tvAccountNo.setTypeface(fontLight);
            tvBankName.setTypeface(fontLight);
            tvRoutinNo.setTypeface(fontLight);
        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BankListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_bank_details, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.tvAccountNo.setText("xxxxxxxx"+ bankLists.get(position).getLast4());
        holder.tvBankName.setText(bankLists.get(position).getBank_name());
        holder.tvRoutinNo.setText(context.getString(R.string.rountinNoLabel)+ " "+bankLists.get(position).getRouting_number());
        //change to default account when we get the proper responce
        if(bankLists.get(position).getDefault_for_currency().equals("true"))
        {
              holder.ivTick.setVisibility(View.VISIBLE);
        }
        else
        {
              holder.ivTick.setVisibility(View.INVISIBLE);
        }

        holder.llBankDetails.setOnClickListener(this);
        holder.llBankDetails.setTag(holder);

    }

    @Override
    public int getItemCount() {
        return bankLists.size();
    }

    @Override
    public void onClick(View v)
    {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();

        switch (v.getId())
        {
            case R.id.llBankDetails:
                BottomSheetDialogFragment bottomSheetDialogFragment = BankBottomSheetFragment.newInstance(bankLists.get(position),refreshBankDetails);
                bottomSheetDialogFragment.show(fragmentManager, bottomSheetDialogFragment.getTag());
                break;

        }
    }

    /**
     * <h1>RefreshBankDetails</h1>
     * Interface for refresh the bank list
     * */

    public interface RefreshBankDetails extends Serializable {
        void onRefresh();
    }

}
