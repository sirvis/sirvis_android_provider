package com.sirvis.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.pojo.profile.helpcenter.TicketEvents;
import com.sirvis.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by murashid on 28-Dec-17.
 */

public class TicketEventListAdapter extends RecyclerView.Adapter<TicketEventListAdapter.ViewHolder> {

    private ArrayList<TicketEvents> events;
    private Typeface fontRegular, fontMedium;
    private SimpleDateFormat displayDateFormat,displayTimeFormat,serverFormat;

    public TicketEventListAdapter(Context context, ArrayList<TicketEvents> events)
    {
        this.events = events;
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);

        displayDateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        displayTimeFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvNameHead,tvName,tvDate,tvMessage,tvTime;
        public ViewHolder(View itemView) {
            super(itemView);
            tvNameHead = itemView.findViewById(R.id.tvNameHead);
            tvName = itemView.findViewById(R.id.tvName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            tvTime = itemView.findViewById(R.id.tvTime);

            tvNameHead.setTypeface(fontMedium);
            tvName.setTypeface(fontRegular);
            tvDate.setTypeface(fontRegular);
            tvMessage.setTypeface(fontRegular);
            tvTime.setTypeface(fontRegular);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_ticket_event, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvNameHead.setText(String.valueOf(events.get(position).getName().charAt(0)).toUpperCase());
        holder.tvName.setText(events.get(position).getName());
        holder.tvMessage.setText(events.get(position).getBody());

        try {
            holder.tvDate.setText(displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(events.get(position).getTimeStamp()))));
            holder.tvTime.setText(displayTimeFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(events.get(position).getTimeStamp()))));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

}
