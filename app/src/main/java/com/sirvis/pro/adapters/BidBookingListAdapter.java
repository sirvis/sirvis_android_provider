package com.sirvis.pro.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.bookingflow.AcceptRejectActivity;
import com.sirvis.pro.bookingflow.ArrivedActivity;
import com.sirvis.pro.bookingflow.EventStartedCompletedActivity;
import com.sirvis.pro.bookingflow.InCallActivity;
import com.sirvis.pro.bookingflow.InvoiceActivity;
import com.sirvis.pro.bookingflow.OnTheWayActivity;
import com.sirvis.pro.bookingflow.TeleCallActivity;
import com.sirvis.pro.pojo.booking.Booking;
import com.sirvis.pro.utility.AppController;
import com.sirvis.pro.utility.NotificationHelper;
import com.sirvis.pro.utility.SessionManager;
import com.sirvis.pro.utility.Utility;
import com.sirvis.pro.utility.VariableConstant;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.sirvis.pro.utility.Utility.getDayNumberSuffix;

/**
 * Created by murashid on 09-Sep-17.
 * <h1>BookingStatus List Adapter</h1>
 * BookingStatus recycler view adapter for inflating list of booking in BookingFragment under home screen
 *
 * @see com.sirvis.pro.main.booking.BookingFragment
 */

public class BidBookingListAdapter extends RecyclerView.Adapter<BidBookingListAdapter.ViewHolder> implements View.OnClickListener {

    private String TAG = getClass().getName();

    private String type;//Adapter creating from 1. offers => New Booking, 2.bid = > Active Bids, 3.accepted = > My booking list,
    private Typeface fontRegular, fontMedium;
    private Activity mAcitvity;
    private ArrayList<Booking> bookings;
    private SimpleDateFormat serverFormat, displayFormat, displayScheduleTimeFormat, displayScheduleDateFormat;
    private SessionManager sessionManager;

    private long currentTime = 0;
    private Handler handler;
    private Runnable runnable;
    private ArrayList<CountDownTimer> timers;

    private LayoutInflater inflater;

    public BidBookingListAdapter(Activity mAcitvity, ArrayList<Booking> bookings, String type) {
        this.mAcitvity = mAcitvity;
        this.bookings = bookings;
        this.type = type;
        timers = new ArrayList<>();
        sessionManager = SessionManager.getSessionManager(mAcitvity);
        fontRegular = Utility.getFontRegular(mAcitvity);
        fontMedium = Utility.getFontMedium(mAcitvity);
        displayFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a", Locale.US);
        displayScheduleTimeFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        displayScheduleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        inflater = mAcitvity.getLayoutInflater();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_bid_booking, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tvCustomerName.setText(bookings.get(position).getFirstName() + " " + bookings.get(position).getLastName());
        holder.tvBookingStatus.setText(bookings.get(position).getStatusMsg());

        switch (bookings.get(position).getCallType()) {
            case "1":
                holder.tvCategory.setText(bookings.get(position).getCategory() + "(" + mAcitvity.getString(R.string.inCall) + ")");
                break;

            case "2":
                holder.tvCategory.setText(bookings.get(position).getCategory());
                break;

            case "3":
                holder.tvCategory.setText(bookings.get(position).getCategory() + "(" + mAcitvity.getString(R.string.teleCall) + ")");
                break;
        }

        try {
            SimpleDateFormat getDateOnly = new SimpleDateFormat("dd", Locale.US);
            String date = getDateOnly.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime())));
            String dayNumberSuffix = getDayNumberSuffix(Integer.parseInt(date));
            SimpleDateFormat displayDateFormat = new SimpleDateFormat("MMMM yyyy | hh:mm a", Locale.US);

            holder.tvBookingTime.setText(date + dayNumberSuffix + displayDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
            if (!bookings.get(position).getCallType().equals("3"))
                holder.tvDistance.setText("" + Utility.getFormattedPrice(String.valueOf((Double.parseDouble(bookings.get(position).getDistance())) * sessionManager.getDistanceUnitConverter())) + " " +
                        sessionManager.getDistanceUnit() + "  " + mAcitvity.getString(R.string.away));
        } catch (Exception e) {
            e.printStackTrace();
        }

        switch (bookings.get(position).getStatus()) {
            case VariableConstant.RECEIVED:
            case VariableConstant.NEW_BOOKING:
                double shift = 1, amount = 0;

                if (bookings.get(position).getBookingType().equals("1")) {
                    holder.tvBookingTypeBidAmount.setText(mAcitvity.getString((R.string.asap)));
                    holder.rlSchedule.setVisibility(View.GONE);
                    holder.llSheduleDays.setVisibility(View.GONE);
                    holder.tvBookingTime.setVisibility(View.VISIBLE);
                } else if (bookings.get(position).getBookingType().equals("2")) {
                    holder.tvBookingTypeBidAmount.setText(mAcitvity.getString((R.string.scheduled)));
                    holder.rlSchedule.setVisibility(View.GONE);
                    holder.llSheduleDays.setVisibility(View.GONE);
                    holder.tvBookingTime.setVisibility(View.VISIBLE);
                } else {
                    holder.rlSchedule.setVisibility(View.VISIBLE);
                    holder.llSheduleDays.setVisibility(View.VISIBLE);
                    holder.tvBookingTime.setVisibility(View.GONE);
                    try {
                        holder.tvScheduleStartTime.setText(displayScheduleTimeFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
                        holder.tvScheduleStartDate.setText(displayScheduleDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
                        holder.tvScheduleEndTime.setText(displayScheduleTimeFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getBookingEndtime()))));
                        holder.tvScheduleEndDate.setText(displayScheduleDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getBookingEndtime()))));
                        holder.tvTotalNoOfShift.setText(bookings.get(position).getAccounting().getTotalShiftBooking());
                        shift = Double.parseDouble(bookings.get(position).getAccounting().getTotalShiftBooking());

                        holder.llSheduleDays.removeAllViews();
                        for (String days : bookings.get(position).getDaysArr()) {
                            TextView tvDay = (TextView) inflater.inflate(R.layout.single_row_text_schedule_day, null);
                            tvDay.setText(days.substring(0, 1));
                            holder.llSheduleDays.addView(tvDay);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (!bookings.get(position).getBookingModel().equals("3")) {
                    holder.tvToBid.setVisibility(View.GONE);
                    amount = shift * (Double.parseDouble(bookings.get(position).getAccounting().getTotal()) - Double.parseDouble(bookings.get(position).getAccounting().getLastDues()));
                } else {
                    holder.tvToBid.setVisibility(View.VISIBLE);
                    holder.tvBookingTypeBidAmount.setText(Utility.getPrice(bookings.get(position).getAccounting().getBidCredit(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                    amount = shift * Double.parseDouble(bookings.get(position).getAccounting().getBidPrice());
                }

                holder.tvPrice.setText(Utility.getPrice(String.valueOf(amount), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                break;

            case VariableConstant.ACTIVE_BIDS:
                if (!bookings.get(position).getBookingType().equals("3")) {
                    holder.rlSchedule.setVisibility(View.GONE);
                    holder.llSheduleDays.setVisibility(View.GONE);
                    holder.tvBookingTime.setVisibility(View.VISIBLE);
                    holder.tvBookingTypeBidAmount.setText(Utility.getPrice(bookings.get(position).getAccounting().getBidPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                } else {
                    holder.rlSchedule.setVisibility(View.VISIBLE);
                    holder.llSheduleDays.setVisibility(View.VISIBLE);
                    holder.tvBookingTime.setVisibility(View.GONE);
                    try {
                        holder.tvScheduleStartTime.setText(displayScheduleTimeFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
                        holder.tvScheduleStartDate.setText(displayScheduleDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getEventStartTime()))));
                        holder.tvScheduleEndTime.setText(displayScheduleTimeFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getBookingEndtime()))));
                        holder.tvScheduleEndDate.setText(displayScheduleDateFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(bookings.get(position).getBookingEndtime()))));
                        holder.tvTotalNoOfShift.setText(bookings.get(position).getAccounting().getTotalShiftBooking());
                        double bidPrice = Double.parseDouble(bookings.get(position).getAccounting().getTotalShiftBooking()) * Double.parseDouble(bookings.get(position).getAccounting().getBidPrice());
                        holder.tvBookingTypeBidAmount.setText(Utility.getPrice(String.valueOf(bidPrice), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                        holder.llSheduleDays.removeAllViews();
                        for (String days : bookings.get(position).getDaysArr()) {
                            TextView textView = (TextView) inflater.inflate(R.layout.single_row_text_schedule_day, null);
                            textView.setText(days.substring(0, 1));
                            holder.llSheduleDays.addView(textView);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                holder.tvToBid.setVisibility(View.GONE);
                holder.tvPrice.setText(mAcitvity.getString(R.string.myQuote) + " : " + Utility.getPrice(bookings.get(position).getQuotedPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                break;

            default:
                holder.tvToBid.setVisibility(View.GONE);
                holder.rlSchedule.setVisibility(View.GONE);
                holder.llSheduleDays.setVisibility(View.GONE);
                holder.tvBookingTime.setVisibility(View.VISIBLE);
                holder.tvBookingTypeBidAmount.setText(Utility.getPrice(String.valueOf(Double.parseDouble(bookings.get(position).getAccounting().getTotal()) - Double.parseDouble(bookings.get(position).getAccounting().getLastDues()) ), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                holder.tvPrice.setText(Utility.getPrice(String.valueOf(Double.parseDouble(bookings.get(position).getAccounting().getTotal()) -Double.parseDouble(bookings.get(position).getAccounting().getLastDues())), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                break;
        }

        if (bookings.get(position).getStatus().equals(VariableConstant.RECEIVED) || bookings.get(position).getStatus().equals(VariableConstant.NEW_BOOKING) || bookings.get(position).getStatus().equals(VariableConstant.ACTIVE_BIDS)) {
            if (currentTime == 0) {
                currentTime = Utility.convertUTCToTimeStamp(bookings.get(position).getServerTime());
                runTimer();
            }
            setTimer(holder, bookings.get(position).getBookingRequestedForProvider(), bookings.get(position).getBookingExpireForProvider());
        }

        holder.cvBooking.setOnClickListener(this);
        holder.cvBooking.setTag(holder);

    }

    private void runTimer() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                currentTime = currentTime + 1000;
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(runnable, 1000);
    }

    private void setTimer(final ViewHolder viewHolder, String bookingStartTime, final String bookingEndTime) {
        final int position = viewHolder.getAdapterPosition();

        if (position != RecyclerView.NO_POSITION) {
            if (viewHolder.timer != null) {
                viewHolder.timer.cancel();
            }

            long start = Utility.convertUTCToTimeStamp(bookingStartTime);
            long end = Utility.convertUTCToTimeStamp(bookingEndTime);

            final long[] totalSecs = {(end - start) / 1000};
            final long[] hours = {0};
            final long[] minutes = {0};
            final long[] seconds = {0};

            //totalSecs[0] for Maximum Progress

            totalSecs[0] = (long) (end - currentTime) / 1000;
            hours[0] = TimeUnit.SECONDS.toHours(totalSecs[0]);
            minutes[0] = TimeUnit.SECONDS.toMinutes(totalSecs[0]) % 60;
            seconds[0] = totalSecs[0] % 60;

            viewHolder.tvRemainingTime.setText(String.format("%02d", hours[0]) + " H : " + String.format("%02d", minutes[0]) + " M : " + String.format("%02d", seconds[0]) + " S");

            viewHolder.timer = new CountDownTimer(totalSecs[0] * 1000, 1000) {
                @Override
                public void onTick(long l) {

                    totalSecs[0] = totalSecs[0] - 1;
                    hours[0] = TimeUnit.SECONDS.toHours(totalSecs[0]);
                    minutes[0] = TimeUnit.SECONDS.toMinutes(totalSecs[0]) % 60;
                    seconds[0] = totalSecs[0] % 60;

                    viewHolder.tvRemainingTime.setText(String.format("%02d", hours[0]) + " H : " + String.format("%02d", minutes[0]) + " M : " + String.format("%02d", seconds[0]) + " S");

                    if (position < bookings.size()) {
                        bookings.get(position).setCurrentTime(currentTime);
                    }
                }

                @Override
                public void onFinish() {
                    try {
                        Log.d(TAG, "onFinish: " + (Utility.convertUTCToTimeStamp(bookingEndTime) - currentTime));
                        if (((Utility.convertUTCToTimeStamp(bookingEndTime) - currentTime) == 1000)) {
                            sessionManager.setLastBooking("");
                            String bookingId = bookings.get(position).getBookingId();
                            if (!bookingId.equals(sessionManager.getLastBookingIdCancel())) {
                                sessionManager.setLastBookingIdCancel(bookingId);
                                Intent intent = new Intent();
                                intent.setAction(VariableConstant.INTENT_ACTION_CANCEL_BOOKING);
                                intent.putExtra("cancelid", bookingId);
                                intent.putExtra("msg", mAcitvity.getString(R.string.bookingExpired));
                                intent.putExtra("header", mAcitvity.getString(R.string.alert));
                                mAcitvity.sendBroadcast(intent);
                                VariableConstant.IS_BOOKING_UPDATED = true;

                                Log.d(TAG, "Booking cancel from Booking adapter" + position);

                                NotificationHelper.sendNotification(mAcitvity, "15", mAcitvity.getString(R.string.app_name), mAcitvity.getString(R.string.bookingExpired));
                            }
                            AppController.getInstance().stopNewBookingRingtoneService();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            if (viewHolder.timer != null) {
                timers.add(viewHolder.timer);
            }
        }
    }

    public void clearHandlers() {
        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }

    public void clearTimer() {
        if (timers != null) {
            for (CountDownTimer timer : timers) {
                timer.cancel();
            }
        }

    }

    @Override
    public void onClick(View v) {
        ViewHolder viewHolder = (ViewHolder) v.getTag();
        int position = viewHolder.getAdapterPosition();
        switch (v.getId()) {
            case R.id.cvBooking:
                AppController.getInstance().stopNewBookingRingtoneService();
                Intent intent;

                if ((bookings.get(position).getCallType()).equals("2")) {
                    switch (bookings.get(position).getStatus()) {
                        case VariableConstant.RECEIVED:
                        case VariableConstant.NEW_BOOKING:
                        case VariableConstant.ACTIVE_BIDS:
                            intent = new Intent(mAcitvity, AcceptRejectActivity.class);
                            break;
                        case VariableConstant.ACCEPT:
                            intent = new Intent(mAcitvity, OnTheWayActivity.class);
                            break;

                        case VariableConstant.ON_THE_WAY:
                            intent = new Intent(mAcitvity, ArrivedActivity.class);
                            break;

                        case VariableConstant.ARRIVED:
                        case VariableConstant.JOB_TIMER_STARTED:
                            intent = new Intent(mAcitvity, EventStartedCompletedActivity.class);
                            break;

                        case VariableConstant.JOB_TIMER_COMPLETED:
                            intent = new Intent(mAcitvity, InvoiceActivity.class);
                            break;

                        default:
                            intent = new Intent(mAcitvity, OnTheWayActivity.class);
                    }
                } else if ((bookings.get(position).getCallType()).equals("1")) {
                    switch (bookings.get(position).getStatus()) {
                        case VariableConstant.RECEIVED:
                        case VariableConstant.NEW_BOOKING:
                        case VariableConstant.ACTIVE_BIDS:
                            intent = new Intent(mAcitvity, AcceptRejectActivity.class);
                            break;

                        case VariableConstant.JOB_TIMER_COMPLETED:
                            intent = new Intent(mAcitvity, InvoiceActivity.class);
                            break;

                        default:
                            bookings.get(position).setStatus(VariableConstant.JOB_TIMER_STARTED);
                            intent = new Intent(mAcitvity, InCallActivity.class);
                    }
                } else {
                    switch (bookings.get(position).getStatus()) {
                        case VariableConstant.RECEIVED:
                        case VariableConstant.NEW_BOOKING:
                        case VariableConstant.ACTIVE_BIDS:
                            intent = new Intent(mAcitvity, AcceptRejectActivity.class);
                            break;

                        case VariableConstant.JOB_TIMER_COMPLETED:
//                            intent = new Intent(mAcitvity,InvoiceActivity.class);

                            intent = new Intent(mAcitvity, TeleCallActivity.class);
                            break;

                        default:
                            bookings.get(position).setStatus(VariableConstant.JOB_TIMER_STARTED);
                            intent = new Intent(mAcitvity, TeleCallActivity.class);
                    }
                }
                Log.d("bs", bookings.get(position).getStatusMsg() + "");
                Bundle bundle = new Bundle();
                intent.putExtra("isFromBookingList", true);
                bundle.putSerializable("booking", bookings.get(position));
                intent.putExtras(bundle);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                    mAcitvity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(mAcitvity).toBundle());
                } else {
                    mAcitvity.startActivity(intent);
                    mAcitvity.overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCustomerName, tvBookingTypeBidAmount, tvCategory, tvToBid, tvBookingTime, tvDistance, tvRemainingTime, tvPrice, tvBookingStatus;
        TextView tvScheduleStartTime, tvScheduleStartDate, tvScheduleEndTime, tvScheduleEndDate, tvTo, tvTotalNoOfShift, tvShift;
        CardView cvBooking;
        RelativeLayout rlRemainingTime, rlSchedule;
        LinearLayout llSheduleDays;
        CountDownTimer timer;

        ViewHolder(View itemView) {
            super(itemView);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvBookingTypeBidAmount = itemView.findViewById(R.id.tvBookingTypeBidAmount);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvToBid = itemView.findViewById(R.id.tvToBid);
            tvBookingTime = itemView.findViewById(R.id.tvBookingTime);
            tvDistance = itemView.findViewById(R.id.tvDistance);
            tvRemainingTime = itemView.findViewById(R.id.tvRemainingTime);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvBookingStatus = itemView.findViewById(R.id.tvBookingStatus);

            tvScheduleStartTime = itemView.findViewById(R.id.tvScheduleStartTime);
            tvScheduleStartDate = itemView.findViewById(R.id.tvScheduleStartDate);
            tvScheduleEndTime = itemView.findViewById(R.id.tvScheduleEndTime);
            tvScheduleEndDate = itemView.findViewById(R.id.tvScheduleEndDate);
            tvTo = itemView.findViewById(R.id.tvTo);
            tvTotalNoOfShift = itemView.findViewById(R.id.tvTotalNoOfShift);
            tvShift = itemView.findViewById(R.id.tvShift);

            cvBooking = itemView.findViewById(R.id.cvBooking);
            rlRemainingTime = itemView.findViewById(R.id.rlRemainingTime);
            rlSchedule = itemView.findViewById(R.id.rlSchedule);
            llSheduleDays = itemView.findViewById(R.id.llSheduleDays);

            tvCustomerName.setTypeface(fontMedium);
            tvBookingTypeBidAmount.setTypeface(fontMedium);
            tvCategory.setTypeface(fontRegular);
            tvToBid.setTypeface(fontRegular);
            tvBookingTime.setTypeface(fontRegular);
            tvDistance.setTypeface(fontRegular);
            tvRemainingTime.setTypeface(fontRegular);
            tvPrice.setTypeface(fontMedium);
            tvBookingStatus.setTypeface(fontRegular);

            tvScheduleStartTime.setTypeface(fontRegular);
            tvScheduleStartDate.setTypeface(fontMedium);
            tvScheduleEndTime.setTypeface(fontRegular);
            tvScheduleEndDate.setTypeface(fontMedium);
            tvTo.setTypeface(fontRegular);
            tvTotalNoOfShift.setTypeface(fontMedium);
            tvShift.setTypeface(fontMedium);

            if (type.equals("accepted")) {
                rlRemainingTime.setVisibility(View.GONE);
                tvBookingStatus.setVisibility(View.VISIBLE);
            }
        }
    }

}

