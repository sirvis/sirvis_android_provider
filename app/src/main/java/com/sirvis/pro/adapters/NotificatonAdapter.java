package com.sirvis.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.pojo.notification.NotificationData;
import com.sirvis.pro.utility.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by murashid on 09-Jun-18.
 */

public class NotificatonAdapter extends RecyclerView.Adapter<NotificatonAdapter.ViewHolder> {

    private Context context;
    private ArrayList<NotificationData> notificationData;
    private Typeface fontRegular;

    private SimpleDateFormat serverFormat, displayFormat;

    public NotificatonAdapter(Context context, ArrayList<NotificationData> notificationData)
    {
        this.context = context;
        this.notificationData = notificationData;
        fontRegular = Utility.getFontRegular(context);

        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        displayFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);

    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvNotification, tvTime;

        public ViewHolder(View itemView)
        {
            super(itemView);

            tvNotification = itemView.findViewById(R.id.tvNotification);
            tvTime = itemView.findViewById(R.id.tvTime);

            tvNotification.setTypeface(fontRegular);
            tvTime.setTypeface(fontRegular);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_notification_list, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        holder.tvNotification.setText(notificationData.get(position).getMsg());
        holder.tvTime.setText(getDisplayTime(notificationData.get(position).getTimeStamp(),""));
    }

    @Override
    public int getItemCount() {
        return notificationData.size();
    }

    private String getDisplayTime(String date, String serverTime)
    {
        long start = Utility.convertUTCToTimeStamp(date);
        long current = System.currentTimeMillis();
        //long current = Utility.convertUTCToTimeStamp(serverTime);
        long totalSecs = (current - start) / 1000;
        if(totalSecs > 86400)
        {
            try {
                return displayFormat.format(serverFormat.parse(Utility.convertUTCToServerFormat(date)));
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return "";
            }
        }
        else if( totalSecs < 86400 && totalSecs > 3600)
        {
            return String.valueOf(TimeUnit.SECONDS.toHours(totalSecs))+context.getString(R.string.hrs);
        }
        else if(totalSecs < 3600 && totalSecs > 60)
        {
            return String.valueOf(TimeUnit.SECONDS.toMinutes(totalSecs))+context.getString(R.string.mins);
        }
        else
        {
            return String.valueOf(totalSecs)+context.getString(R.string.secs);
        }
    }

}
