package com.sirvis.pro.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sirvis.pro.R;
import com.sirvis.pro.pojo.booking.CancelData;
import com.sirvis.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 08-Nov-17.
 */

public class CancelListAdapter extends RecyclerView.Adapter<CancelListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CancelData> cancelData;
    private CancelSelected cancelSelected;
    private TextView tvTemp;
    private RadioButton rbCancel;
    private Typeface fontMedium,fontRegular;

    public CancelListAdapter(Context context, ArrayList<CancelData> cancelData, CancelSelected cancelSelected)
    {
        this.context = context;
        this.cancelData = cancelData;
        this.cancelSelected = cancelSelected;
        fontRegular = Utility.getFontRegular(context);
        fontMedium = Utility.getFontMedium(context);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvCancelReason;
        RadioButton rbCancel;

        ViewHolder(View itemView) {
            super(itemView);
            tvCancelReason = itemView.findViewById(R.id.tvCancelReason);
            rbCancel = itemView.findViewById(R.id.rbCancel);
            tvCancelReason.setTypeface(fontRegular);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cancel_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.tvCancelReason.setText(cancelData.get(position).getReason());

        holder.tvCancelReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSelected.onCancelSeleted(cancelData.get(holder.getAdapterPosition()).getRes_id());
                holder.tvCancelReason.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                holder.tvCancelReason.setTypeface(fontMedium);
                holder.rbCancel.setChecked(true);
                if(tvTemp !=null && tvTemp != holder.tvCancelReason)
                {
                    tvTemp.setTextColor(ContextCompat.getColor(context,R.color.darkTextColor));
                    rbCancel.setChecked(false);
                    tvTemp.setTypeface(fontRegular);
                }
                tvTemp = holder.tvCancelReason;
                rbCancel = holder.rbCancel;
            }
        });
    }

    @Override
    public int getItemCount() {
        return cancelData.size();
    }

    /**
     * <h1>SignupSelectionListener</h1>
     * Interface for selected the city callback
     * */

    public interface CancelSelected
    {
        void onCancelSeleted(String id);
    }
}
