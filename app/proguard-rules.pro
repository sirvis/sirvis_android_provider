# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\murashid\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
# http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-ignorewarnings
-dontobfuscate
#BottomNavigationMenuView
-keep class android.support.design.internal.BottomNavigationMenuView{*;}

#Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

#CompactCalendarView
-keep class com.github.sundeepk.compactcalendarview.CompactCalendarView{*;}
-keep class com.github.sundeepk.compactcalendarview.domain.Event{*;}

# OkHttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontwarn okio.**
-keep class android.databinding.** { *; }
-dontwarn android.databinding.**

#Mqtt
-keepattributes InnerClasses
-keepattributes EnclosingMethod
-keepattributes EnclosingMethod
-keepattributes Exceptions, Signature, InnerClasses
-keep class org.eclipse.paho.android.service.MqttAndroidClient { *; }
-keep class org.eclipse.paho.client.mqttv3.IMqttActionListener { *; }
-keep class org.eclipse.paho.client.mqttv3.IMqttDeliveryToken { *; }
-keep class org.eclipse.paho.client.mqttv3.IMqttToken { *; }
-keep class org.eclipse.paho.client.mqttv3.MqttCallback { *; }
-keep class org.eclipse.paho.client.mqttv3.MqttConnectOptions { *; }
-keep class org.eclipse.paho.client.mqttv3.MqttException { *; }
-keep class org.eclipse.paho.client.mqttv3.MqttMessage { *; }

-keep class android.support.v7.widget.** { *; }
#Mixpanel
-dontwarn com.mixpanel.**

# io card
-keep class io.card.payment.* {*;}

#MPAndroidChart
-keep class com.github.mikephil.charting.** { *; }

-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry

#Amazon
# Class names are needed in reflection
-keepnames class com.amazonaws.**
-keepnames class com.amazon.**
# Request handlers defined in request.handlers
-keep class com.amazonaws.services.**.*Handler
# The following are referenced but aren't required to run
-dontwarn com.fasterxml.jackson.**
-dontwarn org.apache.commons.logging.**
# Android 6.0 release removes support for the Apache HTTP client
-dontwarn org.apache.http.**
# The SDK has several references of Apache HTTP client
-dontwarn com.amazonaws.http.**
-dontwarn com.amazonaws.metrics.**

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-dontwarn java.lang.management.**



#WebRtc
-keep class org.webrtc.** { *; }

-keep class com.amazonaws.services.cognitoidentityprovider.** { *; }
##---------------End: proguard configuration for Gson  ----------
